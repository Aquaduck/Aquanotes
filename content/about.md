+++
title = "about"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Todo {#todo}


### <span class="org-todo todo TODO">TODO</span> Reformat to make backlinks more legible {#reformat-to-make-backlinks-more-legible}


### <span class="org-todo todo TODO">TODO</span> Make tags work properly {#make-tags-work-properly}


### <span class="org-todo todo TODO">TODO</span> Organize notes on landing page better {#organize-notes-on-landing-page-better}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
