+++
title = "Describe how vesicles move, recognize, and fuse with the target compartment."
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Fusion of Cells by Flipped SNARES]({{< relref "fusion_of_cells_by_flipped_snares" >}}) {#from-fusion-of-cells-by-flipped-snares--fusion-of-cells-by-flipped-snares-dot-md}

-   Fusion of intracellular membranes is mediated by [SNARE]({{< relref "snare" >}}) proteins that assemble between lipid bilayers as SNAREpins
    -   [SNAREpins]({{< relref "snarepin" >}}) consist of a bundle of four helices
        -   Four fusion to occur, three of these alpha-helices (contributed by target membrane or [t-SNARE]({{< relref "t_snare" >}})) emanate from one of the membrane partners
        -   The fourth, longer alpha-helix is contributed by the vesicle or [v-SNARE]({{< relref "v_snare" >}}) is rooted in the opposing membrane
    -   The spontaneous formation of the SNARE complex is coupled to [SNAREpins]({{< relref "snarepin" >}}) to promote fusion of the lipid bilayers to the target membrane
-   SNARE dependent fusion is **very specific**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-01 Wed] </span></span> > Organelles & Trafficking I (+ II) > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Describe how vesicles move, recognize, and fuse with the target compartment.]({{< relref "describe_how_vesicles_move_recognize_and_fuse_with_the_target_compartment" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
