+++
title = "Renin-angiotensin-aldosterone mechanism"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Components:
    -   [Renin]({{< relref "renin" >}})
    -   [Angiotensin I]({{< relref "angiotensin_i" >}})
    -   [Angiotensin II]({{< relref "angiotensin_ii" >}})
    -   [Aldosterone]({{< relref "aldosterone" >}})
-   Major effector on the [Cardiovascular system]({{< relref "cardiovascular_system" >}})
-   Ultimately leads to the reabsorption of [sodium]({{< relref "sodium" >}}) by the [kidneys]({{< relref "kidney" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology**

    <!--list-separator-->

    -  [Renin-Angiotensin-Aldosterone Mechanism]({{< relref "renin_angiotensin_aldosterone_mechanism" >}}) (p. 927)

        -   Major effect on the [cardiovascular system]({{< relref "cardiovascular_system" >}})

        <!--list-separator-->

        -  [Renin]({{< relref "renin" >}}) + Angiotensin

            -   Specialized cells in the [kidneys]({{< relref "kidney" >}}) found in the [juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}}) respond to decreased blood flow by secreting _renin_ into the blood
                -   Converts the plasma protein [angiotensinogen]({{< relref "angiotensinogen" >}}) (produced by the liver) into its active form - [angiotensin I]({{< relref "angiotensin_i" >}})

        <!--list-separator-->

        -  Angiotensin ([I]({{< relref "angiotensin_i" >}}) + [II]({{< relref "angiotensin_ii" >}}))

            -   Angiotensin I circulates in the blood -> reaches lungs -> converted into Angiotensin II
            -   Angiotensin II is a powerful **vasoconstrictor** -> greatly increases blood pressure
            -   Angiotensin II stimulates the release of [ADH]({{< relref "antidiuretic_hormone" >}}) from the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) and [aldosterone]({{< relref "aldosterone" >}}) from the [adrenal cortex]({{< relref "adrenal_cortex" >}})
            -   Angiotensin II stimulates the thirst center in the hypothalamus -> increase of fluid consumption -> increase blood volume and blood pressure

        <!--list-separator-->

        -  [Aldosterone]({{< relref "aldosterone" >}})

            -   Increases the reabsorption of [sodium]({{< relref "sodium" >}}) into the [blood]({{< relref "blood" >}}) by the [kidneys]({{< relref "kidney" >}}) -> **increases reabsorption of water** -> increases blood volume -> raises blood pressure


### Unlinked references {#unlinked-references}

[Show unlinked references]
