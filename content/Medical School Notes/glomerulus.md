+++
title = "Glomerulus"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A tuft of capillaries within the [Bowman capsule]({{< relref "bowman_capsule" >}}) of the kidneys formed of three parts


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Mesangial cell]({{< relref "mesangial_cell" >}}) {#mesangial-cell--mesangial-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Remove trapped material from basement membrane of the capillaries in the [Glomerulus]({{< relref "glomerulus" >}})

    ---


#### [Efferent arteriole of the kidney]({{< relref "efferent_arteriole_of_the_kidney" >}}) {#efferent-arteriole-of-the-kidney--efferent-arteriole-of-the-kidney-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The blood vessels that carries previously filtered blood away from the [Glomerulus]({{< relref "glomerulus" >}})

    ---


#### [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}}) {#describe-glomerular-anatomy--ie-efferent-afferent-arteriole----describe-glomerular-anatomy-ie-efferent-afferent-arteriole-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal corpuscle (p. 17)**

    [Mesangial cell]({{< relref "mesangial_cell" >}}): act as phagocytes -> remove trapped material from basement membrane of the capillaries in the [Glomerulus]({{< relref "glomerulus" >}})

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal corpuscle (p. 17)**

    Filled with a compact tuft of interconnected capillary loops called the [Glomerulus]({{< relref "glomerulus" >}})

    ---


#### [Bowman capsule]({{< relref "bowman_capsule" >}}) {#bowman-capsule--bowman-capsule-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Encloses the [Glomerulus]({{< relref "glomerulus" >}}) within the [Renal corpuscle]({{< relref "renal_corpuscle" >}})

    ---


#### [Afferent arteriole of the kidney]({{< relref "afferent_arteriole_of_the_kidney" >}}) {#afferent-arteriole-of-the-kidney--afferent-arteriole-of-the-kidney-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The blood vessel that enters and supplies the [glomerulus]({{< relref "glomerulus" >}}) of the kidney for filtration

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
