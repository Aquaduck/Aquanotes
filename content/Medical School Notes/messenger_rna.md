+++
title = "Messenger RNA"
author = ["Arif Ahsan"]
date = 2021-07-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [RNA]({{< relref "rna" >}}) {#rna--rna-dot-md}

<!--list-separator-->

-  **🔖 Types of RNA**

    **[Messenger RNA]({{< relref "messenger_rna" >}})** (mRNA)

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Ribosomes > Function**

    _Small ribosomal unit_ binds [mRNA]({{< relref "messenger_rna" >}}) and activated [tRNAs]({{< relref "transfer_rna" >}}) -> codons of mRNA base-pair with corresponding anticodons of tRNAs

    ---

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Ribosomes > Structure**

    _Polyribosome (polysome)_: a cluster of ribosomes along a single strand of [mRNA]({{< relref "messenger_rna" >}}) engaged w/ the synthesis of [protein]({{< relref "protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
