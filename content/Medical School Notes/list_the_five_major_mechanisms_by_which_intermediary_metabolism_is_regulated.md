+++
title = "List the five major mechanisms by which intermediary metabolism is regulated"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

From [Intermediary Metabolism and its Regulation]({{< relref "intermediary_metabolism_and_its_regulation" >}})


## Summary of major regulators of intermediary metabolism {#summary-of-major-regulators-of-intermediary-metabolism}

<div class="table-caption">
  <span class="table-number">Table 1</span>:
  The common mechanisms by which enzyme activity is regulated
</div>

| Regulator event                    | Typical effector      | Results                                          | Time required for change |
|------------------------------------|-----------------------|--------------------------------------------------|--------------------------|
| Substrate availability             | Substrate             | Change in v<sub>o</sub>                          | Immediate                |
| Product inhibition                 | Reaction product      | Change in V<sub>max</sub> and/or K<sub>m</sub>   | Immediate                |
| Allosteric control                 | Pathway end product   | Change in V<sub>max</sub> and/or K<sub>0.5</sub> | Immediate                |
| Covalent modification              | Another enzyme        | Change in V<sub>max</sub> and/or K<sub>m</sub>   | Immediate to minutes     |
| Synthesis or degradation of enzyme | Hormone or metabolite | Change in amount of enzyme                       | Hours to days            |

Note: First two mechanisms detailed in JumpStart, Allosteric control covered in [Kinetic and Allosteric Regulation of Glucose Regulation]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}})


## Examples in glucose catabolism {#examples-in-glucose-catabolism}


### Substrate availability {#substrate-availability}

-   [Hexokinase]({{< relref "hexokinase" >}}) needs glucose


### Product inhibition {#product-inhibition}

-   [Hexokinase]({{< relref "hexokinase" >}}) inhibited by [G6P]({{< relref "glucose_6_phosphate" >}})
    -   Note: **[glucokinase]({{< relref "glucokinase" >}}) not subject to same regulation**


### Allosteric control {#allosteric-control}

-   [PFK-1]({{< relref "6_phosphofructo_1_kinase" >}})
    -   Inhibited by [citrate]({{< relref "citrate" >}})
    -   Regulated by energy charge
-   [Pyruvate kinase]({{< relref "pyruvate_kinase" >}})
    -   Activated by fructose-1,6-bisphosphate
    -   Inhibited by:
        -   Alanine
        -   Long-chain fatty acids
    -   Regulated by energy charge


### Covalent modification {#covalent-modification}

-   [Pyruvate kinase]({{< relref "pyruvate_kinase" >}})
    -   Downregulated by phosphorylation by PKA
-   [PFK-2]({{< relref "pfk_2" >}})
    -   _Kinase component_ downregulated by phosphorylation by PKA -> increase activity of _phosphatase_ component


### Synthesis or degradation of enzyme {#synthesis-or-degradation-of-enzyme}

-   Insulin stimulates glycolysis
-   Glucagon inhibits glycolysis


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the five common mechanisms regulating intermediary metabolism and give examples of their use in glucose catabolism.]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}})

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
