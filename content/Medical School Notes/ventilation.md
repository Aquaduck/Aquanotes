+++
title = "Ventilation"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [V/Q ratio]({{< relref "v_q_ratio" >}}) {#v-q-ratio--v-q-ratio-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Ventilation]({{< relref "ventilation" >}})-[perfusion]({{< relref "perfusion" >}}) ratio

    ---


#### [Physiology, Pulmonary Ventilation and Perfusion]({{< relref "physiology_pulmonary_ventilation_and_perfusion" >}}) {#physiology-pulmonary-ventilation-and-perfusion--physiology-pulmonary-ventilation-and-perfusion-dot-md}

<!--list-separator-->

-  **🔖 Physiology, Pulmonary Ventilation and Perfusion > Introduction**

    One of the major roles of the lungs is to facilitate gas exchange between the circulatory system and the external environment. The lungs are composed of branching airways that terminate in respiratory bronchioles and alveoli, which participate in gas exchange. Most bronchioles and large airways are part of the conducting zone of the lung, which delivers gas to sites of gas exchange in alveoli. Gas exchange occurs in the lungs between alveolar air and blood of the pulmonary capillaries. For effective gas exchange to occur, alveoli must be ventilated and perfused. [Ventilation]({{< relref "ventilation" >}}) (V) refers to the flow of air into and out of the alveoli, while [perfusion]({{< relref "perfusion" >}}) (Q) refers to the flow of blood to alveolar capillaries. Individual alveoli have variable degrees of ventilation and perfusion in different regions of the lungs. Collective changes in ventilation and perfusion in the lungs are measured clinically using the ratio of ventilation to perfusion (V/Q). Changes in the V/Q ratio can affect gas exchange and can contribute to hypoxemia.

    ---


#### [Physiological dead space]({{< relref "physiological_dead_space" >}}) {#physiological-dead-space--physiological-dead-space-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Volume of inspired air that does not participate in gas exchange during [ventilation]({{< relref "ventilation" >}})

    ---


#### [Inspiration]({{< relref "inspiration" >}}) {#inspiration--inspiration-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The part of [ventilation]({{< relref "ventilation" >}}) whewre air is taken into the [lungs]({{< relref "lung" >}})

    ---


#### [Hyperventilation]({{< relref "hyperventilation" >}}) {#hyperventilation--hyperventilation-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Abnormally raised [Ventilation]({{< relref "ventilation" >}}) rate

    ---


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  Changes in [Ventilation]({{< relref "ventilation" >}}) (p. 664)

    -   When exercise becomes more vigorous -> increased [lactic acid]({{< relref "lactic_acid" >}}) production -> liberation of more [carbon dioxide]({{< relref "carbon_dioxide" >}}) (CO<sub>2</sub>) -> increase in ventilation
        -   [Hyperventilation]({{< relref "hyperventilation" >}}) -> alveolar oxygen pressure increases
        -   Further accumulation of lactic acid -> increase in ventilation **outstrips** carbon dioxide production -> alveolar + arterial carbon dioxide pressure falls
            -   This provides **respiratory compensation** for [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) produced by increased lactic acid
                -   The additional increase in ventilation produced by acidosis **depends on [carotid bodies]({{< relref "carotid_body" >}})** and does not occur if they are removed


#### [Exhalation]({{< relref "exhalation" >}}) {#exhalation--exhalation-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of [Ventilation]({{< relref "ventilation" >}}) that expels air from the lungs

    ---


#### [Define the difference between V/Q for a shunt vs. V/Q for a dead space.]({{< relref "define_the_difference_between_v_q_for_a_shunt_vs_v_q_for_a_dead_space" >}}) {#define-the-difference-between-v-q-for-a-shunt-vs-dot-v-q-for-a-dead-space-dot--define-the-difference-between-v-q-for-a-shunt-vs-v-q-for-a-dead-space-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > V/Q mismatch > Increased V/Q ratio**

    **Indicative of [dead space]({{< relref "physiological_dead_space" >}})**: Volume of inspired air that does not participate in gas exchange during [ventilation]({{< relref "ventilation" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > V/Q ratio**

    The volumetric ratio of air that reaches the alveoli ([ventilation]({{< relref "ventilation" >}})) to alveolar blood supply ([perfusion]({{< relref "perfusion" >}})) per minute

    ---


#### [Alveolar dead space]({{< relref "alveolar_dead_space" >}}) {#alveolar-dead-space--alveolar-dead-space-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Alveoli are [ventilated]({{< relref "ventilation" >}}) but not perfused

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
