+++
title = "Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## By enzyme {#by-enzyme}


### [Pyruvate DH complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dh-complex--pyruvate-dehydrogenase-complex-dot-md}

-   Thiamine (B1) (as thiamine pyrophosphate) is a cofactor
-   Riboflavin (B2) in the form of FAD
-   Niacin (B3) in the form of NAD<sup>+</sup>
-   Pantothenic acid (B5) in the form of CoA


### [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) {#pyruvate-carboxylase--pyruvate-carboxylase-dot-md}

-   Biotin (B7) is a cofactor of carboxylases


### [Acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}}) {#acetyl-coa-carboxylase--acetyl-coa-carboxylase-dot-md}

-   Biotin (B7) is a cofactor of carboxylases


### [Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}}) {#propionyl-coa-carboxylase--propionyl-coa-carboxylase-dot-md}

-   Biotin (B7) is a cofactor of carboxylases


### [alpha-KG DH]({{< relref "α_ketoglutarate_dehydrogenase" >}}) {#alpha-kg-dh--α-ketoglutarate-dehydrogenase-dot-md}

-   Thiamine (B1) (as thiamine pyrophosphate) is a cofactor
-   Riboflavin (B2) in the form of FAD
-   Niacin (B3) in the form of NAD<sup>+</sup>
-   Pantothenic acid (B5) in the form of CoA


### [Malate DH]({{< relref "malate_dehydrogenase" >}}) {#malate-dh--malate-dehydrogenase-dot-md}

-   Niacin (B3) in the form of NAD<sup>+</sup>


### [Succinate DH]({{< relref "succinate_dehydrogenase" >}}) {#succinate-dh--succinate-dehydrogenase-dot-md}

-   Riboflavin (B2) in the form of FAD


### [Isocitrate DH]({{< relref "isocitrate_dehydrogenase" >}}) {#isocitrate-dh--isocitrate-dehydrogenase-dot-md}

-   Niacin (B3) in the form of NAD<sup>+</sup>


### [PEP CK]({{< relref "pep_carboxykinase" >}}) {#pep-ck--pep-carboxykinase-dot-md}

-   Pyridoxine (B6) (as pyridoxal phosphate) is a cofactor used in decarboxylation reactions


## By vitamin {#by-vitamin}


### [Thiamine]({{< relref "thiamine" >}}) (B1) {#thiamine--thiamine-dot-md----b1}

-   Pyruvate DH complex
-   alpha-KG DH


### [Riboflavin]({{< relref "riboflavin" >}}) (B2) {#riboflavin--riboflavin-dot-md----b2}

-   Succinate DH
-   Pyruvate DH complex
-   alpha-KG DH


### [Niacin]({{< relref "niacin" >}}) (B3) {#niacin--niacin-dot-md----b3}

-   Pyruvate DH complex
-   alpha-KG DH
-   Malate DH
-   Isocitrate DH


### [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5) {#pantothenic-acid--pantothenic-acid-dot-md----b5}

-   Pyruvate DH complex
-   alpha-KG DH


### [Pyridoxine]({{< relref "pyridoxine" >}}) (B6) {#pyridoxine--pyridoxine-dot-md----b6}

-   PEP CK


### [Biotin]({{< relref "biotin" >}}) (B7) {#biotin--biotin-dot-md----b7}

-   Pyruvate carboxylase
-   Acetyl-CoA carboxylase
-   Propionyl-CoA carboxylase


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
