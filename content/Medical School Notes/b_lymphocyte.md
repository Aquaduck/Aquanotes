+++
title = "B lymphocyte"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   One of two [lymphocytes]({{< relref "lymphocyte" >}}) as part of the [adaptive immune system]({{< relref "adaptive_immunity" >}})


## Backlinks {#backlinks}


### 9 linked references {#9-linked-references}


#### [Plasma cell]({{< relref "plasma_cell" >}}) {#plasma-cell--plasma-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A mature [B-lymphocyte]({{< relref "b_lymphocyte" >}}) that produces [antibodies]({{< relref "immunoglobulin" >}})

    ---


#### [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}}) {#list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot--list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymph nodes > Parts of the lymph node:**

    Follicles consist mostly of [B-lymphocytes]({{< relref "b_lymphocyte" >}})

    ---


#### [Germinal center]({{< relref "germinal_center" >}}) {#germinal-center--germinal-center-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Where [helper T cells]({{< relref "cd4_positive_t_lymphocyte" >}}) cause [B-cells]({{< relref "b_lymphocyte" >}}) to go through [somatic hypermutation]({{< relref "somatic_hypermutation" >}}) -> create more of a specific [antibody]({{< relref "immunoglobulin" >}})

    ---


#### [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}}) {#describe-the-major-actions-occurring-in-these-regions--with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot----describe-the-major-actions-occurring-in-these-regions-with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Follicles**

    [B-cells]({{< relref "b_lymphocyte" >}}) waiting to recognize antigen flowing in the lymph are located in the cortex in [primary follicles]({{< relref "primary_follicle" >}})

    ---


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Summary of actions of B and T lymphocytes**

    <!--list-separator-->

    -  [B lymphocytes]({{< relref "b_lymphocyte" >}})

        -   Usually dependent on an activated [T lymphocyte]({{< relref "t_lymphocyte" >}}) to help them become activated

        <!--list-separator-->

        -  Life cycle

            1.  Rearrangement (VDJ recombination) of the BCR (Ig) in bone marrow
            2.  Naive and memory cells circulate and migrate in tissues (especially lymphoid) looking for cognate antigen
            3.  Activate and proliferate in germinal centers where immunoglobulin mutation (somatic hypermutation) improves specificity of the immunoglobulin with T-cell help
            4.  Effector action through plasma cells -> produce highly specific immunoglobulin that enhances actions of innate system
            5.  Many effector cells die after threat abates, some survive as memory cells and return to step 2

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Summary of actions of B and T lymphocytes**

    Both [T-cells]({{< relref "t_lymphocyte" >}}) and [B-cells]({{< relref "b_lymphocyte" >}}) derived from [hematopoietic stem cells]({{< relref "hematopoietic_stem_cell" >}}) of the bone marrow

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Activation and actions - T and B lymphocytes**

    <!--list-separator-->

    -  [B lymphocyte]({{< relref "b_lymphocyte" >}})

        -   Activated by **recognition of intact foreign molecules ([antigens]({{< relref "antigen" >}}))**

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Activation and actions - T and B lymphocytes > T lymphocytes**

    Assist activated [B-cells]({{< relref "b_lymphocyte" >}}) to improve binding of their receptor and differentiation into plasma cells

    ---


#### [CD4-positive T lymphocyte]({{< relref "cd4_positive_t_lymphocyte" >}}) {#cd4-positive-t-lymphocyte--cd4-positive-t-lymphocyte-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Assist activated [B lymphocyte]({{< relref "b_lymphocyte" >}})s

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
