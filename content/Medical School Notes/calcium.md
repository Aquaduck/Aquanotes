+++
title = "Calcium"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [Terminal cisternae]({{< relref "terminal_cisternae" >}}) {#terminal-cisternae--terminal-cisternae-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Store [Calcium]({{< relref "calcium" >}}) and release it when an action potential courses down the t tubules

    ---


#### [Osteocyte]({{< relref "osteocyte" >}}) {#osteocyte--osteocyte-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Involved in [Calcium]({{< relref "calcium" >}}) homeostasis

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Absorption (p. 1144) > Mineral absorption**

    <!--list-separator-->

    -  [Calcium]({{< relref "calcium" >}})

        -   Blood levels of ionic calcium determine the absorption of dietary calcium
        -   When blood levels of ionic calcium drop -> [PTH]({{< relref "parathyroid_hormone" >}}) secreted -> stimulates **release of calcium ions from bone matrices** + **increases reabsorption of calcium by [kidneys]({{< relref "kidney" >}})**
        -   [PTH]({{< relref "parathyroid_hormone" >}}) upregulates the activation of [vitamin D]({{< relref "vitamin_d" >}}) in the [kidney]({{< relref "kidney" >}}) -> facilitates intestinal calcium ion absorption


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Cytologic components of mature bone**

    [Osteocytes]({{< relref "osteocyte" >}}) - emtombed in bone but highly metabolically active in [calcium]({{< relref "calcium" >}}) homeostasis

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Histologic components of mature bone**

    [Calcium]({{< relref "calcium" >}}) is bound to the bone in a crystlaline form called [calcium hydroxyapatite]({{< relref "calcium_hydroxyapatite" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Store [Calcium]({{< relref "calcium" >}}) and release it when an action potential courses down the [t tubules]({{< relref "transverse_tubule" >}})

    ---


#### [Describe the process of neuromuscular transmission.]({{< relref "describe_the_process_of_neuromuscular_transmission" >}}) {#describe-the-process-of-neuromuscular-transmission-dot--describe-the-process-of-neuromuscular-transmission-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Conduction of a nerve impulse across a myoneural junction (p. 116)**

    **Voltage-gated [calcium]({{< relref "calcium" >}}) channels** open -> calcium enters axon terminal

    ---


#### [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}}) {#describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot--describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Regulation of the mobilization of intramuscular fat > Role of skeletal muscle contraction**

    Stimulation of ER to release [Calcium]({{< relref "calcium" >}})

    ---


#### [Calcium hydroxyapatite]({{< relref "calcium_hydroxyapatite" >}}) {#calcium-hydroxyapatite--calcium-hydroxyapatite-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Crystalline form of [Calcium]({{< relref "calcium" >}}) found in [Bone]({{< relref "bone" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Atherosclerosis**

    Formation of [lipid]({{< relref "lipid" >}}), [cholesterol]({{< relref "cholesterol" >}}), and/or [calcium]({{< relref "calcium" >}})-laden plaques within [tunica intima]({{< relref "tunica_intima" >}}) of arterial wall -> restricts [blood]({{< relref "blood" >}}) flow

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
