+++
title = "Discuss the monomeric GTPases and how they are regulated, with specific reference to Ras."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### The Monomeric [GTPases]({{< relref "g_protein_gtpase" >}}) {#the-monomeric-gtpases--g-protein-gtpase-dot-md}

-   Small molecular weight GTPases analogous to the α-subunit of the trimeric G-protein family
-   Located in cytoplasm of cell
-   Has a wide variety of functions


### [Ras]({{< relref "ras" >}}) {#ras--ras-dot-md}


#### Activation {#activation}

-   Requires a Guanine Nucleotide Exchange Factor ([GEF]({{< relref "guanine_nucleotide_exchange_factor" >}}))
-   Replaces GDP on an inactive Ras with GTP
-   Analogous to the hormone-receptor complex of the plasma membrane trimeric G-proteins


#### Inactivation {#inactivation}

-   Requires GTPase-Activating Protein ([GAP]({{< relref "gtpase_activating_protein" >}}))
    -   Significantly boost intrinsic GTPase activity


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Discuss the monomeric GTPases and how they are regulated, with specific reference to Ras.]({{< relref "discuss_the_monomeric_gtpases_and_how_they_are_regulated_with_specific_reference_to_ras" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
