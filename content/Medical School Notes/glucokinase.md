+++
title = "Glucokinase"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A special [Hexokinase]({{< relref "hexokinase" >}})
-   Restricted to [liver]({{< relref "liver" >}}) parenchymal cells and β-cells of [pancreas]({{< relref "pancreas" >}})
-   Has cooperativity with [[glucose]({{< relref "glucose" >}})]
-   Prevents [hyperglycemia]({{< relref "hyperglycemia" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}}) {#list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated--list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated-dot-md}

<!--list-separator-->

-  **🔖 Examples in glucose catabolism > Product inhibition**

    Note: **[glucokinase]({{< relref "glucokinase" >}}) not subject to same regulation**

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis > Glucose-6-phosphatase**

    Reverses [glucokinase]({{< relref "glucokinase" >}})

    ---

<!--list-separator-->

-  **🔖 Glycolysis**

    <!--list-separator-->

    -  [Glucokinase]({{< relref "glucokinase" >}}) (hexokinase IV)

        -   **Restricted to liver parenchymal cells and β-cells of pancreas**
        -   Has cooperativity with [glucose]
        -   Has a **much higher K<sub>m</sub> and V<sub>max</sub>**
            -   Low affinity for glucose ensures that when [glucose] is high, it will be trapped in the liver
                -   Whereas when [glucose] is low, it **will not be recognized as a substrate**
                -   This prevents [hyperglycemia]({{< relref "hyperglycemia" >}})
        -   **Not subject to regulation by product inhibition**
            -   Regulated by glucokinase regulatory protein (GKRP) (_outside scope of FoCS_)


### Unlinked references {#unlinked-references}

[Show unlinked references]
