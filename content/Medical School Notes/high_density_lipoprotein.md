+++
title = "High density lipoprotein"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoCII > From Amboss**

    A component of [chylomicrons]({{< relref "chylomicron" >}}), [VLDL]({{< relref "very_low_density_lipoprotein" >}}), and [HDL]({{< relref "high_density_lipoprotein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
