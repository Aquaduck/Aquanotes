+++
title = "Fatty acid synthase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Pantothenic acid]({{< relref "pantothenic_acid" >}}) {#pantothenic-acid--pantothenic-acid-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Essential component of [coenzyme A]({{< relref "coenzyme_a" >}}) (used in synthesis) and [fatty acid synthase]({{< relref "fatty_acid_synthase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
