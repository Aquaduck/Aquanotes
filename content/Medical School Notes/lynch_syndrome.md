+++
title = "Lynch syndrome"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Lynch syndrome (hereditary nonpolyposis colorectal cancer): Clinical manifestations and diagnosis]({{< relref "lynch_syndrome_hereditary_nonpolyposis_colorectal_cancer_clinical_manifestations_and_diagnosis" >}}) {#lynch-syndrome--hereditary-nonpolyposis-colorectal-cancer--clinical-manifestations-and-diagnosis--lynch-syndrome-hereditary-nonpolyposis-colorectal-cancer-clinical-manifestations-and-diagnosis-dot-md}

<!--list-separator-->

-  [Lynch syndrome]({{< relref "lynch_syndrome" >}})

    <!--list-separator-->

    -  Introduction

        -   Lynch syndrome is the most common cause of [inherited colorectal cancer]({{< relref "inherited_colorectal_cancer" >}}) (CRC)

    <!--list-separator-->

    -  Genetics

        -   Lynch syndrome is an autosomal dominant disorder that is caused by a **germline mutation** in one of several [DNA mismatch repair]({{< relref "dna_mismatch_repair" >}}) (MMR) genes or loss of expression of [MSH2]({{< relref "msh2" >}})
            -   Despite being autosomal dominant, **both alleles need to be inactivated in order for MMR function to become defective**
            -   As a general rule, patients with Lynch syndrome have a germline mutation in one allele of an MMR gene and the second allele is inactivated somatically by mutation, loss of heterozygosity, or epigenetic silencing by promotor hypermethylation


### Unlinked references {#unlinked-references}

[Show unlinked references]
