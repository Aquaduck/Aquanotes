+++
title = "Neurofilament"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A cytoskeletal component of [neurons]({{< relref "neuron" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the general morphology of neurons and levels of organization of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}}) {#describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot--describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127) > Neuron structure > Axons**

    [Neurofilaments]({{< relref "neurofilament" >}}) regulate axon diameter

    ---

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127) > Neuron structure > Neuronal cell body > Cytoskeletal components**

    [Neurofilaments]({{< relref "neurofilament" >}}) run throughout the soma cytoplasm

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
