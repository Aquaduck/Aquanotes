+++
title = "Neuron"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 13 linked references {#13-linked-references}


#### [Unipolar neuron]({{< relref "unipolar_neuron" >}}) {#unipolar-neuron--unipolar-neuron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A morphological type of [neuron]({{< relref "neuron" >}})

    ---


#### [Sensory neuron]({{< relref "sensory_neuron" >}}) {#sensory-neuron--sensory-neuron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A functional type of [neuron]({{< relref "neuron" >}})

    ---


#### [Pseudounipolar neuron]({{< relref "pseudounipolar_neuron" >}}) {#pseudounipolar-neuron--pseudounipolar-neuron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A morphological type of [neuron]({{< relref "neuron" >}})

    ---


#### [Nissl body]({{< relref "nissl_body" >}}) {#nissl-body--nissl-body-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An organelle found in [neurons]({{< relref "neuron" >}})

    ---


#### [Neurofilament]({{< relref "neurofilament" >}}) {#neurofilament--neurofilament-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A cytoskeletal component of [neurons]({{< relref "neuron" >}})

    ---


#### [Multipolar neuron]({{< relref "multipolar_neuron" >}}) {#multipolar-neuron--multipolar-neuron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A morphological type of [neuron]({{< relref "neuron" >}})

    ---


#### [Motor neuron]({{< relref "motor_neuron" >}}) {#motor-neuron--motor-neuron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A functional type of [neuron]({{< relref "neuron" >}})

    ---


#### [Interneuron]({{< relref "interneuron" >}}) {#interneuron--interneuron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A functional type of [neuron]({{< relref "neuron" >}})

    ---


#### [Describe the general morphology of neurons and levels of organization of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}}) {#describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot--describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127)**

    <!--list-separator-->

    -  [Neuron]({{< relref "neuron" >}}) structure

        <!--list-separator-->

        -  Neuronal cell body

            -   Region of a neuron containing the nucleus, various cytoplasmic organelles and inclusions, and cytoskeletal components

            <!--list-separator-->

            -  Nucleus

                -   Large, spherical, pale staining
                -   **Centrally** located in the soma of most neurons
                -   Contains abundant euchromatin and a large nucleolus

            <!--list-separator-->

            -  Cytoplasmic organelles and inclusions

                -   [Nissl bodies]({{< relref "nissl_body" >}}) are composed of polysomes and RER
                    -   Appear as clumps under light microscopy
                    -   Most abundant in **large motor proteins**
                -   _Golgi complex_ is near the nucleus, and _mitochondria_ are scattered throughout the cytoplasm
                -   _Melanin-containing granules_ are present in some neurons in the CNS and in the dorsal root and sympathetic ganglia
                -   _Lipofuscin-containing granules_ are present in some neurons
                    -   Increase in number with age
                -   _Lipid droplets_ occasionally present

            <!--list-separator-->

            -  Cytoskeletal components

                -   [Neurofilaments]({{< relref "neurofilament" >}}) run throughout the soma cytoplasm
                    -   Are [intermediate filaments]({{< relref "intermediate_filament" >}}) composed of three intertwining polypeptide chains
                -   _Microtubules_ are also present in the soma cytoplasm
                -   _Microfilaments_ are associated with the plasma membrane

        <!--list-separator-->

        -  [Dendrites]({{< relref "dendrite" >}})

            -   Receive stimuli from sensory cells, axons, or other neurons -> convert these signals into action potentials transmitted **towards the soma**
            -   Lacks a Golgi complex
            -   Organelles are reduced or absent near terminals **except for mitochondria**, which are abundant
            -   Spines on surface of dendrites increase surface area for synapse formation
                -   Diminish with age and poor neutrition

        <!--list-separator-->

        -  [Axons]({{< relref "axon" >}})

            -   Conduct impulses **away from** the soma to axon terminals **without any diminuition in their strength**
            -   Originate from the [axon hillock]({{< relref "axon_hillock" >}})
                -   A specialized region of the soma that lacks RER, ribosomes, Golgi cisternae, and Nissle bodies
                -   Contains many microtubules and neurofilaments
                    -   [Neurofilaments]({{< relref "neurofilament" >}}) regulate axon diameter
                    -   Permits passage of mitochondria and vesicles into the axon
            -   _Axon cytoplasm_ lacks a Golgi complex but contains SER, RER, and elongated mitochondria
            -   Terminate in many small branches ([axon terminals]({{< relref "axon_terminal" >}})) from which impulses are passed to another neuron or type of cell(s)

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127)**

    [Neurons]({{< relref "neuron" >}}) consist of a cell body and its processes, which usually include multiple [dendrites]({{< relref "dendrite" >}}) and a single [axon]({{< relref "axon" >}})

    ---


#### [Dendrite]({{< relref "dendrite" >}}) {#dendrite--dendrite-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A component of [neurons]({{< relref "neuron" >}})

    ---


#### [Bipolar neuron]({{< relref "bipolar_neuron" >}}) {#bipolar-neuron--bipolar-neuron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A morphological type of [neuron]({{< relref "neuron" >}})

    ---


#### [Axon]({{< relref "axon" >}}) {#axon--axon-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A component of [neurons]({{< relref "neuron" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
