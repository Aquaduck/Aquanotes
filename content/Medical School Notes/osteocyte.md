+++
title = "Osteocyte"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Involved in [Calcium]({{< relref "calcium" >}}) homeostasis
-   Entombed in [Bone]({{< relref "bone" >}})
-   Communicate using [Canaliculi]({{< relref "canaliculi" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Parathyroid hormone]({{< relref "parathyroid_hormone" >}}) {#parathyroid-hormone--parathyroid-hormone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Acts on [osteocytes]({{< relref "osteocyte" >}})

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    A wave of [osteoblasts]({{< relref "osteoblast" >}}) follow the [osteoclasts]({{< relref "osteoclast" >}}) -> lay down concentric rings of [Osteoid]({{< relref "osteoid" >}}) -> osteoblasts differentiate into [osteocytes]({{< relref "osteocyte" >}}) -> encased in bone by the next wave of osteoblasts

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Cytologic components of mature bone**

    [Osteocytes]({{< relref "osteocyte" >}}) - emtombed in bone but highly metabolically active in [calcium]({{< relref "calcium" >}}) homeostasis

    ---


#### [Calcitonin]({{< relref "calcitonin" >}}) {#calcitonin--calcitonin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Acts on [osteocytes]({{< relref "osteocyte" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
