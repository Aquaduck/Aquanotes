+++
title = "Lymphadenopathy"
author = ["Arif Ahsan"]
date = 2021-08-23T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [HIV Infection and AIDS]({{< relref "hiv_infection_and_aids" >}}) {#hiv-infection-and-aids--hiv-infection-and-aids-dot-md}

<!--list-separator-->

-  **🔖 Practice essentials > Manifestations of HIV include the following:**

    Generalized [lymphadenopathy]({{< relref "lymphadenopathy" >}}) is common and may be a presenting symptom

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
