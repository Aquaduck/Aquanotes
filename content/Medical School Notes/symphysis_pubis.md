+++
title = "Symphysis pubis"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Joins the two halves of the [pelvis]({{< relref "pelvis" >}}) in the anterior mid-line


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons > Joints**

    Joints with limited movement consist of fibrocartilage (e.g. intervertebral disks and [symphysis pubis]({{< relref "symphysis_pubis" >}}))

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
