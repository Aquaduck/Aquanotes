+++
title = "Left ventricle"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A chamber of the [heart]({{< relref "heart" >}})
-   Ejects oxygenated [blood]({{< relref "blood" >}}) to the [aorta]({{< relref "aorta" >}}) through the [aortic valve]({{< relref "aortic_valve" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) {#ventricular-septal-defect--ventricular-septal-defect-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An abnormal communication between the [left]({{< relref "left_ventricle" >}}) and [right ventricle]({{< relref "right_ventricle" >}}) -> [left to right shunting]({{< relref "left_to_right_shunt" >}}) of blood flow

    ---


#### [Langman's Medical Embryology]({{< relref "langman_s_medical_embryology" >}}) {#langman-s-medical-embryology--langman-s-medical-embryology-dot-md}

<!--list-separator-->

-  **🔖 13: Cardiovascular System > Establishment and Patterning of the Primary Heart Field**

    From there, they migrate through the streak and into the visceral layer of lateral plate mesoderm -> some form a horseshoe-shaped cluster of cells called the [primary heart field]({{< relref "primary_heart_field" >}}) -> eventually form **portions of the atria** and **the entire [left ventricle]({{< relref "left_ventricle" >}})**

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Stroke volume]({{< relref "stroke_volume" >}}) = volume of [blood]({{< relref "blood" >}}) ejected from [left ventricle]({{< relref "left_ventricle" >}}) per heartbeat

    ---


#### [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}}) {#describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot--describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    In general, occlusion of the [anterior interventricular artery]({{< relref "anterior_interventricular_artery" >}}) -> infarction of anteroapical portion of [left ventricle]({{< relref "left_ventricle" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
