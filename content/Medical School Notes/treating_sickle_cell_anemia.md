+++
title = "Treating sickle cell anemia"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Overview {#overview}

-   [Sickle cell anemia]({{< relref "sickle_cell_disease" >}}) is an inherited disorder caused by a point mutation in the gene that encodes the beta-globin chane of hemoglobin
    -   Replaced **negatively charged glutamate** with a **neutral, hydrophobic valine** that produces **sticky patches on the protein surface**


## Treatment {#treatment}

-   Current treatment options focus largely on supportive care
-   **Even a small decrease in intracellular [HbS] is therapeutic** because of extreme sensitivity to concentration during the period before HbS fibers appear
    -   Allows more cells to escape capillaries of peripheral tissues before sickling occurs
-   A single metric seems to be primary determinent of [SCD]({{< relref "sickle_cell_disease" >}}) severity -> **time taken for red blood cells to transit through capillaries of tissues relative to the delay time for HbS polymerization**
    -   Sickling in narrow vessels can be reduced by increasing delay time or decreasing adhesion of RBCs to vascular endothelium, decreasing transit times


### Drugs {#drugs}

-   [Hydroxyurea]({{< relref "hydroxycarbaminde" >}}) should be considered standard care, but is grossly underutilized
    -   Inhibits HbS polymerization that causes sickling
-   [Voxelotor]({{< relref "voxelotor" >}})
    -   Preferentially binds to R conformation of HbS
        -   R conformation is **high-oxygen affinity and nonpolymerizing**
        -   This reduces concentration of the polymerizing T conformation at every oxygen pressure
        -   Efficacy is questioned, as modest increases in [Hb] may not be an indication of decreased anemia
            -   Increase in Hb is about the same as concentration of drug-bound, non-oxygen-delivering hemoglobin
            -   No current evidence of a decreased frequency of sickle cell crises
-   [Crizanlizumab]({{< relref "crizanlizumab" >}})
    -   [Antibody]({{< relref "immunoglobulin" >}}) that blocks the adhesion molecule P-selectin, which is expressed by RBCs


### Other treatments {#other-treatments}

-   **Bone marrow transplantation**
    -   Red blood cells are produced here
    -   Replace w/ bone marrow that contains RBC-producing stem cells w/ correct beta-globin (_HBB_) gene
    -   Proven curative in 99.5% of cases


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
