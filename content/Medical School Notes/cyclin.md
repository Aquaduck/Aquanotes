+++
title = "Cyclin"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Unstable regulatory subunits of [CDKs]({{< relref "cyclin_dependent_kinase" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop > Activation/inactivation mechanisms for CDKs > Activation of CDKs**

    CDKs are **activated** by [cyclin]({{< relref "cyclin" >}}) binding and phosphorylation of the T-loop

    ---

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop > CDKs and Cyclins**

    <!--list-separator-->

    -  [Cyclins]({{< relref "cyclin" >}})

        -   Family of unstable regulatory subunits of CDKs
        -   Synthesized and degraded at specific times during the cell cycle
        -   Function as **"cell cycle clock"**
        -   Dictate level of activity of [CDKs]({{< relref "cyclin_dependent_kinase" >}}) at specific points in the cell cycle


#### [D cyclin]({{< relref "d_cyclin" >}}) {#d-cyclin--d-cyclin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Cyclins]({{< relref "cyclin" >}}) found in G<sub>1</sub>

    ---


#### [Cyclin-CDK complex]({{< relref "cyclin_cdk_complex" >}}) {#cyclin-cdk-complex--cyclin-cdk-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A complex formed between [cyclin]({{< relref "cyclin" >}}) and [CDK]({{< relref "cdk_activating_kinase" >}}), activating enzymatic function

    ---


#### [Cyclin B]({{< relref "cyclin_b" >}}) {#cyclin-b--cyclin-b-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [cyclin]({{< relref "cyclin" >}}) involved in [mitosis]({{< relref "mitosis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
