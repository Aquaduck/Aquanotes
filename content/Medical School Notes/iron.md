+++
title = "Iron"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Methemoglobin]({{< relref "methemoglobin" >}}): [Iron]({{< relref "iron" >}}) is ferric (3+) instead of ferrous (2+)

    ---


#### [Beta globulin]({{< relref "beta_globulin" >}}) {#beta-globulin--beta-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Transport [iron]({{< relref "iron" >}}), [lipids]({{< relref "lipid" >}}), and the fat-soluble vitamins [A]({{< relref "vitamin_a" >}}), [D]({{< relref "vitamin_d" >}}), [E]({{< relref "vitamin_e" >}}), and [K]({{< relref "vitamin_k" >}}) to cells

    ---


#### [Alpha globulin]({{< relref "alpha_globulin" >}}) {#alpha-globulin--alpha-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Transport [iron]({{< relref "iron" >}}), [lipids]({{< relref "lipid" >}}), and the fat-soluble vitamins [A]({{< relref "vitamin_a" >}}), [D]({{< relref "vitamin_d" >}}), [E]({{< relref "vitamin_e" >}}), and [K]({{< relref "vitamin_k" >}}) to cells

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
