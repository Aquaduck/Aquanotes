+++
title = "DNA Repair"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [ROS and the DNA damage response in cancer]({{< relref "ros_and_the_dna_damage_response_in_cancer" >}}) {#ros-and-the-dna-damage-response-in-cancer--ros-and-the-dna-damage-response-in-cancer-dot-md}

<!--list-separator-->

-  Role of ROS in the induction of [DNA damage]({{< relref "dna_repair" >}})

    <!--list-separator-->

    -  Mediating genotoxin induced damage

        -   Ionizing radiation causes double-stranded breaks (DSB) through high energy, **but also through the generation of free radicals**
            -   Mostly \*OH from water
        -   ROS can also oxidize nucleoside bases -> G-T or G-A transversions if unrepaired
            -   Typically repaired by the Base Excision Repair pathway
                -   When attempted simultaneously on opposing strands -> DSB


#### [Khan - DNA Proofreading and Repair]({{< relref "khan_dna_proofreading_and_repair" >}}) {#khan-dna-proofreading-and-repair--khan-dna-proofreading-and-repair-dot-md}

<!--list-separator-->

-  [DNA damage repair mechanisms]({{< relref "dna_repair" >}})

    <!--list-separator-->

    -  Direct reversal

        -   Some DNA-damaging chemical reactions can be directly undone by enzymes in a cell

    <!--list-separator-->

    -  Excision repair

        <!--list-separator-->

        -  Base excision repair

            -   Detects and **removes certain types of damaged bases**
                -   A _Glycosylase_ detects and removes a specific kind of damaged base

            {{< figure src="/ox-hugo/_20210721_171334screenshot.png" >}}

        <!--list-separator-->

        -  Nucleotide excision repair

            -   Detects and corrects damage that **distorts the DNA double helix**
            -   The damaged nucleotide(s) are removed along with a surrounding patch of DNA
                -   A _helicase_ cranks open the DNA to form a bubble
                -   DNA-cutting enzymes chop out damaged part of the bubble
                -   A _[DNA polymerase]({{< relref "dna_polymerase" >}})_ replaces the missing DNA
                -   _DNA ligase_ seals the gap in the backbone of the strand

            {{< figure src="/ox-hugo/_20210721_171622screenshot.png" >}}

    <!--list-separator-->

    -  Double-stranded break repair

        -   Splitting of a chromosome into two
            -   Large segments chromosomes (and contained genes) can be lost if not repaired
        -   Two pathways exist to repair double-stranded DNA breaks

        <!--list-separator-->

        -  Non-homologous end joining

            -   Two ends of chromosome are glued back together
            -   "Messy"
                -   Involves loss or addition of a few nucleotides at cut site
                -   Better than losing an entire chromosome arm

            {{< figure src="/ox-hugo/_20210721_171807screenshot.png" width="400" >}}

        <!--list-separator-->

        -  Homologous recombination

            -   Information from homologous chromosome that matches damaged one is used to repair the break
                -   Two homologous chromosomes come together -> undamaged region of homologue is used as template to replace damaged region
            -   "Clean"
                -   Does not usually cause mutations

            {{< figure src="/ox-hugo/_20210721_171916screenshot.png" width="400" >}}

<!--list-separator-->

-  [Mismatch repair]({{< relref "dna_repair" >}})

    -   Happens **right after new DNA has been made**
    -   Fixes **mis-paired bases**

    <!--list-separator-->

    -  Process

        1.  Mismatch detected in newly synthesized DNA
        2.  New DNA strand is cut -> mispaired nucleotide + neighbors are removed
        3.  Missing patch replaced with correct nucleotides by a _[DNA polymerase]({{< relref "dna_polymerase" >}})_
        4.  _DNA ligase_ seals the gap

        {{< figure src="/ox-hugo/_20210721_170734screenshot.png" >}}

    <!--list-separator-->

    -  How does DNA repair tell which is the correct base in a base pair?

        -   In bacteria:
            -   _Methylation state_: an old DNA strand will have methyl groups attached to some of its bases, while newly made DNA does not
        -   In eukaryotes:
            -   Recognizes nicks (single-stranded breaks) found only in newly synthesized DNA

<!--list-separator-->

-  [Proofreading]({{< relref "dna_repair" >}})

    -   Done by _[DNA polymerases]({{< relref "dna_polymerase" >}})_ during replication

    {{< figure src="/ox-hugo/_20210721_170438screenshot.png" >}}


#### [Kaplan Biochemistry Chapter 6]({{< relref "kaplan_biochemistry_chapter_6" >}}) {#kaplan-biochemistry-chapter-6--kaplan-biochemistry-chapter-6-dot-md}

<!--list-separator-->

-  6.4 [DNA Repair]({{< relref "dna_repair" >}})

    <!--list-separator-->

    -  Thymine Dimers

        -   **Ultraviolet light** induces the formation of **dimers between adjacent thymine residues in DNA**
            -   Called _thymine dimers_
            -   Interferes with DNA replication and normal gene expression
            -   Distorts the shape of the double helix
            -   Eliminated by [nucleotide excision repair]({{< relref "khan_dna_proofreading_and_repair" >}})

        {{< figure src="/ox-hugo/_20210721_175634screenshot.png" caption="Figure 1: Thymine dimer formation and nucleotide excision repair" >}}

    <!--list-separator-->

    -  Thermal energy

        -   Thermal energy absorbed by DNA -> _Cytosine deamination_
            -   Loss of an amino group from cytosine -> converted into **uracil**


### Unlinked references {#unlinked-references}

[Show unlinked references]
