+++
title = "Glycogen synthase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Activated by:
    -   [G6P]({{< relref "glucose_6_phosphate" >}})
    -   [Insulin]({{< relref "insulin" >}})
    -   [Cortisol]({{< relref "cortisol" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    This is because [PKA]({{< relref "protein_kinase_a" >}}) phosphorylates and activates a key glycogen-degrading enzyme ([glycogen phosphorylase kinase]({{< relref "glycogen_phosphorylase_kinase" >}})), and also phosphorylates and inactivates the rate-limiting enzyme in glycogen synthesis ([glycogen synthase]({{< relref "glycogen_synthase" >}}))

    ---


#### [Lightyear: Infinity notes]({{< relref "lightyear_infinity_notes" >}}) {#lightyear-infinity-notes--lightyear-infinity-notes-dot-md}

<!--list-separator-->

-  **🔖 Biochemistry**

    <!--list-separator-->

    -  The three activators of [Glycogen synthase]({{< relref "glycogen_synthase" >}})

        -   G6P
        -   Insulin
        -   Cortisol


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Metabolism**

    [G6P]({{< relref "glucose_6_phosphate" >}}) binds to [glycogen synthase]({{< relref "glycogen_synthase" >}}) B -> better substrate for [Phosphoprotein phosphatase]({{< relref "phosphoprotein_phosphatase" >}}) -> activated glycogen synthase A

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
