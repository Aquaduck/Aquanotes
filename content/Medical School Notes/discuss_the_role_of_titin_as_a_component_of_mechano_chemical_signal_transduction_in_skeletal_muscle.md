+++
title = "Discuss the role of titin as a component of mechano-chemical signal transduction in skeletal muscle."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### [Titin]({{< relref "titin" >}}) {#titin--titin-dot-md}

-   An enormous (ca 3.5MDa) protein located in the **contractile unit of skeletal and cardiac muscle** - the _sarcomere_
-   Spring-like molecule
-   Ideally positioned to sense mechanical load
-   **Has ser/thr kinase within its structure** - [Titin kinase]({{< relref "titin_kinase" >}})
-   **Main takeaway**: titin and its protein kinase is critical to mechano-chemical signaling in striated muscle


#### Mechanism {#mechanism}

-   Mechanical load triggers Titin Kinase -> signal cascade activation
    -   Protein degradation
    -   Ubiquitin pathway
    -   Cell signaling in the nucleus through [Serum Response Factor]({{< relref "serum_response_factor" >}}) (SRF)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Discuss the role of titin as a component of mechano-chemical signal transduction in skeletal muscle.]({{< relref "discuss_the_role_of_titin_as_a_component_of_mechano_chemical_signal_transduction_in_skeletal_muscle" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
