+++
title = "Prelamin A"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Precursor to mature [lamin A]({{< relref "lamin_a" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}}) {#rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot--rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot-md}

<!--list-separator-->

-  **🔖 From Protein farnesylation and disease > Nuclear lamins > A-type lamins**

    [Prelamin A]({{< relref "prelamin_a" >}}) is the precursor protein of mature lamin A and possesses a COOH terminal CAAX motif -> the site of post-translational modifications

    ---


#### [Progerin]({{< relref "progerin" >}}) {#progerin--progerin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Farnesylated mutant [prelamin A]({{< relref "prelamin_a" >}}) found in [HGPS]({{< relref "hutchinson_guildford_progeria" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
