+++
title = "Proliferating cell nuclear antigen"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Steps in replication**

    A sliding clamp protein known as [PCNA]({{< relref "proliferating_cell_nuclear_antigen" >}}) (proliferating cell nuclear antigen) holds the DNA polymerase in place so that it does not slide off DNA

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
