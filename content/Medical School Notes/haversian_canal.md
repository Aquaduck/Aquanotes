+++
title = "Haversian canal"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   In the center of the [Haversian system]({{< relref "osteon" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Appositional growth and remodeling**

    A broken bone is initially fused together with a fibrous (collagenous) scar that calcifies ([Woven bone]({{< relref "woven_bone" >}})) and then undergoes remodeling by [Haversian canal]({{< relref "haversian_canal" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    The result of the [Bone-remodeling unit]({{< relref "bone_remodeling_unit" >}}) is known as an [Osteon]({{< relref "osteon" >}}) (haversian system) with a central [Haversian canal]({{< relref "haversian_canal" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
