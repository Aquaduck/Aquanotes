+++
title = "Carbon Dioxide"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Respiratory Acidosis: Primary Carbonic Acid/Carbon Dioxide Excess**

    Occurs when the [blood]({{< relref "blood" >}}) is overly acidic due to an excess of [carbonic acid]({{< relref "carbonic_acid" >}}) <- excess [carbon dioxide]({{< relref "carbon_dioxide" >}}) in the blood

    ---


#### [Haldane effect]({{< relref "haldane_effect" >}}) {#haldane-effect--haldane-effect-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The [carbon dioxide]({{< relref "carbon_dioxide" >}}) affinity of [Hemoglobin]({{< relref "hemoglobin" >}}) is **inversely proportional** to the [oxygenation]({{< relref "oxygen" >}}) of hemoglobin

    ---


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Changes in Ventilation (p. 664)**

    When exercise becomes more vigorous -> increased [lactic acid]({{< relref "lactic_acid" >}}) production -> liberation of more [carbon dioxide]({{< relref "carbon_dioxide" >}}) (CO<sub>2</sub>) -> increase in ventilation

    ---


#### [Factors Affecting Hemoglobin Dissociation Curve]({{< relref "factors_affecting_hemoglobin_dissociation_curve" >}}) {#factors-affecting-hemoglobin-dissociation-curve--factors-affecting-hemoglobin-dissociation-curve-dot-md}

<!--list-separator-->

-  **🔖 Notes > Factors affecting O<sub>2</sub>-Hb Dissociation Curve:**

    <!--list-separator-->

    -  [Carbon Dioxide]({{< relref "carbon_dioxide" >}})

        -   Metabolically active cells of our tissue produce more CO<sub>2</sub> as a waste byproduct
            -   Some of the CO<sub>2</sub> binds directly to hemoglobin -> **decreased affinity to oxygen**
        -   **A higher ppCO<sub>2</sub> (i.e. [CO<sub>2</sub>]) in the blood -> more O<sub>2</sub> delivered to tissues**
            -   The _Haldane Effect_


### Unlinked references {#unlinked-references}

[Show unlinked references]
