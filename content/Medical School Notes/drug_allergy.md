+++
title = "Drug allergy"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name one drug in which severe adverse effects may depend on the presence of a specific genetic variation]({{< relref "name_one_drug_in_which_severe_adverse_effects_may_depend_on_the_presence_of_a_specific_genetic_variation" >}}) {#name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation--name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation-dot-md}

<!--list-separator-->

-  **🔖 From Pharmacogenetics Pre-learning Material**

    <!--list-separator-->

    -  [Drug allergy]({{< relref "drug_allergy" >}})

        -   Immune hypersensitivity reactions mediated by [immunoglobulin E]({{< relref "immunoglobulin_e" >}}) and driven by [mast cells]({{< relref "mast_cell" >}})
        -   **Not pharmacogenomic effects** because specific genetic variations in the patient's germline DNA are not predictive of the response
            -   Depend on prior immune sensitizing exposure


### Unlinked references {#unlinked-references}

[Show unlinked references]
