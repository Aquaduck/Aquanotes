+++
title = "Procaspase-9"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The zymogen precursor to [Caspase-9]({{< relref "caspase_9" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Smac, a Mitochondrial Protein that Promotes Cytochrome c–Dependent Caspase Activation by Eliminating IAP Inhibition]({{< relref "smac_a_mitochondrial_protein_that_promotes_cytochrome_c_dependent_caspase_activation_by_eliminating_iap_inhibition" >}}) {#smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition--smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition-dot-md}

<!--list-separator-->

-  **🔖 Discussion**

    <!--list-separator-->

    -  [SMAC]({{< relref "second_mitochondria_derived_activator_of_caspases" >}}) promotes [procaspase-9]({{< relref "procaspase_9" >}}) activation by countering IAPs

        -   SMAC is released from mitochondria during apoptosis -> neutralizes the inhibitory activity of [IAPs]({{< relref "inhibitor_of_apoptosis" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
