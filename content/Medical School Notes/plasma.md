+++
title = "Plasma"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The non-cellular component of [Blood]({{< relref "blood" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Kidney]({{< relref "kidney" >}}) {#kidney--kidney-dot-md}

<!--list-separator-->

-  **🔖 Concept links > Major functions**

    Regulation of [plasma]({{< relref "plasma" >}}) [osmolality]({{< relref "osmolality" >}})

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Renal functions (p. 10)**

    <!--list-separator-->

    -  Regulation of [plasma]({{< relref "plasma" >}}) osmolality


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Glomerular filtration (p. 33)**

    **Almost all [plasma]({{< relref "plasma" >}}) proteins bear net negative charge -> important restrictive role**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
