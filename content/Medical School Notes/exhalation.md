+++
title = "Exhalation"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Part of [Ventilation]({{< relref "ventilation" >}}) that expels air from the lungs


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Residual volume]({{< relref "residual_volume" >}}) {#residual-volume--residual-volume-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Volume of air that remains in the [lungs]({{< relref "lung" >}}) after a maximal [exhalation]({{< relref "exhalation" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
