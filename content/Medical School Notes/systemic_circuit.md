+++
title = "Systemic circuit"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The part of circulation that travels throughout the body


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Right-to-left shunt]({{< relref "right_to_left_shunt" >}}) {#right-to-left-shunt--right-to-left-shunt-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [shunts]({{< relref "shunt" >}}) where [blood]({{< relref "blood" >}}) fails to be oxygenated -> deoxygenated blood enters the [systemic circuit]({{< relref "systemic_circuit" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
