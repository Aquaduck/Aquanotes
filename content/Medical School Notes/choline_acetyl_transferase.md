+++
title = "Choline acetyl transferase"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the process of neuromuscular transmission.]({{< relref "describe_the_process_of_neuromuscular_transmission" >}}) {#describe-the-process-of-neuromuscular-transmission-dot--describe-the-process-of-neuromuscular-transmission-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Conduction of a nerve impulse across a myoneural junction (p. 116)**

    Choline returns to the axon terminal to be recombined with acetyl-CoA (from mitochondria) via [choline acetyl transferase]({{< relref "choline_acetyl_transferase" >}}) to regenerate acetylcholine -> stored in synaptic vesicles at synaptic bouton

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
