+++
title = "Carnitine palmitoyl acyltransferase 1"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    This is required to prevent formation of [malonyl-CoA]({{< relref "malonyl_coa" >}}) (inhibits entry of long-chain fatty acids into mitochondria by blocking [CPT1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}), the first enzyme in the carnitine shuttle)

    ---


#### [Malonyl-CoA]({{< relref "malonyl_coa" >}}) {#malonyl-coa--malonyl-coa-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inhibits [CPT1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) -> prevents synthesized fatty acids (>C12) from entering the [Mitochondria]({{< relref "mitochondrion" >}}) and being oxidized

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Carnitine acyltransferase I (CAT-1) and CAT-II > From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  [CAT-1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}})

        -   [Fatty acyl-carnitine ester]({{< relref "fatty_acyl_carnitine_ester" >}}) is formed by the action of _carnitine acyltransferase I_ once inside the inner membrane space


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism**

    <!--list-separator-->

    -  [CPT1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) and [CPT2]({{< relref "carnitine_palmitoyl_acyltransferase_2" >}})

        -   **Shuttle fatty acids (>12C) into mitochondrial matrix**
        -   Use of [carnitine]({{< relref "carnitine" >}}) in this shuttle ensures that the mitochondrial and cytosolic coenzyme A pools remain intact
        -   **CPT 1 is blocked by malonyl-CoA** -> prevents fatty acids that are being synthesized in cytoplasm from entering the mitochondria for oxidation

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Acetyl-CoA carboxylase**

    [Malonyl-CoA]({{< relref "malonyl_coa" >}}) binds to and inhibits [carnitine palmitoyl acyltransferase 1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) (CPT1) -> prevents synthesized fatty acids from entering mitochondria and being oxidized

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
