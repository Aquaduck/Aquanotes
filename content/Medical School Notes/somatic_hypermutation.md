+++
title = "Somatic hypermutation"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Germinal center]({{< relref "germinal_center" >}}) {#germinal-center--germinal-center-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Where [helper T cells]({{< relref "cd4_positive_t_lymphocyte" >}}) cause [B-cells]({{< relref "b_lymphocyte" >}}) to go through [somatic hypermutation]({{< relref "somatic_hypermutation" >}}) -> create more of a specific [antibody]({{< relref "immunoglobulin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
