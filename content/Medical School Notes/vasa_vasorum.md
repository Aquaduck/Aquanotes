+++
title = "Vasa vasorum"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Structure and Function of Blood Vessels (p. 898) > Shared Structures (p. 899)**

    <!--list-separator-->

    -  [Vasa vasorum]({{< relref "vasa_vasorum" >}}) (p. 900)

        -   Literally "vessels of the vessel"
        -   Small blood vessels in larger arteries and veins to supply themselves with blood
        -   Since pressure within arteries is relatively high, the vasa vasorum must function in the **outer layers of this vessel**
            -   Otherwise, the pressure exerted by passing blood would collapse the vessels
            -   Restriction of vasa vasorum to outer layers of arteries is thought to be one reason that arterial diseases are more common than venous diseases
        -   Lower pressure in veins -> vasa vasorum is located closer to lumin


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Thinner [tunica adventitia]({{< relref "tunica_adventitia" >}}) that also contains collagen, elastic fibers, and [vasa vasorum]({{< relref "vasa_vasorum" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
