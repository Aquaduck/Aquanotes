+++
title = "Energy, Redox Reactions, and Enzymes"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

Link: <https://ecampusontario.pressbooks.pub/microbio/chapter/energy-matter-and-enzymes/#chapter-206-section-6>


## Oxidation and Reduction in Metabolism {#oxidation-and-reduction-in-metabolism}

-   Transfer of energy in the form of electrons allows the cell to transfer and use energy incrementally

{{< figure src="/ox-hugo/_20210715_121449screenshot.png" width="600" >}}

-   _Oxidation reactions_: reactions that remove electrons from donor molecules, leaving them **oxidized**
-   _Reduction reactions_: reactions that add electrons to acceptor molecules, leaving them **reduced**


## Energy Carriers {#energy-carriers}

-   **Energy** released from breakdown of chemical bonds within nutrients **can be stored** either **through the reduction of electron carriers** or **in the bonds of ATP**
-   _Energy carrier_: a molecule that can bind to and shuttle high-energy electrons between compounds


### NAD(H) {#nad--h}

-   _Nicotinamide adenine dinucleotide_ (NAD<sup>+</sup>/NADH) is the most common mobile electron carrier used in catabolism
-   NADP<sup>+</sup>/NADPH is a variant which contains an extra phosphate group, used in anabolic reactions and photosynthesis
-   NAD<sup>+</sup> accepts a **hydride ion** (2 electrons and 1 proton) to be reduced

{{< figure src="/ox-hugo/_20210715_122151screenshot.png" width="400" >}}


### FAD(H) {#fad--h}

-   _Flavin adenine dinucleotide_ (FAD/FADH<sub>2</sub>) is extensively used in energy extraction from sugars during catabolism in chemoheterotrophs


### [ATP]({{< relref "atp" >}}) {#atp--atp-dot-md}

-   _Adenosine triphosphate_
-   Stores energy in bonds with phosphate groups
    -   Phosphate groups are negatively charged -> repel each other -> **makes ATP molecules more unstable and higher in energy**
    -   Release energy by breaking phosphate bonds, converting it to ADP or AMP if releasing pyrophosphate (PP<sub>i</sub>)

{{< figure src="/ox-hugo/_20210715_122440screenshot.png" width="600" >}}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Cellular Metabolism > 3. Describe why ATP, FAD, NAD+, and NADP+ are important in a cell**

    [Energy Carriers]({{< relref "energy_redox_reactions_and_enzymes" >}})

    ---

<!--list-separator-->

-  **🔖 Cellular Metabolism > 2. Describe the importance of oxidation-reduction reactions in metabolism**

    [Oxidation and Reduction in Metabolism]({{< relref "energy_redox_reactions_and_enzymes" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
