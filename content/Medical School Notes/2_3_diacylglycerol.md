+++
title = "2,3-Diacylglycerol"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A form of [Diacylglycerol]({{< relref "diacylglycerol" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Mobilization of triacylglycerol stored in adipocytes**

    Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**

    ---


#### [Explain how fatty acids released from adipocyte stores are utilized.]({{< relref "explain_how_fatty_acids_released_from_adipocyte_stores_are_utilized" >}}) {#explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot--explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > How fatty acids released from adipocyte stores are utilized**

    Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**

    ---


#### [Describe the role of protein kinase A in mobilizing fat from adipocyte stores.]({{< relref "describe_the_role_of_protein_kinase_a_in_mobilizing_fat_from_adipocyte_stores" >}}) {#describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot--describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > The role of PKA in mobilizing fat from adipocytes**

    Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
