+++
title = "Oligonucleotide"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Degradation of dietary nucleic acids in the small intestine**

    [Oligonucleotides]({{< relref "oligonucleotide" >}}) are further hydrolyzed by pancreatic [phosphodiesterases]({{< relref "phosphodiesterase" >}}) -> produces a mixture of 3' and 5'-mononucleotides

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Degradation of dietary nucleic acids in the small intestine**

    Hydrolyze dietary RNA and DNA primarily to [oligonucleotides]({{< relref "oligonucleotide" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
