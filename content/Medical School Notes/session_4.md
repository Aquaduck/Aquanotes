+++
title = "Session 4"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "jumpstart"]
draft = false
+++

## Application Packet 1 {#application-packet-1}

1.  C
    -   Because \\(\Delta G^{o}\\) is negative, \\(K\_{eq}\\) must be greater than 1, and therefore at equilibrium **products must be greater than reactants**
        -   Therefore it cannot be at equilibrium
2.  B
3.  B
4.  A + D
    -   Transition state = free energy of activation, so both are right
5.  D
    -   \\(Products - Reactants\\)


## Application Packet 2 {#application-packet-2}

1.  B
2.  C
3.  A
4.  A
5.  C
6.  E
7.  A


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
