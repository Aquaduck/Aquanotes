+++
title = "Vitamin"
author = ["Arif Ahsan"]
date = 2021-07-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Vitamin K]({{< relref "vitamin_k" >}}) {#vitamin-k--vitamin-k-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [Vitamin]({{< relref "vitamin" >}})

    ---


#### [Vitamin E]({{< relref "vitamin_e" >}}) {#vitamin-e--vitamin-e-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [Vitamin]({{< relref "vitamin" >}})

    ---


#### [Vitamin D]({{< relref "vitamin_d" >}}) {#vitamin-d--vitamin-d-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [Vitamin]({{< relref "vitamin" >}})

    ---


#### [Vitamin A]({{< relref "vitamin_a" >}}) {#vitamin-a--vitamin-a-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [Vitamin]({{< relref "vitamin" >}})

    ---


#### [B vitamin]({{< relref "b_vitamin" >}}) {#b-vitamin--b-vitamin-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A class of water-soluble [vitamins]({{< relref "vitamin" >}}) that play an important role in [cell metabolism]({{< relref "metabolism" >}}) and synthesis of [red blood cells]({{< relref "red_blood_cell" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
