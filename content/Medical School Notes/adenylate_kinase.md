+++
title = "Adenylate kinase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Additional important enzymes**

    <!--list-separator-->

    -  [Adenylate kinase]({{< relref "adenylate_kinase" >}})

        -   Cytoplasmic enzyme
        -   Catalyzes **phosphoryl transfer between two ADP molecules to yield ATP and AMP**
        -   **AMP:ATP ratio is the more sensitive indicator of energy charge**
            -   AMP signals that cell's energy stores are depleted


### Unlinked references {#unlinked-references}

[Show unlinked references]
