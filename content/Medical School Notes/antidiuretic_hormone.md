+++
title = "Antidiuretic hormone"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Secreted by the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) in response to [osmoreceptors]({{< relref "osmoreceptor" >}}) in the [Hypothalamus]({{< relref "hypothalamus" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Osmoreceptor]({{< relref "osmoreceptor" >}}) {#osmoreceptor--osmoreceptor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Signal the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) to release [ADH]({{< relref "antidiuretic_hormone" >}}) in response to high blood osmolarity

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  [Antidiuretic Hormone]({{< relref "antidiuretic_hormone" >}}) (ADH) (p.746)

    -   Blood osmolarity is constantly monitored by [osmoreceptors]({{< relref "osmoreceptor" >}}) - specialized cells within the hypothalamus that are particularly sensitive to the concentration of sodium ions and other solutes
    -   In response to **high blood osmolarity** -> [osmoreceptors]({{< relref "osmoreceptor" >}}) signal the [posterior pituitary]({{< relref "posterior_pituitary" >}}) to release ADH
    -   The target cells of ADH are located in the [tubular cells]({{< relref "tubular_cell" >}}) of the [kidney]({{< relref "kidney" >}})
    -   The **effect of ADH** is to **increase epithelial permeability to water** -> allows **increased water absorption**
    -   In very high concentrations -> constricts blood vessels -> increase in blood pressure via increase of peripheral resistance
    -   The release of ADH is controlled by a negative feedback loop
        -   As blood osmolarity decreases -> hypothalamic [osmoreceptors]({{< relref "osmoreceptor" >}}) sense change -> decrease secretion of ADH -> less water reabsorbed from urine


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Angiotensin (I + II)**

    Angiotensin II stimulates the release of [ADH]({{< relref "antidiuretic_hormone" >}}) from the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) and [aldosterone]({{< relref "aldosterone" >}}) from the [adrenal cortex]({{< relref "adrenal_cortex" >}})

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    E.g. [Sodium]({{< relref "sodium" >}}), [Potassium]({{< relref "potassium" >}}), [Chloride]({{< relref "chloride" >}}), [Bicarbonate]({{< relref "bicarbonate" >}}), [Glucose]({{< relref "glucose" >}}), [Urea]({{< relref "urea" >}}), [amino acids]({{< relref "amino_acid" >}}), peptides such as [Insulin]({{< relref "insulin" >}}) and [ADH]({{< relref "antidiuretic_hormone" >}})

    ---


#### [Atrial natriuretic hormone]({{< relref "atrial_natriuretic_hormone" >}}) {#atrial-natriuretic-hormone--atrial-natriuretic-hormone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Suppresses [Renin]({{< relref "renin" >}}), [Aldosterone]({{< relref "aldosterone" >}}), and [ADH]({{< relref "antidiuretic_hormone" >}}) production/release

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
