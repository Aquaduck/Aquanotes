+++
title = "Voxelotor"
author = ["Arif Ahsan"]
date = 2021-08-17T00:00:00-04:00
tags = ["medschool", "concept", "drug"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Treating sickle cell anemia]({{< relref "treating_sickle_cell_anemia" >}}) {#treating-sickle-cell-anemia--treating-sickle-cell-anemia-dot-md}

<!--list-separator-->

-  **🔖 Treatment > Drugs**

    [Voxelotor]({{< relref "voxelotor" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
