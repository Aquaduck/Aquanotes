+++
title = "Radial glial cell"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## From Amboss {#from-amboss}

-   Embryonic glial cells that play an important role in the production and migration of neurons during brain development. Primarily located in the periventricular zones.


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
