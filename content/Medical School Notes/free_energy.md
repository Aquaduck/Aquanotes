+++
title = "Free Energy"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   Aka \\(\Delta G\\)


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 General Chemistry > 5. Describe the meaning of a positive value, a negative value, and a value of zero for \\(\Delta G\\)**

    [Free Energy]({{< relref "free_energy" >}})

    ---

<!--list-separator-->

-  **🔖 Cellular Metabolism > 6. Define and explain Gibbs free energy (\\(\Delta G\\)), standard free energy (\\(\Delta G^{o}\\)), and the equilibrium constant (K<sub>eq</sub>) as it applies to biochemical functions**

    [Free Energy]({{< relref "free_energy" >}})

    ---


#### [Bioenergetics and Oxidative Phosphorylation]({{< relref "bioenergetics_and_oxidative_phosphorylation" >}}) {#bioenergetics-and-oxidative-phosphorylation--bioenergetics-and-oxidative-phosphorylation-dot-md}

<!--list-separator-->

-  Standard [free energy]({{< relref "free_energy" >}}) change

    -   Equal to free energy change under standard conditions (i.e. concentrations are 1 mol/L)
        -   This is because \\(ln(1) = 0\\), and therefore, above equation becomes \\(\Delta G = \Delta G^{o} + 0\\)

    <!--list-separator-->

    -  Relationship between \\(\Delta G^{o} and K\_{eq}\\)

        -   \\(K\_{eq} = \frac{[B]\_{eq}}{[A]\_{eq}}\\)
            -   K<sub>eq</sub> = equilibrium constant
            -   [A]<sub>eq</sub> and [B]<sub>eq</sub> are concentrations at equilibrium
        -   If reaction \\(A \rightleftharpoons B\\) is allowed to go to equilibrium at **constant temperature and pressure**:
            -   \\(\Delta G = 0 = \Delta G^{o} + RT ln\frac{[B]\_{eq}}{[A]\_{eq}}\\)
            -   Where [A] and [B] = [A]<sub>eq</sub> and [B]<sub>eq</sub>
            -   Therefore, \\(\Delta G^{o} = -RTlnK\_{eq}\\) which allows some simple predictions:
                -   **If \\(K\_{eq} = 1\\)**, then \\(\Delta G^{o} = 0\\)
                -   **If \\(K\_{eq} > 1\\)**, then \\(\Delta G^{o} < 0\\) -> reaction **proceeds forward**
                -   **If \\(K\_{eq} < 1\\)**, then \\(\Delta G^{o} > 0\\) -> reaction **favors reverse**

<!--list-separator-->

-  [Free Energy]({{< relref "free_energy" >}}) Change

    -   Represented by either \\(\Delta G\\) or \\(\Delta G^{o}\\)
        -   \\(\Delta G\\) represents change in free energy = variable
        -   \\(\Delta G^{o}\\) represents **standard** change in free energy = when reactants and products have concentration of 1 mol/L

    <!--list-separator-->

    -  Sign of \\(\Delta G\\) and direction of a reaction

        1.  **Negative \\(\Delta G\\)**: net loss of energy -> **spontaneous**
        2.  **Positive \\(\Delta G\\)**: net gain of energy -> **not spontaneous**
        3.  **\\(\Delta G = 0\\)**: no net change in energy -> **equilibrium**

    <!--list-separator-->

    -  \\(\Delta G\\) of forward and back reactions

        -   **Equal in magnitude** but **opposite in sign**

    <!--list-separator-->

    -  \\(\Delta G\\) and concentration of reactants and products

        -   At **constant temperature and pressure**: \\(\Delta G = \Delta G^{o} + RTln\frac{[B]}{[A]}\\)
            -   R is gas constant (1.987 cal/mol K)
            -   T is absolute temperature (K)
            -   [A] and [B] are actual concentrations of reactant and product
            -   ln is natural log
        -   A reaction with a positive \\(\Delta G^{o}\\) can proceed in the forward direction if the ratio of products to reactants is small enough

<!--list-separator-->

-  [Free Energy]({{< relref "free_energy" >}})

    -   The direction and extent a chemical reaction proceeds is determined by \\(\Delta H\\) (change in enthalpy) and \\(\Delta S\\) (change in entropy)
        -   _Enthalpy_: Change in heat content of reactants + products
        -   _Entropy_: Change in randomness/disorder of reactants + products


### Unlinked references {#unlinked-references}

[Show unlinked references]
