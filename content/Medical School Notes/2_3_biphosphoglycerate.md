+++
title = "2,3-Biphosphoglycerate"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Oxygen Dissociation from Hemoglobin (p. 1074)**

    Certain hormones can affect the oxygen-hemoglobin saturation/dissociation curve by **stimulating the production of [2,3-BPG]({{< relref "2_3_biphosphoglycerate" >}})** e.g.:

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Metabolism**

    [DPG]({{< relref "2_3_biphosphoglycerate" >}}) increases at higher altitudes to unload more oxygen to tissues

    ---


#### [Factors Affecting Hemoglobin Dissociation Curve]({{< relref "factors_affecting_hemoglobin_dissociation_curve" >}}) {#factors-affecting-hemoglobin-dissociation-curve--factors-affecting-hemoglobin-dissociation-curve-dot-md}

<!--list-separator-->

-  **🔖 Notes > Factors affecting O<sub>2</sub>-Hb Dissociation Curve:**

    <!--list-separator-->

    -  [2,3-BPG]({{< relref "2_3_biphosphoglycerate" >}})

        -   Naturally occurring intermediate in glycolysis
        -   High rate of metabolism -> **produce excess 2,3-BPG**
            -   Some of it escapes into blood plasma -> **binds to hemoglobin and lowers its affinity for oxygen**
                -   More O<sub>2</sub> for tissues
        -   ↑[2,3-BPG] -> shifts O<sub>2</sub>-Hb curve **to the right**


### Unlinked references {#unlinked-references}

[Show unlinked references]
