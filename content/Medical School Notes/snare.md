+++
title = "SNARE"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Ensure vesicle docking at the correct target


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [v-SNARE]({{< relref "v_snare" >}}) {#v-snare--v-snare-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [SNARE]({{< relref "snare" >}})

    ---


#### [t-SNARE]({{< relref "t_snare" >}}) {#t-snare--t-snare-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [SNARE]({{< relref "snare" >}})

    ---


#### [Describe how vesicles move, recognize, and fuse with the target compartment.]({{< relref "describe_how_vesicles_move_recognize_and_fuse_with_the_target_compartment" >}}) {#describe-how-vesicles-move-recognize-and-fuse-with-the-target-compartment-dot--describe-how-vesicles-move-recognize-and-fuse-with-the-target-compartment-dot-md}

<!--list-separator-->

-  **🔖 From Fusion of Cells by Flipped SNARES**

    Fusion of intracellular membranes is mediated by [SNARE]({{< relref "snare" >}}) proteins that assemble between lipid bilayers as SNAREpins

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Coated vesicles > Clathrin-coated vesicles > Function**

    _[SNARE]({{< relref "snare" >}})_: proteins that ensure vesicles dock and fuse only with its correct target membrane

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
