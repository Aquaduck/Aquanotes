+++
title = "Session 13"
author = ["Arif Ahsan"]
date = 2021-07-28T00:00:00-04:00
tags = ["medschool", "jumpstart"]
draft = false
+++

## gRAT {#grat}

1.  A
2.  A
3.  D
4.  A
5.  D
6.  B
7.  E
8.  C
9.  C
10. E


## Worksheet {#worksheet}

1.  NADH - Complex I
    FADH<sub>2</sub> - Complex II
    Succinate Dehydrogenase can only reduce FADH<sub>2</sub> and is used to make FAD w/in CII
2.  Lower for FADH<sub>2</sub> because skipping Complex I
3.  Uncouplers and the different shuttles (malate-aspartate vs. glycerol-3-phosphate)
4.  Coenzyme Q, Cytochrome c
5.  Complex I - 4H<sup>+</sup>
    Complex II - 0
    Complex III - 4H<sup>+​</sup>
    Complex IV - 2H<sup>+</sup>
    Pumped out of **matrix** into **innermembrane space**
6.  Energy created by electrons gradient
7.  True/False
    1.  T
    2.  T
    3.  T
    4.  T
8.  Why do we need 6 moles of O<sub>2</sub>? \\(\frac{1}{2}\\)O<sub>2</sub> per NADH


## Application Questions {#application-questions}

1.  B
2.  E
3.  C
4.  B
5.  A
6.  E


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
