+++
title = "Intermediary Metabolism and its Regulation"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Summary of major regulators of intermediary metabolism {#summary-of-major-regulators-of-intermediary-metabolism}

<div class="table-caption">
  <span class="table-number">Table 1</span>:
  The common mechanisms by which enzyme activity is regulated
</div>

| Regulator event                    | Typical effector      | Results                                          | Time required for change |
|------------------------------------|-----------------------|--------------------------------------------------|--------------------------|
| Substrate availability             | Substrate             | Change in v<sub>o</sub>                          | Immediate                |
| Product inhibition                 | Reaction product      | Change in V<sub>max</sub> and/or K<sub>m</sub>   | Immediate                |
| Allosteric control                 | Pathway end product   | Change in V<sub>max</sub> and/or K<sub>0.5</sub> | Immediate                |
| Covalent modification              | Another enzyme        | Change in V<sub>max</sub> and/or K<sub>m</sub>   | Immediate to minutes     |
| Synthesis or degradation of enzyme | Hormone or metabolite | Change in amount of enzyme                       | Hours to days            |

Note: First two mechanisms detailed in JumpStart, Allosteric control covered in [Kinetic and Allosteric Regulation of Glucose Regulation]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}})


## Covalent modification {#covalent-modification}

-   Most commonly the addition or removal of phosphate groups from specific **serine, threonine, or tyrosine** residues of the enzyme
    -   **Phosphorylation is one of the primary ways in which cellular processes (e.g. metabolism) are up- or down-regulated**
    -   Catalyzed by [Protein kinases]({{< relref "protein_kinase" >}})
        -   One family referred to as Ser/Thr kinases based on substrate used
        -   Other family referred to as Tyr kinases
    -   Phosphate groups are cleaved from phosphorylated enzymes by [phosphoprotein phosphatases]({{< relref "phosphoprotein_phosphatase" >}})


## Enzyme synthesis {#enzyme-synthesis}

-   Cells can regulate the amount of enzyme present by **altering the rate of enzyme degradation or enzyme synthesis**
    -   Enzyme synthesis regulation is more common
-   _Induction_: ↑ Enzyme synthesis
-   _Repression_: ↓ Enzyme synthesis
-   Enzymes subject to regulation by protein synthesis are **often needed at only one stage of development** or **under selected physiologic conditions**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}}) {#list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated--list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated-dot-md}

From [Intermediary Metabolism and its Regulation]({{< relref "intermediary_metabolism_and_its_regulation" >}})

---


### Unlinked references {#unlinked-references}

[Show unlinked references]
