+++
title = "Explain why adipose tissue is referred to as a fat depot."
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   The LPL isozyme of adipose tissue has the **largest K<sub>m</sub> -> [adipose tissue's]({{< relref "adipose_cell" >}}) uptake of [dietary fat]({{< relref "dietary_lipid" >}}) is a last resort**
    -   **Adipose tissue stores fat** -> least likely to utilize it
    -   **LPL is not expressed by the [liver]({{< relref "liver" >}}) or the [brain]({{< relref "brain" >}})** -> NO dietary fat reaches either of those tissues


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-25 Mon] </span></span> > Muscle Intermediary Metabolism II > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Explain why adipose tissue is referred to as a fat depot.]({{< relref "explain_why_adipose_tissue_is_referred_to_as_a_fat_depot" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
