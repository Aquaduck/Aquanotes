+++
title = "Guanine"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    [Guanine]({{< relref "guanine" >}}) is deaminated to form [xanthine]({{< relref "xanthine" >}})

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    [Purine nucleoside phosphorylase]({{< relref "purine_nucleoside_phosphorylase" >}}) converts [Inosine]({{< relref "inosine" >}}) and [guanosine]({{< relref "guanosine" >}}) into their respective purine bases, [hypoxanthine]({{< relref "hypoxanthine" >}}) and [guanine]({{< relref "guanine" >}})

    ---


#### [Guanosine]({{< relref "guanosine" >}}) {#guanosine--guanosine-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Nucleoside]({{< relref "nucleoside" >}}) formed by [guanine]({{< relref "guanine" >}}) bound with [ribose]({{< relref "ribose" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
