+++
title = "Discuss the mechanisms by which phosphorylation is involved in turning off the cAMP signal pathway."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

[Inactivate the receptor]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Discuss the mechanisms by which phosphorylation is involved in turning off the cAMP signal pathway.]({{< relref "discuss_the_mechanisms_by_which_phosphorylation_is_involved_in_turning_off_the_camp_signal_pathway" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
