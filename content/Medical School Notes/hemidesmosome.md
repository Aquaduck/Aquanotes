+++
title = "Hemidesmosome"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Adheres cells of the [epidermis]({{< relref "epidermis" >}}) to the [basement membrane]({{< relref "basement_membrane" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}}) {#know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot--know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Overview of Basic Skin > 3 layers > Epidermis > Components**

    Bottom layer of keratinocytes attached by [hemidesmosomes]({{< relref "hemidesmosome" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
