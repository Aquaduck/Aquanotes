+++
title = "List those tissues that respond to insulin vs. glucagon, and epinephrine"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## Insulin {#insulin}

-   Liver
-   Muscle
-   Adipocytes


## Glucagon {#glucagon}

-   Liver


## Epinephrine {#epinephrine}

-   Heart
-   Lung blood vessels
-   Muscles


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [List those tissues that respond to insulin vs. glucagon, and epinephrine]({{< relref "list_those_tissues_that_respond_to_insulin_vs_glucagon_and_epinephrine" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
