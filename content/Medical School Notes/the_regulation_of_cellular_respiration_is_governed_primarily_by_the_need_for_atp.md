+++
title = "The Regulation of Cellular Respiration Is Governed Primarily by the Need for ATP"
author = ["Arif Ahsan"]
date = 2021-07-28T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

[Link](https://www.ncbi.nlm.nih.gov/books/NBK22448/)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Cellular Metabolism > 12. Describe oxidative phosphorylation > Describe what is meant by the concept of "respiratory control"**

    [The Regulation of Cellular Respiration Is Governed Primarily by the Need for ATP]({{< relref "the_regulation_of_cellular_respiration_is_governed_primarily_by_the_need_for_atp" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
