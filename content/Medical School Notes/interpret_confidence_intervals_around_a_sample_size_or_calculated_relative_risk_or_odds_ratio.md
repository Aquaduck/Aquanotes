+++
title = "Interpret confidence intervals around a sample size or calculated relative risk or odds ratio"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}


### [Confidence interval]({{< relref "confidence_interval" >}}) {#confidence-interval--confidence-interval-dot-md}

-   **The range of values that are highly likely to contain the true sample measurement**
-   Provide a way to determine a [Population]({{< relref "population" >}}) measurement or a value that is subject to change from a [Sample]({{< relref "sample" >}}) measurement
-   [Z scores]({{< relref "z_score" >}}) for confidence intervals for normally distributed data:
    -   Z score for 95% confidence interval = 1.96
    -   Z score for a 97.5% confidence interval = 2.24
    -   Z score for a 99% confidence interval = 2.58
-   **Formula**: any sample measurement +/- Z score knowing:
    -   Confidence level (usually fixed at 95%)
    -   Sample measurement
    -   [Standard error of the mean]({{< relref "standard_error_of_the_mean" >}}), which needs:
        -   Sample size - larger sample size -> lower standard error -> more narrow confidence intervals
        -   [Standard deviation]({{< relref "standard_deviation" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > The Practice of Evidence-Based Medicine: Inferential Statistics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Interpret confidence intervals around a sample size or calculated relative risk or odds ratio]({{< relref "interpret_confidence_intervals_around_a_sample_size_or_calculated_relative_risk_or_odds_ratio" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
