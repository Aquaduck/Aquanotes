+++
title = "Smooth endoplasmic reticulum"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   An [organelle]({{< relref "organelle" >}})
-   A type of [Endoplasmic reticulum]({{< relref "endoplasmic_reticulum" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) {#sarcoplasmic-reticulum--sarcoplasmic-reticulum-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Muscle cell [SER]({{< relref "smooth_endoplasmic_reticulum" >}}), which forms a network of [L-tubules]({{< relref "l_tubule" >}})

    ---


#### [Differentiate between rough and smooth endoplasmic reticulum (ER) both in structure and function.]({{< relref "differentiate_between_rough_and_smooth_endoplasmic_reticulum_er_both_in_structure_and_function" >}}) {#differentiate-between-rough-and-smooth-endoplasmic-reticulum--er--both-in-structure-and-function-dot--differentiate-between-rough-and-smooth-endoplasmic-reticulum-er-both-in-structure-and-function-dot-md}

<!--list-separator-->

-  [Smooth endoplasmic reticulum]({{< relref "smooth_endoplasmic_reticulum" >}}) (SER)

    <!--list-separator-->

    -  Structure

        -   Irregular network of membrane-bound channels that **lacks ribosomes on its surface** (why its called "smooth")
        -   Usually appears as branching, cross-connected (_anastomosing_) _tubules_ or _vesicles_ whose membranes **do not contain ribophorins**
        -   Less common than RER
        -   Prominent in cells synthesizing steroids, triglycerides, and cholesterol

    <!--list-separator-->

    -  Function

        -   **Steroid hormone synthesis** occurs in SER-rich cells such as Leydig cells (make testosterone)
        -   **Drug detoxification** occurs in hepatocytes
            -   Follows proliferation of the SER in response to the drug _phenobarbital_
                -   The oxidases that metabolize this drug are located in the SER
        -   **Muscle contraction and relaxation** involve the release and recapture of Ca<sup>2+</sup> by the SER in **skeletal muscle cells**
            -   These are called the _sarcoplasmic reticulum_


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles**

    <!--list-separator-->

    -  [Smooth endoplasmic reticulum]({{< relref "smooth_endoplasmic_reticulum" >}}) (SER)

        <!--list-separator-->

        -  Structure

            -   Irregular network of membrane-bound channels that **lacks ribosomes on its surface** (why its called "smooth")
            -   Usually appears as branching, cross-connected (_anastomosing_) _tubules_ or _vesicles_ whose membranes **do not contain ribophorins**
            -   Less common than RER
            -   Prominent in cells synthesizing steroids, triglycerides, and cholesterol

        <!--list-separator-->

        -  Function

            -   **Steroid hormone synthesis** occurs in SER-rich cells such as Leydig cells (make testosterone)
            -   **Drug detoxification** occurs in hepatocytes
                -   Follows proliferation of the SER in response to the drug _phenobarbital_
                    -   The oxidases that metabolize this drug are located in the SER
            -   **Muscle contraction and relaxation** involve the release and recapture of Ca<sup>2+</sup> by the SER in **skeletal muscle cells**
                -   These are called the _sarcoplasmic reticulum_


### Unlinked references {#unlinked-references}

[Show unlinked references]
