+++
title = "Intercostal nerve"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the procedures associated with intercostal nerve anesthesia and thoracotomy and their indications.]({{< relref "describe_the_procedures_associated_with_intercostal_nerve_anesthesia_and_thoracotomy_and_their_indications" >}}) {#describe-the-procedures-associated-with-intercostal-nerve-anesthesia-and-thoracotomy-and-their-indications-dot--describe-the-procedures-associated-with-intercostal-nerve-anesthesia-and-thoracotomy-and-their-indications-dot-md}

<!--list-separator-->

-  **🔖 From Accessing an Intercostal Space > Anesthesia**

    Anesthesia can be placed in the intercostal space to block the [intercostal nerve]({{< relref "intercostal_nerve" >}})

    ---


#### [Describe the neurovasculature of the intercostal spaces.]({{< relref "describe_the_neurovasculature_of_the_intercostal_spaces" >}}) {#describe-the-neurovasculature-of-the-intercostal-spaces-dot--describe-the-neurovasculature-of-the-intercostal-spaces-dot-md}

<!--list-separator-->

-  **🔖 From Accessing an Intercostal Space**

    <!--list-separator-->

    -  [Intercostal Nerves]({{< relref "intercostal_nerve" >}})

        -   _Ventral rami_ of T1-T11
            -   T12 nerve (aka [subcostal nerve]({{< relref "subcostal_nerve" >}})) is not confined between ribs
        -   Near angle of ribs, intercostal nerves pass between the _internal and innermost intercostal muscles_
        -   Intercostal nerves **supply general sensory innervation to skin on the thoracic wall and to the pleura**
            -   They also **supply motor innervation to intercostal, tranversus thoracis, and serratus posterior muscles**
            -   Carry /postganglionic sympathetic nerve fibers?
        -   Intercostal nerves 2-6 are confined to the thorax
        -   _1st intercostal nerve_ contributes to the [brachial plexus]({{< relref "brachial_plexus" >}}) as well as innervating the region of the first intercostal space
        -   _Intercostal nerves 7-11_ leave the intercostal spaces after innervating them -> **continue on to innervate muscles, skin, and lining of arterial abdominal wall**
        -   Intercostal vessels and nerves travel parallel to one another through the intercostal space
            -   Order of structures **superior -> inferior**:
                1.  Vein
                2.  Artery
                3.  Nerve
            -   Mnemonic: _VAN_


### Unlinked references {#unlinked-references}

[Show unlinked references]
