+++
title = "Pancreas"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Degradation of dietary nucleic acids in the small intestine**

    Ribonucleases and deoxyribonucleases are secreted by the [pancreas]({{< relref "pancreas" >}})

    ---


#### [Glucokinase]({{< relref "glucokinase" >}}) {#glucokinase--glucokinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Restricted to [liver]({{< relref "liver" >}}) parenchymal cells and β-cells of [pancreas]({{< relref "pancreas" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
