+++
title = "Osmosis - Transcription, Translation, and Replication"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "osmosis", "source"]
draft = false
+++

## DNA Structure {#dna-structure}


### Nucleotides {#nucleotides}

{{< figure src="/ox-hugo/_20210721_150734screenshot.png" caption="Figure 1: Nucleotides consist of a phosphate group, 5-carbon sugar (deoxyribose for DNA) and a nitrogenous base. The base can be a purine, which has two rings (adenine, guanine), or a pyrimidine, which has one ring (cytosine, guanine)." width="600" >}}


#### Nucleotide binding and bonding {#nucleotide-binding-and-bonding}

-   Nucleotides bind using sugar, phosphate groups -> sugar-phosphate backbone
    -   phosphate group on 5th carbon of sugar binds covalently to 3rd carbon of sugar
-   Nucleotides form hydrogen bonds with bases on opposing strand
    -   _Complementary base pairing_: A <-> T/U, C <-> G


#### DNA structure and packing {#dna-structure-and-packing}

-   Strands coil around each other once every 10 base pairs -> major + minor grooves

{{< figure src="/ox-hugo/_20210721_151248screenshot.png" caption="Figure 2: Major and minor grooves: larger/smaller spaces between DNA strands where proteins can bind to regulate functions" >}}

-   In order to be packed tightly, DNA wraps around histones
    -   Positive charge attracts to negative charge of phosphate backbone -> forms _nucleosomes_
    -   Nucleosomes further packed as _chromatin_ fibers
        -   _Euchromatin_: loosely packed ([genes]({{< relref "gene" >}}) frequently used)
        -   _Heterchromatin_: densely packed ([genes]({{< relref "gene" >}}) rarely used)

{{< figure src="/ox-hugo/_20210721_151323screenshot.png" caption="Figure 3: DNA wraps around histone proteins to form nucleosomes, which pack tighter again to form chromatin fibers" >}}


## DNA Replication {#dna-replication}

-   Occurs in **S phase** of cell cycle (before cell division)
-   46 chromosomes duplicated -> each daughter cell gets genetic material


### Process {#process}

{{< figure src="/ox-hugo/_20210721_151909screenshot.png" caption="Figure 4: Three steps of DNA replication: initiation, elongation, and termination. DNA replication results in two sets of identital DNA, each containing one old strand and one new one" width="500" >}}


#### Initiation {#initiation}

-   Pre-replication complex seeks origin of replication -> DNA helicase splits strands -> _replication fork_ formed
    -   SIngle-stranded DNA binding proteins improve stability of lone strands
    -   _DNA topoisomerase_ prevents overwinding of later DNA


#### Elongation {#elongation}

-   _RNA primase_ creates multiple RNA primers -> randomly bind -> [DNA polymerase]({{< relref "dna_polymerase" >}}) adds complementary nucleotides in 3->5 direction
    -   Forms signle leading strand
    -   Forms single lagging strand by attaching multiple _Okazaki fragments_ using _DNA ligase_


#### Termination {#termination}

-   [DNA polymerase]({{< relref "dna_polymerase" >}}) leaves strand at telomere (TTAGGG nucleotide sequences)
-   _Hayflick limit_: maximum number of times cell's DNA can be replicated
    -   Due to repeated shortening of telomeres during termination


### DNA Cloning {#dna-cloning}

-   Duplicates segment of DNA within host organism
-   Uses _plasmids_: [genetic]({{< relref "genetics" >}}) structures outside of chromosomes that are replicated independently


#### Process {#process}

1.  Extract desired DNA segment using specific restriction enzymes
2.  Paste segment into plasmid with DNA ligase -> form _recombinant DNA_
3.  Insert plasmid into host organism and encourage uptake with shock (e.g. heat)
4.  Identify bacteria carrying plasmid using antibiotics
    -   The plasmid is given an antibiotic resistance gene
5.  Leave bacteria to replicate DNA segment and mass-manufacture protein(s)

{{< figure src="/ox-hugo/_20210721_152240screenshot.png" caption="Figure 5: DNA cloning. Restriction enzyme (in this case, EcoRI) cleaves a known equence surrounding a target gene and a plasmid, creating pieces with sticky ends. When DNA ligase is added, these pieces form recombinant DNA (plasmid containing target gene), as well as a gene for antibiotic resistance. A host (e.g. _E. coli_) is combined with recombinant plasmids and subjected to a stressor to induce bacteria to take up plasmid. Bacteria are allowed to replicate on plate containing antibiotics so that only the resistant bacteria (i.e. those with the plasmid) survive. These bacteria produce the desired protein from the target gene in the plasmid." width="500" >}}


## Transcription {#transcription}

-   First step in creating protein from [gene]({{< relref "gene" >}})
-   Gene is read and copied into mRNA


### Process {#process}

1.  DNA unpacked form chormatin, undergoes dehelicization
2.  Promoter region identifies starting point for transcription (e.g. TATA box)
3.  RNA polymerase shears hydrogen bonds between two strands -> forms _transcription bubble_
4.  RNA polymerase follows template strand to assemble mRNA molecule (complementary to template strand)
5.  Hydrogen bonds reform on nucleotides already transcribed
6.  Termination sequences contain two complementary sequences -> mRNA binds with itself to form _hairpin loop_
7.  RNA polymerase detaches, DNA closes back up
8.  _Polyadenylate polymerase_ adds _7-methyl guanosine cap_ to 5' end and _polyadenine tail_ (poly-A tail) to 3' end of mRNA
9.  _Spliceosomes_ remove introns to leave behind exons
    -   _Introns_: non-coding regions
    -   _Exons_: coding regions
10. mRNA sent to ribosome for translation

{{< figure src="/ox-hugo/_20210721_152818screenshot.png" width="500" >}}


## Translation {#translation}

-   Second step in creating protein from [gene]({{< relref "gene" >}})
-   [Ribosomes]({{< relref "ribosome" >}}) assemble protein from mRNA template produced in transcription


### Process {#process}

1.  mRNA floats out of nucleus through pore
2.  _Initiation_: [ribosome]({{< relref "ribosome" >}}) grabs mRNA and finds start codon (e.g. AUG)
3.  _Elongation_: [ribosome]({{< relref "ribosome" >}}) moves along mRNA producing specific [AA]({{< relref "amino_acid" >}})s for each codon
4.  _Termination_: [ribosome]({{< relref "ribosome" >}}) reaches stop codon and releases polypeptide

{{< figure src="/ox-hugo/_20210721_154330screenshot.png" width="500" >}}


### [Transfer RNA]({{< relref "transfer_rna" >}}) (tRNA) {#transfer-rna--transfer-rna-dot-md----trna}

-   Finds and carries [amino acids]({{< relref "amino_acid" >}}) to ribosome
    -   Each carries **one** AA
-   Three-letter coding sequence complementary to mRNA
-   Binds to [ribosome]({{< relref "ribosome" >}}) on the aminoacyl/peptidyl/exit site
    -   _Aminoacyl_: binds tRNA with complementary mRNA codon
    -   _Peptidyl_: holds tRNA with polypeptide
    -   _Exit_: holds tRNA after amino acid is released

{{< figure src="/ox-hugo/_20210721_154107screenshot.png" caption="Figure 6: One strand of DNA is called the coding strand and the other is called the template strand. They have complementary nucleotide sequences. RNA polymerase builds an mRNA molecule by reading the template strand and adding complementary nucleotides. Therefore, the mRNA will have the same sequence and direcitonality as the coding strand, only with U instead of T." width="500" >}}


## [Genetic]({{< relref "gene" >}}) Mutations and Repair {#genetic--gene-dot-md--mutations-and-repair}


### DNA mutations {#dna-mutations}

-   Alterations in nucleotide sequence of one or more genes


### Small-scale mutations {#small-scale-mutations}

-   **Single gene**
-   _Substitutions_: nucleotide replaced by another
-   May result in:
    -   _Silent mutation_: results in same AA -> no change
    -   _Missense mutation_: different AA
    -   _Nonsense mutation_: stop codon

{{< figure src="/ox-hugo/_20210721_155602screenshot.png" width="500" >}}


### Insertions and deletions {#insertions-and-deletions}

-   Nucleotide is added or removed from sequence
-   _Frameshift mutation_: Reading frame displaced, resulting in complete change in protein AA sequence (and consequently function)


### Large-scale mutations {#large-scale-mutations}

-   Often occur due to errors in gamete formation


#### Abnormal number of chromosomes {#abnormal-number-of-chromosomes}

-   _Aneuploidy_: Additional or missing chromosomes
-   _Polyploidy_: Increased number of chromosomes per set

{{< figure src="/ox-hugo/_20210721_155634screenshot.png" width="500" >}}


#### Structurally abnormal {#structurally-abnormal}

-   Movement of sections of chromosomes
-   _Deletion_: part of chromosome goes missing
-   _Duplication_: part of chromosome duplicated
-   _Inversion_: part of chromosome breaks off and reattaches
-   _Translocation_: parts of two chromosomes are switched

{{< figure src="/ox-hugo/_20210721_155649screenshot.png" width="500" >}}


### DNA damage {#dna-damage}

-   DNA can be damaged by internal (_endogenous_) or environmental (_exogenous_) factors
-   If damaged DNA cannot be fixed ->
    -   _Senescence_: ceasing of cell division
    -   _Apoptosis_: programmed cell death
    -   Uncontrolled cell division -> tumor growth
-   If damaged DNA can be fixed -> G<sub>0</sub> phase


#### Single strand damage {#single-strand-damage}

-   Causes:
    -   Endogenous: errors in DNA replication
    -   Exogenous: harmful chemical/physical agents
-   Repaired with mismatch/base excision/nucleotide excision repair
    1.  Endonucleases cleave damaged segment
    2.  Exonucleases removed damaged segment
    3.  [DNA polymerase]({{< relref "dna_polymerase" >}}) rebuilds segment
    4.  DNA ligase glues new segment

{{< figure src="/ox-hugo/_20210721_155730screenshot.png" width="500" >}}


#### Double stranded breaks {#double-stranded-breaks}

-   Can occur due to **ionizing radiation**

{{< figure src="/ox-hugo/_20210721_155754screenshot.png" width="500" >}}

<!--list-separator-->

-  Repair mechanisms

    -   _Non-homologous end joining_:
        -   DNA protein kinase binds to each end of the broken DNA -> artemis cuts off rough ends -> ends are rejoined with DNA ligase
    -   _Homologous end joining_:
        -   MRN protein complex binds to each end and removes affected nucleotides -> [DNA polymerase]({{< relref "dna_polymerase" >}}) copies genetic information from sister chromatid


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Khan - The genetic code]({{< relref "khan_the_genetic_code" >}}) {#khan-the-genetic-code--khan-the-genetic-code-dot-md}

<!--list-separator-->

-  **🔖 Overview: Gene expression and the genetic code**

    _[Translation]({{< relref "osmosis_transcription_translation_and_replication" >}})_

    ---

<!--list-separator-->

-  **🔖 Overview: Gene expression and the genetic code**

    _[Transcription]({{< relref "osmosis_transcription_translation_and_replication" >}})_

    ---


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 5. Summarize the mechanism of DNA replication and replication forks, and describe what is meant by the terms semi-conservative, discontinuous, leading strand, lagging strand, processivity, and fidelity**

    [DNA Replication]({{< relref "osmosis_transcription_translation_and_replication" >}})

    ---

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 3. Summarize the structure and function of eukaryotic chromosomes and chromatin, including chromatin modifications, telomeres, and centromeres**

    [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}})

    ---

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 2. Describe the double-stranded, helical, antiparallel, and topological structure of DNA and how it relates to the processes of DNA replication, transcription, recombination and repair**

    [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
