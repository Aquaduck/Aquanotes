+++
title = "Collagen"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Type 1 collagen]({{< relref "type_1_collage" >}}) {#type-1-collagen--type-1-collage-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The most common type of [Collagen]({{< relref "collagen" >}})

    ---


#### [Name the types of cellular and acellular components of connective tissue]({{< relref "name_the_types_of_cellular_and_acellular_components_of_connective_tissue" >}}) {#name-the-types-of-cellular-and-acellular-components-of-connective-tissue--name-the-types-of-cellular-and-acellular-components-of-connective-tissue-dot-md}

<!--list-separator-->

-  **🔖 From Introduction to Connective Tissue (CT) Pre-workshop Reading > Fibrous structural components**

    <!--list-separator-->

    -  [Collagen]({{< relref "collagen" >}})

        -   The most abundant protein in the body
        -   28 types labeled (Type I, Type II, etc.)
        -   90% of collagen is type I


### Unlinked references {#unlinked-references}

[Show unlinked references]
