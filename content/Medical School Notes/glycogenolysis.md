+++
title = "Glycogenolysis"
author = ["Arif Ahsan"]
date = 2021-09-07T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Glycogen phosphorylase]({{< relref "glycogen_phosphorylase" >}}) {#glycogen-phosphorylase--glycogen-phosphorylase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Used in [glycogenolysis]({{< relref "glycogenolysis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
