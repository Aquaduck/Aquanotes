+++
title = "Axon terminal"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The terminal end of an [axon]({{< relref "axon" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the general morphology of neurons and levels of organization of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}}) {#describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot--describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127) > Neuron structure > Axons**

    Terminate in many small branches ([axon terminals]({{< relref "axon_terminal" >}})) from which impulses are passed to another neuron or type of cell(s)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
