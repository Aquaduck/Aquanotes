+++
title = "Explain how the defect leads to dysfunction in the lungs."
author = ["Arif Ahsan"]
date = 2021-10-17T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cystic fibrosis in the year 2020: A disease with a new face]({{< relref "cystic_fibrosis_in_the_year_2020_a_disease_with_a_new_face" >}}) {#from-cystic-fibrosis-in-the-year-2020-a-disease-with-a-new-face--cystic-fibrosis-in-the-year-2020-a-disease-with-a-new-face-dot-md}


### [CF]({{< relref "cystic_fibrosis" >}}) class mutations of [CFTR]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}}) {#cf--cystic-fibrosis-dot-md--class-mutations-of-cftr--cystic-fibrosis-transmembrane-conductance-regulator-dot-md}

|                    | Class 1          | Class 2        | Class 3                  | Class 4                  | Class 5          | Class 6           | Class 7     |
|--------------------|------------------|----------------|--------------------------|--------------------------|------------------|-------------------|-------------|
| CFTR defect        | No protein       | No traffic     | Impaired gating          | Decreased conductance    | Less protein     | Less stable       | No mRNA     |
| Corrective therapy | Rescue synthesis | Rescue traffic | Restore channel activity | Restore channel activity | Correct splicing | Promote stability | Unrescuable |

-   **Note**: Mutation classification is an oversimplification, as most mutations often contain traits of multiple classes


#### Class I Mutations {#class-i-mutations}

-   Near absence of CFTR protein
-   Mainly stop codon mutations and frameshift mutations -> premature stop codons


#### Class II Mutations {#class-ii-mutations}

-   Defective processing and trafficking of the CFTR protein -> degraded in proteasome -> amount of CFTR protein at apical membrane severely reduced


#### Class III Mutations {#class-iii-mutations}

-   CFTR protein reaches cell membrane, but defective regulation of CFTR gating -> no channel opening


#### Class IV Mutations {#class-iv-mutations}

-   Impaired conductance of the CFTR channel -> fewer ions passing through open channel pore


#### Class V Mutations {#class-v-mutations}

-   Often alternative splice mutations
-   A reduced amount of normal CFTR protein


#### Class VI Mutations {#class-vi-mutations}

-   Unstable CFTR protein -> prematurely recycled from apical membrane -> degraded in lysosomes


#### Class VII Mutations {#class-vii-mutations}

-   Large deletions and frameshift mutations not easily amenable to pharmacotherapy


## From [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#from-openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-19 Tue] </span></span> > Cystic Fibrosis Small Group > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Explain how the defect leads to dysfunction in the lungs.]({{< relref "explain_how_the_defect_leads_to_dysfunction_in_the_lungs" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
