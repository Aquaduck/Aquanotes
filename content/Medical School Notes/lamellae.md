+++
title = "Lamellae"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A parallel layer of mature [Bone]({{< relref "bone" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Osteon]({{< relref "osteon" >}}) {#osteon--osteon-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Composed of multiple concentric [Lamellae]({{< relref "lamellae" >}})

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Histologic components of mature bone**

    Mature bone is laid down in parallel layers called [lamellae]({{< relref "lamellae" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
