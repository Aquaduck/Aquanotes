+++
title = "Appositional growth"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work**

    <!--list-separator-->

    -  [Appositional growth]({{< relref "appositional_growth" >}}) and remodeling

        -   Compact bone may form by appositional growth of lamellae
            -   _Appositional_ - on the surface
            -   This can occur on the external surface (external circumferential lamella) or the surface of internal trabeculae
        -   Remodeling of bone occurs **during growth** and **with changing stresses on bone**
            -   In an adult, intact [osteons]({{< relref "osteon" >}}) have a half-life of many years
                -   Remodeling in a growing child or fractured adult bone is much faster
            -   A broken bone is initially fused together with a fibrous (collagenous) scar that calcifies ([Woven bone]({{< relref "woven_bone" >}})) and then undergoes remodeling by [Haversian canal]({{< relref "haversian_canal" >}})
                -   This remodeling can produce a bone with the shape and strength of the original bone


### Unlinked references {#unlinked-references}

[Show unlinked references]
