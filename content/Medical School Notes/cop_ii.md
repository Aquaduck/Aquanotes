+++
title = "COP-II"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [coatomer]({{< relref "coatomer" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Coated vesicles > Clathrin-coated vesicles > Function**

    [COP-II]({{< relref "cop_ii" >}}) transports molecules forward in path mentioned above

    ---


#### [Chylomicron retention disease]({{< relref "chylomicron_retention_disease" >}}) {#chylomicron-retention-disease--chylomicron-retention-disease-dot-md}

<!--list-separator-->

-  **🔖 From Molecular analysis and intestinal expression of SAR1 genes and proteins in Anderson's disease (Chylomicron retention disease)**

    A small GTPase involved [COPII]({{< relref "cop_ii" >}})-dependent transport of proteins from the [ER]({{< relref "endoplasmic_reticulum" >}}) to the [Golgi]({{< relref "golgi_apparatus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
