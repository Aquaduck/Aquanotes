+++
title = "GLUT-1"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Overview of the most important glucose transporters]({{< relref "overview_of_the_most_important_glucose_transporters" >}}) {#overview-of-the-most-important-glucose-transporters--overview-of-the-most-important-glucose-transporters-dot-md}

|                                  |                  |                     |    |
|----------------------------------|------------------|---------------------|----|
| [GLUT1]({{< relref "glut_1" >}}) | Most human cells | Blood-brain barrier | No |

---


#### [Hypoxia-inducible factor 1]({{< relref "hypoxia_inducible_factor_1" >}}) {#hypoxia-inducible-factor-1--hypoxia-inducible-factor-1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Stimulates production of [GLUT-1]({{< relref "glut_1" >}}) and [GLUT-3]({{< relref "glut_3" >}})

    ---


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 HIF-1 target genes involved in glucose and energy metabolism**

    HIF-1 activates the transcription of _SLC2A1_ and _SLC2A3_ -> encode [GLUT-1]({{< relref "glut_1" >}}) and [GLUT-3]({{< relref "glut_3" >}}) respectively (**upregulation of glucose transporters**)

    ---


#### [Distinguish the insulin-responsive glucose membrane transporter and in what tissues it is located, from that used by:]({{< relref "distinguish_the_insulin_responsive_glucose_membrane_transporter_and_in_what_tissues_it_is_located_from_that_used_by" >}}) {#distinguish-the-insulin-responsive-glucose-membrane-transporter-and-in-what-tissues-it-is-located-from-that-used-by--distinguish-the-insulin-responsive-glucose-membrane-transporter-and-in-what-tissues-it-is-located-from-that-used-by-dot-md}

<!--list-separator-->

-  **🔖 Brain**

    Uses [GLUT-1]({{< relref "glut_1" >}}) and [GLUT-3]({{< relref "glut_3" >}})

    ---


#### [A Discussion of the Significance of Two Key Metabolic Branchpoints]({{< relref "a_discussion_of_the_significance_of_two_key_metabolic_branchpoints" >}}) {#a-discussion-of-the-significance-of-two-key-metabolic-branchpoints--a-discussion-of-the-significance-of-two-key-metabolic-branchpoints-dot-md}

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > RBCs > Fed state**

    _[GLUT-1]({{< relref "glut_1" >}})_: primary glucose transporter in RBCs

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
