+++
title = "Pneumonia"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Respiratory Acidosis: Primary Carbonic Acid/Carbon Dioxide Excess**

    Can result from anything that interferes with respiration, e.g. [pneumonia]({{< relref "pneumonia" >}}), [emphysema]({{< relref "emphysema" >}}), or [congestive heart failure]({{< relref "congestive_heart_failure" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
