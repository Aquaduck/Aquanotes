+++
title = "Catecholamine"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Norepinephrine]({{< relref "norepinephrine" >}}) {#norepinephrine--norepinephrine-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [catecholamine]({{< relref "catecholamine" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Catecholamine]({{< relref "catecholamine" >}}) ([norepinephrine]({{< relref "norepinephrine" >}})) binding to an [alpha-1 receptor]({{< relref "alpha_1_receptor" >}}) -> vasoconstriction -> decreased blood flow

    ---


#### [Describe the relationship between ventricular filling and stroke volume on a Frank-Starling curve.]({{< relref "describe_the_relationship_between_ventricular_filling_and_stroke_volume_on_a_frank_starling_curve" >}}) {#describe-the-relationship-between-ventricular-filling-and-stroke-volume-on-a-frank-starling-curve-dot--describe-the-relationship-between-ventricular-filling-and-stroke-volume-on-a-frank-starling-curve-dot-md}

<!--list-separator-->

-  **🔖 From Physiology, Frank Starling Law > Mechanism of the Frank-Starling law**

    An increase in [catecholamines]({{< relref "catecholamine" >}}) (e.g. norepinephrine during exercise) -> upward shift of Frank-Starling curve

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
