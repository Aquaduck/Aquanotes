+++
title = "Propionyl-CoA"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Undergoes an ATP- and biotin-dependent carboxylation reaction to form [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) -> converted to L-methylmalonyl-CoA


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [β-oxidation]({{< relref "β_oxidation" >}}) {#β-oxidation--β-oxidation-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    For odd-chained fatty acids, produces 1 acetyl-CoA and 1 [Propionyl-CoA]({{< relref "propionyl_coa" >}})

    ---


#### [Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}}) {#propionyl-coa-carboxylase--propionyl-coa-carboxylase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Catalyzes the formation of [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) from [Propionyl-CoA]({{< relref "propionyl_coa" >}})

    ---


#### [List the products of the ß-oxidation of odd-chain fatty acids.]({{< relref "list_the_products_of_the_ß_oxidation_of_odd_chain_fatty_acids" >}}) {#list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot--list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Products of beta-oxidation of odd-chain fatty acids**

    [Propionyl-CoA]({{< relref "propionyl_coa" >}}) -([Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}}))> [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) -> [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) -([Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}}))> [succinyl-CoA]({{< relref "succinyl_coa" >}}) -> TCA cycle

    ---

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Products of beta-oxidation of odd-chain fatty acids**

    Initial products of odd-chain fatty acid beta-oxidation are [acetyl-CoA]({{< relref "acetyl_coa" >}}) and [propionyl-CoA]({{< relref "propionyl_coa" >}})

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Propionyl-CoA carboxylase > From Fat Metabolism in Muscle & Adipose Tissue**

    Catalyzes the formation of D-methylmalonyl-CoA from [Propionyl-CoA]({{< relref "propionyl_coa" >}})

    ---

<!--list-separator-->

-  [Propionyl-CoA]({{< relref "propionyl_coa" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Produced via the beta-oxidation of odd-chain fatty acids
        -   Undergoes an ATP- and biotin-dependent carboxylation reaction to form [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) -> converted to L-methylmalonyl-CoA


#### [Detail the vitamin requirements for the metabolism of propionyl-CoA.]({{< relref "detail_the_vitamin_requirements_for_the_metabolism_of_propionyl_coa" >}}) {#detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot--detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  Vitamin requirements for the metabolism of [propionyl-CoA]({{< relref "propionyl_coa" >}})

        1.  [Biotin]({{< relref "biotin" >}}): carboxylation of propionyl-CoA to form [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) via [Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})
        2.  [B12]({{< relref "cobalamin" >}}): Isomerization of [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
