+++
title = "Sheathed capillary"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Blind-ended capillaries without an endothelium
-   They are instead surrounded by aggregates of [histiocytes]({{< relref "histiocyte" >}})
-   Found in the [Spleen]({{< relref "spleen" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Splenic cords]({{< relref "splenic_cords" >}}) {#splenic-cords--splenic-cords-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Sheathed capillaries]({{< relref "sheathed_capillary" >}}) in the [spleen]({{< relref "spleen" >}}) dump blood into here

    ---


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen > Spleen filtration**

    Senescent cells and material are removed as they attempt to navigate the [macrophages]({{< relref "macrophage" >}}) of the [sheath]({{< relref "sheathed_capillary" >}}) and [cords]({{< relref "splenic_cords" >}}) -> squeeze between endothelial cells to return to the vascular system in the sinuses of the [red pulp]({{< relref "red_pulp" >}})

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen > Spleen filtration**

    _Open vascular system_: smallest vessels are blind-ended capillaries ([sheathed capillaries]({{< relref "sheathed_capillary" >}})) without an endothelium -> the ends are instead surrounded by aggregates of [histiocytes]({{< relref "histiocyte" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
