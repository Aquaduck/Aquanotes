+++
title = "Fas-associated death domain"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Recruited as part of formation of the [DISC]({{< relref "death_inducing_signaling_complex" >}})
-   Contains a [Death domain]({{< relref "death_domain" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  **🔖 TRADD in TRAIL/TRAILR Signaling**

    The recruitment of [FADD]({{< relref "fas_associated_death_domain" >}}) was increased in the absence of TRADD, indicating that:

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
