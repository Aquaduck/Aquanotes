+++
title = "Identify key molecules involved in apoptotic cell death"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}}) {#from-molecular-biology-of-the-cell-sixth-edition--molecular-biology-of-the-cell-sixth-edition-dot-md}


### [Caspase]({{< relref "caspase" >}}) {#caspase--caspase-dot-md}


#### [Apoptosis]({{< relref "apoptosis" >}}) depends on an intracellular proteolytic cascade that is mediated by caspases (p.1022) {#apoptosis--apoptosis-dot-md--depends-on-an-intracellular-proteolytic-cascade-that-is-mediated-by-caspases--p-dot-1022}

-   Irreversible apoptosis is triggered by _Caspase_ proteases
    -   A family of specialized intracellular proteases which cleave specific sequences in numerous proteins inside the cell leading to cell death
    -   Have a **cysteine** at their active site and cleave target proteins at specific **aspartic acids**
    -   Synthesized in the cell as inactive precursors -> **activated only during apoptosis**
    -   Two major classes: _initiator_ caspases and _executioner_ caspases


#### [Initiator caspases]({{< relref "initiator_caspase" >}}) {#initiator-caspases--initiator-caspase-dot-md}

-   Begin apoptosis, main function being to **activate executioner caspases**
-   Normally exist as inactive, soluble monomers in the cytosol
-   An apoptotic signal triggers the assembly of large protein platofrms -> formation of large complexes of initiator caspases
    -   Pairs of caspases associate to form dimers -> **protease activation**
        -   Each caspase in the dimer cleaves its partner at a specific site in the protease domain -> stabilizes active complex and required for proper function of the enzyme in the cell


#### [Executioner caspases]({{< relref "executioner_caspase" >}}) {#executioner-caspases--executioner-caspase-dot-md}

-   Normally exist as inactive dimers
    -   Cleaved by initiator caspase -> active site rearranged -> activation


#### Targets of caspase {#targets-of-caspase}

-   Nuclear [lamins]({{< relref "lamin" >}}) -> irreversible breakdown of nuclear lamina
-   Cleavage of protein that frees [endonuclease]({{< relref "endonuclease" >}}) -> cuts up DNA in cell nucleus
-   Components of the cytoskeleton and cell-cell adhesion proteins -> allows cell to round up and detach from neighbors -> easier for neighboring cells to engulf it


### [Death receptors]({{< relref "death_receptor" >}}) {#death-receptors--death-receptor-dot-md}


#### Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024) {#cell-surface-death-receptors-activate-the-extrinsic-pathway--extrinsic-pathway-of-apoptosis-dot-md--of-apoptosis--p-dot-1024}

-   Death receptors are transmembrane proteins containing:
    -   An extracellular ligand-binding domain
    -   A single transmembrane domain
    -   An intracellular [death domain]({{< relref "death_domain" >}}) -> required for receptor to activate [apoptosis]({{< relref "apoptosis" >}})
-   Death receptors are homotrimers and belong to the [tumor necrosis factor]({{< relref "tumor_necrosis_factor_receptor" >}}) (TNF) **receptor** family
    -   The ligands that activate death receptors are also homotrimers and are part of the [TNF]({{< relref "tumor_necrosis_factor" >}}) family of **signal proteins**


### [Fas]({{< relref "fas" >}}) {#fas--fas-dot-md}


#### Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024) {#cell-surface-death-receptors-activate-the-extrinsic-pathway--extrinsic-pathway-of-apoptosis-dot-md--of-apoptosis--p-dot-1024}

-   A well-understood example of how death receptors trigger the extrinsic pathway of apoptosis is the activation of Fas on the surface of a target cell by Fas ligand on the surface of a killer (cytotoxic) lymphocyte
    -   Fas activated by [Fas ligand]({{< relref "fas_ligand" >}}) -> death domains on Fas death receptors bind intracellular adaptor proteins -> bind [initiator caspases]({{< relref "initiator_caspase" >}}) (primarily [caspase-8]({{< relref "caspase_8" >}})) -> formation of a [death-inducing signaling complex]({{< relref "death_inducing_signaling_complex" >}}) (DISC)
    -   Once dimerized + activated in [DISC]({{< relref "death_inducing_signaling_complex" >}}), [initiator caspases]({{< relref "initiator_caspase" >}}) cleave their partners -> activate downstreain [executioner caspases]({{< relref "executioner_caspase" >}}) to induce [apoptosis]({{< relref "apoptosis" >}})

{{< figure src="/ox-hugo/_20210926_143723screenshot.png" >}}


### [FLIP]({{< relref "flice_inhibitor_protein" >}}) {#flip--flice-inhibitor-protein-dot-md}


#### Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024) {#cell-surface-death-receptors-activate-the-extrinsic-pathway--extrinsic-pathway-of-apoptosis-dot-md--of-apoptosis--p-dot-1024}

-   FLIP is an inhibitory protein that **restrains the extrinsic pathway** -> inhibits inappropriate activation of apoptosis
-   FLIP resembles an initiator caspase **but has no protease activity** because it lacks a key cysteine in its active site
-   FLIP dimerizes with [caspase-8]({{< relref "caspase_8" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})
    -   While caspase-8 appears to be active, it is not cleaved at the site required for its stable activation -> blockage of apoptotic signal


### [Cytochrome c]({{< relref "cytochrome_c" >}}) and Apaf1 {#cytochrome-c--cytochrome-c-dot-md--and-apaf1}


#### The [intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}}) depends on mitochondria (p. 1025) {#the-intrinsic-pathway-of-apoptosis--intrinsic-pathway-of-apoptosis-dot-md--depends-on-mitochondria--p-dot-1025}

-   Cytochrome c is a water-soluble component of the mitochondrial electron transport chain
-   When released into the cytosol, cytochrome c binds to [Apaf1]({{< relref "apoptotic_protease_activating_factor_1" >}}) -> Apaf1 oligomerizes into a wheel-like heptamer called an [apoptosome]({{< relref "apoptosome" >}}) -> recruits [caspase-9]({{< relref "caspase_9" >}}) -> activation of downstream executioner caspases -> induction of apoptosis


### [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}}) {#bcl2-family--b-cell-lymphoma-2-family-dot-md}


#### Bcl2 proteins regulate the [intrinsic pathway]({{< relref "intrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1026) {#bcl2-proteins-regulate-the-intrinsic-pathway--intrinsic-pathway-of-apoptosis-dot-md--of-apoptosis--p-dot-1026}

-   Bcl2 proteins are a major class of intracellular regulators of the intrinsic pathway of apoptosis
    -   Controls the release of [cytochrome c]({{< relref "cytochrome_c" >}}) and other intermembrane mitochondrial proteins into the cytosol
    -   Some are _pro-apoptotic_ -> promote apoptosis by enhancing release
    -   Some are _anti-apoptotic_ -> inhibit apoptosis by blocking release
-   Pro- and anti-apoptotic Bcl2 proteins can bind to each other in various combinations to form heterodimers -> can **inhibit each other's function**
    -   Largely determines whether a mammalian cell lives or dies by the intrinsic pathway of apoptosis

<!--list-separator-->

-  [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}})

    -   **Pro-apoptotic** effector Bcl2 family proteins
    -   **At least one of these is required** for the intrinsic pathway of apoptosis to operate

<!--list-separator-->

-  [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) and [BclX<sub>L</sub>]({{< relref "bclx_l" >}})

    -   **Anti-apoptotic**
    -   Bind to pro-apoptotic Bcl2 family proteins -> inhibit apoptosis

<!--list-separator-->

-  [BH3-only proteins]({{< relref "bh3_only_protein" >}})

    -   Cell either produces or activates BH3-only proteins in response to an apoptotic stimulus
    -   Provide the crucial link between apoptotic stimuli and the intrinsic pathway of apoptosis
        -   Different stimuli activates different BH3-only proteins
    -   Thought to promote apoptotis mainly by inhibiting anti-apoptotic Bcl2 family proteins
    -   BH3 domain binds to a long hydrophobic groove on anti-apoptotic Bcl2 family proteins -> neutralizes their activity
        -   Enables the aggregation of [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}}) on the surface of mitochondria -> triggers release of intermembrane mitochondrial proteins -> induction of apoptosis
    -   Some bind to Bax and Bak directly to stimulate their aggregation

    <!--list-separator-->

    -  [Bid]({{< relref "bid" >}})

        -   Normally inactive
        -   When [death receptors]({{< relref "death_receptor" >}}) activate the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}), [Caspase-8]({{< relref "caspase_8" >}}) cleaves Bid -> activates Bid -> translocates to outer mitochondrial membrane -> inhibits anti-apoptotic Bcl2 family proteins -> **amplification of death signal**


### [IAPs]({{< relref "inhibitor_of_apoptosis" >}}) {#iaps--inhibitor-of-apoptosis-dot-md}

-   All IAPs have one or more [baculovirus IAP repeat]({{< relref "baculovirus_iap_repeat" >}}) (BIR) domains
    -   Enable them to bind to and inhibit activated caspases
-   Some IAPs also polyubiquitylate [caspases]({{< relref "caspase" >}}) -> marking them for destruction by proteasomes


### [SMAC]({{< relref "second_mitochondria_derived_activator_of_caspases" >}}) {#smac--second-mitochondria-derived-activator-of-caspases-dot-md}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-27 Mon] </span></span> > Cell Death > Pre-work**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
