+++
title = "PMS2"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A protein involved in [DNA mismatch repair]({{< relref "dna_mismatch_repair" >}}) that can lead to colon cancer
-   A [MutL homolog]({{< relref "mutl_homolog" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
