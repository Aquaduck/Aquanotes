+++
title = "The Equations of Enzyme Kinetics"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

[Source](https://chem.libretexts.org/Bookshelves/Physical%5Fand%5FTheoretical%5FChemistry%5FTextbook%5FMaps/Map%3A%5FPhysical%5FChemistry%5Ffor%5Fthe%5FBiosciences%5F(Chang)/10%3A%5FEnzyme%5FKinetics/10.2%3A%5FThe%5FEquations%5Fof%5FEnzyme%5FKinetics)


## [Lineweaver-Burk plot]({{< relref "lineweaver_burk_plot" >}}) {#lineweaver-burk-plot--lineweaver-burk-plot-dot-md}

-   Inverse of reaction rate \\(\frac{1}{r}\\) plotted against inverse of substrate concentration \\(\frac{1}{[S]}\\)
    -   Basically reciprocal of Michaelis-Menten
-   Provides a useful graphical method for analysis of the [Michaelis-Menten]({{< relref "michaelis_menten_kinetics" >}}) equation

{{< figure src="/ox-hugo/_20210721_192913screenshot.png" caption="Figure 1: Lineweaver-Burk plot of Michaelis-Menten kinetics" >}}


### Example problem {#example-problem}

{{< figure src="/ox-hugo/_20210721_193056screenshot.png" >}}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
