+++
title = "Microfilament"
author = ["Arif Ahsan"]
date = 2021-09-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Actin]({{< relref "actin" >}}) {#actin--actin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [microfilament]({{< relref "microfilament" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
