+++
title = "Session 15"
author = ["Arif Ahsan"]
date = 2021-07-30T00:00:00-04:00
tags = ["medschool"]
draft = false
+++

## Application Questions {#application-questions}

1.  A
    -   ATP Synthase is **reversible**
2.  B
3.  A
4.  C
5.  B
6.  A
7.  B
8.  D
9.  A
10. D
11. A
12. D
13. A


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
