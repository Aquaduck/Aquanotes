+++
title = "Describe the cyclic-AMP (cAMP) second messenger signal pathway from hormone binding to activation of protein kinase-A."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### G-protein signal cascades: cAMP {#g-protein-signal-cascades-camp}

-   Located in the plasma membrane are three key elements of the cAMP signal cascade:
    1.  **Receptor**
        -   **Must traverse the plasma membrane 7 times** <- required of **all** [GPCRs]({{< relref "g_protein_coupled_receptors" >}}) (so far described)
    2.  [G-protein]({{< relref "g_protein" >}})
        -   A trimeric protein situated in the plasma membrane
        -   Requires GTP and GDP to function
    3.  [Adenylyl cyclase]({{< relref "adenylate_cyclase" >}})


### [cAMP]({{< relref "camp" >}}) Activation {#camp--camp-dot-md--activation}

1.  Signal activates receptor -> **conformational change in receptor**
2.  G<sub>s</sub> complex activated -> activates [adenylyl cyclase]({{< relref "adenylate_cyclase" >}}) -> formation of cAMP


### Activation of [Protein kinase A]({{< relref "protein_kinase_a" >}}) (PKA) {#activation-of-protein-kinase-a--protein-kinase-a-dot-md----pka}

<ol class="org-ol">
<li value="20">cAMP activates PKA</li>
</ol>


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the cyclic-AMP (cAMP) second messenger signal pathway from hormone binding to activation of protein kinase-A.]({{< relref "describe_the_cyclic_amp_camp_second_messenger_signal_pathway_from_hormone_binding_to_activation_of_protein_kinase_a" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
