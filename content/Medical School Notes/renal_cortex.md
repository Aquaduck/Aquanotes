+++
title = "Renal cortex"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The outermost layer of the [renal parenchyma]({{< relref "kidney" >}})
-   Surrounds the [renal medulla]({{< relref "renal_medulla" >}}) and extends inward


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Renal corpuscle]({{< relref "renal_corpuscle" >}}) {#renal-corpuscle--renal-corpuscle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Located in the [renal cortex]({{< relref "renal_cortex" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
