+++
title = "Sickle cell disease"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   Sickle cell disease is a group of disorders that affects [hemoglobin]({{< relref "hemoglobin" >}})
    -   Sickling of [RBCs]({{< relref "red_blood_cell" >}}) causes RBCs to breakdown prematurely -> leads to [anemia]({{< relref "anemia" >}})


## Backlinks {#backlinks}


### 8 linked references {#8-linked-references}


#### [Treating sickle cell anemia]({{< relref "treating_sickle_cell_anemia" >}}) {#treating-sickle-cell-anemia--treating-sickle-cell-anemia-dot-md}

<!--list-separator-->

-  **🔖 Treatment**

    A single metric seems to be primary determinent of [SCD]({{< relref "sickle_cell_disease" >}}) severity -> **time taken for red blood cells to transit through capillaries of tissues relative to the delay time for HbS polymerization**

    ---

<!--list-separator-->

-  **🔖 Overview**

    [Sickle cell anemia]({{< relref "sickle_cell_disease" >}}) is an inherited disorder caused by a point mutation in the gene that encodes the beta-globin chane of hemoglobin

    ---


#### [FoCS (Sickle Cell Case)]({{< relref "focs_sickle_cell_case" >}}) {#focs--sickle-cell-case----focs-sickle-cell-case-dot-md}

<!--list-separator-->

-  **🔖 Clinical features of SCD**

    [SCD]({{< relref "sickle_cell_disease" >}}) usually manifests **early in childhood**

    ---

<!--list-separator-->

-  Clinical features of [SCD]({{< relref "sickle_cell_disease" >}})

    -   [SCD]({{< relref "sickle_cell_disease" >}}) usually manifests **early in childhood**
        -   Infants are protected largely by **elevated levels of Hb F**
            -   Soon after, condition becomes evident

    <!--list-separator-->

    -  Vaso-occlusive complications/events

        -   **Vaso-occlusive pain episodes are the most frequent cause of recurrent morbidity** and account for majority of SCD-related hospital admissions as well as school and work absences
        -   **[Dactylitis]({{< relref "dactylitis" >}}) is often the earliest manifestation of SCD**
            -   Occurs in infants and children
        -   **Splenic sequestration and infarction**
            -   Historically most children with [Hb]({{< relref "hemoglobin" >}}) S/S or S/[beta-thalassemia]({{< relref "beta_thalassemia" >}}) will have a dysfunctional spleen w/in first year of life and complete auto-infarction and atrophy due to ischemia of the spleen by five years
        -   **Infection**
            -   Young children with SCD and splenic dysfunction are at high risk for **septicemia and meningitis** among other infections
        -   **[Acute chest syndrome]({{< relref "acute_chest_syndrome" >}}) (ACS)**
            -   Major cause of mortality

    <!--list-separator-->

    -  Neurologic complications

        -   **Ischemic strokes**
            -   Overt strokes occur in as many as 11% of children with SCD, with peak occurrence between ages two and nine years
            -   Recurring strokes occur in 50%-70% of affected individuals within three years after first event
        -   **Silent cerebral infarcts**

    <!--list-separator-->

    -  Complications related to hemolysis

        -   Hyper-hemolysis syndrome
        -   Chronic anemia
        -   Jaundice and cholelithiasis
        -   Predisposition to aplastic crisis
        -   Pulmonary hypertension
        -   Priapism
        -   Cardiopulmonary complications

<!--list-separator-->

-  [Pathophysiology]({{< relref "sickle_cell_disease" >}})

    -   HbS arises from a mutation substituting **thymine** for **adenine** in the sixth codon of the beta-chain gene, GAG to GTG
        -   Causes coding of **valine** instead of **glutamate** in position 6 of the Hb beta chain
        -   The resulting Hb has the physical properties of forming polymers _only in deoxy conditions_
            -   In presence of O<sub>2</sub>, liquid state prevails
    -   **When [RBCs]({{< relref "red_blood_cell" >}}) containing homozygous HbS are exposed to deoxy conditions, the sickling process begins**
        -   A slow and gradual polymer formation ensues
        -   Repeated and prolonged sickling involves the membrane
        -   After recurrent episodes of sickling, membrane damage occurs and the cells are no longer capable of resuming regular form upon reoxygenation -> _Irreversibly Sickled Cells (ISCs)_
            -   5-50% of RBCs are ISCs
    -   **When RBCs sickle, they gain Na<sup>+</sup> and lose K<sup>​+</sup>**
        -   Membrane **permeability to Ca<sup>2+</sup> increases** possibly due to \*impairment in the Ca<sup>2​+</sup> pump that depends on [ATPase]({{< relref "adenosine_triphosphatase" >}})
            -   Intracellular [Ca<sup>2+</sup>] rises to 4x the reference level
            -   **Membrane becomes more rigid**, although unclear whether this is due to calcium
    -   Sickling may be precipated by multiple factors:
        -   Local tissue hypoxia
        -   Dehydration secondary to a viral illness
        -   Nausea and vomiting
        -   **All lead to hypertonicity of the plasma -> induce sickling**
        -   **Any event that can lead to acidosis -> sickling**
            -   E.g. infection or extreme dehydration
        -   Also caused by benign factors and environmental changes
            -   E.g. fatigue, exposure to cold, and psychosocial stress

<!--list-separator-->

-  **🔖 Genetics**

    Mutations in the _HBB_ gene cause [sickle cell disease]({{< relref "sickle_cell_disease" >}})

    ---

<!--list-separator-->

-  **🔖 Introduction**

    A particularly serious complication of [sickle cell disease]({{< relref "sickle_cell_disease" >}}) is **high blood pressure** in the blood vessels that supply the lungs (pulmonary hypertension)

    ---

<!--list-separator-->

-  **🔖 Introduction**

    [Sickle cell disease]({{< relref "sickle_cell_disease" >}}) is a group of disorders that affects [hemoglobin]({{< relref "hemoglobin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
