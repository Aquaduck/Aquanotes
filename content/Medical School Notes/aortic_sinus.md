+++
title = "Aortic sinus"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A space immediately above the [aortic valve]({{< relref "aortic_valve" >}}), between an aortic wall leaflet and the wall of the [ascending aorta]({{< relref "ascending_aorta" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology**

    <!--list-separator-->

    -  [Aortic sinus]({{< relref "aortic_sinus" >}})

        -   A space immediately above the aortic valve, between an aortic wall leaflet and the wall of the ascending aorta
        -   There are **three** aortic sinuses:
            1.  Left aortic sinus
            2.  Right aortic sinus
            3.  Posterior aortic sinuses


### Unlinked references {#unlinked-references}

[Show unlinked references]
