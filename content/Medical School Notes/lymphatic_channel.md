+++
title = "Lymphatic channel"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name and describe the pathways of lymphatic drainage (in a lymph node, in the body).]({{< relref "name_and_describe_the_pathways_of_lymphatic_drainage_in_a_lymph_node_in_the_body" >}}) {#name-and-describe-the-pathways-of-lymphatic-drainage--in-a-lymph-node-in-the-body--dot--name-and-describe-the-pathways-of-lymphatic-drainage-in-a-lymph-node-in-the-body-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymph**

    Lymph gradually moves **centrally** in [lymphatic channels]({{< relref "lymphatic_channel" >}}) where it flows into the [vascular system]({{< relref "vascular_system" >}}) at the **base of the neck**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
