+++
title = "Inherited colorectal cancer"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Lynch syndrome (hereditary nonpolyposis colorectal cancer): Clinical manifestations and diagnosis]({{< relref "lynch_syndrome_hereditary_nonpolyposis_colorectal_cancer_clinical_manifestations_and_diagnosis" >}}) {#lynch-syndrome--hereditary-nonpolyposis-colorectal-cancer--clinical-manifestations-and-diagnosis--lynch-syndrome-hereditary-nonpolyposis-colorectal-cancer-clinical-manifestations-and-diagnosis-dot-md}

<!--list-separator-->

-  **🔖 Lynch syndrome > Introduction**

    Lynch syndrome is the most common cause of [inherited colorectal cancer]({{< relref "inherited_colorectal_cancer" >}}) (CRC)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
