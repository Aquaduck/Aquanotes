+++
title = "Nutrient canal"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Travels through the [Diaphysis]({{< relref "diaphysis" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Parts of a tubular bone**

    [Nutrient canal]({{< relref "nutrient_canal" >}}) goes through the cortex of the [Diaphysis]({{< relref "diaphysis" >}}) -> blood vessel travels to nourish the marrow cavity

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
