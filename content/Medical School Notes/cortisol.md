+++
title = "Cortisol"
author = ["Arif Ahsan"]
date = 2021-09-07T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Glycogen synthase]({{< relref "glycogen_synthase" >}}) {#glycogen-synthase--glycogen-synthase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Cortisol]({{< relref "cortisol" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
