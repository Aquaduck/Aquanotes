+++
title = "Osmosis - Population genetics"
author = ["Arif Ahsan"]
date = 2021-07-19T00:00:00-04:00
tags = ["medschool", "videos", "osmosis", "source"]
draft = false
+++

link: <https://www.osmosis.org/learn/Mendelian%5Fgenetics%5Fand%5Fpunnett%5Fsquares>


## Notes - Population [Genetics]({{< relref "genetics" >}}) {#notes-population-genetics--genetics-dot-md}


### Mendelian genetics & punnett squares {#mendelian-genetics-and-punnett-squares}

-   _Genetics_: science of inheritance
-   Parental generation ("P") -> 1st filial generation ("F1") -> 2nd filial generation ("F2")
-   _Homozygous_: male, female alleles are same
-   _Heterozygous_: male, female alleles differ
-   _Phenotype_: observable trait from genotype


#### Mendel's laws {#mendel-s-laws}

-   _Law of segregation_: alleles segregate -> offspring acquire one allele from each parent
-   _Law of dominance_: alleles can be dominant/recessive
-   _Law of independent assortment_: separate genes assort independently
    -   _Genetic linkage_: proximity of genes on chromosome can cause joint assortment


#### Punnett square {#punnett-square}

-   _Punnett square_: table showing possible combinations of genotypes

{{< figure src="/ox-hugo/_20210719_192620screenshot.png" width="500" >}}

{{< figure src="/ox-hugo/_20210719_192643screenshot.png" width="500" >}}


### Inheritance patterns {#inheritance-patterns}


#### Dominant vs. recessive {#dominant-vs-dot-recessive}

-   _Dominant inheritance_: mutation affects dominant allele -> one copy causes disease
-   _Recessive inheritance_: mutation affects recessive allele -> two copies cause disease


#### Autosomal vs. sexual vs. mitochondrial patterns {#autosomal-vs-dot-sexual-vs-dot-mitochondrial-patterns}

-   _Autosomal inheritance_: mutation affects somatic chromosome
-   _Sexual inheritance_: mutation affects sex chromosome; X-linked/Y-linked
-   _Mitochondrial inheritance_: mutation on egg's mitochondrial DNA

<!--list-separator-->

-  Autosomal inheritance

    {{< figure src="/ox-hugo/_20210719_193703screenshot.png" caption="Figure 1: Autosomal dominant inheritance when crossed with heterozygous and homozygous recessive" >}}

    {{< figure src="/ox-hugo/_20210719_193722screenshot.png" caption="Figure 2: Autosomal recessive inheritance. Punnett square demonstrating probabilities of healhty, disease, and carrier genotypes in the offspring when two healthy carriers reproduce" >}}

    {{< figure src="/ox-hugo/_20210719_193740screenshot.png" caption="Figure 3: Autosomal recessive inheritance. When one affected and one unaffected individual reproduce, all offspring are carriers" >}}

<!--list-separator-->

-  Sex-linked inheritance

    -   Males have one allele for genes on X, Y chromosomes -> _hemizygous_
    -   Females have two alleles for genes on X chromosomes (homoxygous/heterozygous)

    <!--list-separator-->

    -  X-Linked dominant inheritance

        e.g. fragile X syndrome

        -   Dominant hemizygotes, dominant homozygotes, and heterozygotes have disease
        -   **Males** reproducing w/ healthy females have **100% chance to pass onto female children and 0% chance to pass onto male children**
        -   **Females** reproducing w/ healthy males have **50% chance to pass onto children of both sexes**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Genetics > 2. Use a Punnett square to predict the results of a monohybrid cross, stating the phenotypic and genotypic ratios of the F2 generation**

    [Osmosis - Population genetics]({{< relref "osmosis_population_genetics" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
