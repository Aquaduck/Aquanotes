+++
title = "Hematopoietic stem cell"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Found in [bone marrow]({{< relref "bone_marrow" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Summary of actions of B and T lymphocytes**

    Both [T-cells]({{< relref "t_lymphocyte" >}}) and [B-cells]({{< relref "b_lymphocyte" >}}) derived from [hematopoietic stem cells]({{< relref "hematopoietic_stem_cell" >}}) of the bone marrow

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
