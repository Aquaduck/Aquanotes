+++
title = "Zymogen"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   An inactive precursor to an [enzyme]({{< relref "enzyme" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
