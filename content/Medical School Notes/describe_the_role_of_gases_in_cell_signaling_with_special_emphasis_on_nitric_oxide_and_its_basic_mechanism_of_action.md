+++
title = "Describe the role of gases in cell signaling with special emphasis on nitric oxide and its basic mechanism of action."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Gases as second messengers - [NO]({{< relref "nitric_oxide" >}}) {#gases-as-second-messengers-no--nitric-oxide-dot-md}

-   NO is synthesized by certain cells (e.g. epithelial) from arginine
-   Three (of many) physiological responses to elevated levels of NO:
    1.  Smooth muscle relaxation
    2.  Neurotransmission in CNS
    3.  Cell-mediated immune response


#### Mechanism of action {#mechanism-of-action}

1.  Signal binds to receptor
2.  Receptor activates [NO synthase]({{< relref "nitric_oxide_synthase" >}}) -> synthesizes NO
3.  NO diffuses to neighboring cells across membranes
4.  NO binds to _guanylyl cyclase_ -> activation of cGMP pathway
5.  Triggers smooth muscle relaxation


### [CO]({{< relref "carbon_monoxide" >}}) {#co--carbon-monoxide-dot-md}

-   CO is a physiologically important signal molecule and is thought to work in the same way as nitric oxide


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the role of gases in cell signaling with special emphasis on nitric oxide and its basic mechanism of action.]({{< relref "describe_the_role_of_gases_in_cell_signaling_with_special_emphasis_on_nitric_oxide_and_its_basic_mechanism_of_action" >}})


#### [Discuss the concept of mechanochemical signal transduction in biology.]({{< relref "discuss_the_concept_of_mechanochemical_signal_transduction_in_biology" >}}) {#discuss-the-concept-of-mechanochemical-signal-transduction-in-biology-dot--discuss-the-concept-of-mechanochemical-signal-transduction-in-biology-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Mechano-chemical signal transduction**

    Increased shear stress or wall tension -> NO production -> Smooth muscle relaxation as discussed [here]({{< relref "describe_the_role_of_gases_in_cell_signaling_with_special_emphasis_on_nitric_oxide_and_its_basic_mechanism_of_action" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
