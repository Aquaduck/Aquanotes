+++
title = "Human immunodeficiency virus"
author = ["Arif Ahsan"]
date = 2021-08-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [HIV Infection and AIDS]({{< relref "hiv_infection_and_aids" >}}) {#hiv-infection-and-aids--hiv-infection-and-aids-dot-md}

<!--list-separator-->

-  **🔖 Practice essentials > Manifestations of HIV include the following:**

    Asymptomatic phase of [HIV]({{< relref "human_immunodeficiency_virus" >}}) is generally benign

    ---

<!--list-separator-->

-  **🔖 Practice essentials**

    [HIV]({{< relref "human_immunodeficiency_virus" >}}) disease is caused by infection with [HIV-1]({{< relref "hiv_1" >}}) or [HIV-2]({{< relref "hiv_2" >}}), which are retroviruses in the _Retroviridae_ family, _Lentivirus_ genus

    ---

<!--list-separator-->

-  **🔖 Practice essentials**

    [Human immunodeficiency virus]({{< relref "human_immunodeficiency_virus" >}}) (HIV) is a blood-borne virus typically transmitted via:

    ---


#### [HIV-2]({{< relref "hiv_2" >}}) {#hiv-2--hiv-2-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [HIV]({{< relref "human_immunodeficiency_virus" >}})

    ---


#### [HIV-1]({{< relref "hiv_1" >}}) {#hiv-1--hiv-1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [HIV]({{< relref "human_immunodeficiency_virus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
