+++
title = "Purkinje fiber"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   In the [Heart]({{< relref "heart" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Purkinje fibers]({{< relref "purkinje_fiber" >}}) can be found between the [endocardium]({{< relref "endocardium" >}}) and [myocardium]({{< relref "myocardium" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
