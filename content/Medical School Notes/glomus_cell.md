+++
title = "Glomus cell"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Type 2 glomus cell]({{< relref "type_2_glomus_cell" >}}) {#type-2-glomus-cell--type-2-glomus-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [glomus cell]({{< relref "glomus_cell" >}})

    ---


#### [Type 1 glomus cell]({{< relref "type_1_glomus_cell" >}}) {#type-1-glomus-cell--type-1-glomus-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [glomus cell]({{< relref "glomus_cell" >}})

    ---


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Carotid & Aortic bodies (p. 671)**

    Each carotid/aortic body (_glomus_) contains islands of two types of cells ([glomus cells]({{< relref "glomus_cell" >}})) surrounded by fenestered sinusoidal capillaries

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
