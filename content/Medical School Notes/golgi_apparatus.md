+++
title = "Golgi apparatus"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   An [organelle]({{< relref "organelle" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles**

    <!--list-separator-->

    -  [Golgi apparatus]({{< relref "golgi_apparatus" >}}) (complex)

        <!--list-separator-->

        -  Structure

            -   Consists of several membrane-bounded _cisternae_ arranged in a stack
                -   _Cisternae_: disk-shaped and slightly curved, w/ flat centers and dilated rims
                    -   Size and shape vary
                    -   **Integral to packaging and modification processes that occur in the Golgi apparatus**
                -   Positioned and held in place by microtubules
            -   A distinct polarity exists across Golgi stack w/ many vesicles on one side and vacuoles on the other

        <!--list-separator-->

        -  Regions

            1.  **Cis face** lies deep in cell toward nucleus next to RER
                -   Outermost cisterna associated w/ _vesicular-tubular clusters (VTC)_
                    -   Network of interconnected tubes and vesicles
            2.  **Medial compartment** composed of several cisternae lying between the cis and trans faces
            3.  **Trans face** lies at the side of the stack facing the plasma membrane
                -   Associated w/ vacuoles and secretory granules
            4.  **trans-Golgi network** lies apart from the last cisterna at the trans face
                -   separated from the Golgi stack
                -   Sorts proteins for ther final destinations

        <!--list-separator-->

        -  Functions

            -   Processes membrane-packed proteins synthesized in the RER and also recycles and redistributes membranes


#### [Chylomicron retention disease]({{< relref "chylomicron_retention_disease" >}}) {#chylomicron-retention-disease--chylomicron-retention-disease-dot-md}

<!--list-separator-->

-  **🔖 From Molecular analysis and intestinal expression of SAR1 genes and proteins in Anderson's disease (Chylomicron retention disease)**

    A small GTPase involved [COPII]({{< relref "cop_ii" >}})-dependent transport of proteins from the [ER]({{< relref "endoplasmic_reticulum" >}}) to the [Golgi]({{< relref "golgi_apparatus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
