+++
title = "Describe the etiology of pleuritis."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### [Pleuritis]({{< relref "pleuritis" >}}) {#pleuritis--pleuritis-dot-md}

-   During inspiration and expiration, sliding of normally smooth, moist pleurae makes no detectable sound during ascultation of lungs
-   Inflammatoin of pleura (pleuritic) makes lung surfaces rough
    -   Sounds like clump of hairs being rolled between fingers
-   Inflamed surfaces of pleura may also cause parietal and visceral layers of pleura to adhere (pleural adhesion)
-   Acute pleuritis marked by **sharp, stabbing pain especially on exertion**
-   Typically a result of infection, [cancer]({{< relref "cancer" >}}), and [congestive heart failure]({{< relref "congestive_heart_failure" >}})
-   Typical complaints: chest pain, SOB, and pain in shoulder


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the etiology of pleuritis.]({{< relref "describe_the_etiology_of_pleuritis" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
