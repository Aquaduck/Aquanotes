+++
title = "Inbox"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool"]
draft = false
+++

This is where I put notes that I want to write down, but don't have a relevant learning objective for yet. Usually occurs if I find something in my anki deck that is ahead of my currently learned material.


## From Anki {#from-anki}


### Cardiology {#cardiology}

-   Percentage of [blood]({{< relref "blood" >}}) supply:
    -   The abdomen receives 25% of blood supply
    -   The brain receives 15% of blood supply
-   Cardiac action potentials last 300 milliseconds
-   [Purkinje fibers]({{< relref "purkinje_fiber" >}}) can be found between the [endocardium]({{< relref "endocardium" >}}) and [myocardium]({{< relref "myocardium" >}})
-   Ejection phase lasts 0.25 seconds
-   [Stroke volume]({{< relref "stroke_volume" >}}) = volume of [blood]({{< relref "blood" >}}) ejected from [left ventricle]({{< relref "left_ventricle" >}}) per heartbeat
-   The measurement of [systolic]({{< relref "systole" >}}) blood pressure is adequate to clinically measure [afterload]({{< relref "afterload" >}})
-   [Sodium]({{< relref "sodium" >}}) is a direct determinant of blood volume
-   Mechanoreceptors in heart collapsed = low BP
-   Arterial baroreceptors are found in the carotid sinus and aortic arch
-   The ejection can be used to estimate contractility
-   Conduction travels along the endocardial surface
-   Elastic arteries vs. normal arteries:
    -   Thicker tunica intima
    -   Thinner [tunica adventitia]({{< relref "tunica_adventitia" >}}) that also contains collagen, elastic fibers, and [vasa vasorum]({{< relref "vasa_vasorum" >}})
-   [Tunica externa]({{< relref "tunica_adventitia" >}}) made up of:
    -   Nerve fibers
    -   Lymphatic vessels
    -   Elastin
    -   Collagen
-   Law of Laplace: Wall Stress = \\(\frac{P x R}{2W}\\)
    -   Where P = Pressure, R = radius, W = wall thickness
    -   Essentially the tension on the wall of a container depends on the pressure of the container's contents and the container's radius
-   Depolarization travels endocardium -> epicardium
-   Repolarization travels epicardium -> endocardium
-   Reynold's number determines laminar vs turbulent flow
    -   Formula: \\(Re = \frac{2rvd}{\eta}\\)
-   _Cardiac length-tension relationship_: Sarcomere Length/End Diastolic Volume vs. Force developed w/in the ventricles
-   Epinephrine binds to B2 -> vasodilation + increased blood flow in blood vessel
-   Factors that contribute to vascular resistance:
    -   Increased viscosity = increased R
    -   Increased length = increased R
    -   Increased radius = decreased R
-   [Terminal cisternae]({{< relref "terminal_cisternae" >}})
    -   Enlarged areas of [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) surrounding [t tubules]({{< relref "transverse_tubule" >}})
        -   They are the ends of [L tubules]({{< relref "l_tubule" >}})
    -   Store [Calcium]({{< relref "calcium" >}}) and release it when an action potential courses down the [t tubules]({{< relref "transverse_tubule" >}})
-   [Atrial septal defect]({{< relref "atrial_septal_defect" >}}) (ASD)
    -   Causes:
        1.  Apoptosis of septum primum -> too short
        2.  Foramen ovale stays open because the secundum and primum never meet up
        3.  Complete absence of septum primum and septum secundum
-   [Catecholamine]({{< relref "catecholamine" >}}) ([norepinephrine]({{< relref "norepinephrine" >}})) binding to an [alpha-1 receptor]({{< relref "alpha_1_receptor" >}}) -> vasoconstriction -> decreased blood flow
-   [EDV]({{< relref "end_diastolic_volume" >}}) and [venous return]({{< relref "venous_return" >}}) are used to estimate [preload]({{< relref "preload" >}}) clinically
-   Why is phase 0 upstroke of an SA node slow?
    -   [L-type calcium channels]({{< relref "l_type_calcium_channel" >}}) have slow kinetics
    -   There are relatively few [L-type calcium channels]({{< relref "l_type_calcium_channel" >}}) in nodal cell sarcolemma
-   [Methemoglobin]({{< relref "methemoglobin" >}}): [Iron]({{< relref "iron" >}}) is ferric (3+) instead of ferrous (2+)
-   [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}}) is associated with [DiGeorge syndrome]({{< relref "digeorge_syndrome" >}})
-   [Ductus arteriosus]({{< relref "ductus_arteriosus" >}}): connects [pulmonary artery]({{< relref "pulmonary_artery" >}}) and [aorta]({{< relref "aorta" >}})
-   [Adenosine]({{< relref "adenosine" >}}) is the main metabolic vasodilator
-   MTHFR testing can be used to evaluate the cause of elevated homocysteine levels
    -   Can help determine risk for thrombosis or premature cardiovascular disease (CVD)
-   Aortic stenosis and chronic hypertension are examples of pathologic conditions that affect [afterload]({{< relref "afterload" >}})
-   Brain cells maintain equal tonicity through the use of osmolytes to regulate tonicity between the intraceullar and extracellular space despite hypo/hypertonicity
-   Heart sound is split during [inspiration]({{< relref "inspiration" >}}) because the [pulmonic valve]({{< relref "pulmonary_valve" >}}) closing is slightly later than [aortic valve]({{< relref "aortic_valve" >}}) closing


### Embryology {#embryology}


#### Cardiac {#cardiac}

-   [Septum transversum]({{< relref "septum_transversum" >}}) leads to [diaphragm]({{< relref "diaphragm" >}}) and liver cords
-   Superior portion of cardinal vein -> left brachiocephalic vein


#### Respiratory {#respiratory}

-   Stages of [lung]({{< relref "lung" >}}) maturation:
    1.  Embryonic: 0-6 weeks
    2.  Pseudoglandular: 6-16 weeks
    3.  Canalicular: 17-28 weeks
    4.  Saccular: 28-32 weeks
    5.  Alveolar: 32-36 weeks


### Immunology {#immunology}

-   [Marginal sinus]({{< relref "marginal_sinus" >}}) is where afferent vessels enter the [lymph node]({{< relref "lymph_node" >}})


### Metabolism {#metabolism}

-   [Malonyl-CoA]({{< relref "malonyl_coa" >}}) is the rate-limiting step of fatty acid synthesis
-   [DPG]({{< relref "2_3_biphosphoglycerate" >}}) increases at higher altitudes to unload more oxygen to tissues
-   [G6P]({{< relref "glucose_6_phosphate" >}}) binds to [glycogen synthase]({{< relref "glycogen_synthase" >}}) B -> better substrate for [Phosphoprotein phosphatase]({{< relref "phosphoprotein_phosphatase" >}}) -> activated glycogen synthase A


### Respiration/Ventilation {#respiration-ventilation}

-   J-receptors are found in the lungs and around blood vessels
    -   Respond to distortions caused by fluid or stiffness
        -   Leads to rapid, shallow breathing
-   Respiratory coefficient = percent of oxygen converted to carbon dioxide
    -   Normal value = 0.8
        -   Other 0.2 used for water and other molecules
-   _Transpulmonary Pressure (TPP) = Alveoli pressure - intrapleural pressure (IPP)_
-   _FRC_: Functional residual capacity
-   _PVR_: Pulmonary vascular resistance
-   _DLCO_: Diffusing capacity for carbon monoxide
    -   Measures capacity of lungs to transfer gas from inhaled air to RBCs in pulmonary arteries
-   Anything that causes [hyperventilation]({{< relref "hyperventilation" >}}) leads to [respiratory alkalosis]({{< relref "respiratory_alkalosis" >}})
-   [Tidal volume]({{< relref "tidal_volume" >}}): the amount of air we breath in and out with every resting breath


## From Osmosis {#from-osmosis}


### [Fick's law of diffusion]({{< relref "fick_s_law_of_diffusion" >}}) {#fick-s-law-of-diffusion--fick-s-law-of-diffusion-dot-md}

-   Describes diffusion of gases
-   Formula: \\(V\_{x} = \frac{DA\Delta P}{\Delta x}\\) where:
    -   _V<sub>x</sub>_: volume of gas transferred per unit time
    -   _D_: gas diffusion coefficient
    -   _A_: surface area
    -   _ΔP_: partial pressure difference of gas
    -   _Δx_: membrane thickness
-   The **driving force of gas diffusion is ΔP across the membrane**
    -   **NOT** the concentration difference
-   _D_ dramatically affects diffusion rate
    -   e.g. diffusion coefficient for CO<sub>2</sub> is ~20x greater than that of O<sub>2</sub>


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
