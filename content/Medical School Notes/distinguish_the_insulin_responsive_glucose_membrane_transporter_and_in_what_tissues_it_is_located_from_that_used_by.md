+++
title = "Distinguish the insulin-responsive glucose membrane transporter and in what tissues it is located, from that used by:"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

[Overview of the most important glucose transporters]({{< relref "overview_of_the_most_important_glucose_transporters" >}})
The insulin-responsive glucose membrane transporter is [GLUT-4]({{< relref "glut_4" >}}) and is found in:

-   Adipose tissue
-   Striated muscle
    -   Skeletal muscle
    -   Heart muscle


## Brain {#brain}

-   Uses [GLUT-1]({{< relref "glut_1" >}}) and [GLUT-3]({{< relref "glut_3" >}})


## Liver/Pancreas {#liver-pancreas}

-   Uses [GLUT-2]({{< relref "glut_2" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Distinguish the insulin-responsive glucose membrane transporter and in what tissues it is located, from that used by:]({{< relref "distinguish_the_insulin_responsive_glucose_membrane_transporter_and_in_what_tissues_it_is_located_from_that_used_by" >}})

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Brain

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Liver & Pancreas


### Unlinked references {#unlinked-references}

[Show unlinked references]
