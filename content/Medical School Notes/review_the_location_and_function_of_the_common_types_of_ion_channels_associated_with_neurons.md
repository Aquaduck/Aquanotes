+++
title = "Review the location and function of the common types of ion channels associated with neurons."
author = ["Arif Ahsan"]
date = 2021-09-11T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Forehand, Action Potential]({{< relref "forehand_action_potential" >}}) {#from-forehand-action-potential--forehand-action-potential-dot-md}


### Non-gated ion channels {#non-gated-ion-channels}


#### Function {#function}

-   Always open
-   Responsible for **the influx of Na<sup>+</sup> and K<sup>​+</sup>** when the neuron is in its **resting state**
    -   Important for **establishing resting membrane potential**


#### Location {#location}

-   Found **throughout the neuron**


### Ligand-gated ion channels {#ligand-gated-ion-channels}


#### Function {#function}

-   Directly or indirectly activated by the **binding of chemical neurotransmitters ot membrane receptors**
-   The receptor itself can form part of the ion channel or it may be coupled to the channel via a G-protein and second messenger
-   Chemical transmitters bind to receptors -> associated ion channel opens/closes to permit/block the movement of **specific ions** across the cell membrane


#### Location {#location}

-   **Sites of synaptic contact**
    -   Dendritic spines
    -   Dendrites
    -   Somata


### Mechanically-gated channels {#mechanically-gated-channels}


#### Function {#function}

-   Open when force (stretch) is applied across a membrane
    -   Close when force is removed


#### Location {#location}

-   **Specialized sensory cells**
-   **Peripheral branches** of **pseudounipolar sensory neurons**


### Voltage-gated ion channels {#voltage-gated-ion-channels}


#### Function {#function}

-   Sensitive to voltage difference across the membrane
-   **In initial resting state channels are typically closed**
    -   Open when a critical voltage level is reached
-   Required for **initiation and propogation of action potentials** or **neurotransmitter release**


#### Location {#location}

-   **Axons** and **axon terminals**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-10 Fri] </span></span> > Passive Membrane Properties, the Action Potential, and Electrical Signaling by Neurons > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Review the location and function of the common types of ion channels associated with neurons.]({{< relref "review_the_location_and_function_of_the_common_types_of_ion_channels_associated_with_neurons" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
