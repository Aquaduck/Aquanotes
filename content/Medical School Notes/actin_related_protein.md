+++
title = "Actin related protein"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   45% identical to [actin]({{< relref "actin" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Arp 2/3 complex]({{< relref "arp_2_3_complex" >}}) {#arp-2-3-complex--arp-2-3-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An [actin-related protein]({{< relref "actin_related_protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
