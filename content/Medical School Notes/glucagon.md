+++
title = "Glucagon"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Protein kinase A]({{< relref "protein_kinase_a" >}}) {#protein-kinase-a--protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Activated by [glucagon]({{< relref "glucagon" >}})

    ---


#### [Osmosis - Gluconeogenesis]({{< relref "osmosis_gluconeogenesis" >}}) {#osmosis-gluconeogenesis--osmosis-gluconeogenesis-dot-md}

<!--list-separator-->

-  **🔖 Notes > Fat metabolism**

    _Pancreatic \\(\alpha\\) cells_ sense blood glucose levels decreasing -> release [glucagon]({{< relref "glucagon" >}})

    ---


#### [Name the second messenger produced in response to glucagon-induced cellular signaling and how it functions to elicit the appropriate intracellular response]({{< relref "name_the_second_messenger_produced_in_response_to_glucagon_induced_cellular_signaling_and_how_it_functions_to_elicit_the_appropriate_intracellular_response" >}}) {#name-the-second-messenger-produced-in-response-to-glucagon-induced-cellular-signaling-and-how-it-functions-to-elicit-the-appropriate-intracellular-response--name-the-second-messenger-produced-in-response-to-glucagon-induced-cellular-signaling-and-how-it-functions-to-elicit-the-appropriate-intracellular-response-dot-md}

[cAMP]({{< relref "camp" >}}) is the "second messenger" = the first intracellular effector formed in response to the first messenger [glucagon]({{< relref "glucagon" >}})

---


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    <!--list-separator-->

    -  Reversal of [glucagon]({{< relref "glucagon" >}})'s actions

        -   Phosphate groups added to [PKA]({{< relref "protein_kinase_a" >}}) substrates are removed by protein [phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) via hydrolytic cleavage of phosphate esters
            -   Ensures that changes in protein activity induced by phosphorylation are **reversible**
        -   [cAMP]({{< relref "camp" >}}) can be rapidly hydrolyzed to 5'-AMP by [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}}) through cleavage of cyclic 3',5'-phosphodiester bond
            -   5'-AMP is **not** a signaling molecule -> cAMP affects cease

<!--list-separator-->

-  **🔖 Second messenger systems**

    <!--list-separator-->

    -  [Glucagon]({{< relref "glucagon" >}})'s metabolic effects

        -   IV administration of glucagon -> **immediate rise** in blood glucose
            -   Due to immediate increase in degradation of liver glycogen + later increase in hepatic gluconeogenesis
        -   Glucagon's binding to its hepatic receptor -> **immediate** initiation of glycogenolysis
            -   This is because [PKA]({{< relref "protein_kinase_a" >}}) phosphorylates and activates a key glycogen-degrading enzyme ([glycogen phosphorylase kinase]({{< relref "glycogen_phosphorylase_kinase" >}})), and also phosphorylates and inactivates the rate-limiting enzyme in glycogen synthesis ([glycogen synthase]({{< relref "glycogen_synthase" >}}))
        -   [PKA]({{< relref "protein_kinase_a" >}})-mediated phosphorylation of [CREB]({{< relref "camp_response_element_binding_protein" >}}) -> increased transcription of [gluconeogenic]({{< relref "gluconeogenesis" >}}) enzymes if liver glycogen stores become depleted
            -   Pyruvate carboxylase
            -   PEP carboxykinase
            -   F1,6bPase
        -   [PKA]({{< relref "protein_kinase_a" >}}) also phosphorylates and inactivates the liver pyruvate kinase isoenzyme -> ensures that when PEP is formed from pyruvate, it remains a substrate for enolase
        -   Glucagon affects lipid metabolism in liver by initiating the PKA-catalyzed phosphorylation and inactivation of [acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}}) (catalyzes rate-limiting and committed step in faty acid synthesis)
            -   This is required to prevent formation of [malonyl-CoA]({{< relref "malonyl_coa" >}}) (inhibits entry of long-chain fatty acids into mitochondria by blocking [CPT1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}), the first enzyme in the carnitine shuttle)
            -   Promotes the uptake of adipose-derived fatty acids into liver mitochondria -> ensures their oxidation + subsequent formation of ATP required to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})
        -   Glucagon affects gluconeogenic process by **increasing liver's uptake of amino acids supplied by muscle breakdown** -> increases availability of carbon skeletons for [gluconeogenesis]({{< relref "gluconeogenesis" >}})
            -   As a consequence, **plasma concentrations of amino acids are decreased**

        <!--list-separator-->

        -  Summary of glucagon's overall metabolic effects on liver

            1.  ↑ Glycogenolysis
            2.  ↓ Glycogen synthesis
            3.  ↑ Gluconeogenesis
            4.  ↑ Amino acid uptake
            5.  ↓ Fatty acid synthesis
            6.  ↑ Fatty acid oxidation

        <!--list-separator-->

        -  Reversal of [glucagon]({{< relref "glucagon" >}})'s actions

            -   Phosphate groups added to [PKA]({{< relref "protein_kinase_a" >}}) substrates are removed by protein [phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) via hydrolytic cleavage of phosphate esters
                -   Ensures that changes in protein activity induced by phosphorylation are **reversible**
            -   [cAMP]({{< relref "camp" >}}) can be rapidly hydrolyzed to 5'-AMP by [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}}) through cleavage of cyclic 3',5'-phosphodiester bond
                -   5'-AMP is **not** a signaling molecule -> cAMP affects cease

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase**

    [Glucagon]({{< relref "glucagon" >}}) antagonizes [insulin]({{< relref "insulin" >}})'s effects

    ---

<!--list-separator-->

-  **🔖 Second messenger systems**

    [Glucagon]({{< relref "glucagon" >}}) (fasting state) and [epinephrine]({{< relref "epinephrine" >}}) (stress response) signal the most important second messenger system regulating the pathways of intermediary metabolism: **the adenylate cyclase system**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
