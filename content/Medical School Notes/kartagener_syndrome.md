+++
title = "Kartagener syndrome"
author = ["Arif Ahsan"]
date = 2021-09-15T00:00:00-04:00
tags = ["medschool", "learning_objective", "concept"]
draft = false
+++

## From [Kartagener's syndrome: A case series]({{< relref "kartagener_s_syndrome_a_case_series" >}}) {#from-kartagener-s-syndrome-a-case-series--kartagener-s-syndrome-a-case-series-dot-md}


### Introduction {#introduction}

-   **Kartagener's syndrome (KS)** is a subset of a larger group of ciliary motility disorders called [primary ciliary dyskinesias]({{< relref "primary_ciliary_dyskinesia" >}}) (PCDs)
    -   PCD w/ [situs inversus]({{< relref "situs_inversus" >}}) = Kartagener's syndrome
-   Autosomal recessive inheritance
-   Comprises the **triad** of situs inversus, bronchiectasis, and sinusitis


### Discussion {#discussion}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-08 Wed] </span></span> > Cytoskeleton > Session > Describe the cell biological defects underlying:**

    <!--list-separator-->

    -  [Kartagener syndrome]({{< relref "kartagener_syndrome" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
