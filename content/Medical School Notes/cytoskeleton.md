+++
title = "Cytoskeleton"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Discuss the concept of mechanochemical signal transduction in biology.]({{< relref "discuss_the_concept_of_mechanochemical_signal_transduction_in_biology" >}}) {#discuss-the-concept-of-mechanochemical-signal-transduction-in-biology-dot--discuss-the-concept-of-mechanochemical-signal-transduction-in-biology-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Mechano-chemical signal transduction > Tension affects cell shape**

    Mechanically distorting any one of a cell's elements -> changes in shape/mechanical forces throughout entire cell because of [cytoskeleton]({{< relref "cytoskeleton" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
