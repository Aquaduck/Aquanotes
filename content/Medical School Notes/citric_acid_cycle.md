+++
title = "Citric Acid Cycle"
author = ["Arif Ahsan"]
date = 2021-07-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   A cellular [metabolic]({{< relref "metabolism" >}}) process

{{< figure src="/ox-hugo/_20210729_185701screenshot.png" caption="Figure 1: The citric acid (Krebs) cycle. Each acetyl-CoA molecule generates 12 ATP." >}}


## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [Thiamine]({{< relref "thiamine" >}}) {#thiamine--thiamine-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry - Nutrition > Function**

    [α-ketoglutarate dehydrogenase]({{< relref "α_ketoglutarate_dehydrogenase" >}}) ([TCA cycle]({{< relref "citric_acid_cycle" >}}))

    ---


#### [Riboflavin]({{< relref "riboflavin" >}}) {#riboflavin--riboflavin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    e.g. [Succinate dehydrogenase]({{< relref "succinate_dehydrogenase" >}}) reaction in the [TCA cycle]({{< relref "citric_acid_cycle" >}})

    ---


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Connects [glycolysis]({{< relref "glycolysis" >}}) to the [TCA cycle]({{< relref "citric_acid_cycle" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  [TCA cycle]({{< relref "citric_acid_cycle" >}})

    -   [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)
    -   [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)
    -   [Acetyl-CoA]({{< relref "acetyl_coa" >}}) from CoA from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)
    -   [α-ketoglutarate DH]({{< relref "α_ketoglutarate_dehydrogenase" >}})
        -   Requires same coenzymes as Pyruvate DH complex


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  [Citric Acid Cycle]({{< relref "citric_acid_cycle" >}})

    <!--list-separator-->

    -  [Citrate synthase]({{< relref "citrate_synthase" >}})

        -   **Not allosterically regulated**
            -   [Citrate]({{< relref "citrate" >}}) itself inhibits the enzyme by competing with [oxaloacetate]({{< relref "oxaloacetate" >}})
        -   [Succinyl-CoA]({{< relref "succinyl_coa" >}}) is a strong inhibitor of citrate production
            -   Strictly competitive with [acetyl-CoA]({{< relref "acetyl_coa" >}})
            -   Noncompetitive with [oxaloacetate]({{< relref "oxaloacetate" >}})

    <!--list-separator-->

    -  NAD<sup>+</sup>-linked [isocitrate dehydrogenase]({{< relref "isocitrate_dehydrogenase" >}})

        -   Catalyzes the **rate limiting and committed step of the TCA cycle**
        -   Allosterically regulated by the _energy charge_ of the cell
            -   ATP inhibits
            -   ADP stimulates
        -   [ADP]({{< relref "adenosine_diphosphate" >}}) is the master energy sensor in the mitochondria
            -   Due to the absence of the appropriate adenylate cyclase isozyme
            -   **↑[ADP] signals an energy deficit**
                -   Because ADP is the substrate of ATP synthesis via ATP synthase and oxidative phosphorylation
            -   [AMP]({{< relref "adenosine_monophosphate" >}}) is master switch in the [cytoplasm]({{< relref "cytoplasm" >}})

    <!--list-separator-->

    -  NAD<sup>+</sup>-linked [α-ketoglutarate dehydrogenase]({{< relref "α_ketoglutarate_dehydrogenase" >}})

        -   Allostericaly regulated by **product inhibition**
            -   i.e. succinyl-CoA and NADH
        -   Mechanism is **analogous to that of the [pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}})**
            -   Consists of 3 enzymes
                -   _E1_ accepts a 5C acid rather than a 3C acid
                -   _E2_ and _E3_ are identical to those found in the pyruvate DH complex
                -   α-ketoglutarate DH is NOT subject to covalent modification by a tightly-associated kinase

    <!--list-separator-->

    -  NAD<sup>+</sup>-linked [malate dehydrogenase]({{< relref "malate_dehydrogenase" >}})

        -   Required for the **reversible oxidation of malate to oxaloacetate**
            -   Completes the TCA cycle
            -   **Critical reaction in gluconeogenesis**
                -   Malate - α-ketoglutarate transporter moves oxaloacetate from the mitosol to the cytosol as a **gluconeogenic precursor**

    <!--list-separator-->

    -  FAD-linked [Succinate dehydrogenase]({{< relref "succinate_dehydrogenase" >}})

        -   The **only FAD-linked dehydrogenase** in the TCA cycle
        -   Embedded in inner mitochondrial membrane -> also called _Complex II of ETC_
            -   Electrons generated by oxidation of succinate -> fumarate passed to [Coenzyme Q]({{< relref "coenzyme_q" >}})
                -   Therefore, Succinate DH **is regulated by the availability of FAD**


#### [Kaplan Biochemistry Chapter 10]({{< relref "kaplan_biochemistry_chapter_10" >}}) {#kaplan-biochemistry-chapter-10--kaplan-biochemistry-chapter-10-dot-md}

<!--list-separator-->

-  10.2 Reactions of the [Citric Acid Cycle]({{< relref "citric_acid_cycle" >}})

    -   Takes place in the **mitochondrial matrix**

    {{< figure src="/ox-hugo/_20210727_121307screenshot.png" caption="Figure 2: The Citric Acid Cycle" >}}

    <!--list-separator-->

    -  Key Reactions

        <!--list-separator-->

        -  1) Citrate Formation

            -   Reaction:
                1.  Acetyl-CoA and _oxaloacetate_ undergo condensation reaction -> form intermediate _citryl-CoA_
                2.  citryl-CoA hydrolyzed -> **citrate + CoA-SH**
                3.  Catalyzed by _citrate synthase_

            {{< figure src="/ox-hugo/_20210727_121505screenshot.png" caption="Figure 3: Citrate formation" >}}

        <!--list-separator-->

        -  2) Citrate Isomerized to Isocitrate

            -   Achiral citrate is isomerized to one of four possible isomers of _isocitrate_
            -   Reaction:
                1.  Citrate binds at three points to _[aconitase]({{< relref "enzyme" >}})_
                2.  Water is lost from citrate -> cis-_aconitate_
                3.  Water added back to form _isocitrate_
            -   Enzyme required Fe<sup>2+</sup>

            {{< figure src="/ox-hugo/_20210727_122245screenshot.png" caption="Figure 4: Citrate isomerized to isocitrate" >}}

        <!--list-separator-->

        -  3) \\(\alpha\\)-Ketoglutarate and CO<sub>2</sub> Formation

            -   Reaction:
                1.  Isocitrate oxidized to _oxalosuccinate_ by _[isocitrate dehydrogenase]({{< relref "enzyme" >}})_
                2.  Oxalosuccinate decarboxylated -> _\\(\alpha\\)-ketoglutarate_ and CO<sub>2</sub>
            -   **Isocitrate dehydrogenase is the rate-limiting enzyme of the citric acid cycle**
                -   **First of the two carbons is lost here**
                -   **First NADH produced**

            {{< figure src="/ox-hugo/_20210727_122909screenshot.png" caption="Figure 5: \\(\alpha\\)-Ketoglutarate and CO<sub>2</sub> Formation" >}}

        <!--list-separator-->

        -  4) Succinyl-CoA and CO<sub>2</sub> Formation

            -   Carried out by _\\(\alpha\\)-ketoglutarate dehydrogenase complex_
                -   **Similar to PDH Complex**
            -   **NAD<sup>+</sup> reduced to NADH**
            -   Reaction:
                1.  \\(\alpha\\)-ketoglutarate and CoA come together -> produce CO<sub>2</sub>
                    -   This is the **second and last carbon lost from the cycle**

            {{< figure src="/ox-hugo/_20210727_123245screenshot.png" caption="Figure 6: Succinyl-CoA and CO<sub>2</sub> Formation" >}}

        <!--list-separator-->

        -  5) Succinate Formation

            -   Reaction:
                1.  Hydrolysis of thioester bond on _succinyl-CoA_ yields _succinate_ and CoA-SH
                2.  Coupled to the phosphorylation of GDP to GTP
            -   Succinate formation catalyzed by _[succinyl-CoA synthetase]({{< relref "enzyme" >}})_
            -   _[nucleosidediphosphate kinase]({{< relref "enzyme" >}})_ catalyzes phosphate transfer from GTP to ADP -> producing ATP
            -   **Only time in CAC that ATP is produced directly**
                -   ATP produciton primarily done in within the [ETC]({{< relref "oxidative_phosphorylation" >}})

            {{< figure src="/ox-hugo/_20210727_123830screenshot.png" caption="Figure 7: Succinate formation" >}}

        <!--list-separator-->

        -  6) Fumarate Formation

            -   **Only step** of the CAC that **doesn't take place in the mitochondrial matrix**
                -   Occurs in the **inner membrane**
            -   Reaction:
                1.  Succinate oxidized -> _fumarate_
                    -   Catalyzed by _[succinate dehydrogenase]({{< relref "enzyme" >}})_
                        -   Considered a _flavoprotein_ because it is **covalently bonded to FAD** - electron acceptor in this reaction
                2.  **FAD reduced to FADH<sub>2</sub>**
                    -   NADH not used because succinate is not a good enough reducing agent to reduce NAD<sup>+</sup>
                3.  Each molecule of FADH<sub>2</sub> passes electrons to the [ETC]({{< relref "oxidative_phosphorylation" >}}) -> **production of 1.5 ATP**
                    -   NADH would give rise to 2.5 ATP

        <!--list-separator-->

        -  7) Malate Formation

            -   Reaction:
                1.  _[fumarase]({{< relref "enzyme" >}})_ catalyzes hydrolysis of **alkene bond in fumarate** -> _malate_

        <!--list-separator-->

        -  8) Oxaloacetate Formed Anew

            -   Reaction:
                1.  _[malate dehydrogenase]({{< relref "enzyme" >}})_ catalyzes the oxidation of malate to _oxaloacetate_
                2.  **NAD<sup>+</sup> reduced to NADH**

            {{< figure src="/ox-hugo/_20210727_124400screenshot.png" caption="Figure 8: The final steps of the Citric Acid Cycle" >}}

    <!--list-separator-->

    -  Net Results and ATP Yield

        -   Steps **3, 4, 8** each produce one NADH
        -   Step **6** forms one FADH<sub>2</sub>
        -   Step **5** yields one GTP -> ATP

        {{< figure src="/ox-hugo/_20210727_124552screenshot.png" caption="Figure 9: Total amount of chemical energy harvested per pyruvate" >}}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Cellular Metabolism > 11. Describe the function of the tricarboxylic acid (TCA) cycle**

    [Krebs]({{< relref "citric_acid_cycle" >}})

    ---


#### [Isocitrate dehydrogenase]({{< relref "isocitrate_dehydrogenase" >}}) {#isocitrate-dehydrogenase--isocitrate-dehydrogenase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Rate-limiting and commited step of the [TCA cycle]({{< relref "citric_acid_cycle" >}})

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Mitochondria > Enzymes and genetic apparatus**

    All of the enzymes of the [TCA]({{< relref "citric_acid_cycle" >}}) cycle in the matrix (except for succinate DH which is in the inner membrane)

    ---


#### [A Discussion of the Significance of Two Key Metabolic Branchpoints]({{< relref "a_discussion_of_the_significance_of_two_key_metabolic_branchpoints" >}}) {#a-discussion-of-the-significance-of-two-key-metabolic-branchpoints--a-discussion-of-the-significance-of-two-key-metabolic-branchpoints-dot-md}

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > Brain > Fasting state**

    Brain requires a **constant source of glucose for aerobic [glycolysis]({{< relref "glycolysis" >}}), ATP generation via [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}}), and replenishing lost [TCA]({{< relref "citric_acid_cycle" >}}) cycle intermediates**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
