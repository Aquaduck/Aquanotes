+++
title = "Khan - Steady states and the Michaelis Menten equation"
author = ["Arif Ahsan"]
date = 2021-07-13T00:00:00-04:00
tags = ["medschool", "khan", "source"]
draft = false
+++

Link:  <https://www.khanacademy.org/test-prep/mcat/biomolecules/enzyme-kinetics/v/steady-states-and-the-michaelis-menten-equation>


## Video {#video}

<iframe width="420" height="315"
src="https://youtu.be/7u2MkbsE_dw">
</iframe>


## Notes {#notes}


### Steady-state assumption {#steady-state-assumption}

-   [Enzyme]({{< relref "enzyme" >}}) kinematics equation:
    \\(E + S \overset{1}{\rightleftharpoons} ES \overset{2}{\rightleftharpoons} E + P\\)
    -   E = Enzyme
    -   S = Substrate
    -   P = Product
-   _Steady-state assumption_: [ES] is constant
    -   Formation of ES = Loss of ES
-   Therefore, \\(Rate\_{1} + Rate\_{-2} = Rate\_{-1} + Rate\_{2}\\)
-   Products rarely go back to reactants because these reactions are usually thermodynamically stable
    -   Rate<sub>-2</sub> is so insignificant compared to Rate<sub>1</sub> that we can ignore it
    -   The kinematics equation then looks like this:
        -   \\(E + S \overset{1}{\rightleftharpoons} ES \overset{2}{\rightarrow} E + P\\)


### Deriving [Michaelis-Menten]({{< relref "michaelis_menten_kinetics" >}}) equation {#deriving-michaelis-menten--michaelis-menten-kinetics-dot-md--equation}

Begin with these equations:

1.  \\(E + S \overset{1}{\rightleftharpoons} ES \overset{2}{\rightarrow} E + P\\)
2.  \\(Rate\_{1} = Rate\_{-1} + Rate\_{2}\\)

Swap out Rate values for Rate constant (K) x reactants

-   \\(K\_{1}[E][S] = K\_{-1}[ES] + K\_{2}[ES]\\)

The total amount of enzyme in a system includes both bound and unbound enzymes

-   \\([E]\_{T(otal)} = [E] + [ES]\\)

Rearrange and substitute in for [E] and factor out common term [ES] on right side of equation

-   \\(K\_{1}([E]\_{T} - [ES])[S] = [ES\]\(K\_{-1} + K\_{2})\\)

Expand left side of equation

-   \\(K\_{1}[E]\_{T}[S] - K\_{1}[ES][S] - [ES\]\(K\_{-1} + K\_{2})\\)

Divide both sides of equation by K<sub>1</sub>

-   \\([E]\_{T}[S] - [ES][S] = [ES\]\(\frac{K\_{-1} + K\_{2}}{K\_{1}})\\)

Rate constants are constant values, so lets define this as one term:

-   \\(\frac{K\_{-1} + K\_{2}}{K\_{1}} = K\_{M}\\)

Substitute in K<sub>m</sub> and multiply both sides by [ES][S]

-   \\([E]\_{t}[S] = [ES]K\_{M} + [ES][S]\\)

Flip equation around for legibility and factor out common terms

-   \\([ES\]\(K\_{m} + [S]) = [E]\_{T}[S]\\)

Divide both sides of equation by \\(K\_{M} + S\\) to move that term to right side

-   \\([ES] = \frac{[E]\_{T}[S]}{K\_{M} + [S]}\\)

Remember V<sub>o</sub> is equal to the rate of formation of our product, which is equal to \\(K\_{2}[ES]\\)

-   \\(V\_{o} = \frac{\Delta P}{\Delta t} = K\_{2}[ES]\\)

Multiply both sides by \\(K\_{2}\\)

-   \\(K\_{2}[ES] = \frac{K\_{2}[E]\_{T}[S]}{K\_{M} + [S]}\\)

If \\(V\_{o} = V\_{max}\\), then \\([E]\_{T} = [ES]\\), then \\(K\_{2}[E]\_{T} = V\_{max}\\)
Now we substitute in V<sub>o</sub> and V<sub>max</sub>

-   \\(V\_{o} = \frac{V\_{max}[S]}{K\_{M} + [S]}\\)


### [Michaelis constant]({{< relref "michaelis_menten_kinetics" >}}) (K<sub>M</sub>) {#michaelis-constant--michaelis-menten-kinetics-dot-md----k}

-   What does K<sub>M</sub> mean?
    -   Let's assume \\(K\_{M} = [S]\\)
    -   If we substitute that into the Michaelis-Mentin equation, we get: <br />
        \\(V\_{o} = \frac{V\_{max}[S]}{2[S]}\\) <br />
        Which we can simplify further into <br />
        \\(V\_{o} = \frac{V\_{max}}{2}\\)
    -   Thus, K<sub>M</sub> is the [S] where \\(V\_{o} = \frac{1}{2}V\_{max}\\)

{{< figure src="/ox-hugo/_20210713_212631screenshot.png" caption="Figure 1: The relationship between K<sub>M</sub> and V<sub>max</sub>" width="400" >}}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
