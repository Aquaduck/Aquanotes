+++
title = "Phosphoprotein phosphatase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept link {#concept-link}

-   Reverses the action of [protein kinases]({{< relref "protein_kinase" >}})
-   Activated + induced by [insulin]({{< relref "insulin" >}})


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Protein kinase A]({{< relref "protein_kinase_a" >}}) {#protein-kinase-a--protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inhibited by [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}})

    ---


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Insulin's metabolic effects**

    Insulin imediately activates [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) -> remove any phosphates introduced by [PKA]({{< relref "protein_kinase_a" >}}) -> **reverses processes**

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects > Reversal of glucagon's actions**

    Phosphate groups added to [PKA]({{< relref "protein_kinase_a" >}}) substrates are removed by protein [phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) via hydrolytic cleavage of phosphate esters

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Additional important enzymes**

    <!--list-separator-->

    -  [Phosphoprotein phosphatases]({{< relref "phosphoprotein_phosphatase" >}})

        -   Activated + induced by [insulin]({{< relref "insulin" >}})
        -   **Reverses action (dephosphorylates) of PKA and other kinases**


#### [Intermediary Metabolism and its Regulation]({{< relref "intermediary_metabolism_and_its_regulation" >}}) {#intermediary-metabolism-and-its-regulation--intermediary-metabolism-and-its-regulation-dot-md}

<!--list-separator-->

-  **🔖 Covalent modification**

    Phosphate groups are cleaved from phosphorylated enzymes by [phosphoprotein phosphatases]({{< relref "phosphoprotein_phosphatase" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Metabolism**

    [G6P]({{< relref "glucose_6_phosphate" >}}) binds to [glycogen synthase]({{< relref "glycogen_synthase" >}}) B -> better substrate for [Phosphoprotein phosphatase]({{< relref "phosphoprotein_phosphatase" >}}) -> activated glycogen synthase A

    ---


#### [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}}) {#describe-the-intercellular-response-to-insulin-induced-cellular-signaling--describe-the-intercellular-response-to-insulin-induced-cellular-signaling-dot-md}

<!--list-separator-->

-  **🔖 Insulin's metabolic effects**

    Insulin imediately activates [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) -> remove any phosphates introduced by [PKA]({{< relref "protein_kinase_a" >}}) -> **reverses processes**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
