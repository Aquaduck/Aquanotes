+++
title = "Protein disulfide isomerase"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe protein folding, identifying roles for chaperones, BiP, peptidyl prolyl isomerases and protein disulfide isomerases.]({{< relref "describe_protein_folding_identifying_roles_for_chaperones_bip_peptidyl_prolyl_isomerases_and_protein_disulfide_isomerases" >}}) {#describe-protein-folding-identifying-roles-for-chaperones-bip-peptidyl-prolyl-isomerases-and-protein-disulfide-isomerases-dot--describe-protein-folding-identifying-roles-for-chaperones-bip-peptidyl-prolyl-isomerases-and-protein-disulfide-isomerases-dot-md}

<!--list-separator-->

-  **🔖 From Organelles and Trafficking I-II > Formation of disulfide bonds**

    [Protein disulfide isomerase]({{< relref "protein_disulfide_isomerase" >}}) (PDI)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
