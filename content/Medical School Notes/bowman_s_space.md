+++
title = "Bowman's space"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Space within [Bowman capsule]({{< relref "bowman_capsule" >}}) that is not occupied by capillaries and mesangial cells


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Podocyte]({{< relref "podocyte" >}}) {#podocyte--podocyte-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Spaces between slit diaphragms constitute the path through which filtrate travels to enter the [Bowman's space]({{< relref "bowman_s_space" >}})

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Glomerular filtration (p. 33)**

    **Spaces between slit diaphragms constitute the path through which filtrate travels to enter the [Bowman's space]({{< relref "bowman_s_space" >}})**

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21)**

    [Bowman's space]({{< relref "bowman_s_space" >}}) is topologically **outside the body**

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21)**

    _[Filtration]({{< relref "filtration" >}})_: the process by which [Water]({{< relref "water" >}}) and solutes in the [Blood]({{< relref "blood" >}}) leave the vascular system through the [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) and enter [Bowman's space]({{< relref "bowman_s_space" >}})

    ---


#### [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}}) {#describe-glomerular-anatomy--ie-efferent-afferent-arteriole----describe-glomerular-anatomy-ie-efferent-afferent-arteriole-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal corpuscle (p. 17)**

    [Bowman's space]({{< relref "bowman_s_space" >}}): Space within [Bowman capsule]({{< relref "bowman_capsule" >}}) that is not occupied by capillaries and mesangial cells

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
