+++
title = "Ras"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Mitogens Stimulate G<sub>1</sub>-Cdk and G1/S-Cdk activities (p. 1012) > Mitogens interact with cell-surface receptors to trigger multiple intracellular signaling pathways**

    <!--list-separator-->

    -  [Ras]({{< relref "ras" >}})

        -   GTPase activity activates MAP kinase cascade -> increased production of transcription regulatory proteins such as [Myc]({{< relref "myc" >}})


#### [K-Ras]({{< relref "k_ras" >}}) {#k-ras--k-ras-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A member of the [Ras]({{< relref "ras" >}}) gene family

    ---


#### [Discuss the monomeric GTPases and how they are regulated, with specific reference to Ras.]({{< relref "discuss_the_monomeric_gtpases_and_how_they_are_regulated_with_specific_reference_to_ras" >}}) {#discuss-the-monomeric-gtpases-and-how-they-are-regulated-with-specific-reference-to-ras-dot--discuss-the-monomeric-gtpases-and-how-they-are-regulated-with-specific-reference-to-ras-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  [Ras]({{< relref "ras" >}})

        <!--list-separator-->

        -  Activation

            -   Requires a Guanine Nucleotide Exchange Factor ([GEF]({{< relref "guanine_nucleotide_exchange_factor" >}}))
            -   Replaces GDP on an inactive Ras with GTP
            -   Analogous to the hormone-receptor complex of the plasma membrane trimeric G-proteins

        <!--list-separator-->

        -  Inactivation

            -   Requires GTPase-Activating Protein ([GAP]({{< relref "gtpase_activating_protein" >}}))
                -   Significantly boost intrinsic GTPase activity


#### [Discuss receptor tyrosine kinases and tyrosine-associated receptor signal pathways]({{< relref "discuss_receptor_tyrosine_kinases_and_tyrosine_associated_receptor_signal_pathways" >}}) {#discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways--discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Receptor protein tyrosine kinases (RPTKs) > Two key pathways**

    <!--list-separator-->

    -  [Ras-GTP]({{< relref "ras" >}})

        -   Controls (e.g.):
            -   Endocytosis
            -   Cell cycle
            -   Nuclear transport
            -   Membrane trafficking
            -   Many more

        <!--list-separator-->

        -  Pathway

            1.  RPTK receptors phosphorylated on multiple locations -> activated
            2.  Allows binding of [SH<sub>2</sub>]({{< relref "src_homology_2_domain" >}}) binding proteins -> Ras activation
            3.  Ras activates [MAP-kinase-kinase-kinase]({{< relref "map_kinase_kinase_kinase" >}}) (Raf) -> activates [MAP-kinase-kinase]({{< relref "map_kinase_kinase" >}}) (Mek) -> activates [MAP-kinase]({{< relref "map_kinase" >}}) (Erk)
                -   Abnormal expression due to gene mutation and/or dysregulation -> cancer -> **[oncogenes]({{< relref "oncogene" >}})**


### Unlinked references {#unlinked-references}

[Show unlinked references]
