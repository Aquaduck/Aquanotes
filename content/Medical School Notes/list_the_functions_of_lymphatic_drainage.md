+++
title = "List the functions of lymphatic drainage."
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}}) {#from-lymphatic-and-immune-systems-ppt--lymphatic-and-immune-systems-ppt-dot-md}


### Functions of [lymphatic]({{< relref "lymph" >}}) flow {#functions-of-lymphatic--lymph-dot-md--flow}

-   Slow movement of lymph -> **filter out any foreign or undesirable material that may have inadvertently reached the interstitial fluid**
    -   This filtering action is performed by [lymph nodes]({{< relref "lymph_node" >}}) - the **major sites of activation of the adaptive immune system**
-   The arrangement of lymph and [lymph nodes]({{< relref "lymph_node" >}}) is designed to **activate the immune system before antigens and invading organisms reach the blood**
-   An additional function unrelated to immunity is the **transport of [lipoproteins]({{< relref "lipoprotein" >}}) absored in the G.I. tract**, which **exclusively enter the blood via lymphatics**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon] </span></span> > Lymphatic and Immune Systems > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the functions of lymphatic drainage.]({{< relref "list_the_functions_of_lymphatic_drainage" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
