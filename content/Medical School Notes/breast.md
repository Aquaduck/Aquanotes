+++
title = "Breast"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Describe lymphatic flow in the breast.]({{< relref "describe_lymphatic_flow_in_the_breast" >}}) {#describe-lymphatic-flow-in-the-breast-dot--describe-lymphatic-flow-in-the-breast-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Breast]({{< relref "breast" >}}) quadrants

        {{< figure src="/ox-hugo/_20211028_153603screenshot.png" caption="Figure 1: Four quadrants of the breast" >}}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Breast]({{< relref "breast" >}}) lymphatics

        -   The vast majority of lymph (75%) from the breast **is directed towards** _axillary nodes_
            -   Remaining lymph (especially from medial quadrants) directed to _parasternal nodes_
        -   Some drainage from the inferior region flows along the anterior abdominal wall to the inguinal nodes in the groin


#### [Breast cancer]({{< relref "breast_cancer" >}}) {#breast-cancer--breast-cancer-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Cancer]({{< relref "cancer" >}}) of the [Breast]({{< relref "breast" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
