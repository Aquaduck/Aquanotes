+++
title = "Hilum of the lung"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Pleura of the lung (p. 1057)**

    Visceral and parietal pleura connect to each other at the [hilum of the lung]({{< relref "hilum_of_the_lung" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
