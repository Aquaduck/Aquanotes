+++
title = "Describe the neurovasculature of the intercostal spaces."
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Accessing an Intercostal Space]({{< relref "accessing_an_intercostal_space" >}}) {#from-accessing-an-intercostal-space--accessing-an-intercostal-space-dot-md}


### [Intercostal muscles]({{< relref "intercostal_muscle" >}}) {#intercostal-muscles--intercostal-muscle-dot-md}

-   The intercostal muscles are 3 thin planes of muscular and tendinous fibers
    1.  _External_
    2.  _Internal_
    3.  _Innermost_
-   They are **all innervated by intercostal nerves (ventral rami of thoracic spinal nerves)**


### [Intercostal Vessels]({{< relref "intercostal_vessel" >}}) {#intercostal-vessels--intercostal-vessel-dot-md}

-   The _intercostal arteries_ course through **the thoracic wall between the ribs**
-   **With the exception of 10th and 11th intercostal spaces**, each is supplied by 3 arteries:
    1.  A larger posterior intercostal artery
    2.  **2** small anterior intercostal arteries
-   _Posterior intercostal arteries_ of **all but the first two interspaces** are from the _descending thoracic aorta_
-   _Anterior intercostal arteries_ are branches of the _internal thoracic artery_
-   Veins have similar names and generally follow the distribution of the intercostal arteries
    -   **Exception**: _Posterior intercostal veins_ drain into the _azygos system_


### [Intercostal Nerves]({{< relref "intercostal_nerve" >}}) {#intercostal-nerves--intercostal-nerve-dot-md}

-   _Ventral rami_ of T1-T11
    -   T12 nerve (aka [subcostal nerve]({{< relref "subcostal_nerve" >}})) is not confined between ribs
-   Near angle of ribs, intercostal nerves pass between the _internal and innermost intercostal muscles_
-   Intercostal nerves **supply general sensory innervation to skin on the thoracic wall and to the pleura**
    -   They also **supply motor innervation to intercostal, tranversus thoracis, and serratus posterior muscles**
    -   Carry /postganglionic sympathetic nerve fibers?
-   Intercostal nerves 2-6 are confined to the thorax
-   _1st intercostal nerve_ contributes to the [brachial plexus]({{< relref "brachial_plexus" >}}) as well as innervating the region of the first intercostal space
-   _Intercostal nerves 7-11_ leave the intercostal spaces after innervating them -> **continue on to innervate muscles, skin, and lining of arterial abdominal wall**
-   Intercostal vessels and nerves travel parallel to one another through the intercostal space
    -   Order of structures **superior -> inferior**:
        1.  Vein
        2.  Artery
        3.  Nerve
    -   Mnemonic: _VAN_


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the neurovasculature of the intercostal spaces.]({{< relref "describe_the_neurovasculature_of_the_intercostal_spaces" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
