+++
title = "Wee1"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Inactivates [cyclin-CDK complex]({{< relref "cyclin_cdk_complex" >}}) by phosphorylating two closely spaced sites above the active site


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Dephosphorylation Activates M-Cdk at the Onset of Mitosis**

    [Wee1]({{< relref "wee1" >}}) can be inhibited by M-Cdk

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Dephosphorylation Activates M-Cdk at the Onset of Mitosis**

    At the same time, [Wee1]({{< relref "wee1" >}}) activity is suppressed -> **ensures that M-Cdk activity increases**

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Dephosphorylation Activates M-Cdk at the Onset of Mitosis**

    [Wee1]({{< relref "wee1" >}}) **also** phosphorylates M-Cdk at two neighboring inhibitory sites

    ---

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop > Activation/inactivation mechanisms for CDKs > Inactivation of CDKs**

    Active cyclin-CDK complex is turned off when [Wee1]({{< relref "wee1" >}}) phosphorylates two closely spaced sites above the active site

    ---


#### [Cdc25]({{< relref "cdc25" >}}) {#cdc25--cdc25-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Removes phosphates added by [Wee1]({{< relref "wee1" >}}) to reactivate the [cyclin-CDK complex]({{< relref "cyclin_cdk_complex" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
