+++
title = "Metabolic Effects of Insulin and Glucagon"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Overview {#overview}

-   Metabolic pathways must be coordinated so that production of energy or synthesis of end products meets needs of cell


## Intracellular communication {#intracellular-communication}

-   Rate of a metabolic pathway can respond to regulatory signals that arise from within cell
    -   e.g. availability of substrates, product inhibition, or alterations in levels of allosteric activators/inhibitors
-   Typically elicit **rapid responses** and are important for **moment-to-tmoment regulation of metabolism**


## Intercellular communication {#intercellular-communication}

-   Provides for long-range integration of metabolism
-   Slower than intracellular signals
-   In energy metabolism, most important route is chemical signaling between cells mediated by blood-borne [hormones]({{< relref "hormone" >}})


## Second messenger systems {#second-messenger-systems}

-   [Hormones]({{< relref "hormone" >}}) can be thought of as **signals**, and their receptors as **signal detectors**
-   Receptors respond to a bound ligand by initiating a series of reactions that ultimately result in specific intracellular responses
-   Called "second messenger molecules" because they **intervene between original extracellular messenger (hormone) and ultimate intracellular effect**
-   [Glucagon]({{< relref "glucagon" >}}) (fasting state) and [epinephrine]({{< relref "epinephrine" >}}) (stress response) signal the most important second messenger system regulating the pathways of intermediary metabolism: **the adenylate cyclase system**


### Adenylate cyclase {#adenylate-cyclase}

-   Both glucagon and epinephrine bind to specific [G protein-coupled receptors]({{< relref "g_protein_coupled_receptors" >}}) (GPCRs) on the [plasma membrane]({{< relref "plasma_membrane" >}})
-   GCPRs are characterized by:
    1.  An extracellular ligand-binding domain
    2.  Seven transmembrane α-helices
    3.  Intracellular domain that interacts with trimeric [G proteins]({{< relref "g_protein" >}})
-   [Glucagon]({{< relref "glucagon" >}}) antagonizes [insulin]({{< relref "insulin" >}})'s effects


#### Takeaways {#takeaways}

1.  Glucagon's binding to its membrane receptor -> activation of adenylate cyclase
2.  Activation of adenylate-cyclase -> converts ATP to [cAMP]({{< relref "camp" >}})
3.  [cAMP]({{< relref "camp" >}}) binds to the two regulatory subunits (R) of the inactive [PKA]({{< relref "protein_kinase_a" >}}) -> release of two catalytically-active subunits -> activation of PKA
4.  [PKA]({{< relref "protein_kinase_a" >}}) enters nucleus and phosphorylates [cAMP response element binding protein]({{< relref "camp_response_element_binding_protein" >}}) (CREB)
5.  Phosphorylated CREB binds to its target genes -> induces or represes transcription


### [Glucagon]({{< relref "glucagon" >}})'s metabolic effects {#glucagon--glucagon-dot-md--s-metabolic-effects}

-   IV administration of glucagon -> **immediate rise** in blood glucose
    -   Due to immediate increase in degradation of liver glycogen + later increase in hepatic gluconeogenesis
-   Glucagon's binding to its hepatic receptor -> **immediate** initiation of glycogenolysis
    -   This is because [PKA]({{< relref "protein_kinase_a" >}}) phosphorylates and activates a key glycogen-degrading enzyme ([glycogen phosphorylase kinase]({{< relref "glycogen_phosphorylase_kinase" >}})), and also phosphorylates and inactivates the rate-limiting enzyme in glycogen synthesis ([glycogen synthase]({{< relref "glycogen_synthase" >}}))
-   [PKA]({{< relref "protein_kinase_a" >}})-mediated phosphorylation of [CREB]({{< relref "camp_response_element_binding_protein" >}}) -> increased transcription of [gluconeogenic]({{< relref "gluconeogenesis" >}}) enzymes if liver glycogen stores become depleted
    -   Pyruvate carboxylase
    -   PEP carboxykinase
    -   F1,6bPase
-   [PKA]({{< relref "protein_kinase_a" >}}) also phosphorylates and inactivates the liver pyruvate kinase isoenzyme -> ensures that when PEP is formed from pyruvate, it remains a substrate for enolase
-   Glucagon affects lipid metabolism in liver by initiating the PKA-catalyzed phosphorylation and inactivation of [acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}}) (catalyzes rate-limiting and committed step in faty acid synthesis)
    -   This is required to prevent formation of [malonyl-CoA]({{< relref "malonyl_coa" >}}) (inhibits entry of long-chain fatty acids into mitochondria by blocking [CPT1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}), the first enzyme in the carnitine shuttle)
    -   Promotes the uptake of adipose-derived fatty acids into liver mitochondria -> ensures their oxidation + subsequent formation of ATP required to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})
-   Glucagon affects gluconeogenic process by **increasing liver's uptake of amino acids supplied by muscle breakdown** -> increases availability of carbon skeletons for [gluconeogenesis]({{< relref "gluconeogenesis" >}})
    -   As a consequence, **plasma concentrations of amino acids are decreased**


#### Summary of glucagon's overall metabolic effects on liver {#summary-of-glucagon-s-overall-metabolic-effects-on-liver}

1.  ↑ Glycogenolysis
2.  ↓ Glycogen synthesis
3.  ↑ Gluconeogenesis
4.  ↑ Amino acid uptake
5.  ↓ Fatty acid synthesis
6.  ↑ Fatty acid oxidation


#### Reversal of [glucagon]({{< relref "glucagon" >}})'s actions {#reversal-of-glucagon--glucagon-dot-md--s-actions}

-   Phosphate groups added to [PKA]({{< relref "protein_kinase_a" >}}) substrates are removed by protein [phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) via hydrolytic cleavage of phosphate esters
    -   Ensures that changes in protein activity induced by phosphorylation are **reversible**
-   [cAMP]({{< relref "camp" >}}) can be rapidly hydrolyzed to 5'-AMP by [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}}) through cleavage of cyclic 3',5'-phosphodiester bond
    -   5'-AMP is **not** a signaling molecule -> cAMP affects cease


### [Insulin]({{< relref "insulin" >}})'s metabolic effects {#insulin--insulin-dot-md--s-metabolic-effects}

-   Insulin does not bind to a GPCR
    -   Instead, binds to specific, high-affinity receptors in cell membranes of **liver, striated muscle, adipose tissue, and kidney** -> considered "insulin-sensitive" tissues
-   Insulin-induced changes in activity of several enzymes occur over minutes to hours and reflect changes in phosphorylation states of existing proteins
    -   Over hours to days -> increases **amount** of many enzymes
        -   Reflect an increase in gene expression through increased transcription mediated by specific regulatory element-binding proteins and subsequent translation
-   Crucial aspect of insulin-induced signaling is **activation of enzymes that eliminate glucagon-mediated changes in enzyme activities**
    -   Insulin imediately activates [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) -> remove any phosphates introduced by [PKA]({{< relref "protein_kinase_a" >}}) -> **reverses processes**
    -   E.g. insulin-induced phosphatases stop glycogenolysis and promote glycogen synthesis + storage -> ensures glycogen pools are restored in liver and striated muscle
    -   Restores [pyruvate kinase]({{< relref "pyruvate_kinase" >}}) activity -> stops [gluconeogenesis]({{< relref "gluconeogenesis" >}}) by immediately converting any PEP made back to pyruvate
-   Activates [cAMP-phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})
    -   Hydrolyzes cAMP to 5'-AMP -> eliminate second messenger activating PKA -> [PKA]({{< relref "protein_kinase_a" >}}) activity significantly diminished
-   Promotes storage of other nutrients e.g. triacylglycerol and protein + inhibits their mobilization


#### Summary of insulin's overall metabolic effects {#summary-of-insulin-s-overall-metabolic-effects}

1.  ↑ Glucose uptake
2.  ↑ Glycogen and protein synthesis
3.  ↓ Gluconeogenesis
4.  ↓ Glycogenolysis
5.  ↓ Fatty acid mobilization
6.  ↑ Fat synthesis in insulin-sensitive tissues _other than the liver_


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}}) {#describe-the-intercellular-response-to-insulin-induced-cellular-signaling--describe-the-intercellular-response-to-insulin-induced-cellular-signaling-dot-md}

<!--list-separator-->

-  From [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
