+++
title = "Progerin"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Farnesylated mutant [prelamin A]({{< relref "prelamin_a" >}}) found in [HGPS]({{< relref "hutchinson_guildford_progeria" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}}) {#rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot--rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot-md}

<!--list-separator-->

-  **🔖 From An overview of treatment strategies for Hutchinson-Gilford Progeria syndrome**

    Blocks farnesylation of of [progerin]({{< relref "progerin" >}}) -> restores normal nuclear architecture w/ significantly reduced nuclear blebbing

    ---

<!--list-separator-->

-  **🔖 From Protein farnesylation and disease > Isoprenylation and postprenylation of nuclear lamins**

    Consequently, no mature [lamin A]({{< relref "lamin_a" >}}) is formed -> accumulation of a **farnesylated mutant prelamin A** ([progerin]({{< relref "progerin" >}}))

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
