+++
title = "Podocyte"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Third layer of the [Glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}})
-   Spaces between slit diaphragms constitute the path through which filtrate travels to enter the [Bowman's space]({{< relref "bowman_s_space" >}})
-   Integrity of slit diaphragms of podocytes essential to prevent excessive leak of [albumin]({{< relref "albumin" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Glomerular filtration (p. 33)**

    [Podocytes]({{< relref "podocyte" >}}): epithelial cells that surround capillaries and rest on the capillary basement membrane

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
