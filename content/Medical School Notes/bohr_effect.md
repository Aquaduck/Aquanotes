+++
title = "Bohr Effect"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Factors Affecting Hemoglobin Dissociation Curve]({{< relref "factors_affecting_hemoglobin_dissociation_curve" >}}) {#factors-affecting-hemoglobin-dissociation-curve--factors-affecting-hemoglobin-dissociation-curve-dot-md}

<!--list-separator-->

-  **🔖 Notes > Factors affecting O<sub>2</sub>-Hb Dissociation Curve: > pH**

    Allows more O<sub>2</sub> to be delivered to the tissues <- _[Bohr Effect]({{< relref "bohr_effect" >}})_

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
