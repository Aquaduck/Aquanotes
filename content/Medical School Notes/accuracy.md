+++
title = "Accuracy"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Statistical power]({{< relref "statistical_power" >}}) {#statistical-power--statistical-power-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Positively correlates with [sample size]({{< relref "sample" >}}) and [accuracy]({{< relref "accuracy" >}})

    ---


#### [Define statistical power and explain how it affects the interpretation of negative results]({{< relref "define_statistical_power_and_explain_how_it_affects_the_interpretation_of_negative_results" >}}) {#define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results--define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Statistical power**

    Correlates with measurement [accuracy]({{< relref "accuracy" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
