+++
title = "Cdk1"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A [CDK]({{< relref "cyclin_dependent_kinase" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Dephosphorylation Activates M-Cdk at the Onset of Mitosis**

    [Cdk1]({{< relref "cdk1" >}}) forms a complex with [M-cyclin]({{< relref "cyclin_b" >}}) -> formation of M-Cdk complex as the cell approaches mitosis

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
