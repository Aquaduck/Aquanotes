+++
title = "Describe the basic structural properties of the arteries and veins in the human body including conducting arteries, resistance arteries and arterioles, capillaries, venules and veins, and explain why veins are considered capacitance vessels."
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From <span class="timestamp-wrapper"><span class="timestamp">[2021-10-16 Sat] </span></span> Session {#from-session}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-18 Mon] </span></span> > Regional and Peripheral Circulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Describe the basic structural properties of the arteries and veins in the human body including conducting arteries, resistance arteries and arterioles, capillaries, venules and veins, and explain why veins are considered capacitance vessels.]({{< relref "describe_the_basic_structural_properties_of_the_arteries_and_veins_in_the_human_body_including_conducting_arteries_resistance_arteries_and_arterioles_capillaries_venules_and_veins_and_explain_why_veins_are_considered_capacitance_vessels" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
