+++
title = "Fibrinogen"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Produced by the [liver]({{< relref "liver" >}})
-   Essential for [blood clotting]({{< relref "blood_clotting" >}})
-   Precursor to [fibrin]({{< relref "fibrin" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Plasma Proteins (p. 796)**

    <!--list-separator-->

    -  [Fibrinogen]({{< relref "fibrinogen" >}})

        -   Produced by the liver
        -   Essential for [blood clotting]({{< relref "blood_clotting" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
