+++
title = "Peroxisome"
author = ["Arif Ahsan"]
date = 2021-09-07T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## From First Aid {#from-first-aid}

-   Membrane-enclosed [organelle]({{< relref "organelle" >}}) involved in:
    -   [Beta-oxidation]({{< relref "β_oxidation" >}}) of very-long-chain fatty acids (VLCFA)
    -   Alpha-oxidation (strictly peroxisomal process)
    -   Catabolism of branched-chain fatty acids, amino acids, and ethanol
    -   Synthesis of [cholesterol]({{< relref "cholesterol" >}}), bile acids, and plasmalogens


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
