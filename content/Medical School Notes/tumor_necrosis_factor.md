+++
title = "Tumor necrosis factor"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Produced by [monocytes]({{< relref "monocyte" >}})/macrophages
-   Binds to a [Death receptor]({{< relref "death_receptor" >}}) on the cell surface
-   Default signaling pathway is to bind to [TNFR1]({{< relref "tnfr1" >}}) and induce [NFκB]({{< relref "nfκb" >}})
    -   Induces cell death **only** when this pathway is disrupted


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [TNFR1]({{< relref "tnfr1" >}}) {#tnfr1--tnfr1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The main [TNFR]({{< relref "tumor_necrosis_factor_receptor" >}}) that transduces signals from [TNF-α]({{< relref "tumor_necrosis_factor" >}})

    ---


#### [TNF alpha and the TNF receptor superfamily: structure-function relationship(s)]({{< relref "tnf_alpha_and_the_tnf_receptor_superfamily_structure_function_relationship_s" >}}) {#tnf-alpha-and-the-tnf-receptor-superfamily-structure-function-relationship--s----tnf-alpha-and-the-tnf-receptor-superfamily-structure-function-relationship-s-dot-md}

<!--list-separator-->

-  [TNF-α]({{< relref "tumor_necrosis_factor" >}}) mechanism(s) of action

    -   TNF exerts its effect(s) by forming a trimer that binds to and clusters high-affinity receptors present in great numbers on most cell membranes
        -   The ligand/receptor complex is rapidly internalised via clathrin-coated pits -> secondary lysosomes degrade it
        -   This causes the activation of many secondary proteins that are involved with gene transcription and/or production of ROS and nitrogen radicals:
            1.  G-Protein
            2.  Transcription factors
                -   NF-κB
                -   AP-1
            3.  Protein kinases
                -   CK II
                -   Erk-1
                -   Erk-2
                -   [MAP2]({{< relref "map2" >}})
            4.  Phospholipases
                -   PLA<sub>2</sub>
                -   PLC
                -   PLD
                -   Sphingomyelinase
            5.  Mitochondrial proteins
                -   Manganese superoxide dismutase
            6.  [Caspases]({{< relref "caspase" >}})

<!--list-separator-->

-  Biological role(s) of [TNF-α]({{< relref "tumor_necrosis_factor" >}})

    -   TNF plays several therapeutic roles within the body, which includes:
        -   Immunostimulation
        -   Resistance to infection agents
        -   Resistance to tumours
        -   Sleep regulation
        -   Embryonic development
    -   On the other hand, parasitic, bacterial, and viral infections become more pathogenic or fatal due to TNF circulation
    -   The **major role** of TNF is as an important mediator in resistance against infections


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) in [TNF]({{< relref "tumor_necrosis_factor" >}})/[TNFR1]({{< relref "tnfr1" >}}) signalling

    -   The default signaling pathway activating by the binding of TNF alpha is the induction of [NFκB]({{< relref "nfκb" >}}) -> responsible for **cell survival and growth**
    -   TNFR1 induces cell death (apoptosis AND necrosis) **only when [NFκB]({{< relref "nfκb" >}}) activation is impaired**
    -   Dependence of TNFR1 signaling on TRADD appears to be **cell-type specific**


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Death receptors > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    The ligands that activate death receptors are also homotrimers and are part of the [TNF]({{< relref "tumor_necrosis_factor" >}}) family of **signal proteins**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
