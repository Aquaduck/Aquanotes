+++
title = "List the glucose-utilizing pathways that predominate in:"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## RBC {#rbc}

-   [Glycolysis]({{< relref "glycolysis" >}})
-   [PPP]({{< relref "pentose_phosphate_pathway" >}}) (mainly NADPH for antioxidant)


## Brain {#brain}

-   [Glycogenesis]({{< relref "glycogenesis" >}})
-   [Glycolysis]({{< relref "glycolysis" >}})
-   [PPP]({{< relref "pentose_phosphate_pathway" >}}) (both arms)
-   [Fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})


## Muscle {#muscle}

-   [Fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})
-   [Glycogenesis]({{< relref "glycogenesis" >}})
-   [Glycolysis]({{< relref "glycolysis" >}})
-   [PPP]({{< relref "pentose_phosphate_pathway" >}})
-   **No response to glucagon - only to ↓ insulin**


## Adipose {#adipose}

-   [Fatty acid oxidation]({{< relref "β_oxidation" >}})
-   [Fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})
-   [Glycolysis]({{< relref "glycolysis" >}})
-   [PPP]({{< relref "pentose_phosphate_pathway" >}})
-   Glycogen in **pathological state**
-   **No response to glucagon - only to ↓ insulin**


## Liver {#liver}

-   Steroid and minimal [fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})
-   [PPP]({{< relref "pentose_phosphate_pathway" >}})
-   [Glycogenesis]({{< relref "glycogenesis" >}})
-   **Major glucagon target**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the glucose-utilizing pathways that predominate in:]({{< relref "list_the_glucose_utilizing_pathways_that_predominate_in" >}})

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  RBCs

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Brain

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Muscle

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Adipose

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Liver


### Unlinked references {#unlinked-references}

[Show unlinked references]
