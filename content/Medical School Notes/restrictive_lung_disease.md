+++
title = "Restrictive lung disease"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A class of [lung]({{< relref "lung" >}}) disease


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  Findings of [obstructive]({{< relref "obstructive_lung_disease" >}}) vs. [restrictive lung disease]({{< relref "restrictive_lung_disease" >}})

        | Measurement                                   | Obstructive lung disease                                             | Restrictive lung disease                             |
        |-----------------------------------------------|----------------------------------------------------------------------|------------------------------------------------------|
        | [FEV1]({{< relref "fev1" >}})                 | Low                                                                  | Low or normal                                        |
        | FEV1/FVC                                      | Low                                                                  | Normal or high                                       |
        | [VC]({{< relref "vital_capacity" >}})         | Low                                                                  | Low                                                  |
        | [A-a gradient]({{< relref "a_a_gradient" >}}) | High                                                                 | Normal (extrinsic causes) or high (intrinsic causes) |
        | Lung compliance                               | Normal (may be increased in [emphysema]({{< relref "emphysema" >}})) | Normal (extrinsic causes) or low (intrinsic causes)  |

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Restrictive lung disease]({{< relref "restrictive_lung_disease" >}})

        -   Impaired ability of lungs to expand (i.e. reduced lung compliance)

        <!--list-separator-->

        -  Etiology

            -   _Intrinsic causes_:
                -   [Interstitial lung disease]({{< relref "interstitial_lung_disease" >}})
            -   _Extrinsic causes_:
                -   Diseases of [pleura]({{< relref "pleura" >}}) and [pleural cavity]({{< relref "pleural_cavity" >}})
                    -   Chronic pleural effusion
                    -   Pleural adhesions
                    -   [Pneumothorax]({{< relref "pneumothorax" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
