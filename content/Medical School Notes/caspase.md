+++
title = "Caspase"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Main driver of apoptosis
    -   Involved in both [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}) and [Intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}})
-   Targets:
    -   Nuclear [lamins]({{< relref "lamin" >}})
    -   Cleavage of proteins that free [endonuclease]({{< relref "endonuclease" >}})


## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [TNF alpha and the TNF receptor superfamily: structure-function relationship(s)]({{< relref "tnf_alpha_and_the_tnf_receptor_superfamily_structure_function_relationship_s" >}}) {#tnf-alpha-and-the-tnf-receptor-superfamily-structure-function-relationship--s----tnf-alpha-and-the-tnf-receptor-superfamily-structure-function-relationship-s-dot-md}

<!--list-separator-->

-  **🔖 TNF-α mechanism(s) of action**

    [Caspases]({{< relref "caspase" >}})

    ---


#### [Second mitochondria-derived activator of caspases]({{< relref "second_mitochondria_derived_activator_of_caspases" >}}) {#second-mitochondria-derived-activator-of-caspases--second-mitochondria-derived-activator-of-caspases-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Binds [IAPs]({{< relref "inhibitor_of_apoptosis" >}}) to free [caspases]({{< relref "caspase" >}}) and activate apoptosis

    ---


#### [Pro-caspase]({{< relref "pro_caspase" >}}) {#pro-caspase--pro-caspase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An inactive zymogen that dimerizes to form an active heterodimer [caspase]({{< relref "caspase" >}})

    ---


#### [Initiator caspase]({{< relref "initiator_caspase" >}}) {#initiator-caspase--initiator-caspase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [caspases]({{< relref "caspase" >}})

    ---


#### [Inhibitor of apoptosis]({{< relref "inhibitor_of_apoptosis" >}}) {#inhibitor-of-apoptosis--inhibitor-of-apoptosis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Bind to and inhibit activated [caspases]({{< relref "caspase" >}})

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > IAPs**

    Some IAPs also polyubiquitylate [caspases]({{< relref "caspase" >}}) -> marking them for destruction by proteasomes

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  [Caspase]({{< relref "caspase" >}})

        <!--list-separator-->

        -  [Apoptosis]({{< relref "apoptosis" >}}) depends on an intracellular proteolytic cascade that is mediated by caspases (p.1022)

            -   Irreversible apoptosis is triggered by _Caspase_ proteases
                -   A family of specialized intracellular proteases which cleave specific sequences in numerous proteins inside the cell leading to cell death
                -   Have a **cysteine** at their active site and cleave target proteins at specific **aspartic acids**
                -   Synthesized in the cell as inactive precursors -> **activated only during apoptosis**
                -   Two major classes: _initiator_ caspases and _executioner_ caspases

        <!--list-separator-->

        -  [Initiator caspases]({{< relref "initiator_caspase" >}})

            -   Begin apoptosis, main function being to **activate executioner caspases**
            -   Normally exist as inactive, soluble monomers in the cytosol
            -   An apoptotic signal triggers the assembly of large protein platofrms -> formation of large complexes of initiator caspases
                -   Pairs of caspases associate to form dimers -> **protease activation**
                    -   Each caspase in the dimer cleaves its partner at a specific site in the protease domain -> stabilizes active complex and required for proper function of the enzyme in the cell

        <!--list-separator-->

        -  [Executioner caspases]({{< relref "executioner_caspase" >}})

            -   Normally exist as inactive dimers
                -   Cleaved by initiator caspase -> active site rearranged -> activation

        <!--list-separator-->

        -  Targets of caspase

            -   Nuclear [lamins]({{< relref "lamin" >}}) -> irreversible breakdown of nuclear lamina
            -   Cleavage of protein that frees [endonuclease]({{< relref "endonuclease" >}}) -> cuts up DNA in cell nucleus
            -   Components of the cytoskeleton and cell-cell adhesion proteins -> allows cell to round up and detach from neighbors -> easier for neighboring cells to engulf it


#### [Executioner caspase]({{< relref "executioner_caspase" >}}) {#executioner-caspase--executioner-caspase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [caspases]({{< relref "caspase" >}})

    ---


#### [Death receptor]({{< relref "death_receptor" >}}) {#death-receptor--death-receptor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    "[Death domains]({{< relref "death_domain" >}})" (characteristic feature of DRs) activate [caspases]({{< relref "caspase" >}}) as part of the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}})

    ---


#### [Caspase-9]({{< relref "caspase_9" >}}) {#caspase-9--caspase-9-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [Caspase]({{< relref "caspase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
