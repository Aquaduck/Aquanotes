+++
title = "Ascending thick limb of the loop of Henle"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Third component of the [loop of Henle]({{< relref "loop_of_henle" >}})
-   Marks the border between the [Inner medulla of the kidney]({{< relref "inner_medulla_of_the_kidney" >}}) and the [Outer medulla of the kidney]({{< relref "outer_medulla_of_the_kidney" >}})
-   Rises back into the [Bowman's capsule]({{< relref "bowman_capsule" >}})
-   Contain [macula densa]({{< relref "macula_densa" >}}) cells by the juxtaglomerular apparatus


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Macula densa]({{< relref "macula_densa" >}}) {#macula-densa--macula-densa-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Mark the end of the [thick ascending limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}}) and the beginning of the [distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}})

    ---


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18)**

    <!--list-separator-->

    -  [Ascending thick limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}})

        -   Epithelium thickens
        -   In short loops, the abrupt hairpin turn from the descending thin limb leads **directly** to the ascending thick limb
        -   **All thick ascending limbs begin at the same level**
            -   Marks the border between the [inner]({{< relref "inner_medulla_of_the_kidney" >}}) and [outer medulla]({{< relref "outer_medulla_of_the_kidney" >}})
        -   Each thick ascending limb rises back into the cortex to the [Bowman's capsule]({{< relref "bowman_capsule" >}})
            -   Passes directly between the [afferent]({{< relref "afferent_arteriole_of_the_kidney" >}}) and [efferent]({{< relref "efferent_arteriole_of_the_kidney" >}}) arterioles at the vascular pole
                -   [Macula densa]({{< relref "macula_densa" >}}) cells found at this point between the two arterioles and marks the end of the ascending thick limb of the loop of Henle


#### [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}}) {#explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result--explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Juxtaglomerular apparatus (p. 21) > Macula densa cells**

    Detectors of flow rate and composition of fluid in nephron at th every end of the [thick ascending limb]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
