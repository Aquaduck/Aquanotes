+++
title = "HIV Infection and AIDS"
author = ["Arif Ahsan"]
date = 2021-08-23T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Practice essentials {#practice-essentials}

-   [Human immunodeficiency virus]({{< relref "human_immunodeficiency_virus" >}}) (HIV) is a blood-borne virus typically transmitted via:
    -   Sexual intercourse
    -   Shared intravenous drug paraphernalia
    -   Mother-to-child transmission (MTCT)
        -   Can occur during the birth process or during breastfeeding
-   [HIV]({{< relref "human_immunodeficiency_virus" >}}) disease is caused by infection with [HIV-1]({{< relref "hiv_1" >}}) or [HIV-2]({{< relref "hiv_2" >}}), which are retroviruses in the _Retroviridae_ family, _Lentivirus_ genus
-   No physical findings are specific to HIV infection


### Manifestations of HIV include the following: {#manifestations-of-hiv-include-the-following}

-   Acute seroconversion manifests as a flulike illness, consisting of
    -   Fever
    -   Maliase
    -   generalized rash
-   Asymptomatic phase of [HIV]({{< relref "human_immunodeficiency_virus" >}}) is generally benign
-   Generalized [lymphadenopathy]({{< relref "lymphadenopathy" >}}) is common and may be a presenting symptom


### Risk factors: {#risk-factors}

-   Unprotected sex, especially receptive anal intercourse
-   High # of sexual partners
-   Previous or current sexually transmitted diseases (STDs)
-   Sharing of intravenous (IV) drug paraphernalia
-   Receipt of blood products (before 1985 in the US)
-   Mucosal contact with infected blood or needle-stick injuries
-   Maternal HIV infection (for newborns, infants, and children)


### HIV screening recommendations {#hiv-screening-recommendations}

-   _USPSTF_ recommends to screen all adolescents and adults at increased risk for HIV infection, and all pregnant women
-   CDC recommends opt-out HIV screening for pts in all health-care settings
    -   Persons at high risk -> screened annually
    -   Test with a FDA approved antigen/antibody immunoassay that detects HIV-1 and HIV-2 antibodies and the HIV-1 p24 antigen
        -   Supplemental testing following a reactive assay result to differentiate between HIV-1 and HIV-2 antibodies
            -   If nonreactive or indeterminate, HIV-1 nucleic acid test recommended to differentiate acute HIV-1 infection from a false-positive test result
-   ACP recommends that clinicians adopt routine screening for HIV and encourage all patients to be tested
-   WHO recommends HIV testing whereby a combination of rapid diagnostic tests (RDTs) and/or [EIA]({{< relref "enzyme_immunoassay" >}})s are used to achieve at least a **99% positive predictive value**


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
