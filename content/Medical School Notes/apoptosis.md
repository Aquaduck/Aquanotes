+++
title = "Apoptosis"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The programmed death of a [cell]({{< relref "cell" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Fas > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    Once dimerized + activated in [DISC]({{< relref "death_inducing_signaling_complex" >}}), [initiator caspases]({{< relref "initiator_caspase" >}}) cleave their partners -> activate downstreain [executioner caspases]({{< relref "executioner_caspase" >}}) to induce [apoptosis]({{< relref "apoptosis" >}})

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Death receptors > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    An intracellular [death domain]({{< relref "death_domain" >}}) -> required for receptor to activate [apoptosis]({{< relref "apoptosis" >}})

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Caspase**

    <!--list-separator-->

    -  [Apoptosis]({{< relref "apoptosis" >}}) depends on an intracellular proteolytic cascade that is mediated by caspases (p.1022)

        -   Irreversible apoptosis is triggered by _Caspase_ proteases
            -   A family of specialized intracellular proteases which cleave specific sequences in numerous proteins inside the cell leading to cell death
            -   Have a **cysteine** at their active site and cleave target proteins at specific **aspartic acids**
            -   Synthesized in the cell as inactive precursors -> **activated only during apoptosis**
            -   Two major classes: _initiator_ caspases and _executioner_ caspases


#### [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}) {#extrinsic-pathway-of-apoptosis--extrinsic-pathway-of-apoptosis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    One pathway of [apoptosis]({{< relref "apoptosis" >}})

    ---


#### [Define apoptosis and necrosis]({{< relref "define_apoptosis_and_necrosis" >}}) {#define-apoptosis-and-necrosis--define-apoptosis-and-necrosis-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Intro (p. 1021)**

    [Apoptosis]({{< relref "apoptosis" >}}) is programmed cell death, whereas [necrosis]({{< relref "necrosis" >}}) occurs in response to suboptimal conditions such as truama or inadequate blood supply

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
