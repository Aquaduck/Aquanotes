+++
title = "Riboflavin"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   AKA _Vitamin B<sub>2</sub>_
-   [B vitamin]({{< relref "b_vitamin" >}})


## From First Aid Section II Biochemistry Nutrition {#from-first-aid-section-ii-biochemistry-nutrition}


### Function {#function}

-   Component of flavins [FAD]({{< relref "fadh2" >}}) and FMN
-   Used as cofactors in redox reactions
    -   e.g. [Succinate dehydrogenase]({{< relref "succinate_dehydrogenase" >}}) reaction in the [TCA cycle]({{< relref "citric_acid_cycle" >}})


## Backlinks {#backlinks}


### 8 linked references {#8-linked-references}


#### [Niacin]({{< relref "niacin" >}}) {#niacin--niacin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Synthesis requires [vitamins B<sub>2</sub>]({{< relref "riboflavin" >}}) and [B<sub>6</sub>]({{< relref "pyridoxine" >}})

    ---


#### [List the vitamins, both by number and by name, and indicate, in broad strokes, why they are important to the metabolic processes and/or pathways specified above]({{< relref "list_the_vitamins_both_by_number_and_by_name_and_indicate_in_broad_strokes_why_they_are_important_to_the_metabolic_processes_and_or_pathways_specified_above" >}}) {#list-the-vitamins-both-by-number-and-by-name-and-indicate-in-broad-strokes-why-they-are-important-to-the-metabolic-processes-and-or-pathways-specified-above--list-the-vitamins-both-by-number-and-by-name-and-indicate-in-broad-strokes-why-they-are-important-to-the-metabolic-processes-and-or-pathways-specified-above-dot-md}

<!--list-separator-->

-  [B2]({{< relref "riboflavin" >}})

    -   FAD
    -   Redox potential
    -   PDH complex/alpha-KG


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid oxidation**

    [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)

    ---

<!--list-separator-->

-  **🔖 TCA cycle**

    [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)

    ---

<!--list-separator-->

-  **🔖 Pyruvate DH complex**

    [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)

    ---


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Cellular Metabolism > 14. List the vitamins required for the appropriate functioning of: > TCA cycle**

    [Riboflavin]({{< relref "riboflavin" >}})

    ---


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By vitamin**

    <!--list-separator-->

    -  [Riboflavin]({{< relref "riboflavin" >}}) (B2)

        -   Succinate DH
        -   Pyruvate DH complex
        -   alpha-KG DH


#### [FADH2]({{< relref "fadh2" >}}) {#fadh2--fadh2-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A derivative of [riboflavin]({{< relref "riboflavin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
