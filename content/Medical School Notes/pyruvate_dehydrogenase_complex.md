+++
title = "Pyruvate dehydrogenase complex"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Connects [glycolysis]({{< relref "glycolysis" >}}) to the [TCA cycle]({{< relref "citric_acid_cycle" >}})
-   [Allosterically]({{< relref "allosteric_regulation" >}}) regulated by product inhibition by [acetyl-CoA]({{< relref "acetyl_coa" >}}) & [NADH]({{< relref "nadh" >}}) against their respective enzymes
-   Downregulation via [pyruvate DH kinase]({{< relref "pyruvate_dh_kinase" >}})
-   Upregulation via [pyruvate DH phosphatase]({{< relref "pyruvate_dh_phosphatase" >}})


### Cofactors/Coenzymes {#cofactors-coenzymes}

-   [NAD<sup>+</sup>]({{< relref "nadh" >}})
-   [FAD]({{< relref "fadh2" >}})
-   [CoA]({{< relref "coenzyme_a" >}})
-   [Lipoic acid]({{< relref "lipoic_acid" >}})
-   [Thiamine]({{< relref "thiamine" >}})


## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [α-ketoglutarate dehydrogenase]({{< relref "α_ketoglutarate_dehydrogenase" >}}) {#α-ketoglutarate-dehydrogenase--α-ketoglutarate-dehydrogenase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Mechanism is analogous to that of the [pyruvate DH complex]({{< relref "pyruvate_dehydrogenase_complex" >}})

    ---


#### [Thiamine]({{< relref "thiamine" >}}) {#thiamine--thiamine-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry - Nutrition > Function**

    [Pyruvate dehydrogenase]({{< relref "pyruvate_dehydrogenase_complex" >}}) (links [glycolysis]({{< relref "glycolysis" >}}) to TCA cycle)

    ---


#### [Pyruvate DH kinase]({{< relref "pyruvate_dh_kinase" >}}) {#pyruvate-dh-kinase--pyruvate-dh-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Phosphorylates Ser residues on the first enzyme of the [PDH]({{< relref "pyruvate_dehydrogenase_complex" >}}) complex (E1)

    ---


#### [List the four enzymes, and their intracellular location, for which pyruvate serves as a substrate, and the product that is made]({{< relref "list_the_four_enzymes_and_their_intracellular_location_for_which_pyruvate_serves_as_a_substrate_and_the_product_that_is_made" >}}) {#list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made--list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made-dot-md}

<!--list-separator-->

-  **🔖 Enzymes > Mitochondria**

    [Pyruvate DH complex]({{< relref "pyruvate_dehydrogenase_complex" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  [Pyruvate DH complex]({{< relref "pyruvate_dehydrogenase_complex" >}})

    -   [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)
    -   [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)
    -   [CoA]({{< relref "coenzyme_a" >}}) from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)
    -   [Lipoic acid]({{< relref "lipoic_acid" >}})
    -   [Thiamine]({{< relref "thiamine" >}}) (B1)


#### [Lightyear: Infinity notes]({{< relref "lightyear_infinity_notes" >}}) {#lightyear-infinity-notes--lightyear-infinity-notes-dot-md}

<!--list-separator-->

-  **🔖 Biochemistry**

    <!--list-separator-->

    -  Activation of [Pyruvate DH]({{< relref "pyruvate_dehydrogenase_complex" >}}):

        -   NAD+/NADH
        -   ADP
        -   Calcium


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle > NAD<sup>+</sup>-linked α-ketoglutarate dehydrogenase**

    Mechanism is **analogous to that of the [pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}})**

    ---

<!--list-separator-->

-  **🔖 Pyruvate metabolism**

    <!--list-separator-->

    -  [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}})

        -   The mitochondrial PDH complex is allosterically regulated by product inhibition by acetyl-CoA & NADH against their respective enzymes, as well as by covalent modification

        <!--list-separator-->

        -  [Pyruvate DH kinase]({{< relref "pyruvate_dh_kinase" >}})

            -   Phosphorylates Ser residues on the first enzyme of the PDH complex (E1)
                -   Results in significant **down-regulation of its activity**
            -   Allosterically stimulated by the products of the reaction
            -   Allosterically inhibited by its substrates
            -   Also regulated by the energy charge <- kinase allosterically inhibited by ADP

        <!--list-separator-->

        -  [Pyruvate DH phosphatase]({{< relref "pyruvate_dh_phosphatase" >}})

            -   Activated by high concentrations of Ca<sup>2+</sup> and Mg<sup>2​+</sup> which **reverses the effects of PDH kinase** (i.e. dephosphorylates PDH)
                -   This **ensures glucose utilization**
            -   Activation by Ca<sup>2+</sup> primarily in skeletal muscle
                -   Contraction leads to Ca<sup>2+</sup> release from cellular stores
            -   Activating effect of Mg<sup>2+</sup> due to **low mitochondrial concentrations of ATP**


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By enzyme**

    <!--list-separator-->

    -  [Pyruvate DH complex]({{< relref "pyruvate_dehydrogenase_complex" >}})

        -   Thiamine (B1) (as thiamine pyrophosphate) is a cofactor
        -   Riboflavin (B2) in the form of FAD
        -   Niacin (B3) in the form of NAD<sup>+</sup>
        -   Pantothenic acid (B5) in the form of CoA


#### [Cell Signalling III - Underlying Design Features]({{< relref "cell_signalling_iii_underlying_design_features" >}}) {#cell-signalling-iii-underlying-design-features--cell-signalling-iii-underlying-design-features-dot-md}

<!--list-separator-->

-  **🔖 AM vs FM signaling > Macromolecule complexes**

    <!--list-separator-->

    -  [Pyruvate DH]({{< relref "pyruvate_dehydrogenase_complex" >}})

        -   _E1_: 8 trimers of lipoamide reductase-transacetylase
        -   _E2_: 6 dimers of dihydrolipoyl DH
        -   _E3_: 12 dimers of pyruvate decarboxylase

        {{< figure src="/ox-hugo/_20210830_222433Screenshot 2021-08-30 221729.png" width="700" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
