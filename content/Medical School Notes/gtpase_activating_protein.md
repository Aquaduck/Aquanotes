+++
title = "GTPase-activating protein"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Discuss the monomeric GTPases and how they are regulated, with specific reference to Ras.]({{< relref "discuss_the_monomeric_gtpases_and_how_they_are_regulated_with_specific_reference_to_ras" >}}) {#discuss-the-monomeric-gtpases-and-how-they-are-regulated-with-specific-reference-to-ras-dot--discuss-the-monomeric-gtpases-and-how-they-are-regulated-with-specific-reference-to-ras-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Ras > Inactivation**

    Requires GTPase-Activating Protein ([GAP]({{< relref "gtpase_activating_protein" >}}))

    ---


#### [Describe the general mechanisms of packaging cargo into vesicles.]({{< relref "describe_the_general_mechanisms_of_packaging_cargo_into_vesicles" >}}) {#describe-the-general-mechanisms-of-packaging-cargo-into-vesicles-dot--describe-the-general-mechanisms-of-packaging-cargo-into-vesicles-dot-md}

<!--list-separator-->

-  **🔖 From Organelles and Trafficking I-II > Role of GTPases in coating/uncoating cycle**

    [GAP]({{< relref "gtpase_activating_protein" >}}) dephosphorylates ARF1 -> formation of ARF1-GDP

    ---

<!--list-separator-->

-  **🔖 From Organelles and Trafficking I-II > Role of GTPases in coating/uncoating cycle**

    Regulated by [GEF]({{< relref "guanine_nucleotide_exchange_factor" >}}) and [GAP]({{< relref "gtpase_activating_protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
