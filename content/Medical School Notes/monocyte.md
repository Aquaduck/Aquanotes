+++
title = "Monocyte"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A precursor to some [macrophages]({{< relref "macrophage" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Tumor necrosis factor]({{< relref "tumor_necrosis_factor" >}}) {#tumor-necrosis-factor--tumor-necrosis-factor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Produced by [monocytes]({{< relref "monocyte" >}})/macrophages

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
