+++
title = "List the pathways for which glucose-6-phosphate is the initial substrate"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## Pathways {#pathways}

-   [Glycolysis]({{< relref "glycolysis" >}})
-   [Pentose phosphate pathway]({{< relref "pentose_phosphate_pathway" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the pathways for which glucose-6-phosphate is the initial substrate]({{< relref "list_the_pathways_for_which_glucose_6_phosphate_is_the_initial_substrate" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
