+++
title = "Systole"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Regional and Peripheral Circulation Pre-reading]({{< relref "regional_and_peripheral_circulation_pre_reading" >}}) {#regional-and-peripheral-circulation-pre-reading--regional-and-peripheral-circulation-pre-reading-dot-md}

<!--list-separator-->

-  **🔖 Coronary Blood Flow and the Cardiac Cycle**

    <!--list-separator-->

    -  [Systole]({{< relref "systole" >}})

        -   Coronary arterial inflow varies during the cardiac cycle, **particularly in the left ventricle**
        -   Intramyocardial coronary arterial vessels are squeezed by contracting myocardium during systole
        -   Reduction of vessel radius + increase in coronary vascular resistance -> **reduced coronary arterial inflow**
        -   Aortic pressure is high enough during left ventricular ejection -> blood travels through elevated coronary vascular resistance
        -   Variations in blood flow less in right ventricle
            -   Because lower pressure + thinner-walled


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    The measurement of [systolic]({{< relref "systole" >}}) blood pressure is adequate to clinically measure [afterload]({{< relref "afterload" >}})

    ---


#### [Afterload]({{< relref "afterload" >}}) {#afterload--afterload-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The force of resistance the ventricle must overcome to empty contents at the beginning of [systole]({{< relref "systole" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
