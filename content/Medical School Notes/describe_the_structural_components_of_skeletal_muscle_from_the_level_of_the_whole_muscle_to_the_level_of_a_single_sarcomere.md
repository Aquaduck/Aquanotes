+++
title = "Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere."
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}}) {#from-brs-cell-biology-and-histology--brs-cell-biology-histology-dot-md}


### Overview - [Muscle]({{< relref "muscle" >}}) {#overview-muscle--muscle-dot-md}

-   Muscle is classified into two types: _[striated]({{< relref "striated_muscle" >}})_ and _[smooth]({{< relref "smooth_muscle" >}})_
    -   Striated is further subdivided into _[skeletal]({{< relref "skeletal_muscle" >}})_ and _[cardiac]({{< relref "cardiac_muscle" >}})_ muscles
-   Muscle cells possess _contractile filaments_ whose major components are [actin]({{< relref "actin" >}}) and [myosin]({{< relref "myosin" >}})


### Structure of Skeletal Muscle (p. 109) {#structure-of-skeletal-muscle--p-dot-109}


#### Connective tissue investments {#connective-tissue-investments}

-   Convey neural and vascular elements ot muscle cells and provide a vehicle that harnesses the force of muscle contraction

<!--list-separator-->

-  Epimysium

    -   Surrounds an entire muscle and forms aponeuroses and tendons
        -   [Aponeurosis]({{< relref "aponeurosis" >}}) connects muscle to muscle
        -   [Tendons]({{< relref "tendon" >}}) connect muscle to bone

<!--list-separator-->

-  Perimysium

    -   Surrounds _fascicles_ (small bundles) of muscle cells

<!--list-separator-->

-  Endomysium

    -   Surrounds invididual muscle cells and is composed of _reticular fibers_ and an _external lamina_


#### <span class="org-todo todo TODO">TODO</span> Types of skeletal muscle cells {#types-of-skeletal-muscle-cells}


#### [Skeletal muscle cells]({{< relref "skeletal_muscle_cell" >}}) {#skeletal-muscle-cells--skeletal-muscle-cell-dot-md}

-   Long, cylindrical, multinucleated cells enveloped by an external lamina and reticular fibers
-   Skeletal muscle cell cytoplasm is called a [sarcoplasm]({{< relref "sarcoplasm" >}})
-   Skeletal muscle cell membrane is called a [sarcolemma]({{< relref "sarcolemma" >}}) (variation of [plasmalemma]({{< relref "plasma_membrane" >}}) - another name for cell membrane)
    -   Form deep tubular invaginations called [Transverse (T) tubules]({{< relref "transverse_tubule" >}})

<!--list-separator-->

-  [Myofibril]({{< relref "myofibril" >}})

    -   Extend the entire length of the cell
    -   Composed of longitudinally arranged, cylindrical bundles of [thick]({{< relref "thick_myofilament" >}}) and [thin myofilaments]({{< relref "thin_myofilament" >}})

<!--list-separator-->

-  [Sarcomere]({{< relref "sarcomere" >}})

    <!--list-separator-->

    -  Structure diagram

        {{< figure src="/ox-hugo/_20210913_202331screenshot.png" caption="Figure 1: Components of a sarcomere in myofibril" >}}

        -   Labeled in diagram above:
            -   Z disk
            -   A band
            -   I band
            -   H zone
            -   Myosin/thick filament
            -   Actin/thin filament

<!--list-separator-->

-  [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) (SR)

    -   Modified smooth endoplasmic reticulum (SER) that surrounds myofilaments and forms a meshwork around each myofibril
    -   **Regulates muscle contraction** by sequestering calcium ions (leading to relaxation) or releasing calcium ions (leading to contraction)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-13 Mon] </span></span> > Muscle Structure/Crossbridge Cycle > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
