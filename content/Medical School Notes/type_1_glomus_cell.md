+++
title = "Type 1 glomus cell"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [glomus cell]({{< relref "glomus_cell" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Carotid & Aortic bodies (p. 671)**

    [Type I]({{< relref "type_1_glomus_cell" >}}):

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
