+++
title = "Bowman capsule"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Encloses the [Glomerulus]({{< relref "glomerulus" >}}) within the [Renal corpuscle]({{< relref "renal_corpuscle" >}})


## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Ascending thick limb of the loop of Henle**

    Each thick ascending limb rises back into the cortex to the [Bowman's capsule]({{< relref "bowman_capsule" >}})

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Proximal tubule**

    Drains [Bowman capsule]({{< relref "bowman_capsule" >}})

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18)**

    The _Renal tubule_ begins at and leads out of the [Bowman capsule]({{< relref "bowman_capsule" >}}) on the side **opposite the vascular pole**

    ---


#### [Glomerulus]({{< relref "glomerulus" >}}) {#glomerulus--glomerulus-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A tuft of capillaries within the [Bowman capsule]({{< relref "bowman_capsule" >}}) of the kidneys formed of three parts

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    [Urine]({{< relref "urine" >}}) formation begins with glomerular filtration: the bulk flow of fluid from the glomerular capillaries into [Bowman's capsule]({{< relref "bowman_capsule" >}})

    ---


#### [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}}) {#describe-glomerular-anatomy--ie-efferent-afferent-arteriole----describe-glomerular-anatomy-ie-efferent-afferent-arteriole-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal corpuscle (p. 17)**

    [Bowman's space]({{< relref "bowman_s_space" >}}): Space within [Bowman capsule]({{< relref "bowman_capsule" >}}) that is not occupied by capillaries and mesangial cells

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal corpuscle (p. 17)**

    Two closely spaced arterioles penetrate the [Bowman capsule]({{< relref "bowman_capsule" >}}) at a region called the _vascular pole_:

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal corpuscle (p. 17)**

    Hollow sphere ([Bowman capsule]({{< relref "bowman_capsule" >}})) composed of epithelial cells

    ---


#### [Bowman's space]({{< relref "bowman_s_space" >}}) {#bowman-s-space--bowman-s-space-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Space within [Bowman capsule]({{< relref "bowman_capsule" >}}) that is not occupied by capillaries and mesangial cells

    ---


#### [Ascending thick limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}}) {#ascending-thick-limb-of-the-loop-of-henle--ascending-thick-limb-of-the-loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Rises back into the [Bowman's capsule]({{< relref "bowman_capsule" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
