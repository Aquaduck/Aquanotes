+++
title = "Pharmacology: General Principles of Pharmacodynamics II"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Receptor signaling: drug effects {#receptor-signaling-drug-effects}

-   In order for a drug to reach its target it must either:
    1.  diffuse through membranes (hydrophobic)
    2.  Be actively taken up via transporters
    3.  Bind to membrane receptors outside the cell
-   Target tissues can express multiple receptors that have opposing effects
    -   _Physiologic agonist/antagonist_
        -   Very important for homeostasis and can be used in therapy
-   There are **five main classes** of receptors (and subclasses within each):


### Receptors for lipid-soluble agents {#receptors-for-lipid-soluble-agents}

-   Receive **membrane-permeable** chemical signals
-   Ligands include:
    -   Cell-permeable enzyme inhibitors (e.g. aspirin)
    -   Small molecules (e.g. Nitric oxide)
    -   [Steroids]({{< relref "steroid" >}})
        -   Steroids are lipophilic -> can cross membrane directly
        -   Binds its receptor -> removes **hsp90** protein from the molecule -> translocation of the receptor to the nucleus -> interacts w/ DNA to alter gene transcription


### Ligand-regulated transmembrane enzymes {#ligand-regulated-transmembrane-enzymes}

-   Bind to ligand **extracellularly** -> generate an **intracellular intrinsic enzyme activity**
-   For most of these types of receptors, ligand binding -> **dimerization of the receptor** -> enzyme activation
    -   This can modify the receptor itself or cytoplasmic proteins
        -   Affects both localization and activity of downstream signaling molecules


### Cytokine receptors {#cytokine-receptors}

-   Use a mechanism closely resembling that of receptor tyrosine kinases
    -   **Difference**: protein tyrosine kinase activity is **not intrinsic to the receptor molecule**
        -   Instead, a separate protein tyrosine kinase from the Janus-kinase (JAK) family binds noncovalently to the receptor upon ligand binding -> activates a cascade of signaling through STAT (signal transducers and activators of transcription) proteins -> alter gene expression


### Ion channels (ionotropic receptors) {#ion-channels--ionotropic-receptors}

-   Span the plasma membrane
-   Allow **regulated ion flux across membranes**


#### Ligand-gated {#ligand-gated}

-   Channel opening regulated by the **binding of an agonist**


#### Voltage-gated {#voltage-gated}

-   Open in response to **changes in membrane potential**


#### Second-messenger regulated channels {#second-messenger-regulated-channels}

-   Open/closed in response to **intracellular second messenger signaling** at the membrane or via diffusible second messenger (i.e. G protein subunit or cAMP-mediated regulation)


### [G protein-coupled receptors]({{< relref "g_protein_coupled_receptors" >}}) {#g-protein-coupled-receptors--g-protein-coupled-receptors-dot-md}

-   _G_ refers to guanine nucleotide
-   Link extracellular signals to intracellular enzymes or ion channels via heterotrimeric G proteins
-   When receptor binds to agonist:
    -   Agonist interacts with G protein -> exchange of GTP to GDP
    -   When **bound to GTP** -> G protein can activate/inhibit enzymes
        -   Primarily dependent on the **G protein alpha subunit**
-   Several different [G proteins]({{< relref "g_protein" >}}) that couple to numerous receptors and can have opposing effects:

    <div class="table-caption">
      <span class="table-number">Table 1</span>:
      G protein-coupled receptors
    </div>

    | G protein     | Receptors                                                | Signaling pathway                             |
    |---------------|----------------------------------------------------------|-----------------------------------------------|
    | G<sub>s</sub> | Beta adrenergic receptors                                | Increase cAMP                                 |
    |               | Glucagon                                                 | Excitatory effects                            |
    |               | Histamine                                                |                                               |
    |               | Serotonin                                                |                                               |
    | G<sub>i</sub> | Alpha<sub>2</sub> adrenergic receptors                   | Decrease cAMP                                 |
    |               | mAchR                                                    | Cardiac K+ channel open (decrease heart rate) |
    |               | Opioid                                                   |                                               |
    |               | Serotonin                                                |                                               |
    | G<sub>q</sub> | mAchR                                                    | PLC - IP<sub>3</sub>                          |
    |               | H1                                                       | DAG                                           |
    |               | α1                                                       | Increase cytoplasmic calcium                  |
    |               | Vasopressin type 1                                       |                                               |
    |               | 5HT<sub>1C</sub>                                         |                                               |
    | G<sub>t</sub> | Rhodopsin and color opsins in retinal rod and cone cells | Increase cGMP phosphodiesterase               |
    |               |                                                          | Decrease cGMP                                 |


## Receptor desensitization and downregulation {#receptor-desensitization-and-downregulation}

-   G protein-mediated responses to drugs and hormonal agonists attenuate with time
    -   In some cases, can be reversible:
        -   **Second exposure to agonist**, if provided a **few minutes after termination of first exposure**, results in a response similar to the initial response
-   _Desensitization_ typically occurs within a few minutes via **phosphorylation of the receptor**
    -   Upon removal of agonist, phosphorylation is terminated -> cellular phosphatases reverse the desensitized state (_resensitization_)
-   _Down-regulation_ occurs after prolonged exposure to an agonist -> **internalization of receptors**
-   _Tolerance_: clinical decrease in response to a drug that is used repeatedly
    -   Can involve down-regulation, changes in expression of receptors, or altered metabolism of the drug (_pharmacokinetic tolerance_)


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
