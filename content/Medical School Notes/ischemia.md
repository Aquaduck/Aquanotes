+++
title = "Ischemia"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   Decreased [Blood]({{< relref "blood" >}}) supply that cannot meet [Oxygen]({{< relref "oxygen" >}}) demands of an organ or tissue


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Myocardial infarction]({{< relref "myocardial_infarction" >}}) {#myocardial-infarction--myocardial-infarction-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Infarction]({{< relref "infarction" >}}) of [Heart]({{< relref "heart" >}}) tissue due to prolonged [Ischemia]({{< relref "ischemia" >}})

    ---


#### [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}}) {#describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot--describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    When a coronary artery becomes stenotic (narrowed) or occluded (closed off) -> region of heart supplied by that artery as [ischemia]({{< relref "ischemia" >}}) (lack of oxygen)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
