+++
title = "DNA Polymerase-ɑ"
author = ["Arif Ahsan"]
date = 2021-08-17T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   A type of [DNA Polymerase]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes**

    [pol α]({{< relref "dna_polymerase_ɑ" >}}) (alpha)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
