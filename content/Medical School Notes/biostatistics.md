+++
title = "Biostatistics"
author = ["Arif Ahsan"]
date = 2021-07-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

Statistics stuff


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [ScienceDirect - Significance Level]({{< relref "sciencedirect_significance_level" >}}) {#sciencedirect-significance-level--sciencedirect-significance-level-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "biostatistics" >}})

    -   The _significance level_ of an event is **the probability that the event could have occurred by chance**
        -   If the level is low, the probability of an event occurring by chance is low -> **the event is significant**
    -   Statistical significance does **not** mean that the event has any clinical meaning


#### [ScienceDirect - Null Hypothesis]({{< relref "sciencedirect_null_hypothesis" >}}) {#sciencedirect-null-hypothesis--sciencedirect-null-hypothesis-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "biostatistics" >}})

    -   The _null hypothesis_ suggests that **no statistical relationship and significance exists between two sets of observed data**
    -   Symbolized by \\(H\_{0}\\)


#### [Osmosis - Statistical Probability Distributions]({{< relref "osmosis_statistical_probability_distributions" >}}) {#osmosis-statistical-probability-distributions--osmosis-statistical-probability-distributions-dot-md}

<!--list-separator-->

-  [Notes]({{< relref "biostatistics" >}})

    <!--list-separator-->

    -  Normal Distribution

        -   Data grouped around central value, without left/right bias, in "bell curve" shape

    <!--list-separator-->

    -  Z-scores

        -   Standardized score representing deviation from the mean
        -   Uses data set mean, standard deviation to determine measurement location
        -   Expressed in standard-deviations


#### [Osmosis - Introduction to Biostatistics]({{< relref "osmosis_introduction_to_biostatistics" >}}) {#osmosis-introduction-to-biostatistics--osmosis-introduction-to-biostatistics-dot-md}

<!--list-separator-->

-  [Notes]({{< relref "biostatistics" >}})

    <!--list-separator-->

    -  Variance

        -   Sum of squared deviations from mean, divided by number of distributions
            -   \\(\sigma^{2} = \frac{\sum\_{}^{}(x - x)^{2}}{n}\\)

    <!--list-separator-->

    -  Standard deviation (SD)

        -   Square root of variance
            -   \\(\sigma = \sqrt{\frac{\sum\_{}^{}(x - x)^{2}}{n}}\\)

    <!--list-separator-->

    -  Graphic Description of Data     :ATTACH:

        {{< figure src="/ox-hugo/_20210714_123002screenshot.png" >}}

        <!--list-separator-->

        -  Normal (Gaussian) curve

            -   Symmetrical distribution of scores around the mean
                -   Forms classic bell shape
                -   Values lie within **2 SD** of mean
                -   Most natural phenomena show this type of distribution
                -   Parametric tests utilized in research

        <!--list-separator-->

        -  Non-Gaussian curve

            -   _Asymmetrical_ distribution of scores around mean
                -   Skewed (negatively/positively) curve
                -   Kurtotic (flat/peaked) curve
                -   Nonparametric tests utilized in research

    <!--list-separator-->

    -  Mean

        -   Calculated by \*adding each value in data set -> dividing by total number of data points
        -   **Can be influenced by outliers**

    <!--list-separator-->

    -  Median

        -   Calculates central value when possible outliers present
        -   Divides set of data into two haves

    <!--list-separator-->

    -  Mode

        -   Central value appearing most often in data sequence
        -   **Not affected by outliers**


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Statistics > 3. Define and indicate how you would apply the following:**

    [Biostatistics]({{< relref "biostatistics" >}})

    ---

<!--list-separator-->

-  **🔖 Statistics > 2. Describe the influence that symmetric, skewed, and bimodal distributions have on the mean, median, and mode**

    [Biostatistics]({{< relref "biostatistics" >}})

    ---

<!--list-separator-->

-  **🔖 Statistics > 1. Define, calculate, and interpret descriptive statistics concepts: mean, median, mode, range, and standard deviation**

    [Biostatistics]({{< relref "biostatistics" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
