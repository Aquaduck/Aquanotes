+++
title = "Epiphyseal plate"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Growth plate found on [Long bone]({{< relref "tubular_bone" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone formation > Endochondral bone formation > Steps**

    As proliferating cartilage of the [Growth plate]({{< relref "epiphyseal_plate" >}}) grows away from the [Diaphysis]({{< relref "diaphysis" >}}) (extending the length of the bone) -> cartilage closest to the diaphysis is **converted to ossified bone**

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone formation > Endochondral bone formation > Steps**

    Cartilage of [Growth plate]({{< relref "epiphyseal_plate" >}}) proliferates to lengthen the bone

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone formation > Endochondral bone formation > Steps**

    A region of cartilage is preserved between the [Epiphysis]({{< relref "epiphysis" >}}) and the [Diaphysis]({{< relref "diaphysis" >}}) -> becomes the [Growth plate]({{< relref "epiphyseal_plate" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Parts of a tubular bone**

    Sometimes the [epiphyseal plate]({{< relref "epiphyseal_plate" >}}) is called the physis

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
