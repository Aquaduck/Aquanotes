+++
title = "Pyruvate DH kinase"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Phosphorylates Ser residues on the first enzyme of the [PDH]({{< relref "pyruvate_dehydrogenase_complex" >}}) complex (E1)
-   [Allosterically]({{< relref "allosteric_regulation" >}}) stimulated by the products of the reaction
    -   Inhibited by its substrates


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Downregulation via [pyruvate DH kinase]({{< relref "pyruvate_dh_kinase" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Pyruvate metabolism > Pyruvate dehydrogenase complex**

    <!--list-separator-->

    -  [Pyruvate DH kinase]({{< relref "pyruvate_dh_kinase" >}})

        -   Phosphorylates Ser residues on the first enzyme of the PDH complex (E1)
            -   Results in significant **down-regulation of its activity**
        -   Allosterically stimulated by the products of the reaction
        -   Allosterically inhibited by its substrates
        -   Also regulated by the energy charge <- kinase allosterically inhibited by ADP


#### [Hypoxia-inducible factor 1]({{< relref "hypoxia_inducible_factor_1" >}}) {#hypoxia-inducible-factor-1--hypoxia-inducible-factor-1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Stimulates production of [PDH kinase]({{< relref "pyruvate_dh_kinase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
