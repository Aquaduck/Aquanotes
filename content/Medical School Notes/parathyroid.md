+++
title = "Parathyroid"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Parathyroid hormone]({{< relref "parathyroid_hormone" >}}) {#parathyroid-hormone--parathyroid-hormone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Secreted by the [parathyroid glands]({{< relref "parathyroid" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
