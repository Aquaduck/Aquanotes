+++
title = "MutL"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [MutL homolog]({{< relref "mutl_homolog" >}}) {#mutl-homolog--mutl-homolog-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Form heterodimers that mimic [MutL]({{< relref "mutl" >}}) in _E. coli_

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
