+++
title = "Elastic cartilage"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Perichondrium]({{< relref "perichondrium" >}}) {#perichondrium--perichondrium-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    Found in [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) and [Elastic cartilage]({{< relref "elastic_cartilage" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
