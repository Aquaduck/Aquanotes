+++
title = "Fatty acyl-CoA"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  [Fatty acyl-CoA]({{< relref "fatty_acyl_coa" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   In the **investment phase of fatty acid oxidation**
        -   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
            -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
            -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**


### Unlinked references {#unlinked-references}

[Show unlinked references]
