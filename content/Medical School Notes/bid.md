+++
title = "Bid"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A [BH3-only protein]({{< relref "bh3_only_protein" >}})
-   Links the [intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}}) and [extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026) > BH3-only proteins**

    <!--list-separator-->

    -  [Bid]({{< relref "bid" >}})

        -   Normally inactive
        -   When [death receptors]({{< relref "death_receptor" >}}) activate the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}), [Caspase-8]({{< relref "caspase_8" >}}) cleaves Bid -> activates Bid -> translocates to outer mitochondrial membrane -> inhibits anti-apoptotic Bcl2 family proteins -> **amplification of death signal**


#### [Caspase-8]({{< relref "caspase_8" >}}) {#caspase-8--caspase-8-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Activates [Bid]({{< relref "bid" >}}) via cleavage

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
