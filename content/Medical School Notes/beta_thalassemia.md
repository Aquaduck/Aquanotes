+++
title = "Beta thalassemia"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [FoCS (Sickle Cell Case)]({{< relref "focs_sickle_cell_case" >}}) {#focs--sickle-cell-case----focs-sickle-cell-case-dot-md}

<!--list-separator-->

-  **🔖 Clinical features of SCD > Vaso-occlusive complications/events**

    Historically most children with [Hb]({{< relref "hemoglobin" >}}) S/S or S/[beta-thalassemia]({{< relref "beta_thalassemia" >}}) will have a dysfunctional spleen w/in first year of life and complete auto-infarction and atrophy due to ischemia of the spleen by five years

    ---

<!--list-separator-->

-  **🔖 Genetics**

    Some _HBB_ mutations can result in usually low levels of _beta-globin_ -> called _[beta thalassemia]({{< relref "beta_thalassemia" >}})_

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
