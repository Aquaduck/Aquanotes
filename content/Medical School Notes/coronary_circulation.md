+++
title = "Coronary circulation"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Circulation of blood that perfuses the [Heart]({{< relref "heart" >}})

{{< figure src="/ox-hugo/_20211026_231917screenshot.png" width="500" >}}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
