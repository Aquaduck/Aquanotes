+++
title = "Right ventricle"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The right ventricle of the [heart]({{< relref "heart" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) {#ventricular-septal-defect--ventricular-septal-defect-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An abnormal communication between the [left]({{< relref "left_ventricle" >}}) and [right ventricle]({{< relref "right_ventricle" >}}) -> [left to right shunting]({{< relref "left_to_right_shunt" >}}) of blood flow

    ---


#### [Right ventricular outflow tract obstruction]({{< relref "right_ventricular_outflow_tract_obstruction" >}}) {#right-ventricular-outflow-tract-obstruction--right-ventricular-outflow-tract-obstruction-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inability of blood to pass out of the [right ventricle]({{< relref "right_ventricle" >}}) through the [pulmonic valve]({{< relref "pulmonary_valve" >}}).

    ---


#### [Right ventricular hypertrophy]({{< relref "right_ventricular_hypertrophy" >}}) {#right-ventricular-hypertrophy--right-ventricular-hypertrophy-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A thickening of the muscular wall of the [right ventricle]({{< relref "right_ventricle" >}}) of the heart

    ---


#### [Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.]({{< relref "describe_the_common_clinical_procedures_involving_the_heart_include_a_description_of_the_procedure_s_and_the_structure_s_that_are_accessed_or_repaired" >}}) {#describe-the-common-clinical-procedures-involving-the-heart-dot-include-a-description-of-the-procedure--s--and-the-structure--s--that-are-accessed-or-repaired-dot--describe-the-common-clinical-procedures-involving-the-heart-include-a-description-of-the-procedure-s-and-the-structure-s-that-are-accessed-or-repaired-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Clinical procedures related to heart structures: > Cardiac catheterization:**

    Radiopaque catheter inserted into peripheral vein (e.g. femoral vein) and passed under fluoroscopic control into the [right atrium]({{< relref "right_atrium" >}}), [right ventricle]({{< relref "right_ventricle" >}}), [pulmonary trunk]({{< relref "pulmonary_trunk" >}}) and [pulmonary arteries]({{< relref "pulmonary_artery" >}}) respectively

    ---


#### [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}}) {#describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot--describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    [Right ventricle]({{< relref "right_ventricle" >}}) + [right atria]({{< relref "right_atrium" >}}) do not develop as much pressure -> less oxygen demand -> more rarely infarcted

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
