+++
title = "Hydrothorax"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the presentation of pneumothorax, hydrothorax and hemothorax.]({{< relref "describe_the_presentation_of_pneumothorax_hydrothorax_and_hemothorax" >}}) {#describe-the-presentation-of-pneumothorax-hydrothorax-and-hemothorax-dot--describe-the-presentation-of-pneumothorax-hydrothorax-and-hemothorax-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Hydrothorax]({{< relref "hydrothorax" >}})

        -   Accumulation of a significant amount of fluid in pleural cavity
        -   Can result from pleural effusion


### Unlinked references {#unlinked-references}

[Show unlinked references]
