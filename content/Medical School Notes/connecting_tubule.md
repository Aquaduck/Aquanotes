+++
title = "Connecting tubule"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Part of the [Nephron]({{< relref "nephron" >}}) that follows the [Distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Distal convoluted tubule**

    Followed by the [connecting tubule]({{< relref "connecting_tubule" >}}) -> initial collecting tubule of the cortical collecting duct

    ---


#### [Cortical collecting duct]({{< relref "cortical_collecting_duct" >}}) {#cortical-collecting-duct--cortical-collecting-duct-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [Nephron]({{< relref "nephron" >}}) that follows the [connecting tubule]({{< relref "connecting_tubule" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
