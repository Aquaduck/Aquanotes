+++
title = "Vitamin D"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A class of [Vitamin]({{< relref "vitamin" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Absorption (p. 1144) > Mineral absorption > Calcium**

    [PTH]({{< relref "parathyroid_hormone" >}}) upregulates the activation of [vitamin D]({{< relref "vitamin_d" >}}) in the [kidney]({{< relref "kidney" >}}) -> facilitates intestinal calcium ion absorption

    ---


#### [Kidney]({{< relref "kidney" >}}) {#kidney--kidney-dot-md}

<!--list-separator-->

-  **🔖 Concept links > Major functions**

    Regulation of [vitamin D]({{< relref "vitamin_d" >}}) production

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Major physiological functions of the kidney > Hormone synthesis**

    [Calciferol]({{< relref "vitamin_d" >}})

    ---


#### [Beta globulin]({{< relref "beta_globulin" >}}) {#beta-globulin--beta-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Transport [iron]({{< relref "iron" >}}), [lipids]({{< relref "lipid" >}}), and the fat-soluble vitamins [A]({{< relref "vitamin_a" >}}), [D]({{< relref "vitamin_d" >}}), [E]({{< relref "vitamin_e" >}}), and [K]({{< relref "vitamin_k" >}}) to cells

    ---


#### [Alpha globulin]({{< relref "alpha_globulin" >}}) {#alpha-globulin--alpha-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Transport [iron]({{< relref "iron" >}}), [lipids]({{< relref "lipid" >}}), and the fat-soluble vitamins [A]({{< relref "vitamin_a" >}}), [D]({{< relref "vitamin_d" >}}), [E]({{< relref "vitamin_e" >}}), and [K]({{< relref "vitamin_k" >}}) to cells

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
