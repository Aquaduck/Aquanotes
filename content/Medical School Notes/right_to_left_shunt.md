+++
title = "Right-to-left shunt"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A class of [shunts]({{< relref "shunt" >}}) where [blood]({{< relref "blood" >}}) fails to be oxygenated -> deoxygenated blood enters the [systemic circuit]({{< relref "systemic_circuit" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Shunts**

    <!--list-separator-->

    -  [Right-to-left shunt]({{< relref "right_to_left_shunt" >}})

        -   Blood **fails to be oxygenated** -> **deoxygenated blood enters the systemic circuit**


### Unlinked references {#unlinked-references}

[Show unlinked references]
