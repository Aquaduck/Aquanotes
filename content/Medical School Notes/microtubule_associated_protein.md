+++
title = "Microtubule associated protein"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}}) {#from-molecular-biology-of-the-cell-sixth-edition--molecular-biology-of-the-cell-sixth-edition-dot-md}


### Glossary (p. G:20) {#glossary--p-dot-g-20}

-   Any protein that binds to [microtubules]({{< relref "microtubule" >}}) and modifies their properties


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [MAP2]({{< relref "map2" >}}) {#map2--map2-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [microtubule associated protein]({{< relref "microtubule_associated_protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
