+++
title = "Pyruvate carboxylase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Requires [biotin]({{< relref "biotin" >}})
-   Catalyzes ATP-dependent carboxylation of [pyruvate]({{< relref "pyruvate" >}}) to [oxaloacetate]({{< relref "oxaloacetate" >}})
-   Allosterically activated by [Acetyl-CoA]({{< relref "acetyl_coa" >}})


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [List the four enzymes, and their intracellular location, for which pyruvate serves as a substrate, and the product that is made]({{< relref "list_the_four_enzymes_and_their_intracellular_location_for_which_pyruvate_serves_as_a_substrate_and_the_product_that_is_made" >}}) {#list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made--list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made-dot-md}

<!--list-separator-->

-  **🔖 Enzymes > Mitochondria**

    [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) uses [Biotin]({{< relref "biotin" >}}) (B7)

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis > PEP carboxykinase**

    This + [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) required to convert pyruvate into PEP

    ---

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    <!--list-separator-->

    -  [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}})

<!--list-separator-->

-  **🔖 Pyruvate metabolism**

    <!--list-separator-->

    -  [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}})

        -   [Biotin]({{< relref "biotin" >}})-requiring mitochondrial enzyme
        -   Catalyzes ATP-dependent carboxylation of pyruvate to oxaloacetate
            -   Then converted to PEP by PEP carboxylase for use in gluconeogenesis
        -   Allosterically activated by acetyl-CoA
            -   The ability of Acetyl-CoA to inhibit PDH complex and activate pyruvate carboxylase ensures that **the pyruvate generated in the liver mitochondria will be used as a gluconeogenic substrate**


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By enzyme**

    <!--list-separator-->

    -  [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}})

        -   Biotin (B7) is a cofactor of carboxylases


#### [Biotin]({{< relref "biotin" >}}) {#biotin--biotin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}): pyruvate (3C) -> oxaloacetate (4C)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
