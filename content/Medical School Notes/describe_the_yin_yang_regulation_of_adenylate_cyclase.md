+++
title = "Describe the Yin-Yang regulation of adenylate cyclase."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### The Yin and Yang of cAMP Regulation {#the-yin-and-yang-of-camp-regulation}

-   More than one [GPCR]({{< relref "g_protein_coupled_receptors" >}}) can be coupled to the same intracellular signal pathway
-   Individual signal pathways often are subject to both stimulatory and inhibitory arms - each of which can be regulated indpeendently


#### Opposing G proteins (G<sub>s</sub> vs G<sub>i</sub>) regulating cAMP {#opposing-g-proteins--g--regulating-camp}

-   cAMP can be regulated by changing activity of **either G<sub>s</sub> or G<sub>i</sub>** -> **multiple pathways to regulate cAMP levels**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the Yin-Yang regulation of adenylate cyclase.]({{< relref "describe_the_yin_yang_regulation_of_adenylate_cyclase" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
