+++
title = "Aorta"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Overriding aorta]({{< relref "overriding_aorta" >}}) {#overriding-aorta--overriding-aorta-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Aorta]({{< relref "aorta" >}}) is displaced over the [ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) (instead of the left ventricle).

    ---


#### [Left ventricle]({{< relref "left_ventricle" >}}) {#left-ventricle--left-ventricle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Ejects oxygenated [blood]({{< relref "blood" >}}) to the [aorta]({{< relref "aorta" >}}) through the [aortic valve]({{< relref "aortic_valve" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Ductus arteriosus]({{< relref "ductus_arteriosus" >}}): connects [pulmonary artery]({{< relref "pulmonary_artery" >}}) and [aorta]({{< relref "aorta" >}})

    ---


#### [Ductus arteriosus]({{< relref "ductus_arteriosus" >}}) {#ductus-arteriosus--ductus-arteriosus-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Connects [pulmonary artery]({{< relref "pulmonary_artery" >}}) and [aorta]({{< relref "aorta" >}})

    ---


#### [Ascending aorta]({{< relref "ascending_aorta" >}}) {#ascending-aorta--ascending-aorta-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The portion of the [aorta]({{< relref "aorta" >}}) that rises immediately after the heart

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
