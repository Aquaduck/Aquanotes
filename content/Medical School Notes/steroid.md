+++
title = "Steroid"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Pharmacology: General Principles of Pharmacodynamics II]({{< relref "pharmacology_general_principles_of_pharmacodynamics_ii" >}}) {#pharmacology-general-principles-of-pharmacodynamics-ii--pharmacology-general-principles-of-pharmacodynamics-ii-dot-md}

<!--list-separator-->

-  **🔖 Receptor signaling: drug effects > Receptors for lipid-soluble agents**

    [Steroids]({{< relref "steroid" >}})

    ---


#### [Describe the basic mechanism of action of steroid hormones both in terms of intracellular and surface receptors.]({{< relref "describe_the_basic_mechanism_of_action_of_steroid_hormones_both_in_terms_of_intracellular_and_surface_receptors" >}}) {#describe-the-basic-mechanism-of-action-of-steroid-hormones-both-in-terms-of-intracellular-and-surface-receptors-dot--describe-the-basic-mechanism-of-action-of-steroid-hormones-both-in-terms-of-intracellular-and-surface-receptors-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  Hydrophobic signal molecules - [steroids]({{< relref "steroid" >}}) + thyroid hormones

        -   Examples:
            -   Testosterone
            -   Estradiol
            -   Cortisol
            -   Aldosterone
            -   Vitamin D<sub>3</sub>

        <!--list-separator-->

        -  Intracellular mechanism fundamentals

            1.  Hormone diffuses through plasma membrane -> binds to high-affinity intracellular receptor
                -   This traps the steroid hormone
            2.  Binding causes a change in conformation in recepter that results in **dimerization**
            3.  Occupied, dimerized receptor migrates to the nucleus (if not already there) -> binds to specific transcriptional regulatory elements to either turn on or off specific genes

        <!--list-separator-->

        -  Steroid hormones also act on plasma membrane receptors

            -   Second messenger mechanism similar to that for hydrophilic hormones


### Unlinked references {#unlinked-references}

[Show unlinked references]
