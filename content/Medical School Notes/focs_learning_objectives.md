+++
title = "FoCS Learning Objectives"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "agenda"]
draft = false
+++

## <span class="org-todo todo PROJ">PROJ</span> Assignments {#assignments}


## <span class="org-todo todo PROJ">PROJ</span> Learning Objectives {#learning-objectives}


### Block 2 {#block-2}


#### Week 1 {#week-1}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Metabolism: Organ Specialization and Regulation

        <!--list-separator-->

        - <span class="org-todo todo HOLD">HOLD</span>  Pre-work <code>[12/13]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Define what is meant by the energy charge and redox potential of a cell]({{< relref "define_what_is_meant_by_the_energy_charge_and_redox_potential_of_a_cell" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the pathways for which glucose-6-phosphate is the initial substrate]({{< relref "list_the_pathways_for_which_glucose_6_phosphate_is_the_initial_substrate" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the glucose-utilizing pathways that predominate in:]({{< relref "list_the_glucose_utilizing_pathways_that_predominate_in" >}})

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  RBCs

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Brain

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Muscle

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Adipose

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Liver

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the four enzymes, and their intracellular location, for which pyruvate serves as a substrate, and the product that is made]({{< relref "list_the_four_enzymes_and_their_intracellular_location_for_which_pyruvate_serves_as_a_substrate_and_the_product_that_is_made" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}})

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Glycolysis

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Pyruvate DH complex

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  TCA cycle

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Fatty acid oxidation

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Gluconeogenesis

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Distinguish the insulin-responsive glucose membrane transporter and in what tissues it is located, from that used by:]({{< relref "distinguish_the_insulin_responsive_glucose_membrane_transporter_and_in_what_tissues_it_is_located_from_that_used_by" >}})

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Brain

                <!--list-separator-->

                - <span class="org-todo done _X_">[X]</span>  Liver & Pancreas

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [List those tissues that respond to insulin vs. glucagon, and epinephrine]({{< relref "list_those_tissues_that_respond_to_insulin_vs_glucagon_and_epinephrine" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Name the second messenger produced in response to glucagon-induced cellular signaling and how it functions to elicit the appropriate intracellular response]({{< relref "name_the_second_messenger_produced_in_response_to_glucagon_induced_cellular_signaling_and_how_it_functions_to_elicit_the_appropriate_intracellular_response" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Identify the fructose metabolite that reciprocally regulates the glycolytic and gluconeogenic pathways within the liver and the enzyme that catalyzes its formation]({{< relref "identify_the_fructose_metabolite_that_reciprocally_regulates_the_glycolytic_and_gluconeogenic_pathways_within_the_liver_and_the_enzyme_that_catalyzes_its_formation" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the vitamins, both by number and by name, and indicate, in broad strokes, why they are important to the metabolic processes and/or pathways specified above]({{< relref "list_the_vitamins_both_by_number_and_by_name_and_indicate_in_broad_strokes_why_they_are_important_to_the_metabolic_processes_and_or_pathways_specified_above" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}})

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/9]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss how and why the energy charge and redox potential (reducing power) of the cell are the major regulators of all cellular metabolic processes

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the pathways/reactions that define the metabolic branchpoints of two key intermediary metabolites, glucose-6-phosphate & pyruvate, and under what metabolic circumstances they would be used.

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the five common mechanisms regulating intermediary metabolism and give examples of their use in glucose catabolism.]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how glucagon and insulin antagonize their respective metabolic effects, and the enzymes critical to those processes.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the major effects of insulin and glucagon on glucose catabolism and glucose synthesis (gluconeogenesis), as well as glycogen, protein and lipid metabolism, taking into account tissue specificity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the regulatory mechanisms used by hepatocytes to ensure that gluconeogenesis, rather than glycolysis will occur in the fasting state.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the unique allosteric regulation of glycolysis in hepatocytes and its importance to both liver and whole body metabolism.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why the differential, allosteric regulation of pyruvate carboxylase and the pyruvate dehydrogenase complex are essential to whole body metabolism.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss how and why certain vitamin deficiencies will disrupt glucose metabolism.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Cell Signalling II

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Calendar pre-work

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/17]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe and give an example of receptors as ion channels.]({{< relref "describe_and_give_an_example_of_receptors_as_ion_channels" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Define the enzymes involved in synthesis and degradation of cyclic-AMP.]({{< relref "define_the_enzymes_involved_in_synthesis_and_degradation_of_cyclic_amp" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the cyclic-AMP (cAMP) second messenger signal pathway from hormone binding to activation of protein kinase-A.]({{< relref "describe_the_cyclic_amp_camp_second_messenger_signal_pathway_from_hormone_binding_to_activation_of_protein_kinase_a" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the process of G-protein activation and inactivation.]({{< relref "describe_the_process_of_g_protein_activation_and_inactivation" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the Yin-Yang regulation of adenylate cyclase.]({{< relref "describe_the_yin_yang_regulation_of_adenylate_cyclase" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important.]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Discuss the mechanisms by which phosphorylation is involved in turning off the cAMP signal pathway.]({{< relref "discuss_the_mechanisms_by_which_phosphorylation_is_involved_in_turning_off_the_camp_signal_pathway" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the basic mechanisms that cause the symptoms of cholera and whooping cough.]({{< relref "describe_the_basic_mechanisms_that_cause_the_symptoms_of_cholera_and_whooping_cough" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Discuss the monomeric GTPases and how they are regulated, with specific reference to Ras.]({{< relref "discuss_the_monomeric_gtpases_and_how_they_are_regulated_with_specific_reference_to_ras" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}), including the role of:

                <!--list-separator-->

                -  Membrane phosphatidylinositol

                <!--list-separator-->

                -  G-proteins

                <!--list-separator-->

                -  Phospholipase

                <!--list-separator-->

                -  Inositol-tris-phosphate

                <!--list-separator-->

                -  Diacylglycerol

                <!--list-separator-->

                -  Protein kinase C

                <!--list-separator-->

                -  Calcium

                <!--list-separator-->

                -  Calmodulin

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Compare the cAMP and Inositol Phosphatide signal pathways.]({{< relref "compare_the_camp_and_inositol_phosphatide_signal_pathways" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Discuss receptor tyrosine kinases and tyrosine-associated receptor signal pathways]({{< relref "discuss_receptor_tyrosine_kinases_and_tyrosine_associated_receptor_signal_pathways" >}}).

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the roles of tyrosine vs serine / threonine phosphorylation in the regulation of receptor function]({{< relref "describe_the_roles_of_tyrosine_vs_serine_threonine_phosphorylation_in_the_regulation_of_receptor_function" >}}).

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the role of gases in cell signaling with special emphasis on nitric oxide and its basic mechanism of action.]({{< relref "describe_the_role_of_gases_in_cell_signaling_with_special_emphasis_on_nitric_oxide_and_its_basic_mechanism_of_action" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the basic mechanism of action of steroid hormones both in terms of intracellular and surface receptors.]({{< relref "describe_the_basic_mechanism_of_action_of_steroid_hormones_both_in_terms_of_intracellular_and_surface_receptors" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Discuss the concept of mechanochemical signal transduction in biology.]({{< relref "discuss_the_concept_of_mechanochemical_signal_transduction_in_biology" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Discuss the role of titin as a component of mechano-chemical signal transduction in skeletal muscle.]({{< relref "discuss_the_role_of_titin_as_a_component_of_mechano_chemical_signal_transduction_in_skeletal_muscle" >}})

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-08-31 Tue]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Ethics - Justice: Solidarity with Vulnerable Populations

        <!--list-separator-->

        - <span class="org-todo done DONE">DONE</span>  Pre-work <code>[2/2]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  Recognize why purely biological models cannot fully characterize or improve human health

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  Prepare for an in-class case discussion

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze social and behavioral determinants of health in dividual patients living in diverse communities

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Propose examples of community health interventions that can improve human health via awareness of and interventions regarding the social and behavioral determinants of health.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Critique simplified understandings of free will and external determinants of health.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Cell Signaling III

        <!--list-separator-->

        - <span class="org-todo done DONE">DONE</span>  Calendar pre-work

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/11]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Discuss the concepts that individual surface-acting hormones can act on multiple receptors; and can act through multiple signal pathways.]({{< relref "discuss_the_concepts_that_individual_surface_acting_hormones_can_act_on_multiple_receptors_and_can_act_through_multiple_signal_pathways" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Explain the concept of biased ligands.]({{< relref "explain_the_concept_of_biased_ligands" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Discuss the concept of FM (Frequency Modulated) hormone signaling and give an example.]({{< relref "discuss_the_concept_of_fm_frequency_modulated_hormone_signaling_and_give_an_example" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the role of macromolecular complexes in cell signaling with examples.]({{< relref "describe_the_role_of_macromolecular_complexes_in_cell_signaling_with_examples" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the importance of location in cell signaling and mechanisms involved in creating signaling domains / “signalosomes”.]({{< relref "describe_the_importance_of_location_in_cell_signaling_and_mechanisms_involved_in_creating_signaling_domains_signalosomes" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Give examples of the importance of membrane fluidity in cell signaling.

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the toolbox of mechanisms proposed to contribute to hormone / signal molecule specificity.]({{< relref "describe_the_toolbox_of_mechanisms_proposed_to_contribute_to_hormone_signal_molecule_specificity" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Define the role of hormone surface receptor regulation in determining hormone sensitivity and responsiveness.]({{< relref "define_the_role_of_hormone_surface_receptor_regulation_in_determining_hormone_sensitivity_and_responsiveness" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Define homologous receptor regulation, its physiologic significance and the mechanisms proposed to explain the process.]({{< relref "define_homologous_receptor_regulation_its_physiologic_significance_and_the_mechanisms_proposed_to_explain_the_process" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Define heterologous receptor regulation.]({{< relref "define_heterologous_receptor_regulation" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the pathophysiology behind the cases presented at the beginning of the lecture series.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-01 Wed]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Organelles & Trafficking I (+ II)

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/25]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Differentiate between rough and smooth endoplasmic reticulum (ER) both in structure and function.]({{< relref "differentiate_between_rough_and_smooth_endoplasmic_reticulum_er_both_in_structure_and_function" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare the general mechanisms that allow some newly synthesized proteins are localized in the cytoplasm, whereas others are directed into other cellular compartments.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the mechanism by which soluble proteins are imported into the lumen of the ER and compare this to the mechanism by which transmembrane (TM) proteins are inserted into the ER membrane.

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe protein folding, identifying roles for chaperones, BiP, peptidyl prolyl isomerases and protein disulfide isomerases.]({{< relref "describe_protein_folding_identifying_roles_for_chaperones_bip_peptidyl_prolyl_isomerases_and_protein_disulfide_isomerases" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the roles in protein quality control of the endoplasmic reticulum-associated degradation (ERAD) and unfolded protein response (UPR) pathways, and the value of these pathways to the cell.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe relationship between Golgi structure and function.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare the general features of N- and O-glycoprotein processing.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the destinations of proteins leaving the trans-Golgi network.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare constitutive vs regulated exocytosis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Describe the general mechanisms of packaging cargo into vesicles.]({{< relref "describe_the_general_mechanisms_of_packaging_cargo_into_vesicles" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the lysosomal targeting paradigm.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Describe how vesicles move, recognize, and fuse with the target compartment.]({{< relref "describe_how_vesicles_move_recognize_and_fuse_with_the_target_compartment" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the importance of retrieval pathways to intracellular trafficking.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare pinocytosis, phagocytosis, and receptor-mediated endocytosis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe autophagolysosome formation & maturation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the structure of the nucleus and how macromolecules are imported.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the function and transport of peroxisomal proteins into the peroxisome.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the relationship between mitochondrial structure and function.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Rationalize the existence of the mitochondrial genome with its evolutionary origin.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how the human mitochondrion can contain >1000 proteins even though its genome only encodes 13 proteins.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the cell biological defects underlying:

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  cystic fibrosis caused by the CFTR delta-F508 mutation;

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  alpha-1-antitrypsin deficiency/hereditary emphysema;

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  [Chylomicron retention disease / Anderson disease]({{< relref "chylomicron_retention_disease" >}});

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  familial hypercholesterolemia.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the biochemical nature of and relationship between the A, B and O blood group antigens.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Rationalize the increased severity of I-cell disease compared to other lysosomal storage diseases.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the mechanism of action of botulinum toxins at the neuromuscular synapse.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Rationalize the use of PCSK9 inhibitors to control serum cholesterol levels.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Pharmacodynamics I

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define and describe the terms ligand, receptor and effector in the context of pharmacology, and explain the concept of how the dose-effect curve graphically defines EC50 and Emax.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the difference in effect between an agonist, antagonist, partial agonist and inverse agonist.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how a competitive antagonist affects receptor activity, and discuss how a non-competitive antagonist (in the absence of spare receptors) reduces Emax but does not affect EC50.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Use quantal dose-response curve data (ED50 and TD50) to define therapeutic index as a measure of drug safety.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Differentiate a receptor antagonist from a physiologic antagonist and describe an example of how they are used therapeutically.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Use specific examples of drug effects to discuss why noncovalent bonds are so important for both ligand-receptor interactions and for receptor conformational changes.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare the potency and efficacy of 2 drugs on the basis of their dose-effect curves; analyze graphically how they relate to EC50 and E<sub>max</sub>.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the effects of a non-competitive antagonist on the dose-effect curve in the context of spare receptors.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Predict the effect of a partial agonist in a patient in the presence and in the absence of a full agonist.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Draw curves that represent ED50 and TD50 in a population and discuss how these values can predict the safety of a drug.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-02 Thu]</span></span>

    <!--list-separator-->

    - <span class="org-todo done DONE">DONE</span>  Doctoring Skills

        <!--list-separator-->

        - <span class="org-todo done DONE">DONE</span>  Calendar pre-work

        <!--list-separator-->

        - <span class="org-todo done DONE">DONE</span>  Session <code>[2/2]</code>

            <!--list-separator-->

            - <span class="org-todo done DONE">DONE</span>  Interview SPs using patient-centered and clinician-centered skills practiced in previous weeks

            <!--list-separator-->

            - <span class="org-todo done DONE">DONE</span>  Use a Review of Systems (ROS) check sheet and incorporate it into the patient interview opening and interviewing using open-dended questions

            <!--list-separator-->

            -  Offer feedback to group-mates

    <!--list-separator-->

    -  Organelles and Trafficking II

        -   See Organelles and Trafficking I

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  The Practice of Evidence-Based Medicine: Asking the Question

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Review the evidence based medicine process

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explore the rationale for framing a question correctly.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Review background and foreground type questions.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define PICO.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the importance of refining the elements of PICO.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the types of patient care issues addressed by PICO questions and give examples of each.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Develop an approach to writing PICO questions.

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Practice writing and answering patient related clinical questions.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare “and” and “or” functions while searching.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how to use filters in PubMed.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Review different resources available at Dana Library

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-03 Fri]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Cell Junctions & Histology of Epithelial Tissue

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[1/7]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Name and describe five basic epithelial types and relationship to the basement membrane.]({{< relref "name_and_describe_five_basic_epithelial_types_and_relationship_to_the_basement_membrane" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the basic functions of the epithelial types.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name and describe the function of the three cell junctions.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name and describe the types, structures, and combinations of exocrine glands.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List and describe major properties the loose CT between epithelium and capillaries.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Give the approximate size of an epithelial cell nucleus.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Briefly explain the life cycle of an epithelial cell.

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Solidify objectives from the reading.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the five basic epithelial types in histologic sections.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the types, structures, and combinations of exocrine glands.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Use internal cell sizes to estimate size and distance in histologic sections.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define and recognize three variations of the basic epithelial type.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Practice relating 2-D images to 3-D structures.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Develop good habits when looking at histology slides.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Pharmacodynamics II

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the role of the FDA, and outline the process of drug development including the goal of each Clinical Trial Phase.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name 5 transmembrane signaling methods by which drug-receptor interactions exert their effects and give an example of each.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the intracellular mechanisms that mediate receptor desensitization and down-regulation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the role of receptors in generating second messenger signals through cAMP and phosphoinositides.

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the benefits and problems created by the FDA regulation of drug development; review its historical context and examine its impact on pharmaceutical safety and expense.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the receptor signaling mechanisms through their speed of action, amplification of signal, and feedback regulation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Provide broad and specific examples of how receptor subtype specificity is critical for drugs to avoid off-target effects.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Integrate the dose-effect relationship of a drug with its potency, specificity, efficacy and therapeutic index to evaluate its usefulness as a therapeutic.


#### Week 2 {#week-2}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-07 Tue]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Histology of Extracellular Matrix and Connective Tissue/Cartilage

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[2/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the functions of the connective tissue types

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Name the types of cellular and acellular components of connective tissue]({{< relref "name_the_types_of_cellular_and_acellular_components_of_connective_tissue" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Name and describe the basic types of connective tissues]({{< relref "name_and_describe_the_basic_types_of_connective_tissues" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the characteristics of the connective tissue (CT) components that facilitate their functions.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize morphologic appearance of CT.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize artifacts that form white spaces

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/5]</code>

            <!--list-separator-->

            -  Solidify objectives from the reading (especially function and composition).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize low and high power histologic feature of connective tissue.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize and discuss some of the artifacts that plague histology.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Be aware of the relative sizes of the image components that you view in histology.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Practice relating 2-D images to 3-D structures.

            <!--list-separator-->

            -  Refine your good habits when looking at histology slides!

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-08 Wed]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Gross Anatomy: Dissect Axilla and Anterior Arm

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/8]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the attachments, action(s) and innervation of these muscles.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the vessels which make an anastomosis around the surgical neck of the humerus.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the nerves which arise from the various cords of the brachial plexus.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the source or origin of all arteries.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the tributaries of veins.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the clinical consequences of injuries to nerves (cords and terminal branches).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify relevant bony landmarks and and list the structures which attach to or traverse these.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the joints labeled in blue.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Ethics - Respect for Autonomy: Medical Decision Making

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the required components of informed consent.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Review the central features of decision-making capacity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Appreciate how decision-making capacity is decision-specific.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Distinguish competence from capacity.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Evaluate the decision-making capacity of a hypothetical patient.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Make a treatment plan that acknowledges impaired patient decision making capacity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss limits of respect for patient autonomy when a patient makes an unusual therapeutic request.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Ion Channels & Excitable Membranes

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the general structure of the cell membrane and ion channels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the molecular components that determine membrane potential.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define equilibrium potential and how it relates to membrane potential.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the difference between “membrane potential” and “resting potential”.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  In general terms be able to describe how membrane potential comes about; compare and contrast the roles of active ion transport via the Na/K ATPase vs. ion movement through ion channels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  In general terms, be able to describe what the Nernst equation is used for in the context of understanding membrane potential.

                -   No math, just concepts

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the key concept that opening an ion channel brings the membrane potential to the ion’s Nernst potential (equilibrium potential) regardless of what that equilibrium potential happens to be.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Be able to describe how the Nernst and Goldman equations are similar and how they differ.

                -   Focus on concepts

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  What is Ohm’s law? What does it tell you about the number of ion channels that need to be present (and functional) to affect membrane potential? Why is this concept important for understanding how living cells generate changes in membrane potential?

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Cytoskeleton

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[1/10]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the three primary components of the cell’s cytoskeleton and the subunits from which they are assembled.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare the characteristics and functions of microfilaments, microtubules, and intermediate filaments.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Provide examples of how actin-binding proteins regulate actin filament function.]({{< relref "provide_examples_of_how_actin_binding_proteins_regulate_actin_filament_function" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the role of GTP in microtubule assembly and disassembly.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain dynamic instability of microtubules.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how motor proteins harness energy to move along cytoskeletal tracks.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Rationalize the relevance of filament polarity to motor function.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe how keratins can be used diagnostically as tumor markers.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the cell biological defects underlying:

                <!--list-separator-->

                -  Epidermolysis bullosa symplex

                <!--list-separator-->

                -  [Kartagener syndrome]({{< relref "kartagener_syndrome" >}})

                <!--list-separator-->

                -  Polycystic kidney disease

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}})

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-09 Thu]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Cells of the Nervous System

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[5/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the derivation of the CNS and PNS.

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the general morphology of neurons and levels of organization of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Define the ways in which neurons are categorized into groups.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Identify specific classes of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the cellular and ultrastructural morphology of neurons and know the function of neuronal organelles and membrane components.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the morphology and function of glial cells of the PNS and CNS.]({{< relref "describe_the_morphology_and_function_of_glial_cells_of_the_pns_and_cns" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Know the major functions of astrocytes.

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast these components of neuronal connections: divergence, convergence, serial, hierarchical and parallel.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the process and function of axonal (and dendritic) transport.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast classes of ion channels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the location and function of membrane ion channels that subserve neuronal excitability.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast myelination in the PNS and CNS.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the major functions of astrocytes

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why primary tumors of the adult brain are derived from glia, not neurons.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Introduction to Public Health

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define public health

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify determinants of health

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe commonly-used data indicators to determine the health status of a population

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply health status information to determine the health status of a population, priorities, and make recommendations

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify and prioritize current public health issues

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discover how physicians can impact determinants of health to improve individual and public health

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-10 Fri]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  The U.S. Health Care "System"

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe health insurance coverage in the U.S.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify unique coverage aspects of Medicare and Medicaid.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss characteristics of the uninsured population in the U.S.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare the US system to other comparable countries.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply health care information to determine how individuals access health insurance.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify individual and system-level barriers to access to health care.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discover how physicians can advocate for individual patients and entire populations to improve access to healthcare in the U.S.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Upper Limb Functional Anatomy Integrative Review

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply concepts of muscle innervation and nerve injury to solving clinically related questions or scenarios.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define myotome.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the clinical presentation following nerve injury in the upper limb.

                -   Here, nerve injury refers to injury at the root level (e.g. C5) or injury a to terminal branch of the brachial plexus (e.g. axillary nerve).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the nerve roots which contribute to the actions taking place at the joints of the upper limb.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Passive Membrane Properties, the Action Potential, and Electrical Signaling by Neurons

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[2/11]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Review the location and function of the common types of ion channels associated with neurons.]({{< relref "review_the_location_and_function_of_the_common_types_of_ion_channels_associated_with_neurons" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Review the mechanisms that set up the resting membrane potential.]({{< relref "review_the_mechanisms_that_set_up_the_resting_membrane_potential" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the electrical properties of neuronal membrane that affect ion flow.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast ohmic and capacitative currents.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why passive changes in membrane potential decay with distance from the point of stimulation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the action potential and describe the ionic flow related to each of the phases of membrane potential change in a generic action potential.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the relationship between passive potentials and the generation and propagation of action potentials.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the mechanism of propagation of an action potential down a neuronal membrane and the mechanisms that regulate speed of propagation in an unmyelinated axon.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the mechanisms by which myelin increases action potential conduction velocity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the mechanisms giving rise to relative and absolute refractory periods when multiple stimuli are given to an axon.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the process that results in neurotransmitter release when an action potential invades a nerve terminal.


#### Week 3 {#week-3}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-13 Mon]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Gross Anatomy: Posterior Forearm and Dorsum of the Hand

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify listed muscles, nerves and vessels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the attachments action(s) and innervation of muscles.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the source or origin of all arteries.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the clinical consequences of injuries to nerves.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the boundaries of the anatomical snuffbox and the contents found here.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Neuromuscular Junction, Motor Units and Recruitment

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[1/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the cellular, ultrastructural and functional components of the neuromuscular junction (NMJ) on striated muscle.

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the process of neuromuscular transmission.]({{< relref "describe_the_process_of_neuromuscular_transmission" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the gross and histologic organization of striated muscle and the ultrastructural and molecular components of striated muscle that mediate excitation contraction coupling.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the histology, contractile properties and metabolism of slow and fast twitch muscle fibers.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define motor unit and describe types of motor units.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the development of striated muscle.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe striated, cardiac and smooth muscle histology and motor innervation.

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast generation of and functional relationships between nerve action potential, end plate potential and muscle action potential.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the process of excitation contraction coupling in skeletal muscle beginning with invasion of the action potential into the presynaptic terminal and ending with muscle contraction.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the pathology of myasthenia gravis and Lambert-Eaton myasthenic syndrome.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the way in which the organization and physiology of motor units allows the recruitment of motor units to develop increasing force of muscle contraction in a smoothly graded manner.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Muscle Structure/Crossbridge Cycle

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the sliding filament theory of muscle contraction.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe changes in the appearance of the different bands within a sarcomere during contraction.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the sequence of events in a single crossbridge cycle.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Population Genetics

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/2]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define Hardy-Weinberg equilibrium including its assumptions and equation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the effects of genetically non-random mating, founder effect, genetic drift, gene flow, new mutations and selection.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Pre-work <code>[0/2]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Calculate allele and carrier frequencies given autosomal recessive disease prevalence.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Estimate offspring autosomal recessive disease risk based on parents' family history and applicable population disease prevalence.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-14 Tue]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Electron Transport Disorders

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/10]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List, in order, the required components of the electron transport system/chain (ETS/ETC) and explain the functions they perform.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Outline the ETC to include:

                <!--list-separator-->

                -  The order of the complexes;

                <!--list-separator-->

                -  How they are "fixed" in the inner mitochondrial membrane;

                <!--list-separator-->

                -  The location of the mobile carriers;

                <!--list-separator-->

                -  How the electrons flow through the complexes

                    -   Major oxidation/reduction reactions required

                <!--list-separator-->

                -  Where and how many protons are pumped and in what direction;

                <!--list-separator-->

                -  Where:

                    1.  The electrons generated by the two different cytoplasmic shuttles enter the ETC
                    2.  The electrons generated by fatty acid oxidation enter the ETC

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why defects in any of these ocmponents can cause mitochondrial disease

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the importance of using both NAD+- and FAD-linked substrates to identify where the ETC defect most likely occurs.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the potential mitochondrial NAD+- and FAD-linked substrates that can be used to assess ETC defects.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe common clinical and morphological features of mitochondrial disorders of the ETS.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss how you would use clinical assays of enzyme complex function to diagnose a disorder of the ETS.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how mitochondrial genetic diseases are inherited and why their expression can vary widely between siblings.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why individuals with ETC disorders develop acidosis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply your understanding of pyruvate metabolism and ETC complex function to explain the treatment plan required before the metabolic defect is unequivocally identified, as well as the metabolic aberrations identified in this patient.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Ethics - Respect for Autonomy: Surrogate Decision Making

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Review the “hierarchy of ethical decision-making".

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize which surrogate decision-makers take precedence.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Read and discuss a Vermont advance directive form.

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply the “hierarchy of ethical decision-making” to clinical cases.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss how to identify and collaborate with surrogate decision-makers.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Debate which surrogate decision-makers take precedence.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Mechanical Properties

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the force-length relationship in skeletal muscle isometric contractions.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the origin of the passive, active, and total forces generated in this muscle contraction.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare isometric with isotonic muscle contraction.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the force-velocity relationship in skeletal muscle contraction and its underlying mechanism at the molecular level.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-15 Wed]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Contractile Regulation

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Explain the role of calcium in striated muscle contraction and describe its regulation.]({{< relref "explain_the_role_of_calcium_in_striated_muscle_contraction_and_describe_its_regulation" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the term "excitation-contraction coupling".

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare a muscle twitch with muscle tetanus.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Classic Modes of Inheritance and Pedigrees

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand how to read and interpret a pedigree.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe classic modes of inheritance and identify the most likely mode of inheritance from a given pedigree.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Estimate patient risk based on condition's mode of inheritance.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Demonstrate practical understanding of genetic expression phenomena including:

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  Penetrance

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  Expressivity

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  Pleiotropy

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  Locus heterogenity

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  Allelic heterogeneity

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/2]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Create accurate pedigrees using standard symbols based on family history information provided in a mock patient interview.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Generate questions to help gather important information on family history.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-16 Thu]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Histology of Nerve and Muscle

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Muscle Disease/Smooth Muscle

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Sex-linked and Non-traditional Inheritance

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-17 Fri]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Anatomy Clinical Correlation: Upper Extremity Integrative Review

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Muscular Dystrophy


### Block 3 {#block-3}


#### Week 1 {#week-1}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-21 Tue]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Histology: Skin Workshop

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[3/6]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Understand how the epidermis grows/regenerates.]({{< relref "understand_how_the_epidermis_grows_regenerates" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the skin appendages: hair follicles, sebaceous glands, apocrine sweat glands, and eccrine sweat glands. Know their function and location on different parts of the body.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the function of the specialized cells of the epithelium: melanocytes, Langerhans cells, and Merkel cells.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the function and be able to locate the sensory structures in the skin: Merkel cells, Meissner corpuscle, and Pacinian corpuscles.

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the skin appendages and their location: hair follicles, erector pili muscle, sebaceous glands, apocrine sweat glands, and eccrine sweat glands.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the sensory cells of the skin: Merkel cells, Meissner corpuscle and Pacinian corpuscle.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize (when able) the different cells comprising the epidermis and list their function: keratinocytes, Merkel cells, melanocytes, Langerhans cells.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-22 Wed]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Ethics - Respect for Autonomy: Shared Decision Making

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Review the ethical justifications and ideal contexts for shared decision-making.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Consider obstacles to shared decision-making in the clinical setting.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Connect shared decision-making to outcomes like patient satisfaction and health care expenditures.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Practice shared decision-making including the non-judgmental elicitation of patient values and preferences and use of negotiation and problem-solving skills.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Critique limits of shared decision-making, such as in the case of in appropriate patients requests.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss challenges to shared decision-making, including perverse health care financing.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-23 Thu]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Gross Anatomy: Dissect Gluteal Region and Posterior Thigh

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify listed muscles, ligaments, nerves and vessels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the attachments, action(s) and innervation of identified muscles.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the root levels of nerves.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the origins of vessels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the tributaries of veins.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the clinical consequences of injuries to nerves.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Cell Cycle & Regulation I

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}})

                <!--list-separator-->

                -  Activation/ inactivation mechanisms for Cdks

                <!--list-separator-->

                -  Loading of cohesin to link replicated sister chromosomes

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the details of molecular surveillance systems called checkpoints that stop the cell cycle if damage or problems are detected

                <!--list-separator-->

                -  Basic anatomy of a checkpoint

                <!--list-separator-->

                -  Examples of sensors, transducers, and effectors

                <!--list-separator-->

                -  G1 DNA damage pathway

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Examine how cells decide whether or not to “commit” to another cell cycle

                <!--list-separator-->

                -  Steps involved in Restriction point control

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the mechanisms that facilitate accurate chromosome segregation and cellular division

                <!--list-separator-->

                -  Molecular control of the metaphase to anaphase transition

                <!--list-separator-->

                -  Major steps that occur during telophase and cytokinesis

                <!--list-separator-->

                -  Similarities and differences between mitosis and meiosis

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/9]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify tumor suppressors and proto-oncogenes involved in controlling the G1/S transition.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand how specific G1/S defects might impact tumor treatment decisions.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify genetic diseases resulting from mutations in DNA damage checkpoint genes.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the pattern of inheritance for each of these syndromes.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Become familiar with methods to detect chromosome segregation and structural errors (cytogenetics).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand mechanisms that lead to mitotic chromosome segregation errors.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the effects of anti-microtubule drugs on mitotic cells.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the underlying causes of chromosome nondisjunction in meiosis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Be aware of potential reasons for high rates of chromosome segregation errors during female meiosis I and how they may be impacted by maternal age.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  DNA Repair

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the major types of DNA damage caused by replication errors, chemical agents, ionizing radiation (IR), and reactive oxygen species (ROS).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Be able to compare and contrast the major pathways for the repair of DNA chemical/physical damage including base excision repair, nucleotide excision repair, and recombination.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the processes underlying the fidelity of DNA replication, including nucleotide discrimination by DNA polymerases, DNA polymerase proofreading activity, and DNA mismatch repair.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe details of the E. coli methyl-directed mismatch repair system, including how mismatches are recognized, and how the system distinguishes between the parental and daughter strands of DNA.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the human homologs of the E. coli MutS and MutL proteins, and their apparent roles in the human mismatch repair system.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-24 Fri]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Pharmacogenetics

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[5/6]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the general treatment objectives underlying pharmacogenomic testing]({{< relref "list_the_general_treatment_objectives_underlying_pharmacogenomic_testing" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Identify the five P450 enzyme metabolizer status phenotypes that may guide drug selection or dosing]({{< relref "identify_the_five_p450_enzyme_metabolizer_status_phenotypes_that_may_guide_drug_selection_or_dosing" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Indicate whether duplication of an entire CYP gene will increase or decrease expected drug effect, when the encoded P450 enzyme metabolizes a pro-drug to the active drug.]({{< relref "indicate_whether_duplication_of_an_entire_cyp_gene_will_increase_or_decrease_expected_drug_effect_when_the_encoded_p450_enzyme_metabolizes_a_pro_drug_to_the_active_drug" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Indicate whether duplication of an entire CYP gene will increase or decrease expected drug effect, when the enzyme metabolizes the active drug to an inactive metabolite.]({{< relref "indicate_whether_duplication_of_an_entire_cyp_gene_will_increase_or_decrease_expected_drug_effect_when_the_encoded_p450_enzyme_metabolizes_a_pro_drug_to_the_active_drug" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Name one drug in which severe adverse effects may depend on the presence of a specific genetic variation]({{< relref "name_one_drug_in_which_severe_adverse_effects_may_depend_on_the_presence_of_a_specific_genetic_variation" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Relate mutations that drive certain cancers to the utility of drugs that target and inhibit specific proteins.

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Session <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Provide examples of how genetic variants of metabolizing enzymes can affect drug dosing.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Predict whether a steady state drug level will be therapeutic, sub-therapeutic, or supra-therapeutic, given a poor metabolizer phenotype result, when the standard recommended dose is given.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the most likely gene or genes in which genetic testing may help in prescribing antidepressant drugs in the Selective Serotonin Reuptake Inhibitor (SSRI) and Tricyclics drug classes, using online resources.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Interpret pharmacogenomic test results to determine the best drug to administer for a specific clinical context, given pharmacological and financial constraints.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Demonstrate finding prescribing guidance for specific drugs based on pharmacogenomic results online at FDA and CPIC websites.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Given a targeted therapy biomarker test result, determine whether the targeted therapy is worth using in the tested patient.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Cell Cycle and Regulation II

        <!--list-separator-->

        -  See [Cell Cycle & Regulation I](#cell-cycle-and-regulation-i)


#### Week 2 {#week-2}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-27 Mon]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Cell Death

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[1/5]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Define apoptosis and necrosis]({{< relref "define_apoptosis_and_necrosis" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast cell death by necrosis and apoptosis in terms of stimuli

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast cell death by necrosis and apoptosis in terms of organelles involved

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast cell death by necrosis and apoptosis by effect of the process on surrounding cells and the host

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}})

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[2/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the role of cell death in normal development and homeostasis

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the intrinsic pathway of apoptosis and the stimuli leading to this process]({{< relref "intrinsic_pathway_of_apoptosis" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the extrinsic pathways of apoptosis and the stimuli leading to this process]({{< relref "extrinsic_pathway_of_apoptosis" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify morphologic features associated with different cell death modalities and laboratory tests designed to detect them

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Relate cell death in the context of pathologic conditions (i.e. chronic disease, acute injury)

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Anti-neoplastics

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Describe the mechanism of action of the listed anti-neoplastics in the context of their specific actions and effect during cell division.]({{< relref "describe_the_mechanism_of_action_of_the_listed_anti_neoplastics_in_the_context_of_their_specific_actions_and_effect_during_cell_division" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Classify the listed anticancer drugs on the basis of their cell cycle stage specificity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare the general and specific toxicities of the listed anti-neoplastic drugs.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain preventative/treatment measures to reduce specific toxicities.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Outline the general and specific mechanisms of resistance to anti-neoplastics.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/2]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the basic kinetics of tumor cell growth & explain the concept of “total cell kill”.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the basic principles of combination chemotherapy in cancer treatment and specific examples:

                <!--list-separator-->

                -  CHOP

                <!--list-separator-->

                -  ADVB

                <!--list-separator-->

                -  BEP

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-28 Tue]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Histology: Blood and Marrow

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the major components of the peripheral blood.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name the mature cells in the peripheral blood.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the names of the precursor bone marrow cells stages and the general nuclear and cytoplasmic changes that occur with maturation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the major functions of the three main hematopoietic cell lines:

                <!--list-separator-->

                -  Platelets

                <!--list-separator-->

                -  Red cells

                <!--list-separator-->

                -  White cells

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Briefly describe the content of serum and plasma.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Briefly discuss the role (function) of the major protein components of blood:

                <!--list-separator-->

                -  Albumin

                <!--list-separator-->

                -  Immunoglobulins

                <!--list-separator-->

                -  Fibrinogen

                <!--list-separator-->

                -  Globulins (innate and immune)

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize and identify the cells of the peripheral blood.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize bone marrow precursor cells on a Wright stained smear.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize a myeloblast on Wright stained smear.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize hematopoietic bone marrow and megakaryocytes on H&E sections.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Cancer

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/9]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the process of cell transformation into cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define basic cancer terminology and list the hallmarks of cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the steps in cancer metastasis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the roles of oncogenes and tumor suppressor genes in normal cells and ways in which normal function may be disrupted in cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how certain inherited mutations lead to increased risk of cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how duplications, deletions, and translocations can contribute to cancer formation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the role of epigenetic dysregulation in cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the steps in cancer evolution and selection and explain how this can contribute to tumor heterogeneity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List approaches taken to treat cancer and what makes them effective.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/9]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List approaches taken to treat cancer and what makes them effective.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define basic cancer terminology and list the hallmarks of cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the steps in cancer metastasis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the roles of oncogenes and tumor suppressor genes in normal cells and ways in which normal function may be disrupted in cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how certain inherited mutations lead to increased risk of cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how duplications, deletions, and translocations can contribute to cancer formation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the role of epigenetic dysregulation in cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the steps in cancer evolution and selection and explain how this can contribute to tumor heterogeneity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List approaches taken to treat cancer and what makes them effective.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-29 Wed]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Histology: Bone lab

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the role of that each of the above contributes to the functions of bone.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how bone grows and remodels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how bone is involved in calcium metabolism.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the function of synovium, cartilage and Shapey’s fibers.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/1]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the components of bone morphologically

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Metabolism of Cancer

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/13]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define “metabolic reprogramming.”

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the meaning of “aerobic glycolysis” and under what circumstances it occurs.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  State Warburg’s initial hypothesis explaining why cancer cells employed “aerobic glycolysis.”

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why that hypothesis is now known to be incorrect based on the major mechanisms by which “aerobic glycolysis” supports tumor cell growth and invasion.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the two major changes in glycolytic enzymes that are required for efficient “aerobic glycolysis,” including how those changes facilitate that process.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how and why the products produced in aerobic glycolysis facilitate continuous cytosolic glucose utilization by tumor cells.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the four glycolytic intermediates used by tumor cells and the biosynthetic precursors that they produce.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how those four biosynthetic precursors are used to facilitate tumor cell growth.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how and why tumor cells channel excess cytosolic pyruvate and NADH into fatty acid and cholesterol biosynthesis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how carbons derived from both glucose and glutamine provide essential anaplerotic TCA metabolities.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Consider the reductive pathway of glutamine metabolism in cancer cells with dysfunctional mitochondria. Explain/describe:

                <!--list-separator-->

                -  The metabolic circumstances that dictate its use

                <!--list-separator-->

                -  The unique reaction pathway that is used

                <!--list-separator-->

                -  The unique enzyme and coenzyme required

                <!--list-separator-->

                -  How the product that is formed is used to support both lipid biosynthesis and replenishment of TCA cycle intermediates

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define tumor cell cannibalism and describe why it promotes tumor cell growth.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the mechanisms by which excess fructose intake in the form of high-fructose corn syrup may lead to the development of colorectal cancer and the data in support of that concept.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-09-30 Thu]</span></span>

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Gross Anatomy: Dissect the Plantar Foot

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify listed muscles, ligaments, nerves and vessels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the action(s) and innervation of muscles.

                List the origins of vessels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the clinical consequences of injuries to nerves.

    <!--list-separator-->

    - <span class="org-todo todo TODO">TODO</span>  Ethics - Wise Stewardship Meets Respect for Autonomy: Decision Support Systems

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Consider ways that shared decision making leads to wiser resource allocation in our health care system.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define micro-ethics and how it relates to the role of shared decision-making in respect for patient autonomy

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Consider more in-depth patient- and provider-related obstacles to shared decision-making in clinical practice.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List examples of decision aids in use in clinical practice today

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Consider how wise allocation of the resource of time is relevant to shared decision-making, all of clinical practice, and just maybe your life.

        <!--list-separator-->

        - <span class="org-todo todo WAIT">WAIT</span>  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Consider obstacles to shared decision-making in the clinical setting.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Connect shared decision-making to outcomes like patient satisfaction and health care expenditures

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Critique ways the current US approach to health care delivery could obstruct success of such measures.


#### Week 3 {#week-3}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-04 Mon]</span></span>

    <!--list-separator-->

    -  Inherited Cancer

        <!--list-separator-->

        -  Session <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize general features of hereditary colorectal cancers, specifically Lynch syndrome (HNPCC) and Familial Adenomatous Polyposis (FAP).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast somatic and germline mutations and testing in cancer.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the genetic basis of hereditary colorectal cancer, specifically identifying the normal molecular processes that are interrupted.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss general population and at-risk population for colorectal cancer screening guidelines.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the psychosocial factors that may affect patients considering genetic testing for familial cancer syndromes.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Practice creating a pedigree from relevant family history information and using online genetic and genomic resources.

    <!--list-separator-->

    -  Genomics in the Population

        <!--list-separator-->

        -  Pre-work <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the types of disorders that are part of the newborn screening panel.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the difference between screening and diagnostic testing.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Delineate the features of multifactorial inherited traits.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how models of multifactorial traits are used in complex genetic diseases.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how common variants contributing small susceptibility risk for complex disease are identified.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the benefits and limitations of genome-wide association studies (GWAS).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the role of massively parallel signature sequencing in future disease research.

        <!--list-separator-->

        -  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the ethical, practical, and population considerations in determining the disorders included in newborn screening.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the types of disorders that are part of the newborn screening panel.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply concepts of multifactorial traits to GWAS and massively parallel signature sequencing projects and the role of common variants.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-05 Tue]</span></span>

    <!--list-separator-->

    -  Gout

        <!--list-separator-->

        -  Session <code>[1/8]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP.]({{< relref "outline_the_general_features_of_the_pathways_of_de_novo_synthesis_of_the_two_purine_and_three_pyrimidine_ribonucleotides_noting_the_chemical_changes_required_for_conversion_of_utp_to_ctp_and_imp_to_atp_gtp" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Indicate the roles of PRPP in purine and pyrimidine biosynthesis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how ribonucleotides are reduced to deoxyribonucleotides.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recall the mechanisms of methotrexate and FdUMP as anti-cancer agents.

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the key genetic and environmental factors that can affect the onset and presentation of gout, and explain their mechanisms.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the basis and rationale of treatments to improve symptoms of gout.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the causes and effects of gout and Lesch-Nyhan Syndrome.


### Block 4 {#block-4}


#### Week 1 {#week-1}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon]</span></span>

    <!--list-separator-->

    -  Blood Lecture

        <!--list-separator-->

        -  Session <code>[0/12]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare blood with other organs.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the major functions of blood.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the how hematopoietic cells are produced.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define hematopoiesis, erythropoiesis, granulopoiesis, and thrombopoiesis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the sites of hematopoiesis in humans at different ages.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast pluripotent hematopoietic stem cells and committed myeloid cells.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the sequence of events that occurs as a hematopoietic pluripotent stem cell develops into a fully differentiated cell in the blood.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the system of negative and positive feedback loops and hematopoietic growth factors that influence proliferation, differentiation, and survival of immature progenitor cells.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the site of production, stimulation, mechanism of action and effects of hematopoietic cytokines and therapeutic uses of hematopoietic cytokine analogues.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast early mature hematopoietic cells.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the life cycle of red blood cells, platelets, and neutrophils including life span.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe how hematopoiesis can be assessed.

    <!--list-separator-->

    -  Homeostasis

        <!--list-separator-->

        -  Session <code>[4/10]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the history and meaning of the term Homeostasis.]({{< relref "describe_the_history_and_meaning_of_the_term_homeostasis" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Discuss the concept of steady state and the balancing of input and outputs that determines the level of some substance such as blood sugar.]({{< relref "discuss_the_concept_of_steady_state_and_the_balancing_of_input_and_outputs_that_determines_the_level_of_some_substance_such_as_blood_sugar" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Discuss what happens to balance when input ALONE is increased or decreased and provide an example.]({{< relref "discuss_what_happens_to_balance_when_input_alone_is_increased_or_decreased_and_provide_an_example" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  Discuss what happens to balance when output ALONE is increased or decreased and provide an example.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define and compare positive and negative feedback systems

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the major elements of a negative feedback control system.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Outline the essential elements of feedback loops controlling body temperature, blood osmolarity, respiration and blood pressure.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Provide examples of systemically controlled negative feedback loops

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Outline the essential elements of the negative feedback loops controlling body temperature, blood osmolarity, respiration and blood pressure.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Provide examples of positive feedback loops.

    <!--list-separator-->

    -  Lymphatic and Immune Systems

        <!--list-separator-->

        -  Pre-work <code>[6/9]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Name and describe the pathways of lymphatic drainage (in a lymph node, in the body).]({{< relref "name_and_describe_the_pathways_of_lymphatic_drainage_in_a_lymph_node_in_the_body" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the functions of lymphatic drainage.]({{< relref "list_the_functions_of_lymphatic_drainage" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the cells that participate in immune responses.

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the processes of lymphoid maturation that occur in the thymus.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}})

        <!--list-separator-->

        -  Session <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the above lymphatic tissues and their parts on histologic images and describe the events occurring in the tissues.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how the arrangement of lymphatic tissues facilitates the activation of T and B lymphocytes.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the immune cells and describe the interactions occurring between them in the cortex and paracortex of the lymph node.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe and recognize the structures of Waldeyer’s Ring (tonsils and adenoids) and other MALT tissue.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize lymphoid tissue when it is present in other tissues.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe some physical exam evidence of the presence of lymphoid tissue.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-12 Tue]</span></span>

    <!--list-separator-->

    -  Histology: Lymphatic and Immune Systems

        <!--list-separator-->

        -  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Find and identify the components of lymphoid tissue in whole slide images

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Find and identify key immune cells in images of lymphoid tissue.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Solidify understanding of the interactions and functions of immune cells in lymphoid tissue.

    <!--list-separator-->

    -  Ethics - Beneficence Meets Non-maleficence: Non-beneficial Treatments

        <!--list-separator-->

        -  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define terms that highlight quality of life.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name the three possible definitions of “futility”.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify reasons for the “rise and fall of the futility movement” in the 1990s.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the difference between rationing, bedside rationing, wise resource allocation and non-beneficial care.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Distinguish between rationing and futility.

        <!--list-separator-->

        -  Session <code>[0/2]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Critique a clinician’s assertion that a given kind of care is non-beneficial.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Create questions designed to determine if a patient or surrogate is requesting non-beneficial care.

    <!--list-separator-->

    -  EBM: Study Design

        <!--list-separator-->

        -  Pre-work <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain key components of analytic studies

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the key elements, requirements, strengths, and weaknesses of cohort, case-control, and randomized trial designs to include cross over designs

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain typical outcome measures of association used by each study design

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Calculate relative risk or an odds ratio from appropriate data

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain confounding, selection bias, recall bias, external validity, internal validity, blinding, and double blinding, carry over, absolute risk reduction, and number needed to treat

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare intention to treat analysis and completer analysis

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Interpret the results of cohort, case control, and randomized trials

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze measures of association

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze study designs for weaknesses and strengths

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Select appropriate studies to answer research questions

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-13 Wed]</span></span>

    <!--list-separator-->

    -  A Review of Organ-specific Carbohydrate and Fat Metabolism

        <!--list-separator-->

        -  Session <code>[0/8]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the pathways/reactions that define the metabolic branchpoints of two key intermediary metabolites, glucose-6-phosphate & pyruvate and the final product(s) produced.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List which of the aforementioned pathways are used in the

                <!--list-separator-->

                -  Brain

                <!--list-separator-->

                -  Striated muscle

                <!--list-separator-->

                -  Adipose tissue

                <!--list-separator-->

                -  Liver

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  Indicate under what metabolic circumstances the aforementioned pathways would be used (fed/fasting) and explain why

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List those tissues that respond to insulin vs. glucagon and epinephrine.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how glucagon and insulin antagonize their respective metabolic effects.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the major effects (i.e., increase or decrease) of insulin and glucagon on glucose catabolism and glucose synthesis (gluconeogenesis), as well as glycogen, protein and lipid metabolism taking into account tissue specificity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the regulatory mechanisms used by hepatocytes to ensure that gluconeogenesis, rather than glycolysis will occur in the fasting state.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how the liver and adipose tissue collaborate to maintain appropriate blood glucose levels.

            <!--list-separator-->

            -  _Consider the following pathways_:

                <!--list-separator-->

                -  Glycolysis

                <!--list-separator-->

                -  The formation of pyruvate's four products

                <!--list-separator-->

                -  Pyruvate DH complex

                <!--list-separator-->

                -  Fatty acid oxidation

                <!--list-separator-->

                -  TCA cycle

                <!--list-separator-->

                -  Oxidative phosphorylation

                <!--list-separator-->

                -  Gluconeogenesis

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  For each of the above pathways:

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  Identify the enzymes involved in these processes whose activity will be compromised by:

                    <!--list-separator-->

                    -  A deficiency of one or more of B1, B2, B3, B5, and B7

                    <!--list-separator-->

                    -  Explain the role of the vitamin in the enzyme's function

                <!--list-separator-->

                - <span class="org-todo todo ___">[ ]</span>  List those enzymes that are protein kinase A substrates AND indicate how PKA-catalyzed phosphorylation affects their function

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-14 Thu]</span></span>

    <!--list-separator-->

    -  Histology: Cardiovascular System

        <!--list-separator-->

        -  Pre-work + Session <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name, recognize, and describe the three layers of a vessel.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name, recognize, and describe the functions of the different types of vessels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how the composition of the vessel layers and some of their unique parts facilitate the function of the different types of vessel.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define and describe the value of collaterals and portal systems.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name, recognize, and describe the layers of the heart.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the unique challenge of providing a blood supply to the components of the cardiovascular system with high luminal pressures.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name, recognize and describe the function of the ancillary structures and fibers needed for the heart to function.

    <!--list-separator-->

    -  General features of the cardiovascular system

        <!--list-separator-->

        -  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the basic structures of the heart including:

                <!--list-separator-->

                -  Four chambers

                <!--list-separator-->

                -  Four valves

                <!--list-separator-->

                -  Great vessels

                <!--list-separator-->

                -  Coronary arteries

                <!--list-separator-->

                -  Conduction system

                <!--list-separator-->

                -  Pericardium

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the normal physiology of the heart and circulation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the layers of the heart from the endocardium to pericardium.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the properties of contractile myocytes to nodal and conducting cells in the heart.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe cardiac output, regional blood flow and venous return.

        <!--list-separator-->

        -  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch the general organization of the cardiovascular system, including:

                <!--list-separator-->

                -  Chambers of the heart

                <!--list-separator-->

                -  Valves

                <!--list-separator-->

                -  Pulmonary circulation

                <!--list-separator-->

                -  Systemic circulation

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch the conduction system including:

                <!--list-separator-->

                -  Sinoatrial node

                <!--list-separator-->

                -  Bachmann's bundle

                <!--list-separator-->

                -  Atrioventricular node

                <!--list-separator-->

                -  Bundle of His

                <!--list-separator-->

                -  Left + Right bundles

                <!--list-separator-->

                -  Location of purkinje fibers

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the structural and functional properties of the four valves within the heart.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Calculate cardiac output and determine the distribution of blood flow to the major organs of the body.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Differentiate between the fundamental properties of ventricular systole and diastole.

    <!--list-separator-->

    -  Respiration: Lung Mechanics

        <!--list-separator-->

        -  Session <code>[0/10]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the importance of the function of the respiratory system

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the changes in the anatomy of airways from nose to alveolus

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the action of surfactant

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the anatomy and function of the pleura

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the unique features of the pulmonary circulation

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the importance of lung elastic recoil

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand how the chest wall also has elastic recoil

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the actions of the respiratory muscles

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the events that lead to inflation and deflation of the lung during breathing

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe how mechanical forces determine lung volume

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-15 Fri]</span></span>

    <!--list-separator-->

    -  Anatomy: Dissect Superior Mediastinum

        <!--list-separator-->

        -  Session <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the boundaries of the superior mediastinum.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify and list the functions of the vessels, nerves and other structures in the superior mediastinum.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the course of the aorta as it leaves the left ventricle, and identify its branches in the superior mediastinum.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the drainage of venous blood from the thorax into major vessels leading toward the heart.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Follow the course of the vagus and phrenic nerves through the mediastinum, comparing their positions relative to the root of the lung.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare the course of the left and right recurrent laryngeal nerves relative to major vessels of the mediastinum.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the location of the remnant of the thymus in the adult.

    <!--list-separator-->

    -  The Cardiac Cycle and Pressure Volume Loop

        <!--list-separator-->

        -  Pre-work <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define:

                <!--list-separator-->

                -  Systole

                <!--list-separator-->

                -  Diastole

                <!--list-separator-->

                -  End-diastolic volume

                <!--list-separator-->

                -  End-systolic volume

                <!--list-separator-->

                -  Stroke volume

                <!--list-separator-->

                -  Cardiac output

                <!--list-separator-->

                -  Ejection fraction

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  On a diagram of the cardiac cycle, identify:

                <!--list-separator-->

                -  Systole

                <!--list-separator-->

                -  Diastole

                <!--list-separator-->

                -  Isovolumetric contraction

                <!--list-separator-->

                -  Ventricular ejection

                <!--list-separator-->

                -  Isovolumetric relaxation

                <!--list-separator-->

                -  Rapid ventricular filling

                <!--list-separator-->

                -  Atrial contraction

                <!--list-separator-->

                -  Cardiac chamber pressure

                <!--list-separator-->

                -  Atrial pressure

                <!--list-separator-->

                -  Heart sounds

                <!--list-separator-->

                -  Positions (open/closed) of the four valves

                <!--list-separator-->

                -  Relationship of mechanical events to the electrocradiagram (ECG)

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  On a pressure-volume loop of the left ventricle, identify:

                <!--list-separator-->

                -  Systole

                <!--list-separator-->

                -  Diastole

                <!--list-separator-->

                -  Isovolumetric contraction

                <!--list-separator-->

                -  Ventricular ejection

                <!--list-separator-->

                -  Isovolumetric relaxation

                <!--list-separator-->

                -  Ventricular filling

                <!--list-separator-->

                -  End-diastolic volume

                <!--list-separator-->

                -  End-systolic volume

                <!--list-separator-->

                -  End-systolic volume pressure point

                <!--list-separator-->

                -  Ventricular pressure

                <!--list-separator-->

                -  Arterial pressure

                <!--list-separator-->

                -  Heart sounds

                <!--list-separator-->

                -  Positions (open/closed) of the four valves

                <!--list-separator-->

                -  Stroke volume

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Describe the relationship between ventricular filling and stroke volume on a Frank-Starling curve.]({{< relref "describe_the_relationship_between_ventricular_filling_and_stroke_volume_on_a_frank_starling_curve" >}})

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch a diagram of the cardiac cycle and label the key feactures described in Preparation Objective 2 above.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch a diagram of a left ventricular pressure-volume loop and label the key features described in Preparation Objective 3 above.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch the Frank-Starling relationship.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Construct and then analyze multiple left ventricular pressure-volume loops in order to compare and contrast the effect of end-diastolic volume on stroke volume and cardiac output.

    <!--list-separator-->

    -  Respiration: Ventilation - Perfusion Matching

        <!--list-separator-->

        -  Pre-work <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the principle of partial pressures of gas.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Know the alveolar gas equation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Be familiar with the unique features of the pulmonary circulation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the difference between V/Q for a shunt vs. V/Q for a dead space.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Use the alveolar gas equation to determine the general causes of hypoxemia.

        <!--list-separator-->

        -  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply the principle of partial pressures of gas.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Illustrate the alveolar gas equation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Distinguish the unique features of the pulmonary circulation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Classify the difference between V/Q for a shunt vs. V/Q for a dead space.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply the alveolar gas equation to determine the general causes of a low arterial oxygen level.


#### Week 2 {#week-2}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-18 Mon]</span></span>

    <!--list-separator-->

    -  Embryology Review

        <!--list-separator-->

        -  See Block 1

    <!--list-separator-->

    -  Regional and Peripheral Circulation

        <!--list-separator-->

        -  Pre-work <code>[1/8]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Describe the basic structural properties of the arteries and veins in the human body including conducting arteries, resistance arteries and arterioles, capillaries, venules and veins, and explain why veins are considered capacitance vessels.]({{< relref "describe_the_basic_structural_properties_of_the_arteries_and_veins_in_the_human_body_including_conducting_arteries_resistance_arteries_and_arterioles_capillaries_venules_and_veins_and_explain_why_veins_are_considered_capacitance_vessels" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Describe the series and parallel architecture of the circulation]({{< relref "describe_the_series_and_parallel_architecture_of_the_circulation" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Explain the concepts of resistance, exchange and capacitance in the circulation.]({{< relref "explain_the_concepts_of_resistance_exchange_and_capacitance_in_the_circulation" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Describe the concept and determinants of blood pressure.]({{< relref "describe_the_concept_and_determinants_of_blood_pressure" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Explain the determinants of blood flow in the peripheral circulation (pressure difference, peripheral resistance) and explain the following term: mean arterial pressure.]({{< relref "explain_the_determinants_of_blood_flow_in_the_peripheral_circulation_pressure_difference_peripheral_resistance_and_explain_the_following_term_mean_arterial_pressure" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Identify the basic structural features of regional circulation among different organs.]({{< relref "identify_the_basic_structural_features_of_regional_circulation_among_different_organs" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Explain the basic concepts of the baroreceptors and chemoreceptors in the cardiovascular system.]({{< relref "explain_the_basic_concepts_of_the_baroreceptors_and_chemoreceptors_in_the_cardiovascular_system" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}})

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch the cardiovascular system and describe the determinants of regional blood flow during different physiologic states.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Diagram the baroreceptor system and the renin-angiotensin-aldosterone system (RAAS) and explain how they function.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze a series of Frank-Starling curves and integrate the “actions” of the baroreceptor and RAAS into this analysis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast some of the structural and functional differences among regional vascular beds (e.g. coronary, cerebral).

    <!--list-separator-->

    -  Histology: Respiratory System

        <!--list-separator-->

        -  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the components of respiratory mucosa.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Know the structure and function of the mucociliary escalator and where it is located anatomically.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the changes to the airways that occur as you move from trachea to bronchioles.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Know the cellular components of alveoli and their functions.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the layers through which gases must diffuse (blood-air barrier) in order to go from the alveolar space to hemoglobin.

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize respiratory mucosa and identify its components

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Distinguish trachea, bronchi, bronchioles, terminal bronchioles, and respiratory bronchioles.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify respiratory bronchioles, alveolar ducts, alveolar, alveoli, capillaries, type I and type II pneumocytes, and macrophages

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the 5 layers from alveolar air space to hemoglobin using an electron microscopy image.

    <!--list-separator-->

    -  Anatomy Module: Thorax (heart)

        <!--list-separator-->

        -  Session <code>[0/8]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define mediastinum and the structures found in the superior and inferior mediastina.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the layers of the pericardium and their neurovascular supply.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the chambers of the heart and describe major features and functions of these (e.g. papillary muscles).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the path of blood through the heart.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the locations and functions of the 4 heart valves.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the coronary vessels, their major branches and supply territories (be general here-RCA/LCA supply territories).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the features of the conducting system of the heart and describe the sequence of events leading to spread of electrical activity in the heart.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the steps associated with the cardiac conduction cycle.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-19 Tue]</span></span>

    <!--list-separator-->

    -  Cystic Fibrosis Small Group

        <!--list-separator-->

        -  Session <code>[0/8]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Describe the molecular defect that leads to CF.]({{< relref "describe_the_molecular_defect_that_leads_to_cf" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Explain how the defect leads to dysfunction in the lungs.]({{< relref "explain_how_the_defect_leads_to_dysfunction_in_the_lungs" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Extrapolate how the defect can lead to dysfunction in other organ systems involved in the disease.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify and describe how the gold standard test for CF works.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Rationalize potential drug action with molecular defects and genomics.]({{< relref "rationalize_potential_drug_action_with_molecular_defects_and_genomics" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Predict the consequences of the disease in the lungs.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the inheritance and incidence of CF.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Calculate risk of occurrence of a genetic condition based on carrier frequencies in a population.

    <!--list-separator-->

    -  Ethics: Medical Aid in Dying, Euthenasia, and Beyond

        <!--list-separator-->

        -  Pre-work <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize that respect for patient autonomy, while central to clinical care, has limits.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify example scenarios that test the bounds of respect for patient autonomy, including abortion, physician assisted death, euthanasia, and ethical controversial types of clinical care.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Review the history and current realities of Vermont’s Act 39 regarding physician assisted death.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Distinguish between physician assisted death versus euthanasia.

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze personal reactions to patient requests for physician-assisted death.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Critique drivers of requests for physician assisted death.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss the management of a patient who says, “Doc, I need you help me die.”

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Debate the pros and cons of physician participation in controversial practices like cosmetic surgery.

    <!--list-separator-->

    -  Embryology Lecture 1

        <!--list-separator-->

        -  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the development of the intraembryonic cavity and its subdivisions.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand which layer of origin contributes to each component.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Name the stages of lung development, their approximate times of development, and explain the major changes in each stage.]({{< relref "name_the_stages_of_lung_development_their_approximate_times_of_development_and_explain_the_major_changes_in_each_stage" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the role of various components are required for normal lung development.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain common abnormalities in body wall and lung development.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-20 Wed]</span></span>

    <!--list-separator-->

    -  Ventricular Performance I

        <!--list-separator-->

        -  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain cardiac performance parameters including preload, afterload, contractility, and heart rate.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe how preload, afterload, and contractility are altered.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the major determinants of myocardial wall stress/tension.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the major determinants of myocardial oxygen consumption (MVO2) and the relationship of wall stress/tension to MVO2.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define inotropy, chronotropy, lusitropy, and dromotropy.

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze multiple Frank-Starling curves in order to compare and contrast the effect of preload and contractility on stroke volume and cardiac output

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Construct and then analyze multiple left ventricular pressure-volume loops in order to compare and contrast the effect of preload, afterload, and contractility on stroke volume and cardiac output.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze “clinical” data to determine ventricular wall stress and myocardial oxygen demand (MVO2).

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Design strategies to reduce myocardial oxygen demand (MVO2).

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-21 Thu]</span></span>

    <!--list-separator-->

    -  Doctoring Skills: Cardiopulmonary II Exam

        <!--list-separator-->

        -  Session

            <!--list-separator-->

            -  Perform cardiovascular, pulmonary, and vital sign physical exams on an SP

            <!--list-separator-->

            -  Interview an SP

            <!--list-separator-->

            -  Review a medical record (paper copy) during SP interview

            <!--list-separator-->

            -  Use EHR communication skills

            <!--list-separator-->

            -  Present a patient

            <!--list-separator-->

            -  Offer feedback to group-mates

    <!--list-separator-->

    -  Ventricular Performance II

        <!--list-separator-->

        -  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Outline the cardiac conduction system

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the normal cardiac action potential in atrial myocytes, ventricular myocytes, nodal cells and Purkinje cells

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the concept of excitation-contraction coupling

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the basics of the surface electrocardiogram (ECG/EKG) including the p wave, QRS complex and T wave

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the property of diastolic depolarization in sinoatrial (SA) and atrioventricular (AV) nodal cells

        <!--list-separator-->

        -  Session <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch the cardiac action potential of atrial and ventricular myocytes, label the key ion movement in all phases of the action potential, and identify channels responsible for the ion movement

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Relate the action potential of atrial and ventricular myocytes to the surface ECG

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Formulate ideas as to how certain drug types will affect the action potential and surface ECG

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch the cardiac action potential of sinoatrial (SA) and atrioventricular (AV) nodal cells, label the key ion movement in all phases of the action potential, and identify channels responsible for ion movement

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the electrophysiology of contractile myocytes to nodal cells

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze selective ECGs and determine the hierarchy of diastolic depolarization

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch the key steps in excitation contraction coupling

    <!--list-separator-->

    -  Embryology Lecture 2

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the formation of the heart tube and blood vessels.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the folding and subsequent partitioning of the heart tube to form the 4-chambered heart.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Contrast the pattern of blood flow through the fetal heart with that in the newborn.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand select cardiac defects.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-22 Fri]</span></span>

    <!--list-separator-->

    -  Intermediary Metabolism in Muscle and Adipose Tissue I

        <!--list-separator-->

        -  Pre-work <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recall and detail the pathways/reactions that define the metabolic branchpoints of two key intermediary metabolites, glucose-6-phosphate & pyruvate, and under what metabolic circumstances they would be used.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recall and detail how glucagon and insulin antagonize their respective metabolic effects.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recall and list the insulin-sensitive tissues and explain the significance and consequences of that designation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the ways in which glucose is utilized in skeletal muscle and adipose tissue in the fed state.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the function and storage of glycogen.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how glycogen synthesis and breakdown are reciprocally regulated by the covalent modification of key enzymes.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the compounds required for fatty acid synthesis in adipose tissue.

        <!--list-separator-->

        -  Session <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail the ways in which insulin dictates how glucose is utilized in muscle and fat in the fed state.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the hormonal conditions and the intracellular responses induced, which regulate glycogenesis and glycogenolysis in muscle.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how glucose entry into muscle and the liver controls its own utilization.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the mechanisms required for, and by which, muscle contractions mobilize glycogen stores.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how insulin affects fatty acid synthesis by adipocytes.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe how acetyl-CoA carboxylase function is regulated.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why glycolysis, the PDH complex, the TCA cycle and a citrate transporter are required for fatty acid synthesis.

    <!--list-separator-->

    -  Respiration: Ventilation - Perfusion Matching

        <!--list-separator-->

        -  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Understand the principle of partial pressures of gas.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Know the alveolar gas equation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Be familiar with the unique features of the pulmonary circulation.]({{< relref "be_familiar_with_the_unique_features_of_the_pulmonary_circulation" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  [Define the difference between V/Q for a shunt vs. V/Q for a dead space.]({{< relref "define_the_difference_between_v_q_for_a_shunt_vs_v_q_for_a_dead_space" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Use the alveolar gas equation to determine the general causes of hypoxemia.

        <!--list-separator-->

        -  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply the principle of partial pressures of gas.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Illustrate the alveolar gas equation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Distinguish the unique features of the pulmonary circulation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Classify the difference between V/Q for a shunt vs. V/Q for a dead space.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply the alveolar gas equation to determine the general causes of a low arterial oxygen level.


#### Week 3 {#week-3}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-25 Mon]</span></span>

    <!--list-separator-->

    -  Embryology Lecture 3

        <!--list-separator-->

        -  TBA

    <!--list-separator-->

    -  Muscle Intermediary Metabolism II

        <!--list-separator-->

        -  Pre-work <code>[9/11]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the features and functions of the following:]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}})

                <!--list-separator-->

                -  Triacylglycerol

                <!--list-separator-->

                -  Gastrointestinal lipase

                <!--list-separator-->

                -  Chylomicrons

                <!--list-separator-->

                -  Apolipoproteins

                <!--list-separator-->

                -  apoB48 & apoCII

                <!--list-separator-->

                -  lipoprotein lipase

                <!--list-separator-->

                -  Perilipin

                <!--list-separator-->

                -  Hormone-sensitive lipase

                <!--list-separator-->

                -  Fatty acyl-CoA

                <!--list-separator-->

                -  Fatty acyl-CoA synthetase

                <!--list-separator-->

                -  Carnitine

                <!--list-separator-->

                -  Albumin-bound fatty acids

                <!--list-separator-->

                -  Beta-oxidation

                <!--list-separator-->

                -  Carnitine

                <!--list-separator-->

                -  Carnitine acyltransferase I (CAT-1) and CAT-II

                <!--list-separator-->

                -  FAD- and NAD<sup>+</sup>-linked dehydrogenases

                <!--list-separator-->

                -  Propionyl-CoA

                <!--list-separator-->

                -  Propionyl-CoA carboxylase

                <!--list-separator-->

                -  L-methymalonyl-CoA

                <!--list-separator-->

                -  Methylmalonic acid

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the events required for packaging dietary fat into chylomicrons.]({{< relref "describe_the_events_required_for_packaging_dietary_fat_into_chylomicrons" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Explain why adipose tissue is referred to as a fat depot.]({{< relref "explain_why_adipose_tissue_is_referred_to_as_a_fat_depot" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the role of protein kinase A in mobilizing fat from adipocyte stores.]({{< relref "describe_the_role_of_protein_kinase_a_in_mobilizing_fat_from_adipocyte_stores" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Explain how fatty acids released from adipocyte stores are utilized.]({{< relref "explain_how_fatty_acids_released_from_adipocyte_stores_are_utilized" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recall, then detail, the three stages which define the ß-oxidation of fatty acids.

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the products of the ß-oxidation of odd-chain fatty acids.]({{< relref "list_the_products_of_the_ß_oxidation_of_odd_chain_fatty_acids" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Detail the vitamin requirements for the metabolism of propionyl-CoA.]({{< relref "detail_the_vitamin_requirements_for_the_metabolism_of_propionyl_coa" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  List the major characteristics of the beta-oxidation of fatty acids.

        <!--list-separator-->

        -  Session <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how the problem posed by the insolubility of triacylglycerols is overcome in:

                <!--list-separator-->

                -  Their digestion & absorption

                <!--list-separator-->

                -  Their intravascular tranpsort and delivery to extrahepatic tissues

                <!--list-separator-->

                -  Their storage and subsequent mobilization from adipocyte stores

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the major mechanisms governing dietary fat acquisition and its utilization.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how and why the lack of insulin regulates fat mobilization from adipocyte stores.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the major events required for fat mobilization and release from adipocytes and its subsequent utilization.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why fatty acids are excellent sources of energy.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the differences and similarities by which hormonal and intracellular conditions regulate the mobilization of intramuscular fat and glycogen stores in muscle.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the consequences of a biotin or vitamin B12 deficiency in the ß-oxidation of fatty acids.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue]</span></span>

    <!--list-separator-->

    -  Anatomy clinical correlations

        <!--list-separator-->

        -  Session <code>[19/29]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the neurovasculature of the intercostal spaces.]({{< relref "describe_the_neurovasculature_of_the_intercostal_spaces" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the procedures associated with intercostal nerve anesthesia and thoracotomy and their indications.]({{< relref "describe_the_procedures_associated_with_intercostal_nerve_anesthesia_and_thoracotomy_and_their_indications" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Explain the anatomical basis for the aspiration of foreign objects into the bronchial tree.]({{< relref "explain_the_anatomical_basis_for_the_aspiration_of_foreign_objects_into_the_bronchial_tree" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the anatomical locations for auscultation of the heart valves.]({{< relref "describe_the_anatomical_locations_for_auscultation_of_the_heart_valves" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe lymphatic flow in the breast.]({{< relref "describe_lymphatic_flow_in_the_breast" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe common patterns of cancer metastasis in the venous and lymphatic systems and the modality used to diagnose the presence of breast carcinoma.]({{< relref "describe_lymphatic_flow_in_the_breast" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the relevant anatomy and disease etiology and where applicable, the clinical presentation of disorders associated with the superior mediastinum.]({{< relref "describe_the_relevant_anatomy_and_disease_etiology_and_where_applicable_the_clinical_presentation_of_disorders_associated_with_the_superior_mediastinum" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the clinical significance of the transverse pericardial sinus.]({{< relref "describe_the_clinical_significance_of_the_transverse_pericardial_sinus" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [List the two landmarks for approximating dermatome levels in the thorax.]({{< relref "list_the_two_landmarks_for_approximating_dermatome_levels_in_the_thorax" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the etiology of herpes zoster as well as the clinical presentation of the disease.]({{< relref "describe_the_etiology_of_herpes_zoster_as_well_as_the_clinical_presentation_of_the_disease" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the vessels supplying the conduction system of the heart and the clinical presentation following damage to these.]({{< relref "describe_the_vessels_supplying_the_conduction_system_of_the_heart_and_the_clinical_presentation_following_damage_to_these" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.]({{< relref "describe_the_common_clinical_procedures_involving_the_heart_include_a_description_of_the_procedure_s_and_the_structure_s_that_are_accessed_or_repaired" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the meaning of pleural recesses and describe the locations of the costodiaphragmatic and costmediastinal recesses.]({{< relref "describe_the_meaning_of_pleural_recesses_and_describe_the_locations_of_the_costodiaphragmatic_and_costmediastinal_recesses" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the events leading to lung collapse.]({{< relref "describe_the_events_leading_to_lung_collapse" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the presentation of pneumothorax, hydrothorax and hemothorax.]({{< relref "describe_the_presentation_of_pneumothorax_hydrothorax_and_hemothorax" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the procedure of thoracocentesis, highlighting the rationale behind the use of the technique.]({{< relref "describe_the_procedure_of_thoracocentesis_highlighting_the_rationale_behind_the_use_of_the_technique" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the etiology of pleuritis.]({{< relref "describe_the_etiology_of_pleuritis" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe the causes of pulmonary emboli and the clinical consequences associated with it.]({{< relref "describe_the_causes_of_pulmonary_emboli_and_the_clinical_consequences_associated_with_it" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the anatomical mechanisms of referred pain.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe how pain in the thorax (e.g. angina, pericardial and pleural) is referred to different dermatomes and be able to list the locations in which pain is perceived.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the differences between the fibrous and serous pericardium and describe the location of the pericardial cavity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the terms pericarditis, pericardial effusion, hemopericardium, and cardiac tamponade.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the procedure of pericardiocentesis and its indications.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the locations of the superior and inferior apertures and the boundaries of these.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the locations, symptoms and causes of thoracic outlet syndrome.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the differences between valvular stenosis and insufficiency and list the reasons that may lead to the development of one or the other.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the various valvular diseases including in your description the diseased structure as well as the pattern of abnormal flow of blood.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the locations of the lines of the thoracic wall.

    <!--list-separator-->

    -  Cardio & Respiratory Integration

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[-]</span>  [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Illustrate the interaction of respiratory receptors and mechanisms of dealing with hypoxemia.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Evaluate the changes in cardiovascular mechanics related to ischemic heart disease.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Distinguish the cardiac and respiratory effects associated with acute lung injury.


### Block 5 {#block-5}


#### Week 1 {#week-1}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon]</span></span>

    <!--list-separator-->

    -  Mapping the Nephron Small Group

        <!--list-separator-->

        -  Session <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe concepts of simple diffusion, facilitated diffusion, channel proteins, and primary/secondary active transport.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe homeostasis and be able to define molarity, osmolarity, osmolality, osmotic pressure, and tonicity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Sketch the key elements of the nephron indicating where and how water and sodium enter and leave.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discriminate between where in the nephron excretion, filtration, reabsorption, and secretion occur.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Construct a diagram of the concentration gradient in the nephron to explain how it is created.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the function of concentration gradient in the nephron.

    <!--list-separator-->

    -  Renal Physiology: Glomerular Filtration

        <!--list-separator-->

        -  Pre-work <code>[5/6]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}})

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize that what is excreted in urine = what’s filtered at glomerulus – what’s reabsorbed in the tubule + what’s secreted in the tubule or Excretion = Filtration - Reabsorption + Secretion

        <!--list-separator-->

        -  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Assess how the glomerular filtration barrier affects filtrate and thus the blood and urine – what will be present/absent in filtrate and which situations might alter filtrate

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Outline what the glomerular filtration rate (GFR) is, how we calculate it, and why it is important clinically

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Differentiate the major regulators of renal blood flow (RBF) and GFR and how each would be expected to affect RBF and GFR. From this, predict which regulators will act in clinical situations

    <!--list-separator-->

    -  The Practice of Evidence-Based Medicine: Inferential Statistics

        <!--list-separator-->

        -  Pre-work <code>[7/7]</code>

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Compare and contrast sample and population]({{< relref "compare_and_contrast_sample_and_population" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Compare and contrast the null hypothesis and the alternative hypothesis]({{< relref "compare_and_contrast_the_null_hypothesis_and_the_alternative_hypothesis" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Compare and contrast type I error and type II error]({{< relref "compare_and_contrast_type_i_error_and_type_ii_error" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Define statistical power and explain how it affects the interpretation of negative results]({{< relref "define_statistical_power_and_explain_how_it_affects_the_interpretation_of_negative_results" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Explain the meaning of a p-value less than 0.05]({{< relref "explain_the_meaning_of_a_p_value_less_than_0_05" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Interpret confidence intervals around a sample size or calculated relative risk or odds ratio]({{< relref "interpret_confidence_intervals_around_a_sample_size_or_calculated_relative_risk_or_odds_ratio" >}})

            <!--list-separator-->

            - <span class="org-todo done _X_">[X]</span>  [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}})

        <!--list-separator-->

        -  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze measures of association

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast statistical significance, clinical significance, and effect size

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Interpret methods and results sections from manuscripts

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-02 Tue]</span></span>

    <!--list-separator-->

    -  Histology: Urinary System

        <!--list-separator-->

        -  Pre-work <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast sample and population

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the null hypothesis and the alternative hypothesis

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast type I error and type II error

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define statistical power and explain how it affects the interpretation of negative results

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the meaning of a p-value less than 0.05

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Interpret confidence intervals around a sample size or calculated relative risk or odds ratio

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the t-test and the factors that influence t-values

        <!--list-separator-->

        -  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze measures of association

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast statistical significance, clinical significance, and effect size

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Interpret methods and results sections from manuscripts

    <!--list-separator-->

    -  Ethics - Respect for Autonomy Meets Beneficence: Pediatric Ethics

        <!--list-separator-->

        -  Pre-work <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize how respect for autonomy, beneficence and justice are altered when applied to the care of children.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Review key concepts in pediatric ethics like assent, best interest, the right to an open future, and maternal-fetal conflict.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Consider how we balance parental decision-making authority with pediatric autonomy around reproductive health care.

        <!--list-separator-->

        -  Session <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss a spectrum of respect for parental authority.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Propose ways to address a father’s refusal of blood products for his son.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Critique ways to resolve the ethical and legal issues raised by a young woman’s request to receive reproductive health services without parental notification.

    <!--list-separator-->

    -  Renal Physiology: Tubules and electrolytes and water/sodium regulation

        <!--list-separator-->

        -  Pre-work <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the general role of each region of the renal tubule. The regions are proximal convoluted tubule, proximal straight tubule, descending thin limb of Loop of Henle, ascending thin limb of Loop of Henle, thick ascending limb of Loop of Henle, distal convoluted tubule, connecting tubule, cortical collecting duct, and outer and inner medullary collecting duct.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify what is absorbed in each region and how this contributes to overall tubular function.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Name the major regulators of sodium balance (aldosterone, angiotensin, GFR, ANP) and where they act (including channels they act on if relevant)

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify how ADH and aquaporins are critical for water reabsorption and how they change the composition of the urine

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recognize the factors that are important in the formation of the corticomedullary concentration gradient

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why the Na-K-ATPase is important and provide at least 2 examples of where it’s important.

        <!--list-separator-->

        -  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe how and under what circumstances the kidney produces a concentrated or dilute urine

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Outline how angiotensin II, aldosterone, anti-diuretic hormone, ANP and parathyroid hormone impact filtrate content and what is reabsorbed vs. excreted in the tubule.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discuss how the medullary osmotic gradient contributes to the ability to concentrate urine

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how the structure of the kidney underlies its physiological functions.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Differentiate how sodium and water reabsorption are regulated in the tubule

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-03 Wed]</span></span>

    <!--list-separator-->

    -  Metabolism Review Workshop

        <!--list-separator-->

        -  Session <code>[0/18]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  This absolute monster of a learning objective:

                Consider the following metabolic processes: a) glycolysis, b) the formation of pyruvate’s four products, c) the pyruvate dehydrogenase complex, d) the TCA cycle, e) gluconeogenesis, f) glycogenesis, g) glycogenolysis, h) fatty acid synthesis and i) the ß-oxidation of fatty acids… A. Where appropriate, and in broad strokes, explain if and why it is hormonally regulated. B. Where appropriate identify the enzyme catalyzing the committed and/or rate-limiting step and explain how it is regulated, both hormonally and mechanistically. C. Identify the enzymes involved in those processes whose activity will be compromised by 1) a deficiency of one, or more, of the following B vitamins… B1, B2, B3, B5 and B7… and 2) explain the role of the vitamin in that enzyme’s function. D. Other than those discussed in B above, list those enzymes that are protein kinase A substrates AND indicate how PKA-catalyzed phosphorylation affects their function.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how glycolysis is regulated in cells other than hepatocytes, and the significance of that regulation

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how glycolysis and gluconeogenesis are reciprocally regulated in the liver in the fed and fasting states.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the significance of pyruvate’s reversible conversion to alanine and lactate.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how the pyruvate dehydrogenase complex functions and is regulated.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the reciprocal regulation of the pyruvate dehydrogenase complex and pyruvate carboxylase, and the requirement for, and significance of, that regulation taking into account tissue specificity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how the TCA cycle is regulated, and the significance of that regulation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how defects in the electron transport chain/oxidative phosphorylation will cause lactic acidosis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how dietary triacylglycerols reach extrahepatic tissues in the fed state, including an explanation of why adipose tissue is referred to as the ultimate fat depot.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how the fasting state enables triacylglycerol stores to be mobilized from adipose stores and its significance taking into account tissue specificity.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how acetyl-CoA carboxylase activity is regulated and the significance of that regulation to both fatty acid synthesis vs. fatty acid oxidation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail the steps required for the ß-oxidation of mobilized even- and odd-chained fatty acids and the products obtained.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the ways in which the ß-oxidation of fatty acids in hepatocytes enables gluconeogenesis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the central roles that glycolysis, the pyruvate dehydrogenase complex, pyruvate carboxylase and the TCA cycle play in effecting fatty acid synthesis.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Detail how glycogen metabolism is regulated in the fed and fasting/stressed states in both striated muscle and hepatocytes, as well as its importance to whole body metabolism.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how muscle contractions impact both glycogenolysis and the mobilization of triacylglycerol stores in myocytes.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how the regulation of these metabolic processes is geared to ensuring that a) appropriate blood glucose levels are sustained for utilization by the brain and b) protein use as a gluconeogenic substrate is spared.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Consider the intermediary metabolism you have learned to date and explain why the liver is referred to as the “metabolic” brain.

    <!--list-separator-->

    -  Renal Physiology: Acid base

        <!--list-separator-->

        -  Pre-work <code>[0/3]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify why it is important to regulate acid/base balance

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the body’s acid/base management strategies including buffering

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the mechanism of renal bicarbonate reabsorption from filtrate and H+ excretion with titrable acids and ammonium

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define the mechanisms the kidney has to regulate acid/base management including renal bicarbonate reabsorption, H+ excretion and HCO3 excretion and review where this happens.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain what regulates acid/base management by the kidney.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Illustrate the kidney’s response to variations in pH.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply the above knowledge to clinical situations.

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-04 Thu]</span></span>

    <!--list-separator-->

    -  Embryology Lecture 4

        TBA

    <!--list-separator-->

    -  Renal Physiology: Body Fluids

        <!--list-separator-->

        -  Pre-work <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define osmolarity, osmolality and tonicity

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare effective vs ineffective osmoles

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recall how tonicity and extracellular fluid (ECF) volume relate to the kidney and are regulated

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the distribution of body fluids between intracellular and extracellular space and be able to diagram this distribution

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the major electrolyte differences between the intracellular fluid and the extracellular fluid

        <!--list-separator-->

        -  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Predict what will happen if cells are placed in various solutions of different osmolality and begin to extrapolate this to the human body

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Examine how osmolality and tonicity are regulated (free water excretion/absorption, regulated by ADH and thirst)

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Analyze how ECF volume is regulated (Na absorption with isotonic fluid following it, aldosterone)

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Demonstrate how the separate regulation of water and sodium reabsorption in the kidney are critical to our discussions about osmolality and ECF volume

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Predict how ECF and ICF volume and osmolarity will change in different situations (by applying above knowledge)

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-05 Fri]</span></span>

    <!--list-separator-->

    -  The Practice of Evidence-Based Medicine: Practice Guidelines

        <!--list-separator-->

        -  Pre-work <code>[0/6]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Define a clinical practice guideline

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the difference between a systematic review and a clinical practice guideline

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the use and users of practice guidelines

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the components of a credible guideline

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the rationale for each of the 8 components of a credible guideline and explain in detail how guidelines grade the strength of evidence and strength of recommendation.

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain why guidelines on the same clinical question by different expert groups may disagree

        <!--list-separator-->

        -  Session <code>[0/4]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the reasons for strong and weak recommendations

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the reasons for high and low quality strength of evidence

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Interpret practice guidelines

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply practice guidelines to the care of patients

    <!--list-separator-->

    -  Renal Physiology: Sodium, Potassium and Clearance

        <!--list-separator-->

        -  Pre-work <code>[0/7]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the determinants of the plasma sodium concentration

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Start to differentiate between disturbances of sodium concentration and sodium content

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Describe the sites in the tubule involved in potassium reabsorption and excretion

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Identify the regulators of potassium balance: both transcellular distribution of potassium and excretion of potassium in the urine

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Recall why sodium reabsorption and potassium secretion are linked

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain how to calculate renal clearance of a substance

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Explain the characteristics of an ideal substance to use to calculate GFR

        <!--list-separator-->

        -  Session <code>[0/5]</code>

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Discriminate between the total body sodium vs the plasma sodium concentration

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast the disorders of body Na+ content vs plasma sodium concentration [Na]

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast disorders of body fluids and understand what might have happened to cause them

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Compare and contrast disorders of potassium balance and understand what might have happened to cause the disorder

            <!--list-separator-->

            - <span class="org-todo todo ___">[ ]</span>  Apply renal clearance and GFR calculations clinically


#### Week 2 {#week-2}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-08 Mon]</span></span>

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-09 Tue]</span></span>

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-10 Wed]</span></span>

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-11 Thu]</span></span>

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-12 Fri]</span></span>


#### Week 3 {#week-3}

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-15 Mon]</span></span>

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-16 Tue]</span></span>

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-17 Wed]</span></span>

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-18 Thu]</span></span>

<!--list-separator-->

-  <span class="timestamp-wrapper"><span class="timestamp">[2021-11-19 Fri]</span></span>


## Things to add to notes {#things-to-add-to-notes}


### <span class="org-todo todo TODO">TODO</span> Hemoglobin {#hemoglobin}


#### <span class="org-todo todo ___">[ ]</span> R and T conformation {#r-and-t-conformation}


#### <span class="org-todo todo ___">[ ]</span> Alpha-, beta-, and gamma-globin {#alpha-beta-and-gamma-globin}


### <span class="org-todo done DONE">DONE</span> DNA Polymerase α, β, γ {#dna-polymerase-α-β-γ}


### <span class="org-todo todo TODO">TODO</span> Pentose phosphate pathway {#pentose-phosphate-pathway}


### <span class="org-todo done DONE">DONE</span> Fatty acid synthesis {#fatty-acid-synthesis}


### <span class="org-todo done DONE">DONE</span> Fatty acid oxidation {#fatty-acid-oxidation}


### <span class="org-todo todo TODO">TODO</span> Alpha tubulin and beta tubulin {#alpha-tubulin-and-beta-tubulin}

-   Alpha tubulin - GTP cannot be used for energy because it cannot be hydrolized


### <span class="org-todo todo TODO">TODO</span> Eddy current {#eddy-current}


### <span class="org-todo todo TODO">TODO</span> Myelin internode {#myelin-internode}

-   Where myelin itself is on cell


### <span class="org-todo todo TODO">TODO</span> Glial cell {#glial-cell}


### <span class="org-todo done DONE">DONE</span> ABPs {#abps}

-   Actin binding proteins


### <span class="org-todo todo TODO">TODO</span> Axon transport {#axon-transport}


### <span class="org-todo todo TODO">TODO</span> Profilin {#profilin}

-   Binds to actin at positive end


### <span class="org-todo todo TODO">TODO</span> Skeletal muscle contraction {#skeletal-muscle-contraction}


### <span class="org-todo done DONE">DONE</span> Perilipin {#perilipin}

-   Activates HSL


### <span class="org-todo done DONE">DONE</span> B vitamins {#b-vitamins}

teset


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
