+++
title = "Necrosis"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Lactate dehydrogenase]({{< relref "lactate_dehydrogenase" >}}) {#lactate-dehydrogenase--lactate-dehydrogenase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Circulating levels can indicate [necrosis]({{< relref "necrosis" >}})

    ---


#### [Define apoptosis and necrosis]({{< relref "define_apoptosis_and_necrosis" >}}) {#define-apoptosis-and-necrosis--define-apoptosis-and-necrosis-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Intro (p. 1021)**

    [Apoptosis]({{< relref "apoptosis" >}}) is programmed cell death, whereas [necrosis]({{< relref "necrosis" >}}) occurs in response to suboptimal conditions such as truama or inadequate blood supply

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
