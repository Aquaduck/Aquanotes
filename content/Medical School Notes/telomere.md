+++
title = "Telomere"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes**

    <!--list-separator-->

    -  [Telomere]({{< relref "telomere" >}}) replication

        -   Unlike prokaryotic chromosomes, eukaryotic chromosomes are linear
            -   When the replication fork reaches the end of the linear chromosome, there is **no way** to replace the primer on the 5' end of the lagging strand
                -   The DNA at the ends of the chromosome remain unpaired, and over time these get ends get progressively shorter as cells continue to divide
        -   _Telomeres_ comprise repetitive sequences that code for no gene
            -   In humans, this is a six-base-pair sequence TTAGGG which is repeated 100 to 1000 times in the telomere regions
            -   This in effect **protects the genes from getting deleted as cells continue to divide**
        -   [Telomerase]({{< relref "telomerase" >}}) is responsible for adding telomeres
            -   Contains two parts: a **catalytic part** and a **built-in RNA template**
                -   Attaches to end of a chromosome via DNA nucleotides complementary to the RNA template
                    -   Corresponding nucleotides are added on the 3' end of the DNA strand
                    -   Once sufficiently elongated, DNA pol adds the complementary nucleotides to the other strand -> chromosomes successfully replicated
            -   Telomerase is typically active in germ cells and adult stem cells, and **not** active in adult somatic cells


### Unlinked references {#unlinked-references}

[Show unlinked references]
