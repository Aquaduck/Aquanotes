+++
title = "Tryptophan"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Niacin]({{< relref "niacin" >}}) {#niacin--niacin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Derived from [tryptophan]({{< relref "tryptophan" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
