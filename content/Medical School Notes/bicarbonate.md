+++
title = "Bicarbonate"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Metabolic Alkalosis: Primary Bicarbonate Excess (p. 1281)**

    Can be transiently caused by an ingestion of excessive [bicarbonate]({{< relref "bicarbonate" >}}), [citrate]({{< relref "citrate" >}}), or [antacids]({{< relref "antacid" >}})

    ---

<!--list-separator-->

-  **🔖 Metabolic Alkalosis: Primary Bicarbonate Excess (p. 1281)**

    [Blood]({{< relref "blood" >}}) is too alkaline (pH above 7.45) due to **too much [bicarbonate]({{< relref "bicarbonate" >}})** (called _primary bicarbonate excess_)

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    E.g. [Sodium]({{< relref "sodium" >}}), [Potassium]({{< relref "potassium" >}}), [Chloride]({{< relref "chloride" >}}), [Bicarbonate]({{< relref "bicarbonate" >}}), [Glucose]({{< relref "glucose" >}}), [Urea]({{< relref "urea" >}}), [amino acids]({{< relref "amino_acid" >}}), peptides such as [Insulin]({{< relref "insulin" >}}) and [ADH]({{< relref "antidiuretic_hormone" >}})

    ---


#### [Describe the molecular defect that leads to CF.]({{< relref "describe_the_molecular_defect_that_leads_to_cf" >}}) {#describe-the-molecular-defect-that-leads-to-cf-dot--describe-the-molecular-defect-that-leads-to-cf-dot-md}

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face > Cystic fibrosis**

    [Bicarbonate]({{< relref "bicarbonate" >}}) in the intestines is needed to buffer gastric acidity and enable the activation of pancreatic enzymes

    ---

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face > Cystic fibrosis**

    [Bicarbonate]({{< relref "bicarbonate" >}}) release in the airway is important for the proper unfolding of [mucins]({{< relref "mucin" >}}) and defending against bacteria

    ---

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face > Cystic fibrosis**

    Conducts conducting chloride and [bicarbonate]({{< relref "bicarbonate" >}}) at the apical membrane of different epithelia

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
