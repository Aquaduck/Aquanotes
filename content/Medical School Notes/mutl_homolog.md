+++
title = "MutL homolog"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Form heterodimers that mimic [MutL]({{< relref "mutl" >}}) in _E. coli_
-   MLH1, MLH2, MLH3, PMS1, and PMS2
-   Involved in [DNA mismatch repair]({{< relref "dna_mismatch_repair" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [PMS2]({{< relref "pms2" >}}) {#pms2--pms2-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [MutL homolog]({{< relref "mutl_homolog" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
