+++
title = "Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease."
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}


### [Obstructive lung disease]({{< relref "obstructive_lung_disease" >}}) {#obstructive-lung-disease--obstructive-lung-disease-dot-md}

-   Increased resistance to airflow caused by narrowing of airways


#### Etiology {#etiology}

-   [COPD]({{< relref "copd" >}})
-   Bronchial asthma
-   Bronchiectasis, [cystic fibrosis]({{< relref "cystic_fibrosis" >}})
-   Narrowing of extrathoracic airways: laryngeal tumors, vocal cord palsy


### [Restrictive lung disease]({{< relref "restrictive_lung_disease" >}}) {#restrictive-lung-disease--restrictive-lung-disease-dot-md}

-   Impaired ability of lungs to expand (i.e. reduced lung compliance)


#### Etiology {#etiology}

-   _Intrinsic causes_:
    -   [Interstitial lung disease]({{< relref "interstitial_lung_disease" >}})
-   _Extrinsic causes_:
    -   Diseases of [pleura]({{< relref "pleura" >}}) and [pleural cavity]({{< relref "pleural_cavity" >}})
        -   Chronic pleural effusion
        -   Pleural adhesions
        -   [Pneumothorax]({{< relref "pneumothorax" >}})


### Findings of [obstructive]({{< relref "obstructive_lung_disease" >}}) vs. [restrictive lung disease]({{< relref "restrictive_lung_disease" >}}) {#findings-of-obstructive--obstructive-lung-disease-dot-md--vs-dot-restrictive-lung-disease--restrictive-lung-disease-dot-md}

| Measurement                                   | Obstructive lung disease                                             | Restrictive lung disease                             |
|-----------------------------------------------|----------------------------------------------------------------------|------------------------------------------------------|
| [FEV1]({{< relref "fev1" >}})                 | Low                                                                  | Low or normal                                        |
| FEV1/FVC                                      | Low                                                                  | Normal or high                                       |
| [VC]({{< relref "vital_capacity" >}})         | Low                                                                  | Low                                                  |
| [A-a gradient]({{< relref "a_a_gradient" >}}) | High                                                                 | Normal (extrinsic causes) or high (intrinsic causes) |
| Lung compliance                               | Normal (may be increased in [emphysema]({{< relref "emphysema" >}})) | Normal (extrinsic causes) or low (intrinsic causes)  |


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Cardio & Respiratory Integration > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
