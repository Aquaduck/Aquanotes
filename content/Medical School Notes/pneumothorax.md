+++
title = "Pneumothorax"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   Air in chest cavity causes loss of negative pressure -> collapsed [lung]({{< relref "lung" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the presentation of pneumothorax, hydrothorax and hemothorax.]({{< relref "describe_the_presentation_of_pneumothorax_hydrothorax_and_hemothorax" >}}) {#describe-the-presentation-of-pneumothorax-hydrothorax-and-hemothorax-dot--describe-the-presentation-of-pneumothorax-hydrothorax-and-hemothorax-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Pneumothorax]({{< relref "pneumothorax" >}})

        -   Entry of air into pleural cavity
        -   Usually from penetrating wound of parietal pleura
        -   Fractured ribs may also tear visceral pleural and lung


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Restrictive lung disease > Etiology**

    [Pneumothorax]({{< relref "pneumothorax" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
