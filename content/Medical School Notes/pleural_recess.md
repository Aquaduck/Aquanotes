+++
title = "Pleural recess"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Describe the meaning of pleural recesses and describe the locations of the costodiaphragmatic and costmediastinal recesses.]({{< relref "describe_the_meaning_of_pleural_recesses_and_describe_the_locations_of_the_costodiaphragmatic_and_costmediastinal_recesses" >}}) {#describe-the-meaning-of-pleural-recesses-and-describe-the-locations-of-the-costodiaphragmatic-and-costmediastinal-recesses-dot--describe-the-meaning-of-pleural-recesses-and-describe-the-locations-of-the-costodiaphragmatic-and-costmediastinal-recesses-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Pleural recesses]({{< relref "pleural_recess" >}})

        -   Several parts of thorax where lung (and consequently the visceral pleura) is **not in contact with the parietal pleura** -> _Pleural reflections_

        <!--list-separator-->

        -  [Costodiaphragmatic recesses]({{< relref "costodiaphragmatic_recess" >}})

            -   There are two - right and left
            -   Spaces between lower border of lung and pleural reflections from chest wall onto diaphgram
            -   Potential spaces where flui dmay accumulate
                -   Excess fluid may be drained (pleural tap or thoracentesis) from pleural cavity **without penetrating lungs**
            -   **Only in forced inspiration do the lungs expand into these recesses**

        <!--list-separator-->

        -  [Costomediastinal recess]({{< relref "costomediastinal_recess" >}})

            -   On left side between anterior chest wall and pericardial sac
            -   heart lies close to anterior thoracic wall and costal pleura is close to the mediastinal pleura


#### [Costomediastinal recess]({{< relref "costomediastinal_recess" >}}) {#costomediastinal-recess--costomediastinal-recess-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [Pleural recess]({{< relref "pleural_recess" >}})

    ---


#### [Costodiaphragmatic recess]({{< relref "costodiaphragmatic_recess" >}}) {#costodiaphragmatic-recess--costodiaphragmatic-recess-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [Pleural recess]({{< relref "pleural_recess" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
