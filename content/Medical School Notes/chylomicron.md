+++
title = "Chylomicron"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A [lipoprotein]({{< relref "lipoprotein" >}})
-   [Skeletal muscle]({{< relref "skeletal_muscle" >}}) obtains [dietary fatty acids]({{< relref "dietary_lipid" >}}) from chylomicrons that can be "re-synthesized" into [triacylglycerol]({{< relref "triacylglycerol" >}}) due to the muscle's regular uptake of glucose


## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoCII > From Amboss**

    A component of [chylomicrons]({{< relref "chylomicron" >}}), [VLDL]({{< relref "very_low_density_lipoprotein" >}}), and [HDL]({{< relref "high_density_lipoprotein" >}})

    ---

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoB48 > From Amboss**

    A component of [chylomicrons]({{< relref "chylomicron" >}})

    ---

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoB48 > From Amboss**

    Mediates the secretion of [Chylomicron]({{< relref "chylomicron" >}}) particles that originate from the intestine into the lymphatics

    ---

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoB48 > From Fat Metabolism in Muscle & Adipose Tissue**

    **The structural protein of [chylomicrons]({{< relref "chylomicron" >}})**

    ---

<!--list-separator-->

-  [Chylomicrons]({{< relref "chylomicron" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   The **carriers of [dietary lipids]({{< relref "dietary_lipid" >}}) in the [blood]({{< relref "blood" >}})**
        -   Molecules of [apo-CII]({{< relref "apolipoprotein_cii" >}}) and [apo-CIII]({{< relref "apolipoprotein_ciii" >}}) are also integrated into the chylomicron "membrane"
        -   Formed chylomicrons are **exocytosed into the lymphatics** -> **enter venous circulation**
        -   Chylomicrons **indicate a fed state**
        -   [Skeletal muscle]({{< relref "skeletal_muscle" >}}) obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose


#### [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}}) {#lipoprotein-lipase--lipoprotein-lipase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Catalyzes lipolysis of [triglycerides]({{< relref "triacylglycerol" >}}) circulating in [chylomicrons]({{< relref "chylomicron" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Lipoprotein lipase**

    **Catalyzes lipolysis of triglycerides circulating in [chylomicrons]({{< relref "chylomicron" >}})**

    ---


#### [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}}) {#explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot--explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  Delivering dietary fat to extrahepatic tissues via [chylomicrons]({{< relref "chylomicron" >}})

        -   The formed chylomicrons are then **exocytosed into the lymphatics** -> **enter venous circulation**
        -   Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues
            -   [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**
                -   Done through its effects on gene transcription
            -   [Insulin]({{< relref "insulin" >}}) also **increases glucose uptake in skeletal muscle and adipose tissue**
        -   LPL will cleave the triacylglycerol to **either** _three [fatty acids]({{< relref "fatty_acid" >}}) and glycerol_ or _two fatty acids and a [monoacylglycerol]({{< relref "monoacylglycerol" >}})_
            -   Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})
        -   [Fatty acids]({{< relref "fatty_acid" >}}) will bind to various _fatty acid binding proteins_ -> able to traverse the cell and enter the tissue beneath
            -   In muscle, fatty acids will be oxidized for energy and/or stored
            -   In lactating [mammary glands]({{< relref "mammary_gland" >}}), fatty acids + monoacylglycerol become components of [milk]({{< relref "milk" >}})


#### [Apolipoprotein B-48]({{< relref "apolipoprotein_b_48" >}}) {#apolipoprotein-b-48--apolipoprotein-b-48-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The structural protein of [chylomicrons]({{< relref "chylomicron" >}})

    ---


#### [Apolipoprotein CII]({{< relref "apocii" >}}) {#apolipoprotein-cii--apocii-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    ApoCII anchors the [chylomicron]({{< relref "chylomicron" >}}) to the [LPL]({{< relref "lipoprotein_lipase" >}}) molecules expressed by capillary endothelial cells

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
