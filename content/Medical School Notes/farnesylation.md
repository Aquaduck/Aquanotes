+++
title = "Farnesylation"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}}) {#rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot--rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot-md}

<!--list-separator-->

-  **🔖 From Protein farnesylation and disease > Isoprenylation and postprenylation of nuclear lamins**

    [Farnesylation]({{< relref "farnesylation" >}}): the post-translational addition of the 15 carbon [farnesyl pyrophosphate]({{< relref "farnesyl_pyrophosphate" >}}) (FPP) via thioether bond to the sulphur atom of the cysteine of a carboxyterminal CAAX motif

    ---

<!--list-separator-->

-  **🔖 From Protein farnesylation and disease > Nuclear lamins > A-type lamins**

    Undergoes post-translational modification via [farnesylation]({{< relref "farnesylation" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
