+++
title = "Telomerase"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes**

    <!--list-separator-->

    -  [Telomerase]({{< relref "telomerase" >}}) and aging

        -   Cells that undergo cell division continue to have their telomeres shortened because most somatic cels do not make telomerase
            -   Means that **telomere shortening is associated with aging**
        -   In 2010, scientists found that telomerase cna reverse some age-related conditions in mice
            -   Telomerase-deficient mice had muscle atrophy, stem cell depletion, organ system failure, and impaired tissue injury responses
            -   Following reactivation of telomerase -> caused extension of telomeres, reduced DNA damage, reversed neurodegeneration, and improved function of testes, spleen, and intestines
        -   [Cancer]({{< relref "cancer" >}}) is characterized by uncontrolled cell division of abnormal cells
            -   Cells accumulate mutations, proliferate uncontrollably, and can migrate to different parts of the body through a process called [metastatis]({{< relref "metastasis" >}})
            -   Scientists have observed that **cancerous cells have considerably shortened telomeres and that telomerase is active in these cells**
                -   Only **after telomeres were shortened** did telomerase become active
                    -   If action of telomerase in these cells can be inhibited -> cancerous cells potentially stopped from further division

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Telomere replication**

    [Telomerase]({{< relref "telomerase" >}}) is responsible for adding telomeres

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
