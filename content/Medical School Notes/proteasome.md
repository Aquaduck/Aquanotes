+++
title = "Proteasome"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inhibitor of apoptosis]({{< relref "inhibitor_of_apoptosis" >}}) {#inhibitor-of-apoptosis--inhibitor-of-apoptosis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Polyubiquitylate caspases -> mark for destruction by [proteasomes]({{< relref "proteasome" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
