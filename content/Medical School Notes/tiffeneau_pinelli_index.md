+++
title = "Tiffeneau-Pinelli index"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Ratio of [FEV1]({{< relref "fev1" >}}) to [FVC]({{< relref "forced_vital_capacity" >}}) as a percentage


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
