+++
title = "Thrombocytosis"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   A condition where there are too many [platelets]({{< relref "thrombocyte" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Disorders of Platelets (p. 813)**

    [Thrombocytosis]({{< relref "thrombocytosis" >}}): too many platelets

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
