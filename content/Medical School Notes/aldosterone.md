+++
title = "Aldosterone"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Secreted from the [adrenal cortex]({{< relref "adrenal_cortex" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Renin-angiotensin-aldosterone mechanism]({{< relref "renin_angiotensin_aldosterone_mechanism" >}}) {#renin-angiotensin-aldosterone-mechanism--renin-angiotensin-aldosterone-mechanism-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Aldosterone]({{< relref "aldosterone" >}})

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Metabolic Alkalosis: Primary Bicarbonate Excess (p. 1281)**

    [Cushing's disease]({{< relref "cushing_s_disease" >}}): oversecretion of [ACTH]({{< relref "adrenocorticotrophic_hormone" >}}) -> elevated [aldosterone]({{< relref "aldosterone" >}}) + more [potassium]({{< relref "potassium" >}}) excreted in [urine]({{< relref "urine" >}})

    ---


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927)**

    <!--list-separator-->

    -  [Aldosterone]({{< relref "aldosterone" >}})

        -   Increases the reabsorption of [sodium]({{< relref "sodium" >}}) into the [blood]({{< relref "blood" >}}) by the [kidneys]({{< relref "kidney" >}}) -> **increases reabsorption of water** -> increases blood volume -> raises blood pressure

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Angiotensin (I + II)**

    Angiotensin II stimulates the release of [ADH]({{< relref "antidiuretic_hormone" >}}) from the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) and [aldosterone]({{< relref "aldosterone" >}}) from the [adrenal cortex]({{< relref "adrenal_cortex" >}})

    ---


#### [Atrial natriuretic hormone]({{< relref "atrial_natriuretic_hormone" >}}) {#atrial-natriuretic-hormone--atrial-natriuretic-hormone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Suppresses [Renin]({{< relref "renin" >}}), [Aldosterone]({{< relref "aldosterone" >}}), and [ADH]({{< relref "antidiuretic_hormone" >}}) production/release

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
