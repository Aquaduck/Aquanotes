+++
title = "Identify the fructose metabolite that reciprocally regulates the glycolytic and gluconeogenic pathways within the liver and the enzyme that catalyzes its formation"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

The metabolite is [Fructose-2,6-bisphosphate]({{< relref "fructose_2_6_bisphosphate" >}}) and it is formed by [PFK-2]({{< relref "pfk_2" >}})


## How it is used {#how-it-is-used}

{{< figure src="/ox-hugo/_20210829_180454f26bpfedvsfast.png" width="800" >}}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Identify the fructose metabolite that reciprocally regulates the glycolytic and gluconeogenic pathways within the liver and the enzyme that catalyzes its formation]({{< relref "identify_the_fructose_metabolite_that_reciprocally_regulates_the_glycolytic_and_gluconeogenic_pathways_within_the_liver_and_the_enzyme_that_catalyzes_its_formation" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
