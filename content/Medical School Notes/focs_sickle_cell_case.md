+++
title = "FoCS (Sickle Cell Case)"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Introduction {#introduction}

-   [Sickle cell disease]({{< relref "sickle_cell_disease" >}}) is a group of disorders that affects [hemoglobin]({{< relref "hemoglobin" >}})
-   People with this disorder have atypical hemoglobin called _hemoglobin S_
-   Signs + Symptoms begin in early childhood
-   Characteristic features of this disorder include:
    -   Low # of red blood cells (anemia)
    -   Repeated infections
    -   Periodic episodes of pain
-   Severity of symptoms varies
-   Signs and symptoms caused by **sickling of [red blood cells]({{< relref "red_blood_cell" >}})**
    -   Causes RBCs to breakdown prematurely -> leads to [anemia]({{< relref "anemia" >}})
        -   This rapid breakdown can lead to jaundice
    -   Pain caused by sickled RBCs getting stuck in small blood vessels
        -   Deprives tissues of O<sub>2</sub> and potentially leads to organ damage
            -   Organs such as: lungs, kidneys, spleen, and brain
-   A particularly serious complication of [sickle cell disease]({{< relref "sickle_cell_disease" >}}) is **high blood pressure** in the blood vessels that supply the lungs (pulmonary hypertension)
    -   Can lead to heart failure


## Genetics {#genetics}

-   Mutations in the _HBB_ gene cause [sickle cell disease]({{< relref "sickle_cell_disease" >}})
-   [Hemoglobin]({{< relref "hemoglobin" >}}) consists of four protein subunits
    -   Typically, two subunits called _alpha-globin_ and two subunits called _beta-globin_
    -   The _HBB_ gene provides instructions from making _beta-globin_
-   Various versions of _beta-globin_ can result from different mutations in the _HBB_ gene
    -   Some _HBB_ mutations can result in usually low levels of _beta-globin_ -> called _[beta thalassemia]({{< relref "beta_thalassemia" >}})_
-   Sickle cell disease (SCD) encompasses a group of disorders characterized by the presence of **at least one hemoglobin S allele** and **a second HBB pathogenic variant** -> **abnormal hemoglobin polymerization**


## [Pathophysiology]({{< relref "sickle_cell_disease" >}}) {#pathophysiology--sickle-cell-disease-dot-md}

-   HbS arises from a mutation substituting **thymine** for **adenine** in the sixth codon of the beta-chain gene, GAG to GTG
    -   Causes coding of **valine** instead of **glutamate** in position 6 of the Hb beta chain
    -   The resulting Hb has the physical properties of forming polymers _only in deoxy conditions_
        -   In presence of O<sub>2</sub>, liquid state prevails
-   **When [RBCs]({{< relref "red_blood_cell" >}}) containing homozygous HbS are exposed to deoxy conditions, the sickling process begins**
    -   A slow and gradual polymer formation ensues
    -   Repeated and prolonged sickling involves the membrane
    -   After recurrent episodes of sickling, membrane damage occurs and the cells are no longer capable of resuming regular form upon reoxygenation -> _Irreversibly Sickled Cells (ISCs)_
        -   5-50% of RBCs are ISCs
-   **When RBCs sickle, they gain Na<sup>+</sup> and lose K<sup>​+</sup>**
    -   Membrane **permeability to Ca<sup>2+</sup> increases** possibly due to \*impairment in the Ca<sup>2​+</sup> pump that depends on [ATPase]({{< relref "adenosine_triphosphatase" >}})
        -   Intracellular [Ca<sup>2+</sup>] rises to 4x the reference level
        -   **Membrane becomes more rigid**, although unclear whether this is due to calcium
-   Sickling may be precipated by multiple factors:
    -   Local tissue hypoxia
    -   Dehydration secondary to a viral illness
    -   Nausea and vomiting
    -   **All lead to hypertonicity of the plasma -> induce sickling**
    -   **Any event that can lead to acidosis -> sickling**
        -   E.g. infection or extreme dehydration
    -   Also caused by benign factors and environmental changes
        -   E.g. fatigue, exposure to cold, and psychosocial stress


## Clinical features of [SCD]({{< relref "sickle_cell_disease" >}}) {#clinical-features-of-scd--sickle-cell-disease-dot-md}

-   [SCD]({{< relref "sickle_cell_disease" >}}) usually manifests **early in childhood**
    -   Infants are protected largely by **elevated levels of Hb F**
        -   Soon after, condition becomes evident


### Vaso-occlusive complications/events {#vaso-occlusive-complications-events}

-   **Vaso-occlusive pain episodes are the most frequent cause of recurrent morbidity** and account for majority of SCD-related hospital admissions as well as school and work absences
-   **[Dactylitis]({{< relref "dactylitis" >}}) is often the earliest manifestation of SCD**
    -   Occurs in infants and children
-   **Splenic sequestration and infarction**
    -   Historically most children with [Hb]({{< relref "hemoglobin" >}}) S/S or S/[beta-thalassemia]({{< relref "beta_thalassemia" >}}) will have a dysfunctional spleen w/in first year of life and complete auto-infarction and atrophy due to ischemia of the spleen by five years
-   **Infection**
    -   Young children with SCD and splenic dysfunction are at high risk for **septicemia and meningitis** among other infections
-   **[Acute chest syndrome]({{< relref "acute_chest_syndrome" >}}) (ACS)**
    -   Major cause of mortality


### Neurologic complications {#neurologic-complications}

-   **Ischemic strokes**
    -   Overt strokes occur in as many as 11% of children with SCD, with peak occurrence between ages two and nine years
    -   Recurring strokes occur in 50%-70% of affected individuals within three years after first event
-   **Silent cerebral infarcts**


### Complications related to hemolysis {#complications-related-to-hemolysis}

-   Hyper-hemolysis syndrome
-   Chronic anemia
-   Jaundice and cholelithiasis
-   Predisposition to aplastic crisis
-   Pulmonary hypertension
-   Priapism
-   Cardiopulmonary complications


## Diagnosis {#diagnosis}


### Newborn screening {#newborn-screening}

-   Perform isoelectric focusing and/or HPLC of an eluate of dried blood spots
-   Normal screening result is "FA" (i.e. **more fetal hemoglobin (HbF) compared to adult hemoglobin [HbA]**)


## Management {#management}


### Patient education {#patient-education}

-   Patients must be able to:
    -   Recognize earliest signs of a vaso-occlusive crisis and seek help
    -   Treat all febrile illness promptly
    -   Identify environmental hazards that may precipitate a crisis
-   Families should be educated on the importance of hydration, diet, outpatient medications, and immunization protocol
    -   Emphasize importance of **prophylactic penicillin**


### Medical Prevention {#medical-prevention}

-   Chronic RBC transfusion therapy
-   Hydroxycarbaminde (hydroxyurea)
-   Glutamine
-   Stem cell transplantation (bone marrow transplantation)
-   Gene therapy


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
