+++
title = "BRS Cell Biology & Histology"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 8 linked references {#8-linked-references}


#### [Pericyte]({{< relref "pericyte" >}}) {#pericyte--pericyte-dot-md}

<!--list-separator-->

-  From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}})

    -   **Pericytes** are derived from embryonic mesenchymal cells and may retain a pluripotential role
        -   Possess characteristics of endothelial cells as well as smooth muscle cells
            -   Contain [actin]({{< relref "actin" >}}), [myosin]({{< relref "myosin" >}}), and [tropomyosin]({{< relref "tropomyosin" >}}) -> suggests they play a role in contraction
                -   This + proximity to capillaries -> could function to **modify capillary blood flow**
        -   Smaller than fibroblasts and **are located along most capillaries**
            -   Lie within their own basal lamina
        -   During blood vessel formation and repair, they may differentiate into smooth muscle cells or endothelial cells
        -   In response to injury, may give rise to endothelial cells, fibroblasts, and smooth muscle cells of blood vessel walls


#### [Name and describe the basic types of connective tissues]({{< relref "name_and_describe_the_basic_types_of_connective_tissues" >}}) {#name-and-describe-the-basic-types-of-connective-tissues--name-and-describe-the-basic-types-of-connective-tissues-dot-md}

<!--list-separator-->

-  From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}})

    <!--list-separator-->

    -  Classification of [connective tissue]({{< relref "connective_tissue" >}}) (p. 86)

        <!--list-separator-->

        -  Connective tissue proper

            <!--list-separator-->

            -  Loose CT

                -   Fewer fibers but more cells than dense CT
                -   Well vascularized, flexible, and not resistance to stress
                -   More abundant than dense CT
                -   Fills in spaces just deep to the skin

            <!--list-separator-->

            -  Dense CT

                -   More fibers but fewer cells than loose CT

                <!--list-separator-->

                -  Dense irregular CT

                    -   Most common
                    -   Contains fiber bundles that have **no definite orientation**
                    -   Characteristic of the dermis and capsules of many organs

                <!--list-separator-->

                -  Dense regular CT

                    -   Contains fiber bundles and attenuated fibroblasts arranged in a **uniform parallel fashion**
                    -   **Present only in tendons and ligaments**
                    -   May be collagenous or elastic


#### [Name and describe five basic epithelial types and relationship to the basement membrane.]({{< relref "name_and_describe_five_basic_epithelial_types_and_relationship_to_the_basement_membrane" >}}) {#name-and-describe-five-basic-epithelial-types-and-relationship-to-the-basement-membrane-dot--name-and-describe-five-basic-epithelial-types-and-relationship-to-the-basement-membrane-dot-md}

<!--list-separator-->

-  From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}})

    <!--list-separator-->

    -  Classification of epithelia table (p. 68)

        | Type                                | Shape of Superficial Layer          | Typical Locations                                            |
        |-------------------------------------|-------------------------------------|--------------------------------------------------------------|
        | Simple squamous                     | Flattened                           | Endothelium (lining of blood vessels)                        |
        |                                     |                                     | Mesothelium (lining of peritonium and pleura)                |
        | Simple cuboidal                     | Cuboidal                            | Lining of distal tubule in kidneys                           |
        |                                     |                                     | Lining of ducts in some glands                               |
        |                                     |                                     | Surface of ovary                                             |
        | Simple columnar                     | Columnar                            | Lining of intestine                                          |
        |                                     |                                     | Lining of stomach                                            |
        |                                     |                                     | Lining of excretory ducts in some glands                     |
        | Pseudostratified                    | All cells rest on basal lamina      | Lining of trachea                                            |
        |                                     | Not all reach the lumen             | Lining of primary bronchi                                    |
        |                                     | Causes appearance of stratification | Lining of nasal cavity                                       |
        |                                     |                                     | Lining of excretory ducts in some glands                     |
        | Stratified squamous (nonkeratinzed) | Flattened (nucleated)               | Lining of esophagus                                          |
        |                                     |                                     | Lining of vagina                                             |
        |                                     |                                     | Lining of mouth                                              |
        |                                     |                                     | Lining of true vocal cords                                   |
        | Stratified squamous (keratinized)   | Flattened (without nuclei)          | Epidermis of skin                                            |
        | Stratified cuboidal                 | Cuboidal                            | Lining of ducts in sweat glands                              |
        | Stratified columnar                 | Columnar                            | Lining of large excretory ducts in some glands               |
        |                                     |                                     | Lining of cavernous urethra                                  |
        | Transitional                        | Dome-shaped (when relaxed)          | Lining of urinary passages from renal calyces to the urethra |
        |                                     | Flattened (when stretched)          |                                                              |

    <!--list-separator-->

    -  Classification of epithelia diagrams (p.68)

        {{< figure src="/ox-hugo/_20210913_213055screenshot.png" >}}


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}})

    <!--list-separator-->

    -  Overview - [Muscle]({{< relref "muscle" >}})

        -   Muscle is classified into two types: _[striated]({{< relref "striated_muscle" >}})_ and _[smooth]({{< relref "smooth_muscle" >}})_
            -   Striated is further subdivided into _[skeletal]({{< relref "skeletal_muscle" >}})_ and _[cardiac]({{< relref "cardiac_muscle" >}})_ muscles
        -   Muscle cells possess _contractile filaments_ whose major components are [actin]({{< relref "actin" >}}) and [myosin]({{< relref "myosin" >}})

    <!--list-separator-->

    -  Structure of Skeletal Muscle (p. 109)

        <!--list-separator-->

        -  Connective tissue investments

            -   Convey neural and vascular elements ot muscle cells and provide a vehicle that harnesses the force of muscle contraction

            <!--list-separator-->

            -  Epimysium

                -   Surrounds an entire muscle and forms aponeuroses and tendons
                    -   [Aponeurosis]({{< relref "aponeurosis" >}}) connects muscle to muscle
                    -   [Tendons]({{< relref "tendon" >}}) connect muscle to bone

            <!--list-separator-->

            -  Perimysium

                -   Surrounds _fascicles_ (small bundles) of muscle cells

            <!--list-separator-->

            -  Endomysium

                -   Surrounds invididual muscle cells and is composed of _reticular fibers_ and an _external lamina_

        <!--list-separator-->

        - <span class="org-todo todo TODO">TODO</span>  Types of skeletal muscle cells

        <!--list-separator-->

        -  [Skeletal muscle cells]({{< relref "skeletal_muscle_cell" >}})

            -   Long, cylindrical, multinucleated cells enveloped by an external lamina and reticular fibers
            -   Skeletal muscle cell cytoplasm is called a [sarcoplasm]({{< relref "sarcoplasm" >}})
            -   Skeletal muscle cell membrane is called a [sarcolemma]({{< relref "sarcolemma" >}}) (variation of [plasmalemma]({{< relref "plasma_membrane" >}}) - another name for cell membrane)
                -   Form deep tubular invaginations called [Transverse (T) tubules]({{< relref "transverse_tubule" >}})

            <!--list-separator-->

            -  [Myofibril]({{< relref "myofibril" >}})

                -   Extend the entire length of the cell
                -   Composed of longitudinally arranged, cylindrical bundles of [thick]({{< relref "thick_myofilament" >}}) and [thin myofilaments]({{< relref "thin_myofilament" >}})

            <!--list-separator-->

            -  [Sarcomere]({{< relref "sarcomere" >}})

                <!--list-separator-->

                -  Structure diagram

                    {{< figure src="/ox-hugo/_20210913_202331screenshot.png" caption="Figure 1: Components of a sarcomere in myofibril" >}}

                    -   Labeled in diagram above:
                        -   Z disk
                        -   A band
                        -   I band
                        -   H zone
                        -   Myosin/thick filament
                        -   Actin/thin filament

            <!--list-separator-->

            -  [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) (SR)

                -   Modified smooth endoplasmic reticulum (SER) that surrounds myofilaments and forms a meshwork around each myofibril
                -   **Regulates muscle contraction** by sequestering calcium ions (leading to relaxation) or releasing calcium ions (leading to contraction)


#### [Describe the process of neuromuscular transmission.]({{< relref "describe_the_process_of_neuromuscular_transmission" >}}) {#describe-the-process-of-neuromuscular-transmission-dot--describe-the-process-of-neuromuscular-transmission-dot-md}

<!--list-separator-->

-  From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}})

    <!--list-separator-->

    -  Conduction of a nerve impulse across a myoneural junction (p. 116)

        1.  The presynaptic membrane is **depolarized**
        2.  **Voltage-gated [calcium]({{< relref "calcium" >}}) channels** open -> calcium enters axon terminal
        3.  Rise of [cytosolic calcium] -> **release of [acetylcholine]({{< relref "acetylcholine" >}}) into synaptic cleft**
        4.  Released ACh binds to receptors of the postsynaptic membrane -> **depolarization of sarcolemma** -> **generation of action potential**
        5.  [Acetylcholinesterase]({{< relref "acetylcholinesterase" >}}) degrades acetylcholine -> depolarization signal ends
        6.  Choline returns to the axon terminal to be recombined with acetyl-CoA (from mitochondria) via [choline acetyl transferase]({{< relref "choline_acetyl_transferase" >}}) to regenerate acetylcholine -> stored in synaptic vesicles at synaptic bouton
            -   Membranes of the emptied synaptic vesicles are recycled via [clathrin]({{< relref "clathrin" >}})-coated endocytic vesicles

        After this, continues at step 6 below (at muscle cell)


#### [Describe the morphology and function of glial cells of the PNS and CNS.]({{< relref "describe_the_morphology_and_function_of_glial_cells_of_the_pns_and_cns" >}}) {#describe-the-morphology-and-function-of-glial-cells-of-the-pns-and-cns-dot--describe-the-morphology-and-function-of-glial-cells-of-the-pns-and-cns-dot-md}

<!--list-separator-->

-  From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}})


#### [Describe the general morphology of neurons and levels of organization of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}}) {#describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot--describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot-md}

<!--list-separator-->

-  From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}})

    <!--list-separator-->

    -  III. Cells of nervous system (p. 127)

        -   [Neurons]({{< relref "neuron" >}}) consist of a cell body and its processes, which usually include multiple [dendrites]({{< relref "dendrite" >}}) and a single [axon]({{< relref "axon" >}})

        <!--list-separator-->

        -  Morphologic classification of neurons

            -   [Unipolar neurons]({{< relref "unipolar_neuron" >}}) possess a single process but are rare in vertebrates
            -   [Bipolar neurons]({{< relref "bipolar_neuron" >}}) possess a single axon and a single dendrite
                -   Present in some sense organs (e.g. vestibular-cochlear mechanism)
            -   [Multipolar neurons]({{< relref "multipolar_neuron" >}}) possess a single axon and more than one dendrite
                -   **The most common type of neuron in vertebrates**
            -   [Pseudounipolar neurons]({{< relref "pseudounipolar_neuron" >}}) possess a signle process that extends from the cell body and subsequently **branches** into an axon and dendrite
                -   Present in spinal and cranial ganglia
                -   Originate embryologically as bipolar cells -> axon and dendrite fuse into a single process functionally categorized as an axon
                -   Often referred to as just "unipolar" neuron

        <!--list-separator-->

        -  Functional classification of neurons

            -   [Sensory neurons]({{< relref "sensory_neuron" >}}) **receive stimuli** from the internal and external environments
                -   Conduct impulses **to the CNS** for processing and analysis
            -   [Interneurons]({{< relref "interneuron" >}}) connect other neurons in a chain or sequence
                -   Commonly connect sensory and motor neurons
                -   They also regulate signals transmitted to neurons
            -   [Motor neurons]({{< relref "motor_neuron" >}}) conduct impulses **from the CNS** to other neurons, muscles, and glands

        <!--list-separator-->

        -  [Neuron]({{< relref "neuron" >}}) structure

            <!--list-separator-->

            -  Neuronal cell body

                -   Region of a neuron containing the nucleus, various cytoplasmic organelles and inclusions, and cytoskeletal components

                <!--list-separator-->

                -  Nucleus

                    -   Large, spherical, pale staining
                    -   **Centrally** located in the soma of most neurons
                    -   Contains abundant euchromatin and a large nucleolus

                <!--list-separator-->

                -  Cytoplasmic organelles and inclusions

                    -   [Nissl bodies]({{< relref "nissl_body" >}}) are composed of polysomes and RER
                        -   Appear as clumps under light microscopy
                        -   Most abundant in **large motor proteins**
                    -   _Golgi complex_ is near the nucleus, and _mitochondria_ are scattered throughout the cytoplasm
                    -   _Melanin-containing granules_ are present in some neurons in the CNS and in the dorsal root and sympathetic ganglia
                    -   _Lipofuscin-containing granules_ are present in some neurons
                        -   Increase in number with age
                    -   _Lipid droplets_ occasionally present

                <!--list-separator-->

                -  Cytoskeletal components

                    -   [Neurofilaments]({{< relref "neurofilament" >}}) run throughout the soma cytoplasm
                        -   Are [intermediate filaments]({{< relref "intermediate_filament" >}}) composed of three intertwining polypeptide chains
                    -   _Microtubules_ are also present in the soma cytoplasm
                    -   _Microfilaments_ are associated with the plasma membrane

            <!--list-separator-->

            -  [Dendrites]({{< relref "dendrite" >}})

                -   Receive stimuli from sensory cells, axons, or other neurons -> convert these signals into action potentials transmitted **towards the soma**
                -   Lacks a Golgi complex
                -   Organelles are reduced or absent near terminals **except for mitochondria**, which are abundant
                -   Spines on surface of dendrites increase surface area for synapse formation
                    -   Diminish with age and poor neutrition

            <!--list-separator-->

            -  [Axons]({{< relref "axon" >}})

                -   Conduct impulses **away from** the soma to axon terminals **without any diminuition in their strength**
                -   Originate from the [axon hillock]({{< relref "axon_hillock" >}})
                    -   A specialized region of the soma that lacks RER, ribosomes, Golgi cisternae, and Nissle bodies
                    -   Contains many microtubules and neurofilaments
                        -   [Neurofilaments]({{< relref "neurofilament" >}}) regulate axon diameter
                        -   Permits passage of mitochondria and vesicles into the axon
                -   _Axon cytoplasm_ lacks a Golgi complex but contains SER, RER, and elongated mitochondria
                -   Terminate in many small branches ([axon terminals]({{< relref "axon_terminal" >}})) from which impulses are passed to another neuron or type of cell(s)


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  From [BRS Cell Biology & Histology 6th edition]({{< relref "brs_cell_biology_histology" >}})

    <!--list-separator-->

    -  Structural components

        <!--list-separator-->

        -  [Organelles]({{< relref "organelle" >}})

            <!--list-separator-->

            -  [Plasma membrane]({{< relref "plasma_membrane" >}})

                -   Envelops cell and forms boundary between it and adjacent structures

            <!--list-separator-->

            -  [Ribosomes]({{< relref "ribosome" >}})

                <!--list-separator-->

                -  Structure

                    -   Consist of _small_ and _large_ subunit
                        -   Composed of several types of [rRNA]({{< relref "ribosomal_rna" >}}) and [proteins]({{< relref "protein" >}})
                    -   Ribosomes may be **free in the cytosol** or **bound to membranes of the RER or outer nuclear membrane**
                    -   _Polyribosome (polysome)_: a cluster of ribosomes along a single strand of [mRNA]({{< relref "messenger_rna" >}}) engaged w/ the synthesis of [protein]({{< relref "protein" >}})

                <!--list-separator-->

                -  Function

                    -   **site where mRNA is translated into protein**
                        -   Proteins destined for transport -> synthesized in RER
                        -   Proteins not destined for transport -> synthesized in cytosol
                    -   **In translation**:
                        1.  _Small ribosomal unit_ binds [mRNA]({{< relref "messenger_rna" >}}) and activated [tRNAs]({{< relref "transfer_rna" >}}) -> codons of mRNA base-pair with corresponding anticodons of tRNAs
                        2.  Initiator tRNA recognizes the start codon (AUG) on the mRNA
                        3.  _Large ribosomal subunit_ binds to complex
                            -   _Peptidyl transferase_ in the large subunit catalyzes peptide bond formation -> addition of AAs
                        4.  Stop codon (UAA, UAG, UGA) causes release of polypeptide from ribosome -> ribosomal subunits dissociate from mRNA

            <!--list-separator-->

            -  [Rough endoplasmic reticulum]({{< relref "rough_endoplasmic_reticulum" >}}) (RER)

                <!--list-separator-->

                -  Structure

                    -   System of membrane-bound sacs/cavities
                    -   Outer surface studded with ribosomes (which is what "rough" means)
                    -   _Cisterna or lumen_: interior region
                    -   Outer nuclear membrane **continuous** with RER membrane
                    -   _Ribophorins_: receptors where large [ribosomal]({{< relref "ribosome" >}}) subunits bind
                    -   Abundant in cells synthesizing **secretory proteins**
                        -   In these cells, RER organized into many parallel arrays
                    -   RER sac closest to the Golgi apparatus makes buds w/o ribosomes -> form vesicles
                        -   This is considered a **transitional element**

                <!--list-separator-->

                -  Function

                    -   Location where **membrane-packaged [proteins]({{< relref "protein" >}})** are synthesized
                        -   Secretory proteins
                        -   Plasma membrane proteins
                        -   Lysosomal proteins
                    -   Monitors the assembly, retention, and degradation of certain proteins

            <!--list-separator-->

            -  [Smooth endoplasmic reticulum]({{< relref "smooth_endoplasmic_reticulum" >}}) (SER)

                <!--list-separator-->

                -  Structure

                    -   Irregular network of membrane-bound channels that **lacks ribosomes on its surface** (why its called "smooth")
                    -   Usually appears as branching, cross-connected (_anastomosing_) _tubules_ or _vesicles_ whose membranes **do not contain ribophorins**
                    -   Less common than RER
                    -   Prominent in cells synthesizing steroids, triglycerides, and cholesterol

                <!--list-separator-->

                -  Function

                    -   **Steroid hormone synthesis** occurs in SER-rich cells such as Leydig cells (make testosterone)
                    -   **Drug detoxification** occurs in hepatocytes
                        -   Follows proliferation of the SER in response to the drug _phenobarbital_
                            -   The oxidases that metabolize this drug are located in the SER
                    -   **Muscle contraction and relaxation** involve the release and recapture of Ca<sup>2+</sup> by the SER in **skeletal muscle cells**
                        -   These are called the _sarcoplasmic reticulum_

            <!--list-separator-->

            -  [Annulate lamellae]({{< relref "annulate_lamellae" >}})

                <!--list-separator-->

                -  Structure

                    -   Parallel stacks of membranes (usually 6 to 10) that resemble the nuclear envelope (including its pore complexes)
                    -   Often arraged with their _annuli_ (pores) in register and are **frequently continuous with the RER**

                <!--list-separator-->

                -  Function

                    -   Found in rapidly growing cells
                        -   Germ cells
                        -   Embryonic cells
                        -   Tumor cells
                    -   Function and significance remain unknown

            <!--list-separator-->

            -  [Mitochondria]({{< relref "mitochondrion" >}})

                <!--list-separator-->

                -  Structure

                    -   Rod-shaped
                    -   Have an outer membrane and inner membrane
                    -   Subdivided into an _intermembrane space_ between the two membranes and an inner _matrix_
                    -   Granules w/in the matrix bind the divalent cations Mg<sup>2+</sup> and Ca<sup>2+</sup>

                <!--list-separator-->

                -  Enzymes and genetic apparatus

                    -   Mitochondria contain the following:
                        1.  All of the enzymes of the [TCA]({{< relref "citric_acid_cycle" >}}) cycle in the matrix (except for succinate DH which is in the inner membrane)
                        2.  _ATP synthase_
                            -   Consists of head portion and a transmembrane H ^{+} carrier
                            -   Involved in [coupling oxidation to phosphorylation]({{< relref "oxidative_phosphorylation" >}}) of ADP to ATP
                        3.  Genetic apparatus in the matrix containing:
                            -   circular DNA
                            -   mRNA
                            -   tRNA
                            -   rRNA (w/ limited coding capacity)
                            -   **Most mitochondrial proteins are encoded by nuclear DNA**

                <!--list-separator-->

                -  Origin and proliferation

                    -   Mitochondria may have originated as _symbionts_ (intracellular parasites)
                        -   Anaerobic eukaryotic cells endocytosed aerobic microorganisms that evolved into mitochondria, which function in oxidative processes
                    -   Mitchondria proliferate by _fission_ (division) of preexisting mitochondria
                        -   Typically have a 10-day life span
                        -   Proteins needed to sustain mitochondria are imported into them from the cytosol

                <!--list-separator-->

                -  Mitochondrial ATP synthesis

                    -   Mitochondria synthesize ATP via the Krebs cycle
                    -   ATP also synthesized via [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}})

                <!--list-separator-->

                -  Condensed mitochondria

                    -   Result from a conformational change in the typical form in response to **uncoupling of oxidation from phosphorylation**
                    -   Size of **matrix decreased** -> matrix made **more dense**
                    -   Size of **intermembrane space enlarged**
                    -   Present in _brown fat cells_
                        -   Produce heat rather than ATP
                            -   Special transport protein in inner membrane uncouples respiration from ATP synthesis
                    -   Mitochondria swell in response to calcium, phosphate, and thyroxine -> induce increase in water uptake and uncoupling of phosphorylation
                        -   [ATP]({{< relref "atp" >}}) reverses the swelling

            <!--list-separator-->

            -  [Golgi apparatus]({{< relref "golgi_apparatus" >}}) (complex)

                <!--list-separator-->

                -  Structure

                    -   Consists of several membrane-bounded _cisternae_ arranged in a stack
                        -   _Cisternae_: disk-shaped and slightly curved, w/ flat centers and dilated rims
                            -   Size and shape vary
                            -   **Integral to packaging and modification processes that occur in the Golgi apparatus**
                        -   Positioned and held in place by microtubules
                    -   A distinct polarity exists across Golgi stack w/ many vesicles on one side and vacuoles on the other

                <!--list-separator-->

                -  Regions

                    1.  **Cis face** lies deep in cell toward nucleus next to RER
                        -   Outermost cisterna associated w/ _vesicular-tubular clusters (VTC)_
                            -   Network of interconnected tubes and vesicles
                    2.  **Medial compartment** composed of several cisternae lying between the cis and trans faces
                    3.  **Trans face** lies at the side of the stack facing the plasma membrane
                        -   Associated w/ vacuoles and secretory granules
                    4.  **trans-Golgi network** lies apart from the last cisterna at the trans face
                        -   separated from the Golgi stack
                        -   Sorts proteins for ther final destinations

                <!--list-separator-->

                -  Functions

                    -   Processes membrane-packed proteins synthesized in the RER and also recycles and redistributes membranes

            <!--list-separator-->

            -  [Coated vesicles]({{< relref "coated_vesicle" >}})

                <!--list-separator-->

                -  Clathrin-coated vesicles

                    <!--list-separator-->

                    -  Structure

                        -   _[Clathrin]({{< relref "clathrin" >}})_: consists of three large and three small polypeptine chains that form a three-legged structure (_triskelion_)
                            -   36 of these associate to form a polyhedral cage-like lattice around the vesicle
                        -   _Adaptins_: proteins part of the structure that:
                            -   Recognize and recruit clathrin coat
                            -   Capture cargo receptors containing specific molecules
                            -   Help establish the vesicle curvature
                        -   _Dynamin_: forms a ring around the neck of a budding vesicle/pit and aids in pinching it off the parent membrane
                            -   GTP-binding protein

                    <!--list-separator-->

                    -  Function

                        -   Mediate the continuous _constitutive protein transport_ within the cell
                            -   Specific GTP-binding proteins present at each step of vesicle budding and fusion
                            -   _[SNARE]({{< relref "snare" >}})_: proteins that ensure vesicles dock and fuse only with its correct target membrane
                        -   Transport proteins from RER -> VTC -> cis Golgi -> across cisternae -> TGN (anterograde transport)
                            -   [COP-II]({{< relref "cop_ii" >}}) transports molecules forward in path mentioned above
                            -   [COP-I]({{< relref "cop_i" >}}) facilitates **retrograde transport** - path above but **in reverse**

                <!--list-separator-->

                -  Caveolin-coated vesicles

                    <!--list-separator-->

                    -  Structure

                        -   Invaginations of the plasma membrane **in endothelial and smooth muscle cells**
                        -   Possess a distinct coat formed by the protein _caveolin_

                    <!--list-separator-->

                    -  Function

                        -   Caveolae have been asociated w/ cell signaling and a variety of transport processes (e.g. transcytosis and endocytosis)

            <!--list-separator-->

            -  [Lysosomes]({{< relref "lysosome" >}})

                <!--list-separator-->

                -  Structure

                    -   Dense membrane-bound organelles
                    -   Can be identified in sections of tissue by cytochemical staining for _acid phosphatase_
                    -   Have special membrane proteins and ~50 acid _hydrolases_ synthesized in the RER
                    -   _ATP-powered proton pumps_ maintain **acid pH < 5**

                <!--list-separator-->

                -  Formation

                    -   Sequestered material fuses with _late endosome_ -> enzymatic degradation begins

                    <!--list-separator-->

                    -  Early endosomes

                        -   Irregular vesicles found near cell periphery
                        -   Form part of pathway for _[receptor-mediated endocytosis]({{< relref "receptor_mediated_endocytosis" >}})_
                        -   **CURL**: Compartment for Uncoupling of Receptors and Ligands
                        -   Acidic interior (pH < 6) maintained by ATP-driven proton pumps
                            -   Acidity aids in uncoupling of receptors and ligands
                                -   Receptors return to plasma membrane
                                -   Ligands move to a late endosome

                    <!--list-separator-->

                    -  Late endosomes

                        -   **Play a key role in various lysosomal pathways**
                            -   Because of this, sometimes called "intermediate compartment"
                        -   pH < 5.5
                        -   Lie deep within the cell
                        -   Receive ligands via microtubular transport of vesicles from early endosomes
                        -   Contain both _lysosomal hydrolases_ and _lysosomal membrane proteins_
                            -   Formed in the [RER]({{< relref "rough_endoplasmic_reticulum" >}}) -> transported to Golgi complex -> delivered in separate vesicles to late endosomes
                            -   Once late endosomes receive these, they begin to degrade their ligands -> **officially classified ad lysosomes**

                    <!--list-separator-->

                    -  Types of lysosomes

                        -   Lysosomes are named after **the content of recognizable material**
                            -   Otherwise, just called "lysosome"

                        <!--list-separator-->

                        -  _Multivesicular bodies_

                            -   Formed by fusion of **early endosome containined endocytic vesicles** and a **late endosome**

                        <!--list-separator-->

                        -  _Phagolysosomes_

                            -   Formed by fusion of **phagocytic vacuole** and a **late endosome or lysosome**

                        <!--list-separator-->

                        -  _Autophagolysosomes_

                            -   Formed by fusion of **autophagic vacuole** and a **late endosome or lysosome**
                            -   Formed when cell components targeted for destruction become enveloped by smooth areas of membranes derived from RER

                        <!--list-separator-->

                        -  _Residual bodies_

                            -   Lysosomes **of any time** that have **expended their capacity to degrade material**
                            -   Are eventually excreted from the cell


### Unlinked references {#unlinked-references}

[Show unlinked references]
