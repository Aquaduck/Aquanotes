+++
title = "Joint"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work**

    <!--list-separator-->

    -  Basic articular [joint]({{< relref "joint" >}})

        {{< figure src="/ox-hugo/_20210930_154007screenshot.png" width="700" >}}

        -   In image:
            -   Sinovial cavity
            -   [Bursa]({{< relref "bursa" >}})
            -   Articular cartilage
            -   Epiphyseal bone
            -   Enthesis
            -   Ligament
            -   Joint capsule with synovial lining
            -   Tendon

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons > Ligaments**

    Stablize [joints]({{< relref "joint" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons**

    <!--list-separator-->

    -  [Joints]({{< relref "joint" >}})

        -   Most joints are lined by [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) on the surface of each articulating bone
            -   Contacting cartilage surfaces lack a [Perichondrium]({{< relref "perichondrium" >}}), but are lubricated by synovial cells at the side of the join
        -   Joints with limited movement consist of fibrocartilage (e.g. intervertebral disks and [symphysis pubis]({{< relref "symphysis_pubis" >}}))
        -   Some joints have fibrocartilage supplementing the [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) (e.g. menisci of knee, glenoid labra, ligamentum teres and acetabular labra of hip)


#### [Ligament]({{< relref "ligament" >}}) {#ligament--ligament-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Stabilize [Joint]({{< relref "joint" >}})s

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
