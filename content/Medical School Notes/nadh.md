+++
title = "NADH"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [Niacin]({{< relref "niacin" >}}) is a component


## Backlinks {#backlinks}


### 11 linked references {#11-linked-references}


#### [α-ketoglutarate dehydrogenase]({{< relref "α_ketoglutarate_dehydrogenase" >}}) {#α-ketoglutarate-dehydrogenase--α-ketoglutarate-dehydrogenase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Requires [NAD<sup>+</sup>]({{< relref "nadh" >}})

    ---


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links > Cofactors/Coenzymes**

    [NAD<sup>+</sup>]({{< relref "nadh" >}})

    ---

<!--list-separator-->

-  **🔖 Concept links**

    [Allosterically]({{< relref "allosteric_regulation" >}}) regulated by product inhibition by [acetyl-CoA]({{< relref "acetyl_coa" >}}) & [NADH]({{< relref "nadh" >}}) against their respective enzymes

    ---


#### [Niacin]({{< relref "niacin" >}}) {#niacin--niacin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Constituent of [NAD<sup>+</sup>]({{< relref "nadh" >}}), NADP<sup>​+</sup>

    ---


#### [Malate dehydrogenase]({{< relref "malate_dehydrogenase" >}}) {#malate-dehydrogenase--malate-dehydrogenase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Requires [NAD<sup>+</sup>]({{< relref "nadh" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    [Malate DH]({{< relref "malate_dehydrogenase" >}}) requires [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---

<!--list-separator-->

-  **🔖 Fatty acid oxidation**

    [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---

<!--list-separator-->

-  **🔖 TCA cycle**

    [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---

<!--list-separator-->

-  **🔖 Pyruvate DH complex**

    [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---

<!--list-separator-->

-  **🔖 Glycolysis**

    [NADH]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---


#### [Isocitrate dehydrogenase]({{< relref "isocitrate_dehydrogenase" >}}) {#isocitrate-dehydrogenase--isocitrate-dehydrogenase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Linked to [NAD<sup>+</sup>]({{< relref "nadh" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
