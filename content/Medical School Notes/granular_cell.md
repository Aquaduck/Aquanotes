+++
title = "Granular cell"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Differentiated smooth muscle cells in the walls of the [afferent arterioles]({{< relref "afferent_arteriole_of_the_kidney" >}})
-   Contain secretory vesules (the "granules") that contain [Renin]({{< relref "renin" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}}) {#explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result--explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Juxtaglomerular apparatus (p. 21)**

    <!--list-separator-->

    -  [Granular cells]({{< relref "granular_cell" >}})

        -   Differentiated smooth muscle cells in the walls of the [afferent arterioles]({{< relref "afferent_arteriole_of_the_kidney" >}})
            -   Can sense blood pressure
        -   Contain secretory granules -> contain [Renin]({{< relref "renin" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
