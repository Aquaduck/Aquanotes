+++
title = "List the four enzymes, and their intracellular location, for which pyruvate serves as a substrate, and the product that is made"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## Enzymes {#enzymes}


### Cytosol {#cytosol}

-   [Alanine aminotransferase]({{< relref "alanine_aminotransferase" >}})
    -   Makes alanine
-   [Lactate DH]({{< relref "lactate_dehydrogenase" >}})
    -   Makes lactate and NAD<sup>+</sup>


### Mitochondria {#mitochondria}

-   [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}})
    -   Makes [oxaloacetate]({{< relref "oxaloacetate" >}})
-   [Pyruvate DH complex]({{< relref "pyruvate_dehydrogenase_complex" >}})
    -   Makes acetyl-CoA


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the four enzymes, and their intracellular location, for which pyruvate serves as a substrate, and the product that is made]({{< relref "list_the_four_enzymes_and_their_intracellular_location_for_which_pyruvate_serves_as_a_substrate_and_the_product_that_is_made" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
