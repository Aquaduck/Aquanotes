+++
title = "Forehand, Action Potential"
author = ["Arif Ahsan"]
date = 2021-09-11T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Review the mechanisms that set up the resting membrane potential.]({{< relref "review_the_mechanisms_that_set_up_the_resting_membrane_potential" >}}) {#review-the-mechanisms-that-set-up-the-resting-membrane-potential-dot--review-the-mechanisms-that-set-up-the-resting-membrane-potential-dot-md}

<!--list-separator-->

-  From [Forehand, Action Potential]({{< relref "forehand_action_potential" >}})

    <!--list-separator-->

    -  A steady state potential develops when more than one ion is permeable

        -   Neurons have **negative resting potentials relative to extracellular space**
            -   More negative = _hyperpolarized_
            -   Less negative = _depolarized_
        -   Extracellular concentration of sodium is **much greater** than the intracellular concentration of sodium
            -   The **opposite** is true for potassium
        -   Membrane is **much more permeable** to **potassium over sodium**
            -   More leakage channels (non-gated) for potassium in the membrane
            -   Because of this, **resting membrane potential is much closer to the equilibrium potential for potassium**
        -   Resting membrane potential is quite sensitive to **extracellular potassium concentrations**
        -   Because sodium is far from its equilibrium potential, there is a very **large driving force on sodium** (i.e. sodium wants to move)
            -   Thus, sodium ions move readily whenever a voltage-gated or ligand-gated sodium channel opens in the membrane

    <!--list-separator-->

    -  Variables

        -   _E<sub>m</sub>_: membrane potenital
        -   _E<sub>Na</sub>_ and _E<sub>K</sub>_: equilibrium potentials defined by the [Nernst equation]({{< relref "nernst_equation" >}})


#### [Review the location and function of the common types of ion channels associated with neurons.]({{< relref "review_the_location_and_function_of_the_common_types_of_ion_channels_associated_with_neurons" >}}) {#review-the-location-and-function-of-the-common-types-of-ion-channels-associated-with-neurons-dot--review-the-location-and-function-of-the-common-types-of-ion-channels-associated-with-neurons-dot-md}

<!--list-separator-->

-  From [Forehand, Action Potential]({{< relref "forehand_action_potential" >}})

    <!--list-separator-->

    -  Non-gated ion channels

        <!--list-separator-->

        -  Function

            -   Always open
            -   Responsible for **the influx of Na<sup>+</sup> and K<sup>​+</sup>** when the neuron is in its **resting state**
                -   Important for **establishing resting membrane potential**

        <!--list-separator-->

        -  Location

            -   Found **throughout the neuron**

    <!--list-separator-->

    -  Ligand-gated ion channels

        <!--list-separator-->

        -  Function

            -   Directly or indirectly activated by the **binding of chemical neurotransmitters ot membrane receptors**
            -   The receptor itself can form part of the ion channel or it may be coupled to the channel via a G-protein and second messenger
            -   Chemical transmitters bind to receptors -> associated ion channel opens/closes to permit/block the movement of **specific ions** across the cell membrane

        <!--list-separator-->

        -  Location

            -   **Sites of synaptic contact**
                -   Dendritic spines
                -   Dendrites
                -   Somata

    <!--list-separator-->

    -  Mechanically-gated channels

        <!--list-separator-->

        -  Function

            -   Open when force (stretch) is applied across a membrane
                -   Close when force is removed

        <!--list-separator-->

        -  Location

            -   **Specialized sensory cells**
            -   **Peripheral branches** of **pseudounipolar sensory neurons**

    <!--list-separator-->

    -  Voltage-gated ion channels

        <!--list-separator-->

        -  Function

            -   Sensitive to voltage difference across the membrane
            -   **In initial resting state channels are typically closed**
                -   Open when a critical voltage level is reached
            -   Required for **initiation and propogation of action potentials** or **neurotransmitter release**

        <!--list-separator-->

        -  Location

            -   **Axons** and **axon terminals**


### Unlinked references {#unlinked-references}

[Show unlinked references]
