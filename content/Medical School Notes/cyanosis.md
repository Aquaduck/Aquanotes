+++
title = "Cyanosis"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   Blue coloring of tissues indicative of [hypoxia]({{< relref "hypoxia" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Shunt]({{< relref "shunt" >}}) {#shunt--shunt-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Results in [cyanosis]({{< relref "cyanosis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
