+++
title = "Bone-remodeling unit"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    The result of the [Bone-remodeling unit]({{< relref "bone_remodeling_unit" >}}) is known as an [Osteon]({{< relref "osteon" >}}) (haversian system) with a central [Haversian canal]({{< relref "haversian_canal" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    Remodeling of compact cortical bone is done by a structure called a [bone-remodeling unit]({{< relref "bone_remodeling_unit" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
