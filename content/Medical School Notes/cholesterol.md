+++
title = "Cholesterol"
author = ["Arif Ahsan"]
date = 2021-09-07T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Peroxisome]({{< relref "peroxisome" >}}) {#peroxisome--peroxisome-dot-md}

<!--list-separator-->

-  **🔖 From First Aid**

    Synthesis of [cholesterol]({{< relref "cholesterol" >}}), bile acids, and plasmalogens

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Atherosclerosis**

    Formation of [lipid]({{< relref "lipid" >}}), [cholesterol]({{< relref "cholesterol" >}}), and/or [calcium]({{< relref "calcium" >}})-laden plaques within [tunica intima]({{< relref "tunica_intima" >}}) of arterial wall -> restricts [blood]({{< relref "blood" >}}) flow

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
