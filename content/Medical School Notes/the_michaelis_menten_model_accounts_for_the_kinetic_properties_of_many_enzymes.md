+++
title = "The Michaelis-Menten Model Accounts for the Kinetic Properties of Many Enzymes"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

## [Overview]({{< relref "michaelis_menten_kinetics" >}}) {#overview--michaelis-menten-kinetics-dot-md}

-   \\(V\_{o}\\) is the rate of catalysis = number of moles of product formed per second


### The Significance of K<sub>M</sub> and V<sub>max</sub> Values {#the-significance-of-k-values}

-   K<sub>cat</sub> = _turnover number_ of an enzyme
    -   The number of substrate molecules converted into product by an enzyme molecule in a unit time when the enzyme is fully saturated with substrate
    -   Equal to the kinetic constant k<sub>2</sub>
    -   The maximal rate V<sub>max</sub> reveals the turnover number of an enzyme if the concentration of active sites [E]<sub>T</sub> is known
        -   \\(V\_{max} = K\_{2}[E]\_{t}\\) -> \\(k\_{2} = \frac{V\_{max}}{[E]\_T}\\)


### Kinetic Perfection in Enzymatic Catalysis: The k<sub>cat</sub>/K<sub>M</sub> Criterion {#kinetic-perfection-in-enzymatic-catalysis-the-k-criterion}

-   Under physiological conditions, the [S]/K<sub>M</sub> ratio is typically between _0.01_ and _1.0_
-   When [S] >> K<sub>M</sub> -> V<sub>o</sub> = k<sub>cat</sub>
-   When [S] << K<sub>M</sub>...
    -   Most active sites are unoccupied -> enzymatic rate is much less than k<sub>cat</sub>
    -   \\(V\_{o} = \frac{k\_{cat}}{K\_{M}}[E][S]\\) -> [E] nearly equal to [E]<sub>T</sub> -> \\(V\_{o} = \frac{k\_{cat}}{K\_{M}}[S][E]\_{T}\\)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 10. Define the kinetic parameters K<sub>M</sub>, k<sub>cat</sub>, V<sub>max</sub>, and catalytic efficiency (K<sub>cat</sub>/K<sub>M</sub>) and understand the relationships between them**

    [The Michaelis-Menten Model Accounts for the Kinetic Properties of Many Enzymes]({{< relref "the_michaelis_menten_model_accounts_for_the_kinetic_properties_of_many_enzymes" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
