+++
title = "Paracortex of the lymph node"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}}) {#describe-the-major-actions-occurring-in-these-regions--with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot----describe-the-major-actions-occurring-in-these-regions-with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT**

    <!--list-separator-->

    -  [Paracortex]({{< relref "paracortex_of_the_lymph_node" >}})

        -   [Dendritic cells]({{< relref "dendritic_cell" >}}) and antigen enter the node throug the afferent lymphatics
        -   [Lymphocytes]({{< relref "lymphocyte" >}}) enter through the [HEV]({{< relref "high_endothelial_venule" >}})
        -   These two cell types interact -> testing to see if any [T-cells]({{< relref "t_lymphocyte" >}}) recognize the peptide displayed by the [dendritic cells]({{< relref "dendritic_cell" >}})
            -   If the T-cell **fails to recognize** any antigen peptide -> enters a sinus and exits through the efferent lymphatic -> returns to blood
                -   A lymphocyte may complete this cycle as often as 10 times per day in nodes throughout the body
            -   If a T-cell **recognizes** a peptide while in the paracortex -> enters activation stage -> divides about 10 times -> becomes an effector cell
                -   Specific function depends on the type of T-cell and signals from the innate system


### Unlinked references {#unlinked-references}

[Show unlinked references]
