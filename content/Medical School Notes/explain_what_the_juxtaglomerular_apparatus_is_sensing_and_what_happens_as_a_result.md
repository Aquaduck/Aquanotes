+++
title = "Explain what the juxtaglomerular apparatus is sensing and what happens as a result"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}}) {#from-vander-s-renal-physiology--vander-s-renal-physiology-dot-md}


### [Juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}}) (p. 21) {#juxtaglomerular-apparatus--juxtaglomerular-apparatus-dot-md----p-dot-21}

-   Composed of three cell types


#### [Granular cells]({{< relref "granular_cell" >}}) {#granular-cells--granular-cell-dot-md}

-   Differentiated smooth muscle cells in the walls of the [afferent arterioles]({{< relref "afferent_arteriole_of_the_kidney" >}})
    -   Can sense blood pressure
-   Contain secretory granules -> contain [Renin]({{< relref "renin" >}})


#### Extraglomerular [mesangial cells]({{< relref "mesangial_cell" >}}) {#extraglomerular-mesangial-cells--mesangial-cell-dot-md}

-   Morphologically similar to + continuous with the glomerular mesangial cells, **but lie outside the Bowman's capsule**


#### [Macula densa]({{< relref "macula_densa" >}}) cells {#macula-densa--macula-densa-dot-md--cells}

-   Detectors of flow rate and composition of fluid in nephron at th every end of the [thick ascending limb]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}})
-   Contribute to the control of [glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}) and to the control of [renin]({{< relref "renin" >}}) secretion


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > Renal Physiology: Glomerular Filtration > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
