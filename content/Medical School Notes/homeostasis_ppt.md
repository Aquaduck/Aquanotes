+++
title = "Homeostasis PPT"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Discuss what happens to balance when input ALONE is increased or decreased and provide an example.]({{< relref "discuss_what_happens_to_balance_when_input_alone_is_increased_or_decreased_and_provide_an_example" >}}) {#discuss-what-happens-to-balance-when-input-alone-is-increased-or-decreased-and-provide-an-example-dot--discuss-what-happens-to-balance-when-input-alone-is-increased-or-decreased-and-provide-an-example-dot-md}

<!--list-separator-->

-  From [Homeostasis PPT]({{< relref "homeostasis_ppt" >}})

    <!--list-separator-->

    -  Flow Equation

        -   Flow = \\( \frac{(P\_{1} - P\_{2})}{R} \\) where:
            -   Pressure gradient = \\( P\_{1} - P\_{2}\\)
            -   Resistance = \\( R \\)

    <!--list-separator-->

    -  What happens when input is changed

        -   The pool **does not keep filling**
        -   The pressure gradient will keep increasing/decreasing until a new steady state (deeper pool) is reached (i.e. input and output are equal again)
            -   This can lead to the "wrong" steady state

    <!--list-separator-->

    -  What happens when output is changed

        -   Same thing as above


#### [Discuss the concept of steady state and the balancing of input and outputs that determines the level of some substance such as blood sugar.]({{< relref "discuss_the_concept_of_steady_state_and_the_balancing_of_input_and_outputs_that_determines_the_level_of_some_substance_such_as_blood_sugar" >}}) {#discuss-the-concept-of-steady-state-and-the-balancing-of-input-and-outputs-that-determines-the-level-of-some-substance-such-as-blood-sugar-dot--discuss-the-concept-of-steady-state-and-the-balancing-of-input-and-outputs-that-determines-the-level-of-some-substance-such-as-blood-sugar-dot-md}

<!--list-separator-->

-  From [Homeostasis PPT]({{< relref "homeostasis_ppt" >}})

    <!--list-separator-->

    -  Steady state

        -   _Steady state_ = **Input equals output**
        -   [Homeostasis]({{< relref "homeostasis" >}}) means the maintenance of **the right steady state** in the face of changing inputs and outputs

    <!--list-separator-->

    -  Balance is everything

        -   Nothing is ever completely on or off
        -   Glucose is an example of the steady state issue
            -   Glucose is constantly being added and removed from the blood
            -   Poorly controlled [Diabetes mellitus]({{< relref "diabetes_mellitus" >}}) illustrates **the wrong Steady State**

        {{< figure src="/ox-hugo/_20211009_143608screenshot.png" width="700" >}}

        -   Examples of **too much input**
            -   Diabetes mellitus
            -   High sodium diet
            -   Overeating
        -   Examples of **"Hair in the drain"**
            -   Smoking
            -   Kidney disease
            -   Type II Diabetes Mellitus


#### [Describe the history and meaning of the term Homeostasis.]({{< relref "describe_the_history_and_meaning_of_the_term_homeostasis" >}}) {#describe-the-history-and-meaning-of-the-term-homeostasis-dot--describe-the-history-and-meaning-of-the-term-homeostasis-dot-md}

<!--list-separator-->

-  From [Homeostasis PPT]({{< relref "homeostasis_ppt" >}})

    <!--list-separator-->

    -  Claude Bernard

        -   _Claude Bernard_ (the father of Physiology) explained that **life depended on maintaining the correct internal environment**
            -   He called this _The Goldilocks condition_
        -   _Milieu interieur_: "internal environment" - the compartment between cells and tissues that arose with the development of multicellular life forms

    <!--list-separator-->

    -  Walter Cannon

        -   Actually invented the term [Homeostasis]({{< relref "homeostasis" >}})
            -   Referred to **the problems of maintaining the Goldilocks condition in the face of external challenges**


### Unlinked references {#unlinked-references}

[Show unlinked references]
