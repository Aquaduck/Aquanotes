+++
title = "Glomerular filtration barrier"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Podocyte]({{< relref "podocyte" >}}) {#podocyte--podocyte-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Third layer of the [Glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}})

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Glomerular filtration (p. 33)**

    Surfaces of all three layers of the [Glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) contain fixed polyanions -> **repel negatively charged macromolecules during filtration**

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Glomerular filtration (p. 33)**

    Selectivity of the [Glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) depends on **molecular size** and **electrical charge**

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Glomerular filtration (p. 33)**

    Filtered fluid must pass through a 3-layered [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}})

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    Very similar to blood plasma, but **contains little protein because large plasma proteins are virtually excluded form moving through the [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}})**

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21)**

    _[Filtration]({{< relref "filtration" >}})_: the process by which [Water]({{< relref "water" >}}) and solutes in the [Blood]({{< relref "blood" >}}) leave the vascular system through the [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) and enter [Bowman's space]({{< relref "bowman_s_space" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
