+++
title = "Titin"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Discuss the role of titin as a component of mechano-chemical signal transduction in skeletal muscle.]({{< relref "discuss_the_role_of_titin_as_a_component_of_mechano_chemical_signal_transduction_in_skeletal_muscle" >}}) {#discuss-the-role-of-titin-as-a-component-of-mechano-chemical-signal-transduction-in-skeletal-muscle-dot--discuss-the-role-of-titin-as-a-component-of-mechano-chemical-signal-transduction-in-skeletal-muscle-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  [Titin]({{< relref "titin" >}})

        -   An enormous (ca 3.5MDa) protein located in the **contractile unit of skeletal and cardiac muscle** - the _sarcomere_
        -   Spring-like molecule
        -   Ideally positioned to sense mechanical load
        -   **Has ser/thr kinase within its structure** - [Titin kinase]({{< relref "titin_kinase" >}})
        -   **Main takeaway**: titin and its protein kinase is critical to mechano-chemical signaling in striated muscle

        <!--list-separator-->

        -  Mechanism

            -   Mechanical load triggers Titin Kinase -> signal cascade activation
                -   Protein degradation
                -   Ubiquitin pathway
                -   Cell signaling in the nucleus through [Serum Response Factor]({{< relref "serum_response_factor" >}}) (SRF)


#### [Discuss the concept of mechanochemical signal transduction in biology.]({{< relref "discuss_the_concept_of_mechanochemical_signal_transduction_in_biology" >}}) {#discuss-the-concept-of-mechanochemical-signal-transduction-in-biology-dot--discuss-the-concept-of-mechanochemical-signal-transduction-in-biology-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Mechano-chemical signal transduction > Exercise**

    Believed that [titin]({{< relref "titin" >}}) is an important component of this pathway

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
