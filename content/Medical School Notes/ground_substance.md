+++
title = "Ground substance"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the types of cellular and acellular components of connective tissue]({{< relref "name_the_types_of_cellular_and_acellular_components_of_connective_tissue" >}}) {#name-the-types-of-cellular-and-acellular-components-of-connective-tissue--name-the-types-of-cellular-and-acellular-components-of-connective-tissue-dot-md}

<!--list-separator-->

-  **🔖 From Introduction to Connective Tissue (CT) Pre-workshop Reading > Four major components that make up connective tissue**

    <!--list-separator-->

    -  [Ground substance]({{< relref "ground_substance" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
