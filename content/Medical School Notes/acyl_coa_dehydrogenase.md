+++
title = "Acyl-CoA dehydrogenase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism**

    <!--list-separator-->

    -  FAD<sup>+</sup>-short-, medium- and long-chain [acyl-CoA dehydrogenases]({{< relref "acyl_coa_dehydrogenase" >}}) (ACADs)

        -   **Catalyze the initial step in each cycle of fatty acid β-oxidation**
            -   Results in the introduction of a _trans_ double-bond between C2 (αC) and C3 (βC) of the acyl-CoA substrate
        -   All types of ACADs are mechanistically similar and **require FAD as a coenzyme**


### Unlinked references {#unlinked-references}

[Show unlinked references]
