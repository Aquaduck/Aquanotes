+++
title = "Overview of the most important glucose transporters"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "source", "amboss"]
draft = false
+++

| Name                             | Site                         | Special function                                                                                                                        | Insulin-dependent? |
|----------------------------------|------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|--------------------|
| [GLUT1]({{< relref "glut_1" >}}) | Most human cells             | Blood-brain barrier                                                                                                                     | No                 |
|                                  | RBCs                         | High affinity for glucose                                                                                                               |                    |
|                                  | CNS                          |                                                                                                                                         |                    |
|                                  | Cornea                       |                                                                                                                                         |                    |
|                                  | Placenta                     |                                                                                                                                         |                    |
|                                  | Fetal tissue                 |                                                                                                                                         |                    |
| [GLUT2]({{< relref "glut_2" >}}) | Hepatocytes                  | Transports all monosaccharides from the basolateral membrane of enterocytes into blood                                                  | No                 |
|                                  | Pancreatic β cells           | Glucose sensor                                                                                                                          |                    |
|                                  | Kidneys                      | High-capacity but low affinity for glucose (i.e. glucose only diffuses at **high concentrations**)                                      |                    |
|                                  | Small intestine              | Bidirectional transporter: allows hepatocytes to uptake glucose for glycolysis **and** release glucose during gluconeogenesis           |                    |
| [GLUT3]({{< relref "glut_3" >}}) | Most human cells             | Blood-brain barrier                                                                                                                     | No                 |
|                                  | CNS                          | High affinity for glucose (i.e. glucose diffuses at **low concentrations**)                                                             |                    |
|                                  | Placenta                     |                                                                                                                                         |                    |
| [GLUT4]({{< relref "glut_4" >}}) | Adipose tissue               | Plays a key role in regulating body glucose homeostasis                                                                                 | Yes                |
|                                  | _Striated muscle_            | Insulin stimulates incorporation of GLUT4 (stored in vesicles) into plasma membranes of cells for controlled glucose uptake and storage |                    |
|                                  | - Skeletal muscle            | Physical exercise also induces translocation of GLUT4 into plasma membrane of skeletal muscle in an insulin-independent manner          |                    |
|                                  | - Heart muscle               |                                                                                                                                         |                    |
| [GLUT5]({{< relref "glut_5" >}}) | Small intestinal enterocytes | Fructose transporter                                                                                                                    | No                 |
|                                  | Spermatocytes                |                                                                                                                                         |                    |
|                                  | Kidneys                      |                                                                                                                                         |                    |

_Only GLUT4 has a need 4 insulin_


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Distinguish the insulin-responsive glucose membrane transporter and in what tissues it is located, from that used by:]({{< relref "distinguish_the_insulin_responsive_glucose_membrane_transporter_and_in_what_tissues_it_is_located_from_that_used_by" >}}) {#distinguish-the-insulin-responsive-glucose-membrane-transporter-and-in-what-tissues-it-is-located-from-that-used-by--distinguish-the-insulin-responsive-glucose-membrane-transporter-and-in-what-tissues-it-is-located-from-that-used-by-dot-md}

[Overview of the most important glucose transporters]({{< relref "overview_of_the_most_important_glucose_transporters" >}})
The insulin-responsive glucose membrane transporter is [GLUT-4]({{< relref "glut_4" >}}) and is found in:

---


### Unlinked references {#unlinked-references}

[Show unlinked references]
