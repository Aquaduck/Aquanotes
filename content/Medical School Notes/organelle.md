+++
title = "Organelle"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   Components of the [cell]({{< relref "cell" >}})


## Backlinks {#backlinks}


### 13 linked references {#13-linked-references}


#### [Smooth endoplasmic reticulum]({{< relref "smooth_endoplasmic_reticulum" >}}) {#smooth-endoplasmic-reticulum--smooth-endoplasmic-reticulum-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    An [organelle]({{< relref "organelle" >}})

    ---


#### [Rough endoplasmic reticulum]({{< relref "rough_endoplasmic_reticulum" >}}) {#rough-endoplasmic-reticulum--rough-endoplasmic-reticulum-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    An [organelle]({{< relref "organelle" >}})

    ---


#### [Ribosome]({{< relref "ribosome" >}}) {#ribosome--ribosome-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    An [organelle]({{< relref "organelle" >}})

    ---


#### [Plasma membrane]({{< relref "plasma_membrane" >}}) {#plasma-membrane--plasma-membrane-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    An [organelle]({{< relref "organelle" >}})

    ---


#### [Peroxisome]({{< relref "peroxisome" >}}) {#peroxisome--peroxisome-dot-md}

<!--list-separator-->

-  **🔖 From First Aid**

    Membrane-enclosed [organelle]({{< relref "organelle" >}}) involved in:

    ---


#### [Mitochondrion]({{< relref "mitochondrion" >}}) {#mitochondrion--mitochondrion-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    An [organelle]({{< relref "organelle" >}})

    ---


#### [Lysosome]({{< relref "lysosome" >}}) {#lysosome--lysosome-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    An [organelle]({{< relref "organelle" >}})

    ---


#### [Golgi apparatus]({{< relref "golgi_apparatus" >}}) {#golgi-apparatus--golgi-apparatus-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    An [organelle]({{< relref "organelle" >}})

    ---


#### [Endoplasmic reticulum]({{< relref "endoplasmic_reticulum" >}}) {#endoplasmic-reticulum--endoplasmic-reticulum-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An [organelle]({{< relref "organelle" >}})

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components**

    <!--list-separator-->

    -  [Organelles]({{< relref "organelle" >}})

        <!--list-separator-->

        -  [Plasma membrane]({{< relref "plasma_membrane" >}})

            -   Envelops cell and forms boundary between it and adjacent structures

        <!--list-separator-->

        -  [Ribosomes]({{< relref "ribosome" >}})

            <!--list-separator-->

            -  Structure

                -   Consist of _small_ and _large_ subunit
                    -   Composed of several types of [rRNA]({{< relref "ribosomal_rna" >}}) and [proteins]({{< relref "protein" >}})
                -   Ribosomes may be **free in the cytosol** or **bound to membranes of the RER or outer nuclear membrane**
                -   _Polyribosome (polysome)_: a cluster of ribosomes along a single strand of [mRNA]({{< relref "messenger_rna" >}}) engaged w/ the synthesis of [protein]({{< relref "protein" >}})

            <!--list-separator-->

            -  Function

                -   **site where mRNA is translated into protein**
                    -   Proteins destined for transport -> synthesized in RER
                    -   Proteins not destined for transport -> synthesized in cytosol
                -   **In translation**:
                    1.  _Small ribosomal unit_ binds [mRNA]({{< relref "messenger_rna" >}}) and activated [tRNAs]({{< relref "transfer_rna" >}}) -> codons of mRNA base-pair with corresponding anticodons of tRNAs
                    2.  Initiator tRNA recognizes the start codon (AUG) on the mRNA
                    3.  _Large ribosomal subunit_ binds to complex
                        -   _Peptidyl transferase_ in the large subunit catalyzes peptide bond formation -> addition of AAs
                    4.  Stop codon (UAA, UAG, UGA) causes release of polypeptide from ribosome -> ribosomal subunits dissociate from mRNA

        <!--list-separator-->

        -  [Rough endoplasmic reticulum]({{< relref "rough_endoplasmic_reticulum" >}}) (RER)

            <!--list-separator-->

            -  Structure

                -   System of membrane-bound sacs/cavities
                -   Outer surface studded with ribosomes (which is what "rough" means)
                -   _Cisterna or lumen_: interior region
                -   Outer nuclear membrane **continuous** with RER membrane
                -   _Ribophorins_: receptors where large [ribosomal]({{< relref "ribosome" >}}) subunits bind
                -   Abundant in cells synthesizing **secretory proteins**
                    -   In these cells, RER organized into many parallel arrays
                -   RER sac closest to the Golgi apparatus makes buds w/o ribosomes -> form vesicles
                    -   This is considered a **transitional element**

            <!--list-separator-->

            -  Function

                -   Location where **membrane-packaged [proteins]({{< relref "protein" >}})** are synthesized
                    -   Secretory proteins
                    -   Plasma membrane proteins
                    -   Lysosomal proteins
                -   Monitors the assembly, retention, and degradation of certain proteins

        <!--list-separator-->

        -  [Smooth endoplasmic reticulum]({{< relref "smooth_endoplasmic_reticulum" >}}) (SER)

            <!--list-separator-->

            -  Structure

                -   Irregular network of membrane-bound channels that **lacks ribosomes on its surface** (why its called "smooth")
                -   Usually appears as branching, cross-connected (_anastomosing_) _tubules_ or _vesicles_ whose membranes **do not contain ribophorins**
                -   Less common than RER
                -   Prominent in cells synthesizing steroids, triglycerides, and cholesterol

            <!--list-separator-->

            -  Function

                -   **Steroid hormone synthesis** occurs in SER-rich cells such as Leydig cells (make testosterone)
                -   **Drug detoxification** occurs in hepatocytes
                    -   Follows proliferation of the SER in response to the drug _phenobarbital_
                        -   The oxidases that metabolize this drug are located in the SER
                -   **Muscle contraction and relaxation** involve the release and recapture of Ca<sup>2+</sup> by the SER in **skeletal muscle cells**
                    -   These are called the _sarcoplasmic reticulum_

        <!--list-separator-->

        -  [Annulate lamellae]({{< relref "annulate_lamellae" >}})

            <!--list-separator-->

            -  Structure

                -   Parallel stacks of membranes (usually 6 to 10) that resemble the nuclear envelope (including its pore complexes)
                -   Often arraged with their _annuli_ (pores) in register and are **frequently continuous with the RER**

            <!--list-separator-->

            -  Function

                -   Found in rapidly growing cells
                    -   Germ cells
                    -   Embryonic cells
                    -   Tumor cells
                -   Function and significance remain unknown

        <!--list-separator-->

        -  [Mitochondria]({{< relref "mitochondrion" >}})

            <!--list-separator-->

            -  Structure

                -   Rod-shaped
                -   Have an outer membrane and inner membrane
                -   Subdivided into an _intermembrane space_ between the two membranes and an inner _matrix_
                -   Granules w/in the matrix bind the divalent cations Mg<sup>2+</sup> and Ca<sup>2+</sup>

            <!--list-separator-->

            -  Enzymes and genetic apparatus

                -   Mitochondria contain the following:
                    1.  All of the enzymes of the [TCA]({{< relref "citric_acid_cycle" >}}) cycle in the matrix (except for succinate DH which is in the inner membrane)
                    2.  _ATP synthase_
                        -   Consists of head portion and a transmembrane H ^{+} carrier
                        -   Involved in [coupling oxidation to phosphorylation]({{< relref "oxidative_phosphorylation" >}}) of ADP to ATP
                    3.  Genetic apparatus in the matrix containing:
                        -   circular DNA
                        -   mRNA
                        -   tRNA
                        -   rRNA (w/ limited coding capacity)
                        -   **Most mitochondrial proteins are encoded by nuclear DNA**

            <!--list-separator-->

            -  Origin and proliferation

                -   Mitochondria may have originated as _symbionts_ (intracellular parasites)
                    -   Anaerobic eukaryotic cells endocytosed aerobic microorganisms that evolved into mitochondria, which function in oxidative processes
                -   Mitchondria proliferate by _fission_ (division) of preexisting mitochondria
                    -   Typically have a 10-day life span
                    -   Proteins needed to sustain mitochondria are imported into them from the cytosol

            <!--list-separator-->

            -  Mitochondrial ATP synthesis

                -   Mitochondria synthesize ATP via the Krebs cycle
                -   ATP also synthesized via [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}})

            <!--list-separator-->

            -  Condensed mitochondria

                -   Result from a conformational change in the typical form in response to **uncoupling of oxidation from phosphorylation**
                -   Size of **matrix decreased** -> matrix made **more dense**
                -   Size of **intermembrane space enlarged**
                -   Present in _brown fat cells_
                    -   Produce heat rather than ATP
                        -   Special transport protein in inner membrane uncouples respiration from ATP synthesis
                -   Mitochondria swell in response to calcium, phosphate, and thyroxine -> induce increase in water uptake and uncoupling of phosphorylation
                    -   [ATP]({{< relref "atp" >}}) reverses the swelling

        <!--list-separator-->

        -  [Golgi apparatus]({{< relref "golgi_apparatus" >}}) (complex)

            <!--list-separator-->

            -  Structure

                -   Consists of several membrane-bounded _cisternae_ arranged in a stack
                    -   _Cisternae_: disk-shaped and slightly curved, w/ flat centers and dilated rims
                        -   Size and shape vary
                        -   **Integral to packaging and modification processes that occur in the Golgi apparatus**
                    -   Positioned and held in place by microtubules
                -   A distinct polarity exists across Golgi stack w/ many vesicles on one side and vacuoles on the other

            <!--list-separator-->

            -  Regions

                1.  **Cis face** lies deep in cell toward nucleus next to RER
                    -   Outermost cisterna associated w/ _vesicular-tubular clusters (VTC)_
                        -   Network of interconnected tubes and vesicles
                2.  **Medial compartment** composed of several cisternae lying between the cis and trans faces
                3.  **Trans face** lies at the side of the stack facing the plasma membrane
                    -   Associated w/ vacuoles and secretory granules
                4.  **trans-Golgi network** lies apart from the last cisterna at the trans face
                    -   separated from the Golgi stack
                    -   Sorts proteins for ther final destinations

            <!--list-separator-->

            -  Functions

                -   Processes membrane-packed proteins synthesized in the RER and also recycles and redistributes membranes

        <!--list-separator-->

        -  [Coated vesicles]({{< relref "coated_vesicle" >}})

            <!--list-separator-->

            -  Clathrin-coated vesicles

                <!--list-separator-->

                -  Structure

                    -   _[Clathrin]({{< relref "clathrin" >}})_: consists of three large and three small polypeptine chains that form a three-legged structure (_triskelion_)
                        -   36 of these associate to form a polyhedral cage-like lattice around the vesicle
                    -   _Adaptins_: proteins part of the structure that:
                        -   Recognize and recruit clathrin coat
                        -   Capture cargo receptors containing specific molecules
                        -   Help establish the vesicle curvature
                    -   _Dynamin_: forms a ring around the neck of a budding vesicle/pit and aids in pinching it off the parent membrane
                        -   GTP-binding protein

                <!--list-separator-->

                -  Function

                    -   Mediate the continuous _constitutive protein transport_ within the cell
                        -   Specific GTP-binding proteins present at each step of vesicle budding and fusion
                        -   _[SNARE]({{< relref "snare" >}})_: proteins that ensure vesicles dock and fuse only with its correct target membrane
                    -   Transport proteins from RER -> VTC -> cis Golgi -> across cisternae -> TGN (anterograde transport)
                        -   [COP-II]({{< relref "cop_ii" >}}) transports molecules forward in path mentioned above
                        -   [COP-I]({{< relref "cop_i" >}}) facilitates **retrograde transport** - path above but **in reverse**

            <!--list-separator-->

            -  Caveolin-coated vesicles

                <!--list-separator-->

                -  Structure

                    -   Invaginations of the plasma membrane **in endothelial and smooth muscle cells**
                    -   Possess a distinct coat formed by the protein _caveolin_

                <!--list-separator-->

                -  Function

                    -   Caveolae have been asociated w/ cell signaling and a variety of transport processes (e.g. transcytosis and endocytosis)

        <!--list-separator-->

        -  [Lysosomes]({{< relref "lysosome" >}})

            <!--list-separator-->

            -  Structure

                -   Dense membrane-bound organelles
                -   Can be identified in sections of tissue by cytochemical staining for _acid phosphatase_
                -   Have special membrane proteins and ~50 acid _hydrolases_ synthesized in the RER
                -   _ATP-powered proton pumps_ maintain **acid pH < 5**

            <!--list-separator-->

            -  Formation

                -   Sequestered material fuses with _late endosome_ -> enzymatic degradation begins

                <!--list-separator-->

                -  Early endosomes

                    -   Irregular vesicles found near cell periphery
                    -   Form part of pathway for _[receptor-mediated endocytosis]({{< relref "receptor_mediated_endocytosis" >}})_
                    -   **CURL**: Compartment for Uncoupling of Receptors and Ligands
                    -   Acidic interior (pH < 6) maintained by ATP-driven proton pumps
                        -   Acidity aids in uncoupling of receptors and ligands
                            -   Receptors return to plasma membrane
                            -   Ligands move to a late endosome

                <!--list-separator-->

                -  Late endosomes

                    -   **Play a key role in various lysosomal pathways**
                        -   Because of this, sometimes called "intermediate compartment"
                    -   pH < 5.5
                    -   Lie deep within the cell
                    -   Receive ligands via microtubular transport of vesicles from early endosomes
                    -   Contain both _lysosomal hydrolases_ and _lysosomal membrane proteins_
                        -   Formed in the [RER]({{< relref "rough_endoplasmic_reticulum" >}}) -> transported to Golgi complex -> delivered in separate vesicles to late endosomes
                        -   Once late endosomes receive these, they begin to degrade their ligands -> **officially classified ad lysosomes**

                <!--list-separator-->

                -  Types of lysosomes

                    -   Lysosomes are named after **the content of recognizable material**
                        -   Otherwise, just called "lysosome"

                    <!--list-separator-->

                    -  _Multivesicular bodies_

                        -   Formed by fusion of **early endosome containined endocytic vesicles** and a **late endosome**

                    <!--list-separator-->

                    -  _Phagolysosomes_

                        -   Formed by fusion of **phagocytic vacuole** and a **late endosome or lysosome**

                    <!--list-separator-->

                    -  _Autophagolysosomes_

                        -   Formed by fusion of **autophagic vacuole** and a **late endosome or lysosome**
                        -   Formed when cell components targeted for destruction become enveloped by smooth areas of membranes derived from RER

                    <!--list-separator-->

                    -  _Residual bodies_

                        -   Lysosomes **of any time** that have **expended their capacity to degrade material**
                        -   Are eventually excreted from the cell


#### [Cytoplasm]({{< relref "cytoplasm" >}}) {#cytoplasm--cytoplasm-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The cytosol component between [organelles]({{< relref "organelle" >}})

    ---


#### [Coated vesicle]({{< relref "coated_vesicle" >}}) {#coated-vesicle--coated-vesicle-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    An [organelle]({{< relref "organelle" >}})

    ---


#### [Annulate lamellae]({{< relref "annulate_lamellae" >}}) {#annulate-lamellae--annulate-lamellae-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    An [organelle]({{< relref "organelle" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
