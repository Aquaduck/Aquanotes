+++
title = "Reactive Oxygen Species"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [ROS and the DNA damage response in cancer]({{< relref "ros_and_the_dna_damage_response_in_cancer" >}}) {#ros-and-the-dna-damage-response-in-cancer--ros-and-the-dna-damage-response-in-cancer-dot-md}

<!--list-separator-->

-  [Reactive Oxygen Species]({{< relref "reactive_oxygen_species" >}}) (ROS)

    -   A family of short-lived molecules that exist as _free radicals_


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 HIF-1 target genes involved in glucose and energy metabolism**

    HIF-1 appears to play a crucial homeostatic role in **managing [oxygen]({{< relref "oxygen" >}}) consumption** to balance [ATP]({{< relref "atp" >}}) and [ROS]({{< relref "reactive_oxygen_species" >}}) production

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
