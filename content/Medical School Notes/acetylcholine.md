+++
title = "Acetylcholine"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Describe the process of neuromuscular transmission.]({{< relref "describe_the_process_of_neuromuscular_transmission" >}}) {#describe-the-process-of-neuromuscular-transmission-dot--describe-the-process-of-neuromuscular-transmission-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Conduction of a nerve impulse across a myoneural junction (p. 116)**

    Rise of [cytosolic calcium] -> **release of [acetylcholine]({{< relref "acetylcholine" >}}) into synaptic cleft**

    ---


#### [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}}) {#describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot--describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Regulation of the mobilization of intramuscular fat > Role of skeletal muscle contraction**

    Nerve impulse via [acetylcholine]({{< relref "acetylcholine" >}}) to acetylcholine receptor

    ---


#### [Describe and give an example of receptors as ion channels.]({{< relref "describe_and_give_an_example_of_receptors_as_ion_channels" >}}) {#describe-and-give-an-example-of-receptors-as-ion-channels-dot--describe-and-give-an-example-of-receptors-as-ion-channels-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  Synapses between neurons - [acetylcholine]({{< relref "acetylcholine" >}})

        -   Major neurotransmitter in the CNS and ANS
        -   Major role is to **open sodium channels in the membrane -> depolarization**
        -   Motor neurons cause muscle contraction via th eopening of acetylcholine receptors at the neuromuscular junction

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Receptors as Ion Channels**

    The prototypic example is the **[acetylcholine]({{< relref "acetylcholine" >}}) (ACh) receptor**

    ---


#### [Acetylcholinesterase]({{< relref "acetylcholinesterase" >}}) {#acetylcholinesterase--acetylcholinesterase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Breaks down [acetylcholine]({{< relref "acetylcholine" >}}) into acetic acid and choline

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
