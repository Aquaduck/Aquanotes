+++
title = "Khan - The genetic code"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "khan", "source"]
draft = false
+++

[Link](https://www.khanacademy.org/science/high-school-biology/hs-molecular-genetics/hs-rna-and-protein-synthesis/a/the-genetic-code)


## Overview: Gene expression and the genetic code {#overview-gene-expression-and-the-genetic-code}

-   Two step process
    1.  _[Transcription]({{< relref "osmosis_transcription_translation_and_replication" >}})_
    2.  _[Translation]({{< relref "osmosis_transcription_translation_and_replication" >}})_


## <span class="org-todo todo TODO">TODO</span> Properties of the genetic code {#properties-of-the-genetic-code}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
