+++
title = "Stratum basale"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The lowest layer of the [epidermis]({{< relref "epidermis" >}}) where cell division occurs


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Understand how the epidermis grows/regenerates.]({{< relref "understand_how_the_epidermis_grows_regenerates" >}}) {#understand-how-the-epidermis-grows-regenerates-dot--understand-how-the-epidermis-grows-regenerates-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Growth/regeneration**

    The [basal layer]({{< relref "stratum_basale" >}}) (stratum basilis or stratum germinativum) is **where cell division occurs**

    ---


#### [Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}}) {#name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot--name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Epidermis - strata**

    <!--list-separator-->

    -  [Stratum basale]({{< relref "stratum_basale" >}})

        -   Single layer of **cuboidal cells** attached to the basement membrane
        -   New cells are generated from this layer (stem cells)


### Unlinked references {#unlinked-references}

[Show unlinked references]
