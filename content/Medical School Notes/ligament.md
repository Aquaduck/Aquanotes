+++
title = "Ligament"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Stabilize [Joint]({{< relref "joint" >}})s


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons > Tendons**

    At the site of tendinous and [ligamentous]({{< relref "ligament" >}}) insertions there are collagen fibers extending perpendicularly into the bone - [Sharpey's fibers]({{< relref "sharpey_s_fibers" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons**

    <!--list-separator-->

    -  [Ligaments]({{< relref "ligament" >}})

        -   Stablize [joints]({{< relref "joint" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
