+++
title = "Kaplan Biochemistry Chapter 2"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
tags = ["medschool", "kaplan", "source"]
draft = false
+++

## 2.1 Enzymes as Biological Catalysts {#2-dot-1-enzymes-as-biological-catalysts}

-   [Enzymes]({{< relref "enzyme" >}}) are important as _catalysts_
    -   _Catalysts_ do not impact the thermodynamics of a biological reaction
    -   They help the reaction proceed at a much faster rate
-   Enzymes that act as catalysts **are not changed**


### Enzyme classifications {#enzyme-classifications}

-   _Substrate_: molecule upon which an enzyme acts
-   _Enzyme specificity_: property in which enzymes only catalyze only a single reaction or class of reactions with a substrate


#### 6 categories of [enzymes]({{< relref "enzyme" >}}) mechanisms {#6-categories-of-enzymes--enzyme-dot-md--mechanisms}

<!--list-separator-->

-  Oxidoreductases

    -   Catalyze _oxidation-reduction_ reactions - the transfer of electrions between biological molecules
    -   Often have a cofactor that acts as an electron carrier
        -   e.g NAD+ or NADP+
    -   _Reductant_: electron donor
    -   _Oxidant_: electron acceptor
    -   Enzymes with _dehydrogenase_ or _reductase_ in their names are usually oxidoreductases

<!--list-separator-->

-  Transferases

    -   Catalyze movement of a functional group from one molecule to another
    -   _Kinase_: transferase that moves phosphate

<!--list-separator-->

-  Hydrolases

    -   Catalyze the breaking of a compound into two molecules using the addition of **water**
    -   Most are named only for their substrate
        -   e.g. phosphatase

<!--list-separator-->

-  Lyases

    -   Catalyze cleavage of a single molecule into two products
    -   **Do not require water** and **do not act as oxidoreductases**
    -   Synthesis of two molecules may also be catalyzed by a lyase
        -   If so, referred to as _synthases_

<!--list-separator-->

-  Isomerases

    -   Catalyze the rearrangement of bonds within a molecule
    -   Some can also be classified as _oxidoreductases_, _transferases_, or _lyases_
    -   Work on both stereoisomers and constitutional [isomers]({{< relref "isomers" >}})

<!--list-separator-->

-  Ligases

    -   Catalyze **addition or synthesis reactions**
        -   Generally between large similar molecules
        -   Often require ATP
    -   Synthesis reactions of smaller molecules are generally accomplished by _lyases_


## 2.2 Mechanisms of Enzyme Activity {#2-dot-2-mechanisms-of-enzyme-activity}


### Enzyme-substrate binding {#enzyme-substrate-binding}

-   _Enzyme-substrate complex_: formed by physical interaction between enzyme and substrate
-   _Active site_: location within enzyme where substrate is held during chemical reaction
    ![](/ox-hugo/_20210713_151258screenshot.png)


#### Lock and Key Theory vs. Induced Fit Model for Enzyme Catalysts {#lock-and-key-theory-vs-dot-induced-fit-model-for-enzyme-catalysts}

{{< figure src="/ox-hugo/_20210713_152510screenshot.png" >}}


### Cofactors and coenzymes {#cofactors-and-coenzymes}

-   Nonprotein molecules that increase effectiveness of [enzymes]({{< relref "enzyme" >}})
    -   Tend to be small in size so they can bind to the active site of the enzyme and participate in catalysis of reaction
        -   Usually done through carrying charge through ionization, protonation, or deprotonation
-   Kept at low concentrations in cells so they can be recruited only when needed
-   _Apoenzyme_: [enzyme]({{< relref "enzyme" >}}) without its cofactor
-   _Holoenzyme_: [enzyme]({{< relref "enzyme" >}}) with cofactor
-   _Prosthetic group_: Tightly bound cofactors/coenzymes necessary for [enzyme]({{< relref "enzyme" >}}) function


## 2.3 Enzyme Kinetics {#2-dot-3-enzyme-kinetics}


### Kinetics of monomeric enzymes {#kinetics-of-monomeric-enzymes}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 7. Describe the type of reactions these enzymes potentiate:**

    [Enzyme classifications]({{< relref "kaplan_biochemistry_chapter_2" >}})

    ---

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 6. Define and describe the roles of the following enzyme-related terms:**

    [Cofactors and coenzymes]({{< relref "kaplan_biochemistry_chapter_2" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
