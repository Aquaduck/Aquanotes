+++
title = "Receptor-mediated endocytosis"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Lysosomes > Formation > Early endosomes**

    Form part of pathway for _[receptor-mediated endocytosis]({{< relref "receptor_mediated_endocytosis" >}})_

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
