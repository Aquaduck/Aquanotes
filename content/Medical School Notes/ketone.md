+++
title = "Ketone"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Lumen - Functional Group Names, Properties, and Reactions]({{< relref "lumen_functional_group_names_properties_and_reactions" >}}) {#lumen-functional-group-names-properties-and-reactions--lumen-functional-group-names-properties-and-reactions-dot-md}

<!--list-separator-->

-  **🔖 Functional Groups > Aldehydes and Ketones**

    <!--list-separator-->

    -  Similarities between [aldehydes]({{< relref "aldehyde" >}}) and [ketones]({{< relref "ketone" >}})

        -   Both exist in equilibrium with their enol forms:

        {{< figure src="/home/kita/Documents/logseq-org-roam-wiki/hugo/content-org/medschool/_20210714_215454screenshot.png" width="600" >}}

        -   Keto form predominates at equilibrium
        -   Deprotonated enolate is a **strong nucleophile**

<!--list-separator-->

-  **🔖 Functional Groups > Aldehydes and Ketones**

    <!--list-separator-->

    -  [Ketones]({{< relref "ketone" >}})

        -   Ketone carbon is sp2 hybridized -> **trigonal planar geometry**
            -   120<sup>o</sup> bond angles
        -   **Polar**
            -   Can form hydrogen bonds
        -   Not usually hydrogen bond donors -> often more voltaile than alcohols and carboxylic acids
        -   Can participate in _keto-enol tautomerism_


#### [Kaplan Organic Chemistry Chapter 1]({{< relref "kaplan_organic_chemistry_chapter_1" >}}) {#kaplan-organic-chemistry-chapter-1--kaplan-organic-chemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 1.3 Aldehydes and Ketones**

    <!--list-separator-->

    -  [Ketones]({{< relref "ketone" >}})

        -   Carbonyl group **in the middle of a carbon chain**
            -   Thus, we always have to assign a number to it
        -   Ketones are named by replacing the \\(-e\\) of the parent alkane with the suffix \\(-one\\)
        -   When the carbonyl is not the highest priority, its prefix is \\(-oxo\\) or \\(-keto\\)

        {{< figure src="/ox-hugo/_20210714_193925screenshot.png" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
