+++
title = "Coatomer"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [COP-II]({{< relref "cop_ii" >}}) {#cop-ii--cop-ii-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [coatomer]({{< relref "coatomer" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
