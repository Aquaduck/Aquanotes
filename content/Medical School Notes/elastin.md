+++
title = "Elastin"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Reticular dermis]({{< relref "reticular_dermis" >}}) {#reticular-dermis--reticular-dermis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Contains [elastin]({{< relref "elastin" >}}) fibers

    ---


#### [Papillary dermis]({{< relref "papillary_dermis" >}}) {#papillary-dermis--papillary-dermis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Contains [elastin]({{< relref "elastin" >}}) fibers

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
