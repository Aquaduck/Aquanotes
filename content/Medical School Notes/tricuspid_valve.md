+++
title = "Tricuspid valve"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the anatomical locations for auscultation of the heart valves.]({{< relref "describe_the_anatomical_locations_for_auscultation_of_the_heart_valves" >}}) {#describe-the-anatomical-locations-for-auscultation-of-the-heart-valves-dot--describe-the-anatomical-locations-for-auscultation-of-the-heart-valves-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    [Tricuspid valve]({{< relref "tricuspid_valve" >}}): Over left half of inferior end of body of sternum

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
