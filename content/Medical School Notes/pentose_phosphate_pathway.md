+++
title = "Pentose phosphate pathway"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP.]({{< relref "outline_the_general_features_of_the_pathways_of_de_novo_synthesis_of_the_two_purine_and_three_pyrimidine_ribonucleotides_noting_the_chemical_changes_required_for_conversion_of_utp_to_ctp_and_imp_to_atp_gtp" >}}) {#outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot--outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Synthesis of purine nucleotides (p. 291)**

    [Ribose 5-phosphate]({{< relref "ribose_5_phosphate" >}}) from the [PPP]({{< relref "pentose_phosphate_pathway" >}}) is used as a starting molecule

    ---


#### [List the pathways for which glucose-6-phosphate is the initial substrate]({{< relref "list_the_pathways_for_which_glucose_6_phosphate_is_the_initial_substrate" >}}) {#list-the-pathways-for-which-glucose-6-phosphate-is-the-initial-substrate--list-the-pathways-for-which-glucose-6-phosphate-is-the-initial-substrate-dot-md}

<!--list-separator-->

-  **🔖 Pathways**

    [Pentose phosphate pathway]({{< relref "pentose_phosphate_pathway" >}})

    ---


#### [List the glucose-utilizing pathways that predominate in:]({{< relref "list_the_glucose_utilizing_pathways_that_predominate_in" >}}) {#list-the-glucose-utilizing-pathways-that-predominate-in--list-the-glucose-utilizing-pathways-that-predominate-in-dot-md}

<!--list-separator-->

-  **🔖 Liver**

    [PPP]({{< relref "pentose_phosphate_pathway" >}})

    ---

<!--list-separator-->

-  **🔖 Adipose**

    [PPP]({{< relref "pentose_phosphate_pathway" >}})

    ---

<!--list-separator-->

-  **🔖 Muscle**

    [PPP]({{< relref "pentose_phosphate_pathway" >}})

    ---

<!--list-separator-->

-  **🔖 Brain**

    [PPP]({{< relref "pentose_phosphate_pathway" >}}) (both arms)

    ---

<!--list-separator-->

-  **🔖 RBC**

    [PPP]({{< relref "pentose_phosphate_pathway" >}}) (mainly NADPH for antioxidant)

    ---


#### [A Discussion of the Significance of Two Key Metabolic Branchpoints]({{< relref "a_discussion_of_the_significance_of_two_key_metabolic_branchpoints" >}}) {#a-discussion-of-the-significance-of-two-key-metabolic-branchpoints--a-discussion-of-the-significance-of-two-key-metabolic-branchpoints-dot-md}

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > Brain > Fed state**

    Neural tissue utilizes both "arms" of the [pentose phosphate pathway]({{< relref "pentose_phosphate_pathway" >}}) as they synthesize nucleotides

    ---

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > RBCs > Fasting state**

    **[Pentose phosphate pathway]({{< relref "pentose_phosphate_pathway" >}}) slowed considerably**

    ---

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > RBCs > Fed state**

    [Pentose phosphate pathway]({{< relref "pentose_phosphate_pathway" >}}) -> production of NADPH

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
