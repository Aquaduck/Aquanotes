+++
title = "Cytochrome c oxidase"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 HIF-1 target genes involved in glucose and energy metabolism**

    HIF-1 orchestrates a subunit switch in [cytochrome c oxidase]({{< relref "cytochrome_c_oxidase" >}}) -> optimizes efficiency of respiration in response to cellular O<sub>2</sub> changes

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
