+++
title = "Describe the procedures associated with intercostal nerve anesthesia and thoracotomy and their indications."
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Accessing an Intercostal Space]({{< relref "accessing_an_intercostal_space" >}}) {#from-accessing-an-intercostal-space--accessing-an-intercostal-space-dot-md}


### [Anesthesia]({{< relref "anesthesia" >}}) {#anesthesia--anesthesia-dot-md}

-   Anesthesia can be placed in the intercostal space to block the [intercostal nerve]({{< relref "intercostal_nerve" >}})
-   **Ideally, it should be given between internal and innermost intercostal layers** where the nerve resides
    -   Can use ultrasound to guide procedure
-   Procedure is commonly used in **patients with rib fractures or for thoracic surgery**
-   When inserting a chest tube, the tube is inserted in the **inferior portion of the intercostal space**, just above the lower rib
    -   **Minimizes risk to vessels in the superior portion of the intercostal space**


### [Thoracotomy]({{< relref "thoracotomy" >}}) {#thoracotomy--thoracotomy-dot-md}

-   _Thoracotomy_: the surgical creation of an opening throuhg the thoracic wall to enter a pleural cavity
-   **Posterolateral aspects of 5th and 7th intercostal spaces are important sites for posterior thoracotomy incisions**
-   **Often used to treat or diagnose a problem with the lungs or heart**
    -   Most commonly done for lung cancer


#### Procedure {#procedure}

-   With pt lying on contralateral side, fully abduct upper limb, placing forearm beside pt's head
    -   This elevates and laterally rotates inferior angle of scapula -> allows access as high as 4th intercostal space
    -   Most commonly, rib retraction allows procedures to be performed through a single intercostal space, with care to avoid the superior neurovascular bundle
-   If **wider exposure is required**, surgeons use an **H-shaped incision** -> incise superficial aspect of periosteum that ensheaths rib, strip periosteum from rib, and excise a wide segment of rib to gain better access


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the procedures associated with intercostal nerve anesthesia and thoracotomy and their indications.]({{< relref "describe_the_procedures_associated_with_intercostal_nerve_anesthesia_and_thoracotomy_and_their_indications" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
