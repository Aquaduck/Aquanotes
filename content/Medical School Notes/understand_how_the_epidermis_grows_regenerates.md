+++
title = "Understand how the epidermis grows/regenerates."
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Histology - Skin Pre-session Powerpoint]({{< relref "histology_skin_pre_session_powerpoint" >}}) {#from-histology-skin-pre-session-powerpoint--histology-skin-pre-session-powerpoint-dot-md}


### Growth/regeneration {#growth-regeneration}

-   Epidermis grows from **bottom to top**
-   The [basal layer]({{< relref "stratum_basale" >}}) (stratum basilis or stratum germinativum) is **where cell division occurs**
    -   As cells of the basal layer divide, some cells stay in place to maintain the basal layer while others are pushed up off the basement membrane by the growth of new [keratinocytes]({{< relref "keratinocyte" >}})
        -   Keratinocytes that are pushed off the basement membrane begin to differentiate
            -   As they reach the middle layer -> degenerate and lose organelles/nuclei
            -   At the top -> flattened dead keratinocytes that gradually flake off as they are replaced by newer cells
    -   The length of the growth process varies with site and slows with age
        -   In general, epidermis turns over (sheds) about once a month
            -   Faster in babies (~2 weeks) and slower in elderly


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-21 Tue] </span></span> > Histology: Skin Workshop > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Understand how the epidermis grows/regenerates.]({{< relref "understand_how_the_epidermis_grows_regenerates" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
