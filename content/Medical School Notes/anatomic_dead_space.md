+++
title = "Anatomic dead space"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [Dead space]({{< relref "physiological_dead_space" >}}) that occurs in the conducting zone (e.g. mouth, trachea)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Define the difference between V/Q for a shunt vs. V/Q for a dead space.]({{< relref "define_the_difference_between_v_q_for_a_shunt_vs_v_q_for_a_dead_space" >}}) {#define-the-difference-between-v-q-for-a-shunt-vs-dot-v-q-for-a-dead-space-dot--define-the-difference-between-v-q-for-a-shunt-vs-v-q-for-a-dead-space-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > V/Q mismatch > Increased V/Q ratio**

    [Anatomic dead space]({{< relref "anatomic_dead_space" >}}): the volume of air in the [conducting zone]({{< relref "conducting_zone" >}}) (e.g. mouth, trachea)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
