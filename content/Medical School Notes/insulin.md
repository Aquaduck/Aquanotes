+++
title = "Insulin"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}


## Backlinks {#backlinks}


### 21 linked references {#21-linked-references}


#### [Phosphoprotein phosphatase]({{< relref "phosphoprotein_phosphatase" >}}) {#phosphoprotein-phosphatase--phosphoprotein-phosphatase-dot-md}

<!--list-separator-->

-  **🔖 Concept link**

    Activated + induced by [insulin]({{< relref "insulin" >}})

    ---


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems**

    <!--list-separator-->

    -  [Insulin]({{< relref "insulin" >}})'s metabolic effects

        -   Insulin does not bind to a GPCR
            -   Instead, binds to specific, high-affinity receptors in cell membranes of **liver, striated muscle, adipose tissue, and kidney** -> considered "insulin-sensitive" tissues
        -   Insulin-induced changes in activity of several enzymes occur over minutes to hours and reflect changes in phosphorylation states of existing proteins
            -   Over hours to days -> increases **amount** of many enzymes
                -   Reflect an increase in gene expression through increased transcription mediated by specific regulatory element-binding proteins and subsequent translation
        -   Crucial aspect of insulin-induced signaling is **activation of enzymes that eliminate glucagon-mediated changes in enzyme activities**
            -   Insulin imediately activates [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) -> remove any phosphates introduced by [PKA]({{< relref "protein_kinase_a" >}}) -> **reverses processes**
            -   E.g. insulin-induced phosphatases stop glycogenolysis and promote glycogen synthesis + storage -> ensures glycogen pools are restored in liver and striated muscle
            -   Restores [pyruvate kinase]({{< relref "pyruvate_kinase" >}}) activity -> stops [gluconeogenesis]({{< relref "gluconeogenesis" >}}) by immediately converting any PEP made back to pyruvate
        -   Activates [cAMP-phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})
            -   Hydrolyzes cAMP to 5'-AMP -> eliminate second messenger activating PKA -> [PKA]({{< relref "protein_kinase_a" >}}) activity significantly diminished
        -   Promotes storage of other nutrients e.g. triacylglycerol and protein + inhibits their mobilization

        <!--list-separator-->

        -  Summary of insulin's overall metabolic effects

            1.  ↑ Glucose uptake
            2.  ↑ Glycogen and protein synthesis
            3.  ↓ Gluconeogenesis
            4.  ↓ Glycogenolysis
            5.  ↓ Fatty acid mobilization
            6.  ↑ Fat synthesis in insulin-sensitive tissues _other than the liver_

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase**

    [Glucagon]({{< relref "glucagon" >}}) antagonizes [insulin]({{< relref "insulin" >}})'s effects

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Lipoprotein lipase > From Fat Metabolism in Muscle & Adipose Tissue**

    [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Mobilization of triacylglycerol stored in adipocytes**

    **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    [Insulin]({{< relref "insulin" >}}) also **increases glucose uptake in skeletal muscle and adipose tissue**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues

    ---


#### [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}}) {#lipoprotein-lipase--lipoprotein-lipase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Induced by [insulin]({{< relref "insulin" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Additional important enzymes > Phosphoprotein phosphatases**

    Activated + induced by [insulin]({{< relref "insulin" >}})

    ---


#### [Glycogen synthase]({{< relref "glycogen_synthase" >}}) {#glycogen-synthase--glycogen-synthase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Insulin]({{< relref "insulin" >}})

    ---


#### [GLUT4 Exocytosis]({{< relref "glut4_exocytosis" >}}) {#glut4-exocytosis--glut4-exocytosis-dot-md}

<!--list-separator-->

-  **🔖 Notes**

    <!--list-separator-->

    -  There is evidence that [GSVs]({{< relref "glut4_storage_vesicles" >}}) directly fuse with the plasma membrane, but this occurs as a transient burst -> further effort needed to confirm whether GLUT-4 continues to recycle back to plasma membrane in GSVs or in endosomes when [insulin]({{< relref "insulin" >}}) is present

<!--list-separator-->

-  **🔖 Notes > GLUT-4 resides in specialised vesicles called GSVs that undergo insulin-dependent translocation to the plasma membrane**

    This happens in the absense of [insulin]({{< relref "insulin" >}})

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    E.g. [Sodium]({{< relref "sodium" >}}), [Potassium]({{< relref "potassium" >}}), [Chloride]({{< relref "chloride" >}}), [Bicarbonate]({{< relref "bicarbonate" >}}), [Glucose]({{< relref "glucose" >}}), [Urea]({{< relref "urea" >}}), [amino acids]({{< relref "amino_acid" >}}), peptides such as [Insulin]({{< relref "insulin" >}}) and [ADH]({{< relref "antidiuretic_hormone" >}})

    ---


#### [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}}) {#explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot--explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Delivering dietary fat to extrahepatic tissues via chylomicrons**

    [Insulin]({{< relref "insulin" >}}) also **increases glucose uptake in skeletal muscle and adipose tissue**

    ---

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Delivering dietary fat to extrahepatic tissues via chylomicrons**

    [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**

    ---

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Delivering dietary fat to extrahepatic tissues via chylomicrons**

    Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues

    ---


#### [Describe the role of protein kinase A in mobilizing fat from adipocyte stores.]({{< relref "describe_the_role_of_protein_kinase_a_in_mobilizing_fat_from_adipocyte_stores" >}}) {#describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot--describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > The role of PKA in mobilizing fat from adipocytes**

    **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**

    ---


#### [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}}) {#describe-the-intercellular-response-to-insulin-induced-cellular-signaling--describe-the-intercellular-response-to-insulin-induced-cellular-signaling-dot-md}

<!--list-separator-->

-  [Insulin]({{< relref "insulin" >}})'s metabolic effects

    -   Insulin does not bind to a GPCR
        -   Instead, binds to specific, high-affinity receptors in cell membranes of **liver, striated muscle, adipose tissue, and kidney** -> considered "insulin-sensitive" tissues
    -   Insulin-induced changes in activity of several enzymes occur over minutes to hours and reflect changes in phosphorylation states of existing proteins
        -   Over hours to days -> increases **amount** of many enzymes
            -   Reflect an increase in gene expression through increased transcription mediated by specific regulatory element-binding proteins and subsequent translation
    -   Crucial aspect of insulin-induced signaling is **activation of enzymes that eliminate glucagon-mediated changes in enzyme activities**
        -   Insulin imediately activates [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) -> remove any phosphates introduced by [PKA]({{< relref "protein_kinase_a" >}}) -> **reverses processes**
        -   E.g. insulin-induced phosphatases stop glycogenolysis and promote glycogen sysnthesis + storage -> ensures glycogen pools are respored in liver and striated muscle
        -   Restores [pyruvate kinase]({{< relref "pyruvate_kinase" >}}) activity -> stops [gluconeogenesis]({{< relref "gluconeogenesis" >}}) by immediately converting any PEP made back to pyruvate
    -   Activates [cAMP-phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})
        -   Hydrolyzes cAMP to 5'-AMP -> eliminate second messenger activating PKA -> [PKA]({{< relref "protein_kinase_a" >}}) activity significantly diminished
    -   Promotes storage of other nutrients e.g. triacylglycerol and protein + inhibits their mobilization

    <!--list-separator-->

    -  Summary of insulin's overall metabolic effects

        1.  ↑ Glucose uptake
        2.  ↑ Glycogen and protein synthesis
        3.  ↓ Gluconeogenesis
        4.  ↓ Glycogenolysis
        5.  ↓ Fatty acid mobilization
        6.  ↑ Fat synthesis in insulin-sensitive tissues _other than the liver_


#### [Cell Signalling III - Underlying Design Features]({{< relref "cell_signalling_iii_underlying_design_features" >}}) {#cell-signalling-iii-underlying-design-features--cell-signalling-iii-underlying-design-features-dot-md}

<!--list-separator-->

-  **🔖 AM vs FM signaling > Macromolecule complexes > Lipid rafts**

    An example of this is [insulin]({{< relref "insulin" >}}) signaling

    ---


#### [A Discussion of the Significance of Two Key Metabolic Branchpoints]({{< relref "a_discussion_of_the_significance_of_two_key_metabolic_branchpoints" >}}) {#a-discussion-of-the-significance-of-two-key-metabolic-branchpoints--a-discussion-of-the-significance-of-two-key-metabolic-branchpoints-dot-md}

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > RBCs > Fed state**

    [Insulin]({{< relref "insulin" >}}) has **no effect on glucose uptake into [RBCs]({{< relref "red_blood_cell" >}})** in the **fed state**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
