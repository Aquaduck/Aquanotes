+++
title = "Glycogen phosphorylase kinase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    This is because [PKA]({{< relref "protein_kinase_a" >}}) phosphorylates and activates a key glycogen-degrading enzyme ([glycogen phosphorylase kinase]({{< relref "glycogen_phosphorylase_kinase" >}})), and also phosphorylates and inactivates the rate-limiting enzyme in glycogen synthesis ([glycogen synthase]({{< relref "glycogen_synthase" >}}))

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
