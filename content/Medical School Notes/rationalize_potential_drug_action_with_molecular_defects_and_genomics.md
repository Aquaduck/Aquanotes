+++
title = "Rationalize potential drug action with molecular defects and genomics."
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cystic fibrosis in the year 2020: A disease with a new face]({{< relref "cystic_fibrosis_in_the_year_2020_a_disease_with_a_new_face" >}}) {#from-cystic-fibrosis-in-the-year-2020-a-disease-with-a-new-face--cystic-fibrosis-in-the-year-2020-a-disease-with-a-new-face-dot-md}


### Drugs treating [cystic fibrosis]({{< relref "cystic_fibrosis" >}}) {#drugs-treating-cystic-fibrosis--cystic-fibrosis-dot-md}

-   Three oral drugs approved to treat cystic fibrosis:
    1.  _Ivacaftor_
        -   Potentiator: improves [CFTR]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}}) channel opening -> more ions flow through pore
    2.  _Tezacaftor_
        -   Corrector: improve CFTR protein folding and trafficking -> more mature CFTR appears at cell membrane
    3.  _Lumacaftor_
        -   Same as _Tezacaftor_
-   Treatment regimens:
    1.  Kalydeco (ivacaftor)
    2.  Orkambi (lumacaftor + ivacaftor)
    3.  Symkevi, Symdeko (tezacaftor + ivacaftor)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-19 Tue] </span></span> > Cystic Fibrosis Small Group > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Rationalize potential drug action with molecular defects and genomics.]({{< relref "rationalize_potential_drug_action_with_molecular_defects_and_genomics" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
