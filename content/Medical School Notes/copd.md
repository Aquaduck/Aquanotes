+++
title = "COPD"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   A family of [Obstructive lung disease]({{< relref "obstructive_lung_disease" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Obstructive lung disease > Etiology**

    [COPD]({{< relref "copd" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
