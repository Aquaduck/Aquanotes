+++
title = "Pantothenic acid"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   AKA _Vitamin B<sub>5</sub>_
-   [B vitamin]({{< relref "b_vitamin" >}})


## From First Aid Section II Biochemistry Nutrition {#from-first-aid-section-ii-biochemistry-nutrition}


### Function {#function}

-   Essential component of [coenzyme A]({{< relref "coenzyme_a" >}}) (used in synthesis) and [fatty acid synthase]({{< relref "fatty_acid_synthase" >}})


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [List the vitamins, both by number and by name, and indicate, in broad strokes, why they are important to the metabolic processes and/or pathways specified above]({{< relref "list_the_vitamins_both_by_number_and_by_name_and_indicate_in_broad_strokes_why_they_are_important_to_the_metabolic_processes_and_or_pathways_specified_above" >}}) {#list-the-vitamins-both-by-number-and-by-name-and-indicate-in-broad-strokes-why-they-are-important-to-the-metabolic-processes-and-or-pathways-specified-above--list-the-vitamins-both-by-number-and-by-name-and-indicate-in-broad-strokes-why-they-are-important-to-the-metabolic-processes-and-or-pathways-specified-above-dot-md}

<!--list-separator-->

-  [B5]({{< relref "pantothenic_acid" >}})

    -   CoA


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    [Acetyl-CoA]({{< relref "acetyl_coa" >}}) from CoA from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)

    ---

<!--list-separator-->

-  **🔖 TCA cycle**

    [Acetyl-CoA]({{< relref "acetyl_coa" >}}) from CoA from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)

    ---

<!--list-separator-->

-  **🔖 Pyruvate DH complex**

    [CoA]({{< relref "coenzyme_a" >}}) from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)

    ---


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Cellular Metabolism > 14. List the vitamins required for the appropriate functioning of: > Fatty acid oxidation**

    [Pantothenic acid]({{< relref "pantothenic_acid" >}})

    ---


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By vitamin**

    <!--list-separator-->

    -  [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)

        -   Pyruvate DH complex
        -   alpha-KG DH


### Unlinked references {#unlinked-references}

[Show unlinked references]
