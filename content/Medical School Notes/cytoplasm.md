+++
title = "Cytoplasm"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A component of the [cell]({{< relref "cell" >}})
-   The cytosol component between [organelles]({{< relref "organelle" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle > NAD<sup>+</sup>-linked isocitrate dehydrogenase**

    [AMP]({{< relref "adenosine_monophosphate" >}}) is master switch in the [cytoplasm]({{< relref "cytoplasm" >}})

    ---


#### [Adenosine Monophosphate]({{< relref "adenosine_monophosphate" >}}) {#adenosine-monophosphate--adenosine-monophosphate-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    AMP is the master switch of the [cytoplasm]({{< relref "cytoplasm" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
