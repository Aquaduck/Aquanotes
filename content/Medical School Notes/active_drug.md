+++
title = "Active drug"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Indicate whether duplication of an entire CYP gene will increase or decrease expected drug effect, when the encoded P450 enzyme metabolizes a pro-drug to the active drug.]({{< relref "indicate_whether_duplication_of_an_entire_cyp_gene_will_increase_or_decrease_expected_drug_effect_when_the_encoded_p450_enzyme_metabolizes_a_pro_drug_to_the_active_drug" >}}) {#indicate-whether-duplication-of-an-entire-cyp-gene-will-increase-or-decrease-expected-drug-effect-when-the-encoded-p450-enzyme-metabolizes-a-pro-drug-to-the-active-drug-dot--indicate-whether-duplication-of-an-entire-cyp-gene-will-increase-or-decrease-expected-drug-effect-when-the-encoded-p450-enzyme-metabolizes-a-pro-drug-to-the-active-drug-dot-md}

<!--list-separator-->

-  **🔖 From Pharmacogenetics Pre-learning Material**

    <!--list-separator-->

    -  [Active drug]({{< relref "active_drug" >}})

        -   A drug that is initially **active** -> **metabolism inactivates it**
        -   Duplication of a CYP gene -> **decrease in available activity**
            -   Drug is very quickly inactivated, preventing the drug from reaching therapeutic doses


### Unlinked references {#unlinked-references}

[Show unlinked references]
