+++
title = "Truncus arteriosus"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Facts about truncus arteriosus]({{< relref "facts_about_truncus_arteriosus" >}}) {#facts-about-truncus-arteriosus--facts-about-truncus-arteriosus-dot-md}

<!--list-separator-->

-  **🔖 Facts about Truncus Arteriosus**

    <!--list-separator-->

    -  What is [Truncus Arteriosus]({{< relref "truncus_arteriosus" >}})

        Truncus arteriosus is a birth defect of the heart. It occurs when the blood vessel coming out of the heart in the developing baby fails to separate completely during development, leaving a connection between the aorta and pulmonary artery. There are several different types of truncus, depending on how the arteries remain connected. There is also usually a hole between the bottom two chambers of the heart (ventricles) called a [ventricular septal defect](https://www.cdc.gov/ncbddd/heartdefects/ventricularseptaldefect.html). Because a baby with this defect may need surgery or other procedures soon after birth, truncus arteriosus is considered a [critical congenital heart defect (CCHD)](https://www.cdc.gov/ncbddd/heartdefects/cchd-facts.html). Congenital means present at birth.

        In a baby without a congenital heart defect, the right side of the heart pumps oxygen-poor blood through the pulmonary artery to the lungs. The left side of the heart pumps oxygen-rich blood through the aorta to the rest of the body.

        In babies with a truncus arteriosus, oxygen-poor blood and oxygen-rich blood are mixed together as blood flows to the lungs and the rest of the body. As a result, too much blood goes to the lungs and the heart works harder to pump blood to the rest of the body. Also, instead of having both an _aortic valve_ and a _pulmonary valve_, babies with truncus arteriosus have a single common valve (truncal valve) controlling blood flow out of the heart. The truncal valve is often abnormal. The valve can be thickened and narrowed, which can block the blood as it leaves the heart. It can also leak, causing blood that leaves the heart to leak back into the heart across the valve.

<!--list-separator-->

-  **🔖 Facts about Truncus Arteriosus**

    [Truncus arteriosus]({{< relref "truncus_arteriosus" >}}) (pronounced TRUNG-kus ahr-teer-e-O-sus), also known as common truncus, is a rare defect of the heart in which a single common blood vessel comes out of the heart, instead of the usual two vessels (the _main pulmonary artery_ and _aorta_).

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
