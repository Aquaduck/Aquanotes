+++
title = "Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS)."
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#from-openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}


### [Renin-Angiotensin-Aldosterone Mechanism]({{< relref "renin_angiotensin_aldosterone_mechanism" >}}) (p. 927) {#renin-angiotensin-aldosterone-mechanism--renin-angiotensin-aldosterone-mechanism-dot-md----p-dot-927}

-   Major effect on the [cardiovascular system]({{< relref "cardiovascular_system" >}})


#### [Renin]({{< relref "renin" >}}) + Angiotensin {#renin--renin-dot-md--plus-angiotensin}

-   Specialized cells in the [kidneys]({{< relref "kidney" >}}) found in the [juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}}) respond to decreased blood flow by secreting _renin_ into the blood
    -   Converts the plasma protein [angiotensinogen]({{< relref "angiotensinogen" >}}) (produced by the liver) into its active form - [angiotensin I]({{< relref "angiotensin_i" >}})


#### Angiotensin ([I]({{< relref "angiotensin_i" >}}) + [II]({{< relref "angiotensin_ii" >}})) {#angiotensin--i-angiotensin-i-dot-md--plus-ii--angiotensin-ii-dot-md}

-   Angiotensin I circulates in the blood -> reaches lungs -> converted into Angiotensin II
-   Angiotensin II is a powerful **vasoconstrictor** -> greatly increases blood pressure
-   Angiotensin II stimulates the release of [ADH]({{< relref "antidiuretic_hormone" >}}) from the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) and [aldosterone]({{< relref "aldosterone" >}}) from the [adrenal cortex]({{< relref "adrenal_cortex" >}})
-   Angiotensin II stimulates the thirst center in the hypothalamus -> increase of fluid consumption -> increase blood volume and blood pressure


#### [Aldosterone]({{< relref "aldosterone" >}}) {#aldosterone--aldosterone-dot-md}

-   Increases the reabsorption of [sodium]({{< relref "sodium" >}}) into the [blood]({{< relref "blood" >}}) by the [kidneys]({{< relref "kidney" >}}) -> **increases reabsorption of water** -> increases blood volume -> raises blood pressure


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-18 Mon] </span></span> > Regional and Peripheral Circulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
