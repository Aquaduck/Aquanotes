+++
title = "Glycolysis"
author = ["Arif Ahsan"]
date = 2021-07-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   Glycolysis is a cellular [metabolic]({{< relref "metabolism" >}}) process that converts glucose into energy via [ATP]({{< relref "atp" >}}) production


## Backlinks {#backlinks}


### 16 linked references {#16-linked-references}


#### [Thiamine]({{< relref "thiamine" >}}) {#thiamine--thiamine-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry - Nutrition > Function**

    [Pyruvate dehydrogenase]({{< relref "pyruvate_dehydrogenase_complex" >}}) (links [glycolysis]({{< relref "glycolysis" >}}) to TCA cycle)

    ---


#### [Pyruvate kinase]({{< relref "pyruvate_kinase" >}}) {#pyruvate-kinase--pyruvate-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Final enzyme in the [glycolytic]({{< relref "glycolysis" >}}) pathway

    ---


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Connects [glycolysis]({{< relref "glycolysis" >}}) to the [TCA cycle]({{< relref "citric_acid_cycle" >}})

    ---


#### [Osmosis - Glycolysis]({{< relref "osmosis_glycolysis" >}}) {#osmosis-glycolysis--osmosis-glycolysis-dot-md}

<!--list-separator-->

-  [Video]({{< relref "glycolysis" >}})

    -   _Glycolysis_ - glucose converted into 2x pyruvate -> forms ATP
    -   Glycolysis can occur **even in low oxygen**
    -   Two phases
        1.  Energy consuming phase
        2.  Energy producing phase

    <!--list-separator-->

    -  Pathway

        1.  Glucose enters bloodstream via small intestine
        2.  Stimulates pancreas to secrete insulin from beta-islet cells

            {{< figure src="/ox-hugo/_20210719_120939screenshot.png" width="500" >}}
        3.  Glucose transporters (GLUT) transport glucose into cell
        4.  Kinases phosphorylate glucose -> **can't easily diffuse out of cell**

            -   **-1 ATP**
            -   Hexokinase/glucokinase convert glucose to **g6p** - glucose w/ phosphate on 6th carbon
                -   _Hexokinase_: found in all cells
                -   _Glucokinase_: induced by insulin
            -   Reaction is **irreversible**

            {{< figure src="/ox-hugo/_20210719_121313screenshot.png" width="500" >}}
        5.  g6p converted to **fructose-6-phosphate** (f6p) via _phosphoglucoisomerase_
        6.  _Phosphofructokinase-1_ (_PFK1_) phosphorylates f6p into **fructose 1,6-bisphosphate**

            -   **Irreversible**
            -   **Rate-limiting**
                -   Cells regulate this via _PFK2_ -> phosphorylates **2nd** carbon -> **fructose 2,6-bisphosphate**
                    -   Activated by _insulin_
                        -   **Increases [PFK1]**
                    -   Inhibited by _glucagon_, _ATP_ and _citrate_
                        -   Decreases [PFK1]
            -   **-1 ATP**
            -   **Determines speed of glycolysis**

            {{< figure src="/ox-hugo/_20210719_122031screenshot.png" >}}

        7.  _Aldolase_ converts F-1,6-B -> **G3P** (glyceraldehyde 3-phosphate) + **DHAP** (dihydroacetone-phosphate)
            -   DHAP converted via _isomerase_ into **G3P** -> **2x G3P produced per 1 molecule of glucose**
        8.  _G3P-dehydrogenase_ phosphorylates G3P -> **1,3-BPG** (1,3-Biphosphoglycerate)

            -   Removes 1H from G3P -> gives it to NAD<sup>+</sup> to form NADH

            {{< figure src="/ox-hugo/_20210719_123639screenshot.png" width="500" >}}
        9.  _Phosphoglycerate kinase_ converts 1,3-BPG -> **3-phosphoglycerate**
            -   Produces 1 ATP per molecule -> **+2 ATP**
        10. _Mutase_ converts 3-phosphoglycerate -> **2-phosphoglycerate**
        11. _Enolase_ converts 2-phosphoglycerate -> **PEP** (phosphoenolpyruvate)

            -   Uses up 1 H<sub>2</sub>O

            {{< figure src="/ox-hugo/_20210719_124002screenshot.png" width="500" >}}
        12. _Pyruvate kinase_ converts PEP -> **pyruvate**

            -   **+2 ATP**
            -   _upregulated_ by F-1,6-B
            -   _downregulated_ by ATP and Alanine
                -   Alanine produced from skeletal muscle breakdown during fasting -> signals that more glucose is needed, prevents from being converted into ATP

            {{< figure src="/ox-hugo/_20210719_124325screenshot.png" width="500" >}}

    <!--list-separator-->

    -  Anaerobic respiration

        -   _Lactate dehydrogenase_ can convert pyruvate + NADH -> lactate + NAD<sup>+</sup>
            -   NAD<sup>+</sup> needed to keep glycolysis going
            -   Lactate removed by **kidneys**

        {{< figure src="/ox-hugo/_20210719_124525screenshot.png" width="500" >}}


#### [Osmosis - Gluconeogenesis]({{< relref "osmosis_gluconeogenesis" >}}) {#osmosis-gluconeogenesis--osmosis-gluconeogenesis-dot-md}

<!--list-separator-->

-  **🔖 Summary**

    Essentially the reverse of [glycolysis]({{< relref "glycolysis" >}}) with _3 irreversible reactions_ bypassed with unique enzymes

    ---


#### [List the pathways for which glucose-6-phosphate is the initial substrate]({{< relref "list_the_pathways_for_which_glucose_6_phosphate_is_the_initial_substrate" >}}) {#list-the-pathways-for-which-glucose-6-phosphate-is-the-initial-substrate--list-the-pathways-for-which-glucose-6-phosphate-is-the-initial-substrate-dot-md}

<!--list-separator-->

-  **🔖 Pathways**

    [Glycolysis]({{< relref "glycolysis" >}})

    ---


#### [List the glucose-utilizing pathways that predominate in:]({{< relref "list_the_glucose_utilizing_pathways_that_predominate_in" >}}) {#list-the-glucose-utilizing-pathways-that-predominate-in--list-the-glucose-utilizing-pathways-that-predominate-in-dot-md}

<!--list-separator-->

-  **🔖 Adipose**

    [Glycolysis]({{< relref "glycolysis" >}})

    ---

<!--list-separator-->

-  **🔖 Muscle**

    [Glycolysis]({{< relref "glycolysis" >}})

    ---

<!--list-separator-->

-  **🔖 Brain**

    [Glycolysis]({{< relref "glycolysis" >}})

    ---

<!--list-separator-->

-  **🔖 RBC**

    [Glycolysis]({{< relref "glycolysis" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  [Glycolysis]({{< relref "glycolysis" >}})

    -   [NADH]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Glycolysis > 6-Phosphofructo-1-kinase (PFK-1)**

    Slows [glycolysis]({{< relref "glycolysis" >}}) and allows for the synthesis of either [glycogen]({{< relref "glycogen" >}}) or fatty acid synthesis

    ---

<!--list-separator-->

-  [Glycolysis]({{< relref "glycolysis" >}})

    -   Occurs **primarily in the fed state**
    -   All tissues **except [liver]({{< relref "liver" >}})** use glucose via glycolysis as an energy source in the fed state
        -   [Brain]({{< relref "brain" >}}) has **obligate requirement** for glucose
        -   Liver uses glucose to **initially replenish its glycogen stores**
            -   Secondarily will generate acetyl-CoA via glycolysis and pyruvate DH complex for use in cholesterol synthesis
            -   Liver **prefers to use amino acids** provided by dietary protein as energy source in fed state

    <!--list-separator-->

    -  [Hexokinases]({{< relref "hexokinase" >}}) I-III

        -   In most tissues (other than pancreas + liver), the first reaction in glycolytic pathway is catalyzed by one of the isozymes of hexokinase
        -   Hexokinase catalyzes the **first** of three highly-regulated **irreversible** glycolytic reactions
            -   Hexokinase catalyzes ATP-dependent phosphorylation of glucose to [G6P]({{< relref "glucose_6_phosphate" >}})
                -   G6P cannot diffuse out of the cell -> allows for continuous glucose uptake by cells
        -   Hexokinase enzymes are **inhibited by [G6P]({{< relref "glucose_6_phosphate" >}}), the reaction product**
            -   Low K<sub>m</sub> (high affinity) for glucose
            -   Low V<sub>max</sub> (low capacity) for glucose phosphorylation
                -   Allows for efficient phosphorylation and metabolism of glucose even w/ low tissue concentrations
            -   This makes them **allosteric enzymes**

    <!--list-separator-->

    -  [Glucokinase]({{< relref "glucokinase" >}}) (hexokinase IV)

        -   **Restricted to liver parenchymal cells and β-cells of pancreas**
        -   Has cooperativity with [glucose]
        -   Has a **much higher K<sub>m</sub> and V<sub>max</sub>**
            -   Low affinity for glucose ensures that when [glucose] is high, it will be trapped in the liver
                -   Whereas when [glucose] is low, it **will not be recognized as a substrate**
                -   This prevents [hyperglycemia]({{< relref "hyperglycemia" >}})
        -   **Not subject to regulation by product inhibition**
            -   Regulated by glucokinase regulatory protein (GKRP) (_outside scope of FoCS_)

    <!--list-separator-->

    -  [6-Phosphofructo-1-kinase]({{< relref "6_phosphofructo_1_kinase" >}}) (PFK-1)

        -   Third enzyme in glycolysis
        -   **Catalyzes the rate-limiting, committed step of glycolysis**
        -   The **second** irreversible glycolytic reaction
        -   **Allosterically regulated** by **energy charge** of the cell
            -   ↑[ATP] **inhibits** PFK-1
            -   ↑[AMP] **stimulates** PFK-1
                -   This is because adenylate cyclase is a cytoplasmic enzyme and catalyzed in the reaction \\(2 ADP -> ATP + AMP\\)
                    -   AMP:ATP ratio is the more sensitive indicator of energy charge
        -   **Elevated levels of citrate** also allosterically **inhibits** PFK-1
            -   This is due to high [ATP] + [acetyl-CoA] in the mitochondria
            -   Slows [glycolysis]({{< relref "glycolysis" >}}) and allows for the synthesis of either [glycogen]({{< relref "glycogen" >}}) or fatty acid synthesis

    <!--list-separator-->

    -  [Pyruvate kinase]({{< relref "pyruvate_kinase" >}})

        -   Final enzyme in the glycolytic pathway
        -   Catalyzes the third irreversible + second substrate-level phosphorylation reaction
        -   Produces [pyruvate]({{< relref "pyruvate" >}}) from [phosphoenolpyruvate]({{< relref "phosphoenolpyruvate" >}}) (PEP)
        -   **Allosterically inhibited** by [ATP]({{< relref "atp" >}}) in all tissues
        -   When glucose is plentiful and PFK-1 is active, fructose 1,6-bisphosphate will feed forward and activate the enzyme
        -   Deficiencies in the red blood cell isozyme causes hemolytic anemia

        <!--list-separator-->

        -  The liver isozyme is uniquely regulated

            -   Allosterically inhibited by ATP, _alanine_, and _long-chain fatty acids_
                -   All of which are plentiful during gluconeogenesis
            -   A substrate of protein kinase A
                -   **Downregulated** by phosphorylation

    <!--list-separator-->

    -  [6-Phosphofructo-2-kinase]({{< relref "pfk_2" >}}) (PFK-2)

        -   Liver enzyme that expresses **both** a _kinase_ & _phosphatase_ activity
        -   The product, [fructose-2,6-bisphosphate]({{< relref "fructose_2_6_bisphosphate" >}}), is formed when the kinase catalytic site is active
            -   This product **uniquely regulates glycolysis vs. gluconeogenesis**
                -   Stimulates PFK-1 -> stimulates glycolysis
                -   Inhibits fructose-1,6-bisphosphatase -> inhibits gluconeogenesis
        -   The _kinase_ component is a [PKA]({{< relref "protein_kinase_a" >}}) substrate
            -   **Inactivated by phosphorylation** -> **increases activity** of the _phosphatase_ component
        -   In the fasting state (gluconeogenesis favored in liver) -> [[F-2,6-bP]({{< relref "fructose_2_6_bisphosphate" >}})] is limited -> **PFK-1 not stimulated** -> further favoring of gluconeogenesis


#### [A Discussion of the Significance of Two Key Metabolic Branchpoints]({{< relref "a_discussion_of_the_significance_of_two_key_metabolic_branchpoints" >}}) {#a-discussion-of-the-significance-of-two-key-metabolic-branchpoints--a-discussion-of-the-significance-of-two-key-metabolic-branchpoints-dot-md}

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > Brain > Fasting state**

    Brain requires a **constant source of glucose for aerobic [glycolysis]({{< relref "glycolysis" >}}), ATP generation via [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}}), and replenishing lost [TCA]({{< relref "citric_acid_cycle" >}}) cycle intermediates**

    ---

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > RBCs > Fed state**

    Anaerobic [glycolysis]({{< relref "glycolysis" >}}) -> generation of ATP

    ---


#### [6-Phosphofructo-1-kinase]({{< relref "6_phosphofructo_1_kinase" >}}) {#6-phosphofructo-1-kinase--6-phosphofructo-1-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Catalyzes the rate-limiting, committed (third) step of [glycolysis]({{< relref "glycolysis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
