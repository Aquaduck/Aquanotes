+++
title = "Lipoprotein"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [List the functions of lymphatic drainage.]({{< relref "list_the_functions_of_lymphatic_drainage" >}}) {#list-the-functions-of-lymphatic-drainage-dot--list-the-functions-of-lymphatic-drainage-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Functions of lymphatic flow**

    An additional function unrelated to immunity is the **transport of [lipoproteins]({{< relref "lipoprotein" >}}) absored in the G.I. tract**, which **exclusively enter the blood via lymphatics**

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoB48 > From Fat Metabolism in Muscle & Adipose Tissue**

    Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB48

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB-48

    ---


#### [Describe the events required for packaging dietary fat into chylomicrons.]({{< relref "describe_the_events_required_for_packaging_dietary_fat_into_chylomicrons" >}}) {#describe-the-events-required-for-packaging-dietary-fat-into-chylomicrons-dot--describe-the-events-required-for-packaging-dietary-fat-into-chylomicrons-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB-48

    ---


#### [Chylomicron]({{< relref "chylomicron" >}}) {#chylomicron--chylomicron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [lipoprotein]({{< relref "lipoprotein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
