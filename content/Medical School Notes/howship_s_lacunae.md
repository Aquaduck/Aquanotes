+++
title = "Howship's lacunae"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Osteoclast]({{< relref "osteoclast" >}}) {#osteoclast--osteoclast-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Resides in [Howship's lacunae]({{< relref "howship_s_lacunae" >}})

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Cytologic components of mature bone**

    Multinucleated cells that reside in [Howship's lacunae]({{< relref "howship_s_lacunae" >}}) (irregular pits) in the bone formed by digestion of the osteoid

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
