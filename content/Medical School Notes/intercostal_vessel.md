+++
title = "Intercostal vessel"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the neurovasculature of the intercostal spaces.]({{< relref "describe_the_neurovasculature_of_the_intercostal_spaces" >}}) {#describe-the-neurovasculature-of-the-intercostal-spaces-dot--describe-the-neurovasculature-of-the-intercostal-spaces-dot-md}

<!--list-separator-->

-  **🔖 From Accessing an Intercostal Space**

    <!--list-separator-->

    -  [Intercostal Vessels]({{< relref "intercostal_vessel" >}})

        -   The _intercostal arteries_ course through **the thoracic wall between the ribs**
        -   **With the exception of 10th and 11th intercostal spaces**, each is supplied by 3 arteries:
            1.  A larger posterior intercostal artery
            2.  **2** small anterior intercostal arteries
        -   _Posterior intercostal arteries_ of **all but the first two interspaces** are from the _descending thoracic aorta_
        -   _Anterior intercostal arteries_ are branches of the _internal thoracic artery_
        -   Veins have similar names and generally follow the distribution of the intercostal arteries
            -   **Exception**: _Posterior intercostal veins_ drain into the _azygos system_


### Unlinked references {#unlinked-references}

[Show unlinked references]
