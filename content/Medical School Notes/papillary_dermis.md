+++
title = "Papillary dermis"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The more superficial layer of the [dermis]({{< relref "dermis" >}}) made up of loose CT
-   Contains [elastin]({{< relref "elastin" >}}) fibers


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}}) {#know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot--know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Overview of Basic Skin > 3 layers > Dermis**

    Loose ([papillary dermis]({{< relref "papillary_dermis" >}})) and dense ([reticular]({{< relref "reticular_dermis" >}})) irregular connective tissue

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
