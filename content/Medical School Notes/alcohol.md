+++
title = "Alcohol"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Lumen - Functional Group Names, Properties, and Reactions]({{< relref "lumen_functional_group_names_properties_and_reactions" >}}) {#lumen-functional-group-names-properties-and-reactions--lumen-functional-group-names-properties-and-reactions-dot-md}

<!--list-separator-->

-  **🔖 Functional Groups**

    <!--list-separator-->

    -  [Alcohols]({{< relref "alcohol" >}})

        <!--list-separator-->

        -  Structure and Physical Properties

            -   Bent shape
                -   Due to electron repulsion and steric bulk of substituents on central oxygen atom
            -   **Polar**
                -   High electronegativity of oxygen vs. carbon -> shortening and strengthening of -OH bond
            -   Can form hydrogen bonds
                -   Boiling point higher than those of parent molecule because of this
            -   Often undergo deprotonation in presence of strong base
                -   Results in formation of alkoxide salt and water molecule
            -   **Not considered good leaving groups**
                -   Participation in nucleophilic substitution rxns instigated by _protonation of oxygen atom_ -> formation of -OH<sub>2</sub>, a better leaving group


#### [Kaplan Organic Chemistry Chapter 1]({{< relref "kaplan_organic_chemistry_chapter_1" >}}) {#kaplan-organic-chemistry-chapter-1--kaplan-organic-chemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 1.2 Hydrocarbons and Alcohols**

    <!--list-separator-->

    -  [Alcohols]({{< relref "alcohol" >}})

        <!--list-separator-->

        -  Overview

            -   Named by replacing `-e` at the end of the corresponding alkane with the suffix `-ol`
            -   Chain is numbered so that carbon attached to hydroxyl group `(-OH)` gets lowest possible number
            -   Hydroxyl group takes precedence over multiple bonds because of the higher oxidation state of the carbon
            -   If alcohol is not highest-priority group on molecule, it is named as a hydroxyl substituent `(hydroxy-)`

        <!--list-separator-->

        -  Examples

            <_20210712_215858screenshot.png>


### Unlinked references {#unlinked-references}

[Show unlinked references]
