+++
title = "GLUT4 storage vesicle"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [GLUT4 Exocytosis]({{< relref "glut4_exocytosis" >}}) {#glut4-exocytosis--glut4-exocytosis-dot-md}

<!--list-separator-->

-  **🔖 Notes**

    <!--list-separator-->

    -  There is evidence that [GSVs]({{< relref "glut4_storage_vesicles" >}}) directly fuse with the plasma membrane, but this occurs as a transient burst -> further effort needed to confirm whether GLUT-4 continues to recycle back to plasma membrane in GSVs or in endosomes when [insulin]({{< relref "insulin" >}}) is present

<!--list-separator-->

-  **🔖 Notes**

    <!--list-separator-->

    -  [GLUT-4]({{< relref "glut_4" >}}) resides in specialised vesicles called [GSVs]({{< relref "glut4_storage_vesicles" >}}) that undergo insulin-dependent translocation to the plasma membrane

        -   This happens in the absense of [insulin]({{< relref "insulin" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
