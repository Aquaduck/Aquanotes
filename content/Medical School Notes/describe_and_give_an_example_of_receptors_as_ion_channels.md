+++
title = "Describe and give an example of receptors as ion channels."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Receptors as Ion Channels {#receptors-as-ion-channels}

-   Simplest and most primitive signal cascade involving plasma membrane receptors
-   Especially prevalent in excitable membranes
    -   E.g. nerve signal transmission at synapses between nerves, between nerves and muscle -> contraction
-   The prototypic example is the **[acetylcholine]({{< relref "acetylcholine" >}}) (ACh) receptor**


### Synapses between neurons - [acetylcholine]({{< relref "acetylcholine" >}}) {#synapses-between-neurons-acetylcholine--acetylcholine-dot-md}

-   Major neurotransmitter in the CNS and ANS
-   Major role is to **open sodium channels in the membrane -> depolarization**
-   Motor neurons cause muscle contraction via th eopening of acetylcholine receptors at the neuromuscular junction


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe and give an example of receptors as ion channels.]({{< relref "describe_and_give_an_example_of_receptors_as_ion_channels" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
