+++
title = "Transverse pericardial sinus"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the clinical significance of the transverse pericardial sinus.]({{< relref "describe_the_clinical_significance_of_the_transverse_pericardial_sinus" >}}) {#describe-the-clinical-significance-of-the-transverse-pericardial-sinus-dot--describe-the-clinical-significance-of-the-transverse-pericardial-sinus-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  Surgical significance of the [transverse pericardial sinus]({{< relref "transverse_pericardial_sinus" >}})

        -   After pericardial sac is opened anteriorly, a finger can be passed through the transverse pericardial sinus posterior to ascending aorta and pulmonary trunk
        -   By passing a surgical clamp or a ligature around these large vessels, inserting tubes of a coronary bypass machine, and then tightening ligature, surgeons can **stop or divert circulation of blood in these arteries while performing cardiac surgery**


### Unlinked references {#unlinked-references}

[Show unlinked references]
