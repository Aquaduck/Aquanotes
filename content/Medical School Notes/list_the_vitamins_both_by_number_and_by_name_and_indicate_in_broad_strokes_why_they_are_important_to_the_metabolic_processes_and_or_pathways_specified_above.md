+++
title = "List the vitamins, both by number and by name, and indicate, in broad strokes, why they are important to the metabolic processes and/or pathways specified above"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## [B1]({{< relref "thiamine" >}}) {#b1--thiamine-dot-md}

-   Required by PDH complex and alpha-KG


## [B2]({{< relref "riboflavin" >}}) {#b2--riboflavin-dot-md}

-   FAD
-   Redox potential
-   PDH complex/alpha-KG


## [B3]({{< relref "niacin" >}}) {#b3--niacin-dot-md}

-   NAD+
-   Redox
-   PDH complex, alpha-KG, many more


## [B5]({{< relref "pantothenic_acid" >}}) {#b5--pantothenic-acid-dot-md}

-   CoA


## [B7]({{< relref "biotin" >}}) {#b7--biotin-dot-md}

-   Required by carboxylases


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the vitamins, both by number and by name, and indicate, in broad strokes, why they are important to the metabolic processes and/or pathways specified above]({{< relref "list_the_vitamins_both_by_number_and_by_name_and_indicate_in_broad_strokes_why_they_are_important_to_the_metabolic_processes_and_or_pathways_specified_above" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
