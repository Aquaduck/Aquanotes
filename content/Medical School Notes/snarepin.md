+++
title = "SNAREpin"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe how vesicles move, recognize, and fuse with the target compartment.]({{< relref "describe_how_vesicles_move_recognize_and_fuse_with_the_target_compartment" >}}) {#describe-how-vesicles-move-recognize-and-fuse-with-the-target-compartment-dot--describe-how-vesicles-move-recognize-and-fuse-with-the-target-compartment-dot-md}

<!--list-separator-->

-  **🔖 From Fusion of Cells by Flipped SNARES**

    The spontaneous formation of the SNARE complex is coupled to [SNAREpins]({{< relref "snarepin" >}}) to promote fusion of the lipid bilayers to the target membrane

    ---

<!--list-separator-->

-  **🔖 From Fusion of Cells by Flipped SNARES**

    [SNAREpins]({{< relref "snarepin" >}}) consist of a bundle of four helices

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
