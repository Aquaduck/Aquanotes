+++
title = "Emphysema"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Respiratory Acidosis: Primary Carbonic Acid/Carbon Dioxide Excess**

    Can result from anything that interferes with respiration, e.g. [pneumonia]({{< relref "pneumonia" >}}), [emphysema]({{< relref "emphysema" >}}), or [congestive heart failure]({{< relref "congestive_heart_failure" >}})

    ---


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Findings of obstructive vs. restrictive lung disease**

    |                 |                                                                      |                                                     |
    |-----------------|----------------------------------------------------------------------|-----------------------------------------------------|
    | Lung compliance | Normal (may be increased in [emphysema]({{< relref "emphysema" >}})) | Normal (extrinsic causes) or low (intrinsic causes) |

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
