+++
title = "Define homologous receptor regulation, its physiologic significance and the mechanisms proposed to explain the process."
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

from [Homologous receptor regulation]({{< relref "cell_signalling_iii_underlying_design_features" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-31 Tue] </span></span> > Cell Signaling III > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Define homologous receptor regulation, its physiologic significance and the mechanisms proposed to explain the process.]({{< relref "define_homologous_receptor_regulation_its_physiologic_significance_and_the_mechanisms_proposed_to_explain_the_process" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
