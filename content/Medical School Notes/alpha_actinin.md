+++
title = "Alpha-actinin"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   An accessory protein for [actin]({{< relref "actin" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Provide examples of how actin-binding proteins regulate actin filament function.]({{< relref "provide_examples_of_how_actin_binding_proteins_regulate_actin_filament_function" >}}) {#provide-examples-of-how-actin-binding-proteins-regulate-actin-filament-function-dot--provide-examples-of-how-actin-binding-proteins-regulate-actin-filament-function-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Higher-order actin filament arrays influence cellular mechanical properties and signaling > Bundling proteins**

    <!--list-separator-->

    -  [Alpha-actinin]({{< relref "alpha_actinin" >}})

        {{< figure src="/home/kita/Documents/logseq-org-roam-wiki/hugo/content-org/medschool/_20210914_191459screenshot.png" >}}

        -   Forms a contractile bundle
            -   Loose packing allows myosin II to enter bundle


### Unlinked references {#unlinked-references}

[Show unlinked references]
