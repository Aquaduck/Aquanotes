+++
title = "Pleura"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A serous membrane that surrounds the [lung]({{< relref "lung" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Visceral pleura]({{< relref "visceral_pleura" >}}) {#visceral-pleura--visceral-pleura-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [Pleura]({{< relref "pleura" >}}) that is superficial to the lungs, extending into and lining the lung fissures

    ---


#### [Pleural fluid]({{< relref "pleural_fluid" >}}) {#pleural-fluid--pleural-fluid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Secreted by [mesothelial cells]({{< relref "mesothelial_cell" >}}) to lubricate the surfaces of the two layers of the [Pleura]({{< relref "pleura" >}})

    ---


#### [Parietal pleura]({{< relref "parietal_pleura" >}}) {#parietal-pleura--parietal-pleura-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [Pleura]({{< relref "pleura" >}})

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  [Pleura]({{< relref "pleura" >}}) of the lung (p. 1057)

    -   Each lung enclosed within a cavity surrounded by the pleura
    -   _Pleura_: a serous membrane that surrounds the lung
        -   [Visceral pleura]({{< relref "visceral_pleura" >}}): the layer superficial to the lungs, extending into and lining the lung fissures
        -   [Parietal pleura]({{< relref "parietal_pleura" >}}): Outer layer that connects the thoracic wall, the mediastinum, and the diaphragm
        -   Visceral and parietal pleura connect to each other at the [hilum of the lung]({{< relref "hilum_of_the_lung" >}})
        -   [Pleural cavity]({{< relref "pleural_cavity" >}}): space between the visceral and parietal layers

    {{< figure src="/ox-hugo/_20211016_201442screenshot.png" caption="Figure 1: Parietal and visceral pleurae of the lungs" >}}

    -   **Two major functions of the pleura:**
        1.  Produce pleural fluid
        2.  Create cavities to separate the major organs
    -   [Pleural fluid]({{< relref "pleural_fluid" >}}): lubricates the surfaces of the two pleural layers
        -   Secreted by mesothelial cells
        -   Both reduces physical trauma from breathing and helps maintain the position of the lungs against the thoracic wall
        -   Also helps prevent the spread of infection


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Restrictive lung disease > Etiology**

    Diseases of [pleura]({{< relref "pleura" >}}) and [pleural cavity]({{< relref "pleural_cavity" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
