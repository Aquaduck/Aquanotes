+++
title = "Adaptive immunity"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [T lymphocyte]({{< relref "t_lymphocyte" >}}) {#t-lymphocyte--t-lymphocyte-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    One of two [lymphocytes]({{< relref "lymphocyte" >}}) as part of the [adaptive immune system]({{< relref "adaptive_immunity" >}})

    ---


#### [Lymph node]({{< relref "lymph_node" >}}) {#lymph-node--lymph-node-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The major site of activation of the [adaptive immune system]({{< relref "adaptive_immunity" >}})

    ---


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Two arms of the immune system**

    <!--list-separator-->

    -  [Adaptive system]({{< relref "adaptive_immunity" >}})

        -   Generates two families of receptors with unique antigen specifities through **DNA mutation** (adaption\*)
            -   **Greater specificity and efficacy** than the recepters of the [innate system]({{< relref "innate_immunity" >}})
        -   The cells (B and T lymphocytes) have **memory** that allows a more rapid and effective response to **re-infection**


#### [B lymphocyte]({{< relref "b_lymphocyte" >}}) {#b-lymphocyte--b-lymphocyte-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    One of two [lymphocytes]({{< relref "lymphocyte" >}}) as part of the [adaptive immune system]({{< relref "adaptive_immunity" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
