+++
title = "Dermis"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Reticular dermis]({{< relref "reticular_dermis" >}}) {#reticular-dermis--reticular-dermis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The deep layer of the [dermis]({{< relref "dermis" >}}) made of dense irregular CT

    ---


#### [Papillary dermis]({{< relref "papillary_dermis" >}}) {#papillary-dermis--papillary-dermis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The more superficial layer of the [dermis]({{< relref "dermis" >}}) made up of loose CT

    ---


#### [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}}) {#know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot--know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Overview of Basic Skin > 3 layers**

    <!--list-separator-->

    -  [Dermis]({{< relref "dermis" >}})

        -   Loose ([papillary dermis]({{< relref "papillary_dermis" >}})) and dense ([reticular]({{< relref "reticular_dermis" >}})) irregular connective tissue
        -   Elastin fibers
        -   Rugged foundation for epidermis
        -   Provides strength and elasticity

        {{< figure src="/ox-hugo/_20210921_161941screenshot.png" caption="Figure 1: Papillary dermis directly under the epidermis" >}}

        {{< figure src="/ox-hugo/_20210921_162120screenshot.png" caption="Figure 2: Reticular dermis directly underneath the papillary dermis" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
