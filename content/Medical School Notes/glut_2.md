+++
title = "GLUT-2"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Overview of the most important glucose transporters]({{< relref "overview_of_the_most_important_glucose_transporters" >}}) {#overview-of-the-most-important-glucose-transporters--overview-of-the-most-important-glucose-transporters-dot-md}

|                                  |             |                                                                                        |    |
|----------------------------------|-------------|----------------------------------------------------------------------------------------|----|
| [GLUT2]({{< relref "glut_2" >}}) | Hepatocytes | Transports all monosaccharides from the basolateral membrane of enterocytes into blood | No |

---


#### [Lightyear: Infinity notes]({{< relref "lightyear_infinity_notes" >}}) {#lightyear-infinity-notes--lightyear-infinity-notes-dot-md}

<!--list-separator-->

-  **🔖 Biochemistry**

    <!--list-separator-->

    -  The [GLUT-2]({{< relref "glut_2" >}}) transporter is bidirectional

        -   Functions as a **glucose sensor** - high capacity but low affinity (Km)
        -   Insulin-**independent**


#### [Distinguish the insulin-responsive glucose membrane transporter and in what tissues it is located, from that used by:]({{< relref "distinguish_the_insulin_responsive_glucose_membrane_transporter_and_in_what_tissues_it_is_located_from_that_used_by" >}}) {#distinguish-the-insulin-responsive-glucose-membrane-transporter-and-in-what-tissues-it-is-located-from-that-used-by--distinguish-the-insulin-responsive-glucose-membrane-transporter-and-in-what-tissues-it-is-located-from-that-used-by-dot-md}

<!--list-separator-->

-  **🔖 Liver/Pancreas**

    Uses [GLUT-2]({{< relref "glut_2" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
