+++
title = "Left coronary artery"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}}) {#describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot--describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    Total occlusion of [left coronary artery]({{< relref "left_coronary_artery" >}}) is **usually fatal**

    ---


#### [Circumflex artery]({{< relref "circumflex_artery" >}}) {#circumflex-artery--circumflex-artery-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Branch of the [left coronary artery]({{< relref "left_coronary_artery" >}})

    ---


#### [Anterior interventricular artery]({{< relref "anterior_interventricular_artery" >}}) {#anterior-interventricular-artery--anterior-interventricular-artery-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Branch of the [Left coronary artery]({{< relref "left_coronary_artery" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
