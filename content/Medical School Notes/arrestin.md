+++
title = "Arrestin"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important.]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}}) {#describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot--describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Off Signals > Several pathways in the cell to turn off a signal: > Inactivate the receptor**

    Creates "docking sites" -> permits binding of [arrestin]({{< relref "arrestin" >}}) -> inhibits receptor

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
