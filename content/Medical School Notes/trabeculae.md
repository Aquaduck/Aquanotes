+++
title = "Trabeculae"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Spongy bone]({{< relref "spongy_bone" >}}) {#spongy-bone--spongy-bone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Porous [bone]({{< relref "bone" >}}) composed of thin [trabeculae]({{< relref "trabeculae" >}})

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Histologic components of mature bone**

    Lamellae of bone are also laid down on the [Trabeculae]({{< relref "trabeculae" >}}) of [Cancellous bone]({{< relref "spongy_bone" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Organization of bones**

    Composed of thin _[trabeculae]({{< relref "trabeculae" >}})_ of bone

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
