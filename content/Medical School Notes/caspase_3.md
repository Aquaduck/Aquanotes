+++
title = "Caspase-3"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The predominant caspase involved in the cleavage of [amyloid-beta 4A precursor protein]({{< relref "amyloid_beta_4a_precursor_protein" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Caspase-8]({{< relref "caspase_8" >}}) {#caspase-8--caspase-8-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Cleaves and activates [Caspase-3]({{< relref "caspase_3" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
