+++
title = "Describe the events required for packaging dietary fat into chylomicrons."
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}


#### Pathway of triacylglycerol breakdown {#pathway-of-triacylglycerol-breakdown}

-   Absorption of dietary fats is dependent on the **presence of amphipathic bile salts** -> creation of small _micelles_ of hydrophobic fat
-   Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by _enterocytes_ (cells of the intestinal lining)
    -   Digested triacylglycerol (except for short- and medium-chain fatty acids) are **resynthesized in the ER** along with phospholipids and cholesterol esters
        -   Short- and medium-chain fatty acids **diffuse directly across the enterocyte into portal capillaries**
-   Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
    -   Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB-48
        -   Molecules of apoCII and apoCIII are also integrated
-   The formed chylomicrons are then **exocytosed into the lymphatics** -> **enter venous circulation**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-25 Mon] </span></span> > Muscle Intermediary Metabolism II > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the events required for packaging dietary fat into chylomicrons.]({{< relref "describe_the_events_required_for_packaging_dietary_fat_into_chylomicrons" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
