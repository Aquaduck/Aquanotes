+++
title = "Vital capacity"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The maximum volume of air that can be expired after maximal [inspiration]({{< relref "inspiration" >}})


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Total lung capacity]({{< relref "total_lung_capacity" >}}) {#total-lung-capacity--total-lung-capacity-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Addition of [Vital capacity]({{< relref "vital_capacity" >}}) and [residual volume]({{< relref "residual_volume" >}})

    ---


#### [Inspiratory vital capacity]({{< relref "inspiratory_vital_capacity" >}}) {#inspiratory-vital-capacity--inspiratory-vital-capacity-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Can be a way to measure [VC]({{< relref "vital_capacity" >}})

    ---


#### [Forced vital capacity]({{< relref "forced_vital_capacity" >}}) {#forced-vital-capacity--forced-vital-capacity-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Can be a way to measure [VC]({{< relref "vital_capacity" >}})

    ---


#### [Expiratory vital capacity]({{< relref "expiratory_vital_capacity" >}}) {#expiratory-vital-capacity--expiratory-vital-capacity-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Can be a way to measure [VC]({{< relref "vital_capacity" >}})

    ---


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Findings of obstructive vs. restrictive lung disease**

    |                                       |     |     |
    |---------------------------------------|-----|-----|
    | [VC]({{< relref "vital_capacity" >}}) | Low | Low |

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration**

    <!--list-separator-->

    -  [Vital capacity]({{< relref "vital_capacity" >}})

        -   The maximum volume of air that can be expired **after maximal inspiration**

        <!--list-separator-->

        -  [Inspiratory vital capacity]({{< relref "inspiratory_vital_capacity" >}}) (IVC)

            -   Maximum volume of air that can be **inspired after maximal expiration**

        <!--list-separator-->

        -  [Expiratory vital capacity]({{< relref "expiratory_vital_capacity" >}}) (EVC)

            -   Maximum volume of air that can be **expired after maximal inspiration**

        <!--list-separator-->

        -  [Forced vital capacity]({{< relref "forced_vital_capacity" >}}) (FVC)

            -   Maximum volume of air that can be **forcefully expired** after maximal inspiration

<!--list-separator-->

-  **🔖 Respiration > Total lung capacity**

    [RV]({{< relref "residual_volume" >}}) + [VC]({{< relref "vital_capacity" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
