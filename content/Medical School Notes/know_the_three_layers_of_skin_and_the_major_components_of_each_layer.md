+++
title = "Know the three layers of skin and the major components of each layer."
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Histology - Skin Pre-session Powerpoint]({{< relref "histology_skin_pre_session_powerpoint" >}}) {#from-histology-skin-pre-session-powerpoint--histology-skin-pre-session-powerpoint-dot-md}


### Overview of Basic Skin {#overview-of-basic-skin}


#### 3 layers {#3-layers}

<!--list-separator-->

-  [Epidermis]({{< relref "epidermis" >}})

    <!--list-separator-->

    -  Overview

        -   Keratinized **stratified squamous** epithelium
        -   Protects from UV damage
        -   Waterproof

        {{< figure src="/ox-hugo/_20210921_155711screenshot.png" width="800" >}}

    <!--list-separator-->

    -  Components

        -   Contain [keratinocytes]({{< relref "keratinocyte" >}}) - cells that make large amounts of [keratin]({{< relref "keratin" >}})
            -   Held together by [desmosomes]({{< relref "desmosome" >}})
                -   Membrane proteins responsible for cell-to-cell adhesion
                -   Keep the skin from falling apart
                -   The reason why skin falls off in sheets (e.g. sunburn)
        -   Rests on a [basement membrane]({{< relref "basement_membrane" >}})
            -   Bottom layer of keratinocytes attached by [hemidesmosomes]({{< relref "hemidesmosome" >}})
                -   Membrane proteins that adhere cells to the basement membrane

<!--list-separator-->

-  [Dermis]({{< relref "dermis" >}})

    -   Loose ([papillary dermis]({{< relref "papillary_dermis" >}})) and dense ([reticular]({{< relref "reticular_dermis" >}})) irregular connective tissue
    -   Elastin fibers
    -   Rugged foundation for epidermis
    -   Provides strength and elasticity

    {{< figure src="/ox-hugo/_20210921_161941screenshot.png" caption="Figure 1: Papillary dermis directly under the epidermis" >}}

    {{< figure src="/ox-hugo/_20210921_162120screenshot.png" caption="Figure 2: Reticular dermis directly underneath the papillary dermis" >}}

<!--list-separator-->

-  [Subcutis]({{< relref "subcutis" >}})

    -   Mostly fat with some connective tissue
    -   Provides cushioning and mobility
    -   Energy storage (fat)

    {{< figure src="/ox-hugo/_20210921_162537screenshot.png" caption="Figure 3: Subcutis directly under reticular dermis. Made mostly of adipose cells (white bubbles)" >}}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-21 Tue] </span></span> > Histology: Skin Workshop > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
