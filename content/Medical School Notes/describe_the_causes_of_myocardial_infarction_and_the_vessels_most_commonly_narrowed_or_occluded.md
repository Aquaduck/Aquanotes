+++
title = "Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}

-   When a coronary artery becomes stenotic (narrowed) or occluded (closed off) -> region of heart supplied by that artery as [ischemia]({{< relref "ischemia" >}}) (lack of oxygen)
-   Severe enough ischemia + lasts long enough -> [myocardial infarction]({{< relref "myocardial_infarction" >}})
    -   If patient survives -> ECG changes and appearance of myocardial enzymes in peripheral blood
-   [Right ventricle]({{< relref "right_ventricle" >}}) + [right atria]({{< relref "right_atrium" >}}) do not develop as much pressure -> less oxygen demand -> more rarely infarcted
-   In general, occlusion of the [anterior interventricular artery]({{< relref "anterior_interventricular_artery" >}}) -> infarction of anteroapical portion of [left ventricle]({{< relref "left_ventricle" >}})
-   Occlusion of [circumflex artery]({{< relref "circumflex_artery" >}}) -> infarction of posterolateral wall of left ventricle
-   Total occlusion of [left coronary artery]({{< relref "left_coronary_artery" >}}) is **usually fatal**
-   Occlusion of [right coronary artery]({{< relref "right_coronary_artery" >}}) -> infarct to diaphgragmatic surface of right and left ventricles
-   **If left coronary artery is dominant** -> right coronary artery occlusion **does not usually result in infarction**


### [Atherosclerosis]({{< relref "atherosclerosis" >}}) and [MI]({{< relref "myocardial_infarction" >}}) {#atherosclerosis--atherosclerosis-dot-md--and-mi--myocardial-infarction-dot-md}

-   Atherosclerotic process: lipid deposits in tunica intima (innermost vascular lining) of the [coronary arteries]({{< relref "coronary_artery" >}})
-   Begins in early adulthood -> slowly results in stenosis of lumina of arteries
-   Collateral channels connecting one coronary artery with the other expand -> initially permits adequate perfusion during relative inactivity
    -   Not enough when heart needs to perform increased amount of work (e.g. strenous exercise)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
