+++
title = "Bradykinin"
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Fibrinolysis (p. 818)**

    [Bradykinin]({{< relref "bradykinin" >}}) is released -> vasodilation -> reverses the effects of serotonin and prostaglandins from platelets

    ---


#### [Endothelin]({{< relref "endothelin" >}}) {#endothelin--endothelin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Bradykinin]({{< relref "bradykinin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
