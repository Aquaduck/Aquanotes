+++
title = "Extrinsic pathway of apoptosis"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   One pathway of [apoptosis]({{< relref "apoptosis" >}})


## Backlinks {#backlinks}


### 9 linked references {#9-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026) > BH3-only proteins > Bid**

    When [death receptors]({{< relref "death_receptor" >}}) activate the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}), [Caspase-8]({{< relref "caspase_8" >}}) cleaves Bid -> activates Bid -> translocates to outer mitochondrial membrane -> inhibits anti-apoptotic Bcl2 family proteins -> **amplification of death signal**

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > FLIP**

    <!--list-separator-->

    -  Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024)

        -   FLIP is an inhibitory protein that **restrains the extrinsic pathway** -> inhibits inappropriate activation of apoptosis
        -   FLIP resembles an initiator caspase **but has no protease activity** because it lacks a key cysteine in its active site
        -   FLIP dimerizes with [caspase-8]({{< relref "caspase_8" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})
            -   While caspase-8 appears to be active, it is not cleaved at the site required for its stable activation -> blockage of apoptotic signal

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Fas**

    <!--list-separator-->

    -  Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024)

        -   A well-understood example of how death receptors trigger the extrinsic pathway of apoptosis is the activation of Fas on the surface of a target cell by Fas ligand on the surface of a killer (cytotoxic) lymphocyte
            -   Fas activated by [Fas ligand]({{< relref "fas_ligand" >}}) -> death domains on Fas death receptors bind intracellular adaptor proteins -> bind [initiator caspases]({{< relref "initiator_caspase" >}}) (primarily [caspase-8]({{< relref "caspase_8" >}})) -> formation of a [death-inducing signaling complex]({{< relref "death_inducing_signaling_complex" >}}) (DISC)
            -   Once dimerized + activated in [DISC]({{< relref "death_inducing_signaling_complex" >}}), [initiator caspases]({{< relref "initiator_caspase" >}}) cleave their partners -> activate downstreain [executioner caspases]({{< relref "executioner_caspase" >}}) to induce [apoptosis]({{< relref "apoptosis" >}})

        {{< figure src="/ox-hugo/_20210926_143723screenshot.png" >}}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Death receptors**

    <!--list-separator-->

    -  Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024)

        -   Death receptors are transmembrane proteins containing:
            -   An extracellular ligand-binding domain
            -   A single transmembrane domain
            -   An intracellular [death domain]({{< relref "death_domain" >}}) -> required for receptor to activate [apoptosis]({{< relref "apoptosis" >}})
        -   Death receptors are homotrimers and belong to the [tumor necrosis factor]({{< relref "tumor_necrosis_factor_receptor" >}}) (TNF) **receptor** family
            -   The ligands that activate death receptors are also homotrimers and are part of the [TNF]({{< relref "tumor_necrosis_factor" >}}) family of **signal proteins**


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-27 Mon] </span></span> > Cell Death > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the extrinsic pathways of apoptosis and the stimuli leading to this process]({{< relref "extrinsic_pathway_of_apoptosis" >}})


#### [FLICE-inhibitor protein]({{< relref "flice_inhibitor_protein" >}}) {#flice-inhibitor-protein--flice-inhibitor-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Restrains [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}) by inhibiting [FLICE]({{< relref "fadd_like_interleukin_1b_converting_enzyme" >}})

    ---


#### [Death receptor]({{< relref "death_receptor" >}}) {#death-receptor--death-receptor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    "[Death domains]({{< relref "death_domain" >}})" (characteristic feature of DRs) activate [caspases]({{< relref "caspase" >}}) as part of the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}})

    ---


#### [Caspase]({{< relref "caspase" >}}) {#caspase--caspase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Involved in both [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}) and [Intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}})

    ---


#### [Bid]({{< relref "bid" >}}) {#bid--bid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Links the [intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}}) and [extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
