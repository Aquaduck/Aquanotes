+++
title = "Beclin 1"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 HIF-1 target genes involved in glucose and energy metabolism**

    HIF-1 activates transcription of the gene encoding the [BH3-only protein]({{< relref "bh3_only_protein" >}}) BNIP3 -> induces selective mitochondrial autophagy by competing with [Beclin 1]({{< relref "beclin_1" >}}) for binding to [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) -> allows Beclin 1 to trigger autophagy

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
