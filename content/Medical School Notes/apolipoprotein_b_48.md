+++
title = "Apolipoprotein B-48"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The structural protein of [chylomicrons]({{< relref "chylomicron" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 apoB48 & apoCII**

    <!--list-separator-->

    -  [ApoB48]({{< relref "apolipoprotein_b_48" >}})

        <!--list-separator-->

        -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

            -   Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
                -   Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB48
            -   **The structural protein of [chylomicrons]({{< relref "chylomicron" >}})**

        <!--list-separator-->

        -  From [Amboss]({{< relref "amboss" >}})

            -   Mediates the secretion of [Chylomicron]({{< relref "chylomicron" >}}) particles that originate from the intestine into the lymphatics
            -   A component of [chylomicrons]({{< relref "chylomicron" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
