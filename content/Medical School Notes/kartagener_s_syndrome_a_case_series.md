+++
title = "Kartagener's syndrome: A case series"
author = ["Arif Ahsan"]
date = 2021-09-15T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Primary ciliary dyskinesia]({{< relref "primary_ciliary_dyskinesia" >}}) {#primary-ciliary-dyskinesia--primary-ciliary-dyskinesia-dot-md}

<!--list-separator-->

-  From [Kartagener's syndrome: A case series]({{< relref "kartagener_s_syndrome_a_case_series" >}})

    <!--list-separator-->

    -  Discussion

        -   PCD is a phenotypically and genetically heterogenous condition
        -   The primary defect is in the **ultrastructure or function of cilia**
            -   Involve the outer [dynein]({{< relref "dynein" >}}) arms, inner dynein arms, or both


#### [Kartagener syndrome]({{< relref "kartagener_syndrome" >}}) {#kartagener-syndrome--kartagener-syndrome-dot-md}

<!--list-separator-->

-  From [Kartagener's syndrome: A case series]({{< relref "kartagener_s_syndrome_a_case_series" >}})

    <!--list-separator-->

    -  Introduction

        -   **Kartagener's syndrome (KS)** is a subset of a larger group of ciliary motility disorders called [primary ciliary dyskinesias]({{< relref "primary_ciliary_dyskinesia" >}}) (PCDs)
            -   PCD w/ [situs inversus]({{< relref "situs_inversus" >}}) = Kartagener's syndrome
        -   Autosomal recessive inheritance
        -   Comprises the **triad** of situs inversus, bronchiectasis, and sinusitis

    <!--list-separator-->

    -  Discussion


### Unlinked references {#unlinked-references}

[Show unlinked references]
