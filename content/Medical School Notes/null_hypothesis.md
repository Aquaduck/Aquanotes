+++
title = "Null hypothesis"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Statistical power]({{< relref "statistical_power" >}}) {#statistical-power--statistical-power-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The probability of correctly rejecting the [null hypothesis]({{< relref "null_hypothesis" >}})

    ---


#### [Explain the meaning of a p-value less than 0.05]({{< relref "explain_the_meaning_of_a_p_value_less_than_0_05" >}}) {#explain-the-meaning-of-a-p-value-less-than-0-dot-05--explain-the-meaning-of-a-p-value-less-than-0-05-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > P-value**

    **It is not possible to prove [H<sub>1</sub>]({{< relref "alternative_hypothesis" >}}) true**, but having a p-value lower than [significance level]({{< relref "significance_level" >}}) -> very unlikely that [H<sub>0</sub>]({{< relref "null_hypothesis" >}}) is correct

    ---


#### [Define statistical power and explain how it affects the interpretation of negative results]({{< relref "define_statistical_power_and_explain_how_it_affects_the_interpretation_of_negative_results" >}}) {#define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results--define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Statistical power**

    The probability of correctly rejecting the [null hypothesis]({{< relref "null_hypothesis" >}})

    ---


#### [Compare and contrast the null hypothesis and the alternative hypothesis]({{< relref "compare_and_contrast_the_null_hypothesis_and_the_alternative_hypothesis" >}}) {#compare-and-contrast-the-null-hypothesis-and-the-alternative-hypothesis--compare-and-contrast-the-null-hypothesis-and-the-alternative-hypothesis-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Null hypothesis]({{< relref "null_hypothesis" >}}) (H<sub>0</sub>)

        -   The assumption that there **is no relationship** between the two measured variables or **no significant difference** between two studied populations
        -   Statistical tests are used to either **reject** or **accept** this hypothesis


#### [Alternative hypothesis]({{< relref "alternative_hypothesis" >}}) {#alternative-hypothesis--alternative-hypothesis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Counterpart to the [null hypothesis]({{< relref "null_hypothesis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
