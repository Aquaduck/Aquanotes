+++
title = "Isocitrate dehydrogenase"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Rate-limiting and commited step of the [TCA cycle]({{< relref "citric_acid_cycle" >}})
-   Linked to [NAD<sup>+</sup>]({{< relref "nadh" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle**

    <!--list-separator-->

    -  NAD<sup>+</sup>-linked [isocitrate dehydrogenase]({{< relref "isocitrate_dehydrogenase" >}})

        -   Catalyzes the **rate limiting and committed step of the TCA cycle**
        -   Allosterically regulated by the _energy charge_ of the cell
            -   ATP inhibits
            -   ADP stimulates
        -   [ADP]({{< relref "adenosine_diphosphate" >}}) is the master energy sensor in the mitochondria
            -   Due to the absence of the appropriate adenylate cyclase isozyme
            -   **↑[ADP] signals an energy deficit**
                -   Because ADP is the substrate of ATP synthesis via ATP synthase and oxidative phosphorylation
            -   [AMP]({{< relref "adenosine_monophosphate" >}}) is master switch in the [cytoplasm]({{< relref "cytoplasm" >}})


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By enzyme**

    <!--list-separator-->

    -  [Isocitrate DH]({{< relref "isocitrate_dehydrogenase" >}})

        -   Niacin (B3) in the form of NAD<sup>+</sup>


### Unlinked references {#unlinked-references}

[Show unlinked references]
