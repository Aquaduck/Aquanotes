+++
title = "Dermatome"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [List the two landmarks for approximating dermatome levels in the thorax.]({{< relref "list_the_two_landmarks_for_approximating_dermatome_levels_in_the_thorax" >}}) {#list-the-two-landmarks-for-approximating-dermatome-levels-in-the-thorax-dot--list-the-two-landmarks-for-approximating-dermatome-levels-in-the-thorax-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Dermatome]({{< relref "dermatome" >}})

        -   A _dermatome_ is an area of skin innervated by a single pair of spinal nerves
        -   In thorax, skin overlies an intercostal space
            -   _Dorsal ramus_ of a spinal nerve innervates the skin over a small regoin of the back
            -   _Ventral ramus_ innervates rest of the dermatome all the way to midline anteriorly
        -   Cell bodies of all sensory axons for one dermatome are in the _dorsal root ganglia_ for that spinal level
        -   **Two landmarks are useful for approximating dermatome levels in the thorax:**
            1.  Nipple = T4
            2.  Xiphoid process = T6


#### [Describe the etiology of herpes zoster as well as the clinical presentation of the disease.]({{< relref "describe_the_etiology_of_herpes_zoster_as_well_as_the_clinical_presentation_of_the_disease" >}}) {#describe-the-etiology-of-herpes-zoster-as-well-as-the-clinical-presentation-of-the-disease-dot--describe-the-etiology-of-herpes-zoster-as-well-as-the-clinical-presentation-of-the-disease-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Herpes zoster**

    After invading a ganglion, virus produces a sharp burning pain in [Dermatome]({{< relref "dermatome" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
