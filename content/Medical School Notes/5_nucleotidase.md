+++
title = "5'-nucleotidase"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    [IMP]({{< relref "inosine_monophosphate" >}}) and [GMP]({{< relref "guanosine_monophosphate" >}}) are converted into their nucleoside forms (inosine and guanosine) by the action of [5'-nucleotidase]({{< relref "5_nucleotidase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
