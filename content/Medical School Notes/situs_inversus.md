+++
title = "Situs inversus"
author = ["Arif Ahsan"]
date = 2021-09-15T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kartagener syndrome]({{< relref "kartagener_syndrome" >}}) {#kartagener-syndrome--kartagener-syndrome-dot-md}

<!--list-separator-->

-  **🔖 From Kartagener's syndrome: A case series > Introduction**

    PCD w/ [situs inversus]({{< relref "situs_inversus" >}}) = Kartagener's syndrome

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
