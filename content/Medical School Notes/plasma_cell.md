+++
title = "Plasma cell"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A mature [B-lymphocyte]({{< relref "b_lymphocyte" >}}) that produces [antibodies]({{< relref "immunoglobulin" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Gamma globulin]({{< relref "gamma_globulin" >}}) {#gamma-globulin--gamma-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Produced by [plasma cells]({{< relref "plasma_cell" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
