+++
title = "Osteoid"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Complete protein matrix of [Bone]({{< relref "bone" >}})


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Osteoclast]({{< relref "osteoclast" >}}) {#osteoclast--osteoclast-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Resorbs [Bone]({{< relref "bone" >}}) and digests the [Osteoid]({{< relref "osteoid" >}})

    ---


#### [Osteoblast]({{< relref "osteoblast" >}}) {#osteoblast--osteoblast-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Lay down new [Osteoid]({{< relref "osteoid" >}}) and calcify it to form new [Bone]({{< relref "bone" >}})

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    Blood vessels remain in the middle of the concentric rings of [Osteoid]({{< relref "osteoid" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    A wave of [osteoblasts]({{< relref "osteoblast" >}}) follow the [osteoclasts]({{< relref "osteoclast" >}}) -> lay down concentric rings of [Osteoid]({{< relref "osteoid" >}}) -> osteoblasts differentiate into [osteocytes]({{< relref "osteocyte" >}}) -> encased in bone by the next wave of osteoblasts

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone microscopic morphology**

    [Osteoid]({{< relref "osteoid" >}}) appears dense and eosinophilic

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Histologic components of mature bone**

    Complete protein matrix of bone is called the [osteoid]({{< relref "osteoid" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
