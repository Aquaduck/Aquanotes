+++
title = "Describe the presentation of pneumothorax, hydrothorax and hemothorax."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### [Pneumothorax]({{< relref "pneumothorax" >}}) {#pneumothorax--pneumothorax-dot-md}

-   Entry of air into pleural cavity
-   Usually from penetrating wound of parietal pleura
-   Fractured ribs may also tear visceral pleural and lung


### [Hydrothorax]({{< relref "hydrothorax" >}}) {#hydrothorax--hydrothorax-dot-md}

-   Accumulation of a significant amount of fluid in pleural cavity
-   Can result from pleural effusion


### [Hemothorax]({{< relref "hemothorax" >}}) {#hemothorax--hemothorax-dot-md}

-   Blood enters pleural cavity
-   Results more commonly from injury to a major intercostal or internal thoracic vessel than from laceration of a lung


### [Hemopneumothorax]({{< relref "hemopneumothorax" >}}) {#hemopneumothorax--hemopneumothorax-dot-md}

-   Fluid **and** air accumulate in pleural cavity
-   Visible on radiograph as an air-fluid level or interface


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the presentation of pneumothorax, hydrothorax and hemothorax.]({{< relref "describe_the_presentation_of_pneumothorax_hydrothorax_and_hemothorax" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
