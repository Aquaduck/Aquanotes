+++
title = "Dynein"
author = ["Arif Ahsan"]
date = 2021-09-15T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Primary ciliary dyskinesia]({{< relref "primary_ciliary_dyskinesia" >}}) {#primary-ciliary-dyskinesia--primary-ciliary-dyskinesia-dot-md}

<!--list-separator-->

-  **🔖 From Kartagener's syndrome: A case series > Discussion**

    Involve the outer [dynein]({{< relref "dynein" >}}) arms, inner dynein arms, or both

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
