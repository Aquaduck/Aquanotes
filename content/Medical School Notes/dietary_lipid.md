+++
title = "Dietary lipid"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [Lipids]({{< relref "lipid" >}}) used as energy storage


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Lipoprotein lipase > From Fat Metabolism in Muscle & Adipose Tissue**

    The LPL isozyme of adipose tissue has the **largest K<sub>m</sub> -> [adipose tissue's]({{< relref "adipose_cell" >}}) uptake of [dietary fat]({{< relref "dietary_lipid" >}}) is a last resort**

    ---

<!--list-separator-->

-  **🔖 Chylomicrons > From Fat Metabolism in Muscle & Adipose Tissue**

    The **carriers of [dietary lipids]({{< relref "dietary_lipid" >}}) in the [blood]({{< relref "blood" >}})**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues

    ---


#### [Explain why adipose tissue is referred to as a fat depot.]({{< relref "explain_why_adipose_tissue_is_referred_to_as_a_fat_depot" >}}) {#explain-why-adipose-tissue-is-referred-to-as-a-fat-depot-dot--explain-why-adipose-tissue-is-referred-to-as-a-fat-depot-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    The LPL isozyme of adipose tissue has the **largest K<sub>m</sub> -> [adipose tissue's]({{< relref "adipose_cell" >}}) uptake of [dietary fat]({{< relref "dietary_lipid" >}}) is a last resort**

    ---


#### [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}}) {#explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot--explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Delivering dietary fat to extrahepatic tissues via chylomicrons**

    Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues

    ---


#### [Chylomicron]({{< relref "chylomicron" >}}) {#chylomicron--chylomicron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Skeletal muscle]({{< relref "skeletal_muscle" >}}) obtains [dietary fatty acids]({{< relref "dietary_lipid" >}}) from chylomicrons that can be "re-synthesized" into [triacylglycerol]({{< relref "triacylglycerol" >}}) due to the muscle's regular uptake of glucose

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
