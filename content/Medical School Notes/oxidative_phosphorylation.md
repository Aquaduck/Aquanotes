+++
title = "Oxidative Phosphorylation"
author = ["Arif Ahsan"]
date = 2021-07-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   Oxidative Phosphorylation is a [metabolic]({{< relref "metabolism" >}}) process where [ATP]({{< relref "atp" >}}) is produced directly through usage of a proton gradient and electron transfers


## Backlinks {#backlinks}


### 9 linked references {#9-linked-references}


#### [Mitochondrion]({{< relref "mitochondrion" >}}) {#mitochondrion--mitochondrion-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Site of [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}})

    ---


#### [Lippincott Bioenergetics and Oxidative Phosphorylation]({{< relref "lippincott_bioenergetics_and_oxidative_phosphorylation" >}}) {#lippincott-bioenergetics-and-oxidative-phosphorylation--lippincott-bioenergetics-and-oxidative-phosphorylation-dot-md}

<!--list-separator-->

-  **🔖 Electron Transport Chain > Mitochondrion**

    [ETC]({{< relref "oxidative_phosphorylation" >}}) is present in the inner mitochondrial membrane

    ---

<!--list-separator-->

-  [Electron Transport Chain]({{< relref "oxidative_phosphorylation" >}})

    <!--list-separator-->

    -  [Mitochondrion]({{< relref "mitochondrion" >}})

        -   [ETC]({{< relref "oxidative_phosphorylation" >}}) is present in the inner mitochondrial membrane

        <!--list-separator-->

        -  Membranes of the mitochondrion

            -   _Outer membrane_ contains special pores -> freely permeable to most ions and small molecules
            -   _Inner membrane_ **impermeable** to most **small ions** (H<sup>+</sup>, Na<sup>​+</sup>, K<sup>​+</sup>) and **small molecules** (ATP, ADP, pyruvate)

        <!--list-separator-->

        -  Matrix of the mitochondrion

            -   Includes enzymes responsible for:
                -   Oxidation of
                    -   Pyruvate
                    -   Amino acids
                    -   Fatty acids (via \\(\Beta\\)-oxidation)
                -   the TCA cycle
            -   Synthesis of glucose, urea, and heme occur partially here

    <!--list-separator-->

    -  Organization of the electron transport chain

        -   _Inner mitochondrial membrane_ broken up into **five complexes** (I-V)


#### [Kaplan Biochemistry Chapter 10]({{< relref "kaplan_biochemistry_chapter_10" >}}) {#kaplan-biochemistry-chapter-10--kaplan-biochemistry-chapter-10-dot-md}

<!--list-separator-->

-  **🔖 10.2 Reactions of the Citric Acid Cycle > Key Reactions > 6) Fumarate Formation**

    Each molecule of FADH<sub>2</sub> passes electrons to the [ETC]({{< relref "oxidative_phosphorylation" >}}) -> **production of 1.5 ATP**

    ---

<!--list-separator-->

-  **🔖 10.2 Reactions of the Citric Acid Cycle > Key Reactions > 5) Succinate Formation**

    ATP produciton primarily done in within the [ETC]({{< relref "oxidative_phosphorylation" >}})

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Mitochondria > Mitochondrial ATP synthesis**

    ATP also synthesized via [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}})

    ---

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Mitochondria > Enzymes and genetic apparatus**

    Involved in [coupling oxidation to phosphorylation]({{< relref "oxidative_phosphorylation" >}}) of ADP to ATP

    ---


#### [Cytochrome c]({{< relref "cytochrome_c" >}}) {#cytochrome-c--cytochrome-c-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Water-soluble component of the [ETC]({{< relref "oxidative_phosphorylation" >}})

    ---


#### [A Discussion of the Significance of Two Key Metabolic Branchpoints]({{< relref "a_discussion_of_the_significance_of_two_key_metabolic_branchpoints" >}}) {#a-discussion-of-the-significance-of-two-key-metabolic-branchpoints--a-discussion-of-the-significance-of-two-key-metabolic-branchpoints-dot-md}

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > Brain > Fasting state**

    Brain requires a **constant source of glucose for aerobic [glycolysis]({{< relref "glycolysis" >}}), ATP generation via [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}}), and replenishing lost [TCA]({{< relref "citric_acid_cycle" >}}) cycle intermediates**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
