+++
title = "Homeostasis"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Discuss the concept of steady state and the balancing of input and outputs that determines the level of some substance such as blood sugar.]({{< relref "discuss_the_concept_of_steady_state_and_the_balancing_of_input_and_outputs_that_determines_the_level_of_some_substance_such_as_blood_sugar" >}}) {#discuss-the-concept-of-steady-state-and-the-balancing-of-input-and-outputs-that-determines-the-level-of-some-substance-such-as-blood-sugar-dot--discuss-the-concept-of-steady-state-and-the-balancing-of-input-and-outputs-that-determines-the-level-of-some-substance-such-as-blood-sugar-dot-md}

<!--list-separator-->

-  **🔖 From Homeostasis PPT > Steady state**

    [Homeostasis]({{< relref "homeostasis" >}}) means the maintenance of **the right steady state** in the face of changing inputs and outputs

    ---


#### [Describe the history and meaning of the term Homeostasis.]({{< relref "describe_the_history_and_meaning_of_the_term_homeostasis" >}}) {#describe-the-history-and-meaning-of-the-term-homeostasis-dot--describe-the-history-and-meaning-of-the-term-homeostasis-dot-md}

<!--list-separator-->

-  **🔖 From Homeostasis PPT > Walter Cannon**

    Actually invented the term [Homeostasis]({{< relref "homeostasis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
