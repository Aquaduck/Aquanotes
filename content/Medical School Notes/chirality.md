+++
title = "Chirality"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

A **chiral** molecule is a molecule with four unique groups attached to it


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Kaplan Biochemistry Chapter 1]({{< relref "kaplan_biochemistry_chapter_1" >}}) {#kaplan-biochemistry-chapter-1--kaplan-biochemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins > Stereochemistry of AAs**

    For most [AAs]({{< relref "amino_acid" >}}), the ɑ-carbon is a [chiral]({{< relref "chirality" >}}) (or stereogenic) center

    ---


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Organic Chemistry > 3. Differentiate chiral and achiral molecules**

    [Chirality]({{< relref "chirality" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
