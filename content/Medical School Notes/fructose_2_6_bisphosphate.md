+++
title = "Fructose-2,6-bisphosphate"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Glycolysis > 6-Phosphofructo-2-kinase (PFK-2)**

    In the fasting state (gluconeogenesis favored in liver) -> [[F-2,6-bP]({{< relref "fructose_2_6_bisphosphate" >}})] is limited -> **PFK-1 not stimulated** -> further favoring of gluconeogenesis

    ---

<!--list-separator-->

-  **🔖 Glycolysis > 6-Phosphofructo-2-kinase (PFK-2)**

    The product, [fructose-2,6-bisphosphate]({{< relref "fructose_2_6_bisphosphate" >}}), is formed when the kinase catalytic site is active

    ---


#### [Identify the fructose metabolite that reciprocally regulates the glycolytic and gluconeogenic pathways within the liver and the enzyme that catalyzes its formation]({{< relref "identify_the_fructose_metabolite_that_reciprocally_regulates_the_glycolytic_and_gluconeogenic_pathways_within_the_liver_and_the_enzyme_that_catalyzes_its_formation" >}}) {#identify-the-fructose-metabolite-that-reciprocally-regulates-the-glycolytic-and-gluconeogenic-pathways-within-the-liver-and-the-enzyme-that-catalyzes-its-formation--identify-the-fructose-metabolite-that-reciprocally-regulates-the-glycolytic-and-gluconeogenic-pathways-within-the-liver-and-the-enzyme-that-catalyzes-its-formation-dot-md}

The metabolite is [Fructose-2,6-bisphosphate]({{< relref "fructose_2_6_bisphosphate" >}}) and it is formed by [PFK-2]({{< relref "pfk_2" >}})

---


### Unlinked references {#unlinked-references}

[Show unlinked references]
