+++
title = "Mesothelial cell"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Pleural fluid]({{< relref "pleural_fluid" >}}) {#pleural-fluid--pleural-fluid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Secreted by [mesothelial cells]({{< relref "mesothelial_cell" >}}) to lubricate the surfaces of the two layers of the [Pleura]({{< relref "pleura" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
