+++
title = "Immunoglobulin"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The most significant group of [gamma globulins]({{< relref "gamma_globulin" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Treating sickle cell anemia]({{< relref "treating_sickle_cell_anemia" >}}) {#treating-sickle-cell-anemia--treating-sickle-cell-anemia-dot-md}

<!--list-separator-->

-  **🔖 Treatment > Drugs**

    [Antibody]({{< relref "immunoglobulin" >}}) that blocks the adhesion molecule P-selectin, which is expressed by RBCs

    ---


#### [Plasma cell]({{< relref "plasma_cell" >}}) {#plasma-cell--plasma-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A mature [B-lymphocyte]({{< relref "b_lymphocyte" >}}) that produces [antibodies]({{< relref "immunoglobulin" >}})

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Plasma Proteins (p. 796) > Globulin**

    [Gamma globulins]({{< relref "gamma_globulin" >}}) are proteins involved in immunity, better known as antibodies or [immunoglobulins]({{< relref "immunoglobulin" >}})

    ---


#### [Germinal center]({{< relref "germinal_center" >}}) {#germinal-center--germinal-center-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Where [helper T cells]({{< relref "cd4_positive_t_lymphocyte" >}}) cause [B-cells]({{< relref "b_lymphocyte" >}}) to go through [somatic hypermutation]({{< relref "somatic_hypermutation" >}}) -> create more of a specific [antibody]({{< relref "immunoglobulin" >}})

    ---


#### [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}}) {#describe-the-major-actions-occurring-in-these-regions--with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot----describe-the-major-actions-occurring-in-these-regions-with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Follicles**

    B-cells producing a highly specific [Ig]({{< relref "immunoglobulin" >}}) become plasma cells -> leave lymph node or enter medullary cords -> produce abundant Ig

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
