+++
title = "Glucose 6-phosphate"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Signals [muscle]({{< relref "muscle" >}}) cells to start [synthesizing]({{< relref "glycogenesis" >}}) [glycogen]({{< relref "glycogen" >}})


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}}) {#list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated--list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated-dot-md}

<!--list-separator-->

-  **🔖 Examples in glucose catabolism > Product inhibition**

    [Hexokinase]({{< relref "hexokinase" >}}) inhibited by [G6P]({{< relref "glucose_6_phosphate" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Glycolysis > Hexokinases I-III**

    Hexokinase enzymes are **inhibited by [G6P]({{< relref "glucose_6_phosphate" >}}), the reaction product**

    ---

<!--list-separator-->

-  **🔖 Glycolysis > Hexokinases I-III**

    Hexokinase catalyzes ATP-dependent phosphorylation of glucose to [G6P]({{< relref "glucose_6_phosphate" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Metabolism**

    [G6P]({{< relref "glucose_6_phosphate" >}}) binds to [glycogen synthase]({{< relref "glycogen_synthase" >}}) B -> better substrate for [Phosphoprotein phosphatase]({{< relref "phosphoprotein_phosphatase" >}}) -> activated glycogen synthase A

    ---


#### [Hexokinase]({{< relref "hexokinase" >}}) {#hexokinase--hexokinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Catalyzes ATP-dependent phosphorylation of [glucose]({{< relref "glucose" >}}) to [G6P]({{< relref "glucose_6_phosphate" >}}) to trap it within a cell

    ---


#### [Glycogen synthase]({{< relref "glycogen_synthase" >}}) {#glycogen-synthase--glycogen-synthase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [G6P]({{< relref "glucose_6_phosphate" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
