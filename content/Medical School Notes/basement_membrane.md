+++
title = "Basement membrane"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}}) {#know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot--know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Overview of Basic Skin > 3 layers > Epidermis > Components**

    Rests on a [basement membrane]({{< relref "basement_membrane" >}})

    ---


#### [Hemidesmosome]({{< relref "hemidesmosome" >}}) {#hemidesmosome--hemidesmosome-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Adheres cells of the [epidermis]({{< relref "epidermis" >}}) to the [basement membrane]({{< relref "basement_membrane" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
