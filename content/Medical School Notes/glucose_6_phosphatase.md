+++
title = "Glucose-6-phosphatase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    <!--list-separator-->

    -  [Glucose-6-phosphatase]({{< relref "glucose_6_phosphatase" >}})

        -   Catalyzes **hydrolysis of G6P to free glucose for release from liver**
        -   Reverses [glucokinase]({{< relref "glucokinase" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
