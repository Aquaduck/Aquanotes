+++
title = "Tonsil"
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Lymphoid Nodules (p. 997)**

    <!--list-separator-->

    -  [Tonsils]({{< relref "tonsil" >}})

        -   Lymphoid nodules located along the inner surface of the pharynx
        -   Important in developing immunity to oral pathogens
        -   The tonsil located at the back of the throat (_pharyngeal tonsil_) is sometimes referred to as the _adenoid_ when swollen
        -   Epithelial layer invaginates deeply into the interior of the tonsil to form _tonsillar crypts_
            -   Accumulate materials taken into the body through eating and breathing
            -   A "honey pot" for pathogens
        -   **Main function**: help children's bodies recognize, destroy, and develop immunity to common environmental pathogens


### Unlinked references {#unlinked-references}

[Show unlinked references]
