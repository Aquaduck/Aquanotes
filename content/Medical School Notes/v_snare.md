+++
title = "v-SNARE"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [SNARE]({{< relref "snare" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe how vesicles move, recognize, and fuse with the target compartment.]({{< relref "describe_how_vesicles_move_recognize_and_fuse_with_the_target_compartment" >}}) {#describe-how-vesicles-move-recognize-and-fuse-with-the-target-compartment-dot--describe-how-vesicles-move-recognize-and-fuse-with-the-target-compartment-dot-md}

<!--list-separator-->

-  **🔖 From Fusion of Cells by Flipped SNARES**

    The fourth, longer alpha-helix is contributed by the vesicle or [v-SNARE]({{< relref "v_snare" >}}) is rooted in the opposing membrane

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
