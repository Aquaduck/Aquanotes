+++
title = "Be familiar with the unique features of the pulmonary circulation."
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Guyton and Hall Textbook of Medical Physiology]({{< relref "guyton_and_hall_textbook_of_medical_physiology" >}}) {#from-guyton-and-hall-textbook-of-medical-physiology--guyton-and-hall-textbook-of-medical-physiology-dot-md}


### [Zones]({{< relref "west_s_zones_of_the_lung" >}}) 1, 2, and 3 of pulmonary blood flow (p. 510) {#zones--west-s-zones-of-the-lung-dot-md--1-2-and-3-of-pulmonary-blood-flow--p-dot-510}

{{< figure src="/ox-hugo/_20211027_162440screenshot.png" caption="Figure 1: Relationship of arteriole, alveolar, and venous pressure in the lung zones" width="400" >}}


#### Zone 1 {#zone-1}

-   **No blood flow during all portions of the cardiac cycle**
-   Local _alveolar capillary pressure_ in this area of the lung **never rises higher** than the _alveolar air pressure_ during any part of the cardiac cycle


#### Zone 2 {#zone-2}

-   **Intermittent blood flow only during peaks of pulmonary arterial pressure**
-   _Systolic pressure_ is greater than _alveolar air pressure_, but _diastolic pressure_ is less than the _alveolar air pressure_


#### Zone 3 {#zone-3}

-   **Continous blood flow**
-   _Alveolar capillary pressure_ **remains greater** than the _alveolar air pressure_ during **the entire cardiac cycle**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-22 Fri] </span></span> > Respiration: Ventilation - Perfusion Matching > Pre-work**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Be familiar with the unique features of the pulmonary circulation.]({{< relref "be_familiar_with_the_unique_features_of_the_pulmonary_circulation" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
