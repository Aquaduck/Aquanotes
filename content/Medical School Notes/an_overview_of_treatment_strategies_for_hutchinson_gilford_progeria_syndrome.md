+++
title = "An overview of treatment strategies for Hutchinson-Gilford Progeria syndrome"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}}) {#rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot--rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot-md}

<!--list-separator-->

-  From [An overview of treatment strategies for Hutchinson-Gilford Progeria syndrome]({{< relref "an_overview_of_treatment_strategies_for_hutchinson_gilford_progeria_syndrome" >}})

    -   [Farnesyltransferase inhibitors]({{< relref "farnesyltransferase_inhibitor" >}}) (FTIs) are small molecules which reversibly bind to the farnesyltransferase CAAX binding site
        -   Blocks farnesylation of of [progerin]({{< relref "progerin" >}}) -> restores normal nuclear architecture w/ significantly reduced nuclear blebbing


### Unlinked references {#unlinked-references}

[Show unlinked references]
