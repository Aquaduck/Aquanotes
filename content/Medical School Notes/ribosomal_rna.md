+++
title = "Ribosomal RNA"
author = ["Arif Ahsan"]
date = 2021-07-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [RNA]({{< relref "rna" >}}) {#rna--rna-dot-md}

<!--list-separator-->

-  **🔖 Types of RNA**

    **[Ribosomal RNA]({{< relref "ribosomal_rna" >}})** (rRNA)

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Ribosomes > Structure**

    Composed of several types of [rRNA]({{< relref "ribosomal_rna" >}}) and [proteins]({{< relref "protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
