+++
title = "Rough endoplasmic reticulum"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   An [organelle]({{< relref "organelle" >}})
-   A type of [Endoplasmic reticulum]({{< relref "endoplasmic_reticulum" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Differentiate between rough and smooth endoplasmic reticulum (ER) both in structure and function.]({{< relref "differentiate_between_rough_and_smooth_endoplasmic_reticulum_er_both_in_structure_and_function" >}}) {#differentiate-between-rough-and-smooth-endoplasmic-reticulum--er--both-in-structure-and-function-dot--differentiate-between-rough-and-smooth-endoplasmic-reticulum-er-both-in-structure-and-function-dot-md}

<!--list-separator-->

-  [Rough endoplasmic reticulum]({{< relref "rough_endoplasmic_reticulum" >}}) (RER)

    <!--list-separator-->

    -  Structure

        -   System of membrane-bound sacs/cavities
        -   Outer surface studded with ribosomes (which is what "rough" means)
        -   _Cisterna or lumen_: interior region
        -   Outer nuclear membrane **continuous** with RER membrane
        -   _Ribophorins_: receptors where large [ribosomal]({{< relref "ribosome" >}}) subunits bind
        -   Abundant in cells synthesizing **secretory proteins**
            -   In these cells, RER organized into many parallel arrays
        -   RER sac closest to the Golgi apparatus makes buds w/o ribosomes -> form vesicles
            -   This is considered a **transitional element**

    <!--list-separator-->

    -  Function

        -   Location where **membrane-packaged [proteins]({{< relref "protein" >}})** are synthesized
            -   Secretory proteins
            -   Plasma membrane proteins
            -   Lysosomal proteins
        -   Monitors the assembly, retention, and degradation of certain proteins


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Lysosomes > Formation > Late endosomes**

    Formed in the [RER]({{< relref "rough_endoplasmic_reticulum" >}}) -> transported to Golgi complex -> delivered in separate vesicles to late endosomes

    ---

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles**

    <!--list-separator-->

    -  [Rough endoplasmic reticulum]({{< relref "rough_endoplasmic_reticulum" >}}) (RER)

        <!--list-separator-->

        -  Structure

            -   System of membrane-bound sacs/cavities
            -   Outer surface studded with ribosomes (which is what "rough" means)
            -   _Cisterna or lumen_: interior region
            -   Outer nuclear membrane **continuous** with RER membrane
            -   _Ribophorins_: receptors where large [ribosomal]({{< relref "ribosome" >}}) subunits bind
            -   Abundant in cells synthesizing **secretory proteins**
                -   In these cells, RER organized into many parallel arrays
            -   RER sac closest to the Golgi apparatus makes buds w/o ribosomes -> form vesicles
                -   This is considered a **transitional element**

        <!--list-separator-->

        -  Function

            -   Location where **membrane-packaged [proteins]({{< relref "protein" >}})** are synthesized
                -   Secretory proteins
                -   Plasma membrane proteins
                -   Lysosomal proteins
            -   Monitors the assembly, retention, and degradation of certain proteins


### Unlinked references {#unlinked-references}

[Show unlinked references]
