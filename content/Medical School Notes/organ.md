+++
title = "Organ"
author = ["Arif Ahsan"]
date = 2021-08-02T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Liver]({{< relref "liver" >}}) {#liver--liver-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    The liver is an [organ]({{< relref "organ" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
