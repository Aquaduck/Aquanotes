+++
title = "Axon"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A component of [neurons]({{< relref "neuron" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Tau protein]({{< relref "tau_protein" >}}) {#tau-protein--tau-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An insoluble [microtubule]({{< relref "microtubule" >}})-associated protein that maintains the stability of microtubules in [axons]({{< relref "axon" >}})

    ---


#### [Describe the general morphology of neurons and levels of organization of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}}) {#describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot--describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127) > Neuron structure**

    <!--list-separator-->

    -  [Axons]({{< relref "axon" >}})

        -   Conduct impulses **away from** the soma to axon terminals **without any diminuition in their strength**
        -   Originate from the [axon hillock]({{< relref "axon_hillock" >}})
            -   A specialized region of the soma that lacks RER, ribosomes, Golgi cisternae, and Nissle bodies
            -   Contains many microtubules and neurofilaments
                -   [Neurofilaments]({{< relref "neurofilament" >}}) regulate axon diameter
                -   Permits passage of mitochondria and vesicles into the axon
        -   _Axon cytoplasm_ lacks a Golgi complex but contains SER, RER, and elongated mitochondria
        -   Terminate in many small branches ([axon terminals]({{< relref "axon_terminal" >}})) from which impulses are passed to another neuron or type of cell(s)

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127)**

    [Neurons]({{< relref "neuron" >}}) consist of a cell body and its processes, which usually include multiple [dendrites]({{< relref "dendrite" >}}) and a single [axon]({{< relref "axon" >}})

    ---


#### [Axon terminal]({{< relref "axon_terminal" >}}) {#axon-terminal--axon-terminal-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The terminal end of an [axon]({{< relref "axon" >}})

    ---


#### [Axon hillock]({{< relref "axon_hillock" >}}) {#axon-hillock--axon-hillock-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The originating point of [axons]({{< relref "axon" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
