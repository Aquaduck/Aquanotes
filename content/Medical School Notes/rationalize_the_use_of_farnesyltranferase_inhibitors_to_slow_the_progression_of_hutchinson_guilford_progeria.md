+++
title = "Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria."
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Protein farnesylation and disease]({{< relref "protein_farnesylation_and_disease" >}}) {#from-protein-farnesylation-and-disease--protein-farnesylation-and-disease-dot-md}


### Nuclear [lamins]({{< relref "lamin" >}}) {#nuclear-lamins--lamin-dot-md}

-   Humans have **three** distinct lamin genes that encode seven different proteins


#### [A-type lamins]({{< relref "lamin_a" >}}) {#a-type-lamins--lamin-a-dot-md}

-   Products of a single gene termed [LMNA]({{< relref "lmna" >}})
-   [Prelamin A]({{< relref "prelamin_a" >}}) is the precursor protein of mature lamin A and possesses a COOH terminal CAAX motif -> the site of post-translational modifications
-   Plays a major role in **nuclear envelope architecture** and a functional role in **heterochromatin organization, cell cycle, differentiation dynamics, and transcriptional regulation**
-   Undergoes post-translational modification via [farnesylation]({{< relref "farnesylation" >}})


### Isoprenylation and postprenylation of nuclear lamins {#isoprenylation-and-postprenylation-of-nuclear-lamins}

-   [Farnesylation]({{< relref "farnesylation" >}}): the post-translational addition of the 15 carbon [farnesyl pyrophosphate]({{< relref "farnesyl_pyrophosphate" >}}) (FPP) via thioether bond to the sulphur atom of the cysteine of a carboxyterminal CAAX motif
-   In [HGPS]({{< relref "hutchinson_guildford_progeria" >}}), a 50 AA deletion in prelamin A removes the site for a second endoproteolytic cleavage
    -   Consequently, no mature [lamin A]({{< relref "lamin_a" >}}) is formed -> accumulation of a **farnesylated mutant prelamin A** ([progerin]({{< relref "progerin" >}}))


## From [An overview of treatment strategies for Hutchinson-Gilford Progeria syndrome]({{< relref "an_overview_of_treatment_strategies_for_hutchinson_gilford_progeria_syndrome" >}}) {#from-an-overview-of-treatment-strategies-for-hutchinson-gilford-progeria-syndrome--an-overview-of-treatment-strategies-for-hutchinson-gilford-progeria-syndrome-dot-md}

-   [Farnesyltransferase inhibitors]({{< relref "farnesyltransferase_inhibitor" >}}) (FTIs) are small molecules which reversibly bind to the farnesyltransferase CAAX binding site
    -   Blocks farnesylation of of [progerin]({{< relref "progerin" >}}) -> restores normal nuclear architecture w/ significantly reduced nuclear blebbing


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-08 Wed] </span></span> > Cytoskeleton > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
