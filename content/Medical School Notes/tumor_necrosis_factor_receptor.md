+++
title = "Tumor necrosis factor receptor"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Tumor necrosis factor receptor superfamily]({{< relref "tumor_necrosis_factor_receptor_superfamily" >}}) {#tumor-necrosis-factor-receptor-superfamily--tumor-necrosis-factor-receptor-superfamily-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A superfamily of [TNFR]({{< relref "tumor_necrosis_factor_receptor" >}})s

    ---


#### [TNFR1]({{< relref "tnfr1" >}}) {#tnfr1--tnfr1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The main [TNFR]({{< relref "tumor_necrosis_factor_receptor" >}}) that transduces signals from [TNF-α]({{< relref "tumor_necrosis_factor" >}})

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Death receptors > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    Death receptors are homotrimers and belong to the [tumor necrosis factor]({{< relref "tumor_necrosis_factor_receptor" >}}) (TNF) **receptor** family

    ---


#### [Death receptor]({{< relref "death_receptor" >}}) {#death-receptor--death-receptor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Subgroup of the [TNFR]({{< relref "tumor_necrosis_factor_receptor" >}}) superfamily

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
