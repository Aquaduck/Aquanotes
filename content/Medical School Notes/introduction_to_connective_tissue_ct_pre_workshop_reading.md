+++
title = "Introduction to Connective Tissue (CT) Pre-workshop Reading"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the types of cellular and acellular components of connective tissue]({{< relref "name_the_types_of_cellular_and_acellular_components_of_connective_tissue" >}}) {#name-the-types-of-cellular-and-acellular-components-of-connective-tissue--name-the-types-of-cellular-and-acellular-components-of-connective-tissue-dot-md}

<!--list-separator-->

-  From [Introduction to Connective Tissue (CT) Pre-workshop Reading]({{< relref "introduction_to_connective_tissue_ct_pre_workshop_reading" >}})

    <!--list-separator-->

    -  Four major components that make up [connective tissue]({{< relref "connective_tissue" >}})

        <!--list-separator-->

        -  Fibrous structural proteins

        <!--list-separator-->

        -  [Ground substance]({{< relref "ground_substance" >}})

        <!--list-separator-->

        -  Adhesion proteins

        <!--list-separator-->

        -  Cells

    <!--list-separator-->

    -  Fibrous structural components

        -   All three of these stain red in H&E, but can be distinguished in special stains

        <!--list-separator-->

        -  [Collagen]({{< relref "collagen" >}})

            -   The most abundant protein in the body
            -   28 types labeled (Type I, Type II, etc.)
            -   90% of collagen is type I

        <!--list-separator-->

        -  Elastin fibers

            -   Stretchy fibers responsible for returning pinched skin to its normal shape

        <!--list-separator-->

        -  Reticulin fibers

            -   Fine collagen fibers (type III) form a very delicate scaffold
            -   Usually only a few reticulin fibers are present in most CT
                -   Despite this, they can sometimes be the dominant fiber types


### Unlinked references {#unlinked-references}

[Show unlinked references]
