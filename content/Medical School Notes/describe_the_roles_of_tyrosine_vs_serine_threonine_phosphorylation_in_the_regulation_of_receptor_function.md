+++
title = "Describe the roles of tyrosine vs serine / threonine phosphorylation in the regulation of receptor function"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Flavor counts {#flavor-counts}

-   Tyrosine phosphorylation **activates**
-   Serine/threonine phosphorylase **inactivates**
    -   Often causes loss of receptor function


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the roles of tyrosine vs serine / threonine phosphorylation in the regulation of receptor function]({{< relref "describe_the_roles_of_tyrosine_vs_serine_threonine_phosphorylation_in_the_regulation_of_receptor_function" >}}).


### Unlinked references {#unlinked-references}

[Show unlinked references]
