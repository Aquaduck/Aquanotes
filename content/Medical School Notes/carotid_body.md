+++
title = "Carotid body"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Changes in Ventilation (p. 664)**

    The additional increase in ventilation produced by acidosis **depends on [carotid bodies]({{< relref "carotid_body" >}})** and does not occur if they are removed

    ---

<!--list-separator-->

-  [Carotid]({{< relref "carotid_body" >}}) & [Aortic]({{< relref "aortic_body" >}}) bodies (p. 671)

    -   There is a carotid body near the carotid bifurcation on each side
    -   There are 2+ aortic bodies near the arch of the aorta
    -   Each carotid/aortic body (_glomus_) contains islands of two types of cells ([glomus cells]({{< relref "glomus_cell" >}})) surrounded by fenestered sinusoidal capillaries
        -   [Type I]({{< relref "type_1_glomus_cell" >}}):
            -   Are closely associated with cuplike endings of the afferent nerves
            -   Resemble adrenal chromaffin cells
            -   Have dense-core granules containing catecholamines that are released upon excitation via exposure to hypoxia and [cyanide]({{< relref "cyanide" >}})
            -   The principle transmitter appears to be [dopamine]({{< relref "dopamine" >}}) -> excites the nerve endings through [D<sub>2</sub> receptors]({{< relref "dopamine_d2_receptor" >}})
            -   Have oxygen-sensitive potassim channels, whose conductance is reduced in proportion to the degree of hypoxia detected
                -   Reduces potassium efflux -> depolarizes cell -> calcium influx primarily via [L-type calcium channels]({{< relref "l_type_calcium_channel" >}}) -> action potentials, neurotransmitter release, excitation of afferent nerve endings
                    -   Smooth muscle of pulmonary arteries contain similar oxygen-sensitive potassium channels -> mediate vasoconstriction caused by hypoxia
                        -   Contrast to systemic arteries, which contain ATP-dependent potassium channels -> permit **more** potassium efflux with hypoxia -> **cause vasodilation instead**
        -   [Type II]({{< relref "type_2_glomus_cell" >}}): glia-like
            -   Surrounds 4-6 type I cells
            -   Function unknown

    {{< figure src="/ox-hugo/_20211016_184151screenshot.png" caption="Figure 1: Organization of the carotid body." >}}

    <!--list-separator-->

    -  Carotid bodies

        -   **Blood flow per unit of tissue is enormous** -> oxygen needs are met largely by **dissolved oxygen** alone
            -   Thus, the receptors are **not stimulated in conditions like anemia or carbon monoxide poisoning**
        -   Receptors stimulated when \*arterial oxygen pressure (PO<sub>2</sub>) delivered to the receptors over time is decreased
        -   [Cyanide]({{< relref "cyanide" >}}) is a powerful stimulus <- prevents oxygen utilization at the tissue level

    <!--list-separator-->

    -  Aortic bodies

        -   Have not been studied as well as carotid bodies due to location
        -   Believed responses is similar but in lesser magnitude


### Unlinked references {#unlinked-references}

[Show unlinked references]
