+++
title = "Session 6"
author = ["Arif Ahsan"]
date = 2021-07-19T00:00:00-04:00
tags = ["medschool", "jumpstart"]
draft = false
+++

## gRAT {#grat}

1.  E
2.  D
3.  A
4.  A
5.  B
6.  E
7.  A
8.  A
    -   Malate-aspartate pathway = NADH
    -   Glycerol phosphate pathway = FADH2
9.  E
10. B


## Metabolism advice {#metabolism-advice}

-   Done from Day 1 of med school until April of M1 + Boards - super important
-   Don't get lost in the details - **remember the concepts**


## Glycolysis Worksheet {#glycolysis-worksheet}

<img src="/ox-hugo/_20210719_142921screenshot.png" alt="_20210719_142921screenshot.png" width="200" />
1a) 1, 3
1b) Hexokinase, PFK-1

1.  7, 10

3a) 6
3b) G3P DH
4a) 3
4b) PFK1
5a) 10
5b) Pyruvate Kinase
6a) 1, 3, 10
6b) Hexokinase, PFK1, Pyruvate Kinase

1.  6 (1,3-bisphosphoglycerate), 9 (PEP)

8a) 6
8b) G3P DH
8c) No net ATP -> RBCs die

-   Mimics inorganic phosphate -> bipassing 1,3-bisphosphoglycerate -> no ATP

9a) Preparing molecules +
9b) Generation ATP + NADH

1.  Redox -> generates NADH

11a) Decrease
11b) Increase
11c) Decrease
11d) None


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
