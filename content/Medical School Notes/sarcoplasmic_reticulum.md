+++
title = "Sarcoplasmic reticulum"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Muscle cell [SER]({{< relref "smooth_endoplasmic_reticulum" >}}), which forms a network of [L-tubules]({{< relref "l_tubule" >}})
-   Forms a meshwork around each [myofibril]({{< relref "myofibril" >}})


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Terminal cisternae]({{< relref "terminal_cisternae" >}}) {#terminal-cisternae--terminal-cisternae-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Enlarged areas of [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) surrounding [t tubules]({{< relref "transverse_tubule" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Enlarged areas of [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) surrounding [t tubules]({{< relref "transverse_tubule" >}})

    ---


#### [Explain the role of calcium in striated muscle contraction and describe its regulation.]({{< relref "explain_the_role_of_calcium_in_striated_muscle_contraction_and_describe_its_regulation" >}}) {#explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot--explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > A sudden rise in cytosolic Ca<sup>2+</sup> concentration initiates muscle contraction (p. 920)**

    Incoming AP activates a calcium channel in the T-tubule membrane -> calcium influx -> opening of calcium release channels in [sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}})

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Structure of Skeletal Muscle (p. 109) > Skeletal muscle cells**

    <!--list-separator-->

    -  [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) (SR)

        -   Modified smooth endoplasmic reticulum (SER) that surrounds myofilaments and forms a meshwork around each myofibril
        -   **Regulates muscle contraction** by sequestering calcium ions (leading to relaxation) or releasing calcium ions (leading to contraction)


#### [Describe the relationship between ventricular filling and stroke volume on a Frank-Starling curve.]({{< relref "describe_the_relationship_between_ventricular_filling_and_stroke_volume_on_a_frank_starling_curve" >}}) {#describe-the-relationship-between-ventricular-filling-and-stroke-volume-on-a-frank-starling-curve-dot--describe-the-relationship-between-ventricular-filling-and-stroke-volume-on-a-frank-starling-curve-dot-md}

<!--list-separator-->

-  **🔖 From Physiology, Frank Starling Law > Mechanism of the Frank-Starling law**

    Caused by catecholamines binding to a myocyte beta1-adrenergic receptor (a G-protein coupled receptor) -> increased calcium channel release from the [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) -> enables force of contraction

    ---


#### [Calcium ATPase]({{< relref "calcium_atpase" >}}) {#calcium-atpase--calcium-atpase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Pumps calcium back into [SR]({{< relref "sarcoplasmic_reticulum" >}}) to keep signal transient

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
