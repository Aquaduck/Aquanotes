+++
title = "Chylomicron retention disease"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "learning_objective", "concept"]
draft = false
+++

## From [Molecular analysis and intestinal expression of SAR1 genes and proteins in Anderson's disease (Chylomicron retention disease)]({{< relref "molecular_analysis_and_intestinal_expression_of_sar1_genes_and_proteins_in_anderson_s_disease_chylomicron_retention_disease" >}}) {#from-molecular-analysis-and-intestinal-expression-of-sar1-genes-and-proteins-in-anderson-s-disease--chylomicron-retention-disease----molecular-analysis-and-intestinal-expression-of-sar1-genes-and-proteins-in-anderson-s-disease-chylomicron-retention-disease-dot-md}

-   Results from a mutation in the [SAR1B gene]({{< relref "sar1b_gene" >}}) which encodes the [SAR1B protein]({{< relref "sar1b_protein" >}})
    -   A small GTPase involved [COPII]({{< relref "cop_ii" >}})-dependent transport of proteins from the [ER]({{< relref "endoplasmic_reticulum" >}}) to the [Golgi]({{< relref "golgi_apparatus" >}})
        -   Sar1/COPII protein complex required for fusion of the specific chylomicron transport vesicle _Pre-chylomicron transport vesicle (PCTV)_ with the Golgi
        -   The mutation here prevents this vesicle from fusing with the Golgi, resulting in the chylomicrons being sent back to the ER and collecting there


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-01 Wed] </span></span> > Organelles & Trafficking I (+ II) > Session > Describe the cell biological defects underlying:**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Chylomicron retention disease / Anderson disease]({{< relref "chylomicron_retention_disease" >}});


### Unlinked references {#unlinked-references}

[Show unlinked references]
