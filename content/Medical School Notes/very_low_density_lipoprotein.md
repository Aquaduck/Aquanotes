+++
title = "Very low-density lipoprotein"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoCII > From Amboss**

    A component of [chylomicrons]({{< relref "chylomicron" >}}), [VLDL]({{< relref "very_low_density_lipoprotein" >}}), and [HDL]({{< relref "high_density_lipoprotein" >}})

    ---


#### [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}}) {#lipoprotein-lipase--lipoprotein-lipase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Targets [very low-density lipoprotein]({{< relref "very_low_density_lipoprotein" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Lipoprotein lipase**

    Also targets [very low-density lipoprotein]({{< relref "very_low_density_lipoprotein" >}}) (VLDL)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
