+++
title = "Sensory neuron"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A functional type of [neuron]({{< relref "neuron" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Interneuron]({{< relref "interneuron" >}}) {#interneuron--interneuron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Commonly connect [sensory neurons]({{< relref "sensory_neuron" >}}) and [motor neurons]({{< relref "motor_neuron" >}})

    ---


#### [Describe the general morphology of neurons and levels of organization of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}}) {#describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot--describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127) > Functional classification of neurons**

    [Sensory neurons]({{< relref "sensory_neuron" >}}) **receive stimuli** from the internal and external environments

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
