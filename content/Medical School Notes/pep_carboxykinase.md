+++
title = "PEP carboxykinase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

\#+roam\_alias : PEPCK "PEP CK"


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    [PEP CK]({{< relref "pep_carboxykinase" >}}) requires [Pyridoxine]({{< relref "pyridoxine" >}}) (B6) (as pyridoxal phosphate) - a cofactor used in decarboxylation reactions

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    <!--list-separator-->

    -  [PEP carboxykinase]({{< relref "pep_carboxykinase" >}})

        -   Catalyzes a **GTP-dependent** reaction
        -   **Converts [OAA]({{< relref "oxaloacetate" >}}) into [PEP]({{< relref "phosphoenolpyruvate" >}})**
        -   This + [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) required to convert pyruvate into PEP
            -   **Reverses action of pyruvate kinase**


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By enzyme**

    <!--list-separator-->

    -  [PEP CK]({{< relref "pep_carboxykinase" >}})

        -   Pyridoxine (B6) (as pyridoxal phosphate) is a cofactor used in decarboxylation reactions


### Unlinked references {#unlinked-references}

[Show unlinked references]
