+++
title = "Afferent arteriole of the kidney"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The blood vessel that enters and supplies the [glomerulus]({{< relref "glomerulus" >}}) of the kidney for filtration


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Ascending thick limb of the loop of Henle**

    Passes directly between the [afferent]({{< relref "afferent_arteriole_of_the_kidney" >}}) and [efferent]({{< relref "efferent_arteriole_of_the_kidney" >}}) arterioles at the vascular pole

    ---


#### [Granular cell]({{< relref "granular_cell" >}}) {#granular-cell--granular-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Differentiated smooth muscle cells in the walls of the [afferent arterioles]({{< relref "afferent_arteriole_of_the_kidney" >}})

    ---


#### [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}}) {#explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result--explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Juxtaglomerular apparatus (p. 21) > Granular cells**

    Differentiated smooth muscle cells in the walls of the [afferent arterioles]({{< relref "afferent_arteriole_of_the_kidney" >}})

    ---


#### [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}}) {#describe-glomerular-anatomy--ie-efferent-afferent-arteriole----describe-glomerular-anatomy-ie-efferent-afferent-arteriole-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal corpuscle (p. 17)**

    [Afferent arteriole of the kidney]({{< relref "afferent_arteriole_of_the_kidney" >}}): brings blood into the capillaries of the glomerulus

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Macula densa > Feedback mechanisms**

    Triggers release of [Adenosine]({{< relref "adenosine" >}}) -> vasoconstriction of the [afferent arteriole]({{< relref "afferent_arteriole_of_the_kidney" >}}) -> **decrease in glomerular filtration rate**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
