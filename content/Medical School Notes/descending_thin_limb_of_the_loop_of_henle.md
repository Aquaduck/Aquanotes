+++
title = "Descending thin limb of the loop of Henle"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   First component of the [loop of Henle]({{< relref "loop_of_henle" >}})
-   Responsible for [water]({{< relref "water" >}}) reabsorption


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18)**

    <!--list-separator-->

    -  [Descending thin limb of the loop of Henle]({{< relref "descending_thin_limb_of_the_loop_of_henle" >}})

        -   The descending thin limbs **begin at the same level in all nephrons** - the point where they connect to proximal straight tubule in the [outer medulla]({{< relref "outer_medulla_of_the_kidney" >}})
            -   Marks the border between the oute rand inner stripes of the outer medulla
            -   On the other hand, the **penetrating depth** of each nephron's descending limb **varies**


### Unlinked references {#unlinked-references}

[Show unlinked references]
