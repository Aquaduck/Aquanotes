+++
title = "Metabolic alkalosis"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  [Metabolic Alkalosis]({{< relref "metabolic_alkalosis" >}}): Primary Bicarbonate Excess (p. 1281)

    -   [Blood]({{< relref "blood" >}}) is too alkaline (pH above 7.45) due to **too much [bicarbonate]({{< relref "bicarbonate" >}})** (called _primary bicarbonate excess_)
    -   Causes of metabolic alkalosis:
        -   Can be transiently caused by an ingestion of excessive [bicarbonate]({{< relref "bicarbonate" >}}), [citrate]({{< relref "citrate" >}}), or [antacids]({{< relref "antacid" >}})
        -   [Cushing's disease]({{< relref "cushing_s_disease" >}}): oversecretion of [ACTH]({{< relref "adrenocorticotrophic_hormone" >}}) -> elevated [aldosterone]({{< relref "aldosterone" >}}) + more [potassium]({{< relref "potassium" >}}) excreted in [urine]({{< relref "urine" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
