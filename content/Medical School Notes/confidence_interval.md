+++
title = "Confidence interval"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Interpret confidence intervals around a sample size or calculated relative risk or odds ratio]({{< relref "interpret_confidence_intervals_around_a_sample_size_or_calculated_relative_risk_or_odds_ratio" >}}) {#interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio--interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Confidence interval]({{< relref "confidence_interval" >}})

        -   **The range of values that are highly likely to contain the true sample measurement**
        -   Provide a way to determine a [Population]({{< relref "population" >}}) measurement or a value that is subject to change from a [Sample]({{< relref "sample" >}}) measurement
        -   [Z scores]({{< relref "z_score" >}}) for confidence intervals for normally distributed data:
            -   Z score for 95% confidence interval = 1.96
            -   Z score for a 97.5% confidence interval = 2.24
            -   Z score for a 99% confidence interval = 2.58
        -   **Formula**: any sample measurement +/- Z score knowing:
            -   Confidence level (usually fixed at 95%)
            -   Sample measurement
            -   [Standard error of the mean]({{< relref "standard_error_of_the_mean" >}}), which needs:
                -   Sample size - larger sample size -> lower standard error -> more narrow confidence intervals
                -   [Standard deviation]({{< relref "standard_deviation" >}})


#### [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}}) {#explain-the-t-test-and-the-factors-that-influence-t-values--explain-the-t-test-and-the-factors-that-influence-t-values-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > T-test > One sample t-test**

    Alternatively, calculate [confidence intervals]({{< relref "confidence_interval" >}}) of the sample observations -> check if [population mean]({{< relref "population_mean" >}}) falls within the range given by confidence intervals

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
