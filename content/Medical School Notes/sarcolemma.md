+++
title = "Sarcolemma"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The [plasma membrane]({{< relref "plasma_membrane" >}}) of [skeletal muscle cells]({{< relref "skeletal_muscle_cell" >}})
    -   Variation of "plasmalemma" - another name for plasma membrane


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Transverse tubule]({{< relref "transverse_tubule" >}}) {#transverse-tubule--transverse-tubule-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Invaginations formed from [sarcolemma]({{< relref "sarcolemma" >}}) of skeletal muscle cells which extend into the cell

    ---


#### [L-type calcium channel]({{< relref "l_type_calcium_channel" >}}) {#l-type-calcium-channel--l-type-calcium-channel-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Located in the [Sarcolemma]({{< relref "sarcolemma" >}}) of [cardiac muscle cells]({{< relref "cardiac_muscle_cell" >}})

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Structure of Skeletal Muscle (p. 109) > Skeletal muscle cells**

    Skeletal muscle cell membrane is called a [sarcolemma]({{< relref "sarcolemma" >}}) (variation of [plasmalemma]({{< relref "plasma_membrane" >}}) - another name for cell membrane)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
