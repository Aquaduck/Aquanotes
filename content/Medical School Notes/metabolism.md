+++
title = "Metabolism"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Resources {#resources}


### [Osmosis Biochemistry and Metabolism Videos](https://www.osmosis.org/library/md/foundational-sciences/biochemistry-and-nutrition#biochemistry-biochemistry%5Fand%5Fmetabolism) {#osmosis-biochemistry-and-metabolism-videos}


### AK Lectures {#ak-lectures}


#### [Carbohydrate Metabolism](https://aklectures.com/subject/biochemistry/carbohydrate-metabolism) {#carbohydrate-metabolism}


#### [Glycogen Metabolism](https://aklectures.com/subject/biochemistry/carbohydrate-metabolism) {#glycogen-metabolism}


#### [Fatty Acid Metabolism](https://aklectures.com/subject/biochemistry/carbohydrate-metabolism) {#fatty-acid-metabolism}


#### [Amino Acid Metabolism](https://aklectures.com/subject/biochemistry/carbohydrate-metabolism) {#amino-acid-metabolism}


#### [Nucleotide Synthesis](https://aklectures.com/subject/biochemistry/carbohydrate-metabolism) {#nucleotide-synthesis}


### [Ninja Nerd Metabolism Playlist](https://www.youtube.com/playlist?list=PLTF9h-T1TcJhy6Og8piwo8doDJavTFOvg) {#ninja-nerd-metabolism-playlist}


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Oxidative Phosphorylation]({{< relref "oxidative_phosphorylation" >}}) {#oxidative-phosphorylation--oxidative-phosphorylation-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Oxidative Phosphorylation is a [metabolic]({{< relref "metabolism" >}}) process where [ATP]({{< relref "atp" >}}) is produced directly through usage of a proton gradient and electron transfers

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Major physiological functions of the kidney > Production of Urine**

    **Excretion** of metabolic waste and end-products of [metabolism]({{< relref "metabolism" >}})

    ---


#### [Glycolysis]({{< relref "glycolysis" >}}) {#glycolysis--glycolysis-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Glycolysis is a cellular [metabolic]({{< relref "metabolism" >}}) process that converts glucose into energy via [ATP]({{< relref "atp" >}}) production

    ---


#### [Gluconeogenesis]({{< relref "gluconeogenesis" >}}) {#gluconeogenesis--gluconeogenesis-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Gluconeogenesis is a [metabolic]({{< relref "metabolism" >}}) process that converts pyruvate back into glucose

    ---


#### [Citric Acid Cycle]({{< relref "citric_acid_cycle" >}}) {#citric-acid-cycle--citric-acid-cycle-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A cellular [metabolic]({{< relref "metabolism" >}}) process

    ---


#### [B vitamin]({{< relref "b_vitamin" >}}) {#b-vitamin--b-vitamin-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A class of water-soluble [vitamins]({{< relref "vitamin" >}}) that play an important role in [cell metabolism]({{< relref "metabolism" >}}) and synthesis of [red blood cells]({{< relref "red_blood_cell" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
