+++
title = "Acetylcholinesterase"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Breaks down [acetylcholine]({{< relref "acetylcholine" >}}) into acetic acid and choline


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the process of neuromuscular transmission.]({{< relref "describe_the_process_of_neuromuscular_transmission" >}}) {#describe-the-process-of-neuromuscular-transmission-dot--describe-the-process-of-neuromuscular-transmission-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Conduction of a nerve impulse across a myoneural junction (p. 116)**

    [Acetylcholinesterase]({{< relref "acetylcholinesterase" >}}) degrades acetylcholine -> depolarization signal ends

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
