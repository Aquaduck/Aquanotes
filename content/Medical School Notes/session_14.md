+++
title = "Session 14"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "jumpstart"]
draft = false
+++

## Worksheet {#worksheet}

1.  Top
    -   Open Reading Frame is on the SENSE strand -> corresponds to mRNA strand
2.  Top
3.  Bottom
4.  AUG CGU AGU CUU GGU CCA GAG UAA
5.  (N-terminus)-Met-Arg-Ser-Leu-Gly-Pro-Glu-(C-terminus)
6.  Elongation -> protein strand is generated but shortened
7.  Question parts:
    1.  5' AUG CCC CUU AAA GAG UUU ACA UAU UGC UGG CGU UAA 3'
    2.  (N-Terminus)-Met-Pro-Leu-Lys-Glu-Phe-Thr-Tyr-Cys-Arg-Arg-(C-terminus)
8.  _Same method as 7_
    1.  _mRNA sequence_
    2.  _AA sequence_
    3.  Point mutation
    4.  Nonconservative
        -   Leu is nonpolar, Arg is basic


## Application Questions {#application-questions}

1.  D
2.  C
3.  D
4.  B
    -   Acidic AA -> nonpolar AA
    -   nonpolar interior, polar exterior
    -   Would ruin structure of protein
5.  C


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
