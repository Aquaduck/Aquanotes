+++
title = "Adenosine Triphosphate (ATP)"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

{{< figure src="/ox-hugo/_20210715_123202screenshot.png" >}}


## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [Pyruvate kinase]({{< relref "pyruvate_kinase" >}}) {#pyruvate-kinase--pyruvate-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Allosterically]({{< relref "allosteric_regulation" >}}) inhibited by: [ATP]({{< relref "atp" >}})

    ---


#### [Oxidative Phosphorylation]({{< relref "oxidative_phosphorylation" >}}) {#oxidative-phosphorylation--oxidative-phosphorylation-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Oxidative Phosphorylation is a [metabolic]({{< relref "metabolism" >}}) process where [ATP]({{< relref "atp" >}}) is produced directly through usage of a proton gradient and electron transfers

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Glycolysis > Pyruvate kinase**

    **Allosterically inhibited** by [ATP]({{< relref "atp" >}}) in all tissues

    ---


#### [Introduction to Metabolism and Glycolysis]({{< relref "introduction_to_metabolism_and_glycolysis" >}}) {#introduction-to-metabolism-and-glycolysis--introduction-to-metabolism-and-glycolysis-dot-md}

<!--list-separator-->

-  **🔖 Catabolic pathways**

    Serve to capture chemical energy in the form of [ATP]({{< relref "atp" >}})

    ---


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 HIF-1 target genes involved in glucose and energy metabolism**

    HIF-1 appears to play a crucial homeostatic role in **managing [oxygen]({{< relref "oxygen" >}}) consumption** to balance [ATP]({{< relref "atp" >}}) and [ROS]({{< relref "reactive_oxygen_species" >}}) production

    ---


#### [Glycolysis]({{< relref "glycolysis" >}}) {#glycolysis--glycolysis-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Glycolysis is a cellular [metabolic]({{< relref "metabolism" >}}) process that converts glucose into energy via [ATP]({{< relref "atp" >}}) production

    ---


#### [Energy, Redox Reactions, and Enzymes]({{< relref "energy_redox_reactions_and_enzymes" >}}) {#energy-redox-reactions-and-enzymes--energy-redox-reactions-and-enzymes-dot-md}

<!--list-separator-->

-  **🔖 Energy Carriers**

    <!--list-separator-->

    -  [ATP]({{< relref "atp" >}})

        -   _Adenosine triphosphate_
        -   Stores energy in bonds with phosphate groups
            -   Phosphate groups are negatively charged -> repel each other -> **makes ATP molecules more unstable and higher in energy**
            -   Release energy by breaking phosphate bonds, converting it to ADP or AMP if releasing pyrophosphate (PP<sub>i</sub>)

        {{< figure src="/ox-hugo/_20210715_122440screenshot.png" width="600" >}}


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Mitochondria > Condensed mitochondria**

    [ATP]({{< relref "atp" >}}) reverses the swelling

    ---


#### [Bioenergetics and Oxidative Phosphorylation]({{< relref "bioenergetics_and_oxidative_phosphorylation" >}}) {#bioenergetics-and-oxidative-phosphorylation--bioenergetics-and-oxidative-phosphorylation-dot-md}

<!--list-separator-->

-  [ATP]({{< relref "atp" >}}) as an Energy Carrier

    -   Reactions that have a large positive \\(\Delta G\\) are made possible by coupling endergonic movement of ions with a second, spontaneous process with a **large negative \\(\Delta G\\)**
        -   The exergonic hydrolysis of ATP is an example of one such reaction
    -   _Note: in the absence of enzymes, ATP is a stable molecule because its hydrolysis has a high activation energy_

    <!--list-separator-->

    -  Common intermediates

        -   A _common intermediate_ is when a molecule serves as both the product of one reaction and the substrate of the next reaction
            -   It can carry chemical energy between the two reactions
        -   Many coupled reactions use ATP to generate a common intermediate
            -   May involve transfer of phosphate group to another molecule, or reverse

    <!--list-separator-->

    -  Energy caried by ATP

        -   Standard free energy of hydrolysis of ATP is approximately -7.3 kcal/mol for each of the two terminal phosphate groups - this is very high


#### [6-Phosphofructo-1-kinase]({{< relref "6_phosphofructo_1_kinase" >}}) {#6-phosphofructo-1-kinase--6-phosphofructo-1-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inhibited by [[ATP]({{< relref "atp" >}})]

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
