+++
title = "Ether"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Lumen - Functional Group Names, Properties, and Reactions]({{< relref "lumen_functional_group_names_properties_and_reactions" >}}) {#lumen-functional-group-names-properties-and-reactions--lumen-functional-group-names-properties-and-reactions-dot-md}

<!--list-separator-->

-  **🔖 Functional Groups**

    <!--list-separator-->

    -  [Ethers]({{< relref "ether" >}})

        <!--list-separator-->

        -  Structure of Ethers

            -   Oxygen atom connected to two alkyl or aryl groups: \\(R-O-R\\)
            -   104.5<sup>o</sup> bond angles
            -   Oxygen of ether more electronegative than carbons
                -   **alpha hydrogens more acidic than in regular hydrocarbons**

        <!--list-separator-->

        -  Nomenclature of Ethers

            -   Most common way is to write two alkyl groups in alphabetical order followed by _ether_
            -   Formal IUPAC way: _short alkyl chain_-_oxy_-_long alkyl chain_
            -   In cyclic ethers, the stem of the compound is known as an _oxacycloalkane_

        <!--list-separator-->

        -  Properties of Ethers

            -   Ethers are **nonpolar**
                -   Because of alkyl group on either side
            -   Bulky alkyl group prevents hydrogen bonding to oxygen
                -   Low boiling points compared to alcohols of similar molecular weight
                -   As alkyl chain becomes longer, difference in boiling points becomes smaller
                    -   Due to increased Van der Waals interactions as # of carbons (+ electrons) increases
            -   Ethers are **more polar than alkanes**, but **less polar than esters, alcohols, and amides**

        <!--list-separator-->

        -  Reactions

            -   **Ethers have relatively low chemical reactivity**
            -   Resist undergoing hydrolysis
            -   Can be **cleaved by acids -> formation of alkyl halide and an alcohol**


### Unlinked references {#unlinked-references}

[Show unlinked references]
