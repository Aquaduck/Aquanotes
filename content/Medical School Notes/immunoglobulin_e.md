+++
title = "Immunoglobulin E"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Name one drug in which severe adverse effects may depend on the presence of a specific genetic variation]({{< relref "name_one_drug_in_which_severe_adverse_effects_may_depend_on_the_presence_of_a_specific_genetic_variation" >}}) {#name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation--name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation-dot-md}

<!--list-separator-->

-  **🔖 From Pharmacogenetics Pre-learning Material > Drug allergy**

    Immune hypersensitivity reactions mediated by [immunoglobulin E]({{< relref "immunoglobulin_e" >}}) and driven by [mast cells]({{< relref "mast_cell" >}})

    ---


#### [Mast Cell: A Multi-Functional Master Cell]({{< relref "mast_cell_a_multi_functional_master_cell" >}}) {#mast-cell-a-multi-functional-master-cell--mast-cell-a-multi-functional-master-cell-dot-md}

<!--list-separator-->

-  **🔖 Mechanism of Activation**

    Mast cells are known for their main mechanism of action: [IgE]({{< relref "immunoglobulin_e" >}})-mediated allergic reactions through the FcɛRI receptor

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
