+++
title = "L-type calcium channel"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Located in the [Sarcolemma]({{< relref "sarcolemma" >}}) of [cardiac muscle cells]({{< relref "cardiac_muscle_cell" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    There are relatively few [L-type calcium channels]({{< relref "l_type_calcium_channel" >}}) in nodal cell sarcolemma

    ---

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [L-type calcium channels]({{< relref "l_type_calcium_channel" >}}) have slow kinetics

    ---


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Carotid & Aortic bodies (p. 671)**

    Reduces potassium efflux -> depolarizes cell -> calcium influx primarily via [L-type calcium channels]({{< relref "l_type_calcium_channel" >}}) -> action potentials, neurotransmitter release, excitation of afferent nerve endings

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
