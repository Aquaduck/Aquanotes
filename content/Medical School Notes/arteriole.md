+++
title = "Arteriole"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Regional and Peripheral Circulation Pre-reading]({{< relref "regional_and_peripheral_circulation_pre_reading" >}}) {#regional-and-peripheral-circulation-pre-reading--regional-and-peripheral-circulation-pre-reading-dot-md}

<!--list-separator-->

-  **🔖 Relationship of Pressure, Flow, and Resistance**

    **Resistance** (in systemic circulation) **resides mostly in the [arterioles]({{< relref "arteriole" >}})**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
