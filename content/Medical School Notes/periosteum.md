+++
title = "Periosteum"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Dense irregular CT covering the outer [bone]({{< relref "bone" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons > Tendons**

    Dense irregular connective tissue of [Periosteum]({{< relref "periosteum" >}}) mostly is oriented parallel to the surface of the bone

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Histologic components of mature bone**

    External circumferential lamella just underneath the [Periosteum]({{< relref "periosteum" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Organization of bones**

    Outer bone covered by a thin layer of dense irregular CT called the [periosteum]({{< relref "periosteum" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
