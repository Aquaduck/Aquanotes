+++
title = "Fusion of Cells by Flipped SNARES"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe how vesicles move, recognize, and fuse with the target compartment.]({{< relref "describe_how_vesicles_move_recognize_and_fuse_with_the_target_compartment" >}}) {#describe-how-vesicles-move-recognize-and-fuse-with-the-target-compartment-dot--describe-how-vesicles-move-recognize-and-fuse-with-the-target-compartment-dot-md}

<!--list-separator-->

-  From [Fusion of Cells by Flipped SNARES]({{< relref "fusion_of_cells_by_flipped_snares" >}})

    -   Fusion of intracellular membranes is mediated by [SNARE]({{< relref "snare" >}}) proteins that assemble between lipid bilayers as SNAREpins
        -   [SNAREpins]({{< relref "snarepin" >}}) consist of a bundle of four helices
            -   Four fusion to occur, three of these alpha-helices (contributed by target membrane or [t-SNARE]({{< relref "t_snare" >}})) emanate from one of the membrane partners
            -   The fourth, longer alpha-helix is contributed by the vesicle or [v-SNARE]({{< relref "v_snare" >}}) is rooted in the opposing membrane
        -   The spontaneous formation of the SNARE complex is coupled to [SNAREpins]({{< relref "snarepin" >}}) to promote fusion of the lipid bilayers to the target membrane
    -   SNARE dependent fusion is **very specific**


### Unlinked references {#unlinked-references}

[Show unlinked references]
