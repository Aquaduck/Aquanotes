+++
title = "Nucleoside"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A [nucleotide]({{< relref "nucleotide" >}}) bound to a [ribose]({{< relref "ribose" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Guanosine]({{< relref "guanosine" >}}) {#guanosine--guanosine-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Nucleoside]({{< relref "nucleoside" >}}) formed by [guanine]({{< relref "guanine" >}}) bound with [ribose]({{< relref "ribose" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
