+++
title = "Plasmin"
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Fibrinolysis (p. 818)**

    Inactive protein plasminogen converted to [plasmin]({{< relref "plasmin" >}}) -> gradually breaks down [fibrin]({{< relref "fibrin" >}}) of the clot

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
