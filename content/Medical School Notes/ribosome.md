+++
title = "Ribosome"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

{{< figure src="/ox-hugo/_20210721_153844screenshot.png" caption="Figure 1: Ribosome binding sites" >}}

-   An [organelle]({{< relref "organelle" >}})
-   Primarily responsible for [protein]({{< relref "protein" >}}) synthesis


## Backlinks {#backlinks}


### 11 linked references {#11-linked-references}


#### [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}}) {#osmosis-transcription-translation-and-replication--osmosis-transcription-translation-and-replication-dot-md}

<!--list-separator-->

-  **🔖 Translation > Transfer RNA (tRNA)**

    Binds to [ribosome]({{< relref "ribosome" >}}) on the aminoacyl/peptidyl/exit site

    ---

<!--list-separator-->

-  **🔖 Translation > Process**

    _Termination_: [ribosome]({{< relref "ribosome" >}}) reaches stop codon and releases polypeptide

    ---

<!--list-separator-->

-  **🔖 Translation > Process**

    _Elongation_: [ribosome]({{< relref "ribosome" >}}) moves along mRNA producing specific [AA]({{< relref "amino_acid" >}})s for each codon

    ---

<!--list-separator-->

-  **🔖 Translation > Process**

    _Initiation_: [ribosome]({{< relref "ribosome" >}}) grabs mRNA and finds start codon (e.g. AUG)

    ---

<!--list-separator-->

-  **🔖 Translation**

    [Ribosomes]({{< relref "ribosome" >}}) assemble protein from mRNA template produced in transcription

    ---


#### [Khan - RNA and Protein Synthesis Review]({{< relref "khan_rna_and_protein_synthesis_review" >}}) {#khan-rna-and-protein-synthesis-review--khan-rna-and-protein-synthesis-review-dot-md}

<!--list-separator-->

-  **🔖 Structure of RNA > Types of RNA**

    |                      |                                                                |
    |----------------------|----------------------------------------------------------------|
    | Ribosomal RNA (rRNA) | Structural component of [ribosomes]({{< relref "ribosome" >}}) |

    ---


#### [Differentiate between rough and smooth endoplasmic reticulum (ER) both in structure and function.]({{< relref "differentiate_between_rough_and_smooth_endoplasmic_reticulum_er_both_in_structure_and_function" >}}) {#differentiate-between-rough-and-smooth-endoplasmic-reticulum--er--both-in-structure-and-function-dot--differentiate-between-rough-and-smooth-endoplasmic-reticulum-er-both-in-structure-and-function-dot-md}

<!--list-separator-->

-  **🔖 Rough endoplasmic reticulum (RER) > Structure**

    _Ribophorins_: receptors where large [ribosomal]({{< relref "ribosome" >}}) subunits bind

    ---


#### [Difference Between Prokaryotic and Eukaryotic Translation]({{< relref "difference_between_prokaryotic_and_eukaryotic_translation" >}}) {#difference-between-prokaryotic-and-eukaryotic-translation--difference-between-prokaryotic-and-eukaryotic-translation-dot-md}

<!--list-separator-->

-  **🔖 Prokaryotic Translation > Elongation**

    Translation elongates until the [ribosome]({{< relref "ribosome" >}}) reaches one of three stop codons (UAA, UGA, UAG)

    ---

<!--list-separator-->

-  **🔖 Prokaryotic Translation > Elongation**

    [Ribosome]({{< relref "ribosome" >}}) binds to the _Shine-Dalgarno sequence_ -> form double-stranded RNA structure

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Rough endoplasmic reticulum (RER) > Structure**

    _Ribophorins_: receptors where large [ribosomal]({{< relref "ribosome" >}}) subunits bind

    ---

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles**

    <!--list-separator-->

    -  [Ribosomes]({{< relref "ribosome" >}})

        <!--list-separator-->

        -  Structure

            -   Consist of _small_ and _large_ subunit
                -   Composed of several types of [rRNA]({{< relref "ribosomal_rna" >}}) and [proteins]({{< relref "protein" >}})
            -   Ribosomes may be **free in the cytosol** or **bound to membranes of the RER or outer nuclear membrane**
            -   _Polyribosome (polysome)_: a cluster of ribosomes along a single strand of [mRNA]({{< relref "messenger_rna" >}}) engaged w/ the synthesis of [protein]({{< relref "protein" >}})

        <!--list-separator-->

        -  Function

            -   **site where mRNA is translated into protein**
                -   Proteins destined for transport -> synthesized in RER
                -   Proteins not destined for transport -> synthesized in cytosol
            -   **In translation**:
                1.  _Small ribosomal unit_ binds [mRNA]({{< relref "messenger_rna" >}}) and activated [tRNAs]({{< relref "transfer_rna" >}}) -> codons of mRNA base-pair with corresponding anticodons of tRNAs
                2.  Initiator tRNA recognizes the start codon (AUG) on the mRNA
                3.  _Large ribosomal subunit_ binds to complex
                    -   _Peptidyl transferase_ in the large subunit catalyzes peptide bond formation -> addition of AAs
                4.  Stop codon (UAA, UAG, UGA) causes release of polypeptide from ribosome -> ribosomal subunits dissociate from mRNA


### Unlinked references {#unlinked-references}

[Show unlinked references]
