+++
title = "Follicle of the lymph node"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Secondary follicle]({{< relref "secondary_follicle" >}}) {#secondary-follicle--secondary-follicle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A kind of [Follicle of the lymph node]({{< relref "follicle_of_the_lymph_node" >}}) that **has a germinal center**

    ---


#### [Primary follicle]({{< relref "primary_follicle" >}}) {#primary-follicle--primary-follicle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A kind of [Follicle of the lymph node]({{< relref "follicle_of_the_lymph_node" >}}) that **does not have a germinal center**

    ---


#### [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}}) {#describe-the-major-actions-occurring-in-these-regions--with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot----describe-the-major-actions-occurring-in-these-regions-with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT**

    <!--list-separator-->

    -  [Follicles]({{< relref "follicle_of_the_lymph_node" >}})

        -   [B-cells]({{< relref "b_lymphocyte" >}}) waiting to recognize antigen flowing in the lymph are located in the cortex in [primary follicles]({{< relref "primary_follicle" >}})
            -   B-cells engulf antigen that binds their receptor -> display peptide form the antigen on their surface
                -   If a previously activated [T-cell]({{< relref "t_lymphocyte" >}}) from the paracortex recognizes the peptide -> T-cell helps B-cell form a [germinal center]({{< relref "germinal_center" >}}) (i.e. [secondary follicle]({{< relref "secondary_follicle" >}})) and try to **make a more specific antibody through hypermutation of the Ig gene**
                -   Unsuccessful B-cells undergo apoptosis
                -   B-cells producing a highly specific [Ig]({{< relref "immunoglobulin" >}}) become plasma cells -> leave lymph node or enter medullary cords -> produce abundant Ig


### Unlinked references {#unlinked-references}

[Show unlinked references]
