+++
title = "Phosphoenolpyruvate"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Pyruvate kinase]({{< relref "pyruvate_kinase" >}}) {#pyruvate-kinase--pyruvate-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Produces [pyruvate]({{< relref "pyruvate" >}}) from [PEP]({{< relref "phosphoenolpyruvate" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis > PEP carboxykinase**

    **Converts [OAA]({{< relref "oxaloacetate" >}}) into [PEP]({{< relref "phosphoenolpyruvate" >}})**

    ---

<!--list-separator-->

-  **🔖 Glycolysis > Pyruvate kinase**

    Produces [pyruvate]({{< relref "pyruvate" >}}) from [phosphoenolpyruvate]({{< relref "phosphoenolpyruvate" >}}) (PEP)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
