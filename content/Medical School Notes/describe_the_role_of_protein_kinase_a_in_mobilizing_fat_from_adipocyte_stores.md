+++
title = "Describe the role of protein kinase A in mobilizing fat from adipocyte stores."
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}


### The role of [PKA]({{< relref "protein_kinase_a" >}}) in mobilizing fat from [adipocytes]({{< relref "adipose_cell" >}}) {#the-role-of-pka--protein-kinase-a-dot-md--in-mobilizing-fat-from-adipocytes--adipose-cell-dot-md}

-   **Triacylglycerol energy reserve is stored in adipocytes in the fed state**
    -   **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**
        -   Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})
        -   Therefore, **cAMP-mediated activation of PKA is required**
-   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
    -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
-   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
-   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-25 Mon] </span></span> > Muscle Intermediary Metabolism II > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the role of protein kinase A in mobilizing fat from adipocyte stores.]({{< relref "describe_the_role_of_protein_kinase_a_in_mobilizing_fat_from_adipocyte_stores" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
