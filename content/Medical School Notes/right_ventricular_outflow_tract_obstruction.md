+++
title = "Right ventricular outflow tract obstruction"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   Inability of blood to pass out of the [right ventricle]({{< relref "right_ventricle" >}}) through the [pulmonic valve]({{< relref "pulmonary_valve" >}}).


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}}) {#tetralogy-of-fallot--tetralogy-of-fallot-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Right ventricular outflow tract obstruction]({{< relref "right_ventricular_outflow_tract_obstruction" >}}) (RVOTO) (due to pulmonary infundibular stenosis)

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Tetralogy of Fallot > Overview**

    <!--list-separator-->

    -  [Right ventricular outflow tract obstruction]({{< relref "right_ventricular_outflow_tract_obstruction" >}}) (RVOTO) (due to pulmonary infundibular stenosis)

        -   Inability of blood to pass out of the right ventricle through the pulmonic valve.
        -   Etiologies include pulmonic valve stenosis, defects in the infundibulum, and defects in the pulmonary artery.


### Unlinked references {#unlinked-references}

[Show unlinked references]
