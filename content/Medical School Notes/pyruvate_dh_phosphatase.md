+++
title = "Pyruvate DH phosphatase"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Upregulation via [pyruvate DH phosphatase]({{< relref "pyruvate_dh_phosphatase" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Pyruvate metabolism > Pyruvate dehydrogenase complex**

    <!--list-separator-->

    -  [Pyruvate DH phosphatase]({{< relref "pyruvate_dh_phosphatase" >}})

        -   Activated by high concentrations of Ca<sup>2+</sup> and Mg<sup>2​+</sup> which **reverses the effects of PDH kinase** (i.e. dephosphorylates PDH)
            -   This **ensures glucose utilization**
        -   Activation by Ca<sup>2+</sup> primarily in skeletal muscle
            -   Contraction leads to Ca<sup>2+</sup> release from cellular stores
        -   Activating effect of Mg<sup>2+</sup> due to **low mitochondrial concentrations of ATP**


### Unlinked references {#unlinked-references}

[Show unlinked references]
