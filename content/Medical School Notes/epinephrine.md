+++
title = "Epinephrine"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Skeletal muscle cell]({{< relref "skeletal_muscle_cell" >}}) {#skeletal-muscle-cell--skeletal-muscle-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept link**

    [Epinephrine]({{< relref "epinephrine" >}}) can mobilize fat stores

    ---


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems**

    [Glucagon]({{< relref "glucagon" >}}) (fasting state) and [epinephrine]({{< relref "epinephrine" >}}) (stress response) signal the most important second messenger system regulating the pathways of intermediary metabolism: **the adenylate cyclase system**

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Mobilization of stored triacylglycerol in muscle**

    These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [Epinephrine]({{< relref "epinephrine" >}})

    ---


#### [Endothelin]({{< relref "endothelin" >}}) {#endothelin--endothelin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Epinephrine]({{< relref "epinephrine" >}})

    ---


#### [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}}) {#describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot--describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Regulation of the mobilization of intramuscular fat**

    <!--list-separator-->

    -  Role of [epinephrine]({{< relref "epinephrine" >}})

        1.  Epinephrine binds to beta-adrenergic receptor
        2.  Activation of G-protein
        3.  Activation of adenylate cyclase
        4.  Convertion of ATP to cAMP
        5.  Activation of [PKA]({{< relref "protein_kinase_a" >}})
        6.  Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Regulation of the mobilization of intramuscular fat**

    These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [Epinephrine]({{< relref "epinephrine" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
