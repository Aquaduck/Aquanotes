+++
title = "Acetyl-CoA"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [β-oxidation]({{< relref "β_oxidation" >}}) {#β-oxidation--β-oxidation-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Removes carbons 1 and 2 as a molecule of [acetyl-CoA]({{< relref "acetyl_coa" >}})

    ---


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Allosterically]({{< relref "allosteric_regulation" >}}) regulated by product inhibition by [acetyl-CoA]({{< relref "acetyl_coa" >}}) & [NADH]({{< relref "nadh" >}}) against their respective enzymes

    ---


#### [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) {#pyruvate-carboxylase--pyruvate-carboxylase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Allosterically activated by [Acetyl-CoA]({{< relref "acetyl_coa" >}})

    ---


#### [List the products of the ß-oxidation of odd-chain fatty acids.]({{< relref "list_the_products_of_the_ß_oxidation_of_odd_chain_fatty_acids" >}}) {#list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot--list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Products of beta-oxidation of odd-chain fatty acids**

    Initial products of odd-chain fatty acid beta-oxidation are [acetyl-CoA]({{< relref "acetyl_coa" >}}) and [propionyl-CoA]({{< relref "propionyl_coa" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    [Acetyl-CoA]({{< relref "acetyl_coa" >}}) from CoA from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)

    ---

<!--list-separator-->

-  **🔖 TCA cycle**

    [Acetyl-CoA]({{< relref "acetyl_coa" >}}) from CoA from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle > Citrate synthase**

    Strictly competitive with [acetyl-CoA]({{< relref "acetyl_coa" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
