+++
title = "Funny channel"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Osmosis]({{< relref "osmosis" >}}) {#osmosis--osmosis-dot-md}

<!--list-separator-->

-  **🔖 Action potentials in pacemaker cells > Phase 4**

    _Sodium_ **moves into cell** through [funny channels]({{< relref "funny_channel" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
