+++
title = "FLICE-inhibitor protein"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Dimerizes with [Caspase-8]({{< relref "caspase_8" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})
-   Restrains [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}) by inhibiting [FLICE]({{< relref "fadd_like_interleukin_1b_converting_enzyme" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  [FLIP]({{< relref "flice_inhibitor_protein" >}})

        <!--list-separator-->

        -  Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024)

            -   FLIP is an inhibitory protein that **restrains the extrinsic pathway** -> inhibits inappropriate activation of apoptosis
            -   FLIP resembles an initiator caspase **but has no protease activity** because it lacks a key cysteine in its active site
            -   FLIP dimerizes with [caspase-8]({{< relref "caspase_8" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})
                -   While caspase-8 appears to be active, it is not cleaved at the site required for its stable activation -> blockage of apoptotic signal


#### [Caspase-8]({{< relref "caspase_8" >}}) {#caspase-8--caspase-8-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Dimerizes with [FLIP]({{< relref "flice_inhibitor_protein" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
