+++
title = "Calcium hydroxyapatite"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Crystalline form of [Calcium]({{< relref "calcium" >}}) found in [Bone]({{< relref "bone" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Histologic components of mature bone**

    [Calcium]({{< relref "calcium" >}}) is bound to the bone in a crystlaline form called [calcium hydroxyapatite]({{< relref "calcium_hydroxyapatite" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
