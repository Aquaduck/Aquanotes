+++
title = "Primitive streak"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Langman's Medical Embryology]({{< relref "langman_s_medical_embryology" >}}) {#langman-s-medical-embryology--langman-s-medical-embryology-dot-md}

<!--list-separator-->

-  **🔖 13: Cardiovascular System > Establishment and Patterning of the Primary Heart Field**

    _Progenitor heart cells_ lie in the [epiblast]({{< relref "epiblast" >}}), immediately adjacent to the **cranial end** of the [primitive streak]({{< relref "primitive_streak" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
