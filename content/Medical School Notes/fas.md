+++
title = "Fas"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A [death receptor]({{< relref "death_receptor" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  **🔖 TRADD and Other Death Receptors**

    [Fas]({{< relref "fas" >}})'s main function is to induce apoptosis via formation of a DISC following binding of [FasL]({{< relref "fas_ligand" >}})

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  [Fas]({{< relref "fas" >}})

        <!--list-separator-->

        -  Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024)

            -   A well-understood example of how death receptors trigger the extrinsic pathway of apoptosis is the activation of Fas on the surface of a target cell by Fas ligand on the surface of a killer (cytotoxic) lymphocyte
                -   Fas activated by [Fas ligand]({{< relref "fas_ligand" >}}) -> death domains on Fas death receptors bind intracellular adaptor proteins -> bind [initiator caspases]({{< relref "initiator_caspase" >}}) (primarily [caspase-8]({{< relref "caspase_8" >}})) -> formation of a [death-inducing signaling complex]({{< relref "death_inducing_signaling_complex" >}}) (DISC)
                -   Once dimerized + activated in [DISC]({{< relref "death_inducing_signaling_complex" >}}), [initiator caspases]({{< relref "initiator_caspase" >}}) cleave their partners -> activate downstreain [executioner caspases]({{< relref "executioner_caspase" >}}) to induce [apoptosis]({{< relref "apoptosis" >}})

            {{< figure src="/ox-hugo/_20210926_143723screenshot.png" >}}


#### [Death receptor]({{< relref "death_receptor" >}}) {#death-receptor--death-receptor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Fas]({{< relref "fas" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
