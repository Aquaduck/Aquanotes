+++
title = "Mesangial cell"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Remove trapped material from basement membrane of the capillaries in the [Glomerulus]({{< relref "glomerulus" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}}) {#explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result--explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Juxtaglomerular apparatus (p. 21)**

    <!--list-separator-->

    -  Extraglomerular [mesangial cells]({{< relref "mesangial_cell" >}})

        -   Morphologically similar to + continuous with the glomerular mesangial cells, **but lie outside the Bowman's capsule**


#### [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}}) {#describe-glomerular-anatomy--ie-efferent-afferent-arteriole----describe-glomerular-anatomy-ie-efferent-afferent-arteriole-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal corpuscle (p. 17)**

    [Mesangial cell]({{< relref "mesangial_cell" >}}): act as phagocytes -> remove trapped material from basement membrane of the capillaries in the [Glomerulus]({{< relref "glomerulus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
