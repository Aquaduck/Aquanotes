+++
title = "Signalosome"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Cell Signalling III - Underlying Design Features]({{< relref "cell_signalling_iii_underlying_design_features" >}}) {#cell-signalling-iii-underlying-design-features--cell-signalling-iii-underlying-design-features-dot-md}

<!--list-separator-->

-  **🔖 Specificity > Location**

    Location and existence of signal micro-domains ([signalosomes]({{< relref "signalosome" >}}))

    ---

<!--list-separator-->

-  **🔖 Location > Cell signaling**

    <!--list-separator-->

    -  [Signalosomes]({{< relref "signalosome" >}})

        -   Assure that signaling molecules/enzymes encounter the **correct substrates in the right place at the right time**
        -   Increase the local concentration of signal molecules, enzymes, substrates -> **↑ efficiency**
        -   Signal molecule/enzyme segregation in a manner that prevents indiscriminate crosstalk/promiscuity/ectopic effects -> **specificity**


### Unlinked references {#unlinked-references}

[Show unlinked references]
