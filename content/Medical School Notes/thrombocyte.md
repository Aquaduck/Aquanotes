+++
title = "Thrombocyte"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Thrombocytosis]({{< relref "thrombocytosis" >}}) {#thrombocytosis--thrombocytosis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A condition where there are too many [platelets]({{< relref "thrombocyte" >}})

    ---


#### [Thrombocytopenia]({{< relref "thrombocytopenia" >}}) {#thrombocytopenia--thrombocytopenia-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Too few [platelets]({{< relref "thrombocyte" >}}) are formed, potentially leading to excessive bleeding

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  Disorders of [Platelets]({{< relref "thrombocyte" >}}) (p. 813)

    -   [Thrombocytosis]({{< relref "thrombocytosis" >}}): too many platelets
        -   Thrombocytosis may lead to the formation of unwanted [blood clots]({{< relref "blood_clotting" >}}) ([thrombosis]({{< relref "thrombosis" >}}))
    -   [Thrombocytopenia]({{< relref "thrombocytopenia" >}}): too few platelets
        -   May lead to blood not clotting properly -> excessive bleeding


### Unlinked references {#unlinked-references}

[Show unlinked references]
