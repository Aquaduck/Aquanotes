+++
title = "Obstructive lung disease"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A class of [lung]({{< relref "lung" >}}) disease


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [COPD]({{< relref "copd" >}}) {#copd--copd-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A family of [Obstructive lung disease]({{< relref "obstructive_lung_disease" >}})

    ---


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  Findings of [obstructive]({{< relref "obstructive_lung_disease" >}}) vs. [restrictive lung disease]({{< relref "restrictive_lung_disease" >}})

        | Measurement                                   | Obstructive lung disease                                             | Restrictive lung disease                             |
        |-----------------------------------------------|----------------------------------------------------------------------|------------------------------------------------------|
        | [FEV1]({{< relref "fev1" >}})                 | Low                                                                  | Low or normal                                        |
        | FEV1/FVC                                      | Low                                                                  | Normal or high                                       |
        | [VC]({{< relref "vital_capacity" >}})         | Low                                                                  | Low                                                  |
        | [A-a gradient]({{< relref "a_a_gradient" >}}) | High                                                                 | Normal (extrinsic causes) or high (intrinsic causes) |
        | Lung compliance                               | Normal (may be increased in [emphysema]({{< relref "emphysema" >}})) | Normal (extrinsic causes) or low (intrinsic causes)  |

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Obstructive lung disease]({{< relref "obstructive_lung_disease" >}})

        -   Increased resistance to airflow caused by narrowing of airways

        <!--list-separator-->

        -  Etiology

            -   [COPD]({{< relref "copd" >}})
            -   Bronchial asthma
            -   Bronchiectasis, [cystic fibrosis]({{< relref "cystic_fibrosis" >}})
            -   Narrowing of extrathoracic airways: laryngeal tumors, vocal cord palsy


### Unlinked references {#unlinked-references}

[Show unlinked references]
