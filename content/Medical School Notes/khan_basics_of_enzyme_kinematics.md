+++
title = "Khan - Basics of enzyme kinematics"
author = ["Arif Ahsan"]
date = 2021-07-13T00:00:00-04:00
tags = ["medschool", "khan", "source"]
draft = false
+++

## Basic [enzyme]({{< relref "enzyme" >}}) kinetics graphs {#basic-enzyme--enzyme-dot-md--kinetics-graphs}

{{< figure src="/ox-hugo/_20210713_154751screenshot.png" caption="Figure 1: Graph of hypothetical enzyme activity" width="400" >}}


## [Enzyme]({{< relref "enzyme" >}}) kinetics inhibitors {#enzyme--enzyme-dot-md--kinetics-inhibitors}

-   _Competitive inhibitors_: impair reaction by **binding to an enzyme and preventing the real substrate from binding**
    -   Often binds at the active site
-   _Noncompetitive inhibitors_: **does not impact substrate binding**, but binds elsewhere and prevents enzyme function

    {{< figure src="/ox-hugo/_20210713_155056screenshot.png" caption="Figure 2: Difference in rate of reaction between normal enzymes, competitive inhibition, and noncompetitive inhibition" width="400" >}}


### With a competitive inhibitor {#with-a-competitive-inhibitor}

-   Reaction is able to reach its normal V<sub>max</sub>, but takes a higher concentration of substrate to get there
    -   V<sub>max</sub> is unchanged, but apparenty K<sub>m</sub> is higher.
-   Why must more substrate be added in order to reach V<sub>max</sub>?
    -   Extra substrate makes substrate molecules abundant enough to consistently "beat" inhibitor molecules to the enzyme


### With a noncompetitive inhibitor {#with-a-noncompetitive-inhibitor}

-   Reaction never reaches its normal V<sub>max</sub>, regardless of how much substrate
    -   Due to inhibition, effective concentration of enzyme is reduced
-   Reaction reaches half if its new V<sub>max</sub> at the same substrate concentration -> **K<sub>m</sub> is unchanged**
    -   This is because the inhibitor doesn't affect binding of enzyme to substrate, just lowers concentration of usable enzyme


## Michaelis-Menten and allosteric enzymes {#michaelis-menten-and-allosteric-enzymes}


### [Michaelis-Menten Kinetics]({{< relref "michaelis_menten_kinetics" >}}) {#michaelis-menten-kinetics--michaelis-menten-kinetics-dot-md}

-   _Michaelis-Menten enzymes_: enzymes that behave according to Michaelis-Menten kinetics


### Allosteric enzymes {#allosteric-enzymes}

-   Have multiple activation sites and display _cooperativity_
    -   Binding of a substrate at one active site increases ability of other active sites to bind
-   Diplay a "switch-like" transition from low to high reaction rate as substrate concentration increases:

{{< figure src="/ox-hugo/_20210713_184431screenshot.png" caption="Figure 3: \"Switch-like\" transition in allosteric enzymes" width="400" >}}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Khan - Enzyme Regulation]({{< relref "khan_enzyme_regulation" >}}) {#khan-enzyme-regulation--khan-enzyme-regulation-dot-md}

<!--list-separator-->

-  **🔖 Regulatory molecules > Alloesteric regulation**

    [Allosteric enzymes]({{< relref "khan_basics_of_enzyme_kinematics" >}}): allosterically regulated enzymes with unique properties

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
