+++
title = "Fructose 1,6-bisphosphatase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    <!--list-separator-->

    -  [Fructose 1,6-bisphosphatase]({{< relref "fructose_1_6_bisphosphatase" >}})

        -   Catalyzes **hydrolysis of F-1,6-bP to F6P**
        -   Reverses [PFK-1]({{< relref "6_phosphofructo_1_kinase" >}})
        -   Allosterically inhibited by F-2,6-bP and AMP


### Unlinked references {#unlinked-references}

[Show unlinked references]
