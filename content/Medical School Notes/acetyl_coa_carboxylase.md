+++
title = "Acetyl-CoA carboxylase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    Glucagon affects lipid metabolism in liver by initiating the PKA-catalyzed phosphorylation and inactivation of [acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}}) (catalyzes rate-limiting and committed step in faty acid synthesis)

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Hormone-sensitive lipase**

    Undergoes reciptrocal regulation with intracellular [acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}})

    ---

<!--list-separator-->

-  **🔖 Fatty acid metabolism**

    <!--list-separator-->

    -  [Acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}})

        -   **Anabolic** cytosolic enzyme
        -   Catalyzes **rate-limiting and committed step of fatty acid synthesis**
            -   ATP-dependent carboxylation of acetyl-CoA to malonyl-CoA
            -   [Malonyl-CoA]({{< relref "malonyl_coa" >}}) binds to and inhibits [carnitine palmitoyl acyltransferase 1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) (CPT1) -> prevents synthesized fatty acids from entering mitochondria and being oxidized
        -   Requires [biotin]({{< relref "biotin" >}})
        -   [PKA]({{< relref "protein_kinase_a" >}}) substrate and **downregulated by phosphorylation**
            -   Dephosphorylation by _insulin-induced phosphoprotein phosphatases_ renews its activity


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By enzyme**

    <!--list-separator-->

    -  [Acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}})

        -   Biotin (B7) is a cofactor of carboxylases


#### [Biotin]({{< relref "biotin" >}}) {#biotin--biotin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    [Acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}}): acetyl-CoA (2C) -> malonyl-CoA (3C)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
