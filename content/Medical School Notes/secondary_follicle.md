+++
title = "Secondary follicle"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A kind of [Follicle of the lymph node]({{< relref "follicle_of_the_lymph_node" >}}) that **has a germinal center**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}}) {#describe-the-major-actions-occurring-in-these-regions--with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot----describe-the-major-actions-occurring-in-these-regions-with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Follicles**

    If a previously activated [T-cell]({{< relref "t_lymphocyte" >}}) from the paracortex recognizes the peptide -> T-cell helps B-cell form a [germinal center]({{< relref "germinal_center" >}}) (i.e. [secondary follicle]({{< relref "secondary_follicle" >}})) and try to **make a more specific antibody through hypermutation of the Ig gene**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
