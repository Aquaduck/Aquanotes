+++
title = "Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat."
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}


### Regulation of the mobilization of [intramuscular]({{< relref "muscle" >}}) fat {#regulation-of-the-mobilization-of-intramuscular--muscle-dot-md--fat}

-   Skeletal muscle obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose
    -   These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [Epinephrine]({{< relref "epinephrine" >}})

{{< figure src="/ox-hugo/_20211024_203003screenshot.png" caption="Figure 1: Fatty acid mobilization mediated via skeletal muscle contraction vs. epinephrine" width="500" >}}


#### Role of [skeletal muscle]({{< relref "skeletal_muscle" >}}) contraction {#role-of-skeletal-muscle--skeletal-muscle-dot-md--contraction}

1.  Nerve impulse via [acetylcholine]({{< relref "acetylcholine" >}}) to acetylcholine receptor
2.  Depolarization
3.  Stimulation of ER to release [Calcium]({{< relref "calcium" >}})
4.  Activation of [PKC]({{< relref "protein_kinase_c" >}})
5.  Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol


#### Role of [epinephrine]({{< relref "epinephrine" >}}) {#role-of-epinephrine--epinephrine-dot-md}

1.  Epinephrine binds to beta-adrenergic receptor
2.  Activation of G-protein
3.  Activation of adenylate cyclase
4.  Convertion of ATP to cAMP
5.  Activation of [PKA]({{< relref "protein_kinase_a" >}})
6.  Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-25 Mon] </span></span> > Muscle Intermediary Metabolism II > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
