+++
title = "Initiator caspase"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A class of [caspases]({{< relref "caspase" >}})
-   main function is to activate [executioner caspases]({{< relref "executioner_caspase" >}}) via cleavage of inactive executioner caspase dimers


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Fas > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    Once dimerized + activated in [DISC]({{< relref "death_inducing_signaling_complex" >}}), [initiator caspases]({{< relref "initiator_caspase" >}}) cleave their partners -> activate downstreain [executioner caspases]({{< relref "executioner_caspase" >}}) to induce [apoptosis]({{< relref "apoptosis" >}})

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Fas > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    Fas activated by [Fas ligand]({{< relref "fas_ligand" >}}) -> death domains on Fas death receptors bind intracellular adaptor proteins -> bind [initiator caspases]({{< relref "initiator_caspase" >}}) (primarily [caspase-8]({{< relref "caspase_8" >}})) -> formation of a [death-inducing signaling complex]({{< relref "death_inducing_signaling_complex" >}}) (DISC)

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Caspase**

    <!--list-separator-->

    -  [Initiator caspases]({{< relref "initiator_caspase" >}})

        -   Begin apoptosis, main function being to **activate executioner caspases**
        -   Normally exist as inactive, soluble monomers in the cytosol
        -   An apoptotic signal triggers the assembly of large protein platofrms -> formation of large complexes of initiator caspases
            -   Pairs of caspases associate to form dimers -> **protease activation**
                -   Each caspase in the dimer cleaves its partner at a specific site in the protease domain -> stabilizes active complex and required for proper function of the enzyme in the cell


#### [Caspase-8]({{< relref "caspase_8" >}}) {#caspase-8--caspase-8-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [Initiator caspase]({{< relref "initiator_caspase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
