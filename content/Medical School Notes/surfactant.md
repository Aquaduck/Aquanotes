+++
title = "Surfactant"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Type 2 alveolar cell]({{< relref "type_2_alveolar_cell" >}}) {#type-2-alveolar-cell--type-2-alveolar-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Synthesizes [surfactant]({{< relref "surfactant" >}})

    ---


#### [Name the stages of lung development, their approximate times of development, and explain the major changes in each stage.]({{< relref "name_the_stages_of_lung_development_their_approximate_times_of_development_and_explain_the_major_changes_in_each_stage" >}}) {#name-the-stages-of-lung-development-their-approximate-times-of-development-and-explain-the-major-changes-in-each-stage-dot--name-the-stages-of-lung-development-their-approximate-times-of-development-and-explain-the-major-changes-in-each-stage-dot-md}

<!--list-separator-->

-  **🔖 From Embryology - Thorax Part 1: Thoracic Cavity & Lung Development PPT > Phases of lung growth: > Alveolar period (32-36 weeks to ~3 years post-natal):**

    [Type II pneumocytes]({{< relref "type_2_alveolar_cell" >}}) increase -> increased [surfactant]({{< relref "surfactant" >}}) production

    ---

<!--list-separator-->

-  **🔖 From Embryology - Thorax Part 1: Thoracic Cavity & Lung Development PPT > Phases of lung growth: > Alveolar period (32-36 weeks to ~3 years post-natal):**

    Increased fetal plasma cortisol -> increased secretion of [surfactant]({{< relref "surfactant" >}}) (progressive increase from 32 weeks)

    ---

<!--list-separator-->

-  **🔖 From Embryology - Thorax Part 1: Thoracic Cavity & Lung Development PPT > Phases of lung growth: > Canalicular period (17 - 28 weeks):**

    **Appearance of [type II alveolar cells]({{< relref "type_2_alveolar_cell" >}})** -> **[surfactant]({{< relref "surfactant" >}}) synthesis**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
