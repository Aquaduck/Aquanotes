+++
title = "Introduction to Metabolism and Glycolysis"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

Link:  <https://vicportal.med.uvm.edu/bbcswebdav/pid-34324-dt-content-rid-134700%5F1/courses/CC%5F2021-1/July%2012%20Anabolism%20vs%20Catabolism.pdf>


## Catabolic pathways {#catabolic-pathways}

-   **Release energy**
-   Serve to capture chemical energy in the form of [ATP]({{< relref "atp" >}})
-   Allows molecules in diet to be converted into building blocks for complex molecules
-   Convergent process (wide variety of molecules transformed into few common products)


### Three stages of catabolism {#three-stages-of-catabolism}

{{< figure src="/ox-hugo/_20210715_121046screenshot.png" >}}

1.  **Hydrolysis of complex molecules**
2.  **Conversion of building blocks to simple intermediates**
3.  **Oxidation of acetyl coenzyme A**


## Anabolic pathways {#anabolic-pathways}

-   **Require energy**
-   Combine small molecules to form complex molecules such as [proteins]({{< relref "protein" >}})
-   Often involve **chemical reductions** where reducing power provided by electron donor NADPH
-   Divergent process (few precursors form wide variety of products)


## Comparison of anabolic vs catabolic pathways {#comparison-of-anabolic-vs-catabolic-pathways}

{{< figure src="/ox-hugo/_20210715_121213screenshot.png" >}}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Cellular Metabolism > 1. Describe the relationship between anabolism, catabolism, and chemical energy**

    [Anabolic pathways]({{< relref "introduction_to_metabolism_and_glycolysis" >}})

    ---

<!--list-separator-->

-  **🔖 Cellular Metabolism > 1. Describe the relationship between anabolism, catabolism, and chemical energy**

    [Catabolic pathways]({{< relref "introduction_to_metabolism_and_glycolysis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
