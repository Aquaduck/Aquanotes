+++
title = "Type I error"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Significance level]({{< relref "significance_level" >}}) {#significance-level--significance-level-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The probability of a [Type 1 error]({{< relref "type_i_error" >}})

    ---


#### [Compare and contrast type I error and type II error]({{< relref "compare_and_contrast_type_i_error_and_type_ii_error" >}}) {#compare-and-contrast-type-i-error-and-type-ii-error--compare-and-contrast-type-i-error-and-type-ii-error-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Type I error]({{< relref "type_i_error" >}})

        -   **False positive** -> observed effect is actually due to chance
        -   Null hypothesis is rejected when it is actually true


### Unlinked references {#unlinked-references}

[Show unlinked references]
