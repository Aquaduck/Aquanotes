+++
title = "Glucose"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Hexokinase]({{< relref "hexokinase" >}}) {#hexokinase--hexokinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Catalyzes ATP-dependent phosphorylation of [glucose]({{< relref "glucose" >}}) to [G6P]({{< relref "glucose_6_phosphate" >}}) to trap it within a cell

    ---


#### [Glucokinase]({{< relref "glucokinase" >}}) {#glucokinase--glucokinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Has cooperativity with [[glucose]({{< relref "glucose" >}})]

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    E.g. [Sodium]({{< relref "sodium" >}}), [Potassium]({{< relref "potassium" >}}), [Chloride]({{< relref "chloride" >}}), [Bicarbonate]({{< relref "bicarbonate" >}}), [Glucose]({{< relref "glucose" >}}), [Urea]({{< relref "urea" >}}), [amino acids]({{< relref "amino_acid" >}}), peptides such as [Insulin]({{< relref "insulin" >}}) and [ADH]({{< relref "antidiuretic_hormone" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
