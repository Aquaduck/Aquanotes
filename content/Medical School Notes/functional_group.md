+++
title = "Functional Group"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

A **functional group** is a specific grouping of atoms within a molecule that has its own characteristic properties, regardless of other atoms present in a molecule


## Summary of Functional Groups {#summary-of-functional-groups}

Table of functional groups ordered by priority:

<div class="table-caption">
  <span class="table-number">Table 1</span>:
  *Note: Alkene and Alkyne are both tied for priority except in cyclic compounds
</div>

| Functional Group | Prefix               | Suffix    |
|------------------|----------------------|-----------|
| Carboxylic acid  | carboxy-             | -oic acid |
| Anhydride        | alkanoyloxycarbonyl- | anhydride |
| Ester            | alkoxycarbonyl-      | -oate     |
| Amide            | carbamoyl- or amido- | -amide    |
| Aldehyde         | oxo-                 | -al       |
| Ketone           | oxo- or keto-        | -one      |
| Alcohol          | hydroxy-             | -ol       |
| Alkene\*         | alkenyl-             | -ene      |
| Alkyne\*         | alkynyl-             | -yne      |
| Alkane           | alkyl-               | -ane      |


## Backlinks {#backlinks}


### 12 linked references {#12-linked-references}


#### [Ketone]({{< relref "ketone" >}}) {#ketone--ketone-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


#### [Kaplan Organic Chemistry Chapter 1]({{< relref "kaplan_organic_chemistry_chapter_1" >}}) {#kaplan-organic-chemistry-chapter-1--kaplan-organic-chemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 1.2 Hydrocarbons and Alcohols**

    <!--list-separator-->

    -  [Alkenes and Alkynes]({{< relref "functional_group" >}})

        -   Double and triple bonds, respectively


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Organic Chemistry > 1. Identify the functional groups present in an organic compounds, given its structure**

    [Functional Group]({{< relref "functional_group" >}})

    ---


#### [Ether]({{< relref "ether" >}}) {#ether--ether-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


#### [Ester]({{< relref "ester" >}}) {#ester--ester-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


#### [Carboxylic Acid]({{< relref "carboxylic_acid" >}}) {#carboxylic-acid--carboxylic-acid-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


#### [Anhydride]({{< relref "anhydride" >}}) {#anhydride--anhydride-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


#### [Amine]({{< relref "amine" >}}) {#amine--amine-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


#### [Amide]({{< relref "amide" >}}) {#amide--amide-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


#### [Alkane]({{< relref "alkane" >}}) {#alkane--alkane-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


#### [Aldehyde]({{< relref "aldehyde" >}}) {#aldehyde--aldehyde-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


#### [Alcohol]({{< relref "alcohol" >}}) {#alcohol--alcohol-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "functional_group" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
