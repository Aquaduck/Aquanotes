+++
title = "NFκB"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Tumor necrosis factor]({{< relref "tumor_necrosis_factor" >}}) {#tumor-necrosis-factor--tumor-necrosis-factor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Default signaling pathway is to bind to [TNFR1]({{< relref "tnfr1" >}}) and induce [NFκB]({{< relref "nfκb" >}})

    ---


#### [TNFR1]({{< relref "tnfr1" >}}) {#tnfr1--tnfr1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Regular function is to induce [NFκB]({{< relref "nfκb" >}}) and promote cell survival and growth

    ---


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  **🔖 TRADD in TNF/TNFR1 signalling**

    TNFR1 induces cell death (apoptosis AND necrosis) **only when [NFκB]({{< relref "nfκb" >}}) activation is impaired**

    ---

<!--list-separator-->

-  **🔖 TRADD in TNF/TNFR1 signalling**

    The default signaling pathway activating by the binding of TNF alpha is the induction of [NFκB]({{< relref "nfκb" >}}) -> responsible for **cell survival and growth**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
