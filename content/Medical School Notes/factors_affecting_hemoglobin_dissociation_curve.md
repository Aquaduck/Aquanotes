+++
title = "Factors Affecting Hemoglobin Dissociation Curve"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Notes {#notes}

-   Hemoglobin is the primary O<sub>2</sub> carrier in the body


### Factors affecting O<sub>2</sub>-[Hb]({{< relref "hemoglobin" >}}) Dissociation Curve: {#factors-affecting-o-hb--hemoglobin-dot-md--dissociation-curve}

{{< figure src="/ox-hugo/_20210816_211337screenshot.png" caption="Figure 1: Percent saturation of Hemoglobin vs. Partial pressure of Oxygen" width="600" >}}


#### pH {#ph}

-   Exercising tissue contains cells that produce more CO<sub>2</sub>
    -   When CO<sub>2</sub> leaves the tissue and enters the [red blood cells]({{< relref "red_blood_cell" >}}) of the capillaries, it is readily converted into _bicarbonate_ and _hydrogen ions_ -> **decreases pH**
-   H<sup>+</sup> can bind onto allosteric regions of hemoglobin -> **decrease in O<sub>2</sub> affinity**
    -   Allows more O<sub>2</sub> to be delivered to the tissues <- _[Bohr Effect]({{< relref "bohr_effect" >}})_
-   A decrease in pH shifts the O<sub>2</sub>-Hb curve **to the right**


#### Temperature {#temperature}

-   [Cells]({{< relref "cell" >}}) that have a high rate of metabolism produce more thermal energy as a byproduct
    -   This thermal energy transfers to the blood plasma -> **raises temperature of blood**
-   Increase in temperature -> **decrease affinity of [hemoglobin]({{< relref "hemoglobin" >}}) to oxygen** -> more likely for **hemoglobin to release oxygen** to the **exercising cells** of the tissue
-   Increase of temperature shifts O<sub>2</sub>-Hb curve **to the right**


#### [Carbon Dioxide]({{< relref "carbon_dioxide" >}}) {#carbon-dioxide--carbon-dioxide-dot-md}

-   Metabolically active cells of our tissue produce more CO<sub>2</sub> as a waste byproduct
    -   Some of the CO<sub>2</sub> binds directly to hemoglobin -> **decreased affinity to oxygen**
-   **A higher ppCO<sub>2</sub> (i.e. [CO<sub>2</sub>]) in the blood -> more O<sub>2</sub> delivered to tissues**
    -   The _Haldane Effect_


#### [2,3-BPG]({{< relref "2_3_biphosphoglycerate" >}}) {#2-3-bpg--2-3-biphosphoglycerate-dot-md}

-   Naturally occurring intermediate in glycolysis
-   High rate of metabolism -> **produce excess 2,3-BPG**
    -   Some of it escapes into blood plasma -> **binds to hemoglobin and lowers its affinity for oxygen**
        -   More O<sub>2</sub> for tissues
-   ↑[2,3-BPG] -> shifts O<sub>2</sub>-Hb curve **to the right**


#### [Carbon Monoxide]({{< relref "carbon_monoxide" >}}) {#carbon-monoxide--carbon-monoxide-dot-md}

-   Carbon monoxide is a **competitive inhibitor of hemoglobin**
    -   250x more likely to bind to heme group
-   By binding to hemoglobin, **CO causes it to increase its affinity for O<sub>2</sub> -> less oxygen delivered to cells of tissue**
-   Causes a **leftward shift** in the curve _and_ **lowers the curve** because it **lowers hemoglobins O<sub>2</sub>-carrying capacity**


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
