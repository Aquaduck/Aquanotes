+++
title = "Phosphodiesterase"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Degradation of dietary nucleic acids in the small intestine**

    [Oligonucleotides]({{< relref "oligonucleotide" >}}) are further hydrolyzed by pancreatic [phosphodiesterases]({{< relref "phosphodiesterase" >}}) -> produces a mixture of 3' and 5'-mononucleotides

    ---


#### [Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important.]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}}) {#describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot--describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Off Signals > Several pathways in the cell to turn off a signal:**

    <!--list-separator-->

    -  Get rid of second messenger - [phosphodiesterases]({{< relref "phosphodiesterase" >}}) (PDE)

        -   You can **degrade the intracellular signal molecule** e.g. cAMP degradation via [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
