+++
title = "GTPase"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the general mechanisms of packaging cargo into vesicles.]({{< relref "describe_the_general_mechanisms_of_packaging_cargo_into_vesicles" >}}) {#describe-the-general-mechanisms-of-packaging-cargo-into-vesicles-dot--describe-the-general-mechanisms-of-packaging-cargo-into-vesicles-dot-md}

<!--list-separator-->

-  **🔖 From Organelles and Trafficking I-II > Role of GTPases in coating/uncoating cycle**

    [GTPases]({{< relref "gtpase" >}}) **regulate** cycles of vesicle coating/uncoating

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
