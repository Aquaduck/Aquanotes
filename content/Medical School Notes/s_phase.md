+++
title = "S phase"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes**

    6 billion replicated during [S phase]({{< relref "s_phase" >}}) of the [cell cycle]({{< relref "cell_cycle" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
