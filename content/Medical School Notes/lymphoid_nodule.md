+++
title = "Lymphoid nodule"
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  [Lymphoid Nodules]({{< relref "lymphoid_nodule" >}}) (p. 997)

    -   Consist of a dense cluster of lymphocytes without a surrounding fibrous capsules
    -   Located in the respiratory and digestive tracts - areas routinely exposed to environmental pathogens

    <!--list-separator-->

    -  [Tonsils]({{< relref "tonsil" >}})

        -   Lymphoid nodules located along the inner surface of the pharynx
        -   Important in developing immunity to oral pathogens
        -   The tonsil located at the back of the throat (_pharyngeal tonsil_) is sometimes referred to as the _adenoid_ when swollen
        -   Epithelial layer invaginates deeply into the interior of the tonsil to form _tonsillar crypts_
            -   Accumulate materials taken into the body through eating and breathing
            -   A "honey pot" for pathogens
        -   **Main function**: help children's bodies recognize, destroy, and develop immunity to common environmental pathogens

    <!--list-separator-->

    -  [MALT]({{< relref "mucosa_associated_lymphoid_tissue" >}})

        -   An aggregate of lymphoid follicles directly assoicated with the mucous membrane epithelia
        -   Dome-shaped structures found underlying mucosa of:
            -   GI tract
            -   Breast tissue
            -   Lungs
            -   Eyes
        -   _Peyer's patches_: contain specialized endothelial cells (called _M (microfold) cells_) -> sample material from intestinal lumen -> transport to nearby follicles -> allows adaptive immune response to potential pathogens

        {{< figure src="/ox-hugo/_20211018_141838screenshot.png" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
