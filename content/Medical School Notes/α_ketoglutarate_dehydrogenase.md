+++
title = "α-ketoglutarate dehydrogenase"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Requires [NAD<sup>+</sup>]({{< relref "nadh" >}})
-   Mechanism is analogous to that of the [pyruvate DH complex]({{< relref "pyruvate_dehydrogenase_complex" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Thiamine]({{< relref "thiamine" >}}) {#thiamine--thiamine-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry - Nutrition > Function**

    [α-ketoglutarate dehydrogenase]({{< relref "α_ketoglutarate_dehydrogenase" >}}) ([TCA cycle]({{< relref "citric_acid_cycle" >}}))

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 TCA cycle**

    [α-ketoglutarate DH]({{< relref "α_ketoglutarate_dehydrogenase" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle**

    <!--list-separator-->

    -  NAD<sup>+</sup>-linked [α-ketoglutarate dehydrogenase]({{< relref "α_ketoglutarate_dehydrogenase" >}})

        -   Allostericaly regulated by **product inhibition**
            -   i.e. succinyl-CoA and NADH
        -   Mechanism is **analogous to that of the [pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}})**
            -   Consists of 3 enzymes
                -   _E1_ accepts a 5C acid rather than a 3C acid
                -   _E2_ and _E3_ are identical to those found in the pyruvate DH complex
                -   α-ketoglutarate DH is NOT subject to covalent modification by a tightly-associated kinase


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By enzyme**

    <!--list-separator-->

    -  [alpha-KG DH]({{< relref "α_ketoglutarate_dehydrogenase" >}})

        -   Thiamine (B1) (as thiamine pyrophosphate) is a cofactor
        -   Riboflavin (B2) in the form of FAD
        -   Niacin (B3) in the form of NAD<sup>+</sup>
        -   Pantothenic acid (B5) in the form of CoA


### Unlinked references {#unlinked-references}

[Show unlinked references]
