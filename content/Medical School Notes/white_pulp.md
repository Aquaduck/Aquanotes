+++
title = "White pulp"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A component of the [splenic]({{< relref "spleen" >}}) parenchyma which contains mostly [lymphocytes]({{< relref "lymphocyte" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen > Splenic function**

    [White pulp]({{< relref "white_pulp" >}}) responds to bloodborne antigens similarly to how the lymph nodes respond to antigen in the lymph

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen**

    The splenic parenchyma contains two interspersed components: [red pulp]({{< relref "red_pulp" >}}) and [white pulp]({{< relref "white_pulp" >}}) (named for their fresh unstained color)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
