+++
title = "Lipolysis"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The breakdown of [triglycerides]({{< relref "triacylglycerol" >}}) into [glycerol]({{< relref "glycerol" >}}) and [fatty acids]({{< relref "fatty_acid" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  [Fatty acid catabolism]({{< relref "lipolysis" >}})

        -   Can be divided into **3 stages**:
            1.  _Investment_ to prepare them for ->
            2.  _Transport_ into the mitochondrial matrix wherein they will undergo ->
            3.  _Oxidation_

        {{< figure src="/ox-hugo/_20211024_192624screenshot.png" width="700" >}}

        <!--list-separator-->

        -  Investment phase

            -   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
                -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
                -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**

        <!--list-separator-->

        -  Transport phase

            -   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_
            -   Fatty aycl-carnitine ester is formed by the action of _carnitine acyltransferase I_ once inside the inner membrane space
                -   Becomes a substrate for _carnitine acyltransferase II_ -> catalyzes transfer of acyl to CoA-SH in matrix -> release carnitine for a second round of fatty acid transport
            -   **Rate-limiting and commits the fatty acyl-CoA to oxidation in the matrix**

            {{< figure src="/ox-hugo/_20211024_193058screenshot.png" width="700" >}}

        <!--list-separator-->

        -  Oxidation phase

            -   Occurs in three stages:
                1.  Beta-oxidation
                2.  Oxidation of acetyl-CoA in TCA cycle -> 3 NADH, 1 FADH<sub>2</sub>, 1 GTP
                3.  ETC -> 9 ATP
                    -   Electrons from 3 NADH and 1 FADH<sub>2</sub>
            -   **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**
                -   Increased [propionic acid] = _biotin deficiency_
                -   Increased [methylmalonic acid] = _vitamin B12 deficiency_
                -   Leads to **mental deficits**

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Mobilization of triacylglycerol stored in adipocytes**

    Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})

    ---


#### [Describe the role of protein kinase A in mobilizing fat from adipocyte stores.]({{< relref "describe_the_role_of_protein_kinase_a_in_mobilizing_fat_from_adipocyte_stores" >}}) {#describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot--describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > The role of PKA in mobilizing fat from adipocytes**

    Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
