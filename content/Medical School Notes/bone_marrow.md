+++
title = "Bone marrow"
author = ["Arif Ahsan"]
date = 2021-08-17T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Yellow marrow]({{< relref "yellow_marrow" >}}) {#yellow-marrow--yellow-marrow-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [bone marrow]({{< relref "bone_marrow" >}}) that contains adipocytes

    ---


#### [Red marrow]({{< relref "red_marrow" >}}) {#red-marrow--red-marrow-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [bone marrow]({{< relref "bone_marrow" >}})

    ---


#### [Red blood cell]({{< relref "red_blood_cell" >}}) {#red-blood-cell--red-blood-cell-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Produced in [bone marrow]({{< relref "bone_marrow" >}})

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Organization of bones**

    Remaining non-bony inside part is called [bone marrow]({{< relref "bone_marrow" >}}) or _medullary cavity_

    ---


#### [Hematopoietic stem cell]({{< relref "hematopoietic_stem_cell" >}}) {#hematopoietic-stem-cell--hematopoietic-stem-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Found in [bone marrow]({{< relref "bone_marrow" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
