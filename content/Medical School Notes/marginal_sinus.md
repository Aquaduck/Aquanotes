+++
title = "Marginal sinus"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Where afferent vessels enter the [Lymph node]({{< relref "lymph_node" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Immunology**

    [Marginal sinus]({{< relref "marginal_sinus" >}}) is where afferent vessels enter the [lymph node]({{< relref "lymph_node" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
