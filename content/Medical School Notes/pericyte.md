+++
title = "Pericyte"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}}) {#from-brs-cell-biology-and-histology--brs-cell-biology-histology-dot-md}

-   **Pericytes** are derived from embryonic mesenchymal cells and may retain a pluripotential role
    -   Possess characteristics of endothelial cells as well as smooth muscle cells
        -   Contain [actin]({{< relref "actin" >}}), [myosin]({{< relref "myosin" >}}), and [tropomyosin]({{< relref "tropomyosin" >}}) -> suggests they play a role in contraction
            -   This + proximity to capillaries -> could function to **modify capillary blood flow**
    -   Smaller than fibroblasts and **are located along most capillaries**
        -   Lie within their own basal lamina
    -   During blood vessel formation and repair, they may differentiate into smooth muscle cells or endothelial cells
    -   In response to injury, may give rise to endothelial cells, fibroblasts, and smooth muscle cells of blood vessel walls


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
