+++
title = "Renal medulla"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Renal cortex]({{< relref "renal_cortex" >}}) {#renal-cortex--renal-cortex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Surrounds the [renal medulla]({{< relref "renal_medulla" >}}) and extends inward

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
