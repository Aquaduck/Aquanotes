+++
title = "ScienceDirect - Null Hypothesis"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "sciencedirect", "papers", "source"]
draft = false
+++

Link: <https://www.sciencedirect.com/topics/earth-and-planetary-sciences/null-hypothesis>


## [Overview]({{< relref "biostatistics" >}}) {#overview--biostatistics-dot-md}

-   The _null hypothesis_ suggests that **no statistical relationship and significance exists between two sets of observed data**
-   Symbolized by \\(H\_{0}\\)


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
