+++
title = "Virus"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the etiology of herpes zoster as well as the clinical presentation of the disease.]({{< relref "describe_the_etiology_of_herpes_zoster_as_well_as_the_clinical_presentation_of_the_disease" >}}) {#describe-the-etiology-of-herpes-zoster-as-well-as-the-clinical-presentation-of-the-disease-dot--describe-the-etiology-of-herpes-zoster-as-well-as-the-clinical-presentation-of-the-disease-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Herpes zoster**

    Caused by _Varicella zoster_ [virus]({{< relref "virus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
