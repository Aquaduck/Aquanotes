+++
title = "Edema"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Plasma Proteins (p. 796) > Albumin**

    Low serum albumin leads to [edema]({{< relref "edema" >}})

    ---


#### [Name and describe the pathways of lymphatic drainage (in a lymph node, in the body).]({{< relref "name_and_describe_the_pathways_of_lymphatic_drainage_in_a_lymph_node_in_the_body" >}}) {#name-and-describe-the-pathways-of-lymphatic-drainage--in-a-lymph-node-in-the-body--dot--name-and-describe-the-pathways-of-lymphatic-drainage-in-a-lymph-node-in-the-body-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymph**

    Fluid accumulation, clinically called [edema]({{< relref "edema" >}}), is a frequent syndrome in patients with **cardiovascular, renal, and protein abnormalities**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
