+++
title = "Amboss"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

Inbox for amboss notes that haven't been connected to a learning objective


## Respiration {#respiration}


### [Macula densa]({{< relref "macula_densa" >}}) {#macula-densa--macula-densa-dot-md}

-   A specialized group of tall cuboidal cells in the [distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}}) of the kidney
-   Part of the [Juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}})
-   Senses [Sodium]({{< relref "sodium" >}}) and Chloride levels and controls [Renin]({{< relref "renin" >}}) release, glomerular filtration, and renal blood flow via signaling pathways
    -   Includes COX-2 mediated prostaglandin secretion (_tubuloglomerular feedback_)


#### Feedback mechanisms {#feedback-mechanisms}

-   In response to **increased NaCl**
    -   Triggers release of [Adenosine]({{< relref "adenosine" >}}) -> vasoconstriction of the [afferent arteriole]({{< relref "afferent_arteriole_of_the_kidney" >}}) -> **decrease in glomerular filtration rate**
-   In reponse to **decreased NaCl**
    -   Triggers release of [Renin]({{< relref "renin" >}}) -> vasoconstriction of the [efferent arteriole]({{< relref "efferent_arteriole_of_the_kidney" >}}) -> **increase in glomerular filtration rate**


### [Distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}}) {#distal-convoluted-tubule--distal-convoluted-tubule-dot-md}

-   Segment of the [nephron]({{< relref "nephron" >}}) located between the ascending loop of Henle and the connecting tubule and collecting duct


### [Hering-Breuer inflation reflex]({{< relref "hering_breuer_inflation_reflex" >}}) {#hering-breuer-inflation-reflex--hering-breuer-inflation-reflex-dot-md}

-   Inhibits inspiration to prevent overinflation of the lungs and alveolar damage
-   Mediated by pulmonary stretch receptors and vagal afferents


### [Total lung capacity]({{< relref "total_lung_capacity" >}}) {#total-lung-capacity--total-lung-capacity-dot-md}

-   [RV]({{< relref "residual_volume" >}}) + [VC]({{< relref "vital_capacity" >}})


### [Residual volume]({{< relref "residual_volume" >}}) {#residual-volume--residual-volume-dot-md}

-   Volume of air that remains in the lungs **after a maximal exhalation**


### [Vital capacity]({{< relref "vital_capacity" >}}) {#vital-capacity--vital-capacity-dot-md}

-   The maximum volume of air that can be expired **after maximal inspiration**


#### [Inspiratory vital capacity]({{< relref "inspiratory_vital_capacity" >}}) (IVC) {#inspiratory-vital-capacity--inspiratory-vital-capacity-dot-md----ivc}

-   Maximum volume of air that can be **inspired after maximal expiration**


#### [Expiratory vital capacity]({{< relref "expiratory_vital_capacity" >}}) (EVC) {#expiratory-vital-capacity--expiratory-vital-capacity-dot-md----evc}

-   Maximum volume of air that can be **expired after maximal inspiration**


#### [Forced vital capacity]({{< relref "forced_vital_capacity" >}}) (FVC) {#forced-vital-capacity--forced-vital-capacity-dot-md----fvc}

-   Maximum volume of air that can be **forcefully expired** after maximal inspiration


### [Club cell]({{< relref "club_cell" >}}) {#club-cell--club-cell-dot-md}

-   Nonciliated, secretory, cuboidal cells located in the terminal and respiratory bronchioles of the [lungs]({{< relref "lung" >}})
-   **Function:** maintain the integrity of respiratory epithelium by **secreting specialized immunomodulary proteins, glycoproteins, and lipids**
-   [Cytochrome P450]({{< relref "cytochrome_p450" >}}) dependent degradation of toxins
-   Act as a reserve for ciliated cells to restore bronchiolar epithelium


### [Fraction of inspired oxygen]({{< relref "fraction_of_inspired_oxygen" >}}) {#fraction-of-inspired-oxygen--fraction-of-inspired-oxygen-dot-md}

-   Fraction of [Oxygen]({{< relref "oxygen" >}}) (by volume) in the [inspired]({{< relref "inspiration" >}}) air
-   Room air = 21%


## Cardiology {#cardiology}


### [Shunts]({{< relref "shunt" >}}) {#shunts--shunt-dot-md}

-   Pathological connections between the right and left heart chambers
-   Results in blood bypassing part of circulation leading to oxygenated or deoxygenated blood in the wrong part of the cardiac cycle
-   V/Q ratio = 0


#### [Right-to-left shunt]({{< relref "right_to_left_shunt" >}}) {#right-to-left-shunt--right-to-left-shunt-dot-md}

-   Blood **fails to be oxygenated** -> **deoxygenated blood enters the systemic circuit**


### [V/Q ratio]({{< relref "v_q_ratio" >}}) {#v-q-ratio--v-q-ratio-dot-md}

-   The volumetric ratio of air that reaches the alveoli ([ventilation]({{< relref "ventilation" >}})) to alveolar blood supply ([perfusion]({{< relref "perfusion" >}})) per minute
-   The ideal is **1**
-   Average V/Q ratio is **0.8**
-   In the upright position, the lung bases are better ventilated and perfused than the apices


### [Aortic sinus]({{< relref "aortic_sinus" >}}) {#aortic-sinus--aortic-sinus-dot-md}

-   A space immediately above the aortic valve, between an aortic wall leaflet and the wall of the ascending aorta
-   There are **three** aortic sinuses:
    1.  Left aortic sinus
    2.  Right aortic sinus
    3.  Posterior aortic sinuses


### [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}}) {#tetralogy-of-fallot--tetralogy-of-fallot-dot-md}


#### Overview {#overview}

-   The simultaneous occurrence of four defects:

<!--list-separator-->

-  [Right ventricular outflow tract obstruction]({{< relref "right_ventricular_outflow_tract_obstruction" >}}) (RVOTO) (due to pulmonary infundibular stenosis)

    -   Inability of blood to pass out of the right ventricle through the pulmonic valve.
    -   Etiologies include pulmonic valve stenosis, defects in the infundibulum, and defects in the pulmonary artery.

<!--list-separator-->

-  [Right ventricular hypertrophy]({{< relref "right_ventricular_hypertrophy" >}}) (RVH)

    -   A thickening of the muscular wall of the right ventricle of the heart
        -   Usually due to chronic pumping against increased resistance
            -   E.g. pulmonary stenosis, pulmonary hypertension, chronic lung disease

<!--list-separator-->

-  [Ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) (VSD)

    -   An abnormal communication between the left and right ventricle -> [left to right shunting]({{< relref "left_to_right_shunt" >}}) of blood flow
    -   The most common congenital heart defect
    -   Manifests as a loud, harsh holosystolic murmur best appreciated at the left sternal border.

<!--list-separator-->

-  [Overriding aorta]({{< relref "overriding_aorta" >}}) (above the VSD)

    -   Aorta is displaced over the ventricular septal defect (instead of the left ventricle).
    -   Causes blood from both ventricles to flow into the aorta.


### [Haldane effect]({{< relref "haldane_effect" >}}) {#haldane-effect--haldane-effect-dot-md}

-   The carbon dioxide affinity of [Hemoglobin]({{< relref "hemoglobin" >}}) is **inversely proportional** to the oxygenation of hemoglobin


### [Infarction]({{< relref "infarction" >}}) {#infarction--infarction-dot-md}

-   Tissue necrosis that results from insufficient [Blood]({{< relref "blood" >}}) nad [Oxygen]({{< relref "oxygen" >}}) supply to affected region
-   Etiologies include:
    -   Thromboembolic occlusion
    -   Rupture
    -   Vasoconstriction
    -   Vessel compression


### [Atherosclerosis]({{< relref "atherosclerosis" >}}) {#atherosclerosis--atherosclerosis-dot-md}

-   Formation of [lipid]({{< relref "lipid" >}}), [cholesterol]({{< relref "cholesterol" >}}), and/or [calcium]({{< relref "calcium" >}})-laden plaques within [tunica intima]({{< relref "tunica_intima" >}}) of arterial wall -> restricts [blood]({{< relref "blood" >}}) flow


## Embryology {#embryology}


### [Gastroschisis]({{< relref "gastroschisis" >}}) {#gastroschisis--gastroschisis-dot-md}

-   A ventral wall defect -> paraumbilical herniation of the intestine through the abdominal wall **without formation of a hernia sac**


### [Ectopia cordis]({{< relref "ectopia_cordis" >}}) {#ectopia-cordis--ectopia-cordis-dot-md}

-   Rare congenital condition where the [Heart]({{< relref "heart" >}}) is completely or partially located outside the thoracic cavity
-   Caused by impaired migration of lateral mesoderm to the midline -> incomplete fusion of anterior chest wall


## Immunology {#immunology}


### [DiGeorge syndrome]({{< relref "digeorge_syndrome" >}}) {#digeorge-syndrome--digeorge-syndrome-dot-md}

-   22q11.2 deletion
-   Autosomal dominant


#### Clinical features: {#clinical-features}

<!--list-separator-->

-  Cardiac anomalies

    -   [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}})
    -   [Atrial septal defect]({{< relref "atrial_septal_defect" >}})

<!--list-separator-->

-  Anomalous face

<!--list-separator-->

-  Thymus aplasia/hypoplasia

    -   Recurent infections due to [T-cell]({{< relref "t_lymphocyte" >}}) deficiency

<!--list-separator-->

-  Cleft palate

<!--list-separator-->

-  Hypoparathyroidism

    -   Hypocalcemia with tetany


## Backlinks {#backlinks}


### 12 linked references {#12-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoCII**

    <!--list-separator-->

    -  From [Amboss]({{< relref "amboss" >}})

        -   Cofactor for [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})
        -   A component of [chylomicrons]({{< relref "chylomicron" >}}), [VLDL]({{< relref "very_low_density_lipoprotein" >}}), and [HDL]({{< relref "high_density_lipoprotein" >}})

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoB48**

    <!--list-separator-->

    -  From [Amboss]({{< relref "amboss" >}})

        -   Mediates the secretion of [Chylomicron]({{< relref "chylomicron" >}}) particles that originate from the intestine into the lymphatics
        -   A component of [chylomicrons]({{< relref "chylomicron" >}})


#### [Interpret confidence intervals around a sample size or calculated relative risk or odds ratio]({{< relref "interpret_confidence_intervals_around_a_sample_size_or_calculated_relative_risk_or_odds_ratio" >}}) {#interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio--interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    <!--list-separator-->

    -  [Confidence interval]({{< relref "confidence_interval" >}})

        -   **The range of values that are highly likely to contain the true sample measurement**
        -   Provide a way to determine a [Population]({{< relref "population" >}}) measurement or a value that is subject to change from a [Sample]({{< relref "sample" >}}) measurement
        -   [Z scores]({{< relref "z_score" >}}) for confidence intervals for normally distributed data:
            -   Z score for 95% confidence interval = 1.96
            -   Z score for a 97.5% confidence interval = 2.24
            -   Z score for a 99% confidence interval = 2.58
        -   **Formula**: any sample measurement +/- Z score knowing:
            -   Confidence level (usually fixed at 95%)
            -   Sample measurement
            -   [Standard error of the mean]({{< relref "standard_error_of_the_mean" >}}), which needs:
                -   Sample size - larger sample size -> lower standard error -> more narrow confidence intervals
                -   [Standard deviation]({{< relref "standard_deviation" >}})


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    <!--list-separator-->

    -  Major physiological functions of the [kidney]({{< relref "kidney" >}})

        <!--list-separator-->

        -  Production of [Urine]({{< relref "urine" >}})

            -   **Excretion** of metabolic waste and end-products of [metabolism]({{< relref "metabolism" >}})
            -   **Regulation** of extracellular fluid volume and [osmolality]({{< relref "osmolality" >}})
            -   **Maintenance** of acid-base balance
            -   **Maintenance** of [electrolyte]({{< relref "electrolyte" >}}) concentrations
            -   **Regulation** of blood pressure and blood volume
            -   Participation in **[gluconeogenesis]({{< relref "gluconeogenesis" >}})** ([glutamine]({{< relref "glutamine" >}}) and [glutamate]({{< relref "glutamate" >}})) and **[ketogenesis]({{< relref "ketogenesis" >}})**

        <!--list-separator-->

        -  [Hormone]({{< relref "hormone" >}}) synthesis

            -   [Erythropoietin]({{< relref "erythropoietin" >}})
            -   [Calciferol]({{< relref "vitamin_d" >}})
            -   [Prostaglandins]({{< relref "prostaglandin" >}})
            -   [Dopamine]({{< relref "dopamine" >}})
            -   [Renin]({{< relref "renin" >}}) **<- focus mostly on this one in class**


#### [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}}) {#explain-the-t-test-and-the-factors-that-influence-t-values--explain-the-t-test-and-the-factors-that-influence-t-values-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    <!--list-separator-->

    -  [T-test]({{< relref "t_test" >}})

        -   Calculates the difference between the [means]({{< relref "sample_mean" >}}) of two samples _or_ between a sample and a [population]({{< relref "population" >}})/value subject to change
            -   Especially when samples are small and/or population or _value subject to change distribution_ is not known

        <!--list-separator-->

        -  One sample t-test

            -   T-value can be classified according to a table that lists t-values and corresponding quantities **based on number of [degrees of freedom]({{< relref "degrees_of_freedom" >}})** and **[significance level]({{< relref "significance_level" >}})**
            -   Alternatively, calculate [confidence intervals]({{< relref "confidence_interval" >}}) of the sample observations -> check if [population mean]({{< relref "population_mean" >}}) falls within the range given by confidence intervals
            -   _Formula:_ \\(t = \frac{\bar{x} - \mu}{\frac{s}{\sqrt{n}}}\\) where:
                -   \\(t\\) = T-value
                -   \\(\bar{x}\\) = sample mean
                -   \\(\mu\\) = population mean
                -   \\(s\\) = sample standard deviation
                -   \\(n\\) = number of observations

        <!--list-separator-->

        -  Two sample t-test

            -   Calculates whether the means of two groups differ from one another
            -   **Prerequisites:**
                1.  Both sample groups are drawn from the same population and have the same (but unknown) [variance]({{< relref "variance" >}})
                2.  The difference between the observations in the two groups approximately follows a [normal distribution]({{< relref "normal_distribution" >}})
            -   _Formula:_ \\(t = \frac{\bar{x\_{1}} - \bar{x\_{2}}}{\sqrt{s^2(\frac{1}{n\_{1}} + \frac{1}{n\_{2}})}}\\) where:
                -   \\(\bar{x\_{1}} - \bar{x\_{2}\\) = mean difference between two samples
                -   \\(s\\) = sample standard deviation
                -   \\(n\\) = number of observations

            <!--list-separator-->

            -  [Unpaired t-test]({{< relref "unpaired_t_test" >}})

                -   Two **different groups** are sampled at the **same time**
                -   The difference between the means of a continuous outcome variable of these 2 groups is compared
                -   H<sub>0</sub> = \\(\bar{x\_{1}} = \bar{x\_{2}}\\)
                -   Statistically significant difference **rejects** the null hypothesis

            <!--list-separator-->

            -  [Paired t-test]({{< relref "paired_t_test" >}})

                -   The **same group** is sampled at **two different times**
                -   The difference between the means of a continuous outcome variable of these 2 groups is compared
                -   H<sub>0</sub> = \\(\bar{x\_{t\_{1}}} = \bar{x\_{t\_{2}}}\\) -> means at both times are **equal**
                -   Statistically significant difference **rejects** the null hypothesis


#### [Explain the meaning of a p-value less than 0.05]({{< relref "explain_the_meaning_of_a_p_value_less_than_0_05" >}}) {#explain-the-meaning-of-a-p-value-less-than-0-dot-05--explain-the-meaning-of-a-p-value-less-than-0-05-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    <!--list-separator-->

    -  [P-value]({{< relref "p_value" >}})

        -   Probability that a statistical test leads to false positive
        -   If p-value is <= a predetermined significance level (usually **0.05**) -> association considered **statistically significant**
        -   **It is not possible to prove [H<sub>1</sub>]({{< relref "alternative_hypothesis" >}}) true**, but having a p-value lower than [significance level]({{< relref "significance_level" >}}) -> very unlikely that [H<sub>0</sub>]({{< relref "null_hypothesis" >}}) is correct


#### [Define the difference between V/Q for a shunt vs. V/Q for a dead space.]({{< relref "define_the_difference_between_v_q_for_a_shunt_vs_v_q_for_a_dead_space" >}}) {#define-the-difference-between-v-q-for-a-shunt-vs-dot-v-q-for-a-dead-space-dot--define-the-difference-between-v-q-for-a-shunt-vs-v-q-for-a-dead-space-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    <!--list-separator-->

    -  [V/Q mismatch]({{< relref "v_q_mismatch" >}})

        -   An imbalance between total lung ventilation (airflow; _V_) and total lung perfusion (blood flow; _Q_)
        -   Characterized by an increased A-a gradient
        -   **The most common cause of [hypoxemia]({{< relref "hypoxemia" >}})**

        <!--list-separator-->

        -  Increased V/Q ratio

            -   **Indicative of [dead space]({{< relref "physiological_dead_space" >}})**: Volume of inspired air that does not participate in gas exchange during [ventilation]({{< relref "ventilation" >}})
                -   [Anatomic dead space]({{< relref "anatomic_dead_space" >}}): the volume of air in the [conducting zone]({{< relref "conducting_zone" >}}) (e.g. mouth, trachea)
                -   [Alveolar dead space]({{< relref "alveolar_dead_space" >}}): the sum of hte volumes of alveoli that do not participate in gas exchange (mainly apex of lungs)
                    -   These alveoli are ventilated **but not perfused**
            -   Causes:
                -   Blood flow obstruction (e.g. pulmonary embolism)
                -   Exercise
                    -   Cardiac output increases -> vasodilation of apical capillaries


#### [Define statistical power and explain how it affects the interpretation of negative results]({{< relref "define_statistical_power_and_explain_how_it_affects_the_interpretation_of_negative_results" >}}) {#define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results--define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    <!--list-separator-->

    -  [Statistical power]({{< relref "statistical_power" >}})

        -   The probability of correctly rejecting the [null hypothesis]({{< relref "null_hypothesis" >}})
        -   Complementary to the [type 2 error]({{< relref "type_ii_error" >}}) rate
        -   Positively correlates with sample size and magnitude of association of interest
            -   i.e. **increasing sample size = increasing statistical power**
            -   Correlates with measurement [accuracy]({{< relref "accuracy" >}})
        -   Most studies aim to achieve **80% statistical power**


#### [Compare and contrast type I error and type II error]({{< relref "compare_and_contrast_type_i_error_and_type_ii_error" >}}) {#compare-and-contrast-type-i-error-and-type-ii-error--compare-and-contrast-type-i-error-and-type-ii-error-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    | True state of the world |                     |                    |
    |-------------------------|---------------------|--------------------|
    | Decision                | H<sub>0</sub> false | H<sub>0</sub> true |
    | Reject H<sub>0</sub>    | Correct decision    | Type I error       |
    | Retain H<sub>0</sub>    | Type II error       | Correct decision   |

    <!--list-separator-->

    -  [Type I error]({{< relref "type_i_error" >}})

        -   **False positive** -> observed effect is actually due to chance
        -   Null hypothesis is rejected when it is actually true

    <!--list-separator-->

    -  [Type II error]({{< relref "type_ii_error" >}})

        -   **False negative** -> observed effect did not occur due to chance
        -   Null hypothesis is accpeted when it is actually false


#### [Compare and contrast the null hypothesis and the alternative hypothesis]({{< relref "compare_and_contrast_the_null_hypothesis_and_the_alternative_hypothesis" >}}) {#compare-and-contrast-the-null-hypothesis-and-the-alternative-hypothesis--compare-and-contrast-the-null-hypothesis-and-the-alternative-hypothesis-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    <!--list-separator-->

    -  [Null hypothesis]({{< relref "null_hypothesis" >}}) (H<sub>0</sub>)

        -   The assumption that there **is no relationship** between the two measured variables or **no significant difference** between two studied populations
        -   Statistical tests are used to either **reject** or **accept** this hypothesis

    <!--list-separator-->

    -  [Alternative hypothesis]({{< relref "alternative_hypothesis" >}}) (H<sub>1</sub>)

        -   The assumption that there **is a relationship** between two measured variables or **a significant difference** between two studied populations
        -   The counterpart to the null hypothesis


#### [Compare and contrast sample and population]({{< relref "compare_and_contrast_sample_and_population" >}}) {#compare-and-contrast-sample-and-population--compare-and-contrast-sample-and-population-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    <!--list-separator-->

    -  [Population]({{< relref "population" >}})

        -   The total number of inhabitants in a region from which a sample is drawn for statistical measurement

    <!--list-separator-->

    -  [Sample]({{< relref "sample" >}})

        -   A group of people that is representative of a larger population


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  From [Amboss]({{< relref "amboss" >}})

    <!--list-separator-->

    -  [Obstructive lung disease]({{< relref "obstructive_lung_disease" >}})

        -   Increased resistance to airflow caused by narrowing of airways

        <!--list-separator-->

        -  Etiology

            -   [COPD]({{< relref "copd" >}})
            -   Bronchial asthma
            -   Bronchiectasis, [cystic fibrosis]({{< relref "cystic_fibrosis" >}})
            -   Narrowing of extrathoracic airways: laryngeal tumors, vocal cord palsy

    <!--list-separator-->

    -  [Restrictive lung disease]({{< relref "restrictive_lung_disease" >}})

        -   Impaired ability of lungs to expand (i.e. reduced lung compliance)

        <!--list-separator-->

        -  Etiology

            -   _Intrinsic causes_:
                -   [Interstitial lung disease]({{< relref "interstitial_lung_disease" >}})
            -   _Extrinsic causes_:
                -   Diseases of [pleura]({{< relref "pleura" >}}) and [pleural cavity]({{< relref "pleural_cavity" >}})
                    -   Chronic pleural effusion
                    -   Pleural adhesions
                    -   [Pneumothorax]({{< relref "pneumothorax" >}})

    <!--list-separator-->

    -  Findings of [obstructive]({{< relref "obstructive_lung_disease" >}}) vs. [restrictive lung disease]({{< relref "restrictive_lung_disease" >}})

        | Measurement                                   | Obstructive lung disease                                             | Restrictive lung disease                             |
        |-----------------------------------------------|----------------------------------------------------------------------|------------------------------------------------------|
        | [FEV1]({{< relref "fev1" >}})                 | Low                                                                  | Low or normal                                        |
        | FEV1/FVC                                      | Low                                                                  | Normal or high                                       |
        | [VC]({{< relref "vital_capacity" >}})         | Low                                                                  | Low                                                  |
        | [A-a gradient]({{< relref "a_a_gradient" >}}) | High                                                                 | Normal (extrinsic causes) or high (intrinsic causes) |
        | Lung compliance                               | Normal (may be increased in [emphysema]({{< relref "emphysema" >}})) | Normal (extrinsic causes) or low (intrinsic causes)  |


### Unlinked references {#unlinked-references}

[Show unlinked references]
