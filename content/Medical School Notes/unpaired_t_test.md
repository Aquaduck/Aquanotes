+++
title = "Unpaired t-test"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of two-sample [t-test]({{< relref "t_test" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}}) {#explain-the-t-test-and-the-factors-that-influence-t-values--explain-the-t-test-and-the-factors-that-influence-t-values-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > T-test > Two sample t-test**

    <!--list-separator-->

    -  [Unpaired t-test]({{< relref "unpaired_t_test" >}})

        -   Two **different groups** are sampled at the **same time**
        -   The difference between the means of a continuous outcome variable of these 2 groups is compared
        -   H<sub>0</sub> = \\(\bar{x\_{1}} = \bar{x\_{2}}\\)
        -   Statistically significant difference **rejects** the null hypothesis


### Unlinked references {#unlinked-references}

[Show unlinked references]
