+++
title = "End-diastolic volume"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [EDV]({{< relref "end_diastolic_volume" >}}) and [venous return]({{< relref "venous_return" >}}) are used to estimate [preload]({{< relref "preload" >}}) clinically

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
