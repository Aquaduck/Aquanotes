+++
title = "Compare the cAMP and Inositol Phosphatide signal pathways."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Common features of cAMP and IP3 {#common-features-of-camp-and-ip3}

1.  Both use cell surface receptors
2.  Both involve a G-protein
    -   cAMP: G<sub>s</sub> or G<sub>i</sub>
    -   IP3: G<sub>q</sub>
3.  Both involve enzyme activity
    -   cAMP: Adenylate Cyclase
    -   IP3: Phospholipase C-β
4.  Both involve kinase activity -> phosphorylation of targets (on ser/thr residues)
    -   cAMP: PKA
    -   IP3: PKC
5.  Both affect gene transcription


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Compare the cAMP and Inositol Phosphatide signal pathways.]({{< relref "compare_the_camp_and_inositol_phosphatide_signal_pathways" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
