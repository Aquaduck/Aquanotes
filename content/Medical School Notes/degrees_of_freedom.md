+++
title = "Degrees of freedom"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}}) {#explain-the-t-test-and-the-factors-that-influence-t-values--explain-the-t-test-and-the-factors-that-influence-t-values-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > T-test > One sample t-test**

    T-value can be classified according to a table that lists t-values and corresponding quantities **based on number of [degrees of freedom]({{< relref "degrees_of_freedom" >}})** and **[significance level]({{< relref "significance_level" >}})**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
