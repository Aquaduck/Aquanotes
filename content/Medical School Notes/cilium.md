+++
title = "Cilium"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Nexin]({{< relref "nexin" >}}) {#nexin--nexin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Connects adjacent doublet [microtubules]({{< relref "microtubule" >}}) to help maintain the shape of [cilium]({{< relref "cilium" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
