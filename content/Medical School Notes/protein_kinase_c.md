+++
title = "Protein kinase C"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}) {#describe-the-inositol-phosphatide-second-messenger-system--describe-the-inositol-phosphatide-second-messenger-system-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Sequence of events**

    DAG recruits [protein kinase-C]({{< relref "protein_kinase_c" >}}) to the plasma membrane -> partial PKC activation

    ---

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Important members**

    <!--list-separator-->

    -  [Protein kinase C]({{< relref "protein_kinase_c" >}})

        -   Activated by DAG
            -   Reinforced by increased intracellular levels of calcium
        -   Phosphorylates ser/thr residues on target proteins
            -   Results in gene regulation of genes involved with hypertrophy and proliferation (growth and differentiation)


#### [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}}) {#describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot--describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Regulation of the mobilization of intramuscular fat > Role of skeletal muscle contraction**

    Activation of [PKC]({{< relref "protein_kinase_c" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
