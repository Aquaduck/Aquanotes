+++
title = "Fick's law of diffusion"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Osmosis**

    <!--list-separator-->

    -  [Fick's law of diffusion]({{< relref "fick_s_law_of_diffusion" >}})

        -   Describes diffusion of gases
        -   Formula: \\(V\_{x} = \frac{DA\Delta P}{\Delta x}\\) where:
            -   _V<sub>x</sub>_: volume of gas transferred per unit time
            -   _D_: gas diffusion coefficient
            -   _A_: surface area
            -   _ΔP_: partial pressure difference of gas
            -   _Δx_: membrane thickness
        -   The **driving force of gas diffusion is ΔP across the membrane**
            -   **NOT** the concentration difference
        -   _D_ dramatically affects diffusion rate
            -   e.g. diffusion coefficient for CO<sub>2</sub> is ~20x greater than that of O<sub>2</sub>


### Unlinked references {#unlinked-references}

[Show unlinked references]
