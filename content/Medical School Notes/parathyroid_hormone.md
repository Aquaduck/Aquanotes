+++
title = "Parathyroid hormone"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Acts on [osteocytes]({{< relref "osteocyte" >}})
-   Secreted by the [parathyroid glands]({{< relref "parathyroid" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Absorption (p. 1144) > Mineral absorption > Calcium**

    [PTH]({{< relref "parathyroid_hormone" >}}) upregulates the activation of [vitamin D]({{< relref "vitamin_d" >}}) in the [kidney]({{< relref "kidney" >}}) -> facilitates intestinal calcium ion absorption

    ---

<!--list-separator-->

-  **🔖 Absorption (p. 1144) > Mineral absorption > Calcium**

    When blood levels of ionic calcium drop -> [PTH]({{< relref "parathyroid_hormone" >}}) secreted -> stimulates **release of calcium ions from bone matrices** + **increases reabsorption of calcium by [kidneys]({{< relref "kidney" >}})**

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Cytologic components of mature bone**

    Respond to the hormones [parathyroid hormone]({{< relref "parathyroid_hormone" >}}) and [calcitonin]({{< relref "calcitonin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
