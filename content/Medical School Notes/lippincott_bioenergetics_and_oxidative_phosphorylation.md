+++
title = "Lippincott Bioenergetics and Oxidative Phosphorylation"
author = ["Arif Ahsan"]
date = 2021-07-30T00:00:00-04:00
tags = ["medschool", "source", "lippincott"]
draft = false
+++

## [Electron Transport Chain]({{< relref "oxidative_phosphorylation" >}}) {#electron-transport-chain--oxidative-phosphorylation-dot-md}


### [Mitochondrion]({{< relref "mitochondrion" >}}) {#mitochondrion--mitochondrion-dot-md}

-   [ETC]({{< relref "oxidative_phosphorylation" >}}) is present in the inner mitochondrial membrane


#### Membranes of the mitochondrion {#membranes-of-the-mitochondrion}

-   _Outer membrane_ contains special pores -> freely permeable to most ions and small molecules
-   _Inner membrane_ **impermeable** to most **small ions** (H<sup>+</sup>, Na<sup>​+</sup>, K<sup>​+</sup>) and **small molecules** (ATP, ADP, pyruvate)


#### Matrix of the mitochondrion {#matrix-of-the-mitochondrion}

-   Includes enzymes responsible for:
    -   Oxidation of
        -   Pyruvate
        -   Amino acids
        -   Fatty acids (via \\(\Beta\\)-oxidation)
    -   the TCA cycle
-   Synthesis of glucose, urea, and heme occur partially here


### Organization of the electron transport chain {#organization-of-the-electron-transport-chain}

-   _Inner mitochondrial membrane_ broken up into **five complexes** (I-V)


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
