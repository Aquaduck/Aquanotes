+++
title = "Transfer RNA"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [RNA]({{< relref "rna" >}}) {#rna--rna-dot-md}

<!--list-separator-->

-  **🔖 Types of RNA**

    **[Transfer RNA]({{< relref "transfer_rna" >}})** (tRNA)

    ---


#### [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}}) {#osmosis-transcription-translation-and-replication--osmosis-transcription-translation-and-replication-dot-md}

<!--list-separator-->

-  **🔖 Translation**

    <!--list-separator-->

    -  [Transfer RNA]({{< relref "transfer_rna" >}}) (tRNA)

        -   Finds and carries [amino acids]({{< relref "amino_acid" >}}) to ribosome
            -   Each carries **one** AA
        -   Three-letter coding sequence complementary to mRNA
        -   Binds to [ribosome]({{< relref "ribosome" >}}) on the aminoacyl/peptidyl/exit site
            -   _Aminoacyl_: binds tRNA with complementary mRNA codon
            -   _Peptidyl_: holds tRNA with polypeptide
            -   _Exit_: holds tRNA after amino acid is released

        {{< figure src="/ox-hugo/_20210721_154107screenshot.png" caption="Figure 1: One strand of DNA is called the coding strand and the other is called the template strand. They have complementary nucleotide sequences. RNA polymerase builds an mRNA molecule by reading the template strand and adding complementary nucleotides. Therefore, the mRNA will have the same sequence and direcitonality as the coding strand, only with U instead of T." width="500" >}}


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Ribosomes > Function**

    _Small ribosomal unit_ binds [mRNA]({{< relref "messenger_rna" >}}) and activated [tRNAs]({{< relref "transfer_rna" >}}) -> codons of mRNA base-pair with corresponding anticodons of tRNAs

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
