+++
title = "Hemoglobin"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [Sickle cell disease]({{< relref "sickle_cell_disease" >}}) {#sickle-cell-disease--sickle-cell-disease-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Sickle cell disease is a group of disorders that affects [hemoglobin]({{< relref "hemoglobin" >}})

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  [Oxygen]({{< relref "oxygen" >}}) Dissociation from [Hemoglobin]({{< relref "hemoglobin" >}}) (p. 1074)

    -   _Oxygen-hemoglobin dissociation curve_: graph that describes the relationship of partial pressure to the binding of oxygen to [heme]({{< relref "heme" >}}) vs. its subsequent dissociation from heme
        -   Two important rules to this relationship:
            1.  **Gases travel from an area of high partial pressure to an area of lower partial pressure**
            2.  **Oxygen affinity to [heme]({{< relref "heme" >}}) increases as more oxygen molecules are bound**
        -   Thus -> **as partial pressure of oxygen increases, more oxygen is bound by heme**

    {{< figure src="/ox-hugo/_20211017_162608screenshot.png" caption="Figure 1: Partial pressure of oxygen and hemoglobin saturation" >}}

    -   Certain hormones can affect the oxygen-hemoglobin saturation/dissociation curve by **stimulating the production of [2,3-BPG]({{< relref "2_3_biphosphoglycerate" >}})** e.g.:
        -   Androgens
        -   Epinephrine
        -   Thyroid hormones
        -   Growth hormone
    -   BPG is a byproduct of glycolysis


#### [Methemoglobin]({{< relref "methemoglobin" >}}) {#methemoglobin--methemoglobin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Hemoglobin]({{< relref "hemoglobin" >}}) but with ferric (3+) iron instead of ferrous (2+) iron

    ---


#### [Haldane effect]({{< relref "haldane_effect" >}}) {#haldane-effect--haldane-effect-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The [carbon dioxide]({{< relref "carbon_dioxide" >}}) affinity of [Hemoglobin]({{< relref "hemoglobin" >}}) is **inversely proportional** to the [oxygenation]({{< relref "oxygen" >}}) of hemoglobin

    ---


#### [FoCS (Sickle Cell Case)]({{< relref "focs_sickle_cell_case" >}}) {#focs--sickle-cell-case----focs-sickle-cell-case-dot-md}

<!--list-separator-->

-  **🔖 Clinical features of SCD > Vaso-occlusive complications/events**

    Historically most children with [Hb]({{< relref "hemoglobin" >}}) S/S or S/[beta-thalassemia]({{< relref "beta_thalassemia" >}}) will have a dysfunctional spleen w/in first year of life and complete auto-infarction and atrophy due to ischemia of the spleen by five years

    ---

<!--list-separator-->

-  **🔖 Genetics**

    [Hemoglobin]({{< relref "hemoglobin" >}}) consists of four protein subunits

    ---

<!--list-separator-->

-  **🔖 Introduction**

    [Sickle cell disease]({{< relref "sickle_cell_disease" >}}) is a group of disorders that affects [hemoglobin]({{< relref "hemoglobin" >}})

    ---


#### [Factors Affecting Hemoglobin Dissociation Curve]({{< relref "factors_affecting_hemoglobin_dissociation_curve" >}}) {#factors-affecting-hemoglobin-dissociation-curve--factors-affecting-hemoglobin-dissociation-curve-dot-md}

<!--list-separator-->

-  **🔖 Notes > Factors affecting O<sub>2</sub>-Hb Dissociation Curve: > Temperature**

    Increase in temperature -> **decrease affinity of [hemoglobin]({{< relref "hemoglobin" >}}) to oxygen** -> more likely for **hemoglobin to release oxygen** to the **exercising cells** of the tissue

    ---

<!--list-separator-->

-  **🔖 Notes**

    <!--list-separator-->

    -  Factors affecting O<sub>2</sub>-[Hb]({{< relref "hemoglobin" >}}) Dissociation Curve:

        {{< figure src="/ox-hugo/_20210816_211337screenshot.png" caption="Figure 2: Percent saturation of Hemoglobin vs. Partial pressure of Oxygen" width="600" >}}

        <!--list-separator-->

        -  pH

            -   Exercising tissue contains cells that produce more CO<sub>2</sub>
                -   When CO<sub>2</sub> leaves the tissue and enters the [red blood cells]({{< relref "red_blood_cell" >}}) of the capillaries, it is readily converted into _bicarbonate_ and _hydrogen ions_ -> **decreases pH**
            -   H<sup>+</sup> can bind onto allosteric regions of hemoglobin -> **decrease in O<sub>2</sub> affinity**
                -   Allows more O<sub>2</sub> to be delivered to the tissues <- _[Bohr Effect]({{< relref "bohr_effect" >}})_
            -   A decrease in pH shifts the O<sub>2</sub>-Hb curve **to the right**

        <!--list-separator-->

        -  Temperature

            -   [Cells]({{< relref "cell" >}}) that have a high rate of metabolism produce more thermal energy as a byproduct
                -   This thermal energy transfers to the blood plasma -> **raises temperature of blood**
            -   Increase in temperature -> **decrease affinity of [hemoglobin]({{< relref "hemoglobin" >}}) to oxygen** -> more likely for **hemoglobin to release oxygen** to the **exercising cells** of the tissue
            -   Increase of temperature shifts O<sub>2</sub>-Hb curve **to the right**

        <!--list-separator-->

        -  [Carbon Dioxide]({{< relref "carbon_dioxide" >}})

            -   Metabolically active cells of our tissue produce more CO<sub>2</sub> as a waste byproduct
                -   Some of the CO<sub>2</sub> binds directly to hemoglobin -> **decreased affinity to oxygen**
            -   **A higher ppCO<sub>2</sub> (i.e. [CO<sub>2</sub>]) in the blood -> more O<sub>2</sub> delivered to tissues**
                -   The _Haldane Effect_

        <!--list-separator-->

        -  [2,3-BPG]({{< relref "2_3_biphosphoglycerate" >}})

            -   Naturally occurring intermediate in glycolysis
            -   High rate of metabolism -> **produce excess 2,3-BPG**
                -   Some of it escapes into blood plasma -> **binds to hemoglobin and lowers its affinity for oxygen**
                    -   More O<sub>2</sub> for tissues
            -   ↑[2,3-BPG] -> shifts O<sub>2</sub>-Hb curve **to the right**

        <!--list-separator-->

        -  [Carbon Monoxide]({{< relref "carbon_monoxide" >}})

            -   Carbon monoxide is a **competitive inhibitor of hemoglobin**
                -   250x more likely to bind to heme group
            -   By binding to hemoglobin, **CO causes it to increase its affinity for O<sub>2</sub> -> less oxygen delivered to cells of tissue**
            -   Causes a **leftward shift** in the curve _and_ **lowers the curve** because it **lowers hemoglobins O<sub>2</sub>-carrying capacity**


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Haldane effect**

    The carbon dioxide affinity of [Hemoglobin]({{< relref "hemoglobin" >}}) is **inversely proportional** to the oxygenation of hemoglobin

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
