+++
title = "Inosine monophosphate"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    [IMP]({{< relref "inosine_monophosphate" >}}) and [GMP]({{< relref "guanosine_monophosphate" >}}) are converted into their nucleoside forms (inosine and guanosine) by the action of [5'-nucleotidase]({{< relref "5_nucleotidase" >}})

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    An amino group is removed from [AMP]({{< relref "adenosine_monophosphate" >}}) to produce [IMP]({{< relref "inosine_monophosphate" >}}) by [AMP deaminase]({{< relref "amp_deaminase" >}})

    ---


#### [Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP.]({{< relref "outline_the_general_features_of_the_pathways_of_de_novo_synthesis_of_the_two_purine_and_three_pyrimidine_ribonucleotides_noting_the_chemical_changes_required_for_conversion_of_utp_to_ctp_and_imp_to_atp_gtp" >}}) {#outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot--outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Synthesis of purine nucleotides (p. 291)**

    <!--list-separator-->

    -  Conversion of [IMP]({{< relref "inosine_monophosphate" >}}) to AMP and GMP

        -   Two-step energy-requiring pathway
        -   Synthesis of **AMP requires GTP** as an energy source
        -   Synthesis of **GMP requires ATP** as an energy source

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Synthesis of purine nucleotides (p. 291)**

    <!--list-separator-->

    -  Synthesis of [Inosine monophosphate]({{< relref "inosine_monophosphate" >}}), the "parent" purine nucleotide

        -   The next **nine steps** in purine nucleotide biosynthesis lead to the synthesis of IMP
        -   Requires ATP as an energy source


### Unlinked references {#unlinked-references}

[Show unlinked references]
