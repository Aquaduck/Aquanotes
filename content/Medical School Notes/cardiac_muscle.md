+++
title = "Cardiac muscle"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A subtype of [striated muscle]({{< relref "striated_muscle" >}}) found in the [heart]({{< relref "heart" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Describe the vessels supplying the conduction system of the heart and the clinical presentation following damage to these.]({{< relref "describe_the_vessels_supplying_the_conduction_system_of_the_heart_and_the_clinical_presentation_following_damage_to_these" >}}) {#describe-the-vessels-supplying-the-conduction-system-of-the-heart-and-the-clinical-presentation-following-damage-to-these-dot--describe-the-vessels-supplying-the-conduction-system-of-the-heart-and-the-clinical-presentation-following-damage-to-these-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Coronary occlusion and conducting system of Heart**

    Damage to conducting system of heart -> disturbances of [cardiac muscle]({{< relref "cardiac_muscle" >}}) contraction

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Overview - Muscle**

    Striated is further subdivided into _[skeletal]({{< relref "skeletal_muscle" >}})_ and _[cardiac]({{< relref "cardiac_muscle" >}})_ muscles

    ---


#### [Cardiac muscle cell]({{< relref "cardiac_muscle_cell" >}}) {#cardiac-muscle-cell--cardiac-muscle-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept link**

    The cellular component of [Cardiac muscle]({{< relref "cardiac_muscle" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
