+++
title = "Explain the anatomical basis for the aspiration of foreign objects into the bronchial tree."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Aspiration of objects or fluids into the lungs]({{< relref "aspiration_of_objects_or_fluids_into_the_lungs" >}}) {#from-aspiration-of-objects-or-fluids-into-the-lungs--aspiration-of-objects-or-fluids-into-the-lungs-dot-md}

-   Aspirated objects are **more likely to lodge into the right main bronchus** because it is **shorter, wider, and more vertical** than the left
-   Aspirated liquids in a **supine patient** move by gravity to **the more posterior aspects of the lung**
    -   More likely to enter the **superior segments of the right and left lower lobes** -> the first posterior branches of the bronchial tree in the lower lobes


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Explain the anatomical basis for the aspiration of foreign objects into the bronchial tree.]({{< relref "explain_the_anatomical_basis_for_the_aspiration_of_foreign_objects_into_the_bronchial_tree" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
