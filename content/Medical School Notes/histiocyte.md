+++
title = "Histiocyte"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Sheathed capillary]({{< relref "sheathed_capillary" >}}) {#sheathed-capillary--sheathed-capillary-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    They are instead surrounded by aggregates of [histiocytes]({{< relref "histiocyte" >}})

    ---


#### [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}}) {#list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot--list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymph nodes > Parts of the lymph node:**

    [Histiocytes]({{< relref "histiocyte" >}}) (macrophages) mostly reside in the sinuses

    ---


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen > Spleen filtration**

    _Open vascular system_: smallest vessels are blind-ended capillaries ([sheathed capillaries]({{< relref "sheathed_capillary" >}})) without an endothelium -> the ends are instead surrounded by aggregates of [histiocytes]({{< relref "histiocyte" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
