+++
title = "Neuromuscular Junction Session Powerpoint"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the process of neuromuscular transmission.]({{< relref "describe_the_process_of_neuromuscular_transmission" >}}) {#describe-the-process-of-neuromuscular-transmission-dot--describe-the-process-of-neuromuscular-transmission-dot-md}

<!--list-separator-->

-  From [Neuromuscular Junction Session Powerpoint]({{< relref "neuromuscular_junction_session_powerpoint" >}})

    <!--list-separator-->

    -  Order of neuromuscular signaling:

        1.  Action potential invades the nerve terminal
        2.  Influx of calcium through voltage-gated calcium channels
        3.  ACh released into the synaptic cleft
        4.  Diffusion of ACh across cleft
        5.  Reaction of ACh with specific receptor sites
        6.  Activation of a nonselective cation channel that initiates an outward current (end-place current, EPC)
        7.  EPC produces depolarization of the end-plate region (end-plate potential, EPP)
        8.  Suprathreshold EPP initiates a muscle action potential
        9.  Muscle action potential propagates along muscle fiber surface and down the T-tubules
        10. Initiate process of muscle contraction in the muscle fibers


### Unlinked references {#unlinked-references}

[Show unlinked references]
