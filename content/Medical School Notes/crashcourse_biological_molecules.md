+++
title = "CrashCourse - Biological Molecules"
author = ["Arif Ahsan"]
date = 2021-07-12T00:00:00-04:00
tags = ["medschool", "crashcourse", "videos", "source"]
draft = false
+++

Link: <https://www.youtube.com/watch?v=H8WJ2KENlK0>


## Biological Molecules {#biological-molecules}

-   Molecules necessary for every living thing to survive


### Carbohydrates {#carbohydrates}

-   Source of all energy
-   Made up of sugars
    -   Simplest are _monosaccharides_
    -   Also _disaccharides_ - two monosaccharides
        -   e.g. Sucrose
    -   _Polysaccharides_ - a lot of them
        -   Used more for structural things
        -   e.g. Cellulose, starch
    -   Glucose - most essential
        -   Created by plants through using sunlight
-   Humans mainly store carb energy in Glycogen
    -   Otherwise stored in fat


### Lipids {#lipids}

-   Smaller and simpler than carbohydrates
-   **Hydrophobic** because the molecules are **nonpolar**
-   Fats made up of two main things:
    -   Glycerol
    -   Fatty Acid
-   Glycerol + 3 Fatty Acids = _Triglyceride_
-   _Saturated fat_: Fatty Acid all connected with single bonds and bonded to two hydrogens
    -   Usually solid at room temperature
-   _Unsaturated fat_: Double bonds exist, lacking the two hydrogen bond
    -   Usually liquid at room temperature
-   _Trans fats_: Double bonds but in a different conformation, causing them to still be straight molecules


#### Phospholipids {#phospholipids}

-   Two fatty acids + Phosphate attached to glycerol
-   Make up cell membrane walls
-   Phosphate end polar, fatty acid end nonpolar
    -   This leads to the lipid bilayer - phosphate on outside and fatty acids on inside


#### Steroids {#steroids}

-   Backbone of four carbon rings
-   _Cholesterol_
    -   Binds with phospholipids to help form cell walls
-   Also lipid hormones


### Proteins {#proteins}

-   e.g. Enzymes, hormones, antibodies
-   Made up of 20 [Amino Acids]({{< relref "amino_acid" >}})
    -   Use nitrogen, but we can't make it naturally -> have to acquire it from food


#### 9 [Amino Acids]({{< relref "amino_acid" >}}) we can't make ourselves: {#9-amino-acids--amino-acid-dot-md--we-can-t-make-ourselves}

1.  Lysine
2.  Methionine
3.  Histidine
4.  Isoleucine
5.  Leucine
6.  Phenylalanine
7.  Threonine
8.  Tryptophan
9.  Valine


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 General Biology > 1. Name and be able to identify: > Four classes of biomolecules**

    [Biological Molecules]({{< relref "crashcourse_biological_molecules" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
