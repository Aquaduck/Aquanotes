+++
title = "Renal corpuscle"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Filter fluid from the [Blood]({{< relref "blood" >}})
-   Located in the [renal cortex]({{< relref "renal_cortex" >}})
-   Part of the [Nephron]({{< relref "nephron" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

The [Nephron]({{< relref "nephron" >}}) is the functional unit of the kidney composed of the [Renal corpuscle]({{< relref "renal_corpuscle" >}}) and a [Renal tubule]({{< relref "renal_tubule" >}})

---


#### [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}}) {#describe-glomerular-anatomy--ie-efferent-afferent-arteriole----describe-glomerular-anatomy-ie-efferent-afferent-arteriole-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology**

    <!--list-separator-->

    -  The [Renal corpuscle]({{< relref "renal_corpuscle" >}}) (p. 17)

        -   Hollow sphere ([Bowman capsule]({{< relref "bowman_capsule" >}})) composed of epithelial cells
        -   Filled with a compact tuft of interconnected capillary loops called the [Glomerulus]({{< relref "glomerulus" >}})
        -   Two closely spaced arterioles penetrate the [Bowman capsule]({{< relref "bowman_capsule" >}}) at a region called the _vascular pole_:
            1.  [Afferent arteriole of the kidney]({{< relref "afferent_arteriole_of_the_kidney" >}}): brings blood into the capillaries of the glomerulus
            2.  [Efferent arteriole of the kidney]({{< relref "efferent_arteriole_of_the_kidney" >}}): drains blood from the capillaries of the glomerulus
        -   [Mesangial cell]({{< relref "mesangial_cell" >}}): act as phagocytes -> remove trapped material from basement membrane of the capillaries in the [Glomerulus]({{< relref "glomerulus" >}})
            -   Mesangial cells also contain large numbers of myofilaments -> contract in response to a variety of stimuli similar to vascular smooth muscle cells
        -   [Bowman's space]({{< relref "bowman_s_space" >}}): Space within [Bowman capsule]({{< relref "bowman_capsule" >}}) that is not occupied by capillaries and mesangial cells
            -   It is here where fluid filters from the glomerular capillaries before flowing into the first portion of the tubule opposite the vascular pole


#### [Bowman capsule]({{< relref "bowman_capsule" >}}) {#bowman-capsule--bowman-capsule-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Encloses the [Glomerulus]({{< relref "glomerulus" >}}) within the [Renal corpuscle]({{< relref "renal_corpuscle" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
