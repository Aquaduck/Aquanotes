+++
title = "Inferior vena cava"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Right atrium]({{< relref "right_atrium" >}}) {#right-atrium--right-atrium-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Chamber of the [heart]({{< relref "heart" >}}) fed by the [IVC]({{< relref "inferior_vena_cava" >}}) and [SVC]({{< relref "superior_vena_cava" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
