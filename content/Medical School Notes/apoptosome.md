+++
title = "Apoptosome"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Made of a wheel-like heptamer of [Apaf1]({{< relref "apoptotic_protease_activating_factor_1" >}}) proteins


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Cytochrome c and Apaf1 > The intrinsic pathway of apoptosis depends on mitochondria (p. 1025)**

    When released into the cytosol, cytochrome c binds to [Apaf1]({{< relref "apoptotic_protease_activating_factor_1" >}}) -> Apaf1 oligomerizes into a wheel-like heptamer called an [apoptosome]({{< relref "apoptosome" >}}) -> recruits [caspase-9]({{< relref "caspase_9" >}}) -> activation of downstream executioner caspases -> induction of apoptosis

    ---


#### [Cytochrome c]({{< relref "cytochrome_c" >}}) {#cytochrome-c--cytochrome-c-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    When released into the cytosol, cytochrome c binds to [Apaf1]({{< relref "apoptotic_protease_activating_factor_1" >}}) -> formation of [Apoptosome]({{< relref "apoptosome" >}}) -> recruits [Caspase-9]({{< relref "caspase_9" >}}) -> activation of downstream [Executioner caspase]({{< relref "executioner_caspase" >}}) -> induction of apoptosis

    ---


#### [Caspase-9]({{< relref "caspase_9" >}}) {#caspase-9--caspase-9-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Activated by the [Apoptosome]({{< relref "apoptosome" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
