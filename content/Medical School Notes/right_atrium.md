+++
title = "Right atrium"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Chamber of the [heart]({{< relref "heart" >}}) fed by the [IVC]({{< relref "inferior_vena_cava" >}}) and [SVC]({{< relref "superior_vena_cava" >}})
-   First site of venous return in heart


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.]({{< relref "describe_the_common_clinical_procedures_involving_the_heart_include_a_description_of_the_procedure_s_and_the_structure_s_that_are_accessed_or_repaired" >}}) {#describe-the-common-clinical-procedures-involving-the-heart-dot-include-a-description-of-the-procedure--s--and-the-structure--s--that-are-accessed-or-repaired-dot--describe-the-common-clinical-procedures-involving-the-heart-include-a-description-of-the-procedure-s-and-the-structure-s-that-are-accessed-or-repaired-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Clinical procedures related to heart structures: > Cardiac catheterization:**

    Radiopaque catheter inserted into peripheral vein (e.g. femoral vein) and passed under fluoroscopic control into the [right atrium]({{< relref "right_atrium" >}}), [right ventricle]({{< relref "right_ventricle" >}}), [pulmonary trunk]({{< relref "pulmonary_trunk" >}}) and [pulmonary arteries]({{< relref "pulmonary_artery" >}}) respectively

    ---


#### [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}}) {#describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot--describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    [Right ventricle]({{< relref "right_ventricle" >}}) + [right atria]({{< relref "right_atrium" >}}) do not develop as much pressure -> less oxygen demand -> more rarely infarcted

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
