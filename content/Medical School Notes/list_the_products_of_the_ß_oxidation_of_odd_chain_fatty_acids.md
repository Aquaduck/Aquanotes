+++
title = "List the products of the ß-oxidation of odd-chain fatty acids."
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}


### Products of [beta-oxidation]({{< relref "β_oxidation" >}}) of odd-chain fatty acids {#products-of-beta-oxidation--β-oxidation-dot-md--of-odd-chain-fatty-acids}

-   Initial products of odd-chain fatty acid beta-oxidation are [acetyl-CoA]({{< relref "acetyl_coa" >}}) and [propionyl-CoA]({{< relref "propionyl_coa" >}})
-   Acetyl-CoA goes to TCA
-   [Propionyl-CoA]({{< relref "propionyl_coa" >}}) -([Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}}))> [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) -> [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) -([Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}}))> [succinyl-CoA]({{< relref "succinyl_coa" >}}) -> TCA cycle


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-25 Mon] </span></span> > Muscle Intermediary Metabolism II > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the products of the ß-oxidation of odd-chain fatty acids.]({{< relref "list_the_products_of_the_ß_oxidation_of_odd_chain_fatty_acids" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
