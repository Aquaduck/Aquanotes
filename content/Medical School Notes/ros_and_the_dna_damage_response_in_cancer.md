+++
title = "ROS and the DNA damage response in cancer"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

## [Reactive Oxygen Species]({{< relref "reactive_oxygen_species" >}}) (ROS) {#reactive-oxygen-species--reactive-oxygen-species-dot-md----ros}

-   A family of short-lived molecules that exist as _free radicals_


## Role of ROS in the induction of [DNA damage]({{< relref "dna_repair" >}}) {#role-of-ros-in-the-induction-of-dna-damage--dna-repair-dot-md}


### Mediating genotoxin induced damage {#mediating-genotoxin-induced-damage}

-   Ionizing radiation causes double-stranded breaks (DSB) through high energy, **but also through the generation of free radicals**
    -   Mostly \*OH from water
-   ROS can also oxidize nucleoside bases -> G-T or G-A transversions if unrepaired
    -   Typically repaired by the Base Excision Repair pathway
        -   When attempted simultaneously on opposing strands -> DSB


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 6. Summarize the major types of DNA damage caused by replication errors, ionizing radiation, and reactive oxygen species**

    [ROS and the DNA damage response in cancer]({{< relref "ros_and_the_dna_damage_response_in_cancer" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
