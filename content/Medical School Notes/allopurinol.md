+++
title = "Allopurinol"
author = ["Arif Ahsan"]
date = 2021-10-07T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [HGPRT]({{< relref "hypoxanthine_guanine_phosphoribosyltransferase" >}}) dependent


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
