+++
title = "Describe the procedure of thoracocentesis, highlighting the rationale behind the use of the technique."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### [Thoracocentesis]({{< relref "thoracocentesis" >}}) {#thoracocentesis--thoracocentesis-dot-md}

-   Used when you need to insert a hypodermic needle through an intercostal space into the pleural cavity to obtain a sample of fluid or to remove blood or pus
-   To avoid damage ot intercostal nerve and vessels -> needl einserted **superior to rib**, high enough to avoid collateral branches
-   Needle passes through intercostal muscles and costal parietal pleura into pleural cavity
-   When patient in upright position -> intrapleural fluid accumulates in [costodiaphragmatic recess]({{< relref "costodiaphragmatic_recess" >}})
-   Inserting needle into 9th intercostal space in midaxillary line during expiration -> **aovids inferior border of lung**
-   **Needle should be angled upward** to avoid penetrating deep side of recess (thin layer of diaphragmatic parietal pleura and diaphragm overlying the liver)


### Insertion of a Chest Tube {#insertion-of-a-chest-tube}

-   Major amounts of air, blood, serous fluid, and/or pus in pleural cavity typically remove dby insertion of chest tube
-   Short incision made in 5th or 6th intercostal space in midaxillary line
-   Tube may be directed **superiorly** toward cervical pleura for **air removal**
-   Tube may be directed **inferiorly** towards [Costodiaphragmatic recess]({{< relref "costodiaphragmatic_recess" >}}) for **fluid drainage**
-   Extracorporeal (outside body) end of tube connected to an underwater drainage system w/ controlled suction -> **prevent air form being sucked back into pleural cavity**
-   Removal of air allows reinflation of a collapsed lung
-   Failure to remove fluid -> development of resistant fibrous covering over lung that inhibits expansion unless it is peeled off (lung decoritcation)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the procedure of thoracocentesis, highlighting the rationale behind the use of the technique.]({{< relref "describe_the_procedure_of_thoracocentesis_highlighting_the_rationale_behind_the_use_of_the_technique" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
