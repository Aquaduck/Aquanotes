+++
title = "Thoracocentesis"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the procedure of thoracocentesis, highlighting the rationale behind the use of the technique.]({{< relref "describe_the_procedure_of_thoracocentesis_highlighting_the_rationale_behind_the_use_of_the_technique" >}}) {#describe-the-procedure-of-thoracocentesis-highlighting-the-rationale-behind-the-use-of-the-technique-dot--describe-the-procedure-of-thoracocentesis-highlighting-the-rationale-behind-the-use-of-the-technique-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Thoracocentesis]({{< relref "thoracocentesis" >}})

        -   Used when you need to insert a hypodermic needle through an intercostal space into the pleural cavity to obtain a sample of fluid or to remove blood or pus
        -   To avoid damage ot intercostal nerve and vessels -> needl einserted **superior to rib**, high enough to avoid collateral branches
        -   Needle passes through intercostal muscles and costal parietal pleura into pleural cavity
        -   When patient in upright position -> intrapleural fluid accumulates in [costodiaphragmatic recess]({{< relref "costodiaphragmatic_recess" >}})
        -   Inserting needle into 9th intercostal space in midaxillary line during expiration -> **aovids inferior border of lung**
        -   **Needle should be angled upward** to avoid penetrating deep side of recess (thin layer of diaphragmatic parietal pleura and diaphragm overlying the liver)


### Unlinked references {#unlinked-references}

[Show unlinked references]
