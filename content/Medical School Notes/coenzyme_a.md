+++
title = "Coenzyme A"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links > Cofactors/Coenzymes**

    [CoA]({{< relref "coenzyme_a" >}})

    ---


#### [Pantothenic acid]({{< relref "pantothenic_acid" >}}) {#pantothenic-acid--pantothenic-acid-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Essential component of [coenzyme A]({{< relref "coenzyme_a" >}}) (used in synthesis) and [fatty acid synthase]({{< relref "fatty_acid_synthase" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Pyruvate DH complex**

    [CoA]({{< relref "coenzyme_a" >}}) from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
