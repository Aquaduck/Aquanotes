+++
title = "Angiotensin I"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Active form of [Angiotensinogen]({{< relref "angiotensinogen" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Renin-angiotensin-aldosterone mechanism]({{< relref "renin_angiotensin_aldosterone_mechanism" >}}) {#renin-angiotensin-aldosterone-mechanism--renin-angiotensin-aldosterone-mechanism-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Angiotensin I]({{< relref "angiotensin_i" >}})

    ---


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927)**

    <!--list-separator-->

    -  Angiotensin ([I]({{< relref "angiotensin_i" >}}) + [II]({{< relref "angiotensin_ii" >}}))

        -   Angiotensin I circulates in the blood -> reaches lungs -> converted into Angiotensin II
        -   Angiotensin II is a powerful **vasoconstrictor** -> greatly increases blood pressure
        -   Angiotensin II stimulates the release of [ADH]({{< relref "antidiuretic_hormone" >}}) from the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) and [aldosterone]({{< relref "aldosterone" >}}) from the [adrenal cortex]({{< relref "adrenal_cortex" >}})
        -   Angiotensin II stimulates the thirst center in the hypothalamus -> increase of fluid consumption -> increase blood volume and blood pressure

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Renin + Angiotensin**

    Converts the plasma protein [angiotensinogen]({{< relref "angiotensinogen" >}}) (produced by the liver) into its active form - [angiotensin I]({{< relref "angiotensin_i" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
