+++
title = "Endoplasmic reticulum"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   An [organelle]({{< relref "organelle" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Smooth endoplasmic reticulum]({{< relref "smooth_endoplasmic_reticulum" >}}) {#smooth-endoplasmic-reticulum--smooth-endoplasmic-reticulum-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A type of [Endoplasmic reticulum]({{< relref "endoplasmic_reticulum" >}})

    ---


#### [Rough endoplasmic reticulum]({{< relref "rough_endoplasmic_reticulum" >}}) {#rough-endoplasmic-reticulum--rough-endoplasmic-reticulum-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A type of [Endoplasmic reticulum]({{< relref "endoplasmic_reticulum" >}})

    ---


#### [Chylomicron retention disease]({{< relref "chylomicron_retention_disease" >}}) {#chylomicron-retention-disease--chylomicron-retention-disease-dot-md}

<!--list-separator-->

-  **🔖 From Molecular analysis and intestinal expression of SAR1 genes and proteins in Anderson's disease (Chylomicron retention disease)**

    A small GTPase involved [COPII]({{< relref "cop_ii" >}})-dependent transport of proteins from the [ER]({{< relref "endoplasmic_reticulum" >}}) to the [Golgi]({{< relref "golgi_apparatus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
