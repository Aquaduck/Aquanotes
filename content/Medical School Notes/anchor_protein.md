+++
title = "Anchor protein"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Cell Signalling III - Underlying Design Features]({{< relref "cell_signalling_iii_underlying_design_features" >}}) {#cell-signalling-iii-underlying-design-features--cell-signalling-iii-underlying-design-features-dot-md}

<!--list-separator-->

-  **🔖 AM vs FM signaling > Macromolecule complexes > cAMP multi-protein complex**

    One way this is engineered is through the actions of [anchor proteins]({{< relref "anchor_protein" >}}) - in this case, [A-Kinase Anchor Protein]({{< relref "a_kinase_anchor_protein" >}}) (AKAP)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
