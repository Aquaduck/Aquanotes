+++
title = "Small intestine"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296)**

    <!--list-separator-->

    -  Degradation of dietary [nucleic acids]({{< relref "nucleic_acid" >}}) in the [small intestine]({{< relref "small_intestine" >}})

        -   Ribonucleases and deoxyribonucleases are secreted by the [pancreas]({{< relref "pancreas" >}})
            -   Hydrolyze dietary RNA and DNA primarily to [oligonucleotides]({{< relref "oligonucleotide" >}})
            -   [Oligonucleotides]({{< relref "oligonucleotide" >}}) are further hydrolyzed by pancreatic [phosphodiesterases]({{< relref "phosphodiesterase" >}}) -> produces a mixture of 3' and 5'-mononucleotides
        -   Dietary purine bases are **not used for the synthesis of tissue nucleic acids**
            -   They are instead converted to [uric acid]({{< relref "uric_acid" >}}) in intestinal mucosal cells
            -   Most of the [Uric acid]({{< relref "uric_acid" >}}) enters the blood -> excreted in urine


### Unlinked references {#unlinked-references}

[Show unlinked references]
