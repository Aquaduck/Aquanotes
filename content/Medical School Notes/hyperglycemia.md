+++
title = "Hyperglycemia"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Glycolysis > Glucokinase (hexokinase IV)**

    This prevents [hyperglycemia]({{< relref "hyperglycemia" >}})

    ---


#### [Glucokinase]({{< relref "glucokinase" >}}) {#glucokinase--glucokinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Prevents [hyperglycemia]({{< relref "hyperglycemia" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
