+++
title = "Explain how the glomerular filtration barrier works and affects filtrate"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}}) {#from-vander-s-renal-physiology--vander-s-renal-physiology-dot-md}


### Basic renal excretory processes (p. 21) {#basic-renal-excretory-processes--p-dot-21}

{{< figure src="/ox-hugo/_20211031_173409screenshot.png" caption="Figure 1: Fundamental elements of renal function - glomerular filtration, tubular secretion and tubular reabsorption - and the association between the tubule and vasculature in the cortex" >}}

-   _[Filtration]({{< relref "filtration" >}})_: the process by which [Water]({{< relref "water" >}}) and solutes in the [Blood]({{< relref "blood" >}}) leave the vascular system through the [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) and enter [Bowman's space]({{< relref "bowman_s_space" >}})
    -   [Bowman's space]({{< relref "bowman_s_space" >}}) is topologically **outside the body**
-   _[Reabsorption]({{< relref "reabsorption" >}})_: the process of moving substances from the lumen across the epithelial layer into the surrounding interstitium
    -   In most cases, reabsorbed substances then move into surrounding blood vessels -> **2-step process**:
        1.  Removal from tubular lumen
        2.  Movement into blood
-   _[Excretion]({{< relref "excretion" >}})_: exit of the substance from the body


#### Glomerular filtration (p. 23) {#glomerular-filtration--p-dot-23}

-   [Urine]({{< relref "urine" >}}) formation begins with glomerular filtration: the bulk flow of fluid from the glomerular capillaries into [Bowman's capsule]({{< relref "bowman_capsule" >}})
-   _Glomerular filtrate_: fluid within the Bowman's capsule
    -   Very similar to blood plasma, but **contains little protein because large plasma proteins are virtually excluded form moving through the [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}})**
        -   E.g. albumin and globulins are excluded
        -   Smaller proteins (e.g. peptide hormones) are still able to pass through the barrier, but their combined mass is miniscule
    -   Contains most **inorganic ions** and **low-molecular-weight organic solutes**
        -   Substances with same concentration in filtrate as in plasma are considered _freely filtered_
            -   E.g. [Sodium]({{< relref "sodium" >}}), [Potassium]({{< relref "potassium" >}}), [Chloride]({{< relref "chloride" >}}), [Bicarbonate]({{< relref "bicarbonate" >}}), [Glucose]({{< relref "glucose" >}}), [Urea]({{< relref "urea" >}}), [amino acids]({{< relref "amino_acid" >}}), peptides such as [Insulin]({{< relref "insulin" >}}) and [ADH]({{< relref "antidiuretic_hormone" >}})
-   [Glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}): **the volume of filtrate formed per unit of time**
    -   Normally 180 L/day


### Glomerular filtration (p. 33) {#glomerular-filtration--p-dot-33}

-   Filtered fluid must pass through a 3-layered [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}})
    1.  Endothelial cells of the capillaries
        -   Perforated by many large _fenestrae_ ("windows") which are freely permeable to everything in the blood **except cells and platelets**
    2.  Capillary basement membrane
        -   Gel-like acellular meshwork of glycoproteins and proteoglycans
    3.  [Podocytes]({{< relref "podocyte" >}}): epithelial cells that surround capillaries and rest on the capillary basement membrane
        -   Have an unusual octopus-like structure with small "fingers" called _pedicels_
            -   Coated by a thick layer of extracellular material -> partially occludes the slits
            -   These interdigitate with the pedicels from other podocytes
        -   _Slit diaphragm_: bridge the slits between pedicals
            -   Widened versions of tight junctions and adhering junctions of other epithelial cells
            -   **Spaces between slit diaphragms constitute the path through which filtrate travels to enter the [Bowman's space]({{< relref "bowman_s_space" >}})**
            -   **Integrity of slit diaphragms of podocytes essential to prevent excessive leak of [albumin]({{< relref "albumin" >}})**
-   Selectivity of the [Glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) depends on **molecular size** and **electrical charge**

    {{< figure src="/ox-hugo/_20211031_180522screenshot.png" caption="Figure 2: Relationships between molecular weight vs. filtrate and charge vs. filtrate" >}}

    1.  Allows unhindered movement of molecules less than 7000 Da
        -   From 7000 to 70,000, movement is more progressively hindered until there is no movement
    2.  **Negatively charged macromolecules are permitted less** relative to neutral molecules
        -   Surfaces of all three layers of the [Glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) contain fixed polyanions -> **repel negatively charged macromolecules during filtration**
            -   **Almost all [plasma]({{< relref "plasma" >}}) proteins bear net negative charge -> important restrictive role**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > Renal Physiology: Glomerular Filtration > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
