+++
title = "ScienceDirect - Processivity"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

## Processivity {#processivity}

-   The capability to remain associated with the primer-template complex over a number of catalytic cycles
-   Continuous threading of a single carbohydrate chain through the catalytic site of an enzyme
-   **One of the most important properties of [DNA polymerases]({{< relref "dna_polymerase" >}})**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 5. Summarize the mechanism of DNA replication and replication forks, and describe what is meant by the terms semi-conservative, discontinuous, leading strand, lagging strand, processivity, and fidelity**

    [ScienceDirect - Processivity]({{< relref "sciencedirect_processivity" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
