+++
title = "BH3-only protein"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The largest subclass of [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}}) proteins


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026)**

    <!--list-separator-->

    -  [BH3-only proteins]({{< relref "bh3_only_protein" >}})

        -   Cell either produces or activates BH3-only proteins in response to an apoptotic stimulus
        -   Provide the crucial link between apoptotic stimuli and the intrinsic pathway of apoptosis
            -   Different stimuli activates different BH3-only proteins
        -   Thought to promote apoptotis mainly by inhibiting anti-apoptotic Bcl2 family proteins
        -   BH3 domain binds to a long hydrophobic groove on anti-apoptotic Bcl2 family proteins -> neutralizes their activity
            -   Enables the aggregation of [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}}) on the surface of mitochondria -> triggers release of intermembrane mitochondrial proteins -> induction of apoptosis
        -   Some bind to Bax and Bak directly to stimulate their aggregation

        <!--list-separator-->

        -  [Bid]({{< relref "bid" >}})

            -   Normally inactive
            -   When [death receptors]({{< relref "death_receptor" >}}) activate the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}), [Caspase-8]({{< relref "caspase_8" >}}) cleaves Bid -> activates Bid -> translocates to outer mitochondrial membrane -> inhibits anti-apoptotic Bcl2 family proteins -> **amplification of death signal**


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 HIF-1 target genes involved in glucose and energy metabolism**

    HIF-1 activates transcription of the gene encoding the [BH3-only protein]({{< relref "bh3_only_protein" >}}) BNIP3 -> induces selective mitochondrial autophagy by competing with [Beclin 1]({{< relref "beclin_1" >}}) for binding to [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) -> allows Beclin 1 to trigger autophagy

    ---


#### [Bid]({{< relref "bid" >}}) {#bid--bid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [BH3-only protein]({{< relref "bh3_only_protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
