+++
title = "Triacylglycerol"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Triacylglycerol energy reserve is stored in [adipocytes]({{< relref "adipose_cell" >}}) in the fed state


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  [Triacylglycerol]({{< relref "triacylglycerol" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   The **insolubility of triacylglycerols** poses a significant problem for both:
            -   **The absorption and utilization of dietary lipids**
            -   **The mobilization of triacylglycerls stored in adipocytes**

        <!--list-separator-->

        -  Pathway of triacylglycerol breakdown

            -   Absorption of dietary fats is dependent on the **presence of amphipathic bile salts** -> creation of small _micelles_ of hydrophobic fat
            -   Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by _enterocytes_ (cells of the intestinal lining)
                -   Digested triacylglycerol (except for short- and medium-chain fatty acids) are **resynthesized in the ER** along with phospholipids and cholesterol esters
                    -   Short- and medium-chain fatty acids **diffuse directly across the enterocyte into portal capillaries**
            -   Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
                -   Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB-48
                    -   Molecules of apoCII and apoCIII are also integrated
            -   The formed chylomicrons are then **exocytosed into the lymphatics** -> **enter venous circulation**
            -   Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues
                -   [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**
                    -   Done through its effects on gene transcription
                -   [Insulin]({{< relref "insulin" >}}) also **increases glucose uptake in skeletal muscle and adipose tissue**
            -   LPL will cleave the triacylglycerol to **either** _three [fatty acids]({{< relref "fatty_acid" >}}) and glycerol_ or _two fatty acids and a [monoacylglycerol]({{< relref "monoacylglycerol" >}})_
                -   Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})
            -   [Fatty acids]({{< relref "fatty_acid" >}}) will bind to various _fatty acid binding proteins_ -> able to traverse the cell and enter the tissue beneath
                -   In muscle, fatty acids will be oxidized for energy and/or stored
                -   In lactating [mammary glands]({{< relref "mammary_gland" >}}), fatty acids + monoacylglycerol become components of [milk]({{< relref "milk" >}})

        <!--list-separator-->

        -  Mobilization of triacylglycerol stored in [adipocytes]({{< relref "adipose_cell" >}})

            -   **Triacylglycerol energy reserve is stored in adipocytes in the fed state**
                -   **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**
                    -   Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})
                    -   Therefore, **cAMP-mediated activation of PKA is required**
            -   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
                -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
            -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
            -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol
            -   **Ultimately, 3 fatty acids are mobilized/liberated and glycerol is formed**
                -   Fatty acids exit adipose tissue via diffusoin and bind reversibly to plasma [albumin]({{< relref "albumin" >}})
                -   Glycerol taken up by the liver

        <!--list-separator-->

        -  Mobilization of stored triacylglycerol in [muscle]({{< relref "muscle" >}})

            -   Skeletal muscle obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose
                -   These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [Epinephrine]({{< relref "epinephrine" >}})

            {{< figure src="/ox-hugo/_20211024_191932screenshot.png" caption="Figure 1: Fatty acid mobilization mediated via skeletal muscle contraction vs. epinephrine" width="500" >}}

        <!--list-separator-->

        -  [Fatty acid catabolism]({{< relref "lipolysis" >}})

            -   Can be divided into **3 stages**:
                1.  _Investment_ to prepare them for ->
                2.  _Transport_ into the mitochondrial matrix wherein they will undergo ->
                3.  _Oxidation_

            {{< figure src="/ox-hugo/_20211024_192624screenshot.png" width="700" >}}

            <!--list-separator-->

            -  Investment phase

                -   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
                    -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
                    -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**

            <!--list-separator-->

            -  Transport phase

                -   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_
                -   Fatty aycl-carnitine ester is formed by the action of _carnitine acyltransferase I_ once inside the inner membrane space
                    -   Becomes a substrate for _carnitine acyltransferase II_ -> catalyzes transfer of acyl to CoA-SH in matrix -> release carnitine for a second round of fatty acid transport
                -   **Rate-limiting and commits the fatty acyl-CoA to oxidation in the matrix**

                {{< figure src="/ox-hugo/_20211024_193058screenshot.png" width="700" >}}

            <!--list-separator-->

            -  Oxidation phase

                -   Occurs in three stages:
                    1.  Beta-oxidation
                    2.  Oxidation of acetyl-CoA in TCA cycle -> 3 NADH, 1 FADH<sub>2</sub>, 1 GTP
                    3.  ETC -> 9 ATP
                        -   Electrons from 3 NADH and 1 FADH<sub>2</sub>
                -   **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**
                    -   Increased [propionic acid] = _biotin deficiency_
                    -   Increased [methylmalonic acid] = _vitamin B12 deficiency_
                    -   Leads to **mental deficits**


#### [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}}) {#lipoprotein-lipase--lipoprotein-lipase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Catalyzes lipolysis of [triglycerides]({{< relref "triacylglycerol" >}}) circulating in [chylomicrons]({{< relref "chylomicron" >}})

    ---


#### [Lipolysis]({{< relref "lipolysis" >}}) {#lipolysis--lipolysis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The breakdown of [triglycerides]({{< relref "triacylglycerol" >}}) into [glycerol]({{< relref "glycerol" >}}) and [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [Glycerol]({{< relref "glycerol" >}}) {#glycerol--glycerol-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Taken up by the [liver]({{< relref "liver" >}}) following cleavage of [Triacylglycerol]({{< relref "triacylglycerol" >}}) via [LPL]({{< relref "lipoprotein_lipase" >}})

    ---


#### [Chylomicron]({{< relref "chylomicron" >}}) {#chylomicron--chylomicron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Skeletal muscle]({{< relref "skeletal_muscle" >}}) obtains [dietary fatty acids]({{< relref "dietary_lipid" >}}) from chylomicrons that can be "re-synthesized" into [triacylglycerol]({{< relref "triacylglycerol" >}}) due to the muscle's regular uptake of glucose

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
