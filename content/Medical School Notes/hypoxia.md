+++
title = "Hypoxia"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   Inadequate supply of [oxygen]({{< relref "oxygen" >}}) to tissues


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Cyanosis]({{< relref "cyanosis" >}}) {#cyanosis--cyanosis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Blue coloring of tissues indicative of [hypoxia]({{< relref "hypoxia" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
