+++
title = "Name the second messenger produced in response to glucagon-induced cellular signaling and how it functions to elicit the appropriate intracellular response"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

[cAMP]({{< relref "camp" >}}) is the "second messenger" = the first intracellular effector formed in response to the first messenger [glucagon]({{< relref "glucagon" >}})

-   Adenylate cyclase activated by glucagon -> converts ATP to cAMP

_From "Intermediary Metabolism and its Regulation" pre-work_


## Function of [cAMP]({{< relref "camp" >}}) {#function-of-camp--camp-dot-md}

1.  Upon activation by glucagon -> cAMP binds to two regulatory subunits (R) of inactive [PKA]({{< relref "protein_kinase_a" >}}) -> releases two catalytically-active subunits + activation of PKA -> phosphorylation of substrates -> activation of enzymes involved in catabolism


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Name the second messenger produced in response to glucagon-induced cellular signaling and how it functions to elicit the appropriate intracellular response]({{< relref "name_the_second_messenger_produced_in_response_to_glucagon_induced_cellular_signaling_and_how_it_functions_to_elicit_the_appropriate_intracellular_response" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
