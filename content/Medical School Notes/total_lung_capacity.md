+++
title = "Total lung capacity"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Addition of [Vital capacity]({{< relref "vital_capacity" >}}) and [residual volume]({{< relref "residual_volume" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration**

    <!--list-separator-->

    -  [Total lung capacity]({{< relref "total_lung_capacity" >}})

        -   [RV]({{< relref "residual_volume" >}}) + [VC]({{< relref "vital_capacity" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
