+++
title = "Striated muscle"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A subtype of [muscle]({{< relref "muscle" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Skeletal muscle]({{< relref "skeletal_muscle" >}}) {#skeletal-muscle--skeletal-muscle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A subtype of [striated muscle]({{< relref "striated_muscle" >}})

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Overview - Muscle**

    Muscle is classified into two types: _[striated]({{< relref "striated_muscle" >}})_ and _[smooth]({{< relref "smooth_muscle" >}})_

    ---


#### [Cardiac muscle]({{< relref "cardiac_muscle" >}}) {#cardiac-muscle--cardiac-muscle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A subtype of [striated muscle]({{< relref "striated_muscle" >}}) found in the [heart]({{< relref "heart" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
