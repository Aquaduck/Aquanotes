+++
title = "Mean"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [T-test]({{< relref "t_test" >}}) {#t-test--t-test-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Calculates the difference between the [means]({{< relref "mean" >}}) of two [samples]({{< relref "sample" >}}) _or_ between a sample and a [population]({{< relref "population" >}})/value subject to change

    ---


#### [Sample mean]({{< relref "sample_mean" >}}) {#sample-mean--sample-mean-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Mean]({{< relref "mean" >}}) of a [Sample]({{< relref "sample" >}})

    ---


#### [Population mean]({{< relref "population_mean" >}}) {#population-mean--population-mean-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Mean]({{< relref "mean" >}}) of a [Population]({{< relref "population" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
