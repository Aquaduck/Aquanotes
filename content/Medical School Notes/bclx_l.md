+++
title = "BclX_{L}"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A member of the [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}}) of proteins


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026)**

    <!--list-separator-->

    -  [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) and [BclX<sub>L</sub>]({{< relref "bclx_l" >}})

        -   **Anti-apoptotic**
        -   Bind to pro-apoptotic Bcl2 family proteins -> inhibit apoptosis


### Unlinked references {#unlinked-references}

[Show unlinked references]
