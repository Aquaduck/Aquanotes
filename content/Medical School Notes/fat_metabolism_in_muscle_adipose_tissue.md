+++
title = "Fat Metabolism in Muscle & Adipose Tissue"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

Pre-work for Muscle Intermediary Metabolism II


## Backlinks {#backlinks}


### 28 linked references {#28-linked-references}


#### [List the products of the ß-oxidation of odd-chain fatty acids.]({{< relref "list_the_products_of_the_ß_oxidation_of_odd_chain_fatty_acids" >}}) {#list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot--list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot-md}

<!--list-separator-->

-  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

    <!--list-separator-->

    -  Products of [beta-oxidation]({{< relref "β_oxidation" >}}) of odd-chain fatty acids

        -   Initial products of odd-chain fatty acid beta-oxidation are [acetyl-CoA]({{< relref "acetyl_coa" >}}) and [propionyl-CoA]({{< relref "propionyl_coa" >}})
        -   Acetyl-CoA goes to TCA
        -   [Propionyl-CoA]({{< relref "propionyl_coa" >}}) -([Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}}))> [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) -> [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) -([Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}}))> [succinyl-CoA]({{< relref "succinyl_coa" >}}) -> TCA cycle


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Methylmalonic acid**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Increased [methylmalonic acid] is indicative of a [vitamin B12]({{< relref "cobalamin" >}}) deficiency

<!--list-separator-->

-  **🔖 L-methylmalonyl-CoA**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Propionyl-CoA undergoes an ATP- and biotin-dependent carboxylation reaction to form D-methylmalonyl-CoA -> converted to L-methylmalonyl-CoA
        -   L-methylmalonyl-CoA converted to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via [B12]({{< relref "cobalamin" >}}) dependent isomerization catalyzed by [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})
            -   Succinyl-CoA then oxidized in TCA cycle

<!--list-separator-->

-  **🔖 Propionyl-CoA carboxylase**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Catalyzes the formation of D-methylmalonyl-CoA from [Propionyl-CoA]({{< relref "propionyl_coa" >}})

<!--list-separator-->

-  **🔖 Propionyl-CoA**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Produced via the beta-oxidation of odd-chain fatty acids
        -   Undergoes an ATP- and biotin-dependent carboxylation reaction to form [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) -> converted to L-methylmalonyl-CoA

<!--list-separator-->

-  **🔖 FAD- and NAD<sup>+</sup>-linked dehydrogenases**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Recognize very long-, long-, middle-, or small-chain fatty acids as substrates -> carry out oxidation reactions in [beta-oxidation]({{< relref "β_oxidation" >}})

<!--list-separator-->

-  **🔖 Carnitine acyltransferase I (CAT-1) and CAT-II**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        <!--list-separator-->

        -  [CAT-1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}})

            -   [Fatty acyl-carnitine ester]({{< relref "fatty_acyl_carnitine_ester" >}}) is formed by the action of _carnitine acyltransferase I_ once inside the inner membrane space

        <!--list-separator-->

        -  [CAT-2]({{< relref "carnitine_palmitoyl_acyltransferase_2" >}})

            -   [Fatty acyl-carnitine ester]({{< relref "fatty_acyl_carnitine_ester" >}}) becomes a substrate for _carnitine acyltransferase II_ -> catalyzes transfer of acyl to CoA-SH in matrix -> release [carnitine]({{< relref "carnitine" >}}) for a second round of fatty acid transport

<!--list-separator-->

-  **🔖 Carnitine**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_

<!--list-separator-->

-  **🔖 Beta-oxidation**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   First stage of oxidation of [fatty acids]({{< relref "fatty_acid" >}})
        -   Oxidation of the fatty acid at the beta-carbon, followed by a hydration, an oxidation, and a thiolysis
        -   Removes carbons 1 and 2 as a molecule of acetyl-CoA
        -   One round of beta-oxidation -> 1 molecule of NADH and FADH<sub>2</sub>
        -   Odd-chain fatty acids produce acetyl-CoA **and** propionyl-CoA
        -   **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**
            -   Increased [propionic acid] = _biotin deficiency_
            -   Increased [methylmalonic acid] = _vitamin B12 deficiency_
            -   Leads to **mental deficits**

<!--list-separator-->

-  **🔖 Albumin-bound fatty acids**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   **Two major purposes:**
            1.  Half-life of albumin-bound fatty acid is **~3 min -> taken up and oxidized by rapidly metabolizing tissues due to fasting state**
                -   E.g. [muscle]({{< relref "muscle" >}}) and [kidney]({{< relref "kidney" >}})
            2.  Fasting state -> **fatty acids enter the [liver]({{< relref "liver" >}}) as the energy source needed to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})**
                -   **Released [glycerol]({{< relref "glycerol" >}}) is used primarly by the [liver]({{< relref "liver" >}}) as a [gluconeogenic]({{< relref "gluconeogenesis" >}}) substrate**

<!--list-separator-->

-  **🔖 Carnitine**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_

<!--list-separator-->

-  **🔖 Fatty acyl-CoA synthetase**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   In the **investment phase of fatty acid oxidation**
        -   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
            -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
            -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**

<!--list-separator-->

-  **🔖 Fatty acyl-CoA**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   In the **investment phase of fatty acid oxidation**
        -   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
            -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
            -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**

<!--list-separator-->

-  **🔖 Hormone-sensitive lipase**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
        -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol

<!--list-separator-->

-  **🔖 Perilipin**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
            -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
        -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of 2,3-DAG**

<!--list-separator-->

-  **🔖 Lipoprotein lipase**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**
            -   Done through its effects on gene transcription
        -   LPL will cleave the triacylglycerol to **either** _three fatty acids and glycerol_ or _two fatty acids and a monoacylglycerol_
        -   The LPL isozyme of adipose tissue has the **largest K<sub>m</sub> -> [adipose tissue's]({{< relref "adipose_cell" >}}) uptake of [dietary fat]({{< relref "dietary_lipid" >}}) is a last resort**
            -   **Adipose tissue stores fat** -> least likely to utilize it
            -   **LPL is not expressed by the [liver]({{< relref "liver" >}}) or the [brain]({{< relref "brain" >}})** -> NO dietary fat reaches either of those tissues
        -   Tethering the LPL to the cell via a long polysaccharide chain consisting of _heparan sulfates_ -> **significantly improves the likelihood of an [apoCII]({{< relref "apocii" >}}):LPL interaction within the capillary**

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoCII**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   ApoCII plays a **crucial role in delivering dietary fat to extrahepatic tissues**
        -   ApoCII anchors the chylomicron to the LPL molecules expressed by capillary endothelial cells

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoB48**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
            -   Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB48
        -   **The structural protein of [chylomicrons]({{< relref "chylomicron" >}})**

<!--list-separator-->

-  **🔖 Chylomicrons**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   The **carriers of [dietary lipids]({{< relref "dietary_lipid" >}}) in the [blood]({{< relref "blood" >}})**
        -   Molecules of [apo-CII]({{< relref "apolipoprotein_cii" >}}) and [apo-CIII]({{< relref "apolipoprotein_ciii" >}}) are also integrated into the chylomicron "membrane"
        -   Formed chylomicrons are **exocytosed into the lymphatics** -> **enter venous circulation**
        -   Chylomicrons **indicate a fed state**
        -   [Skeletal muscle]({{< relref "skeletal_muscle" >}}) obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose

<!--list-separator-->

-  **🔖 Gastrointestinal lipase**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by _enterocytes_ (cells of the intestinal lining)

<!--list-separator-->

-  **🔖 Triacylglycerol**

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   The **insolubility of triacylglycerols** poses a significant problem for both:
            -   **The absorption and utilization of dietary lipids**
            -   **The mobilization of triacylglycerls stored in adipocytes**

        <!--list-separator-->

        -  Pathway of triacylglycerol breakdown

            -   Absorption of dietary fats is dependent on the **presence of amphipathic bile salts** -> creation of small _micelles_ of hydrophobic fat
            -   Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by _enterocytes_ (cells of the intestinal lining)
                -   Digested triacylglycerol (except for short- and medium-chain fatty acids) are **resynthesized in the ER** along with phospholipids and cholesterol esters
                    -   Short- and medium-chain fatty acids **diffuse directly across the enterocyte into portal capillaries**
            -   Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
                -   Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB-48
                    -   Molecules of apoCII and apoCIII are also integrated
            -   The formed chylomicrons are then **exocytosed into the lymphatics** -> **enter venous circulation**
            -   Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues
                -   [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**
                    -   Done through its effects on gene transcription
                -   [Insulin]({{< relref "insulin" >}}) also **increases glucose uptake in skeletal muscle and adipose tissue**
            -   LPL will cleave the triacylglycerol to **either** _three [fatty acids]({{< relref "fatty_acid" >}}) and glycerol_ or _two fatty acids and a [monoacylglycerol]({{< relref "monoacylglycerol" >}})_
                -   Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})
            -   [Fatty acids]({{< relref "fatty_acid" >}}) will bind to various _fatty acid binding proteins_ -> able to traverse the cell and enter the tissue beneath
                -   In muscle, fatty acids will be oxidized for energy and/or stored
                -   In lactating [mammary glands]({{< relref "mammary_gland" >}}), fatty acids + monoacylglycerol become components of [milk]({{< relref "milk" >}})

        <!--list-separator-->

        -  Mobilization of triacylglycerol stored in [adipocytes]({{< relref "adipose_cell" >}})

            -   **Triacylglycerol energy reserve is stored in adipocytes in the fed state**
                -   **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**
                    -   Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})
                    -   Therefore, **cAMP-mediated activation of PKA is required**
            -   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
                -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
            -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
            -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol
            -   **Ultimately, 3 fatty acids are mobilized/liberated and glycerol is formed**
                -   Fatty acids exit adipose tissue via diffusoin and bind reversibly to plasma [albumin]({{< relref "albumin" >}})
                -   Glycerol taken up by the liver

        <!--list-separator-->

        -  Mobilization of stored triacylglycerol in [muscle]({{< relref "muscle" >}})

            -   Skeletal muscle obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose
                -   These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [Epinephrine]({{< relref "epinephrine" >}})

            {{< figure src="/ox-hugo/_20211024_191932screenshot.png" caption="Figure 1: Fatty acid mobilization mediated via skeletal muscle contraction vs. epinephrine" width="500" >}}

        <!--list-separator-->

        -  [Fatty acid catabolism]({{< relref "lipolysis" >}})

            -   Can be divided into **3 stages**:
                1.  _Investment_ to prepare them for ->
                2.  _Transport_ into the mitochondrial matrix wherein they will undergo ->
                3.  _Oxidation_

            {{< figure src="/ox-hugo/_20211024_192624screenshot.png" width="700" >}}

            <!--list-separator-->

            -  Investment phase

                -   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
                    -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
                    -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**

            <!--list-separator-->

            -  Transport phase

                -   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_
                -   Fatty aycl-carnitine ester is formed by the action of _carnitine acyltransferase I_ once inside the inner membrane space
                    -   Becomes a substrate for _carnitine acyltransferase II_ -> catalyzes transfer of acyl to CoA-SH in matrix -> release carnitine for a second round of fatty acid transport
                -   **Rate-limiting and commits the fatty acyl-CoA to oxidation in the matrix**

                {{< figure src="/ox-hugo/_20211024_193058screenshot.png" width="700" >}}

            <!--list-separator-->

            -  Oxidation phase

                -   Occurs in three stages:
                    1.  Beta-oxidation
                    2.  Oxidation of acetyl-CoA in TCA cycle -> 3 NADH, 1 FADH<sub>2</sub>, 1 GTP
                    3.  ETC -> 9 ATP
                        -   Electrons from 3 NADH and 1 FADH<sub>2</sub>
                -   **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**
                    -   Increased [propionic acid] = _biotin deficiency_
                    -   Increased [methylmalonic acid] = _vitamin B12 deficiency_
                    -   Leads to **mental deficits**


#### [Explain why adipose tissue is referred to as a fat depot.]({{< relref "explain_why_adipose_tissue_is_referred_to_as_a_fat_depot" >}}) {#explain-why-adipose-tissue-is-referred-to-as-a-fat-depot-dot--explain-why-adipose-tissue-is-referred-to-as-a-fat-depot-dot-md}

<!--list-separator-->

-  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

    -   The LPL isozyme of adipose tissue has the **largest K<sub>m</sub> -> [adipose tissue's]({{< relref "adipose_cell" >}}) uptake of [dietary fat]({{< relref "dietary_lipid" >}}) is a last resort**
        -   **Adipose tissue stores fat** -> least likely to utilize it
        -   **LPL is not expressed by the [liver]({{< relref "liver" >}}) or the [brain]({{< relref "brain" >}})** -> NO dietary fat reaches either of those tissues


#### [Explain how fatty acids released from adipocyte stores are utilized.]({{< relref "explain_how_fatty_acids_released_from_adipocyte_stores_are_utilized" >}}) {#explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot--explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot-md}

<!--list-separator-->

-  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

    <!--list-separator-->

    -  How [fatty acids]({{< relref "fatty_acid" >}}) released from [adipocyte]({{< relref "adipose_cell" >}}) stores are utilized

        -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
        -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol
        -   **Ultimately, 3 fatty acids are mobilized/liberated and glycerol is formed**
            -   Fatty acids exit adipose tissue via diffusion and bind reversibly to plasma [albumin]({{< relref "albumin" >}})
            -   Glycerol taken up by the liver


#### [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}}) {#explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot--explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot-md}

<!--list-separator-->

-  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

    <!--list-separator-->

    -  Delivering dietary fat to extrahepatic tissues via [chylomicrons]({{< relref "chylomicron" >}})

        -   The formed chylomicrons are then **exocytosed into the lymphatics** -> **enter venous circulation**
        -   Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues
            -   [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**
                -   Done through its effects on gene transcription
            -   [Insulin]({{< relref "insulin" >}}) also **increases glucose uptake in skeletal muscle and adipose tissue**
        -   LPL will cleave the triacylglycerol to **either** _three [fatty acids]({{< relref "fatty_acid" >}}) and glycerol_ or _two fatty acids and a [monoacylglycerol]({{< relref "monoacylglycerol" >}})_
            -   Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})
        -   [Fatty acids]({{< relref "fatty_acid" >}}) will bind to various _fatty acid binding proteins_ -> able to traverse the cell and enter the tissue beneath
            -   In muscle, fatty acids will be oxidized for energy and/or stored
            -   In lactating [mammary glands]({{< relref "mammary_gland" >}}), fatty acids + monoacylglycerol become components of [milk]({{< relref "milk" >}})


#### [Detail the vitamin requirements for the metabolism of propionyl-CoA.]({{< relref "detail_the_vitamin_requirements_for_the_metabolism_of_propionyl_coa" >}}) {#detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot--detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot-md}

<!--list-separator-->

-  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

    <!--list-separator-->

    -  Vitamin requirements for the metabolism of [propionyl-CoA]({{< relref "propionyl_coa" >}})

        1.  [Biotin]({{< relref "biotin" >}}): carboxylation of propionyl-CoA to form [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) via [Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})
        2.  [B12]({{< relref "cobalamin" >}}): Isomerization of [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})


#### [Describe the role of protein kinase A in mobilizing fat from adipocyte stores.]({{< relref "describe_the_role_of_protein_kinase_a_in_mobilizing_fat_from_adipocyte_stores" >}}) {#describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot--describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot-md}

<!--list-separator-->

-  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

    <!--list-separator-->

    -  The role of [PKA]({{< relref "protein_kinase_a" >}}) in mobilizing fat from [adipocytes]({{< relref "adipose_cell" >}})

        -   **Triacylglycerol energy reserve is stored in adipocytes in the fed state**
            -   **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**
                -   Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})
                -   Therefore, **cAMP-mediated activation of PKA is required**
        -   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
            -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
        -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
        -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol


#### [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}}) {#describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot--describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot-md}

<!--list-separator-->

-  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

    <!--list-separator-->

    -  Regulation of the mobilization of [intramuscular]({{< relref "muscle" >}}) fat

        -   Skeletal muscle obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose
            -   These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [Epinephrine]({{< relref "epinephrine" >}})

        {{< figure src="/ox-hugo/_20211024_203003screenshot.png" caption="Figure 2: Fatty acid mobilization mediated via skeletal muscle contraction vs. epinephrine" width="500" >}}

        <!--list-separator-->

        -  Role of [skeletal muscle]({{< relref "skeletal_muscle" >}}) contraction

            1.  Nerve impulse via [acetylcholine]({{< relref "acetylcholine" >}}) to acetylcholine receptor
            2.  Depolarization
            3.  Stimulation of ER to release [Calcium]({{< relref "calcium" >}})
            4.  Activation of [PKC]({{< relref "protein_kinase_c" >}})
            5.  Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol

        <!--list-separator-->

        -  Role of [epinephrine]({{< relref "epinephrine" >}})

            1.  Epinephrine binds to beta-adrenergic receptor
            2.  Activation of G-protein
            3.  Activation of adenylate cyclase
            4.  Convertion of ATP to cAMP
            5.  Activation of [PKA]({{< relref "protein_kinase_a" >}})
            6.  Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol


#### [Describe the events required for packaging dietary fat into chylomicrons.]({{< relref "describe_the_events_required_for_packaging_dietary_fat_into_chylomicrons" >}}) {#describe-the-events-required-for-packaging-dietary-fat-into-chylomicrons-dot--describe-the-events-required-for-packaging-dietary-fat-into-chylomicrons-dot-md}

<!--list-separator-->

-  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

    <!--list-separator-->

    -  Pathway of triacylglycerol breakdown

        -   Absorption of dietary fats is dependent on the **presence of amphipathic bile salts** -> creation of small _micelles_ of hydrophobic fat
        -   Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by _enterocytes_ (cells of the intestinal lining)
            -   Digested triacylglycerol (except for short- and medium-chain fatty acids) are **resynthesized in the ER** along with phospholipids and cholesterol esters
                -   Short- and medium-chain fatty acids **diffuse directly across the enterocyte into portal capillaries**
        -   Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
            -   Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB-48
                -   Molecules of apoCII and apoCIII are also integrated
        -   The formed chylomicrons are then **exocytosed into the lymphatics** -> **enter venous circulation**


### Unlinked references {#unlinked-references}

[Show unlinked references]
