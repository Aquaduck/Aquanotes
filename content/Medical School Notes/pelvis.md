+++
title = "Pelvis"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Symphysis pubis]({{< relref "symphysis_pubis" >}}) {#symphysis-pubis--symphysis-pubis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Joins the two halves of the [pelvis]({{< relref "pelvis" >}}) in the anterior mid-line

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
