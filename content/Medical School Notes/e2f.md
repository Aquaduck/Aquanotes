+++
title = "E2F"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Mitogens Stimulate G<sub>1</sub>-Cdk and G1/S-Cdk activities (p. 1012) > Mitogens interact with cell-surface receptors to trigger multiple intracellular signaling pathways**

    <!--list-separator-->

    -  [E2F]({{< relref "e2f" >}}) proteins

        -   Activation of E2F is a key function of G<sub>1</sub>-Cdk complexes
        -   E2F proteins bind to specific DNA sequences in promoters of genes that encode proteins required for S-phase entry
            -   G<sub>1</sub>/S-cyclins
            -   S-cyclins
            -   Proteins involved in DNA synthesis + chromosome duplication
        -   In the absence of mitogenic stimulation, E2F-dependent gene expression is inhibited by members of the [Retinoblastoma protein family]({{< relref "retinoblastoma_protein_family" >}})


#### [Retinoblastoma protein family]({{< relref "retinoblastoma_protein_family" >}}) {#retinoblastoma-protein-family--retinoblastoma-protein-family-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inhibits [E2F]({{< relref "e2f" >}}) activity

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
