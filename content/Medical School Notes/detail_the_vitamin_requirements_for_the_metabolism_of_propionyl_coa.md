+++
title = "Detail the vitamin requirements for the metabolism of propionyl-CoA."
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}


### Vitamin requirements for the metabolism of [propionyl-CoA]({{< relref "propionyl_coa" >}}) {#vitamin-requirements-for-the-metabolism-of-propionyl-coa--propionyl-coa-dot-md}

1.  [Biotin]({{< relref "biotin" >}}): carboxylation of propionyl-CoA to form [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) via [Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})
2.  [B12]({{< relref "cobalamin" >}}): Isomerization of [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-25 Mon] </span></span> > Muscle Intermediary Metabolism II > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Detail the vitamin requirements for the metabolism of propionyl-CoA.]({{< relref "detail_the_vitamin_requirements_for_the_metabolism_of_propionyl_coa" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
