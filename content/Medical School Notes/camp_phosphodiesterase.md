+++
title = "cAMP phosphodiesterase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Insulin's metabolic effects**

    Activates [cAMP-phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects > Reversal of glucagon's actions**

    [cAMP]({{< relref "camp" >}}) can be rapidly hydrolyzed to 5'-AMP by [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}}) through cleavage of cyclic 3',5'-phosphodiester bond

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Additional important enzymes**

    <!--list-separator-->

    -  [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})

        -   **Hydrolyzes cAMP to 6'AMP**
            -   5'AMP **does not signal** -> eliminates second messenger in glucagon signaling required to activate PKA
                -   **PKA activity down-regulated**


#### [Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important.]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}}) {#describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot--describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Off Signals > Several pathways in the cell to turn off a signal: > Get rid of second messenger - phosphodiesterases (PDE)**

    You can **degrade the intracellular signal molecule** e.g. cAMP degradation via [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})

    ---


#### [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}}) {#describe-the-intercellular-response-to-insulin-induced-cellular-signaling--describe-the-intercellular-response-to-insulin-induced-cellular-signaling-dot-md}

<!--list-separator-->

-  **🔖 Insulin's metabolic effects**

    Activates [cAMP-phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})

    ---


#### [Define the enzymes involved in synthesis and degradation of cyclic-AMP.]({{< relref "define_the_enzymes_involved_in_synthesis_and_degradation_of_cyclic_amp" >}}) {#define-the-enzymes-involved-in-synthesis-and-degradation-of-cyclic-amp-dot--define-the-enzymes-involved-in-synthesis-and-degradation-of-cyclic-amp-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  [Cyclic AMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})

        -   converts cAMP -> 5'AMP (degradation)


### Unlinked references {#unlinked-references}

[Show unlinked references]
