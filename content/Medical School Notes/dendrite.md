+++
title = "Dendrite"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A component of [neurons]({{< relref "neuron" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [MAP2]({{< relref "map2" >}}) {#map2--map2-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Microtubule-binding proteins modulate filament dynamics and organization**

    Due to its long projecting domain, MAP2 **forms bundles of stable microtubules** that are kept widely spaced and is thus confined to neuron cell bodies and its [dendrites]({{< relref "dendrite" >}})

    ---


#### [Describe the general morphology of neurons and levels of organization of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}}) {#describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot--describe-the-general-morphology-of-neurons-and-levels-of-organization-of-neurons-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127) > Neuron structure**

    <!--list-separator-->

    -  [Dendrites]({{< relref "dendrite" >}})

        -   Receive stimuli from sensory cells, axons, or other neurons -> convert these signals into action potentials transmitted **towards the soma**
        -   Lacks a Golgi complex
        -   Organelles are reduced or absent near terminals **except for mitochondria**, which are abundant
        -   Spines on surface of dendrites increase surface area for synapse formation
            -   Diminish with age and poor neutrition

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > III. Cells of nervous system (p. 127)**

    [Neurons]({{< relref "neuron" >}}) consist of a cell body and its processes, which usually include multiple [dendrites]({{< relref "dendrite" >}}) and a single [axon]({{< relref "axon" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
