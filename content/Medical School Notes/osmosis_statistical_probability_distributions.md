+++
title = "Osmosis - Statistical Probability Distributions"
author = ["Arif Ahsan"]
date = 2021-07-13T00:00:00-04:00
tags = ["medschool", "osmosis", "source"]
draft = false
+++

Link:  <https://www.osmosis.org/notes/Statistical%5FProbability%5FDistributions#page-1>


## [Notes]({{< relref "biostatistics" >}}) {#notes--biostatistics-dot-md}


### Normal Distribution {#normal-distribution}

-   Data grouped around central value, without left/right bias, in "bell curve" shape


### Z-scores {#z-scores}

-   Standardized score representing deviation from the mean
-   Uses data set mean, standard deviation to determine measurement location
-   Expressed in standard-deviations


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
