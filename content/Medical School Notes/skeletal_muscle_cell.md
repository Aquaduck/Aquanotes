+++
title = "Skeletal muscle cell"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept link {#concept-link}

-   Cellular unit of [skeletal muscle]({{< relref "skeletal_muscle" >}})
-   [Epinephrine]({{< relref "epinephrine" >}}) can mobilize fat stores


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Sarcoplasm]({{< relref "sarcoplasm" >}}) {#sarcoplasm--sarcoplasm-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The cytoplasm of [skeletal muscle cells]({{< relref "skeletal_muscle_cell" >}})

    ---


#### [Sarcolemma]({{< relref "sarcolemma" >}}) {#sarcolemma--sarcolemma-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The [plasma membrane]({{< relref "plasma_membrane" >}}) of [skeletal muscle cells]({{< relref "skeletal_muscle_cell" >}})

    ---


#### [Myofibril]({{< relref "myofibril" >}}) {#myofibril--myofibril-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Cylindrical collections found in [skeletal muscle cells]({{< relref "skeletal_muscle_cell" >}})

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Structure of Skeletal Muscle (p. 109)**

    <!--list-separator-->

    -  [Skeletal muscle cells]({{< relref "skeletal_muscle_cell" >}})

        -   Long, cylindrical, multinucleated cells enveloped by an external lamina and reticular fibers
        -   Skeletal muscle cell cytoplasm is called a [sarcoplasm]({{< relref "sarcoplasm" >}})
        -   Skeletal muscle cell membrane is called a [sarcolemma]({{< relref "sarcolemma" >}}) (variation of [plasmalemma]({{< relref "plasma_membrane" >}}) - another name for cell membrane)
            -   Form deep tubular invaginations called [Transverse (T) tubules]({{< relref "transverse_tubule" >}})

        <!--list-separator-->

        -  [Myofibril]({{< relref "myofibril" >}})

            -   Extend the entire length of the cell
            -   Composed of longitudinally arranged, cylindrical bundles of [thick]({{< relref "thick_myofilament" >}}) and [thin myofilaments]({{< relref "thin_myofilament" >}})

        <!--list-separator-->

        -  [Sarcomere]({{< relref "sarcomere" >}})

            <!--list-separator-->

            -  Structure diagram

                {{< figure src="/ox-hugo/_20210913_202331screenshot.png" caption="Figure 1: Components of a sarcomere in myofibril" >}}

                -   Labeled in diagram above:
                    -   Z disk
                    -   A band
                    -   I band
                    -   H zone
                    -   Myosin/thick filament
                    -   Actin/thin filament

        <!--list-separator-->

        -  [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) (SR)

            -   Modified smooth endoplasmic reticulum (SER) that surrounds myofilaments and forms a meshwork around each myofibril
            -   **Regulates muscle contraction** by sequestering calcium ions (leading to relaxation) or releasing calcium ions (leading to contraction)


### Unlinked references {#unlinked-references}

[Show unlinked references]
