+++
title = "Citrate"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Metabolic Alkalosis: Primary Bicarbonate Excess (p. 1281)**

    Can be transiently caused by an ingestion of excessive [bicarbonate]({{< relref "bicarbonate" >}}), [citrate]({{< relref "citrate" >}}), or [antacids]({{< relref "antacid" >}})

    ---


#### [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}}) {#list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated--list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated-dot-md}

<!--list-separator-->

-  **🔖 Examples in glucose catabolism > Allosteric control**

    Inhibited by [citrate]({{< relref "citrate" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle > Citrate synthase**

    [Citrate]({{< relref "citrate" >}}) itself inhibits the enzyme by competing with [oxaloacetate]({{< relref "oxaloacetate" >}})

    ---


#### [6-Phosphofructo-1-kinase]({{< relref "6_phosphofructo_1_kinase" >}}) {#6-phosphofructo-1-kinase--6-phosphofructo-1-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inhibited by [citrate]({{< relref "citrate" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
