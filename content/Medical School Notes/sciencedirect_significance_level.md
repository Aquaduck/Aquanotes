+++
title = "ScienceDirect - Significance Level"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "sciencedirect", "papers", "source"]
draft = false
+++

## [Overview]({{< relref "biostatistics" >}}) {#overview--biostatistics-dot-md}

-   The _significance level_ of an event is **the probability that the event could have occurred by chance**
    -   If the level is low, the probability of an event occurring by chance is low -> **the event is significant**
-   Statistical significance does **not** mean that the event has any clinical meaning


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
