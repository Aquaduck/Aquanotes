+++
title = "Cytochrome P450"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Identify the five P450 enzyme metabolizer status phenotypes that may guide drug selection or dosing]({{< relref "identify_the_five_p450_enzyme_metabolizer_status_phenotypes_that_may_guide_drug_selection_or_dosing" >}}) {#identify-the-five-p450-enzyme-metabolizer-status-phenotypes-that-may-guide-drug-selection-or-dosing--identify-the-five-p450-enzyme-metabolizer-status-phenotypes-that-may-guide-drug-selection-or-dosing-dot-md}

<!--list-separator-->

-  **🔖 From Pharmacogenetics Pre-learning Material**

    <!--list-separator-->

    -  Five types of [P450]({{< relref "cytochrome_p450" >}}) enzyme metabolizers

        1.  _Poor metabolizer_: markedly reduced or absent enzymatic activity -> **cannot be relied on to metabolize drugs or pro-drugs**
        2.  _Intermediate metabolizer_: **reduced but still adequate metabolic capacity** depending on the context
        3.  _Normal (extensive) metabolizer_: baseline enzymatic activity -> **do not require any adjustment in dose or drug choice**
        4.  _Rapid metabolizer_: **mild increase in enzymatic activity**
        5.  _Ultrarapid metabolizer_: **greatly increased enzymatic activity**


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Club cell**

    [Cytochrome P450]({{< relref "cytochrome_p450" >}}) dependent degradation of toxins

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
