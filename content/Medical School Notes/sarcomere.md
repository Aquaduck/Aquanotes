+++
title = "Sarcomere"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## From Amboss {#from-amboss}

-   Smallest functional unit of striated muscle fiber
-   One sarcomere is the area between two Z bands
-   During contraction, sarcomeres shorten while myofilaments stay the same length


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Structure of Skeletal Muscle (p. 109) > Skeletal muscle cells**

    <!--list-separator-->

    -  [Sarcomere]({{< relref "sarcomere" >}})

        <!--list-separator-->

        -  Structure diagram

            {{< figure src="/ox-hugo/_20210913_202331screenshot.png" caption="Figure 1: Components of a sarcomere in myofibril" >}}

            -   Labeled in diagram above:
                -   Z disk
                -   A band
                -   I band
                -   H zone
                -   Myosin/thick filament
                -   Actin/thin filament


### Unlinked references {#unlinked-references}

[Show unlinked references]
