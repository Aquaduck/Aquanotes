+++
title = "Renal calyx"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Part of the [renal pelvis]({{< relref "renal_pelvis" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Cortical collecting duct**

    **Urine is not altered after it enters a [calyx]({{< relref "renal_calyx" >}})**

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Cortical collecting duct**

    Each [renal calyx]({{< relref "renal_calyx" >}}) is continuous with the [ureter]({{< relref "ureter" >}})

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Cortical collecting duct**

    All cortical collecting ducks then run downward -> enter the medulla -> become [Outer medullary collecting duct]({{< relref "outer_medullary_collecting_duct" >}}) -> become [Inner medullary collecting duct]({{< relref "inner_medullary_collecting_duct" >}}) -> _papillary collecting ducts_ empties into a [calyx]({{< relref "renal_calyx" >}}) of the [Renal pelvis]({{< relref "renal_pelvis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
