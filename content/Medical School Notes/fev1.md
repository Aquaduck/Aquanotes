+++
title = "FEV1"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Measures the forced expiratory volume of the lung in one second


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Tiffeneau-Pinelli index]({{< relref "tiffeneau_pinelli_index" >}}) {#tiffeneau-pinelli-index--tiffeneau-pinelli-index-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Ratio of [FEV1]({{< relref "fev1" >}}) to [FVC]({{< relref "forced_vital_capacity" >}}) as a percentage

    ---


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Findings of obstructive vs. restrictive lung disease**

    |                               |     |               |
    |-------------------------------|-----|---------------|
    | [FEV1]({{< relref "fev1" >}}) | Low | Low or normal |

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
