+++
title = "LMNA"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Gene that codes for [A-type lamins]({{< relref "lamin_a" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}}) {#rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot--rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot-md}

<!--list-separator-->

-  **🔖 From Protein farnesylation and disease > Nuclear lamins > A-type lamins**

    Products of a single gene termed [LMNA]({{< relref "lmna" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
