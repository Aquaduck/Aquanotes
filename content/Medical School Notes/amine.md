+++
title = "Amine"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Lumen - Functional Group Names, Properties, and Reactions]({{< relref "lumen_functional_group_names_properties_and_reactions" >}}) {#lumen-functional-group-names-properties-and-reactions--lumen-functional-group-names-properties-and-reactions-dot-md}

<!--list-separator-->

-  **🔖 Functional Groups**

    <!--list-separator-->

    -  [Amines]({{< relref "amine" >}})

        <!--list-separator-->

        -  Structure

            -   Basic nitrogen atom with lone pair of electrons bound to up to 3 substituents, the rest being hydrogens
                -   **1<sup>o</sup> amines**: One substituent
                -   **2<sup>o</sup> amines**: Two substituents
                -   **3<sup>o</sup> amines**: Three substituents
                -   Possible to have four substituents on the nitrogen, making it an ammonium cation with charged nitrogen center

        <!--list-separator-->

        -  Physical properties

            -   **Can form hydrogen bonds**
            -   Somewhat soluble in water
                -   Solubility decreases with increase in carbon atoms
            -   _Aliphatic amine_: amine connected to alkyl chain
                -   Soluble in organic polar solvents
            -   Aromatic amines donate lone electron pair to benzene ring -> decreased hydrgeon bonding
                -   Decreased solubility in water and higher boiling point

        <!--list-separator-->

        -  Acidity and Alkalinity

            -   \\(NHRR'\\) and \\(NR'R''R'''\\) amines are chiral molecules
                -   Low barrier for inversion -> cannot be resolved optically
            -   **Amines are bases**
                -   Basicity depends on electronic properties of substituents
                    -   Alkyl groups _increase_ basicity
                    -   Aryl groups _decrease_ basicity
                    -   Aromatic rings _decrease_ basicity
                        -   Because of delocalized lone pair
            -   Solubility order of ammoniums: **1<sup>o</sup>** RNH<sub>3</sub><sup>+</sup> > **2<sup>o</sup>** R<sub>2</sub>NH<sub>2</sub><sup>​+</sup> > **3<sup>o</sup>** R<sub>3</sub>NH<sup>​+</sup> > Quaternary ammonium

        <!--list-separator-->

        -  Reactivity

            -   **Very reactive** due to basicity and nucleophilicity
            -   Most 1<sup>o</sup> amines are good ligands -> form coordination complexes w/ metal ions
            -   Imine formation:
                <_20210714_222815screenshot.png>

        <!--list-separator-->

        -  Applications of Amines

            -   Many important biological molecules are amine-based e.g. neurotransmitters and amino acids


### Unlinked references {#unlinked-references}

[Show unlinked references]
