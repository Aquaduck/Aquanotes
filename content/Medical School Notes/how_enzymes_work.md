+++
title = "How Enzymes Work"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

Link: <https://vicportal.med.uvm.edu/bbcswebdav/pid-34318-dt-content-rid-134698%5F1/courses/CC%5F2021-1/July%2012%20Activation%20energy%20objective.pdf>


## Objective: {#objective}

-   Understand the effect of [enzymes]({{< relref "enzyme" >}}) on the activation energy of reactions, and why this makes these chemical reactions proceed more quickly


## Energy changes occuring during the reaction {#energy-changes-occuring-during-the-reaction}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
