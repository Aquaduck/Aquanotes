+++
title = "Efferent arteriole of the kidney"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The blood vessels that carries previously filtered blood away from the [Glomerulus]({{< relref "glomerulus" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Ascending thick limb of the loop of Henle**

    Passes directly between the [afferent]({{< relref "afferent_arteriole_of_the_kidney" >}}) and [efferent]({{< relref "efferent_arteriole_of_the_kidney" >}}) arterioles at the vascular pole

    ---


#### [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}}) {#describe-glomerular-anatomy--ie-efferent-afferent-arteriole----describe-glomerular-anatomy-ie-efferent-afferent-arteriole-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal corpuscle (p. 17)**

    [Efferent arteriole of the kidney]({{< relref "efferent_arteriole_of_the_kidney" >}}): drains blood from the capillaries of the glomerulus

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Macula densa > Feedback mechanisms**

    Triggers release of [Renin]({{< relref "renin" >}}) -> vasoconstriction of the [efferent arteriole]({{< relref "efferent_arteriole_of_the_kidney" >}}) -> **increase in glomerular filtration rate**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
