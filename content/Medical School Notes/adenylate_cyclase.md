+++
title = "Adenylate cyclase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Additional important enzymes > Protein kinase A (PKA)**

    Glucagon or epinephrine binds to respective membrane receptors -> [adenylate cyclase]({{< relref "adenylate_cyclase" >}}) catalyzed conversion of ATP to cAMP

    ---


#### [Describe the cyclic-AMP (cAMP) second messenger signal pathway from hormone binding to activation of protein kinase-A.]({{< relref "describe_the_cyclic_amp_camp_second_messenger_signal_pathway_from_hormone_binding_to_activation_of_protein_kinase_a" >}}) {#describe-the-cyclic-amp--camp--second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot--describe-the-cyclic-amp-camp-second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > cAMP Activation**

    G<sub>s</sub> complex activated -> activates [adenylyl cyclase]({{< relref "adenylate_cyclase" >}}) -> formation of cAMP

    ---

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > G-protein signal cascades: cAMP**

    [Adenylyl cyclase]({{< relref "adenylate_cyclase" >}})

    ---


#### [Define the enzymes involved in synthesis and degradation of cyclic-AMP.]({{< relref "define_the_enzymes_involved_in_synthesis_and_degradation_of_cyclic_amp" >}}) {#define-the-enzymes-involved-in-synthesis-and-degradation-of-cyclic-amp-dot--define-the-enzymes-involved-in-synthesis-and-degradation-of-cyclic-amp-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  Adenylyl cyclase/[Adenylate cyclase]({{< relref "adenylate_cyclase" >}})

        -   converts ATP -> cAMP (synthesis)


### Unlinked references {#unlinked-references}

[Show unlinked references]
