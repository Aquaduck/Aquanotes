+++
title = "List and describe the major morphologic regions of a lymph node."
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}}) {#from-lymphatic-and-immune-systems-ppt--lymphatic-and-immune-systems-ppt-dot-md}


### [Lymphatic]({{< relref "lymph" >}}) anatomy {#lymphatic--lymph-dot-md--anatomy}

-   Only a few named lymphatic vessels
-   All lymph from **below the diaphragm** and **on the left side** passes through the _thoracic duct_ to enter the vascular system at the **bifurcation of the left internal juglar and subclavian veins**
-   Lymph from **upper right side** enters through the **right lymphatic duct** at the same junction on the right
-   Lymph from **the GI tract** joins that from the lower extremities in the [Chyle cistern]({{< relref "chyle_cistern" >}}) -> enters thoracic duct

{{< figure src="/ox-hugo/_20211009_180548screenshot.png" width="600" >}}


### [Lymph nodes]({{< relref "lymph_node" >}}) {#lymph-nodes--lymph-node-dot-md}

-   Small kidney-bean shaped structures normally a few mm in size, but capable of rapidly growing to centimeters during an infection
-   Occur in groups along the lymphatics
    -   Lymph flows through more than one lymph node on its journey back to the blood
-   **Lymph nodes are the major site of the Basic Stages 2 and 3 of the [lymphocyte's]({{< relref "lymphocyte" >}}) life cycle** (search for its cognate antigen and activation)


#### Parts of the lymph node: {#parts-of-the-lymph-node}

{{< figure src="/ox-hugo/_20211009_220911screenshot.png" caption="Figure 1: Anatomy of a lymph node" width="700" >}}

-   There is a distinctive _hilum_ (circled) where a small artery enters and a vein exits (blood vessels not illustrated)
    -   The _efferent lymphatic_ (10) also exits here
    -   This hilum occupies only a very small percentage of the lymph node surface
-   The remainder of the surface has a thin _capsule_ (7) of connective tissue with _trabeculae_ (6) that penetrate inward
-   Lymph enters through _afferent lymphatics_ (1) that perforate the capsule
    -   There is a circumferential _marginal sinus_ (aka subcapsular sinus - 8) and _trabecular sinuses_ that parlalel the trabeculae and lead inwards to a labyrinth of _medullary sinuses_ (4)
-   The medullary sinuses come together to form the _efferent lymphatic_ (10)
-   The lymph node is divided into an **outer cortex of follicles and paracortex** and an **inner medulla comprised of medullary sinuses** and the _medullary cords_ (5)
-   Nodular aggregates of lymphocytes in the cortex are called lymphoid follicles (9)
    -   Follicles consist mostly of [B-lymphocytes]({{< relref "b_lymphocyte" >}})
    -   When activated -> follicle has a **pale germinal center with a dark rim of small resting B-lymphocytes** = _mantle zone_
-   The paracortex (3) is the part of the cortex without follicles
    -   Comprised of [T-lymphocytes]({{< relref "t_lymphocyte" >}}) with scattered [APCs]({{< relref "antigen_presenting_cell" >}})

-   _Interdigitating dendritic cells_ are a type of APC that are sprinkled among the T-lymphocytes of the paracortex
-   [Histiocytes]({{< relref "histiocyte" >}}) (macrophages) mostly reside in the sinuses
    -   Histiocytes filter antigen and debris from the sinuses and may migrate to the paracortex -> differentiate into APCs
-   Medullary cords contain numerous plasma cells and lymphocytes preparing to leave the lymph node
-   [High endothelial venule]({{< relref "high_endothelial_venule" >}}) (HEV), a small post-capillary vein (i.e. venule) in the paracortex, is where [lymphocytes]({{< relref "lymphocyte" >}}) move from blood into the lymph node
-   The overall structure of a lymph node is formed by [reticulin fibers]({{< relref "reticulin_fiber" >}}) - a thin/fine type of collagen that allows easy movement of immune cells


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon] </span></span> > Lymphatic and Immune Systems > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
