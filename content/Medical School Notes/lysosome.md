+++
title = "Lysosome"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   An [organelle]({{< relref "organelle" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles**

    <!--list-separator-->

    -  [Lysosomes]({{< relref "lysosome" >}})

        <!--list-separator-->

        -  Structure

            -   Dense membrane-bound organelles
            -   Can be identified in sections of tissue by cytochemical staining for _acid phosphatase_
            -   Have special membrane proteins and ~50 acid _hydrolases_ synthesized in the RER
            -   _ATP-powered proton pumps_ maintain **acid pH < 5**

        <!--list-separator-->

        -  Formation

            -   Sequestered material fuses with _late endosome_ -> enzymatic degradation begins

            <!--list-separator-->

            -  Early endosomes

                -   Irregular vesicles found near cell periphery
                -   Form part of pathway for _[receptor-mediated endocytosis]({{< relref "receptor_mediated_endocytosis" >}})_
                -   **CURL**: Compartment for Uncoupling of Receptors and Ligands
                -   Acidic interior (pH < 6) maintained by ATP-driven proton pumps
                    -   Acidity aids in uncoupling of receptors and ligands
                        -   Receptors return to plasma membrane
                        -   Ligands move to a late endosome

            <!--list-separator-->

            -  Late endosomes

                -   **Play a key role in various lysosomal pathways**
                    -   Because of this, sometimes called "intermediate compartment"
                -   pH < 5.5
                -   Lie deep within the cell
                -   Receive ligands via microtubular transport of vesicles from early endosomes
                -   Contain both _lysosomal hydrolases_ and _lysosomal membrane proteins_
                    -   Formed in the [RER]({{< relref "rough_endoplasmic_reticulum" >}}) -> transported to Golgi complex -> delivered in separate vesicles to late endosomes
                    -   Once late endosomes receive these, they begin to degrade their ligands -> **officially classified ad lysosomes**

            <!--list-separator-->

            -  Types of lysosomes

                -   Lysosomes are named after **the content of recognizable material**
                    -   Otherwise, just called "lysosome"

                <!--list-separator-->

                -  _Multivesicular bodies_

                    -   Formed by fusion of **early endosome containined endocytic vesicles** and a **late endosome**

                <!--list-separator-->

                -  _Phagolysosomes_

                    -   Formed by fusion of **phagocytic vacuole** and a **late endosome or lysosome**

                <!--list-separator-->

                -  _Autophagolysosomes_

                    -   Formed by fusion of **autophagic vacuole** and a **late endosome or lysosome**
                    -   Formed when cell components targeted for destruction become enveloped by smooth areas of membranes derived from RER

                <!--list-separator-->

                -  _Residual bodies_

                    -   Lysosomes **of any time** that have **expended their capacity to degrade material**
                    -   Are eventually excreted from the cell


### Unlinked references {#unlinked-references}

[Show unlinked references]
