+++
title = "Compare and contrast the null hypothesis and the alternative hypothesis"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}


### [Null hypothesis]({{< relref "null_hypothesis" >}}) (H<sub>0</sub>) {#null-hypothesis--null-hypothesis-dot-md----h}

-   The assumption that there **is no relationship** between the two measured variables or **no significant difference** between two studied populations
-   Statistical tests are used to either **reject** or **accept** this hypothesis


### [Alternative hypothesis]({{< relref "alternative_hypothesis" >}}) (H<sub>1</sub>) {#alternative-hypothesis--alternative-hypothesis-dot-md----h}

-   The assumption that there **is a relationship** between two measured variables or **a significant difference** between two studied populations
-   The counterpart to the null hypothesis


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > The Practice of Evidence-Based Medicine: Inferential Statistics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Compare and contrast the null hypothesis and the alternative hypothesis]({{< relref "compare_and_contrast_the_null_hypothesis_and_the_alternative_hypothesis" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
