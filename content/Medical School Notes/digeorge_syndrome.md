+++
title = "DiGeorge syndrome"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   A congenital [T-cell]({{< relref "t_lymphocyte" >}}) deficiency
-   Associated with:
    -   [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}})
    -   [Atrial septal defect]({{< relref "atrial_septal_defect" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}}) is associated with [DiGeorge syndrome]({{< relref "digeorge_syndrome" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Immunology**

    <!--list-separator-->

    -  [DiGeorge syndrome]({{< relref "digeorge_syndrome" >}})

        -   22q11.2 deletion
        -   Autosomal dominant

        <!--list-separator-->

        -  Clinical features:

            <!--list-separator-->

            -  Cardiac anomalies

                -   [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}})
                -   [Atrial septal defect]({{< relref "atrial_septal_defect" >}})

            <!--list-separator-->

            -  Anomalous face

            <!--list-separator-->

            -  Thymus aplasia/hypoplasia

                -   Recurent infections due to [T-cell]({{< relref "t_lymphocyte" >}}) deficiency

            <!--list-separator-->

            -  Cleft palate

            <!--list-separator-->

            -  Hypoparathyroidism

                -   Hypocalcemia with tetany


### Unlinked references {#unlinked-references}

[Show unlinked references]
