+++
title = "Lactate dehydrogenase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The only way in which [lactate]({{< relref "lactate" >}}) is utilized physiologically
-   Circulating levels can indicate [necrosis]({{< relref "necrosis" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [List the four enzymes, and their intracellular location, for which pyruvate serves as a substrate, and the product that is made]({{< relref "list_the_four_enzymes_and_their_intracellular_location_for_which_pyruvate_serves_as_a_substrate_and_the_product_that_is_made" >}}) {#list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made--list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made-dot-md}

<!--list-separator-->

-  **🔖 Enzymes > Cytosol**

    [Lactate DH]({{< relref "lactate_dehydrogenase" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Pyruvate metabolism**

    <!--list-separator-->

    -  NAD<sup>+</sup>-linked [lactate dehydrogenase]({{< relref "lactate_dehydrogenase" >}})

        -   A cytosolic enzyme
        -   Catalyzes a **freely reversible** redox reaction between _lactate_ and _pyruvate_
        -   **The only way in which lactate is utilized physiologically**
            -   Lactate released from anaerobic glyolysis occuring in RBCs + exercising muscle -> taken up by liver + used as a gluconeogenic substrate


### Unlinked references {#unlinked-references}

[Show unlinked references]
