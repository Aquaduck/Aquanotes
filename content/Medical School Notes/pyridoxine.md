+++
title = "Pyridoxine"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   AKA _Vitamin B<sub>6</sub>_
-   [B vitamin]({{< relref "b_vitamin" >}})


## From First Aid 2015 {#from-first-aid-2015}


### Function {#function}

-   Converted to pyridoxal phosphate (PLP), a cofactor used in:
    -   Transamination
    -   Decarboxylation reactions
    -   [Glycogen phosphorylase]({{< relref "glycogen_phosphorylase" >}})
    -   Synthesis of:
        -   Cystathionine
        -   Heme
        -   [Niacin]({{< relref "niacin" >}})
        -   Histamine
        -   Neurotransmitters:
            -   Serotonin
            -   Epinephrine
            -   Norepinephrine
            -   Dopamine
            -   Gaba


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Osmosis - Gluconeogenesis]({{< relref "osmosis_gluconeogenesis" >}}) {#osmosis-gluconeogenesis--osmosis-gluconeogenesis-dot-md}

<!--list-separator-->

-  **🔖 Notes > Sources of pyruvate**

    Transaminase [enzymes]({{< relref "enzyme" >}}) require _pyridoxine ([vitamin B<sub>6</sub>]({{< relref "pyridoxine" >}}))_ as a cofactor

    ---


#### [Niacin]({{< relref "niacin" >}}) {#niacin--niacin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Synthesis requires [vitamins B<sub>2</sub>]({{< relref "riboflavin" >}}) and [B<sub>6</sub>]({{< relref "pyridoxine" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    [PEP CK]({{< relref "pep_carboxykinase" >}}) requires [Pyridoxine]({{< relref "pyridoxine" >}}) (B6) (as pyridoxal phosphate) - a cofactor used in decarboxylation reactions

    ---


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Cellular Metabolism > 14. List the vitamins required for the appropriate functioning of: > Gluconeogenesis**

    [Pyridoxine]({{< relref "pyridoxine" >}})

    ---


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By vitamin**

    <!--list-separator-->

    -  [Pyridoxine]({{< relref "pyridoxine" >}}) (B6)

        -   PEP CK


### Unlinked references {#unlinked-references}

[Show unlinked references]
