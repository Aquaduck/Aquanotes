+++
title = "Pyruvate kinase"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Final enzyme in the [glycolytic]({{< relref "glycolysis" >}}) pathway
-   [Allosterically]({{< relref "allosteric_regulation" >}}) inhibited by: [ATP]({{< relref "atp" >}})
-   Deficiencies in the RBC isozyme cause hemolytic [anemia]({{< relref "anemia" >}})
-   Produces [pyruvate]({{< relref "pyruvate" >}}) from [PEP]({{< relref "phosphoenolpyruvate" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Insulin's metabolic effects**

    Restores [pyruvate kinase]({{< relref "pyruvate_kinase" >}}) activity -> stops [gluconeogenesis]({{< relref "gluconeogenesis" >}}) by immediately converting any PEP made back to pyruvate

    ---


#### [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}}) {#list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated--list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated-dot-md}

<!--list-separator-->

-  **🔖 Examples in glucose catabolism > Covalent modification**

    [Pyruvate kinase]({{< relref "pyruvate_kinase" >}})

    ---

<!--list-separator-->

-  **🔖 Examples in glucose catabolism > Allosteric control**

    [Pyruvate kinase]({{< relref "pyruvate_kinase" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Glycolysis**

    <!--list-separator-->

    -  [Pyruvate kinase]({{< relref "pyruvate_kinase" >}})

        -   Final enzyme in the glycolytic pathway
        -   Catalyzes the third irreversible + second substrate-level phosphorylation reaction
        -   Produces [pyruvate]({{< relref "pyruvate" >}}) from [phosphoenolpyruvate]({{< relref "phosphoenolpyruvate" >}}) (PEP)
        -   **Allosterically inhibited** by [ATP]({{< relref "atp" >}}) in all tissues
        -   When glucose is plentiful and PFK-1 is active, fructose 1,6-bisphosphate will feed forward and activate the enzyme
        -   Deficiencies in the red blood cell isozyme causes hemolytic anemia

        <!--list-separator-->

        -  The liver isozyme is uniquely regulated

            -   Allosterically inhibited by ATP, _alanine_, and _long-chain fatty acids_
                -   All of which are plentiful during gluconeogenesis
            -   A substrate of protein kinase A
                -   **Downregulated** by phosphorylation


#### [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}}) {#describe-the-intercellular-response-to-insulin-induced-cellular-signaling--describe-the-intercellular-response-to-insulin-induced-cellular-signaling-dot-md}

<!--list-separator-->

-  **🔖 Insulin's metabolic effects**

    Restores [pyruvate kinase]({{< relref "pyruvate_kinase" >}}) activity -> stops [gluconeogenesis]({{< relref "gluconeogenesis" >}}) by immediately converting any PEP made back to pyruvate

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
