+++
title = "Gout"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Diseases associated with purine degradation > Gout > Overproduction of uric acid**

    Less common cause of [Gout]({{< relref "gout" >}})

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Diseases associated with purine degradation > Gout > Underexcretion of uric acid**

    The cause of the vast majority of [Gout]({{< relref "gout" >}}) cases

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Diseases associated with purine degradation**

    <!--list-separator-->

    -  [Gout]({{< relref "gout" >}})

        -   Characterized by high levels of [Uric acid]({{< relref "uric_acid" >}}) in the blood ([hyperuricemia]({{< relref "hyperuricemia" >}})) as a result of either **overproduction** or **underexcretion** of **uric acid**
        -   [Hyperuricemia]({{< relref "hyperuricemia" >}}) -> deposition of monosodium urate crystlas in the joints -> triggers inflammatory response to the crystals -> _gouty arthritis_

        <!--list-separator-->

        -  Underexcretion of [uric acid]({{< relref "uric_acid" >}})

            -   The cause of the vast majority of [Gout]({{< relref "gout" >}}) cases

        <!--list-separator-->

        -  Overproduction of [uric acid]({{< relref "uric_acid" >}})

            -   Less common cause of [Gout]({{< relref "gout" >}})
            -   Mutations in X-linked [PRPP synthetase]({{< relref "prpp_synthetase" >}}) gene -> increased V<sub>max</sub> for the production of PRPP + lower K<sub>m</sub> for ribose 5-phosphate, or decresaed sensitivity to purine nucleotides (allosteric inhibitors)
                -   Increased availability of PRPP -> **increased purine production** -> **elevated levels of plasma uric acid**


### Unlinked references {#unlinked-references}

[Show unlinked references]
