+++
title = "Inositol-tris-phosphate"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}) {#describe-the-inositol-phosphatide-second-messenger-system--describe-the-inositol-phosphatide-second-messenger-system-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Important members**

    <!--list-separator-->

    -  [Inositol-tris-phosphate]({{< relref "inositol_tris_phosphate" >}}) (IP<sub>3</sub>)

        -   Created through Phospholipase C-β cleavage of PIP<sub>2</sub>


### Unlinked references {#unlinked-references}

[Show unlinked references]
