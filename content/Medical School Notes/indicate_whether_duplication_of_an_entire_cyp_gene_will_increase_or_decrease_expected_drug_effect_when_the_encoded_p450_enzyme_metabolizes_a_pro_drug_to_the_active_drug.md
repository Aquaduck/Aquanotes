+++
title = "Indicate whether duplication of an entire CYP gene will increase or decrease expected drug effect, when the encoded P450 enzyme metabolizes a pro-drug to the active drug."
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Pharmacogenetics Pre-learning Material]({{< relref "pharmacogenetics_pre_learning_material" >}}) {#from-pharmacogenetics-pre-learning-material--pharmacogenetics-pre-learning-material-dot-md}


### [Pro-drug]({{< relref "pro_drug" >}}) {#pro-drug--pro-drug-dot-md}

-   A drug that is initially **inactive** -> **metabolism activates it**
-   Duplication of a CYP gene -> **increase in available activity** -> individual classified as an ultrarapid metabolizer
    -   This can lead to **increased risk of overdose** since the increased reactivity competitively inhibits inactivation


### [Active drug]({{< relref "active_drug" >}}) {#active-drug--active-drug-dot-md}

-   A drug that is initially **active** -> **metabolism inactivates it**
-   Duplication of a CYP gene -> **decrease in available activity**
    -   Drug is very quickly inactivated, preventing the drug from reaching therapeutic doses


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-24 Fri] </span></span> > Pharmacogenetics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Indicate whether duplication of an entire CYP gene will increase or decrease expected drug effect, when the enzyme metabolizes the active drug to an inactive metabolite.]({{< relref "indicate_whether_duplication_of_an_entire_cyp_gene_will_increase_or_decrease_expected_drug_effect_when_the_encoded_p450_enzyme_metabolizes_a_pro_drug_to_the_active_drug" >}})

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-24 Fri] </span></span> > Pharmacogenetics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Indicate whether duplication of an entire CYP gene will increase or decrease expected drug effect, when the encoded P450 enzyme metabolizes a pro-drug to the active drug.]({{< relref "indicate_whether_duplication_of_an_entire_cyp_gene_will_increase_or_decrease_expected_drug_effect_when_the_encoded_p450_enzyme_metabolizes_a_pro_drug_to_the_active_drug" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
