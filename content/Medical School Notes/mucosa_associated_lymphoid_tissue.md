+++
title = "Mucosa-associated lymphoid tissue"
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Lymphoid Nodules (p. 997)**

    <!--list-separator-->

    -  [MALT]({{< relref "mucosa_associated_lymphoid_tissue" >}})

        -   An aggregate of lymphoid follicles directly assoicated with the mucous membrane epithelia
        -   Dome-shaped structures found underlying mucosa of:
            -   GI tract
            -   Breast tissue
            -   Lungs
            -   Eyes
        -   _Peyer's patches_: contain specialized endothelial cells (called _M (microfold) cells_) -> sample material from intestinal lumen -> transport to nearby follicles -> allows adaptive immune response to potential pathogens

        {{< figure src="/ox-hugo/_20211018_141838screenshot.png" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
