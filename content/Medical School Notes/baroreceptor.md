+++
title = "Baroreceptor"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Input to the Cardiovascular Center (p.879)**

    <!--list-separator-->

    -  [Baroreceptor]({{< relref "baroreceptor" >}})

        -   Stretch receptors located in the aortic sinus, carotid bodies, venae cavae, and other locations such as pulmonary vessels and the right side of the heart itself
        -   Rates of firing from baroreceptors represent blood pressure, level of physical activity, and relative distribution of blood
        -   [Baroreceptor reflex]({{< relref "baroreceptor_reflex" >}}): Cardiac center monitoring of baroreceptor firing to maintain cardiac homeostasis
            -   In response to **increased pressure and stretch** -> rate of baroreceptor firing increases -> cardiac centers decrease sympathetic stimulation + increase parasympathetic stimulation
            -   In response to **decreased pressure and stretch** -> rate of baroreceptor firing decreases -> cardiac centers increase sympathetic stimulation + decrease parasympathetic stimulation


### Unlinked references {#unlinked-references}

[Show unlinked references]
