+++
title = "Visceral pleura"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [Pleura]({{< relref "pleura" >}}) that is superficial to the lungs, extending into and lining the lung fissures


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Pleura of the lung (p. 1057)**

    [Visceral pleura]({{< relref "visceral_pleura" >}}): the layer superficial to the lungs, extending into and lining the lung fissures

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
