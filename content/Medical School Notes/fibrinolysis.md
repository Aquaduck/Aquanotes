+++
title = "Fibrinolysis"
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Final step in [clotting]({{< relref "blood_clotting" >}}) to restore normal circulation by breaking down the clot


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  [Fibrinolysis]({{< relref "fibrinolysis" >}}) (p. 818)

    -   Stablized clot is acted upon by contractile proteins within platelets
    -   To restore normal blood flow, the clot must be removed -> _Fibrinolysis_
    -   Inactive protein plasminogen converted to [plasmin]({{< relref "plasmin" >}}) -> gradually breaks down [fibrin]({{< relref "fibrin" >}}) of the clot
    -   [Bradykinin]({{< relref "bradykinin" >}}) is released -> vasodilation -> reverses the effects of serotonin and prostaglandins from platelets
        -   Allows smooth muscle in the walls of the vessels to relax + restore circulation


### Unlinked references {#unlinked-references}

[Show unlinked references]
