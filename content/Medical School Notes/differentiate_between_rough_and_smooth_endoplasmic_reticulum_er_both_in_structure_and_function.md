+++
title = "Differentiate between rough and smooth endoplasmic reticulum (ER) both in structure and function."
author = ["Arif Ahsan"]
date = 2021-09-02T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

from [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}})


## [Rough endoplasmic reticulum]({{< relref "rough_endoplasmic_reticulum" >}}) (RER) {#rough-endoplasmic-reticulum--rough-endoplasmic-reticulum-dot-md----rer}


### Structure {#structure}

-   System of membrane-bound sacs/cavities
-   Outer surface studded with ribosomes (which is what "rough" means)
-   _Cisterna or lumen_: interior region
-   Outer nuclear membrane **continuous** with RER membrane
-   _Ribophorins_: receptors where large [ribosomal]({{< relref "ribosome" >}}) subunits bind
-   Abundant in cells synthesizing **secretory proteins**
    -   In these cells, RER organized into many parallel arrays
-   RER sac closest to the Golgi apparatus makes buds w/o ribosomes -> form vesicles
    -   This is considered a **transitional element**


### Function {#function}

-   Location where **membrane-packaged [proteins]({{< relref "protein" >}})** are synthesized
    -   Secretory proteins
    -   Plasma membrane proteins
    -   Lysosomal proteins
-   Monitors the assembly, retention, and degradation of certain proteins


## [Smooth endoplasmic reticulum]({{< relref "smooth_endoplasmic_reticulum" >}}) (SER) {#smooth-endoplasmic-reticulum--smooth-endoplasmic-reticulum-dot-md----ser}


### Structure {#structure}

-   Irregular network of membrane-bound channels that **lacks ribosomes on its surface** (why its called "smooth")
-   Usually appears as branching, cross-connected (_anastomosing_) _tubules_ or _vesicles_ whose membranes **do not contain ribophorins**
-   Less common than RER
-   Prominent in cells synthesizing steroids, triglycerides, and cholesterol


### Function {#function}

-   **Steroid hormone synthesis** occurs in SER-rich cells such as Leydig cells (make testosterone)
-   **Drug detoxification** occurs in hepatocytes
    -   Follows proliferation of the SER in response to the drug _phenobarbital_
        -   The oxidases that metabolize this drug are located in the SER
-   **Muscle contraction and relaxation** involve the release and recapture of Ca<sup>2+</sup> by the SER in **skeletal muscle cells**
    -   These are called the _sarcoplasmic reticulum_


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-01 Wed] </span></span> > Organelles & Trafficking I (+ II) > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Differentiate between rough and smooth endoplasmic reticulum (ER) both in structure and function.]({{< relref "differentiate_between_rough_and_smooth_endoplasmic_reticulum_er_both_in_structure_and_function" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
