+++
title = "Session 12"
author = ["Arif Ahsan"]
date = 2021-07-27T00:00:00-04:00
tags = ["medschool", "jumpstart"]
draft = false
+++

## gRAT {#grat}

1.  2
2.  1
3.  5
4.  3
5.  4
6.  5
7.  2
8.  2
9.  1
10. 3


## TCA Cycle Worksheet {#tca-cycle-worksheet}

1.  Isocitrate, Oxaloacetate
2.  Metabolites:
    -   Citrate: 6
    -   Isocitrate: 6
    -   \\(\alpha\\)-ketoglutarate: 5
    -   Succinyl-CoA: 4
    -   Succinate: 4
    -   Fumarate: 4
    -   Malate: 4
    -   Oxaloacetate: 4
3.  Reactions:
    3 - Isocitrate DH (**NAD-linked**)
    4 - \\(\alpha\\)-ketoglutarate DH
    5 - Succinyl-CoA synthetase
    8 - Malate DH
4.  3 and 4
5.  Isocitrate DH
    Synthases don't require as much energy and second step energetically favors the formation of citrate
6.  Enzymes:
    -   Isocitrate DH - rate-limiting and committed step
7.  Succinyl-CoA Synthetase
8.  Isocitrate DH, alpha-kg DH, malate DH
9.  Succinate DH
10. Occurs in the inner-membrane as Complex II in ETC, and uses FAD<sup>+​</sup> instead of NAD<sup>+</sup>
11. Reactions:
    -   4, B<sub>1, 2, 3, 5</sub>
    -   2, B<sub>1</sub>
12. alpha-kg DH, requires lipoic acid which is inactivated by Arsenic
13. Succinyl-CoA
14. Citrate
15. 12.5 ATP - _won't be asked this question on STEP I or ever thank god_
16. -
    1.  Increased [citrate]: none
    2.  Increased [ATP]: decrease
    3.  Increased [G6P]: increase
    4.  Increased [succinyl-CoA]: increase
17. E
18. A
    -   Succinyl-CoA can't cross the mitochondrial membrane


## Application Questions {#application-questions}

1.  C
2.  C

**NADH creates more ATP**


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
