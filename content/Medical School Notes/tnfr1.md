+++
title = "TNFR1"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The main [TNFR]({{< relref "tumor_necrosis_factor_receptor" >}}) that transduces signals from [TNF-α]({{< relref "tumor_necrosis_factor" >}})
    -   Regular function is to induce [NFκB]({{< relref "nfκb" >}}) and promote cell survival and growth
        -   **Only** when this pathway is disrupted does it induce cell death
-   Signalling to [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) is cell-type specific


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Tumor necrosis factor]({{< relref "tumor_necrosis_factor" >}}) {#tumor-necrosis-factor--tumor-necrosis-factor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Default signaling pathway is to bind to [TNFR1]({{< relref "tnfr1" >}}) and induce [NFκB]({{< relref "nfκb" >}})

    ---


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) in [TNF]({{< relref "tumor_necrosis_factor" >}})/[TNFR1]({{< relref "tnfr1" >}}) signalling

    -   The default signaling pathway activating by the binding of TNF alpha is the induction of [NFκB]({{< relref "nfκb" >}}) -> responsible for **cell survival and growth**
    -   TNFR1 induces cell death (apoptosis AND necrosis) **only when [NFκB]({{< relref "nfκb" >}}) activation is impaired**
    -   Dependence of TNFR1 signaling on TRADD appears to be **cell-type specific**


#### [Death receptor]({{< relref "death_receptor" >}}) {#death-receptor--death-receptor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [TNFR1]({{< relref "tnfr1" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
