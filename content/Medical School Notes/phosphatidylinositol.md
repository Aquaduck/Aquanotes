+++
title = "Phosphatidylinositol"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}) {#describe-the-inositol-phosphatide-second-messenger-system--describe-the-inositol-phosphatide-second-messenger-system-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Important members > Membrane phosphatidylinositol**

    <!--list-separator-->

    -  [PIP<sub>2</sub>]({{< relref "phosphatidylinositol" >}})

        -   Naturally occuring membrane phospholipid
        -   Composed of a glycerol backbone attached to two fatty acids and the sugar _inositol_ via a covalently bound phosphate
            -   The inositol is bound to 2 phosphate groups


### Unlinked references {#unlinked-references}

[Show unlinked references]
