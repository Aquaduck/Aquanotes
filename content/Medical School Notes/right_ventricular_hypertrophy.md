+++
title = "Right ventricular hypertrophy"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   A thickening of the muscular wall of the [right ventricle]({{< relref "right_ventricle" >}}) of the heart


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}}) {#tetralogy-of-fallot--tetralogy-of-fallot-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Right ventricular hypertrophy]({{< relref "right_ventricular_hypertrophy" >}}) (RVH)

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Tetralogy of Fallot > Overview**

    <!--list-separator-->

    -  [Right ventricular hypertrophy]({{< relref "right_ventricular_hypertrophy" >}}) (RVH)

        -   A thickening of the muscular wall of the right ventricle of the heart
            -   Usually due to chronic pumping against increased resistance
                -   E.g. pulmonary stenosis, pulmonary hypertension, chronic lung disease


### Unlinked references {#unlinked-references}

[Show unlinked references]
