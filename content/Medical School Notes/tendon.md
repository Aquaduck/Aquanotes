+++
title = "Tendon"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons**

    <!--list-separator-->

    -  [Tendons]({{< relref "tendon" >}})

        -   Connect muscles to bones
        -   Made of dense regular CT
        -   Connections at myotendinous junction is an intertwining of dense CT with finger-like projections of sarcolemmal (skeletal muscle) membrane
        -   Dense irregular connective tissue of [Periosteum]({{< relref "periosteum" >}}) mostly is oriented parallel to the surface of the bone
            -   At the site of tendinous and [ligamentous]({{< relref "ligament" >}}) insertions there are collagen fibers extending perpendicularly into the bone - [Sharpey's fibers]({{< relref "sharpey_s_fibers" >}})


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Structure of Skeletal Muscle (p. 109) > Connective tissue investments > Epimysium**

    [Tendons]({{< relref "tendon" >}}) connect muscle to bone

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
