+++
title = "Glycerol"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Taken up by the [liver]({{< relref "liver" >}}) following cleavage of [Triacylglycerol]({{< relref "triacylglycerol" >}}) via [LPL]({{< relref "lipoprotein_lipase" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Albumin-bound fatty acids > From Fat Metabolism in Muscle & Adipose Tissue**

    **Released [glycerol]({{< relref "glycerol" >}}) is used primarly by the [liver]({{< relref "liver" >}}) as a [gluconeogenic]({{< relref "gluconeogenesis" >}}) substrate**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})

    ---


#### [Lipolysis]({{< relref "lipolysis" >}}) {#lipolysis--lipolysis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The breakdown of [triglycerides]({{< relref "triacylglycerol" >}}) into [glycerol]({{< relref "glycerol" >}}) and [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}}) {#explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot--explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Delivering dietary fat to extrahepatic tissues via chylomicrons**

    Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
