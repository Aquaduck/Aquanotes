+++
title = "Review the mechanisms that set up the resting membrane potential."
author = ["Arif Ahsan"]
date = 2021-09-11T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Forehand, Action Potential]({{< relref "forehand_action_potential" >}}) {#from-forehand-action-potential--forehand-action-potential-dot-md}


### A steady state potential develops when more than one ion is permeable {#a-steady-state-potential-develops-when-more-than-one-ion-is-permeable}

-   Neurons have **negative resting potentials relative to extracellular space**
    -   More negative = _hyperpolarized_
    -   Less negative = _depolarized_
-   Extracellular concentration of sodium is **much greater** than the intracellular concentration of sodium
    -   The **opposite** is true for potassium
-   Membrane is **much more permeable** to **potassium over sodium**
    -   More leakage channels (non-gated) for potassium in the membrane
    -   Because of this, **resting membrane potential is much closer to the equilibrium potential for potassium**
-   Resting membrane potential is quite sensitive to **extracellular potassium concentrations**
-   Because sodium is far from its equilibrium potential, there is a very **large driving force on sodium** (i.e. sodium wants to move)
    -   Thus, sodium ions move readily whenever a voltage-gated or ligand-gated sodium channel opens in the membrane


### Variables {#variables}

-   _E<sub>m</sub>_: membrane potenital
-   _E<sub>Na</sub>_ and _E<sub>K</sub>_: equilibrium potentials defined by the [Nernst equation]({{< relref "nernst_equation" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-10 Fri] </span></span> > Passive Membrane Properties, the Action Potential, and Electrical Signaling by Neurons > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Review the mechanisms that set up the resting membrane potential.]({{< relref "review_the_mechanisms_that_set_up_the_resting_membrane_potential" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
