+++
title = "Eukaryote"
author = ["Arif Ahsan"]
date = 2021-08-17T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Steps in replication**

    [Histones]({{< relref "histone" >}}) must be removed and then replaced during the replication process -> accounts for lower replication rate in [eukaryotes]({{< relref "eukaryote" >}})

    ---

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes**

    **14 [DNA polymerases]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})**

    ---


#### [DNA Polymerase-δ]({{< relref "dna_polymerase_δ" >}}) {#dna-polymerase-δ--dna-polymerase-δ-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A type of [DNA Polymerase]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})

    ---


#### [DNA Polymerase-ɛ]({{< relref "dna_polymerase_ɛ" >}}) {#dna-polymerase-ɛ--dna-polymerase-ɛ-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A type of [DNA Polymerase]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})

    ---


#### [DNA Polymerase-ɑ]({{< relref "dna_polymerase_ɑ" >}}) {#dna-polymerase-ɑ--dna-polymerase-ɑ-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A type of [DNA Polymerase]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
