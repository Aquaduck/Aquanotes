+++
title = "GLUT-4"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Overview of the most important glucose transporters]({{< relref "overview_of_the_most_important_glucose_transporters" >}}) {#overview-of-the-most-important-glucose-transporters--overview-of-the-most-important-glucose-transporters-dot-md}

|                                  |                |                                                         |     |
|----------------------------------|----------------|---------------------------------------------------------|-----|
| [GLUT4]({{< relref "glut_4" >}}) | Adipose tissue | Plays a key role in regulating body glucose homeostasis | Yes |

---


#### [GLUT4 Exocytosis]({{< relref "glut4_exocytosis" >}}) {#glut4-exocytosis--glut4-exocytosis-dot-md}

<!--list-separator-->

-  **🔖 Notes**

    <!--list-separator-->

    -  [GLUT-4]({{< relref "glut_4" >}}) resides in specialised vesicles called [GSVs]({{< relref "glut4_storage_vesicles" >}}) that undergo insulin-dependent translocation to the plasma membrane

        -   This happens in the absense of [insulin]({{< relref "insulin" >}})


#### [Distinguish the insulin-responsive glucose membrane transporter and in what tissues it is located, from that used by:]({{< relref "distinguish_the_insulin_responsive_glucose_membrane_transporter_and_in_what_tissues_it_is_located_from_that_used_by" >}}) {#distinguish-the-insulin-responsive-glucose-membrane-transporter-and-in-what-tissues-it-is-located-from-that-used-by--distinguish-the-insulin-responsive-glucose-membrane-transporter-and-in-what-tissues-it-is-located-from-that-used-by-dot-md}

[Overview of the most important glucose transporters]({{< relref "overview_of_the_most_important_glucose_transporters" >}})
The insulin-responsive glucose membrane transporter is [GLUT-4]({{< relref "glut_4" >}}) and is found in:

---


### Unlinked references {#unlinked-references}

[Show unlinked references]
