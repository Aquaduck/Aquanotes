+++
title = "Lamin"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}}) {#rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot--rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot-md}

<!--list-separator-->

-  **🔖 From Protein farnesylation and disease**

    <!--list-separator-->

    -  Nuclear [lamins]({{< relref "lamin" >}})

        -   Humans have **three** distinct lamin genes that encode seven different proteins

        <!--list-separator-->

        -  [A-type lamins]({{< relref "lamin_a" >}})

            -   Products of a single gene termed [LMNA]({{< relref "lmna" >}})
            -   [Prelamin A]({{< relref "prelamin_a" >}}) is the precursor protein of mature lamin A and possesses a COOH terminal CAAX motif -> the site of post-translational modifications
            -   Plays a major role in **nuclear envelope architecture** and a functional role in **heterochromatin organization, cell cycle, differentiation dynamics, and transcriptional regulation**
            -   Undergoes post-translational modification via [farnesylation]({{< relref "farnesylation" >}})


#### [Lamin A]({{< relref "lamin_a" >}}) {#lamin-a--lamin-a-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [Lamin]({{< relref "lamin" >}}) protein

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Caspase > Targets of caspase**

    Nuclear [lamins]({{< relref "lamin" >}}) -> irreversible breakdown of nuclear lamina

    ---


#### [Caspase]({{< relref "caspase" >}}) {#caspase--caspase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Nuclear [lamins]({{< relref "lamin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
