+++
title = "Gene"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   A unit of [genetic]({{< relref "genetics" >}}) material


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}}) {#osmosis-transcription-translation-and-replication--osmosis-transcription-translation-and-replication-dot-md}

<!--list-separator-->

-  [Genetic]({{< relref "gene" >}}) Mutations and Repair

    <!--list-separator-->

    -  DNA mutations

        -   Alterations in nucleotide sequence of one or more genes

    <!--list-separator-->

    -  Small-scale mutations

        -   **Single gene**
        -   _Substitutions_: nucleotide replaced by another
        -   May result in:
            -   _Silent mutation_: results in same AA -> no change
            -   _Missense mutation_: different AA
            -   _Nonsense mutation_: stop codon

        {{< figure src="/ox-hugo/_20210721_155602screenshot.png" width="500" >}}

    <!--list-separator-->

    -  Insertions and deletions

        -   Nucleotide is added or removed from sequence
        -   _Frameshift mutation_: Reading frame displaced, resulting in complete change in protein AA sequence (and consequently function)

    <!--list-separator-->

    -  Large-scale mutations

        -   Often occur due to errors in gamete formation

        <!--list-separator-->

        -  Abnormal number of chromosomes

            -   _Aneuploidy_: Additional or missing chromosomes
            -   _Polyploidy_: Increased number of chromosomes per set

            {{< figure src="/ox-hugo/_20210721_155634screenshot.png" width="500" >}}

        <!--list-separator-->

        -  Structurally abnormal

            -   Movement of sections of chromosomes
            -   _Deletion_: part of chromosome goes missing
            -   _Duplication_: part of chromosome duplicated
            -   _Inversion_: part of chromosome breaks off and reattaches
            -   _Translocation_: parts of two chromosomes are switched

            {{< figure src="/ox-hugo/_20210721_155649screenshot.png" width="500" >}}

    <!--list-separator-->

    -  DNA damage

        -   DNA can be damaged by internal (_endogenous_) or environmental (_exogenous_) factors
        -   If damaged DNA cannot be fixed ->
            -   _Senescence_: ceasing of cell division
            -   _Apoptosis_: programmed cell death
            -   Uncontrolled cell division -> tumor growth
        -   If damaged DNA can be fixed -> G<sub>0</sub> phase

        <!--list-separator-->

        -  Single strand damage

            -   Causes:
                -   Endogenous: errors in DNA replication
                -   Exogenous: harmful chemical/physical agents
            -   Repaired with mismatch/base excision/nucleotide excision repair
                1.  Endonucleases cleave damaged segment
                2.  Exonucleases removed damaged segment
                3.  [DNA polymerase]({{< relref "dna_polymerase" >}}) rebuilds segment
                4.  DNA ligase glues new segment

            {{< figure src="/ox-hugo/_20210721_155730screenshot.png" width="500" >}}

        <!--list-separator-->

        -  Double stranded breaks

            -   Can occur due to **ionizing radiation**

            {{< figure src="/ox-hugo/_20210721_155754screenshot.png" width="500" >}}

            <!--list-separator-->

            -  Repair mechanisms

                -   _Non-homologous end joining_:
                    -   DNA protein kinase binds to each end of the broken DNA -> artemis cuts off rough ends -> ends are rejoined with DNA ligase
                -   _Homologous end joining_:
                    -   MRN protein complex binds to each end and removes affected nucleotides -> [DNA polymerase]({{< relref "dna_polymerase" >}}) copies genetic information from sister chromatid

<!--list-separator-->

-  **🔖 Translation**

    Second step in creating protein from [gene]({{< relref "gene" >}})

    ---

<!--list-separator-->

-  **🔖 Transcription**

    First step in creating protein from [gene]({{< relref "gene" >}})

    ---

<!--list-separator-->

-  **🔖 DNA Structure > Nucleotides > DNA structure and packing**

    _Heterchromatin_: densely packed ([genes]({{< relref "gene" >}}) rarely used)

    ---

<!--list-separator-->

-  **🔖 DNA Structure > Nucleotides > DNA structure and packing**

    _Euchromatin_: loosely packed ([genes]({{< relref "gene" >}}) frequently used)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
