+++
title = "Describe the causes of pulmonary emboli and the clinical consequences associated with it."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### [Pulmonary embolism]({{< relref "pulmonary_embolism" >}}) {#pulmonary-embolism--pulmonary-embolism-dot-md}

-   Obstruction of a pulmonary artery by a blood clot (_embolus_)
    -   Common cause of morbidity and mortality
-   Forms when a blood clot, fat globule, or air bubble travels in blood to lungs after a compound fracture
    -   Embolus pases through right side of heart -> lung throug hpulmonary artery -> blocked pulmonary artery or branch
-   Immediate result -> partial or complete obstruction of blood flow to lung
    -   Results in a section or whole lung that is ventilated but not perfused with blood
-   Large embolus -> major decrease in oxygenation of blood -> patient suffers **acute respiratory distress**
    -   Volume of blood arriving from systemic circuit cannot be pushed through pulmonary circuit -> **right side of heart acutely dilated**
    -   Death may accur in a few minutes
-   Medium-size embolus -> block artery supplying bronchopulmonary segment -> pulmonary infarct
-   In physically active people, collateral circulation often exists and develops further when there is a PE -> less likely for infarction to occur
-   When an area of visceral pleura is also deprived of blood -> becomes inflamed (_pleuritic_) and irritates or becomes fused to sensitive parietal pleura -> pain
    -   Pain referred to cutaneous distribution of intercostal nerves to thoracic wall or, inc ase of inferior nerves,to anterio abdominal wall


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the causes of pulmonary emboli and the clinical consequences associated with it.]({{< relref "describe_the_causes_of_pulmonary_emboli_and_the_clinical_consequences_associated_with_it" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
