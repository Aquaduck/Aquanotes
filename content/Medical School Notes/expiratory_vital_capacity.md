+++
title = "Expiratory vital capacity"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Can be a way to measure [VC]({{< relref "vital_capacity" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Vital capacity**

    <!--list-separator-->

    -  [Expiratory vital capacity]({{< relref "expiratory_vital_capacity" >}}) (EVC)

        -   Maximum volume of air that can be **expired after maximal inspiration**


### Unlinked references {#unlinked-references}

[Show unlinked references]
