+++
title = "Anhydride"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kaplan Organic Chemistry Chapter 1]({{< relref "kaplan_organic_chemistry_chapter_1" >}}) {#kaplan-organic-chemistry-chapter-1--kaplan-organic-chemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 1.4 Carboxylic Acids and Derivatives**

    <!--list-separator-->

    -  [Anhydrides]({{< relref "anhydride" >}})

        -   Two carboxylic acids joined together via removal of water
        -   Many are cyclic
            -   May result from intramolecular dehydration of a dicarboxylic acid
        -   **If anhydride formed from only one type of carboxylic acid**: named by replacing _acid_ with _anhydride_ in the name of the corresponding carboxylic acid
        -   \*If anhydride not symmetrical, both carboxylic acids are named (without the _acid_) before _anhydride_ is added to the name
            <_20210714_205613screenshot.png>


### Unlinked references {#unlinked-references}

[Show unlinked references]
