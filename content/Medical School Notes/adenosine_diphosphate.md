+++
title = "Adenosine Diphosphate"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle > NAD<sup>+</sup>-linked isocitrate dehydrogenase**

    [ADP]({{< relref "adenosine_diphosphate" >}}) is the master energy sensor in the mitochondria

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
