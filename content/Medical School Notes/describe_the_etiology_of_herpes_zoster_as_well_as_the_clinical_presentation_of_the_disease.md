+++
title = "Describe the etiology of herpes zoster as well as the clinical presentation of the disease."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### [Herpes zoster]({{< relref "herpes_zoster" >}}) {#herpes-zoster--herpes-zoster-dot-md}

-   Caused by _Varicella zoster_ [virus]({{< relref "virus" >}})
-   Virus remains dormant in the _dorsal root gangla_ -> reactivates later in life as "shingles" within dermatome supplied by a particular DRG
-   After invading a ganglion, virus produces a sharp burning pain in [Dermatome]({{< relref "dermatome" >}})
    -   Affected skin area becomes red -> **stripe dermatome pattern**
    -   Vesicular eruptions appear
-   Pain can precede or follow skin eruptions
-   Weakness form motor involvement occurs in up to 5% of people
    -   Typically occurs in same myotomal distribution as dermatomal pain
-   Can present with sensory issues (e.g. burning, itching) and a blistering rash within that dermatome


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the etiology of herpes zoster as well as the clinical presentation of the disease.]({{< relref "describe_the_etiology_of_herpes_zoster_as_well_as_the_clinical_presentation_of_the_disease" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
