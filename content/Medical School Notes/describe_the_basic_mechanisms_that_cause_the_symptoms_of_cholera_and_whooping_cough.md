+++
title = "Describe the basic mechanisms that cause the symptoms of cholera and whooping cough."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Cholera Activates G<sub>s</sub> {#cholera-activates-g}

-   [Cholera]({{< relref "cholera" >}}) causes inappropriate, chronic activation of G<sub>s</sub>
    -   Modifies the Gα-subunit by ribosylation -> inhibition
        -   (don't need to know specifics, just that the subunit is chemically modified)
    -   Occurs in the epithelium of the gut


### Pertussis Inhibits G<sub>i</sub> {#pertussis-inhibits-g}

-   [Whooping cough]({{< relref "whooping_cough" >}}) is caused by Bordetella Pertussis
-   Modifies the Gα-subunit by ribosylation -> inhibition
-   Occurs in the lung


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the basic mechanisms that cause the symptoms of cholera and whooping cough.]({{< relref "describe_the_basic_mechanisms_that_cause_the_symptoms_of_cholera_and_whooping_cough" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
