+++
title = "Pyruvate"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Pyruvate kinase]({{< relref "pyruvate_kinase" >}}) {#pyruvate-kinase--pyruvate-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Produces [pyruvate]({{< relref "pyruvate" >}}) from [PEP]({{< relref "phosphoenolpyruvate" >}})

    ---


#### [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) {#pyruvate-carboxylase--pyruvate-carboxylase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Catalyzes ATP-dependent carboxylation of [pyruvate]({{< relref "pyruvate" >}}) to [oxaloacetate]({{< relref "oxaloacetate" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  [Pyruvate]({{< relref "pyruvate" >}}) metabolism

    <!--list-separator-->

    -  [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}})

        -   The mitochondrial PDH complex is allosterically regulated by product inhibition by acetyl-CoA & NADH against their respective enzymes, as well as by covalent modification

        <!--list-separator-->

        -  [Pyruvate DH kinase]({{< relref "pyruvate_dh_kinase" >}})

            -   Phosphorylates Ser residues on the first enzyme of the PDH complex (E1)
                -   Results in significant **down-regulation of its activity**
            -   Allosterically stimulated by the products of the reaction
            -   Allosterically inhibited by its substrates
            -   Also regulated by the energy charge <- kinase allosterically inhibited by ADP

        <!--list-separator-->

        -  [Pyruvate DH phosphatase]({{< relref "pyruvate_dh_phosphatase" >}})

            -   Activated by high concentrations of Ca<sup>2+</sup> and Mg<sup>2​+</sup> which **reverses the effects of PDH kinase** (i.e. dephosphorylates PDH)
                -   This **ensures glucose utilization**
            -   Activation by Ca<sup>2+</sup> primarily in skeletal muscle
                -   Contraction leads to Ca<sup>2+</sup> release from cellular stores
            -   Activating effect of Mg<sup>2+</sup> due to **low mitochondrial concentrations of ATP**

    <!--list-separator-->

    -  [Alanine aminotransferase]({{< relref "alanine_aminotransferase" >}})

        -   A cytosolic enzyme
        -   Catalyzes the readily reversible transfer of an amino group from alanine to α-ketoglutarate -> form pyruvate and glutamate
            -   Allows amino acids to be used as gluconeogenic precursors

    <!--list-separator-->

    -  NAD<sup>+</sup>-linked [lactate dehydrogenase]({{< relref "lactate_dehydrogenase" >}})

        -   A cytosolic enzyme
        -   Catalyzes a **freely reversible** redox reaction between _lactate_ and _pyruvate_
        -   **The only way in which lactate is utilized physiologically**
            -   Lactate released from anaerobic glyolysis occuring in RBCs + exercising muscle -> taken up by liver + used as a gluconeogenic substrate

    <!--list-separator-->

    -  [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}})

        -   [Biotin]({{< relref "biotin" >}})-requiring mitochondrial enzyme
        -   Catalyzes ATP-dependent carboxylation of pyruvate to oxaloacetate
            -   Then converted to PEP by PEP carboxylase for use in gluconeogenesis
        -   Allosterically activated by acetyl-CoA
            -   The ability of Acetyl-CoA to inhibit PDH complex and activate pyruvate carboxylase ensures that **the pyruvate generated in the liver mitochondria will be used as a gluconeogenic substrate**

<!--list-separator-->

-  **🔖 Glycolysis > Pyruvate kinase**

    Produces [pyruvate]({{< relref "pyruvate" >}}) from [phosphoenolpyruvate]({{< relref "phosphoenolpyruvate" >}}) (PEP)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
