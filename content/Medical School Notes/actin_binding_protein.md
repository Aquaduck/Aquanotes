+++
title = "Actin-binding protein"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Provide examples of how actin-binding proteins regulate actin filament function.]({{< relref "provide_examples_of_how_actin_binding_proteins_regulate_actin_filament_function" >}}) {#provide-examples-of-how-actin-binding-proteins-regulate-actin-filament-function-dot--provide-examples-of-how-actin-binding-proteins-regulate-actin-filament-function-dot-md}

<!--list-separator-->

-  **🔖 From Cytoskeleton Powerpoint**

    <!--list-separator-->

    -  [ABP]({{< relref "actin_binding_protein" >}}) functions

        1.  ABPs can **regulate actin polymerization**
            -   E.g. Arp 2/3 complex
        2.  ABPs can cross-link actin
        3.  ABPs can link actin to the membrane


### Unlinked references {#unlinked-references}

[Show unlinked references]
