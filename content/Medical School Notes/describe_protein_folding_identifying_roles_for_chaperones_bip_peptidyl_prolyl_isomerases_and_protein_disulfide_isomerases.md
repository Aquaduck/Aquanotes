+++
title = "Describe protein folding, identifying roles for chaperones, BiP, peptidyl prolyl isomerases and protein disulfide isomerases."
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Organelles and Trafficking I-II]({{< relref "organelles_and_trafficking_i_ii" >}}) {#from-organelles-and-trafficking-i-ii--organelles-and-trafficking-i-ii-dot-md}


### Folding {#folding}

-   [Chaperone proteins]({{< relref "chaperone_protein" >}}) bind to:
    -   Hydrophobic patches (e.g. BiP)
        -   [Binding immunoglobulin protein]({{< relref "binding_immunoglobulin_protein" >}}) - helps keep hydrophobic sections "buried" within protein
    -   Incompletely trimmed sugars
    -   Non disulfide-linked cystein residues
    -   On incompletely folded proteins -> **prevent aggregation & keep unfolded proteins in the ER until they achieve the right conformation**
-   [Peptidyl-prolyl isomerase]({{< relref "peptidyl_prolyl_isomerase" >}})
    -   Catalyzes **rotation** around the peptide bond N-terminal to **proline**
        -   Due to proline's ring, it creates **rigidity**


### Formation of disulfide bonds {#formation-of-disulfide-bonds}

-   [Protein disulfide isomerase]({{< relref "protein_disulfide_isomerase" >}}) (PDI)
    -   In the ER
    -   **Oxidoreductase** and **isomerase** properties
    -   Formation of disulfide bonds and fixes incorrect disulfide bond


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-01 Wed] </span></span> > Organelles & Trafficking I (+ II) > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe protein folding, identifying roles for chaperones, BiP, peptidyl prolyl isomerases and protein disulfide isomerases.]({{< relref "describe_protein_folding_identifying_roles_for_chaperones_bip_peptidyl_prolyl_isomerases_and_protein_disulfide_isomerases" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
