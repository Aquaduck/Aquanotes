+++
title = "Lactic acid"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept link {#concept-link}

-   Acid form of [Lactate]({{< relref "lactate" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Lactate]({{< relref "lactate" >}}) {#lactate--lactate-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Basic form of [lactic acid]({{< relref "lactic_acid" >}})

    ---


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Changes in Ventilation (p. 664)**

    When exercise becomes more vigorous -> increased [lactic acid]({{< relref "lactic_acid" >}}) production -> liberation of more [carbon dioxide]({{< relref "carbon_dioxide" >}}) (CO<sub>2</sub>) -> increase in ventilation

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
