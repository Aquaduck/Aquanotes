+++
title = "Osmosis - Introduction to Biostatistics"
author = ["Arif Ahsan"]
date = 2021-07-13T00:00:00-04:00
tags = ["medschool", "osmosis", "source"]
draft = false
+++

Link: <https://www.osmosis.org/notes/Introductory%5FBiostatistics#page-1>


## [Notes]({{< relref "biostatistics" >}}) {#notes--biostatistics-dot-md}


### Variance {#variance}

-   Sum of squared deviations from mean, divided by number of distributions
    -   \\(\sigma^{2} = \frac{\sum\_{}^{}(x - x)^{2}}{n}\\)


### Standard deviation (SD) {#standard-deviation--sd}

-   Square root of variance
    -   \\(\sigma = \sqrt{\frac{\sum\_{}^{}(x - x)^{2}}{n}}\\)


### Graphic Description of Data {#graphic-description-of-data}

{{< figure src="/ox-hugo/_20210714_123002screenshot.png" >}}


#### Normal (Gaussian) curve {#normal--gaussian--curve}

-   Symmetrical distribution of scores around the mean
    -   Forms classic bell shape
    -   Values lie within **2 SD** of mean
    -   Most natural phenomena show this type of distribution
    -   Parametric tests utilized in research


#### Non-Gaussian curve {#non-gaussian-curve}

-   _Asymmetrical_ distribution of scores around mean
    -   Skewed (negatively/positively) curve
    -   Kurtotic (flat/peaked) curve
    -   Nonparametric tests utilized in research


### Mean {#mean}

-   Calculated by \*adding each value in data set -> dividing by total number of data points
-   **Can be influenced by outliers**


### Median {#median}

-   Calculates central value when possible outliers present
-   Divides set of data into two haves


### Mode {#mode}

-   Central value appearing most often in data sequence
-   **Not affected by outliers**


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
