+++
title = "Ascending thin limb of the loop of Henle"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The second component of the [loop of Henle]({{< relref "loop_of_henle" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18)**

    <!--list-separator-->

    -  [Ascending thin limb of the loop of Henle]({{< relref "ascending_thin_limb_of_the_loop_of_henle" >}})

        -   Begins at the abrupt hairpin turn from the descending thin limb
        -   In long loops (deeply penetrated), epithelium of the first portion of the ascending limb remains thin **but has a different function than the descending limb**


### Unlinked references {#unlinked-references}

[Show unlinked references]
