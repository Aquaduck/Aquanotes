+++
title = "Perfusion"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [V/Q ratio]({{< relref "v_q_ratio" >}}) {#v-q-ratio--v-q-ratio-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Ventilation]({{< relref "ventilation" >}})-[perfusion]({{< relref "perfusion" >}}) ratio

    ---


#### [Physiology, Pulmonary Ventilation and Perfusion]({{< relref "physiology_pulmonary_ventilation_and_perfusion" >}}) {#physiology-pulmonary-ventilation-and-perfusion--physiology-pulmonary-ventilation-and-perfusion-dot-md}

<!--list-separator-->

-  **🔖 Physiology, Pulmonary Ventilation and Perfusion > Introduction**

    One of the major roles of the lungs is to facilitate gas exchange between the circulatory system and the external environment. The lungs are composed of branching airways that terminate in respiratory bronchioles and alveoli, which participate in gas exchange. Most bronchioles and large airways are part of the conducting zone of the lung, which delivers gas to sites of gas exchange in alveoli. Gas exchange occurs in the lungs between alveolar air and blood of the pulmonary capillaries. For effective gas exchange to occur, alveoli must be ventilated and perfused. [Ventilation]({{< relref "ventilation" >}}) (V) refers to the flow of air into and out of the alveoli, while [perfusion]({{< relref "perfusion" >}}) (Q) refers to the flow of blood to alveolar capillaries. Individual alveoli have variable degrees of ventilation and perfusion in different regions of the lungs. Collective changes in ventilation and perfusion in the lungs are measured clinically using the ratio of ventilation to perfusion (V/Q). Changes in the V/Q ratio can affect gas exchange and can contribute to hypoxemia.

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > V/Q ratio**

    The volumetric ratio of air that reaches the alveoli ([ventilation]({{< relref "ventilation" >}})) to alveolar blood supply ([perfusion]({{< relref "perfusion" >}})) per minute

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
