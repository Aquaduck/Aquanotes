+++
title = "Information Transfer 1: Telomere Replication & Maintenance"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## DNA Replication in Eukaryotes {#dna-replication-in-eukaryotes}

-   Human genome has 3 billion base pairs per haploid set of chromosomes
    -   6 billion replicated during [S phase]({{< relref "s_phase" >}}) of the [cell cycle]({{< relref "cell_cycle" >}})
-   **Multiple origins of replication on each eukaryotic chromosome**
-   Rate of replication ~ **100 nucleotides/s**
    -   Much **slower than prokaryotic replication**
-   **14 [DNA polymerases]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})**
    -   5 are known to have major roles during replication and have been well studied:
        -   [pol α]({{< relref "dna_polymerase_ɑ" >}}) (alpha)
        -   pol β (beta)
        -   pol γ (gamma)
        -   [pol ẟ]({{< relref "dna_polymerase_δ" >}}) (delta)
        -   [pol ε]({{< relref "dna_polymerase_ɛ" >}}) (epsilon)


### Steps in replication {#steps-in-replication}

The essential steps of replication are the same as in prokaryotes

1.  Before replication begins, DNA has to be made available as a template
    -   [DNA]({{< relref "dna" >}}) is bound to basic proteins known as [histones]({{< relref "histone" >}}) to form stuctures called [nucleosomes]({{< relref "nucleosome" >}})
    -   [Histones]({{< relref "histone" >}}) must be removed and then replaced during the replication process -> accounts for lower replication rate in [eukaryotes]({{< relref "eukaryote" >}})
    -   [Chromatin]({{< relref "chromatin" >}}) undergoes chemical modifications so that DNA can slide off histones or be accessible to the enzymes of the DNA replication machinery
    -   At the origin of replication, a pre-replication complex is made with other initiator proteins
        -   [Helicase]({{< relref "helicase" >}}) and other proteins are then recruited to start replication
2.  A [helicase]({{< relref "helicase" >}}) opens up the DNA helix using energy from ATP hydrolysis
    -   Replication forks are formed at each replication origin as the DNA unwinds
        -   This causes overwinding/supercoiling
            -   Resolved by the action of [topoisomerases]({{< relref "topoisomerase" >}})
3.  Primers are formed by the enzyme [primase]({{< relref "primase" >}})
    -   Using the primer, [DNA polymerase]({{< relref "dna_polymerase" >}}) can start synthesis
4.  DNA pol α adds a short (20-30 nucleotide) DNA fragment to the RNA primer on both strands, and then hands off to a second polymerase (DNA Pol-δ or DNA Pol-ε)
    -   [DNA Pol-δ]({{< relref "dna_polymerase_δ" >}})  synthesizes lagging strand
    -   [DNA Pol-ε]({{< relref "dna_polymerase_ɛ" >}})  synthesizes leading strand
5.  A sliding clamp protein known as [PCNA]({{< relref "proliferating_cell_nuclear_antigen" >}}) (proliferating cell nuclear antigen) holds the DNA polymerase in place so that it does not slide off DNA
6.  As pol δ runs into the primer RNA on the lagging strand, it displaces it from the DNA template
    -   Then removed by [RNase H]({{< relref "rnase_h" >}}) (aka flap endonuclease) and replaced with DNA nucleotides
    -   Gaps between okazaki fragments sealed by [DNA ligase]({{< relref "dna_ligase" >}}), which forms the phosphodiester bond


### Differences between Prokaryotic and Eukaryotic replication {#differences-between-prokaryotic-and-eukaryotic-replication}

| Property              | Prokaryotes        | Eukaryotes           |
|-----------------------|--------------------|----------------------|
| Origin of replication | Single             | Multiple             |
| Rate of replication   | 1000 nucleotides/s | 50-100 nucleotides/s |
| DNA polymerase types  | 5                  | 14                   |
| Telomerase            | Not present        | Present              |
| RNA primer removal    | DNA pol I          | RNase H              |
| Strand elongation     | DNA pol III        | Pol α, pol δ, pol ε  |
| Sliding clamp         | Sliding clamp      | PCNA                 |


### [Telomere]({{< relref "telomere" >}}) replication {#telomere--telomere-dot-md--replication}

-   Unlike prokaryotic chromosomes, eukaryotic chromosomes are linear
    -   When the replication fork reaches the end of the linear chromosome, there is **no way** to replace the primer on the 5' end of the lagging strand
        -   The DNA at the ends of the chromosome remain unpaired, and over time these get ends get progressively shorter as cells continue to divide
-   _Telomeres_ comprise repetitive sequences that code for no gene
    -   In humans, this is a six-base-pair sequence TTAGGG which is repeated 100 to 1000 times in the telomere regions
    -   This in effect **protects the genes from getting deleted as cells continue to divide**
-   [Telomerase]({{< relref "telomerase" >}}) is responsible for adding telomeres
    -   Contains two parts: a **catalytic part** and a **built-in RNA template**
        -   Attaches to end of a chromosome via DNA nucleotides complementary to the RNA template
            -   Corresponding nucleotides are added on the 3' end of the DNA strand
            -   Once sufficiently elongated, DNA pol adds the complementary nucleotides to the other strand -> chromosomes successfully replicated
    -   Telomerase is typically active in germ cells and adult stem cells, and **not** active in adult somatic cells


### [Telomerase]({{< relref "telomerase" >}}) and aging {#telomerase--telomerase-dot-md--and-aging}

-   Cells that undergo cell division continue to have their telomeres shortened because most somatic cels do not make telomerase
    -   Means that **telomere shortening is associated with aging**
-   In 2010, scientists found that telomerase cna reverse some age-related conditions in mice
    -   Telomerase-deficient mice had muscle atrophy, stem cell depletion, organ system failure, and impaired tissue injury responses
    -   Following reactivation of telomerase -> caused extension of telomeres, reduced DNA damage, reversed neurodegeneration, and improved function of testes, spleen, and intestines
-   [Cancer]({{< relref "cancer" >}}) is characterized by uncontrolled cell division of abnormal cells
    -   Cells accumulate mutations, proliferate uncontrollably, and can migrate to different parts of the body through a process called [metastatis]({{< relref "metastasis" >}})
    -   Scientists have observed that **cancerous cells have considerably shortened telomeres and that telomerase is active in these cells**
        -   Only **after telomeres were shortened** did telomerase become active
            -   If action of telomerase in these cells can be inhibited -> cancerous cells potentially stopped from further division


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
