+++
title = "Adenosine Triphosphatase"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS (Sickle Cell Case)]({{< relref "focs_sickle_cell_case" >}}) {#focs--sickle-cell-case----focs-sickle-cell-case-dot-md}

<!--list-separator-->

-  **🔖 Pathophysiology**

    Membrane **permeability to Ca<sup>2+</sup> increases** possibly due to \*impairment in the Ca<sup>2​+</sup> pump that depends on [ATPase]({{< relref "adenosine_triphosphatase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
