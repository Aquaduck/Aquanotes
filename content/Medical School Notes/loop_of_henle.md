+++
title = "Loop of Henle"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Component of the [Nephron]({{< relref "nephron" >}}) responsible for reabsorption of [water]({{< relref "water" >}}) and [sodium]({{< relref "sodium" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Descending thin limb of the loop of Henle]({{< relref "descending_thin_limb_of_the_loop_of_henle" >}}) {#descending-thin-limb-of-the-loop-of-henle--descending-thin-limb-of-the-loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    First component of the [loop of Henle]({{< relref "loop_of_henle" >}})

    ---


#### [Ascending thin limb of the loop of Henle]({{< relref "ascending_thin_limb_of_the_loop_of_henle" >}}) {#ascending-thin-limb-of-the-loop-of-henle--ascending-thin-limb-of-the-loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The second component of the [loop of Henle]({{< relref "loop_of_henle" >}})

    ---


#### [Ascending thick limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}}) {#ascending-thick-limb-of-the-loop-of-henle--ascending-thick-limb-of-the-loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Third component of the [loop of Henle]({{< relref "loop_of_henle" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
