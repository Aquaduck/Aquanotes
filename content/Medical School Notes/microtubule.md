+++
title = "Microtubule"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Tau protein]({{< relref "tau_protein" >}}) {#tau-protein--tau-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An insoluble [microtubule]({{< relref "microtubule" >}})-associated protein that maintains the stability of microtubules in [axons]({{< relref "axon" >}})

    ---


#### [Nexin]({{< relref "nexin" >}}) {#nexin--nexin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Connects adjacent doublet [microtubules]({{< relref "microtubule" >}}) to help maintain the shape of [cilium]({{< relref "cilium" >}})

    ---


#### [Microtubule associated protein]({{< relref "microtubule_associated_protein" >}}) {#microtubule-associated-protein--microtubule-associated-protein-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Glossary (p. G:20)**

    Any protein that binds to [microtubules]({{< relref "microtubule" >}}) and modifies their properties

    ---


#### [Describe the mechanism of action of the listed anti-neoplastics in the context of their specific actions and effect during cell division.]({{< relref "describe_the_mechanism_of_action_of_the_listed_anti_neoplastics_in_the_context_of_their_specific_actions_and_effect_during_cell_division" >}}) {#describe-the-mechanism-of-action-of-the-listed-anti-neoplastics-in-the-context-of-their-specific-actions-and-effect-during-cell-division-dot--describe-the-mechanism-of-action-of-the-listed-anti-neoplastics-in-the-context-of-their-specific-actions-and-effect-during-cell-division-dot-md}

<!--list-separator-->

-  **🔖 From Anti-neoplastics powerpoint > By Mechanism of Action**

    |                                                        |                                           |                         |                           |
    |--------------------------------------------------------|-------------------------------------------|-------------------------|---------------------------|
    | [Microtubule]({{< relref "microtubule" >}}) inhibitors | [Paclitaxel]({{< relref "paclitaxel" >}}) | Inhibits MT disassembly | M + Peripheral neuropathy |

    ---

<!--list-separator-->

-  **🔖 From Anti-neoplastics powerpoint > By Mechanism of Action**

    |                                                        |                                             |                       |                       |
    |--------------------------------------------------------|---------------------------------------------|-----------------------|-----------------------|
    | [Microtubule]({{< relref "microtubule" >}}) inhibitors | [Vincristine]({{< relref "vincristine" >}}) | Inhibits MT formation | Peripheral neuropathy |

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
