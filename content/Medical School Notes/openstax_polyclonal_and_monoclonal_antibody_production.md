+++
title = "Openstax - Polyclonal and Monoclonal Antibody Production"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "papers", "openstax", "source"]
draft = false
+++

Link: <https://openstax.org/books/microbiology/pages/20-1-polyclonal-and-monoclonal-antibody-production#27566>

<div class="table-caption">
  <span class="table-number">Table 1</span>:
  Characteristics of Monoclonal and Polyclonal Antibodies
</div>

| Monoclonal Antibodies                                           | Polyclonal Antibodies                        |
|-----------------------------------------------------------------|----------------------------------------------|
| Expensive production                                            | Inexpensive production                       |
| Long production time                                            | Rapid production                             |
| Large quantities of _specific_ antibodies                       | Large quantities of _nonspecific_ antibodies |
| Recognizes _a single epitope_ on an antigen                     | Recognizes _multiple epitopes_ on an antigen |
| Production is continuous and uniform once the hybridoma is made | Different batches vary in composition        |

{{< figure src="/ox-hugo/_20210715_200156screenshot.png" caption="Figure 1: Interaction between antibodies and an antigen" width="400" >}}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 General Biochemistry and Techniques > 4. Distinguish between monoclonal and polyclonal antibodies**

    [Openstax - Polyclonal and Monoclonal Antibody Production]({{< relref "openstax_polyclonal_and_monoclonal_antibody_production" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
