+++
title = "Myosin"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Thick myofilament]({{< relref "thick_myofilament" >}}) {#thick-myofilament--thick-myofilament-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Made of [myosin]({{< relref "myosin" >}})

    ---


#### [Pericyte]({{< relref "pericyte" >}}) {#pericyte--pericyte-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology**

    Contain [actin]({{< relref "actin" >}}), [myosin]({{< relref "myosin" >}}), and [tropomyosin]({{< relref "tropomyosin" >}}) -> suggests they play a role in contraction

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Overview - Muscle**

    Muscle cells possess _contractile filaments_ whose major components are [actin]({{< relref "actin" >}}) and [myosin]({{< relref "myosin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
