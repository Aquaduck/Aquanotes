+++
title = "Name the types of cellular and acellular components of connective tissue"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Introduction to Connective Tissue (CT) Pre-workshop Reading]({{< relref "introduction_to_connective_tissue_ct_pre_workshop_reading" >}}) {#from-introduction-to-connective-tissue--ct--pre-workshop-reading--introduction-to-connective-tissue-ct-pre-workshop-reading-dot-md}


### Four major components that make up [connective tissue]({{< relref "connective_tissue" >}}) {#four-major-components-that-make-up-connective-tissue--connective-tissue-dot-md}


#### Fibrous structural proteins {#fibrous-structural-proteins}


#### [Ground substance]({{< relref "ground_substance" >}}) {#ground-substance--ground-substance-dot-md}


#### Adhesion proteins {#adhesion-proteins}


#### Cells {#cells}


### Fibrous structural components {#fibrous-structural-components}

-   All three of these stain red in H&E, but can be distinguished in special stains


#### [Collagen]({{< relref "collagen" >}}) {#collagen--collagen-dot-md}

-   The most abundant protein in the body
-   28 types labeled (Type I, Type II, etc.)
-   90% of collagen is type I


#### Elastin fibers {#elastin-fibers}

-   Stretchy fibers responsible for returning pinched skin to its normal shape


#### Reticulin fibers {#reticulin-fibers}

-   Fine collagen fibers (type III) form a very delicate scaffold
-   Usually only a few reticulin fibers are present in most CT
    -   Despite this, they can sometimes be the dominant fiber types


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-07 Tue] </span></span> > Histology of Extracellular Matrix and Connective Tissue/Cartilage > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Name the types of cellular and acellular components of connective tissue]({{< relref "name_the_types_of_cellular_and_acellular_components_of_connective_tissue" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
