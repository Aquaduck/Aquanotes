+++
title = "Blood"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 19 linked references {#19-linked-references}


#### [West's zones of the lung]({{< relref "west_s_zones_of_the_lung" >}}) {#west-s-zones-of-the-lung--west-s-zones-of-the-lung-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Relative distributions of [blood]({{< relref "blood" >}}) flow in the [lung]({{< relref "lung" >}}) during the [cardiac cycle]({{< relref "cardiac_cycle" >}})

    ---


#### [Right-to-left shunt]({{< relref "right_to_left_shunt" >}}) {#right-to-left-shunt--right-to-left-shunt-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [shunts]({{< relref "shunt" >}}) where [blood]({{< relref "blood" >}}) fails to be oxygenated -> deoxygenated blood enters the [systemic circuit]({{< relref "systemic_circuit" >}})

    ---


#### [Renal corpuscle]({{< relref "renal_corpuscle" >}}) {#renal-corpuscle--renal-corpuscle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Filter fluid from the [Blood]({{< relref "blood" >}})

    ---


#### [Plasma]({{< relref "plasma" >}}) {#plasma--plasma-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The non-cellular component of [Blood]({{< relref "blood" >}})

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Respiratory Acidosis: Primary Carbonic Acid/Carbon Dioxide Excess**

    Occurs when the [blood]({{< relref "blood" >}}) is overly acidic due to an excess of [carbonic acid]({{< relref "carbonic_acid" >}}) <- excess [carbon dioxide]({{< relref "carbon_dioxide" >}}) in the blood

    ---

<!--list-separator-->

-  **🔖 Metabolic Alkalosis: Primary Bicarbonate Excess (p. 1281)**

    [Blood]({{< relref "blood" >}}) is too alkaline (pH above 7.45) due to **too much [bicarbonate]({{< relref "bicarbonate" >}})** (called _primary bicarbonate excess_)

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Chylomicrons > From Fat Metabolism in Muscle & Adipose Tissue**

    The **carriers of [dietary lipids]({{< relref "dietary_lipid" >}}) in the [blood]({{< relref "blood" >}})**

    ---


#### [Left ventricle]({{< relref "left_ventricle" >}}) {#left-ventricle--left-ventricle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Ejects oxygenated [blood]({{< relref "blood" >}}) to the [aorta]({{< relref "aorta" >}}) through the [aortic valve]({{< relref "aortic_valve" >}})

    ---


#### [Ischemia]({{< relref "ischemia" >}}) {#ischemia--ischemia-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Decreased [Blood]({{< relref "blood" >}}) supply that cannot meet [Oxygen]({{< relref "oxygen" >}}) demands of an organ or tissue

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Stroke volume]({{< relref "stroke_volume" >}}) = volume of [blood]({{< relref "blood" >}}) ejected from [left ventricle]({{< relref "left_ventricle" >}}) per heartbeat

    ---

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Percentage of [blood]({{< relref "blood" >}}) supply:

    ---


#### [Hypoxemia]({{< relref "hypoxemia" >}}) {#hypoxemia--hypoxemia-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Decreased [oxygen]({{< relref "oxygen" >}}) content in arterial [blood]({{< relref "blood" >}}) (↓ PaO<sub>2</sub>)

    ---


#### [Hemorrhage]({{< relref "hemorrhage" >}}) {#hemorrhage--hemorrhage-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    When [blood]({{< relref "blood" >}}) leaves the [vascular system]({{< relref "vascular_system" >}})

    ---


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Aldosterone**

    Increases the reabsorption of [sodium]({{< relref "sodium" >}}) into the [blood]({{< relref "blood" >}}) by the [kidneys]({{< relref "kidney" >}}) -> **increases reabsorption of water** -> increases blood volume -> raises blood pressure

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21)**

    _[Filtration]({{< relref "filtration" >}})_: the process by which [Water]({{< relref "water" >}}) and solutes in the [Blood]({{< relref "blood" >}}) leave the vascular system through the [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) and enter [Bowman's space]({{< relref "bowman_s_space" >}})

    ---


#### [Cardiac cycle]({{< relref "cardiac_cycle" >}}) {#cardiac-cycle--cardiac-cycle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The cycle of the [Heart]({{< relref "heart" >}}) to pump [blood]({{< relref "blood" >}}) through the [Cardiovascular system]({{< relref "cardiovascular_system" >}})

    ---


#### [Blood clotting]({{< relref "blood_clotting" >}}) {#blood-clotting--blood-clotting-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The process of clotting [blood]({{< relref "blood" >}}) to close wounds and prevent excessive bleeding

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Atherosclerosis**

    Formation of [lipid]({{< relref "lipid" >}}), [cholesterol]({{< relref "cholesterol" >}}), and/or [calcium]({{< relref "calcium" >}})-laden plaques within [tunica intima]({{< relref "tunica_intima" >}}) of arterial wall -> restricts [blood]({{< relref "blood" >}}) flow

    ---

<!--list-separator-->

-  **🔖 Cardiology > Infarction**

    Tissue necrosis that results from insufficient [Blood]({{< relref "blood" >}}) nad [Oxygen]({{< relref "oxygen" >}}) supply to affected region

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
