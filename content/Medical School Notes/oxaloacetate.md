+++
title = "Oxaloacetate"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) {#pyruvate-carboxylase--pyruvate-carboxylase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Catalyzes ATP-dependent carboxylation of [pyruvate]({{< relref "pyruvate" >}}) to [oxaloacetate]({{< relref "oxaloacetate" >}})

    ---


#### [List the four enzymes, and their intracellular location, for which pyruvate serves as a substrate, and the product that is made]({{< relref "list_the_four_enzymes_and_their_intracellular_location_for_which_pyruvate_serves_as_a_substrate_and_the_product_that_is_made" >}}) {#list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made--list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made-dot-md}

<!--list-separator-->

-  **🔖 Enzymes > Mitochondria**

    Makes [oxaloacetate]({{< relref "oxaloacetate" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis > PEP carboxykinase**

    **Converts [OAA]({{< relref "oxaloacetate" >}}) into [PEP]({{< relref "phosphoenolpyruvate" >}})**

    ---

<!--list-separator-->

-  **🔖 Citric Acid Cycle > Citrate synthase**

    Noncompetitive with [oxaloacetate]({{< relref "oxaloacetate" >}})

    ---

<!--list-separator-->

-  **🔖 Citric Acid Cycle > Citrate synthase**

    [Citrate]({{< relref "citrate" >}}) itself inhibits the enzyme by competing with [oxaloacetate]({{< relref "oxaloacetate" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
