+++
title = "Khan - RNA and Protein Synthesis Review"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "khan", "source"]
draft = false
+++

## Structure of [RNA]({{< relref "rna" >}}) {#structure-of-rna--rna-dot-md}


### 3 main differences between DNA and RNA: {#3-main-differences-between-dna-and-rna}

1.  RNA uses _ribose_ instead of _deoxyribose_
2.  RNA is usually _single-stranded_
3.  RNA contains _uracil_ in place of _thymine_

{{< figure src="/ox-hugo/_20210721_165142screenshot.png" >}}


### Types of RNA {#types-of-rna}

| Type                 | Role                                                                                     |
|----------------------|------------------------------------------------------------------------------------------|
| Messenger RNA (mRNA) | Carries information from DNA in the nucleus to ribosomes in the cytoplasm                |
| Ribosomal RNA (rRNA) | Structural component of [ribosomes]({{< relref "ribosome" >}})                           |
| Transfer RNA (tRNA)  | Carries amino acids to the ribosome during tranlsation to help build an amino acid chain |


## Common mistakes and misconceptions {#common-mistakes-and-misconceptions}

-   **Amino acids are not made during protein synthesis**
-   **Mutations do not always have drastic or negative effects**
-   **Insertions and deletions that are multiples of three nucleotides will not cuase frameshift mutations**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 4. Compare and contrast the different types of RNA**

    [Types of RNA]({{< relref "khan_rna_and_protein_synthesis_review" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
