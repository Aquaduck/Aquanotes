+++
title = "Beta globulin"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   One of the three subgroups of [globulins]({{< relref "globulin" >}})
-   Transport [iron]({{< relref "iron" >}}), [lipids]({{< relref "lipid" >}}), and the fat-soluble vitamins [A]({{< relref "vitamin_a" >}}), [D]({{< relref "vitamin_d" >}}), [E]({{< relref "vitamin_e" >}}), and [K]({{< relref "vitamin_k" >}}) to cells
-   Similar in function to [Alpha globulin]({{< relref "alpha_globulin" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Plasma Proteins (p. 796) > Globulin**

    [Alpha]({{< relref "alpha_globulin" >}}) and [beta]({{< relref "beta_globulin" >}}) globulins transport iron, lipids, and fat-soluble vitamins A, D, E, and K to the cells

    ---


#### [Alpha globulin]({{< relref "alpha_globulin" >}}) {#alpha-globulin--alpha-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Similar in function to [Beta globulin]({{< relref "beta_globulin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
