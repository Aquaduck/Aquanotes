+++
title = "Lineweaver-Burk Plot"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

{{< figure src="/ox-hugo/_20210721_192657screenshot.png" >}}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [The Equations of Enzyme Kinetics]({{< relref "the_equations_of_enzyme_kinetics" >}}) {#the-equations-of-enzyme-kinetics--the-equations-of-enzyme-kinetics-dot-md}

<!--list-separator-->

-  [Lineweaver-Burk plot]({{< relref "lineweaver_burk_plot" >}})

    -   Inverse of reaction rate \\(\frac{1}{r}\\) plotted against inverse of substrate concentration \\(\frac{1}{[S]}\\)
        -   Basically reciprocal of Michaelis-Menten
    -   Provides a useful graphical method for analysis of the [Michaelis-Menten]({{< relref "michaelis_menten_kinetics" >}}) equation

    {{< figure src="/ox-hugo/_20210721_192913screenshot.png" caption="Figure 1: Lineweaver-Burk plot of Michaelis-Menten kinetics" >}}

    <!--list-separator-->

    -  Example problem

        {{< figure src="/ox-hugo/_20210721_193056screenshot.png" >}}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 12. Interpret Michaelis-Menten and Lineweaver-Burk plots**

    [Lineweaver-Burk Plot]({{< relref "lineweaver_burk_plot" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
