+++
title = "G2/M transition checkpoint"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A checkpoint in the [cell cycle]({{< relref "cell_cycle" >}}) that triggers early [mitosis]({{< relref "mitosis" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Mitosis (p. 978)**

    An abrupt increase in [M-Cdk]({{< relref "m_cdk" >}}) activity at the [G2/M transition checkpoint]({{< relref "g2_m_transition_checkpoint" >}}) triggers the events of early [mitosis]({{< relref "mitosis" >}})

    ---


#### [M-Cdk]({{< relref "m_cdk" >}}) {#m-cdk--m-cdk-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Increase of M-Cdk at the [G2/M transition checkpoint]({{< relref "g2_m_transition_checkpoint" >}}) triggers mitosis

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
