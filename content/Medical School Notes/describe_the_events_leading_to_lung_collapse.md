+++
title = "Describe the events leading to lung collapse."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### Lung collapse {#lung-collapse}

-   If a penetrating wound opens through the thoracic wall or surface of lungs -> air sucked into pleural cavity because of negative pressure
-   Surface tension adhering visceral to parietal pleura broken -> lung collapse -> expellation of most of its air because of elastic recoil
    -   Pleural cavity (normally a potential space) becomes a **real space**
-   Pulmonary cavity **doesn't increase in size during inspiration**
    -   Evident radiographically on affected side by:
        1.  Elevation of diaphragm above usual level
        2.  Intercostal space narrowing
        3.  Displacement of mediastinum towards affected side


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the events leading to lung collapse.]({{< relref "describe_the_events_leading_to_lung_collapse" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
