+++
title = "Amoeba Sisters - Protein Structure and Folding"
author = ["Arif Ahsan"]
date = 2021-07-16T00:00:00-04:00
tags = ["medschool", "videos"]
draft = false
+++

Link: <https://www.youtube.com/watch?v=hok2hyED9go&t=275s>


## Overview {#overview}


## Primary Structure {#primary-structure}

-   Sequence of [AAs]({{< relref "amino_acid" >}}) that make up a protein held together by peptide bonds
    -   Formed into _polypeptides_


## Secondary Structure {#secondary-structure}

-   Folding occurring due to **hydrogen bonds**
-   alpha-helix or beta-pleated sheet
    -   Dependent on hydrogen bonds


## Tertiary Structure {#tertiary-structure}

-   Folding occurring due to interactions between **R-groups**
    -   Hydrophilic R groups on outside and Hydrophobic R groups on inside
-   Many types of bonds can influence tertiary structure


## Quaternary {#quaternary}

-   Interactions between **different polypeptides**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 4. Be able to describe the four levels of protein structure (primary, secondary, tertiary, quaternary) including:**

    [Amoeba Sisters - Protein Structure and Folding]({{< relref "amoeba_sisters_protein_structure_and_folding" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
