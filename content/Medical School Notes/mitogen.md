+++
title = "Mitogen"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  [Mitogens]({{< relref "mitogen" >}}) Stimulate G<sub>1</sub>-Cdk and G1/S-Cdk activities (p. 1012)

        -   For the vast majority of animal cells, mitogens control the rate of cell division by **acting in the G<sub>1</sub> phase of the cell cycle**
            -   Mitogens allow Cdk activity to occur in G<sub>1</sub> -> entry into a new cell cycle

        <!--list-separator-->

        -  Mitogens interact with cell-surface receptors to trigger multiple intracellular signaling pathways

            <!--list-separator-->

            -  [Ras]({{< relref "ras" >}})

                -   GTPase activity activates MAP kinase cascade -> increased production of transcription regulatory proteins such as [Myc]({{< relref "myc" >}})

            <!--list-separator-->

            -  [Myc]({{< relref "myc" >}})

                -   Myc is thought to promote cell-cycle entry by several mechanisms
                    -   Increase expression of genes encoding G<sub>1</sub> cyclins ([D cyclins]({{< relref "d_cyclin" >}})) -> increase G<sub>1</sub>-Cdk (cyclin D-Cdk4) activity
                    -   Myc also stimulates the transcription of genes that increase cell growth

            <!--list-separator-->

            -  [E2F]({{< relref "e2f" >}}) proteins

                -   Activation of E2F is a key function of G<sub>1</sub>-Cdk complexes
                -   E2F proteins bind to specific DNA sequences in promoters of genes that encode proteins required for S-phase entry
                    -   G<sub>1</sub>/S-cyclins
                    -   S-cyclins
                    -   Proteins involved in DNA synthesis + chromosome duplication
                -   In the absence of mitogenic stimulation, E2F-dependent gene expression is inhibited by members of the [Retinoblastoma protein family]({{< relref "retinoblastoma_protein_family" >}})

            <!--list-separator-->

            -  [Rb family]({{< relref "retinoblastoma_protein_family" >}})

                -   When cells are stimulated to divide by mitogens -> active G<sub>1</sub>/Cdk accumulates -> **phosphorylates** Rb -> reduces Rb binding to E2F
                    -   Allows E2F to activate expression of target genes
                -   Loss of both copies of the Rb gene -> excessive proliferation of some cells in the developing retina
                    -   Suggests that **Rb is particularly important for restraining cell division in this tissue**

            <!--list-separator-->

            -  Overview

                {{< figure src="/ox-hugo/_20210928_161037screenshot.png" caption="Figure 1: Mitogen stimulated cell cycle entry" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
