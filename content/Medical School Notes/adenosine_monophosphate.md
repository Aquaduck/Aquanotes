+++
title = "Adenosine Monophosphate"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   AMP is the master switch of the [cytoplasm]({{< relref "cytoplasm" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    An amino group is removed from [AMP]({{< relref "adenosine_monophosphate" >}}) to produce [IMP]({{< relref "inosine_monophosphate" >}}) by [AMP deaminase]({{< relref "amp_deaminase" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle > NAD<sup>+</sup>-linked isocitrate dehydrogenase**

    [AMP]({{< relref "adenosine_monophosphate" >}}) is master switch in the [cytoplasm]({{< relref "cytoplasm" >}})

    ---


#### [6-Phosphofructo-1-kinase]({{< relref "6_phosphofructo_1_kinase" >}}) {#6-phosphofructo-1-kinase--6-phosphofructo-1-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Stimulated by [[AMP]({{< relref "adenosine_monophosphate" >}})]

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
