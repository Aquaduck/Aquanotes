+++
title = "Discuss what happens to balance when input ALONE is increased or decreased and provide an example."
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Homeostasis PPT]({{< relref "homeostasis_ppt" >}}) {#from-homeostasis-ppt--homeostasis-ppt-dot-md}


### Flow Equation {#flow-equation}

-   Flow = \\( \frac{(P\_{1} - P\_{2})}{R} \\) where:
    -   Pressure gradient = \\( P\_{1} - P\_{2}\\)
    -   Resistance = \\( R \\)


### What happens when input is changed {#what-happens-when-input-is-changed}

-   The pool **does not keep filling**
-   The pressure gradient will keep increasing/decreasing until a new steady state (deeper pool) is reached (i.e. input and output are equal again)
    -   This can lead to the "wrong" steady state


### What happens when output is changed {#what-happens-when-output-is-changed}

-   Same thing as above


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon] </span></span> > Homeostasis > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Discuss what happens to balance when input ALONE is increased or decreased and provide an example.]({{< relref "discuss_what_happens_to_balance_when_input_alone_is_increased_or_decreased_and_provide_an_example" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
