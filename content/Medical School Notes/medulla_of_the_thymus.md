+++
title = "Medulla of the thymus"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Multilobate medulla of the [thymus]({{< relref "thymus" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}}) {#describe-the-structure-of-the-thymus--include-changes-with-age-dot----describe-the-structure-of-the-thymus-include-changes-with-age-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus**

    <!--list-separator-->

    -  [Medulla of the thymus]({{< relref "medulla_of_the_thymus" >}})

        {{< figure src="/ox-hugo/_20211009_165743screenshot.png" caption="Figure 1: Medulla of the thymus (and a bit of cortex and capsule in the upper right)" width="700" >}}

        -   Epithelial cells are more prominant than in the cortex
            -   Cause the pallor
        -   Function of the [epithelial cells]({{< relref "epithelium_of_the_thymus" >}}):
            -   Test the new [T-cells]({{< relref "t_lymphocyte" >}}) for self-reactivity and HLA molecule recognition
                -   Eliminate those with innapropriate reactivity
            -   Help [T-lymphocytes]({{< relref "t_lymphocyte" >}}) develop into helper or suppressor cells
        -   Some medullary epithelial cells form [Hassall's corpuscles]({{< relref "hassall_s_corpuscle" >}})

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Thymus epithelium**

    In the [Medulla of the thymus]({{< relref "medulla_of_the_thymus" >}}) epithelial cells direct elimination of autoreactive lymphocytes and help [T-cell]({{< relref "t_lymphocyte" >}}) differentiation into helper or suppressor cells

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus**

    The thymus has a central, pale, [multilobate medulla]({{< relref "medulla_of_the_thymus" >}}) covered by a dark [cortex]({{< relref "cortex_of_the_thymus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
