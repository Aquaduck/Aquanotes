+++
title = "Amyloid-beta 4A precursor protein"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Generates amyloid beta, a component of the amyloid plaques found in brains of [Alzheimer's disease]({{< relref "alzheimer_s_disease" >}}) patients


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Caspase-3]({{< relref "caspase_3" >}}) {#caspase-3--caspase-3-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The predominant caspase involved in the cleavage of [amyloid-beta 4A precursor protein]({{< relref "amyloid_beta_4a_precursor_protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
