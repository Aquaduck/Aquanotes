+++
title = "Genetics"
author = ["Arif Ahsan"]
date = 2021-07-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

Genetics is the science of inheritance


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}}) {#osmosis-transcription-translation-and-replication--osmosis-transcription-translation-and-replication-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication > DNA Cloning**

    Uses _plasmids_: [genetic]({{< relref "genetics" >}}) structures outside of chromosomes that are replicated independently

    ---


#### [Osmosis - Population genetics]({{< relref "osmosis_population_genetics" >}}) {#osmosis-population-genetics--osmosis-population-genetics-dot-md}

<!--list-separator-->

-  Notes - Population [Genetics]({{< relref "genetics" >}})

    <!--list-separator-->

    -  Mendelian genetics & punnett squares

        -   _Genetics_: science of inheritance
        -   Parental generation ("P") -> 1st filial generation ("F1") -> 2nd filial generation ("F2")
        -   _Homozygous_: male, female alleles are same
        -   _Heterozygous_: male, female alleles differ
        -   _Phenotype_: observable trait from genotype

        <!--list-separator-->

        -  Mendel's laws

            -   _Law of segregation_: alleles segregate -> offspring acquire one allele from each parent
            -   _Law of dominance_: alleles can be dominant/recessive
            -   _Law of independent assortment_: separate genes assort independently
                -   _Genetic linkage_: proximity of genes on chromosome can cause joint assortment

        <!--list-separator-->

        -  Punnett square

            -   _Punnett square_: table showing possible combinations of genotypes

            {{< figure src="/ox-hugo/_20210719_192620screenshot.png" width="500" >}}

            {{< figure src="/ox-hugo/_20210719_192643screenshot.png" width="500" >}}

    <!--list-separator-->

    -  Inheritance patterns

        <!--list-separator-->

        -  Dominant vs. recessive

            -   _Dominant inheritance_: mutation affects dominant allele -> one copy causes disease
            -   _Recessive inheritance_: mutation affects recessive allele -> two copies cause disease

        <!--list-separator-->

        -  Autosomal vs. sexual vs. mitochondrial patterns

            -   _Autosomal inheritance_: mutation affects somatic chromosome
            -   _Sexual inheritance_: mutation affects sex chromosome; X-linked/Y-linked
            -   _Mitochondrial inheritance_: mutation on egg's mitochondrial DNA

            <!--list-separator-->

            -  Autosomal inheritance

                {{< figure src="/ox-hugo/_20210719_193703screenshot.png" caption="Figure 1: Autosomal dominant inheritance when crossed with heterozygous and homozygous recessive" >}}

                {{< figure src="/ox-hugo/_20210719_193722screenshot.png" caption="Figure 2: Autosomal recessive inheritance. Punnett square demonstrating probabilities of healhty, disease, and carrier genotypes in the offspring when two healthy carriers reproduce" >}}

                {{< figure src="/ox-hugo/_20210719_193740screenshot.png" caption="Figure 3: Autosomal recessive inheritance. When one affected and one unaffected individual reproduce, all offspring are carriers" >}}

            <!--list-separator-->

            -  Sex-linked inheritance

                -   Males have one allele for genes on X, Y chromosomes -> _hemizygous_
                -   Females have two alleles for genes on X chromosomes (homoxygous/heterozygous)

                <!--list-separator-->

                -  X-Linked dominant inheritance

                    e.g. fragile X syndrome

                    -   Dominant hemizygotes, dominant homozygotes, and heterozygotes have disease
                    -   **Males** reproducing w/ healthy females have **100% chance to pass onto female children and 0% chance to pass onto male children**
                    -   **Females** reproducing w/ healthy males have **50% chance to pass onto children of both sexes**


#### [Gene]({{< relref "gene" >}}) {#gene--gene-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A unit of [genetic]({{< relref "genetics" >}}) material

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
