+++
title = "Hypothalamus"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Osmoreceptor]({{< relref "osmoreceptor" >}}) {#osmoreceptor--osmoreceptor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Specialized cells within the [hypothalamus]({{< relref "hypothalamus" >}}) that are particularly sensitive to the concentration of sodium ions and other solutes

    ---


#### [Antidiuretic hormone]({{< relref "antidiuretic_hormone" >}}) {#antidiuretic-hormone--antidiuretic-hormone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Secreted by the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) in response to [osmoreceptors]({{< relref "osmoreceptor" >}}) in the [Hypothalamus]({{< relref "hypothalamus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
