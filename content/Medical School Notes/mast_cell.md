+++
title = "Mast cell"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Name one drug in which severe adverse effects may depend on the presence of a specific genetic variation]({{< relref "name_one_drug_in_which_severe_adverse_effects_may_depend_on_the_presence_of_a_specific_genetic_variation" >}}) {#name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation--name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation-dot-md}

<!--list-separator-->

-  **🔖 From Pharmacogenetics Pre-learning Material > Drug allergy**

    Immune hypersensitivity reactions mediated by [immunoglobulin E]({{< relref "immunoglobulin_e" >}}) and driven by [mast cells]({{< relref "mast_cell" >}})

    ---


#### [Mast Cell: A Multi-Functional Master Cell]({{< relref "mast_cell_a_multi_functional_master_cell" >}}) {#mast-cell-a-multi-functional-master-cell--mast-cell-a-multi-functional-master-cell-dot-md}

<!--list-separator-->

-  [Mechanism of Activation]({{< relref "mast_cell" >}})

    -   Mast cells are known for their main mechanism of action: [IgE]({{< relref "immunoglobulin_e" >}})-mediated allergic reactions through the FcɛRI receptor

<!--list-separator-->

-  [Introduction]({{< relref "mast_cell" >}})

    -   Mast cells originated from pluripotent progenitor cells of the bone marrow
        -   Mature under the influence of the c-kit ligand and stem cell factor in the presence of other distinct growth factors in the tissue they are destined to reside in
    -   Under normal conditions, mast cells **do not circulate in the blood stream**
        -   Mast cell progenitors migrate into tissues and differentiate into mast cells under the influence of stem cell fator and various cytokines

<!--list-separator-->

-  **🔖 Overview**

    <!--list-separator-->

    -  Pathophysiological implications of [mast cells]({{< relref "mast_cell" >}})

        -   Diseases such as:
            -   Asthma
            -   Allergies
            -   Anaphylaxis
            -   Gastrointestinal disorders
            -   Many types of malignancies
            -   Cardiovascular disease

<!--list-separator-->

-  **🔖 Overview**

    <!--list-separator-->

    -  Normal physiological functions of [mast cells]({{< relref "mast_cell" >}})

        -   Regulate:
            -   Vasodilation
            -   Vascular homeostasis
            -   Innate and adaptive immune responses
            -   Angiogenesis
            -   Venom detoxification

<!--list-separator-->

-  **🔖 Overview**

    [Mast cells]({{< relref "mast_cell" >}}) are immune cells of the myeloid lineage and are present in connective tissues throughout the body

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
