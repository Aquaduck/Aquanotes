+++
title = "HIF-1: upstream and downstream of cancer metabolism"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## [HIF-1]({{< relref "hypoxia_inducible_factor_1" >}}) target genes involved in glucose and energy metabolism {#hif-1--hypoxia-inducible-factor-1-dot-md--target-genes-involved-in-glucose-and-energy-metabolism}

-   HIF-1 activates the transcription of _SLC2A1_ and _SLC2A3_ -> encode [GLUT-1]({{< relref "glut_1" >}}) and [GLUT-3]({{< relref "glut_3" >}}) respectively (**upregulation of glucose transporters**)
-   HIF-1 activates the transcription of _HK1_ and _HK2_ -> encode [Hexokinase]({{< relref "hexokinase" >}}) (**upregulation of glycolytic enzymes**)
-   HIF-1 activates transcription of the gene encoding the [BH3-only protein]({{< relref "bh3_only_protein" >}}) BNIP3 -> induces selective mitochondrial autophagy by competing with [Beclin 1]({{< relref "beclin_1" >}}) for binding to [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) -> allows Beclin 1 to trigger autophagy
-   HIF-1 appears to play a crucial homeostatic role in **managing [oxygen]({{< relref "oxygen" >}}) consumption** to balance [ATP]({{< relref "atp" >}}) and [ROS]({{< relref "reactive_oxygen_species" >}}) production
    -   HIF-1 orchestrates a subunit switch in [cytochrome c oxidase]({{< relref "cytochrome_c_oxidase" >}}) -> optimizes efficiency of respiration in response to cellular O<sub>2</sub> changes


## Genetic and metabolic activators of [HIF-1]({{< relref "hypoxia_inducible_factor_1" >}}) {#genetic-and-metabolic-activators-of-hif-1--hypoxia-inducible-factor-1-dot-md}


### [mTOR]({{< relref "mechanistic_target_of_rapamycin" >}}) {#mtor--mechanistic-target-of-rapamycin-dot-md}

-   mTOR is a serine-threonine protein kinase that **increases the rate of translation of HIF-1α**
    -   Phosphorylates ribosomal protein S6 kinase and eIF-4E binding protein 1


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
