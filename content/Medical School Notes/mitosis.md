+++
title = "Mitosis"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  Dephosphorylation Activates [M-Cdk]({{< relref "m_cdk" >}}) at the Onset of [Mitosis]({{< relref "mitosis" >}})

        1.  M-Cdk activation begins with the accumulation of [M-cyclin]({{< relref "cyclin_b" >}})
            -   In **embryonic cell cycles**, the synthesis of M-cyclin is constant throughout, and M-cyclin accumulation results from the high stability of the protein in interphase
            -   In **most other cell types**, M-cyclin synthesis increases during G2 and M, owing primarily to an increase in M-cyclin gene transcription
        2.  [Cdk1]({{< relref "cdk1" >}}) forms a complex with [M-cyclin]({{< relref "cyclin_b" >}}) -> formation of M-Cdk complex as the cell approaches mitosis
        3.  [CAK]({{< relref "cdk_activating_kinase" >}}) phosphorylates M-Cdk at its activating site
            -   [Wee1]({{< relref "wee1" >}}) **also** phosphorylates M-Cdk at two neighboring inhibitory sites
            -   Ultimately, this **holds M-Cdk in the inactive state** -> by the end of G2, the cell contains an abundant stockpile of M-Cdk ready to act
        4.  [Cdc25]({{< relref "cdc25" >}}) is phosphorylated -> **activated** -> **removes the inhibitory phosphates that restrain M-Cdk**
            -   At the same time, [Wee1]({{< relref "wee1" >}}) activity is suppressed -> **ensures that M-Cdk activity increases**
        5.  M-Cdk can interact with its own effectors:
            -   [Cdc25]({{< relref "cdc25" >}}) can be activated (in part) by M-Cdk
            -   [Wee1]({{< relref "wee1" >}}) can be inhibited by M-Cdk
            -   This suggests that **M-Cdk activation in mitosis involves positive feedback loops**

        {{< figure src="/ox-hugo/_20210927_172513screenshot.png" width="700" >}}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Mitosis (p. 978)**

    An abrupt increase in [M-Cdk]({{< relref "m_cdk" >}}) activity at the [G2/M transition checkpoint]({{< relref "g2_m_transition_checkpoint" >}}) triggers the events of early [mitosis]({{< relref "mitosis" >}})

    ---


#### [M-phase]({{< relref "m_phase" >}}) {#m-phase--m-phase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A phase in the [cell cycle]({{< relref "cell_cycle" >}}) where [mitosis]({{< relref "mitosis" >}}) occurs

    ---


#### [G2/M transition checkpoint]({{< relref "g2_m_transition_checkpoint" >}}) {#g2-m-transition-checkpoint--g2-m-transition-checkpoint-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A checkpoint in the [cell cycle]({{< relref "cell_cycle" >}}) that triggers early [mitosis]({{< relref "mitosis" >}})

    ---


#### [Cyclin B]({{< relref "cyclin_b" >}}) {#cyclin-b--cyclin-b-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [cyclin]({{< relref "cyclin" >}}) involved in [mitosis]({{< relref "mitosis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
