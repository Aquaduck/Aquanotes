+++
title = "RNA"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Types of RNA {#types-of-rna}

-   **[Messenger RNA]({{< relref "messenger_rna" >}})** (mRNA)
-   **[Transfer RNA]({{< relref "transfer_rna" >}})** (tRNA)
-   **[Ribosomal RNA]({{< relref "ribosomal_rna" >}})** (rRNA)


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Khan - RNA and Protein Synthesis Review]({{< relref "khan_rna_and_protein_synthesis_review" >}}) {#khan-rna-and-protein-synthesis-review--khan-rna-and-protein-synthesis-review-dot-md}

<!--list-separator-->

-  Structure of [RNA]({{< relref "rna" >}})

    <!--list-separator-->

    -  3 main differences between DNA and RNA:

        1.  RNA uses _ribose_ instead of _deoxyribose_
        2.  RNA is usually _single-stranded_
        3.  RNA contains _uracil_ in place of _thymine_

        {{< figure src="/ox-hugo/_20210721_165142screenshot.png" >}}

    <!--list-separator-->

    -  Types of RNA

        | Type                 | Role                                                                                     |
        |----------------------|------------------------------------------------------------------------------------------|
        | Messenger RNA (mRNA) | Carries information from DNA in the nucleus to ribosomes in the cytoplasm                |
        | Ribosomal RNA (rRNA) | Structural component of [ribosomes]({{< relref "ribosome" >}})                           |
        | Transfer RNA (tRNA)  | Carries amino acids to the ribosome during tranlsation to help build an amino acid chain |


#### [Folate]({{< relref "folate" >}}) {#folate--folate-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Important for the synthesis of nitrogenous bases in [DNA]({{< relref "dna" >}}) and [RNA]({{< relref "rna" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
