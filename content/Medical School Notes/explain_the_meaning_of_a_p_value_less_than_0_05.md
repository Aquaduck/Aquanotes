+++
title = "Explain the meaning of a p-value less than 0.05"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}


### [P-value]({{< relref "p_value" >}}) {#p-value--p-value-dot-md}

-   Probability that a statistical test leads to false positive
-   If p-value is <= a predetermined significance level (usually **0.05**) -> association considered **statistically significant**
-   **It is not possible to prove [H<sub>1</sub>]({{< relref "alternative_hypothesis" >}}) true**, but having a p-value lower than [significance level]({{< relref "significance_level" >}}) -> very unlikely that [H<sub>0</sub>]({{< relref "null_hypothesis" >}}) is correct


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > The Practice of Evidence-Based Medicine: Inferential Statistics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Explain the meaning of a p-value less than 0.05]({{< relref "explain_the_meaning_of_a_p_value_less_than_0_05" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
