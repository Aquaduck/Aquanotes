+++
title = "Biotin"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   AKA _Vitamin B<sub>7</sub>_
-   [B vitamin]({{< relref "b_vitamin" >}})


## From First Aid Section II Biochemistry Nutrition {#from-first-aid-section-ii-biochemistry-nutrition}


### Function {#function}

-   Cofactor for carboxylation [enzymes]({{< relref "enzyme" >}})/carboxylases (which add a 1-carbon group):
    -   [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}): pyruvate (3C) -> oxaloacetate (4C)
    -   [Acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}}): acetyl-CoA (2C) -> malonyl-CoA (3C)
    -   [Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}}): propionyl-CoA (3C) -> methylmalonyl-CoA (4C)


## Backlinks {#backlinks}


### 9 linked references {#9-linked-references}


#### [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) {#pyruvate-carboxylase--pyruvate-carboxylase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Requires [biotin]({{< relref "biotin" >}})

    ---


#### [List the vitamins, both by number and by name, and indicate, in broad strokes, why they are important to the metabolic processes and/or pathways specified above]({{< relref "list_the_vitamins_both_by_number_and_by_name_and_indicate_in_broad_strokes_why_they_are_important_to_the_metabolic_processes_and_or_pathways_specified_above" >}}) {#list-the-vitamins-both-by-number-and-by-name-and-indicate-in-broad-strokes-why-they-are-important-to-the-metabolic-processes-and-or-pathways-specified-above--list-the-vitamins-both-by-number-and-by-name-and-indicate-in-broad-strokes-why-they-are-important-to-the-metabolic-processes-and-or-pathways-specified-above-dot-md}

<!--list-separator-->

-  [B7]({{< relref "biotin" >}})

    -   Required by carboxylases


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Beta-oxidation > From Fat Metabolism in Muscle & Adipose Tissue**

    **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Fatty acid catabolism > Oxidation phase**

    **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) uses [Biotin]({{< relref "biotin" >}}) (B7)

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Acetyl-CoA carboxylase**

    Requires [biotin]({{< relref "biotin" >}})

    ---

<!--list-separator-->

-  **🔖 Pyruvate metabolism > Pyruvate carboxylase**

    [Biotin]({{< relref "biotin" >}})-requiring mitochondrial enzyme

    ---


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By vitamin**

    <!--list-separator-->

    -  [Biotin]({{< relref "biotin" >}}) (B7)

        -   Pyruvate carboxylase
        -   Acetyl-CoA carboxylase
        -   Propionyl-CoA carboxylase


#### [Detail the vitamin requirements for the metabolism of propionyl-CoA.]({{< relref "detail_the_vitamin_requirements_for_the_metabolism_of_propionyl_coa" >}}) {#detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot--detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Vitamin requirements for the metabolism of propionyl-CoA**

    [Biotin]({{< relref "biotin" >}}): carboxylation of propionyl-CoA to form [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) via [Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
