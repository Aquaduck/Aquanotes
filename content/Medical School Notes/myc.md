+++
title = "Myc"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Mitogens Stimulate G<sub>1</sub>-Cdk and G1/S-Cdk activities (p. 1012) > Mitogens interact with cell-surface receptors to trigger multiple intracellular signaling pathways**

    <!--list-separator-->

    -  [Myc]({{< relref "myc" >}})

        -   Myc is thought to promote cell-cycle entry by several mechanisms
            -   Increase expression of genes encoding G<sub>1</sub> cyclins ([D cyclins]({{< relref "d_cyclin" >}})) -> increase G<sub>1</sub>-Cdk (cyclin D-Cdk4) activity
            -   Myc also stimulates the transcription of genes that increase cell growth

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Mitogens Stimulate G<sub>1</sub>-Cdk and G1/S-Cdk activities (p. 1012) > Mitogens interact with cell-surface receptors to trigger multiple intracellular signaling pathways > Ras**

    GTPase activity activates MAP kinase cascade -> increased production of transcription regulatory proteins such as [Myc]({{< relref "myc" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
