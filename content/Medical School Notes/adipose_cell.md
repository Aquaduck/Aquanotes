+++
title = "Adipose cell"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Triacylglycerol]({{< relref "triacylglycerol" >}}) {#triacylglycerol--triacylglycerol-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Triacylglycerol energy reserve is stored in [adipocytes]({{< relref "adipose_cell" >}}) in the fed state

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Lipoprotein lipase > From Fat Metabolism in Muscle & Adipose Tissue**

    The LPL isozyme of adipose tissue has the **largest K<sub>m</sub> -> [adipose tissue's]({{< relref "adipose_cell" >}}) uptake of [dietary fat]({{< relref "dietary_lipid" >}}) is a last resort**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  Mobilization of triacylglycerol stored in [adipocytes]({{< relref "adipose_cell" >}})

        -   **Triacylglycerol energy reserve is stored in adipocytes in the fed state**
            -   **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**
                -   Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})
                -   Therefore, **cAMP-mediated activation of PKA is required**
        -   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
            -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
        -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
        -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol
        -   **Ultimately, 3 fatty acids are mobilized/liberated and glycerol is formed**
            -   Fatty acids exit adipose tissue via diffusoin and bind reversibly to plasma [albumin]({{< relref "albumin" >}})
            -   Glycerol taken up by the liver


#### [Explain why adipose tissue is referred to as a fat depot.]({{< relref "explain_why_adipose_tissue_is_referred_to_as_a_fat_depot" >}}) {#explain-why-adipose-tissue-is-referred-to-as-a-fat-depot-dot--explain-why-adipose-tissue-is-referred-to-as-a-fat-depot-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    The LPL isozyme of adipose tissue has the **largest K<sub>m</sub> -> [adipose tissue's]({{< relref "adipose_cell" >}}) uptake of [dietary fat]({{< relref "dietary_lipid" >}}) is a last resort**

    ---


#### [Explain how fatty acids released from adipocyte stores are utilized.]({{< relref "explain_how_fatty_acids_released_from_adipocyte_stores_are_utilized" >}}) {#explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot--explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  How [fatty acids]({{< relref "fatty_acid" >}}) released from [adipocyte]({{< relref "adipose_cell" >}}) stores are utilized

        -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
        -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol
        -   **Ultimately, 3 fatty acids are mobilized/liberated and glycerol is formed**
            -   Fatty acids exit adipose tissue via diffusion and bind reversibly to plasma [albumin]({{< relref "albumin" >}})
            -   Glycerol taken up by the liver


#### [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}}) {#describe-the-structure-of-the-thymus--include-changes-with-age-dot----describe-the-structure-of-the-thymus-include-changes-with-age-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus**

    The thymus involutes in adults and is mostly replaced with [adipose cells]({{< relref "adipose_cell" >}}) -> makes it difficult to locate despite remaining active

    ---


#### [Describe the role of protein kinase A in mobilizing fat from adipocyte stores.]({{< relref "describe_the_role_of_protein_kinase_a_in_mobilizing_fat_from_adipocyte_stores" >}}) {#describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot--describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  The role of [PKA]({{< relref "protein_kinase_a" >}}) in mobilizing fat from [adipocytes]({{< relref "adipose_cell" >}})

        -   **Triacylglycerol energy reserve is stored in adipocytes in the fed state**
            -   **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**
                -   Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})
                -   Therefore, **cAMP-mediated activation of PKA is required**
        -   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
            -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
        -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
        -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol


### Unlinked references {#unlinked-references}

[Show unlinked references]
