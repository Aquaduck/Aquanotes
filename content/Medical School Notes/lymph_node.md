+++
title = "Lymph node"
author = ["Arif Ahsan"]
date = 2021-08-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The major site of activation of the [adaptive immune system]({{< relref "adaptive_immunity" >}})


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Name and describe the pathways of lymphatic drainage (in a lymph node, in the body).]({{< relref "name_and_describe_the_pathways_of_lymphatic_drainage_in_a_lymph_node_in_the_body" >}}) {#name-and-describe-the-pathways-of-lymphatic-drainage--in-a-lymph-node-in-the-body--dot--name-and-describe-the-pathways-of-lymphatic-drainage-in-a-lymph-node-in-the-body-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymph**

    Lymph is filtered through groups of [lymph nodes]({{< relref "lymph_node" >}}) as it moves centrally

    ---


#### [Marginal sinus]({{< relref "marginal_sinus" >}}) {#marginal-sinus--marginal-sinus-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Where afferent vessels enter the [Lymph node]({{< relref "lymph_node" >}})

    ---


#### [List the functions of lymphatic drainage.]({{< relref "list_the_functions_of_lymphatic_drainage" >}}) {#list-the-functions-of-lymphatic-drainage-dot--list-the-functions-of-lymphatic-drainage-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Functions of lymphatic flow**

    The arrangement of lymph and [lymph nodes]({{< relref "lymph_node" >}}) is designed to **activate the immune system before antigens and invading organisms reach the blood**

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Functions of lymphatic flow**

    This filtering action is performed by [lymph nodes]({{< relref "lymph_node" >}}) - the **major sites of activation of the adaptive immune system**

    ---


#### [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}}) {#list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot--list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT**

    <!--list-separator-->

    -  [Lymph nodes]({{< relref "lymph_node" >}})

        -   Small kidney-bean shaped structures normally a few mm in size, but capable of rapidly growing to centimeters during an infection
        -   Occur in groups along the lymphatics
            -   Lymph flows through more than one lymph node on its journey back to the blood
        -   **Lymph nodes are the major site of the Basic Stages 2 and 3 of the [lymphocyte's]({{< relref "lymphocyte" >}}) life cycle** (search for its cognate antigen and activation)

        <!--list-separator-->

        -  Parts of the lymph node:

            {{< figure src="/ox-hugo/_20211009_220911screenshot.png" caption="Figure 1: Anatomy of a lymph node" width="700" >}}

            -   There is a distinctive _hilum_ (circled) where a small artery enters and a vein exits (blood vessels not illustrated)
                -   The _efferent lymphatic_ (10) also exits here
                -   This hilum occupies only a very small percentage of the lymph node surface
            -   The remainder of the surface has a thin _capsule_ (7) of connective tissue with _trabeculae_ (6) that penetrate inward
            -   Lymph enters through _afferent lymphatics_ (1) that perforate the capsule
                -   There is a circumferential _marginal sinus_ (aka subcapsular sinus - 8) and _trabecular sinuses_ that parlalel the trabeculae and lead inwards to a labyrinth of _medullary sinuses_ (4)
            -   The medullary sinuses come together to form the _efferent lymphatic_ (10)
            -   The lymph node is divided into an **outer cortex of follicles and paracortex** and an **inner medulla comprised of medullary sinuses** and the _medullary cords_ (5)
            -   Nodular aggregates of lymphocytes in the cortex are called lymphoid follicles (9)
                -   Follicles consist mostly of [B-lymphocytes]({{< relref "b_lymphocyte" >}})
                -   When activated -> follicle has a **pale germinal center with a dark rim of small resting B-lymphocytes** = _mantle zone_
            -   The paracortex (3) is the part of the cortex without follicles
                -   Comprised of [T-lymphocytes]({{< relref "t_lymphocyte" >}}) with scattered [APCs]({{< relref "antigen_presenting_cell" >}})

            -   _Interdigitating dendritic cells_ are a type of APC that are sprinkled among the T-lymphocytes of the paracortex
            -   [Histiocytes]({{< relref "histiocyte" >}}) (macrophages) mostly reside in the sinuses
                -   Histiocytes filter antigen and debris from the sinuses and may migrate to the paracortex -> differentiate into APCs
            -   Medullary cords contain numerous plasma cells and lymphocytes preparing to leave the lymph node
            -   [High endothelial venule]({{< relref "high_endothelial_venule" >}}) (HEV), a small post-capillary vein (i.e. venule) in the paracortex, is where [lymphocytes]({{< relref "lymphocyte" >}}) move from blood into the lymph node
            -   The overall structure of a lymph node is formed by [reticulin fibers]({{< relref "reticulin_fiber" >}}) - a thin/fine type of collagen that allows easy movement of immune cells


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Immunology**

    [Marginal sinus]({{< relref "marginal_sinus" >}}) is where afferent vessels enter the [lymph node]({{< relref "lymph_node" >}})

    ---


#### [Amboss - Lymphadenopathy]({{< relref "amboss_lymphadenopathy" >}}) {#amboss-lymphadenopathy--amboss-lymphadenopathy-dot-md}

<!--list-separator-->

-  **🔖 Summary**

    Enlargement of [lymph nodes]({{< relref "lymph_node" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
