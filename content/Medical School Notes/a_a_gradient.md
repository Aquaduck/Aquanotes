+++
title = "A-a gradient"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Difference in partial pressure of [oxygen]({{< relref "oxygen" >}}) in the [alveoli]({{< relref "alveoli" >}}) (A) and the partial pressure of oxygen in the arteries (a)


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [V/Q mismatch]({{< relref "v_q_mismatch" >}}) {#v-q-mismatch--v-q-mismatch-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Characterized by an increased [A-a gradient]({{< relref "a_a_gradient" >}})

    ---


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Findings of obstructive vs. restrictive lung disease**

    |                                               |      |                                                      |
    |-----------------------------------------------|------|------------------------------------------------------|
    | [A-a gradient]({{< relref "a_a_gradient" >}}) | High | Normal (extrinsic causes) or high (intrinsic causes) |

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
