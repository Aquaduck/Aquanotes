+++
title = "Aspiration of objects or fluids into the lungs"
author = ["\"Moore", "Keith L\" \"Dalley", "Arthur F.\""]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

_Prework taken from:_ Moore, Keith L, and Arthur F. Dalley. **Clinical Oriented Anatomy**. Philadelphia: Lippincott Williams & Wilkins, 1999 (8th Ed).


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain the anatomical basis for the aspiration of foreign objects into the bronchial tree.]({{< relref "explain_the_anatomical_basis_for_the_aspiration_of_foreign_objects_into_the_bronchial_tree" >}}) {#explain-the-anatomical-basis-for-the-aspiration-of-foreign-objects-into-the-bronchial-tree-dot--explain-the-anatomical-basis-for-the-aspiration-of-foreign-objects-into-the-bronchial-tree-dot-md}

<!--list-separator-->

-  From [Aspiration of objects or fluids into the lungs]({{< relref "aspiration_of_objects_or_fluids_into_the_lungs" >}})

    -   Aspirated objects are **more likely to lodge into the right main bronchus** because it is **shorter, wider, and more vertical** than the left
    -   Aspirated liquids in a **supine patient** move by gravity to **the more posterior aspects of the lung**
        -   More likely to enter the **superior segments of the right and left lower lobes** -> the first posterior branches of the bronchial tree in the lower lobes


### Unlinked references {#unlinked-references}

[Show unlinked references]
