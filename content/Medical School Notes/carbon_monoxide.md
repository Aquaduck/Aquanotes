+++
title = "Carbon Monoxide"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Factors Affecting Hemoglobin Dissociation Curve]({{< relref "factors_affecting_hemoglobin_dissociation_curve" >}}) {#factors-affecting-hemoglobin-dissociation-curve--factors-affecting-hemoglobin-dissociation-curve-dot-md}

<!--list-separator-->

-  **🔖 Notes > Factors affecting O<sub>2</sub>-Hb Dissociation Curve:**

    <!--list-separator-->

    -  [Carbon Monoxide]({{< relref "carbon_monoxide" >}})

        -   Carbon monoxide is a **competitive inhibitor of hemoglobin**
            -   250x more likely to bind to heme group
        -   By binding to hemoglobin, **CO causes it to increase its affinity for O<sub>2</sub> -> less oxygen delivered to cells of tissue**
        -   Causes a **leftward shift** in the curve _and_ **lowers the curve** because it **lowers hemoglobins O<sub>2</sub>-carrying capacity**


#### [Describe the role of gases in cell signaling with special emphasis on nitric oxide and its basic mechanism of action.]({{< relref "describe_the_role_of_gases_in_cell_signaling_with_special_emphasis_on_nitric_oxide_and_its_basic_mechanism_of_action" >}}) {#describe-the-role-of-gases-in-cell-signaling-with-special-emphasis-on-nitric-oxide-and-its-basic-mechanism-of-action-dot--describe-the-role-of-gases-in-cell-signaling-with-special-emphasis-on-nitric-oxide-and-its-basic-mechanism-of-action-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  [CO]({{< relref "carbon_monoxide" >}})

        -   CO is a physiologically important signal molecule and is thought to work in the same way as nitric oxide


### Unlinked references {#unlinked-references}

[Show unlinked references]
