+++
title = "K-Ras"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A member of the [Ras]({{< relref "ras" >}}) gene family
-   Involves [receptor tyrosine kinase signaling]({{< relref "receptor_protein_tyrosine_kinase" >}})
-   An oncogene associated with colorectal cancer


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
