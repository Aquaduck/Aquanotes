+++
title = "Cytochrome c"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Water-soluble component of the [ETC]({{< relref "oxidative_phosphorylation" >}})
-   Part of the [Intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}})
-   When released into the cytosol, cytochrome c binds to [Apaf1]({{< relref "apoptotic_protease_activating_factor_1" >}}) -> formation of [Apoptosome]({{< relref "apoptosome" >}}) -> recruits [Caspase-9]({{< relref "caspase_9" >}}) -> activation of downstream [Executioner caspase]({{< relref "executioner_caspase" >}}) -> induction of apoptosis


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Smac, a Mitochondrial Protein that Promotes Cytochrome c–Dependent Caspase Activation by Eliminating IAP Inhibition]({{< relref "smac_a_mitochondrial_protein_that_promotes_cytochrome_c_dependent_caspase_activation_by_eliminating_iap_inhibition" >}}) {#smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition--smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition-dot-md}

<!--list-separator-->

-  **🔖 Discussion > Regulation of SMAC**

    This is analogous to the requirement for mitochondrial processing of [cytochrome c]({{< relref "cytochrome_c" >}}), which promotes caspase activation after release from the mitochondrial intermembrnae space

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026)**

    Controls the release of [cytochrome c]({{< relref "cytochrome_c" >}}) and other intermembrane mitochondrial proteins into the cytosol

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  [Cytochrome c]({{< relref "cytochrome_c" >}}) and Apaf1

        <!--list-separator-->

        -  The [intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}}) depends on mitochondria (p. 1025)

            -   Cytochrome c is a water-soluble component of the mitochondrial electron transport chain
            -   When released into the cytosol, cytochrome c binds to [Apaf1]({{< relref "apoptotic_protease_activating_factor_1" >}}) -> Apaf1 oligomerizes into a wheel-like heptamer called an [apoptosome]({{< relref "apoptosome" >}}) -> recruits [caspase-9]({{< relref "caspase_9" >}}) -> activation of downstream executioner caspases -> induction of apoptosis


### Unlinked references {#unlinked-references}

[Show unlinked references]
