+++
title = "Atrial septal defect"
author = ["Arif Ahsan"]
date = 2021-10-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Atrial septal defect]({{< relref "atrial_septal_defect" >}}) (ASD)

    ---


#### [DiGeorge syndrome]({{< relref "digeorge_syndrome" >}}) {#digeorge-syndrome--digeorge-syndrome-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Atrial septal defect]({{< relref "atrial_septal_defect" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Immunology > DiGeorge syndrome > Clinical features: > Cardiac anomalies**

    [Atrial septal defect]({{< relref "atrial_septal_defect" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
