+++
title = "Describe the process of G-protein activation and inactivation."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### The Trimeric G-Protein Cycle {#the-trimeric-g-protein-cycle}

-   All G-proteins work in this way
-   **Inactive when GDP is bound** to α subunit
    -   Involves **dephosphorylation** of already bound GTP
-   **Active when GTP is bound**
    -   **completely replaces** GDP with GTP


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the process of G-protein activation and inactivation.]({{< relref "describe_the_process_of_g_protein_activation_and_inactivation" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
