+++
title = "Name and describe five basic epithelial types and relationship to the basement membrane."
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}}) {#from-brs-cell-biology-and-histology--brs-cell-biology-histology-dot-md}


### Classification of epithelia table (p. 68) {#classification-of-epithelia-table--p-dot-68}

| Type                                | Shape of Superficial Layer          | Typical Locations                                            |
|-------------------------------------|-------------------------------------|--------------------------------------------------------------|
| Simple squamous                     | Flattened                           | Endothelium (lining of blood vessels)                        |
|                                     |                                     | Mesothelium (lining of peritonium and pleura)                |
| Simple cuboidal                     | Cuboidal                            | Lining of distal tubule in kidneys                           |
|                                     |                                     | Lining of ducts in some glands                               |
|                                     |                                     | Surface of ovary                                             |
| Simple columnar                     | Columnar                            | Lining of intestine                                          |
|                                     |                                     | Lining of stomach                                            |
|                                     |                                     | Lining of excretory ducts in some glands                     |
| Pseudostratified                    | All cells rest on basal lamina      | Lining of trachea                                            |
|                                     | Not all reach the lumen             | Lining of primary bronchi                                    |
|                                     | Causes appearance of stratification | Lining of nasal cavity                                       |
|                                     |                                     | Lining of excretory ducts in some glands                     |
| Stratified squamous (nonkeratinzed) | Flattened (nucleated)               | Lining of esophagus                                          |
|                                     |                                     | Lining of vagina                                             |
|                                     |                                     | Lining of mouth                                              |
|                                     |                                     | Lining of true vocal cords                                   |
| Stratified squamous (keratinized)   | Flattened (without nuclei)          | Epidermis of skin                                            |
| Stratified cuboidal                 | Cuboidal                            | Lining of ducts in sweat glands                              |
| Stratified columnar                 | Columnar                            | Lining of large excretory ducts in some glands               |
|                                     |                                     | Lining of cavernous urethra                                  |
| Transitional                        | Dome-shaped (when relaxed)          | Lining of urinary passages from renal calyces to the urethra |
|                                     | Flattened (when stretched)          |                                                              |


### Classification of epithelia diagrams (p.68) {#classification-of-epithelia-diagrams--p-dot-68}

{{< figure src="/ox-hugo/_20210913_213055screenshot.png" >}}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-03 Fri] </span></span> > Cell Junctions & Histology of Epithelial Tissue > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Name and describe five basic epithelial types and relationship to the basement membrane.]({{< relref "name_and_describe_five_basic_epithelial_types_and_relationship_to_the_basement_membrane" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
