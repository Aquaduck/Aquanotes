+++
title = "Hormone-sensitive lipase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [lipase]({{< relref "lipase" >}})
-   [PKA]({{< relref "protein_kinase_a" >}})-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [The perilipin family of lipid droplet proteins: Gatekeepers of intracellular lipolysis]({{< relref "the_perilipin_family_of_lipid_droplet_proteins_gatekeepers_of_intracellular_lipolysis" >}}) {#the-perilipin-family-of-lipid-droplet-proteins-gatekeepers-of-intracellular-lipolysis--the-perilipin-family-of-lipid-droplet-proteins-gatekeepers-of-intracellular-lipolysis-dot-md}

<!--list-separator-->

-  **🔖 3. Lipolysis plays an essential role in lipid homeostasis**

    Lipolysis is catalyzed by lipases that cycle between the cytoplasm or cytoplasmic surfaces of the endoplasmic reticulum and the surfaces of lipid droplets; for recent reviews on lipases, see [\\[30\\](https://www.sciencedirect.com/science/article/pii/S1388198117301403?via%3Dihub#bb0150)], [\\[31\\](https://www.sciencedirect.com/science/article/pii/S1388198117301403?via%3Dihub#bb0155)], [\\[32\\](https://www.sciencedirect.com/science/article/pii/S1388198117301403?via%3Dihub#bb0160)], [\\[33\\](https://www.sciencedirect.com/science/article/pii/S1388198117301403?via%3Dihub#bb0165)], [\\[34\\](https://www.sciencedirect.com/science/article/pii/S1388198117301403?via%3Dihub#bb0170)]. The first identified and most highly characterized lipase is [hormone-sensitive lipase](https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/hormone-sensitive-lipase) ([HSL]({{< relref "hormone_sensitive_lipase" >}})), an enzyme with strong [diacylglycerol](https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/diacylglycerol) and cholesterol ester [hydrolase](https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/hydrolase) activity, and weaker triacylglycerol, monoacylglycerol, and retinyl ester hydrolase activity _in vitro_. HSL is highly expressed in white and brown adipose tissue, and at lower levels in a variety of tissues including testis, ovaries, adrenal gland, skeletal muscle, heart, and mammary gland. The [subcellular localization](https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/subcellular-localization) and activity of HSL is dynamically regulated by phosphorylation. The cAMP-dependent protein kinase (PKA)-mediated phosphorylation of HSL on 2 [serine](https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/serine) residues (Ser659 and Ser660 in rat HSL) is required for activating the lipase and promoting the translocation of HSL from the cytoplasm to the surfaces of lipid droplets to gain access to substrate lipids [\\[35\\](https://www.sciencedirect.com/science/article/pii/S1388198117301403?via%3Dihub#bb0175)]. An additional serine residue (Ser563 in rat HSL) is phosphorylated by PKA, although the consequence of this phosphorylation is unknown. The use of antibodies raised against these specific phosphorylated residues of HSL has revealed that the rapid PKA-mediated phosphorylation of Ser660 precedes the phosphorylation of Ser563 in hormonally stimulated [3T3-L1](https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/3t3-l1) adipocytes [\\[36\\](https://www.sciencedirect.com/science/article/pii/S1388198117301403?via%3Dihub#bb0180)]. Moreover, within 1 min of the activation of PKA, HSL with phosphorylated Ser660 can be detected on lipid droplets, as well as in the cytoplasm, particularly near the periphery of cells. In contrast, phosphorylated Ser563 was detected only on lipid droplets at 10 min after the activation of PKA, suggesting both spatial and temporal differences in the phosphorylation of these serine residues by PKA. These data suggest that the phosphorylation of Ser660 (and likely Ser659) is required for translocation of HSL from the cytoplasm to lipid droplets. HSL is also phosphorylated by AMP kinase; a recent study has shown that the phosphorylation of Ser565 of murine HSL impedes the PKA-mediated phosphorylation of HSL at Ser563 and Ser660, thus attenuating translocation and activation of the lipase [\\[37\\](https://www.sciencedirect.com/science/article/pii/S1388198117301403?via%3Dihub#bb0185)].

    ---


#### [Protein kinase A]({{< relref "protein_kinase_a" >}}) {#protein-kinase-a--protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    PKA-catalyzed phosphorylation of [perilipin]({{< relref "perilipin" >}}) and [HSL]({{< relref "hormone_sensitive_lipase" >}}) is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  [Hormone-sensitive lipase]({{< relref "hormone_sensitive_lipase" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
        -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism**

    <!--list-separator-->

    -  [Hormone-sensitive lipase]({{< relref "hormone_sensitive_lipase" >}})

        -   **Catabolic** enzyme
        -   Undergoes reciptrocal regulation with intracellular [acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}})
            -   [PKA]({{< relref "protein_kinase_a" >}}) substrate
                -   Up-regulated by phosphorylation
                -   Down-regulated by dephosphorylation
        -   The **second** of three lipases **required for the complete hydrolysis of triglycerides** to 3 fatty acids and glycerol in adipose tissue
            -   **The isozyme restricted to adipose tissue is ONLY active in the fasting state**
                -   Consequently, glycerol and fatty acids produced via lipolysis are released from the adipocytes
                    -   Glycerol taken up by liver -> used as [gluconeogenic]({{< relref "gluconeogenesis" >}}) substrate
                    -   Released fatty acids -> taken-up by rapidly metabolizing tissues (e.g. liver) -> use fatty acids as energy source to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})


#### [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}}) {#describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot--describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Regulation of the mobilization of intramuscular fat > Role of epinephrine**

    Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol

    ---

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Regulation of the mobilization of intramuscular fat > Role of skeletal muscle contraction**

    Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
