+++
title = "TNF alpha and the TNF receptor superfamily: structure-function relationship(s)"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Biological role(s) of [TNF-α]({{< relref "tumor_necrosis_factor" >}}) {#biological-role--s--of-tnf-α--tumor-necrosis-factor-dot-md}

-   TNF plays several therapeutic roles within the body, which includes:
    -   Immunostimulation
    -   Resistance to infection agents
    -   Resistance to tumours
    -   Sleep regulation
    -   Embryonic development
-   On the other hand, parasitic, bacterial, and viral infections become more pathogenic or fatal due to TNF circulation
-   The **major role** of TNF is as an important mediator in resistance against infections


## [TNF-α]({{< relref "tumor_necrosis_factor" >}}) mechanism(s) of action {#tnf-α--tumor-necrosis-factor-dot-md--mechanism--s--of-action}

-   TNF exerts its effect(s) by forming a trimer that binds to and clusters high-affinity receptors present in great numbers on most cell membranes
    -   The ligand/receptor complex is rapidly internalised via clathrin-coated pits -> secondary lysosomes degrade it
    -   This causes the activation of many secondary proteins that are involved with gene transcription and/or production of ROS and nitrogen radicals:
        1.  G-Protein
        2.  Transcription factors
            -   NF-κB
            -   AP-1
        3.  Protein kinases
            -   CK II
            -   Erk-1
            -   Erk-2
            -   [MAP2]({{< relref "map2" >}})
        4.  Phospholipases
            -   PLA<sub>2</sub>
            -   PLC
            -   PLD
            -   Sphingomyelinase
        5.  Mitochondrial proteins
            -   Manganese superoxide dismutase
        6.  [Caspases]({{< relref "caspase" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
