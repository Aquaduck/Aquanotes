+++
title = "Hyaline cartilage"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Perichondrium]({{< relref "perichondrium" >}}) {#perichondrium--perichondrium-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    Found in [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) and [Elastic cartilage]({{< relref "elastic_cartilage" >}})

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons > Joints**

    Some joints have fibrocartilage supplementing the [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) (e.g. menisci of knee, glenoid labra, ligamentum teres and acetabular labra of hip)

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons > Joints**

    Most joints are lined by [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) on the surface of each articulating bone

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone formation > Endochondral bone formation > Steps**

    [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) first forms a model of the bone that is then converted to bone

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
