+++
title = "Session 3"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "jumpstart"]
draft = false
+++

## Applications Packet 1 {#applications-packet-1}

1.  D
    -   2x standard deviation = 95%
    -   5% outside that range _on both sides_
    -   Therefore, above is half that -> 2.5%
2.  D
    -   Normal distribution = average is mean, median, and mode
        -   Necessary to calculate standard deviation
    -   Therefore all must be true
3.  D
    -   Left-skew: Mode > Median > Mean
    -   Right-skew: Mean > Median > Mode
    -   Graph in question is right-skewed, therefore mean is greater than median
4.  B
    -   Bimodal distribution = SD is **maximum distance** from mean
5.  A
    -   SD = 0.3
    -   Less than 1% = 3 SDs away from 12 oz (3 SDs is 99.7%)
    -   11.2 is closest to this value
6.  C
    -   More combinations -> more normal distribution
7.  C
8.  C
9.  B
    -   2 SDs away = falls to 95% range
    -   Below = lower end of bell curve -> around 2%


## Applications Packet 2 {#applications-packet-2}

1.  B
2.  B
3.  C
4.  D


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
