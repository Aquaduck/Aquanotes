+++
title = "Cardiac muscle cell"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept link {#concept-link}

-   The cellular component of [Cardiac muscle]({{< relref "cardiac_muscle" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Osmosis]({{< relref "osmosis" >}}) {#osmosis--osmosis-dot-md}

<!--list-separator-->

-  Action potentials in [myocytes]({{< relref "cardiac_muscle_cell" >}})

    <!--list-separator-->

    -  Phase 0 - depolarization

        -   **Rapid influx of** _sodium_ into the cell -> _inward current_
            -   Responsible for **rapid depolarization**

    <!--list-separator-->

    -  Phase 1

        -   _Sodium_ **current stops**
        -   _Potassium_ **slowly** flows out of cell
        -   Depolarization stops, repolarization begins

    <!--list-separator-->

    -  Phase 2

        -   _Calcium_ **moves into the cell**
            -   Balances _potassium_ current moving out of cell -> **plateau** (charges are balanced)

    <!--list-separator-->

    -  Phase 3

        -   _Calcium_ current stops\*
        -   _Potassium_ **continues to move** out of cell -> **repolarization continues**

    <!--list-separator-->

    -  Phase 4

        -   _Potassium_ current moving out of cell approaches equilibrium
            -   _Sodium_ + _calcium_ current moving into cell balances this -> **resting membrane potential achieved**


#### [L-type calcium channel]({{< relref "l_type_calcium_channel" >}}) {#l-type-calcium-channel--l-type-calcium-channel-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Located in the [Sarcolemma]({{< relref "sarcolemma" >}}) of [cardiac muscle cells]({{< relref "cardiac_muscle_cell" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
