+++
title = "Red blood cell"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   Produced in [bone marrow]({{< relref "bone_marrow" >}})


## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [Sickle cell disease]({{< relref "sickle_cell_disease" >}}) {#sickle-cell-disease--sickle-cell-disease-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Sickling of [RBCs]({{< relref "red_blood_cell" >}}) causes RBCs to breakdown prematurely -> leads to [anemia]({{< relref "anemia" >}})

    ---


#### [Red pulp]({{< relref "red_pulp" >}}) {#red-pulp--red-pulp-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A component of the [splenic]({{< relref "spleen" >}}) parenchyma that contains mostly [red blood cells]({{< relref "red_blood_cell" >}})

    ---


#### [Kidney]({{< relref "kidney" >}}) {#kidney--kidney-dot-md}

<!--list-separator-->

-  **🔖 Concept links > Major functions**

    Regulation of [RBC]({{< relref "red_blood_cell" >}}) production

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Renal functions (p. 10)**

    <!--list-separator-->

    -  Regulation of [RBC]({{< relref "red_blood_cell" >}}) production


#### [FoCS (Sickle Cell Case)]({{< relref "focs_sickle_cell_case" >}}) {#focs--sickle-cell-case----focs-sickle-cell-case-dot-md}

<!--list-separator-->

-  **🔖 Pathophysiology**

    **When [RBCs]({{< relref "red_blood_cell" >}}) containing homozygous HbS are exposed to deoxy conditions, the sickling process begins**

    ---

<!--list-separator-->

-  **🔖 Introduction**

    Signs and symptoms caused by **sickling of [red blood cells]({{< relref "red_blood_cell" >}})**

    ---


#### [Factors Affecting Hemoglobin Dissociation Curve]({{< relref "factors_affecting_hemoglobin_dissociation_curve" >}}) {#factors-affecting-hemoglobin-dissociation-curve--factors-affecting-hemoglobin-dissociation-curve-dot-md}

<!--list-separator-->

-  **🔖 Notes > Factors affecting O<sub>2</sub>-Hb Dissociation Curve: > pH**

    When CO<sub>2</sub> leaves the tissue and enters the [red blood cells]({{< relref "red_blood_cell" >}}) of the capillaries, it is readily converted into _bicarbonate_ and _hydrogen ions_ -> **decreases pH**

    ---


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen > Splenic function**

    Ranges from unwanted or damaged proteins to senescent cells (especially aged [RBCs]({{< relref "red_blood_cell" >}}))

    ---


#### [B vitamin]({{< relref "b_vitamin" >}}) {#b-vitamin--b-vitamin-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A class of water-soluble [vitamins]({{< relref "vitamin" >}}) that play an important role in [cell metabolism]({{< relref "metabolism" >}}) and synthesis of [red blood cells]({{< relref "red_blood_cell" >}})

    ---


#### [A Discussion of the Significance of Two Key Metabolic Branchpoints]({{< relref "a_discussion_of_the_significance_of_two_key_metabolic_branchpoints" >}}) {#a-discussion-of-the-significance-of-two-key-metabolic-branchpoints--a-discussion-of-the-significance-of-two-key-metabolic-branchpoints-dot-md}

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > RBCs > Fed state**

    [Insulin]({{< relref "insulin" >}}) has **no effect on glucose uptake into [RBCs]({{< relref "red_blood_cell" >}})** in the **fed state**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
