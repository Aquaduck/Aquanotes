+++
title = "Distal convoluted tubule"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Macula densa]({{< relref "macula_densa" >}}) {#macula-densa--macula-densa-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Mark the end of the [thick ascending limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}}) and the beginning of the [distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}})

    ---


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18)**

    <!--list-separator-->

    -  [Distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}})

        -   Marked by the [Macula densa]({{< relref "macula_densa" >}}) cells at the vascular pole of the Bowman's capsule
        -   Followed by the [connecting tubule]({{< relref "connecting_tubule" >}}) -> initial collecting tubule of the cortical collecting duct


#### [Connecting tubule]({{< relref "connecting_tubule" >}}) {#connecting-tubule--connecting-tubule-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [Nephron]({{< relref "nephron" >}}) that follows the [Distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration**

    <!--list-separator-->

    -  [Distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}})

        -   Segment of the [nephron]({{< relref "nephron" >}}) located between the ascending loop of Henle and the connecting tubule and collecting duct

<!--list-separator-->

-  **🔖 Respiration > Macula densa**

    A specialized group of tall cuboidal cells in the [distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}}) of the kidney

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
