+++
title = "G protein-coupled receptors"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Pharmacology: General Principles of Pharmacodynamics II]({{< relref "pharmacology_general_principles_of_pharmacodynamics_ii" >}}) {#pharmacology-general-principles-of-pharmacodynamics-ii--pharmacology-general-principles-of-pharmacodynamics-ii-dot-md}

<!--list-separator-->

-  **🔖 Receptor signaling: drug effects**

    <!--list-separator-->

    -  [G protein-coupled receptors]({{< relref "g_protein_coupled_receptors" >}})

        -   _G_ refers to guanine nucleotide
        -   Link extracellular signals to intracellular enzymes or ion channels via heterotrimeric G proteins
        -   When receptor binds to agonist:
            -   Agonist interacts with G protein -> exchange of GTP to GDP
            -   When **bound to GTP** -> G protein can activate/inhibit enzymes
                -   Primarily dependent on the **G protein alpha subunit**
        -   Several different [G proteins]({{< relref "g_protein" >}}) that couple to numerous receptors and can have opposing effects:

            <div class="table-caption">
              <span class="table-number">Table 1</span>:
              G protein-coupled receptors
            </div>

            | G protein     | Receptors                                                | Signaling pathway                             |
            |---------------|----------------------------------------------------------|-----------------------------------------------|
            | G<sub>s</sub> | Beta adrenergic receptors                                | Increase cAMP                                 |
            |               | Glucagon                                                 | Excitatory effects                            |
            |               | Histamine                                                |                                               |
            |               | Serotonin                                                |                                               |
            | G<sub>i</sub> | Alpha<sub>2</sub> adrenergic receptors                   | Decrease cAMP                                 |
            |               | mAchR                                                    | Cardiac K+ channel open (decrease heart rate) |
            |               | Opioid                                                   |                                               |
            |               | Serotonin                                                |                                               |
            | G<sub>q</sub> | mAchR                                                    | PLC - IP<sub>3</sub>                          |
            |               | H1                                                       | DAG                                           |
            |               | α1                                                       | Increase cytoplasmic calcium                  |
            |               | Vasopressin type 1                                       |                                               |
            |               | 5HT<sub>1C</sub>                                         |                                               |
            | G<sub>t</sub> | Rhodopsin and color opsins in retinal rod and cone cells | Increase cGMP phosphodiesterase               |
            |               |                                                          | Decrease cGMP                                 |


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase**

    Both glucagon and epinephrine bind to specific [G protein-coupled receptors]({{< relref "g_protein_coupled_receptors" >}}) (GPCRs) on the [plasma membrane]({{< relref "plasma_membrane" >}})

    ---


#### [Describe the Yin-Yang regulation of adenylate cyclase.]({{< relref "describe_the_yin_yang_regulation_of_adenylate_cyclase" >}}) {#describe-the-yin-yang-regulation-of-adenylate-cyclase-dot--describe-the-yin-yang-regulation-of-adenylate-cyclase-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > The Yin and Yang of cAMP Regulation**

    More than one [GPCR]({{< relref "g_protein_coupled_receptors" >}}) can be coupled to the same intracellular signal pathway

    ---


#### [Describe the cyclic-AMP (cAMP) second messenger signal pathway from hormone binding to activation of protein kinase-A.]({{< relref "describe_the_cyclic_amp_camp_second_messenger_signal_pathway_from_hormone_binding_to_activation_of_protein_kinase_a" >}}) {#describe-the-cyclic-amp--camp--second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot--describe-the-cyclic-amp-camp-second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > G-protein signal cascades: cAMP**

    **Must traverse the plasma membrane 7 times** <- required of **all** [GPCRs]({{< relref "g_protein_coupled_receptors" >}}) (so far described)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
