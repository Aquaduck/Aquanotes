+++
title = "Urine"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Metabolic Alkalosis: Primary Bicarbonate Excess (p. 1281)**

    [Cushing's disease]({{< relref "cushing_s_disease" >}}): oversecretion of [ACTH]({{< relref "adrenocorticotrophic_hormone" >}}) -> elevated [aldosterone]({{< relref "aldosterone" >}}) + more [potassium]({{< relref "potassium" >}}) excreted in [urine]({{< relref "urine" >}})

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Major physiological functions of the kidney**

    <!--list-separator-->

    -  Production of [Urine]({{< relref "urine" >}})

        -   **Excretion** of metabolic waste and end-products of [metabolism]({{< relref "metabolism" >}})
        -   **Regulation** of extracellular fluid volume and [osmolality]({{< relref "osmolality" >}})
        -   **Maintenance** of acid-base balance
        -   **Maintenance** of [electrolyte]({{< relref "electrolyte" >}}) concentrations
        -   **Regulation** of blood pressure and blood volume
        -   Participation in **[gluconeogenesis]({{< relref "gluconeogenesis" >}})** ([glutamine]({{< relref "glutamine" >}}) and [glutamate]({{< relref "glutamate" >}})) and **[ketogenesis]({{< relref "ketogenesis" >}})**


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    [Urine]({{< relref "urine" >}}) formation begins with glomerular filtration: the bulk flow of fluid from the glomerular capillaries into [Bowman's capsule]({{< relref "bowman_capsule" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
