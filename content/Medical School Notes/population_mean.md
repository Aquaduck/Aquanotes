+++
title = "Population mean"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [Mean]({{< relref "mean" >}}) of a [Population]({{< relref "population" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}}) {#explain-the-t-test-and-the-factors-that-influence-t-values--explain-the-t-test-and-the-factors-that-influence-t-values-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > T-test > One sample t-test**

    Alternatively, calculate [confidence intervals]({{< relref "confidence_interval" >}}) of the sample observations -> check if [population mean]({{< relref "population_mean" >}}) falls within the range given by confidence intervals

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
