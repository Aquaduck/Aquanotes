+++
title = "Define statistical power and explain how it affects the interpretation of negative results"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}


### [Statistical power]({{< relref "statistical_power" >}}) {#statistical-power--statistical-power-dot-md}

-   The probability of correctly rejecting the [null hypothesis]({{< relref "null_hypothesis" >}})
-   Complementary to the [type 2 error]({{< relref "type_ii_error" >}}) rate
-   Positively correlates with sample size and magnitude of association of interest
    -   i.e. **increasing sample size = increasing statistical power**
    -   Correlates with measurement [accuracy]({{< relref "accuracy" >}})
-   Most studies aim to achieve **80% statistical power**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > The Practice of Evidence-Based Medicine: Inferential Statistics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Define statistical power and explain how it affects the interpretation of negative results]({{< relref "define_statistical_power_and_explain_how_it_affects_the_interpretation_of_negative_results" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
