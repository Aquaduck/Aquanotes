+++
title = "Red pulp"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A component of the [splenic]({{< relref "spleen" >}}) parenchyma that contains mostly [red blood cells]({{< relref "red_blood_cell" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen > Spleen filtration**

    Senescent cells and material are removed as they attempt to navigate the [macrophages]({{< relref "macrophage" >}}) of the [sheath]({{< relref "sheathed_capillary" >}}) and [cords]({{< relref "splenic_cords" >}}) -> squeeze between endothelial cells to return to the vascular system in the sinuses of the [red pulp]({{< relref "red_pulp" >}})

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen > Splenic function**

    The [red pulp]({{< relref "red_pulp" >}}) filters and digests "particulate" material in the blood

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen**

    The splenic parenchyma contains two interspersed components: [red pulp]({{< relref "red_pulp" >}}) and [white pulp]({{< relref "white_pulp" >}}) (named for their fresh unstained color)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
