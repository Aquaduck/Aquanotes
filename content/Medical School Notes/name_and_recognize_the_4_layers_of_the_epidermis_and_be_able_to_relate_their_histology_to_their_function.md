+++
title = "Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function."
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Histology - Skin Pre-session Powerpoint]({{< relref "histology_skin_pre_session_powerpoint" >}}) {#from-histology-skin-pre-session-powerpoint--histology-skin-pre-session-powerpoint-dot-md}


### [Epidermis]({{< relref "epidermis" >}}) - strata {#epidermis--epidermis-dot-md--strata}


#### [Stratum basale]({{< relref "stratum_basale" >}}) {#stratum-basale--stratum-basale-dot-md}

-   Single layer of **cuboidal cells** attached to the basement membrane
-   New cells are generated from this layer (stem cells)


#### [Stratum spinosum]({{< relref "stratum_spinosum" >}}) - "spiny layer" {#stratum-spinosum--stratum-spinosum-dot-md--spiny-layer}

-   Named for the spiny cell-to-cell bridges that are sometimes visible
    -   Correspond to [desmosomes]({{< relref "desmosome" >}})


#### [Stratum granulosum]({{< relref "stratum_granulosum" >}}) - granular layer {#stratum-granulosum--stratum-granulosum-dot-md--granular-layer}

-   [Keratinocytes]({{< relref "keratinocyte" >}}) start to degenerate and make keratin precursors
    -   Keratin-precursors accumulate in the cytoplasm as _keratin-hyaline granules_


#### [Stratum corneum]({{< relref "stratum_corneum" >}}) - keratin layer {#stratum-corneum--stratum-corneum-dot-md--keratin-layer}

-   Skeletal remains of dead cells that are still held together by desmosomes
-   [Keratin]({{< relref "keratin" >}}) and thick plasma membranes form a waxy barrier


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-21 Tue] </span></span> > Histology: Skin Workshop > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}})

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-21 Tue] </span></span> > Histology: Skin Workshop > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
