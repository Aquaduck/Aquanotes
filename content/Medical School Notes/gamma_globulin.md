+++
title = "Gamma globulin"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Produced by [plasma cells]({{< relref "plasma_cell" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Plasma Proteins (p. 796) > Globulin**

    [Gamma globulins]({{< relref "gamma_globulin" >}}) are proteins involved in immunity, better known as antibodies or [immunoglobulins]({{< relref "immunoglobulin" >}})

    ---


#### [Immunoglobulin]({{< relref "immunoglobulin" >}}) {#immunoglobulin--immunoglobulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The most significant group of [gamma globulins]({{< relref "gamma_globulin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
