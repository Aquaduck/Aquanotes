+++
title = "Tubular bone"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [bone]({{< relref "bone" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work**

    <!--list-separator-->

    -  Parts of a [tubular bone]({{< relref "tubular_bone" >}})

        -   3 named regions:
            1.  [Diaphysis]({{< relref "diaphysis" >}}):  mid-shaft of bone
            2.  [Metaphysis]({{< relref "metaphysis" >}}): expanding part of bone next to the growth plate
            3.  [Epiphysis]({{< relref "epiphysis" >}}): end of the bone adjacent to the articular surface and including the growth plate
        -   A [Metaphysis]({{< relref "metaphysis" >}}) and [Epiphysis]({{< relref "epiphysis" >}}) are also present at the top of the bone
        -   Sometimes the [epiphyseal plate]({{< relref "epiphyseal_plate" >}}) is called the physis
            -   Area where a child's bones grow in length
            -   Once this growth plate calcifies (in teenage years), lengthening of bone is no longer possible
        -   [Nutrient canal]({{< relref "nutrient_canal" >}}) goes through the cortex of the [Diaphysis]({{< relref "diaphysis" >}}) -> blood vessel travels to nourish the marrow cavity


#### [Metaphysis]({{< relref "metaphysis" >}}) {#metaphysis--metaphysis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Expanding part of [Long bone]({{< relref "tubular_bone" >}}) next to growth plate

    ---


#### [Epiphysis]({{< relref "epiphysis" >}}) {#epiphysis--epiphysis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    End of [Long bone]({{< relref "tubular_bone" >}}) adjacent to the articular surface and including the growth plate

    ---


#### [Epiphyseal plate]({{< relref "epiphyseal_plate" >}}) {#epiphyseal-plate--epiphyseal-plate-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Growth plate found on [Long bone]({{< relref "tubular_bone" >}})

    ---


#### [Diaphysis]({{< relref "diaphysis" >}}) {#diaphysis--diaphysis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Mid-shaft of [long bone]({{< relref "tubular_bone" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
