+++
title = "Name one drug in which severe adverse effects may depend on the presence of a specific genetic variation"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Pharmacogenetics Pre-learning Material]({{< relref "pharmacogenetics_pre_learning_material" >}}) {#from-pharmacogenetics-pre-learning-material--pharmacogenetics-pre-learning-material-dot-md}


### [Drug allergy]({{< relref "drug_allergy" >}}) {#drug-allergy--drug-allergy-dot-md}

-   Immune hypersensitivity reactions mediated by [immunoglobulin E]({{< relref "immunoglobulin_e" >}}) and driven by [mast cells]({{< relref "mast_cell" >}})
-   **Not pharmacogenomic effects** because specific genetic variations in the patient's germline DNA are not predictive of the response
    -   Depend on prior immune sensitizing exposure


### Examples of pharmacogenomic effects {#examples-of-pharmacogenomic-effects}

-   Genetic variations in the HLA-A and HLA-B genes of the [MHC]({{< relref "major_histocompatibility_complex" >}}) can predict risk for severe toxicities ot certain drugs
    -   Not all individuals with these higher-risk variations wlil have an adverse reaction -> their risk **as a group** is higher


#### Specific drugs {#specific-drugs}

| Drug           | Gene (allele) with high risk variations | Adverse reaction                              |
|----------------|-----------------------------------------|-----------------------------------------------|
| Azathioprine   | TPMT (loss of function)                 | Life-threatening myelosuppression (up to 14%) |
| Mercaptopurine |                                         |                                               |
| Abacavir       | HLA-B (\*57:01)                         | Hypersensitivity reaction (5-8%)              |
| Carbamazepine  | HLA-A (\*15:02, \*31:01)                | Stevens-Johnson Syndrome (SJS)                |
| Oxcarbazepine  |                                         | Toxic Epidermal Necrolysis (TEN)              |
|                |                                         | Eosinophilic Eruption                         |
| Fluorouracil   | DPYD (multiple)                         | Drug toxicity                                 |
| Rasburicase    | G6PD (any deficiency)                   | Severe acute hemolytic anemia                 |


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-24 Fri] </span></span> > Pharmacogenetics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Name one drug in which severe adverse effects may depend on the presence of a specific genetic variation]({{< relref "name_one_drug_in_which_severe_adverse_effects_may_depend_on_the_presence_of_a_specific_genetic_variation" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
