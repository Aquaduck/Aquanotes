+++
title = "Carbonic acid"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Respiratory Acidosis: Primary Carbonic Acid/Carbon Dioxide Excess**

    Occurs when the [blood]({{< relref "blood" >}}) is overly acidic due to an excess of [carbonic acid]({{< relref "carbonic_acid" >}}) <- excess [carbon dioxide]({{< relref "carbon_dioxide" >}}) in the blood

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
