+++
title = "Pleural fluid"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Secreted by [mesothelial cells]({{< relref "mesothelial_cell" >}}) to lubricate the surfaces of the two layers of the [Pleura]({{< relref "pleura" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Pleura of the lung (p. 1057)**

    [Pleural fluid]({{< relref "pleural_fluid" >}}): lubricates the surfaces of the two pleural layers

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
