+++
title = "Nernst equation"
author = ["Arif Ahsan"]
date = 2021-09-11T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Review the mechanisms that set up the resting membrane potential.]({{< relref "review_the_mechanisms_that_set_up_the_resting_membrane_potential" >}}) {#review-the-mechanisms-that-set-up-the-resting-membrane-potential-dot--review-the-mechanisms-that-set-up-the-resting-membrane-potential-dot-md}

<!--list-separator-->

-  **🔖 From Forehand, Action Potential > Variables**

    _E<sub>Na</sub>_ and _E<sub>K</sub>_: equilibrium potentials defined by the [Nernst equation]({{< relref "nernst_equation" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
