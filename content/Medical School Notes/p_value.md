+++
title = "P-value"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain the meaning of a p-value less than 0.05]({{< relref "explain_the_meaning_of_a_p_value_less_than_0_05" >}}) {#explain-the-meaning-of-a-p-value-less-than-0-dot-05--explain-the-meaning-of-a-p-value-less-than-0-05-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [P-value]({{< relref "p_value" >}})

        -   Probability that a statistical test leads to false positive
        -   If p-value is <= a predetermined significance level (usually **0.05**) -> association considered **statistically significant**
        -   **It is not possible to prove [H<sub>1</sub>]({{< relref "alternative_hypothesis" >}}) true**, but having a p-value lower than [significance level]({{< relref "significance_level" >}}) -> very unlikely that [H<sub>0</sub>]({{< relref "null_hypothesis" >}}) is correct


### Unlinked references {#unlinked-references}

[Show unlinked references]
