+++
title = "Lippincott's Illustrated Reviews: Biochemistry, 5e"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  From [Lippincott's Illustrated Reviews: Biochemistry, 5e]({{< relref "lippincott_s_illustrated_reviews_biochemistry_5e" >}})

    <!--list-separator-->

    -  [Salvage pathway for purines]({{< relref "salvage_pathway_for_purines" >}}) (p. 294)

        -   [Purines]({{< relref "purine" >}}) that result from normal turnover of cellular nucleic acids or the small amount obtained from diet that aren't degraded -> converted to nucleoside triphosphates

        <!--list-separator-->

        -  Conversion of purine bases to nucleotides

            -   Two enzymes are involved:
                1.  [Adenine phosphoribosyltransferase]({{< relref "adenine_phosphoribosyltransferase" >}}) (APRT)
                2.  [Hypoxanthine-guanine phosphoribosyltransferase]({{< relref "hypoxanthine_guanine_phosphoribosyltransferase" >}}) (HGPRT)
            -   **Both** use [PRPP]({{< relref "phosphoribosyl_diphosphate" >}}) as the source of ribose 5-phosphate
                -   Hydrolysis by [pyrophosphatase]({{< relref "pyrophosphatase" >}}) makes these reactions **irreversible**

    <!--list-separator-->

    -  Degradation of [Purine]({{< relref "purine" >}}) Nucleotides (p. 296)

        <!--list-separator-->

        -  Degradation of dietary [nucleic acids]({{< relref "nucleic_acid" >}}) in the [small intestine]({{< relref "small_intestine" >}})

            -   Ribonucleases and deoxyribonucleases are secreted by the [pancreas]({{< relref "pancreas" >}})
                -   Hydrolyze dietary RNA and DNA primarily to [oligonucleotides]({{< relref "oligonucleotide" >}})
                -   [Oligonucleotides]({{< relref "oligonucleotide" >}}) are further hydrolyzed by pancreatic [phosphodiesterases]({{< relref "phosphodiesterase" >}}) -> produces a mixture of 3' and 5'-mononucleotides
            -   Dietary purine bases are **not used for the synthesis of tissue nucleic acids**
                -   They are instead converted to [uric acid]({{< relref "uric_acid" >}}) in intestinal mucosal cells
                -   Most of the [Uric acid]({{< relref "uric_acid" >}}) enters the blood -> excreted in urine

        <!--list-separator-->

        -  Formation of [uric acid]({{< relref "uric_acid" >}})

            -   Steps:
                1.  An amino group is removed from [AMP]({{< relref "adenosine_monophosphate" >}}) to produce [IMP]({{< relref "inosine_monophosphate" >}}) by [AMP deaminase]({{< relref "amp_deaminase" >}})
                    -   Alternatively, an amino group is removed from [adenosine]({{< relref "adenosine" >}}) -> produces [inosine]({{< relref "inosine" >}}) (hypoxanthine-ribose) by [adenosine deaminase]({{< relref "adenosine_deaminase" >}})
                2.  [IMP]({{< relref "inosine_monophosphate" >}}) and [GMP]({{< relref "guanosine_monophosphate" >}}) are converted into their nucleoside forms (inosine and guanosine) by the action of [5'-nucleotidase]({{< relref "5_nucleotidase" >}})
                3.  [Purine nucleoside phosphorylase]({{< relref "purine_nucleoside_phosphorylase" >}}) converts [Inosine]({{< relref "inosine" >}}) and [guanosine]({{< relref "guanosine" >}}) into their respective purine bases, [hypoxanthine]({{< relref "hypoxanthine" >}}) and [guanine]({{< relref "guanine" >}})
                4.  [Guanine]({{< relref "guanine" >}}) is deaminated to form [xanthine]({{< relref "xanthine" >}})
                5.  Hypoxanthine is oxidized by [xanthine oxidase]({{< relref "xanthine_oxidase" >}}) to [Xanthine]({{< relref "xanthine" >}}) -> further oxidized by xanthine oxidase to [Uric acid]({{< relref "uric_acid" >}})

        <!--list-separator-->

        -  Diseases associated with purine degradation

            <!--list-separator-->

            -  [Gout]({{< relref "gout" >}})

                -   Characterized by high levels of [Uric acid]({{< relref "uric_acid" >}}) in the blood ([hyperuricemia]({{< relref "hyperuricemia" >}})) as a result of either **overproduction** or **underexcretion** of **uric acid**
                -   [Hyperuricemia]({{< relref "hyperuricemia" >}}) -> deposition of monosodium urate crystlas in the joints -> triggers inflammatory response to the crystals -> _gouty arthritis_

                <!--list-separator-->

                -  Underexcretion of [uric acid]({{< relref "uric_acid" >}})

                    -   The cause of the vast majority of [Gout]({{< relref "gout" >}}) cases

                <!--list-separator-->

                -  Overproduction of [uric acid]({{< relref "uric_acid" >}})

                    -   Less common cause of [Gout]({{< relref "gout" >}})
                    -   Mutations in X-linked [PRPP synthetase]({{< relref "prpp_synthetase" >}}) gene -> increased V<sub>max</sub> for the production of PRPP + lower K<sub>m</sub> for ribose 5-phosphate, or decresaed sensitivity to purine nucleotides (allosteric inhibitors)
                        -   Increased availability of PRPP -> **increased purine production** -> **elevated levels of plasma uric acid**


#### [Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP.]({{< relref "outline_the_general_features_of_the_pathways_of_de_novo_synthesis_of_the_two_purine_and_three_pyrimidine_ribonucleotides_noting_the_chemical_changes_required_for_conversion_of_utp_to_ctp_and_imp_to_atp_gtp" >}}) {#outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot--outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot-md}

<!--list-separator-->

-  From [Lippincott's Illustrated Reviews: Biochemistry, 5e]({{< relref "lippincott_s_illustrated_reviews_biochemistry_5e" >}})

    <!--list-separator-->

    -  Synthesis of [purine]({{< relref "purine" >}}) nucleotides (p. 291)

        -   The purine ring is constructed primarily in the [liver]({{< relref "liver" >}})
        -   [Ribose 5-phosphate]({{< relref "ribose_5_phosphate" >}}) from the [PPP]({{< relref "pentose_phosphate_pathway" >}}) is used as a starting molecule

        <!--list-separator-->

        -  Synthesis of [5-phosphoribosyl-1-pyrophosphate]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) (PRPP)

            -   PRPP is an "activated pentose" that participates in the **synthesis and salvage of purines and pyrimidines**
            -   PRPP is synthesized from ATP and ribose 5-phosphate and catalyzed by [PRPP synthetase]({{< relref "prpp_synthetase" >}})
                -   PRPP-synthetase is activated by inorganic phosphate and inhibited by purine nucleotides (end-product inhibition)

        <!--list-separator-->

        -  Synthesis of [5'-phosphoribosylamine]({{< relref "5_phosphoribosylamine" >}})

            -   5'-phosphorybosylamine (PRA) is synthesized from [PRPP]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) and glutamine
            -   **Amide group of glutamine** replaces the pyrophosphate group attached to **carbon 1** of PRPP
            -   Synthesized by [ATase]({{< relref "amidophosphoribosyl_transferase" >}})
                -   ATase inhibited by AMP and GMP (end product inhibition)
            -   **Committed step in purine biosynthesis**
                -   [PRPP]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) controls the rate of this reaction

        <!--list-separator-->

        -  Synthesis of [Inosine monophosphate]({{< relref "inosine_monophosphate" >}}), the "parent" purine nucleotide

            -   The next **nine steps** in purine nucleotide biosynthesis lead to the synthesis of IMP
            -   Requires ATP as an energy source

        <!--list-separator-->

        -  Conversion of [IMP]({{< relref "inosine_monophosphate" >}}) to AMP and GMP

            -   Two-step energy-requiring pathway
            -   Synthesis of **AMP requires GTP** as an energy source
            -   Synthesis of **GMP requires ATP** as an energy source

    <!--list-separator-->

    -  [Pyrimidine]({{< relref "pyrimidine" >}}) synthesis and degradation

        -   The pyrimidine ring is synthesized **before** being attached to [ribose 5-phosphate]({{< relref "ribose_5_phosphate" >}}) (donated by PRPP)

        <!--list-separator-->

        -  Synthesis of [carbamoyl phosphate]({{< relref "carbamoyl_phosphate" >}})

            -   Synthesized from glutamine and carbon dioxide
            -   Catalyzed by [carbamoyl phosphate synthetase II]({{< relref "carbamoyl_phosphate_synthetase_ii" >}})
                -   CPS II is inhibited by UTP (end-product inhibition)


### Unlinked references {#unlinked-references}

[Show unlinked references]
