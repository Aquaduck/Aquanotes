+++
title = "Pacemaker cell"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Cells responsible for the automation of the [Cardiac cycle]({{< relref "cardiac_cycle" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Osmosis]({{< relref "osmosis" >}}) {#osmosis--osmosis-dot-md}

<!--list-separator-->

-  Action potentials in [pacemaker cells]({{< relref "pacemaker_cell" >}})

    -   Note: Phase 1 and 2 are **absent in pacemaker cells** -> **no plateau**

    <!--list-separator-->

    -  Phase 4

        -   _Sodium_ **moves into cell** through [funny channels]({{< relref "funny_channel" >}})
            -   Funny channels open in response to hyperpolarization
        -   **Slowly depolarizes cell** until threshold met
            -   **Responsible for instability of resting membrane potential**

    <!--list-separator-->

    -  Phase 0

        -   **Strong inward** _calcium_ **movement**
        -   Causes **rapid depolarization**

    <!--list-separator-->

    -  Phase 3

        -   **Strong** _potassium_ **current moves out of cell**
            -   Responsible for **repolarization**


### Unlinked references {#unlinked-references}

[Show unlinked references]
