+++
title = "Myofibril"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Cylindrical collections found in [skeletal muscle cells]({{< relref "skeletal_muscle_cell" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) {#sarcoplasmic-reticulum--sarcoplasmic-reticulum-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Forms a meshwork around each [myofibril]({{< relref "myofibril" >}})

    ---


#### [Explain the role of calcium in striated muscle contraction and describe its regulation.]({{< relref "explain_the_role_of_calcium_in_striated_muscle_contraction_and_describe_its_regulation" >}}) {#explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot--explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > A sudden rise in cytosolic Ca<sup>2+</sup> concentration initiates muscle contraction (p. 920)**

    Calcium is then rapidly pumped back into SR by [calcium atpase]({{< relref "calcium_atpase" >}}), an abundant ATP-dependent calcium pump -> [myofibrils]({{< relref "myofibril" >}}) relax

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Structure of Skeletal Muscle (p. 109) > Skeletal muscle cells**

    <!--list-separator-->

    -  [Myofibril]({{< relref "myofibril" >}})

        -   Extend the entire length of the cell
        -   Composed of longitudinally arranged, cylindrical bundles of [thick]({{< relref "thick_myofilament" >}}) and [thin myofilaments]({{< relref "thin_myofilament" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
