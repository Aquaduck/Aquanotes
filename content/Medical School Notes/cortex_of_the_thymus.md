+++
title = "Cortex of the thymus"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Cortex of the [thymus]({{< relref "thymus" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}}) {#describe-the-structure-of-the-thymus--include-changes-with-age-dot----describe-the-structure-of-the-thymus-include-changes-with-age-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus**

    <!--list-separator-->

    -  [Cortex of the thymus]({{< relref "cortex_of_the_thymus" >}})

        {{< figure src="/ox-hugo/_20211009_165706screenshot.png" caption="Figure 1: Cortex of the thymus" width="500" >}}

        -   Many more [lymphocytes]({{< relref "lymphocyte" >}}) than epithelial cells
        -   [T-lymphocytes]({{< relref "t_lymphocyte" >}}) are rearranging their [TCR]({{< relref "t_cell_receptor" >}})
        -   Many of the lymphocytes are unsuccessful -> die and are engulfed by [macrophages]({{< relref "macrophage" >}})
            -   Seen as **white holes in the dark cortex**

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Thymus epithelium**

    Epithelial cells in the [Cortex of the thymus]({{< relref "cortex_of_the_thymus" >}}) assist precursor T-cells to rearrange their [TCR]({{< relref "t_cell_receptor" >}}) genes and become naive T-cells

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus**

    The thymus has a central, pale, [multilobate medulla]({{< relref "medulla_of_the_thymus" >}}) covered by a dark [cortex]({{< relref "cortex_of_the_thymus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
