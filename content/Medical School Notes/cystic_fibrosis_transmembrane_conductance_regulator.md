+++
title = "Cystic fibrosis transmembrane conductance regulator"
author = ["Arif Ahsan"]
date = 2021-10-17T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Rationalize potential drug action with molecular defects and genomics.]({{< relref "rationalize_potential_drug_action_with_molecular_defects_and_genomics" >}}) {#rationalize-potential-drug-action-with-molecular-defects-and-genomics-dot--rationalize-potential-drug-action-with-molecular-defects-and-genomics-dot-md}

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face > Drugs treating cystic fibrosis**

    Potentiator: improves [CFTR]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}}) channel opening -> more ions flow through pore

    ---


#### [Explain how the defect leads to dysfunction in the lungs.]({{< relref "explain_how_the_defect_leads_to_dysfunction_in_the_lungs" >}}) {#explain-how-the-defect-leads-to-dysfunction-in-the-lungs-dot--explain-how-the-defect-leads-to-dysfunction-in-the-lungs-dot-md}

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face**

    <!--list-separator-->

    -  [CF]({{< relref "cystic_fibrosis" >}}) class mutations of [CFTR]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}})

        |                    | Class 1          | Class 2        | Class 3                  | Class 4                  | Class 5          | Class 6           | Class 7     |
        |--------------------|------------------|----------------|--------------------------|--------------------------|------------------|-------------------|-------------|
        | CFTR defect        | No protein       | No traffic     | Impaired gating          | Decreased conductance    | Less protein     | Less stable       | No mRNA     |
        | Corrective therapy | Rescue synthesis | Rescue traffic | Restore channel activity | Restore channel activity | Correct splicing | Promote stability | Unrescuable |

        -   **Note**: Mutation classification is an oversimplification, as most mutations often contain traits of multiple classes

        <!--list-separator-->

        -  Class I Mutations

            -   Near absence of CFTR protein
            -   Mainly stop codon mutations and frameshift mutations -> premature stop codons

        <!--list-separator-->

        -  Class II Mutations

            -   Defective processing and trafficking of the CFTR protein -> degraded in proteasome -> amount of CFTR protein at apical membrane severely reduced

        <!--list-separator-->

        -  Class III Mutations

            -   CFTR protein reaches cell membrane, but defective regulation of CFTR gating -> no channel opening

        <!--list-separator-->

        -  Class IV Mutations

            -   Impaired conductance of the CFTR channel -> fewer ions passing through open channel pore

        <!--list-separator-->

        -  Class V Mutations

            -   Often alternative splice mutations
            -   A reduced amount of normal CFTR protein

        <!--list-separator-->

        -  Class VI Mutations

            -   Unstable CFTR protein -> prematurely recycled from apical membrane -> degraded in lysosomes

        <!--list-separator-->

        -  Class VII Mutations

            -   Large deletions and frameshift mutations not easily amenable to pharmacotherapy


#### [Describe the molecular defect that leads to CF.]({{< relref "describe_the_molecular_defect_that_leads_to_cf" >}}) {#describe-the-molecular-defect-that-leads-to-cf-dot--describe-the-molecular-defect-that-leads-to-cf-dot-md}

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face > Cystic fibrosis**

    [CFTR]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}}) is an anion channel that:

    ---

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face > Cystic fibrosis**

    Cystic fibrosis is caused by mutations in the [cystic fibrosis transmembrane conductance regulator]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
