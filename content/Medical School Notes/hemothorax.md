+++
title = "Hemothorax"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the presentation of pneumothorax, hydrothorax and hemothorax.]({{< relref "describe_the_presentation_of_pneumothorax_hydrothorax_and_hemothorax" >}}) {#describe-the-presentation-of-pneumothorax-hydrothorax-and-hemothorax-dot--describe-the-presentation-of-pneumothorax-hydrothorax-and-hemothorax-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Hemothorax]({{< relref "hemothorax" >}})

        -   Blood enters pleural cavity
        -   Results more commonly from injury to a major intercostal or internal thoracic vessel than from laceration of a lung


### Unlinked references {#unlinked-references}

[Show unlinked references]
