+++
title = "Lightyear: Infinity notes"
author = ["Arif Ahsan"]
date = 2021-09-07T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Biochemistry {#biochemistry}


### The [GLUT-2]({{< relref "glut_2" >}}) transporter is bidirectional {#the-glut-2--glut-2-dot-md--transporter-is-bidirectional}

-   Functions as a **glucose sensor** - high capacity but low affinity (Km)
-   Insulin-**independent**


### The three activators of [Glycogen synthase]({{< relref "glycogen_synthase" >}}) {#the-three-activators-of-glycogen-synthase--glycogen-synthase-dot-md}

-   G6P
-   Insulin
-   Cortisol


### Activation of [Pyruvate DH]({{< relref "pyruvate_dehydrogenase_complex" >}}): {#activation-of-pyruvate-dh--pyruvate-dehydrogenase-complex-dot-md}

-   NAD+/NADH
-   ADP
-   Calcium


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
