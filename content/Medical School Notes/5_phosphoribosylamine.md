+++
title = "5'-phosphoribosylamine"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP.]({{< relref "outline_the_general_features_of_the_pathways_of_de_novo_synthesis_of_the_two_purine_and_three_pyrimidine_ribonucleotides_noting_the_chemical_changes_required_for_conversion_of_utp_to_ctp_and_imp_to_atp_gtp" >}}) {#outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot--outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Synthesis of purine nucleotides (p. 291)**

    <!--list-separator-->

    -  Synthesis of [5'-phosphoribosylamine]({{< relref "5_phosphoribosylamine" >}})

        -   5'-phosphorybosylamine (PRA) is synthesized from [PRPP]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) and glutamine
        -   **Amide group of glutamine** replaces the pyrophosphate group attached to **carbon 1** of PRPP
        -   Synthesized by [ATase]({{< relref "amidophosphoribosyl_transferase" >}})
            -   ATase inhibited by AMP and GMP (end product inhibition)
        -   **Committed step in purine biosynthesis**
            -   [PRPP]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) controls the rate of this reaction


### Unlinked references {#unlinked-references}

[Show unlinked references]
