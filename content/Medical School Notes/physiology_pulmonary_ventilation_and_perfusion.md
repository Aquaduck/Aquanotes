+++
title = "Physiology, Pulmonary Ventilation and Perfusion"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

**Warning:** The NCBI web site requires JavaScript to function. [more...](/guide/browsers/#enablejs)

NCBI Bookshelf. A service of the National Library of Medicine, National Institutes of Health.

StatPearls \\[Internet\\]. Treasure Island (FL): StatPearls Publishing; 2021 Jan-.


## Physiology, Pulmonary Ventilation and Perfusion {#physiology-pulmonary-ventilation-and-perfusion}

<!--list-separator-->

-  Authors

    Kyle A. Powers<sup>1</sup>; Amit S. Dhamoon<sup>2</sup>.

<!--list-separator-->

-  Affiliations

    ^{1} Upstate Medical University

    ^{2} SUNY Upstate Medical University

    Last Update: March 24, 2021.


### Introduction {#introduction}

One of the major roles of the lungs is to facilitate gas exchange between the circulatory system and the external environment. The lungs are composed of branching airways that terminate in respiratory bronchioles and alveoli, which participate in gas exchange. Most bronchioles and large airways are part of the conducting zone of the lung, which delivers gas to sites of gas exchange in alveoli. Gas exchange occurs in the lungs between alveolar air and blood of the pulmonary capillaries. For effective gas exchange to occur, alveoli must be ventilated and perfused. [Ventilation]({{< relref "ventilation" >}}) (V) refers to the flow of air into and out of the alveoli, while [perfusion]({{< relref "perfusion" >}}) (Q) refers to the flow of blood to alveolar capillaries. Individual alveoli have variable degrees of ventilation and perfusion in different regions of the lungs. Collective changes in ventilation and perfusion in the lungs are measured clinically using the ratio of ventilation to perfusion (V/Q). Changes in the V/Q ratio can affect gas exchange and can contribute to hypoxemia.


### Cellular {#cellular}

Gas exchange occurs in the respiratory zone of the lung, where alveoli are present. The respiratory zone of the lung includes respiratory bronchioles, alveolar ducts, alveolar sacs, and alveoli. Thin alveolar septa separate adjacent alveoli. Adjacent alveoli have connections via small openings, called pores of Kohn, that allow for collateral airflow and equalization of pressure between alveoli. The control of opening or closing of alveoli to regulate ventilation occurs at the alveolar duct.

The alveolar septum has numerous capillaries and thin walls for gas exchange. In addition to capillary endothelial cells, the alveolar septum contains type I pneumocytes that are very thin and line the alveoli, as well as type II pneumocytes that secrete dipalmitoylphosphatidylcholine (DPPT) surfactant to decrease alveolar surface tension. Alveolar macrophages, also known as dust cells, are active in defending against pathogens and irritants.

Gas exchange in the alveoli occurs primarily by diffusion. Traveling from the alveoli to capillary blood, gases must pass through alveolar surfactant, alveolar epithelium, basement membrane, and capillary endothelium. According to Fick’s law of diffusion, diffusion of a gas across the alveolar membrane increases with:

-   Increased surface area of the membrane

-   Increased alveolar pressure difference (PA-Pa)

-   Increased solubility of the gas

-   Decreased membrane thickness

The exchange of both oxygen and carbon dioxide is perfusion limited. Diffusion of gases reaches equilibrium one-third of the way through the capillary/alveolar interface. Deoxygenated blood from the pulmonary arteries has a PVO2 of 40 mmHg, and alveolar air has a PAO2 of 100 mmHg, resulting in a movement of oxygen into capillaries until arterial blood equilibrates at 100 mmHg (PaO2). Meanwhile, carbon dioxide partial pressure decreases from a PVCO2 of 46 mmHg to a PaCO2 of 40 mmHg in alveolar capillaries due to a PACO2 of 40 mmHg.


### Organ Systems Involved {#organ-systems-involved}

-   Cardiovascular system
    -   Pulmonary circulation

    -   Alveolar capillaries

<!--listend-->

-   Respiratory system
    -   Alveoli

    -   Alveolar sacs

    -   Alveolar ducts

    -   Respiratory bronchioles


### Mechanism {#mechanism}

The V/Q ratio evaluates the matching of ventilation (V) to perfusion (Q). There is regional variation in the V/Q ratio within the lung. Ventilation is 50% greater at the base of the lung than at the apex. The weight of fluid in the pleural cavity increases the intrapleural pressure at the base to a less negative value. As a result, alveoli are less expanded and have higher compliance at the base, resulting in a more substantial increase in volume on inspiration for increased ventilation. Perfusion is also greater at the base of the lung due to gravity pulling blood down towards the base. Overall, perfusion increases more than ventilation at the base of the lung, resulting in lower V/Q ratios in the base of the lung compared to the apex. In a healthy individual, the V/Q ratio is 1 at the middle of the lung, with a minimal spread of V/Q ratios from 0.3 to 2.1 from base to apex.[\\[1\\](https://www.ncbi.nlm.nih.gov/books/NBK539907/?report=printable#)] In cases of high V/Q ratios, PO2 increases and PCO2 decreases as alveolar air more closely matches the larger volume of inspired air than perfused blood.[\\[2\\](https://www.ncbi.nlm.nih.gov/books/NBK539907/?report=printable#)] On the other hand, low V/Q ratios result in a decreased PO2 and an increased PCO2.


### Related Testing {#related-testing}

Clinically, the diffusion capacity of the lung (DLCO) is measured using low concentrations of carbon monoxide. DLCO can be calculated by the equation VCO = DLCO (PACO – PaCO). PaCO is approximately zero due to hemoglobin binding so that the equation can rearrange to DLCO = VCO / PACO. To measure DLCO, low CO content air is breathed for 10 seconds, with the flow of CO into and out of the lungs being measured to calculate VCO.[\\[3\\](https://www.ncbi.nlm.nih.gov/books/NBK539907/?report=printable#)] Measurement of PACO allows for the calculation of DLCO, with a normal value of DLCO being 25 mL/min/mmHg.

DLCO is related to the diffusion coefficient (D) of carbon monoxide, the alveolar area (A), and the alveolar thickness (T) according to the equation DLCO = D x A / T. Because D is a constant, the value of DLCO provides insight into the alveolar area and thickness that impacts gas exchange. Also, the diffusion of other gases can be calculated at different pressure conditions by correcting for their different diffusion coefficients.

The V/Q ratio of the lung is measurable by the multiple inert gas elimination technique (MIGET). The lung divides into a number of compartments, and a mixture of 6 gases is inhaled.[\\[4\\](https://www.ncbi.nlm.nih.gov/books/NBK539907/?report=printable#)] The V/Q of each compartment can be calculated using the expiration of the different gases and mathematical models.


### Pathophysiology {#pathophysiology}

Systemic circulation to the lungs takes place via the bronchial circulation, which ultimately drains into the pulmonary vein. This normal left-to-left anatomic shunt results in a slight drop in PaO2 from 100 mmHg at the end of pulmonary capillaries to 95 mmHg in the pulmonary vein. The A-a gradient typically measures this slight drop in oxygen partial pressure from the alveoli (PAO2) to the pulmonary vein (PaO2) due to shunting, which may increase in some pathological conditions. Right to left shunts may further reduce PaO2 to cause hypoxemia and exhibit an increased A-a gradient. Diffusion problems may also lead to an increased A-a gradient because arterial blood may not reach equilibrium with alveolar air due to diffusion-limited gas exchange.

Physiological conditions can cause extreme V/Q ratios outside of the normal range. In a right-to-left shunt, a portion of the pulmonary blood flow is shunted away from alveoli, resulting in ventilation without perfusion and a higher V/Q ratio. Although diffusion between capillaries and alveoli is unaffected, the arterial PO2 is decreased from the lack of ventilation of the shunted blood, resulting in an increased A-a gradient. The arterial PCO2 is increased from a lack of gas exchange as well. Because of the plateau in the hemoglobin oxygen-binding curve, a small change in oxygen content of the arterial blood causes a significant shift in PO2. As a result, right-to-left shunts result in more hypoxemia than hypercapnia. Generally, increasing FiO2 through supplemental oxygen therapy does not improve hypoxemia in patients with right-to-left shunts. The increased oxygen content of the inspired air never reaches the shunted blood for gas exchange.

Alveolar dead space occurs when some alveoli are not ventilated, resulting in a low V/Q ratio. Alveolar dead space increases the total physiological dead space, decreasing alveolar ventilation; this results in a decreased V/Q ratio and decreases PAO2 for functional alveoli. Hypoxemia results from the reduced PAO2, which may be corrected by oxygen therapy to increase the PAO2 of functioning alveoli.


### Clinical Significance {#clinical-significance}

A number of conditions can cause right-to-left shunts that cause a V/Q mismatch. At a microscopic level, pulmonary arteriovenous malformations provide a route from arterial to venous blood in the pulmonary circulation that bypasses the pulmonary capillaries where gas exchange occurs. Congenital heart defects can cause right-to-left shunts at a macroscopic level. In the case of ventricular septal defects, the right ventricle may hypertrophy to the point that the right ventricle has a higher pressure during systole than the left ventricle, causing blood to flow from the right to left ventricle, bypassing the pulmonary circulation. Physiologic right-to-left shunts may also occur if perfusion reaches areas of the lungs that are not ventilated, which may result from airway obstructions, pulmonary edema, and pneumonia. The effect of physiologic right-to-left shunts is minimized by hypoxic vasoconstriction in the pulmonary circulation, which redirects blood flow to better-ventilated areas of the lungs for more efficient exchange.

Asthma is often referred to as a “false shunt” because bronchoconstriction decreases ventilation, resulting in a low V/Q ratio, as occurs in alveolar dead space. In the case of asthma, oxygen therapy is indicated because some ventilation of the bronchoconstricted alveoli still occurs, and oxygen therapy increases the PAO2 of alveoli with obstructed airflow. Treatment with a bronchodilator such as a beta-2 agonist is more beneficial to patients with asthma than oxygen therapy because of its reduction of bronchoconstriction.

V/Q mismatches can occur in the case of pulmonary embolism (PE). Emboli may restrict blood flow in the pulmonary circulation, resulting in alveoli that are ventilated but not perfused; this results in an increased V/Q ratio and decreased gas exchange. The impaired gas exchange may cause hypoxemia in cases of PE.[\\[5\\](https://www.ncbi.nlm.nih.gov/books/NBK539907/?report=printable#)]

Inspiratory hypoxia, as occurs at high altitudes, can cause a V/Q mismatch and affect blood gases. The decreased atmospheric pressure at altitude causes a decreased PAO2. However, normal diffusion occurs (normal A-a gradient), the arterial PaO2 decreases, and hypoxemia results. Oxygen therapy can correct the hypoxemia in this instance because the inspired air increases the PAO2 back to normal levels.

Intrinsic diffusion barriers between alveoli and capillaries can result in hypoxemia. Although PAO2 is normal, impaired diffusion results in a decreased PaO2 and increased A-a gradient. Patients with diffusion problems have more hypoxemia than hypercapnia, and oxygen therapy improves hypoxemia in diffusion deficits. Supplemental oxygen increases PAO2, resulting in an increased oxygen gradient contributing to diffusion. Even with oxygen therapy, there is still an increased A-a gradient.

A variety of conditions can affect diffusion, which can be clinically assessed using DLCO. In lung fibrosis, the thickening of lung tissue increases the alveolar wall thickness, decreasing DLCO.[\\[3\\](https://www.ncbi.nlm.nih.gov/books/NBK539907/?report=printable#)] DLCO is also decreased in emphysema, in this case, due to the destruction of alveoli decreasing the area for gas exchange.[\\[3\\](https://www.ncbi.nlm.nih.gov/books/NBK539907/?report=printable#)] The decreased diffusion in emphysema sometimes causes diffusion-limited oxygen exchange during exercise, as increased heart rate reduces the time blood spends in alveolar capillaries for gas exchange.

A significant cause of diffusion problems is pulmonary edema, as fluid in the lungs increases the effective thickness of the alveolar wall and decreases the area of gas exchange. Pulmonary edema results in greater hypoxemia than hypercapnia because carbon dioxide can more easily dissolve into the fluid to reach the alveolar membrane for exchange. The edema prevents air from reaching pulmonary capillaries, resulting in perfusion without ventilation and alveolar dead space.

Pulmonary edema has several causes, most of which affect Starling forces to increase filtration at alveolar capillaries. Left-sided heart failure may increase left atrial pressure, which in turn can result in increased capillary hydrostatic pressure to cause pulmonary edema. Over-administration of IV fluids may similarly increase capillary hydrostatic pressure to cause edema. Acute respiratory distress syndrome (ARDS) and sepsis can cause increased capillary permeability to cause pulmonary edema. Decreases in capillary osmotic pressure can also cause pulmonary edema, as occurs in nephrotic syndrome and liver failure. Pulmonary edema may also result from obstructed lymphatic drainage of filtered fluid, as may occur with tumors.


### Review Questions {#review-questions}

-   [Access free multiple choice questions on this topic.](https://www.statpearls.com/account/trialuserreg/?articleid=796&utm%5Fsource=pubmed&utm%5Fcampaign=reviews&utm%5Fcontent=796)

-   [Comment on this article.](https://www.statpearls.com/articlelibrary/commentarticle/796/?utm%5Fsource=pubmed&utm%5Fcampaign=comments&utm%5Fcontent=796)


### References {#references}

1.

Wagner PD, Laravuso RB, Uhl RR, West JB. Continuous distributions of ventilation-perfusion ratios in normal subjects breathing air and 100 per cent O2. J Clin Invest. 1974 Jul;54(1):54-68. \\[[[https://www.ncbi.nlm.nih.gov/pmc/articles/PMC301524/][PMC free article: PMC301524]]\\] \\[[[https://www.ncbi.nlm.nih.gov/pubmed/4601004][PubMed: 4601004]]\\]

1.

Petersson J, Glenny RW. Gas exchange and ventilation-perfusion relationships in the lung. Eur Respir J. 2014 Oct;44(4):1023-41. \\[[[https://www.ncbi.nlm.nih.gov/pubmed/25063240][PubMed: 25063240]]\\]

1.

Enright Md P. Office-based DLCO tests help pulmonologists to make important clinical decisions. Respir Investig. 2016 Sep;54(5):305-11. \\[[[https://www.ncbi.nlm.nih.gov/pubmed/27566377][PubMed: 27566377]]\\]

1.

Wagner PD. The multiple inert gas elimination technique (MIGET). Intensive Care Med. 2008 Jun;34(6):994-1001. \\[[[https://www.ncbi.nlm.nih.gov/pubmed/18421437][PubMed: 18421437]]\\]

1.

Huet Y, Lemaire F, Brun-Buisson C, Knaus WA, Teisseire B, Payen D, Mathieu D. Hypoxemia in acute pulmonary embolism. Chest. 1985 Dec;88(6):829-36. \\[[[https://www.ncbi.nlm.nih.gov/pubmed/4064770][PubMed: 4064770]]\\]

[Copyright](https://www.ncbi.nlm.nih.gov/books/about/copyright/) © 2021, StatPearls Publishing LLC.

This book is distributed under the terms of the Creative Commons Attribution 4.0 International License ([http://creativecommons.org/licenses/by/4.0/](http://creativecommons.org/licenses/by/4.0/)), which permits use, duplication, adaptation, distribution, and reproduction in any medium or format, as long as you give appropriate credit to the original author(s) and the source, a link is provided to the Creative Commons license, and any changes made are indicated.

Bookshelf ID: NBK539907PMID: [30969729](https://www.ncbi.nlm.nih.gov/pubmed/30969729)

External link. Please review our [privacy policy](https://www.nlm.nih.gov/privacy.html).

<https://www.ncbi.nlm.nih.gov/stat?HTTP%5FREFERER=https%3A%2F%2Fwww.ncbi.nlm.nih.gov%2Fbooks%2FNBK539907%2F&SELF%5FURL=https%3A%2F%2Fwww.ncbi.nlm.nih.gov%2Fbooks%2FNBK539907%2F%3Freport%3Dprintable&browserheight=793&browserwidth=1426&colorDepth=24&connection%5Feffectivetype=&connection%5Ftype=&cookieSize=581&cookieenabled=true&is%5Fbrowser%5Fsupported=true&jsevent=render&jsloadtime=231&jsperf%5FbasePage=9&jsperf%5Fconnect=0&jsperf%5Fdns=0&jsperf%5FnavType=2&jsperf%5FredirectCount=0&jsperf%5Fttfb=59&jsrendertime=248&myncbi%5Fsigned%5Fin=false&ncbi%5Facc=NBK539907&ncbi%5Falgorithm=&ncbi%5Fapp=bookshelf&ncbi%5Fdb=books&ncbi%5Fdomain=statpearls&ncbi%5Ffeatured%5Fsrcdb=&ncbi%5Fnwds=&ncbi%5Fobjectid=&ncbi%5Fpcid=%2FNBK539907%2F%3Freport%3Dprintable&ncbi%5Fpdid=book-part&ncbi%5Fphid=CE8DEFAC165DCBE1000000000419011B.m%5F4&ncbi%5Fprogram=&ncbi%5Freport=printable&ncbi%5Ftimesinceload=16&ncbi%5Ftimesincenavstart=619&ncbi%5Ftype=fulltext&pagename=bookshelf%3Abooks%3Abook-part%3A%2FNBK539907%2F%3Freport%3Dprintable&prev%5Fphid=CE899341165D27610000000006C801CF.m%5F4&screenavailheight=878&screenavailwidth=1600&screenheight=900&screenwidth=1600&server=www.ncbi.nlm.nih.gov&sgSource=native&sgversion=0.31.1&sgversion%5Fhotfix=1&sgversion%5Fmajor=0&sgversion%5Fminor=31&spa%5Findex=0><https://www.ncbi.nlm.nih.gov/stat?browserheight=793&browserwidth=1426&canscroll%5Fx=false&canscroll%5Fy=true&eventid=0&jsevent=unloadnext&jsperf%5Fconnect=61&jsperf%5Fdns=1&jsperf%5Fttfb=316&ncbi%5Fapp=bookshelf&ncbi%5Fdb=books&ncbi%5Fpcid=%2FNBK539908%2F%3Freport%3Dprintable&ncbi%5Fpdid=book-part&ncbi%5Fphid=CE899341165D27610000000006C801CF.m%5F4&ncbi%5Freport=printable&ncbi%5Ftimeonpage=&ncbi%5Ftimesinceload=3367&ncbi%5Ftimesincenavstart=4362&next%5Fphid=CE8DEFAC165DCBE1000000000419011B.m%5F4&pagename=bookshelf%3Abooks%3Abook-part%3A%2FNBK539908%2F%3Freport%3Dprintable&screenavailheight=878&screenavailwidth=1600&screenheight=900&screenwidth=1600&server=www.ncbi.nlm.nih.gov&sgversion=0.31.1>

</stat?jsdisabled=true&ncbi_db=books&ncbi_pdid=book-part&ncbi_acc=NBK539907&ncbi_domain=statpearls&ncbi_report=printable&ncbi_type=fulltext&ncbi_objectid=&ncbi_pcid=/NBK539907/?report=printable&ncbi_app=bookshelf>

<https://www.ncbi.nlm.nih.gov/stat?browserheight=793&browserwidth=1426&colorDepth=24&connection%5Feffectivetype=&connection%5Ftype=&cookieSize=217&cookieenabled=true&is%5Fbrowser%5Fsupported=true&jsevent=domready&myncbi%5Fsigned%5Fin=false&ncbi%5Facc=NBK539907&ncbi%5Falgorithm=&ncbi%5Fapp=bookshelf&ncbi%5Fdb=books&ncbi%5Fdomain=statpearls&ncbi%5Ffeatured%5Fsrcdb=&ncbi%5Fnwds=&ncbi%5Fobjectid=&ncbi%5Fpcid=%2FNBK539907%2F%3Freport%3Dprintable&ncbi%5Fpdid=book-part&ncbi%5Fphid=CE8DEFAC165DCBE1000000000419011B.m%5F4&ncbi%5Fprogram=&ncbi%5Freport=printable&ncbi%5Ftimesinceload=108&ncbi%5Ftimesincenavstart=709&ncbi%5Ftype=fulltext&pagename=bookshelf%3Abooks%3Abook-part%3A%2FNBK539907%2F%3Freport%3Dprintable&screenavailheight=878&screenavailwidth=1600&screenheight=900&screenwidth=1600&server=www.ncbi.nlm.nih.gov&sgSource=native&sgversion=0.31.1&sgversion%5Fhotfix=1&sgversion%5Fmajor=0&sgversion%5Fminor=31&spa%5Findex=0>

Making content easier to read in Bookshelf Close

We are experimenting with display styles that make it easier to read books and documents in Bookshelf. Our first effort uses ebook readers, which have several "ease of reading" features already built in.

The content is best viewed in the _iBooks reader_ . You may notice problems with the display of some features of books or documents in other eReaders.

Cancel Download


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
