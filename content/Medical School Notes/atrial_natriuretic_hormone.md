+++
title = "Atrial natriuretic hormone"
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Antagonistic to [Angiotensin II]({{< relref "angiotensin_ii" >}})
-   Suppresses [Renin]({{< relref "renin" >}}), [Aldosterone]({{< relref "aldosterone" >}}), and [ADH]({{< relref "antidiuretic_hormone" >}}) production/release
-   Increases glomular filtration rate (GFR) by vasodilation of afferent arteriole + vasoconstriction of efferent arteriole


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  [Atrial Natriuretic Hormone]({{< relref "atrial_natriuretic_hormone" >}}) (p. 928)

    -   Secreted by cells in the atria of the heart when high blood volume -> extreme stretching of cardiac cells
    -   ANH is antagonistic to angiotensin II
        -   Promote **loss of sodium and water from the kidneys**
        -   Suppress renin, aldosterone, and ADH production/release
        -   **Results in drop of blood volume and blood pressure**


### Unlinked references {#unlinked-references}

[Show unlinked references]
