+++
title = "Osteon"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Composed of multiple concentric [Lamellae]({{< relref "lamellae" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Appositional growth and remodeling**

    In an adult, intact [osteons]({{< relref "osteon" >}}) have a half-life of many years

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    The result of the [Bone-remodeling unit]({{< relref "bone_remodeling_unit" >}}) is known as an [Osteon]({{< relref "osteon" >}}) (haversian system) with a central [Haversian canal]({{< relref "haversian_canal" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Histologic components of mature bone**

    Dense cortical bone between internal and external lamellae consists of [osteons]({{< relref "osteon" >}})

    ---


#### [Haversian canal]({{< relref "haversian_canal" >}}) {#haversian-canal--haversian-canal-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    In the center of the [Haversian system]({{< relref "osteon" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
