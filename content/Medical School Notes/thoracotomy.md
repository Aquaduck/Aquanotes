+++
title = "Thoracotomy"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the procedures associated with intercostal nerve anesthesia and thoracotomy and their indications.]({{< relref "describe_the_procedures_associated_with_intercostal_nerve_anesthesia_and_thoracotomy_and_their_indications" >}}) {#describe-the-procedures-associated-with-intercostal-nerve-anesthesia-and-thoracotomy-and-their-indications-dot--describe-the-procedures-associated-with-intercostal-nerve-anesthesia-and-thoracotomy-and-their-indications-dot-md}

<!--list-separator-->

-  **🔖 From Accessing an Intercostal Space**

    <!--list-separator-->

    -  [Thoracotomy]({{< relref "thoracotomy" >}})

        -   _Thoracotomy_: the surgical creation of an opening throuhg the thoracic wall to enter a pleural cavity
        -   **Posterolateral aspects of 5th and 7th intercostal spaces are important sites for posterior thoracotomy incisions**
        -   **Often used to treat or diagnose a problem with the lungs or heart**
            -   Most commonly done for lung cancer

        <!--list-separator-->

        -  Procedure

            -   With pt lying on contralateral side, fully abduct upper limb, placing forearm beside pt's head
                -   This elevates and laterally rotates inferior angle of scapula -> allows access as high as 4th intercostal space
                -   Most commonly, rib retraction allows procedures to be performed through a single intercostal space, with care to avoid the superior neurovascular bundle
            -   If **wider exposure is required**, surgeons use an **H-shaped incision** -> incise superficial aspect of periosteum that ensheaths rib, strip periosteum from rib, and excise a wide segment of rib to gain better access


### Unlinked references {#unlinked-references}

[Show unlinked references]
