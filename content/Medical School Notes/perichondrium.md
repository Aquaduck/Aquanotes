+++
title = "Perichondrium"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## From Amboss {#from-amboss}

-   Connective tissue composed of an outer fibrous layer and an inner chondrogenic layer, which surrounds elastic and extra-articular cartilage
    -   Outer fibrous layer contains blood vessels, lymphatics, and nerves to provide nutrients to and drain the cartilage
        -   Cartilage not surrounded by perichondrium is supplied by synovial fluid and a subchondral vascular network
    -   Inner chondrogenic layer contains progenitor cells -> regenerate cartilage
-   Found in [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) and [Elastic cartilage]({{< relref "elastic_cartilage" >}})
-   Dense irregular [CT]({{< relref "connective_tissue" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Joints, ligaments, and tendons > Joints**

    Contacting cartilage surfaces lack a [Perichondrium]({{< relref "perichondrium" >}}), but are lubricated by synovial cells at the side of the join

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
