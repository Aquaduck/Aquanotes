+++
title = "Fructose bisposphatase-2"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Fructose-2,6-bisphosphate (feed-forward regulation)]({{< relref "fructose_2_6_bisphosphate_feed_forward_regulation" >}}) {#fructose-2-6-bisphosphate--feed-forward-regulation----fructose-2-6-bisphosphate-feed-forward-regulation-dot-md}

<!--list-separator-->

-  **🔖 Synthesis**

    <!--list-separator-->

    -  [PFK-2]({{< relref "pfk_2" >}}) + [FBPase-2]({{< relref "fructose_bisposphatase_2" >}})

        -   Two domains of the same bifunctional enzyme


### Unlinked references {#unlinked-references}

[Show unlinked references]
