+++
title = "Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}}) {#from-lymphatic-and-immune-systems-ppt--lymphatic-and-immune-systems-ppt-dot-md}


### [Paracortex]({{< relref "paracortex_of_the_lymph_node" >}}) {#paracortex--paracortex-of-the-lymph-node-dot-md}

-   [Dendritic cells]({{< relref "dendritic_cell" >}}) and antigen enter the node throug the afferent lymphatics
-   [Lymphocytes]({{< relref "lymphocyte" >}}) enter through the [HEV]({{< relref "high_endothelial_venule" >}})
-   These two cell types interact -> testing to see if any [T-cells]({{< relref "t_lymphocyte" >}}) recognize the peptide displayed by the [dendritic cells]({{< relref "dendritic_cell" >}})
    -   If the T-cell **fails to recognize** any antigen peptide -> enters a sinus and exits through the efferent lymphatic -> returns to blood
        -   A lymphocyte may complete this cycle as often as 10 times per day in nodes throughout the body
    -   If a T-cell **recognizes** a peptide while in the paracortex -> enters activation stage -> divides about 10 times -> becomes an effector cell
        -   Specific function depends on the type of T-cell and signals from the innate system


### [Follicles]({{< relref "follicle_of_the_lymph_node" >}}) {#follicles--follicle-of-the-lymph-node-dot-md}

-   [B-cells]({{< relref "b_lymphocyte" >}}) waiting to recognize antigen flowing in the lymph are located in the cortex in [primary follicles]({{< relref "primary_follicle" >}})
    -   B-cells engulf antigen that binds their receptor -> display peptide form the antigen on their surface
        -   If a previously activated [T-cell]({{< relref "t_lymphocyte" >}}) from the paracortex recognizes the peptide -> T-cell helps B-cell form a [germinal center]({{< relref "germinal_center" >}}) (i.e. [secondary follicle]({{< relref "secondary_follicle" >}})) and try to **make a more specific antibody through hypermutation of the Ig gene**
        -   Unsuccessful B-cells undergo apoptosis
        -   B-cells producing a highly specific [Ig]({{< relref "immunoglobulin" >}}) become plasma cells -> leave lymph node or enter medullary cords -> produce abundant Ig


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon] </span></span> > Lymphatic and Immune Systems > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
