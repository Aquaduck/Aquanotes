+++
title = "Gluconeogenesis"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   Gluconeogenesis is a [metabolic]({{< relref "metabolism" >}}) process that converts pyruvate back into glucose


## Backlinks {#backlinks}


### 18 linked references {#18-linked-references}


#### [Osmosis - Gluconeogenesis]({{< relref "osmosis_gluconeogenesis" >}}) {#osmosis-gluconeogenesis--osmosis-gluconeogenesis-dot-md}

<!--list-separator-->

-  Alcoholism affect on [gluconeogenesis]({{< relref "gluconeogenesis" >}})

    -   Ethanol processed in liver cells -> over time, results in accumulation of NADH and ATP in cytoplasm
        -   Increased NADH -> signal the lactic, malate, and glycerol-3-phosphate DH [enzymes]({{< relref "enzyme" >}}) in gluconeogenesis to go in reverse
        -   High ATP -> slows down fatty acid beta oxidation -> abnormal accumulation of fat in liver
    -   Both impair gluconeogenesis -> chronic low blood sugar when fasting

<!--list-separator-->

-  [Notes]({{< relref "gluconeogenesis" >}})

    -   Body maintains glucose levels during fasting using **gluconeogenesis**
    -   Primarily takes place in **liver**, but can also happen in **epithelial cells of kidney and intestines**
        -   Specifically in **cytoplasm, mitochondria, and ER** of cells found in these tissues
    -   Extra glucose stored in liver cells as _glycogen_ - glucose molecules strung together
    -   **RBCs and brain can only metabolize glucose**
    -   Liver gets energy **exclusively from burning fat**
        -   As soon as its **ATP levels are high enough -> release glucose into blood**
    -   By 12 hours of fasting, gluconeogensis is the main provider of glucose in the bloodstream

    <div class="table-caption">
      <span class="table-number">Table 1</span>:
      Glycogenolysis vs. Gluconeogenesis
    </div>

    | Glycogenolysis                      | Gluconeogenesis                                     |
    |-------------------------------------|-----------------------------------------------------|
    | Breaks down **glycogen** -> glucose | **glucose from scratch**                            |
    | **12-24 hours** of fasting          | Main provider of glucose **by 12 hours of fasting** |

    -   7 reactions in glycolysis are reversible

        -   3 are reversible -> **needs bypass**

        {{< figure src="/ox-hugo/_20210729_175427screenshot.png" caption="Figure 1: Gluconeogenesis vs. Glycolysis" width="500" >}}

    <!--list-separator-->

    -  Sources of pyruvate

        -   Two main **sources of pyruvate** are **lactate** and **amino acids like alanine**
            -   _Lactate_ produced as a byproduct of anaerobic respiration in RBCs and exercising skeletal muscle cells
                -   _Lactate DH_ removes a hydrogen from lactate -> pyruvate

                    {{< figure src="/ox-hugo/_20210729_175851screenshot.png" width="500" >}}
            -   18/20 [amino acids]({{< relref "amino_acid" >}}) are _glucogenic_ - can be used to make glucose
                -   Leucine and Lysine cannot - "The lazy Ls"
                -   When fasting for a long time -> breakdown of protein in skeletal muscle into AAs
                -   AA attaches to \\(\alpha\\)-ketoglutarate -> glutamate
                -   Transaminase [enzymes]({{< relref "enzyme" >}}) require _pyridoxine ([vitamin B<sub>6</sub>]({{< relref "pyridoxine" >}}))_ as a cofactor

                    {{< figure src="/ox-hugo/_20210729_180304screenshot.png" caption="Figure 2: Alanine metabolism in skeletal muscle" width="500" >}}

    <!--list-separator-->

    -  Fat metabolism

        -   After fasting for a long time -> body breaks down fats (triacylglycerides)
            -   _Pancreatic \\(\alpha\\) cells_ sense blood glucose levels decreasing -> release [glucagon]({{< relref "glucagon" >}})
            -   Low levels of insulin, presence of epinephrine and ACTH -> adipocytes stimulate [hormone-sensitive lipase]({{< relref "enzyme" >}}) (HSL) -> break down triacylgyceride into 3 fatty acids + glycerol
            -   Fatty acids enter bloodstream -> enter hepatocyte mitochondria -> broken down into Acetyl-CoA + ATP via beta oxidation

    <!--list-separator-->

    -  Gluconeogenesis steps

        1.  Convert pyruvate to PEP **alternate route**

            -   Involves 3 enzymes: _pyruvate carboxylase_, _malate dehydrogenase_, and _PEP carboxykinase (PEPCK)_
                -   _Pyruvate carboxylase_ has 3 cofactors: (ABCs)
                    1.  \*A\*TP
                    2.  \*B\*iotin
                    3.  \*C\*arbon dioxide
            -   First converted to oxaloacetate -> **can't go through mitochondrial membrane** -> **Malate shuttle**

            {{< figure src="/ox-hugo/_20210729_181104screenshot.png" width="500" >}}

            {{< figure src="/ox-hugo/_20210729_181334screenshot.png" caption="Figure 3: Malate shuttle" width="500" >}}

            -   Once oxaloacetate is in the cytoplasm -> _PEPCK_ removes a carbon group + adds phosphate group -> formation of PEP
                -   Uses **GTP** as energy source
                -   _"stress" hormones_ enhance activity of _PEPCK_ via induction
                    -   Glucagon
                    -   Epinephrine
                    -   Cortisol
        2.  Reversible reactions **until its converted to DHAP**
            -   Alternatively, _glycerol_ from HSL activity used to make DHAP

                {{< figure src="/ox-hugo/_20210729_183625screenshot.png" caption="Figure 4: Pathway from glycerol to DHAP" width="500" >}}
        3.  DHAP converted to fructose-1,6-bisphosphate by a reversible reaction via _[aldolase]({{< relref "enzyme" >}})_
        4.  Fructose-1,6-bisphosphate -> fructose-6-phosphate via _[fructose-1,6-bisphosphatase]({{< relref "enzyme" >}})_
            -   **RATE-LIMITING STEP**
            -   Highly regulated

                {{< figure src="/ox-hugo/_20210729_184345screenshot.png" caption="Figure 5: Conversion between Fructose-1,6-bisphosphate and fructose-6-phosphate in glycolysis and gluconeogenesis" width="500" >}}
        5.  Isomerize F6P -> G6P

            {{< figure src="/ox-hugo/_20210729_184517screenshot.png" caption="Figure 6: Fructose-6-phosphate isomerized to glucose-6-phosphate via _isomerase_" width="500" >}}
        6.  Remove phosphate off G6P -> allow glucose to leave cell
            -   **phosphate group is what keeps glucose inside cell**
            -   Performed by _[glucose-6-phosphatase]({{< relref "enzyme" >}})_ in the endoplasmic reticulum

<!--list-separator-->

-  [Summary]({{< relref "gluconeogenesis" >}})

    -   Essentially the reverse of [glycolysis]({{< relref "glycolysis" >}}) with _3 irreversible reactions_ bypassed with unique enzymes
        -   Pyruvate kinase bypassed by _pyruvate carboxylase_, _malate dehydrogenase_, and _PEPCK_
        -   PFK1 bypassed by _fructose-1,6-bisphosphatase_
        -   Hexokinase bypassed by _glucose-6-phosphate_
    -   Production of glucose from pyruvate to help maintain steady blood glucose levels during prolonged fasts


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Insulin's metabolic effects**

    Restores [pyruvate kinase]({{< relref "pyruvate_kinase" >}}) activity -> stops [gluconeogenesis]({{< relref "gluconeogenesis" >}}) by immediately converting any PEP made back to pyruvate

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    Glucagon affects gluconeogenic process by **increasing liver's uptake of amino acids supplied by muscle breakdown** -> increases availability of carbon skeletons for [gluconeogenesis]({{< relref "gluconeogenesis" >}})

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    Promotes the uptake of adipose-derived fatty acids into liver mitochondria -> ensures their oxidation + subsequent formation of ATP required to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    [PKA]({{< relref "protein_kinase_a" >}})-mediated phosphorylation of [CREB]({{< relref "camp_response_element_binding_protein" >}}) -> increased transcription of [gluconeogenic]({{< relref "gluconeogenesis" >}}) enzymes if liver glycogen stores become depleted

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Albumin-bound fatty acids > From Fat Metabolism in Muscle & Adipose Tissue**

    **Released [glycerol]({{< relref "glycerol" >}}) is used primarly by the [liver]({{< relref "liver" >}}) as a [gluconeogenic]({{< relref "gluconeogenesis" >}}) substrate**

    ---

<!--list-separator-->

-  **🔖 Albumin-bound fatty acids > From Fat Metabolism in Muscle & Adipose Tissue**

    Fasting state -> **fatty acids enter the [liver]({{< relref "liver" >}}) as the energy source needed to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})**

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  [Gluconeogenesis]({{< relref "gluconeogenesis" >}})

    -   [Acetyl-CoA]({{< relref "acetyl_coa" >}}) from CoA from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)
    -   [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) uses [Biotin]({{< relref "biotin" >}}) (B7)
    -   [Malate DH]({{< relref "malate_dehydrogenase" >}}) requires [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)
    -   [PEP CK]({{< relref "pep_carboxykinase" >}}) requires [Pyridoxine]({{< relref "pyridoxine" >}}) (B6) (as pyridoxal phosphate) - a cofactor used in decarboxylation reactions


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  [Gluconeogenesis]({{< relref "gluconeogenesis" >}})

    -   Anabolic process (i.e. making glucose) that occurs in the **fasting [liver]({{< relref "liver" >}})**
    -   Most steps catalyzed by **reversible** glycolytic enzymes
        -   4 enzymes required to replace the 3 irreversible steps of glycolysis

    <!--list-separator-->

    -  [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}})

    <!--list-separator-->

    -  [PEP carboxykinase]({{< relref "pep_carboxykinase" >}})

        -   Catalyzes a **GTP-dependent** reaction
        -   **Converts [OAA]({{< relref "oxaloacetate" >}}) into [PEP]({{< relref "phosphoenolpyruvate" >}})**
        -   This + [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) required to convert pyruvate into PEP
            -   **Reverses action of pyruvate kinase**

    <!--list-separator-->

    -  [Fructose 1,6-bisphosphatase]({{< relref "fructose_1_6_bisphosphatase" >}})

        -   Catalyzes **hydrolysis of F-1,6-bP to F6P**
        -   Reverses [PFK-1]({{< relref "6_phosphofructo_1_kinase" >}})
        -   Allosterically inhibited by F-2,6-bP and AMP

    <!--list-separator-->

    -  [Glucose-6-phosphatase]({{< relref "glucose_6_phosphatase" >}})

        -   Catalyzes **hydrolysis of G6P to free glucose for release from liver**
        -   Reverses [glucokinase]({{< relref "glucokinase" >}})

    <!--list-separator-->

    -  NAD<sup>+</sup>-linked lactate dehydrogenase

        -   Converts lactate to pyruvate for entry into gluconeogenesis

    <!--list-separator-->

    -  Alanine aminotransferase

        -   Catalyzes conversion of alanine to pyruvate for entry into gluconeogenesis

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Hormone-sensitive lipase**

    Released fatty acids -> taken-up by rapidly metabolizing tissues (e.g. liver) -> use fatty acids as energy source to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})

    ---

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Hormone-sensitive lipase**

    Glycerol taken up by liver -> used as [gluconeogenic]({{< relref "gluconeogenesis" >}}) substrate

    ---


#### [Kidney]({{< relref "kidney" >}}) {#kidney--kidney-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Participates in [Gluconeogenesis]({{< relref "gluconeogenesis" >}})

    ---


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Cellular Metabolism > 13. Describe gluconeogenesis**

    [Gluconeogenesis]({{< relref "gluconeogenesis" >}})

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Major physiological functions of the kidney > Production of Urine**

    Participation in **[gluconeogenesis]({{< relref "gluconeogenesis" >}})** ([glutamine]({{< relref "glutamine" >}}) and [glutamate]({{< relref "glutamate" >}})) and **[ketogenesis]({{< relref "ketogenesis" >}})**

    ---


#### [Fatty acid]({{< relref "fatty_acid" >}}) {#fatty-acid--fatty-acid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Fasting state -> fatty acids enter the [liver]({{< relref "liver" >}}) as the energy source needed to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})

    ---


#### [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}}) {#describe-the-intercellular-response-to-insulin-induced-cellular-signaling--describe-the-intercellular-response-to-insulin-induced-cellular-signaling-dot-md}

<!--list-separator-->

-  **🔖 Insulin's metabolic effects**

    Restores [pyruvate kinase]({{< relref "pyruvate_kinase" >}}) activity -> stops [gluconeogenesis]({{< relref "gluconeogenesis" >}}) by immediately converting any PEP made back to pyruvate

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
