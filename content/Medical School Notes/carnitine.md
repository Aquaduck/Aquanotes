+++
title = "Carnitine"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Carnitine acyltransferase I (CAT-1) and CAT-II > From Fat Metabolism in Muscle & Adipose Tissue > CAT-2**

    [Fatty acyl-carnitine ester]({{< relref "fatty_acyl_carnitine_ester" >}}) becomes a substrate for _carnitine acyltransferase II_ -> catalyzes transfer of acyl to CoA-SH in matrix -> release [carnitine]({{< relref "carnitine" >}}) for a second round of fatty acid transport

    ---

<!--list-separator-->

-  [Carnitine]({{< relref "carnitine" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_

<!--list-separator-->

-  [Carnitine]({{< relref "carnitine" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism > CPT1 and CPT2**

    Use of [carnitine]({{< relref "carnitine" >}}) in this shuttle ensures that the mitochondrial and cytosolic coenzyme A pools remain intact

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
