+++
title = "List the coenzymes, and where appropriate their vitamin precursors, essential to:"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## [Glycolysis]({{< relref "glycolysis" >}}) {#glycolysis--glycolysis-dot-md}

-   [NADH]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)


## [Pyruvate DH complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dh-complex--pyruvate-dehydrogenase-complex-dot-md}

-   [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)
-   [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)
-   [CoA]({{< relref "coenzyme_a" >}}) from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)
-   [Lipoic acid]({{< relref "lipoic_acid" >}})
-   [Thiamine]({{< relref "thiamine" >}}) (B1)


## [TCA cycle]({{< relref "citric_acid_cycle" >}}) {#tca-cycle--citric-acid-cycle-dot-md}

-   [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)
-   [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)
-   [Acetyl-CoA]({{< relref "acetyl_coa" >}}) from CoA from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)
-   [α-ketoglutarate DH]({{< relref "α_ketoglutarate_dehydrogenase" >}})
    -   Requires same coenzymes as Pyruvate DH complex


## [Fatty acid oxidation]({{< relref "β_oxidation" >}}) {#fatty-acid-oxidation--β-oxidation-dot-md}

-   [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)
-   [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)


## [Gluconeogenesis]({{< relref "gluconeogenesis" >}}) {#gluconeogenesis--gluconeogenesis-dot-md}

-   [Acetyl-CoA]({{< relref "acetyl_coa" >}}) from CoA from [Pantothenic acid]({{< relref "pantothenic_acid" >}}) (B5)
-   [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) uses [Biotin]({{< relref "biotin" >}}) (B7)
-   [Malate DH]({{< relref "malate_dehydrogenase" >}}) requires [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)
-   [PEP CK]({{< relref "pep_carboxykinase" >}}) requires [Pyridoxine]({{< relref "pyridoxine" >}}) (B6) (as pyridoxal phosphate) - a cofactor used in decarboxylation reactions


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}})

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Glycolysis

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Pyruvate DH complex

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  TCA cycle

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Fatty acid oxidation

        <!--list-separator-->

        - <span class="org-todo done _X_">[X]</span>  Gluconeogenesis


### Unlinked references {#unlinked-references}

[Show unlinked references]
