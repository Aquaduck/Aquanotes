+++
title = "Jumpstart VIC Hybrid Learning Objectives"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "jumpstart"]
draft = false
+++

## <span class="org-todo todo TODO">TODO</span> Cell Biology {#cell-biology}


### <span class="org-todo todo ___">[ ]</span> 1. Describe the fluid mosaic model of cell membranes {#1-dot-describe-the-fluid-mosaic-model-of-cell-membranes}


### <span class="org-todo todo ___">[ ]</span> 2. Explain how membrane fluidity is influenced by temperature and membrane composition (including the role of cholesterol) {#2-dot-explain-how-membrane-fluidity-is-influenced-by-temperature-and-membrane-composition--including-the-role-of-cholesterol}


### <span class="org-todo todo ___">[ ]</span> 3. Distinguish between peripheral and integral membrane proteins {#3-dot-distinguish-between-peripheral-and-integral-membrane-proteins}


### <span class="org-todo todo ___">[ ]</span> 4. Distinguish between solutions that are hypertonic, hypotonic, and isotonic to cell contents {#4-dot-distinguish-between-solutions-that-are-hypertonic-hypotonic-and-isotonic-to-cell-contents}


### <span class="org-todo todo ___">[ ]</span> 5. Distinguish between osmosis, facilitated diffusion, and active transport {#5-dot-distinguish-between-osmosis-facilitated-diffusion-and-active-transport}


### <span class="org-todo todo ___">[ ]</span> 6. Diagram the major events of mitotic cell division that enable the genome of one cell to be passed onto 2 daughter cells {#6-dot-diagram-the-major-events-of-mitotic-cell-division-that-enable-the-genome-of-one-cell-to-be-passed-onto-2-daughter-cells}

-   <https://www.osmosis.org/learn/Mitosis%5Fand%5Fmeiosis>


### <span class="org-todo todo ___">[ ]</span> 7. Describe key differences between mitosis and meiosis and explain how the end result of meiosis differs from that of mitosis {#7-dot-describe-key-differences-between-mitosis-and-meiosis-and-explain-how-the-end-result-of-meiosis-differs-from-that-of-mitosis}

-   <https://www.osmosis.org/learn/Mitosis%5Fand%5Fmeiosis>


### <span class="org-todo todo ___">[ ]</span> 8. Diagram the human life cycle and indicate where in the human body that mitosis and meiosis occur: {#8-dot-diagram-the-human-life-cycle-and-indicate-where-in-the-human-body-that-mitosis-and-meiosis-occur}

-   <https://www.osmosis.org/learn/Mitosis%5Fand%5Fmeiosis>


#### <span class="org-todo todo ___">[ ]</span> Which cells are the result of meiosis and mitosis {#which-cells-are-the-result-of-meiosis-and-mitosis}


#### <span class="org-todo todo ___">[ ]</span> Which cells are haploid and diploid {#which-cells-are-haploid-and-diploid}


### <span class="org-todo todo ___">[ ]</span> 9. List the phases of the cell cycle and describe the sequence of events that occurs during each phase {#9-dot-list-the-phases-of-the-cell-cycle-and-describe-the-sequence-of-events-that-occurs-during-each-phase}

-   <https://www.osmosis.org/learn/Cell%5Fcycle>


## <span class="org-todo todo TODO">TODO</span> Cellular Metabolism {#cellular-metabolism}


### <span class="org-todo done _X_">[X]</span> 1. Describe the relationship between anabolism, catabolism, and chemical energy {#1-dot-describe-the-relationship-between-anabolism-catabolism-and-chemical-energy}

-   [Catabolic pathways]({{< relref "introduction_to_metabolism_and_glycolysis" >}})
-   [Anabolic pathways]({{< relref "introduction_to_metabolism_and_glycolysis" >}})


### <span class="org-todo done _X_">[X]</span> 2. Describe the importance of oxidation-reduction reactions in metabolism {#2-dot-describe-the-importance-of-oxidation-reduction-reactions-in-metabolism}

-   [Oxidation and Reduction in Metabolism]({{< relref "energy_redox_reactions_and_enzymes" >}})


### <span class="org-todo done _X_">[X]</span> 3. Describe why ATP, FAD, NAD+, and NADP+ are important in a cell {#3-dot-describe-why-atp-fad-nad-plus-and-nadp-plus-are-important-in-a-cell}

-   [Energy Carriers]({{< relref "energy_redox_reactions_and_enzymes" >}})


### <span class="org-todo todo ___">[ ]</span> 4. [Discuss how and why the energy charge and reducing potential of the cell are the major regulators of cellular metabolic processes](https://vicportal.med.uvm.edu/bbcswebdav/pid-34120-dt-content-rid-135796%5F1/xid-135796%5F1) {#4-dot-discuss-how-and-why-the-energy-charge-and-reducing-potential-of-the-cell-are-the-major-regulators-of-cellular-metabolic-processes}


### <span class="org-todo todo ___">[ ]</span> 5. [Define and explain allosterism](https://ezp.med.uvm.edu/login?url=https://meded.lwwhealthlibrary.com/content.aspx?sectionid=147991036&bookid=1988#147991169) {#5-dot-define-and-explain-allosterism}


### <span class="org-todo done _X_">[X]</span> 6. Define and explain Gibbs free energy (\\(\Delta G\\)), standard free energy (\\(\Delta G^{o}\\)), and the equilibrium constant (K<sub>eq</sub>) as it applies to biochemical functions {#6-dot-define-and-explain-gibbs-free-energy--delta-g--standard-free-energy--delta-g-o--and-the-equilibrium-constant--k--as-it-applies-to-biochemical-functions}

-   [Free Energy]({{< relref "free_energy" >}})


### <span class="org-todo todo ___">[ ]</span> 7. Detail the reactions that define the metabolic branchpoints of two key intermediary metabolites (glucose-6-phosphate and pyruvate) i.e. products of and enzymes catalyzing those reactions {#7-dot-detail-the-reactions-that-define-the-metabolic-branchpoints-of-two-key-intermediary-metabolites--glucose-6-phosphate-and-pyruvate--i-dot-e-dot-products-of-and-enzymes-catalyzing-those-reactions}


#### Sources {#sources}

-   [Key Metabolic Branchpoints - July 16th.pptx](https://vicportal.med.uvm.edu/bbcswebdav/pid-34273-dt-content-rid-134678%5F1/xid-134678%5F1)
-   [Key metabolic Branchpoints - text - JumpStart VIC.docx](https://vicportal.med.uvm.edu/bbcswebdav/pid-34273-dt-content-rid-134679%5F1/xid-134679%5F1)
-   [Lippincotts Biochemistry Chapter 8: Introduction to Metabolism and Glycolysis (Section VII)](https://ezp.med.uvm.edu/login?url=https://meded.lwwhealthlibrary.com/content.aspx?sectionid=147991426&bookid=1988)


### <span class="org-todo todo ___">[ ]</span> 8. [Describe the function, regulation and intraceullar location of glycolysis](https://vicportal.med.uvm.edu/bbcswebdav/pid-34180-dt-content-rid-134633%5F1/xid-134633%5F1) {#8-dot-describe-the-function-regulation-and-intraceullar-location-of-glycolysis}


#### <span class="org-todo todo ___">[ ]</span> List and describe the unique features of glycolysis: {#list-and-describe-the-unique-features-of-glycolysis}

<!--list-separator-->

- <span class="org-todo todo ___">[-]</span>  How glucose is trapped in the cell for use in metabolic process

    -   Phosphate group added to glucose (G6P) prevents it from crossing membrane

<!--list-separator-->

- <span class="org-todo todo ___">[ ]</span>  The irreversible and rate-limiting steps in glycolysis

<!--list-separator-->

- <span class="org-todo done _X_">[X]</span>  The reactions in which energy is utilized and generated

<!--list-separator-->

- <span class="org-todo todo ___">[ ]</span>  The reversible reactions in which high-energy intermediates are generated

<!--list-separator-->

- <span class="org-todo todo ___">[ ]</span>  How the NADH generated is oxidized under anaerobic vs. aerobic conditions


#### <span class="org-todo todo ___">[ ]</span> Explain the concept of substrate level phosphorylation and its importance to the cell {#explain-the-concept-of-substrate-level-phosphorylation-and-its-importance-to-the-cell}


#### <span class="org-todo todo ___">[ ]</span> Describe the committed step of glycolysis and explain: {#describe-the-committed-step-of-glycolysis-and-explain}

<!--list-separator-->

- <span class="org-todo todo ___">[ ]</span>  Why it is the committed step

<!--list-separator-->

- <span class="org-todo todo ___">[ ]</span>  How is it regulated


#### <span class="org-todo todo ___">[ ]</span> Describe how and why _pyruvate kinase_ is regulated {#describe-how-and-why-pyruvate-kinase-is-regulated}


#### <span class="org-todo todo ___">[ ]</span> Determine the net energy yield from aerobic vs. anaerobic glycolysis {#determine-the-net-energy-yield-from-aerobic-vs-dot-anaerobic-glycolysis}


#### <span class="org-todo todo ___">[ ]</span> Describe how arsenic disables the glycolytic process and substrate-level phosphorylation {#describe-how-arsenic-disables-the-glycolytic-process-and-substrate-level-phosphorylation}


#### Additional Materials: {#additional-materials}

-   <https://ezp.med.uvm.edu/login?url=https://meded.lwwhealthlibrary.com/content.aspx?sectionid=147991426&bookid=1988>
    ![](/ox-hugo/_20210729_214833screenshot.png)


### <span class="org-todo todo ___">[ ]</span> 9. Describe the function of the pyruvate dehydrogenase (PDH) complex {#9-dot-describe-the-function-of-the-pyruvate-dehydrogenase--pdh--complex}


#### Sources {#sources}

-   [PDH Complex - Function and Regulation.pptx](https://vicportal.med.uvm.edu/bbcswebdav/pid-34292-dt-content-rid-134687%5F1/xid-134687%5F1)
-   [Lippincotts Biochemistry Chapter 9: Section IIA (Oxidative Decarboxylation of Pyruvate)](https://ezp.med.uvm.edu/login?url=https://meded.lwwhealthlibrary.com/content.aspx?sectionid=147991586&bookid=1988)


#### <span class="org-todo todo ___">[ ]</span> Describe its overall purpose, its reactants and products, and its cellular location {#describe-its-overall-purpose-its-reactants-and-products-and-its-cellular-location}


#### <span class="org-todo todo ___">[ ]</span> List the coenzymes required for PDH function and the vitamins required for their synthesis {#list-the-coenzymes-required-for-pdh-function-and-the-vitamins-required-for-their-synthesis}


#### <span class="org-todo todo ___">[ ]</span> Write the balanced equation that describes the PDH complex-catalyzed conversion of pyruvate to acetyl-CoA {#write-the-balanced-equation-that-describes-the-pdh-complex-catalyzed-conversion-of-pyruvate-to-acetyl-coa}


#### <span class="org-todo todo ___">[ ]</span> Describe how arsenic disables the PDH complex {#describe-how-arsenic-disables-the-pdh-complex}


### <span class="org-todo todo ___">[ ]</span> 10. Describe the beta-oxidation of fatty acids and ketone body formation {#10-dot-describe-the-beta-oxidation-of-fatty-acids-and-ketone-body-formation}


#### Sources {#sources}

-   [Osmosis - Fatty Acid Oxidation](https://www.osmosis.org/learn/Fatty%5Facid%5Foxidation)
-   [Lippincotts Biochemistry 7e: Chapter 16 "Fatty Acid, Triacylglycerol, and ketone Body Metabolism"](https://ezp.med.uvm.edu/login?url=https://meded.lwwhealthlibrary.com/content.aspx?sectionid=147992111&bookid=1988)
    -   Section I (Overview)
    -   Section II (Fatty Acid Structure)
    -   Section IV (Fat Mobilization and Fatty Acid Oxidation) B 1 through 4
    -   Section V (Ketone Bodies: Alternative Fuel for Cells) A&B


#### <span class="org-todo todo ___">[ ]</span> Describe the mechanism for actiation and transport of fatty acids into mitochondria for catabolism {#describe-the-mechanism-for-actiation-and-transport-of-fatty-acids-into-mitochondria-for-catabolism}


#### <span class="org-todo todo ___">[ ]</span> Describe the process by which fatty acids enter the mitochondrial matrix. {#describe-the-process-by-which-fatty-acids-enter-the-mitochondrial-matrix-dot}


#### <span class="org-todo todo ___">[ ]</span> Describe how fatty acids are oxidized to acetyl-CoA, and how that process provides the energy required to drive gluconeogenesis in the fasting liver {#describe-how-fatty-acids-are-oxidized-to-acetyl-coa-and-how-that-process-provides-the-energy-required-to-drive-gluconeogenesis-in-the-fasting-liver}


#### <span class="org-todo todo ___">[ ]</span> Write the balanced equation describing the complete oxidation of palmitate (C16, totally saturated fatty acid) {#write-the-balanced-equation-describing-the-complete-oxidation-of-palmitate--c16-totally-saturated-fatty-acid}


#### <span class="org-todo todo ___">[ ]</span> Describe the source of ketone bodies {#describe-the-source-of-ketone-bodies}


#### <span class="org-todo todo ___">[ ]</span> Outline ketone body formation {#outline-ketone-body-formation}


### <span class="org-todo todo ___">[ ]</span> 11. [Describe the function of the tricarboxylic acid (TCA) cycle](https://www.osmosis.org/learn/Citric%5Facid%5Fcycle) {#11-dot-describe-the-function-of-the-tricarboxylic-acid--tca--cycle}

-   [Krebs]({{< relref "citric_acid_cycle" >}})


#### <span class="org-todo todo ___">[-]</span> List key features of TCA: {#list-key-features-of-tca}

1.  Where it occurs
    -   Mitochondrial matrix **except for succinate DH, which is inner membrane**
2.  Oxygen requirements
3.  Required coenzymes
4.  Products
5.  Why it is an amphibolic pathway
6.  Why it is a catalytic cycle


#### <span class="org-todo todo ___">[-]</span> List + describe key reactions {#list-plus-describe-key-reactions}

1.  Committed + Rate-limiting step
2.  Oxidative decarboxylation reactions
3.  Redox reactions
4.  Substrate-level phosphorylation (ATP producing) reactions


#### <span class="org-todo todo ___">[ ]</span> Describe how arsenic disables the TCA cycle {#describe-how-arsenic-disables-the-tca-cycle}


### <span class="org-todo todo ___">[ ]</span> 12. Describe oxidative phosphorylation {#12-dot-describe-oxidative-phosphorylation}


#### <span class="org-todo todo ___">[ ]</span> Define the physiologic importance of the ETC and oxidative phosphorylation {#define-the-physiologic-importance-of-the-etc-and-oxidative-phosphorylation}


#### <span class="org-todo todo ___">[ ]</span> Describe the enzymatic complexes of the mitochondrial ETC and their function {#describe-the-enzymatic-complexes-of-the-mitochondrial-etc-and-their-function}


#### <span class="org-todo todo ___">[ ]</span> Describe the process of oxidative phosphorylation through discussion of the chemiosmotic hypothesis, proton pumping and ATP synthesis {#describe-the-process-of-oxidative-phosphorylation-through-discussion-of-the-chemiosmotic-hypothesis-proton-pumping-and-atp-synthesis}


#### <span class="org-todo todo ___">[ ]</span> Describe the structure/function of ATP synthase {#describe-the-structure-function-of-atp-synthase}


#### <span class="org-todo todo ___">[-]</span> Describe what is meant by the concept of "respiratory control" {#describe-what-is-meant-by-the-concept-of-respiratory-control}

-   [The Regulation of Cellular Respiration Is Governed Primarily by the Need for ATP]({{< relref "the_regulation_of_cellular_respiration_is_governed_primarily_by_the_need_for_atp" >}})


#### <span class="org-todo todo ___">[ ]</span> Distinguish between the effects of ETC uncouples vs. inhibitors of oxidative phosphorylation and explain the consequences of their actions {#distinguish-between-the-effects-of-etc-uncouples-vs-dot-inhibitors-of-oxidative-phosphorylation-and-explain-the-consequences-of-their-actions}


### <span class="org-todo todo ___">[-]</span> [13. Describe gluconeogenesis](https://www.osmosis.org/learn/Gluconeogenesis) {#13-dot-describe-gluconeogenesis}

-   [Gluconeogenesis]({{< relref "gluconeogenesis" >}})


#### <span class="org-todo todo ___">[-]</span> Discuss the essential role of pyruvate in gluconeogenesis {#discuss-the-essential-role-of-pyruvate-in-gluconeogenesis}


#### <span class="org-todo todo ___">[-]</span> Explain why and how gluconeogenesis and glycolysis are integrally related {#explain-why-and-how-gluconeogenesis-and-glycolysis-are-integrally-related}


#### <span class="org-todo todo ___">[-]</span> List the enzymes unique to gluconeogenesis, when compared to glycolysis, including their intracellular location and required coenzymes {#list-the-enzymes-unique-to-gluconeogenesis-when-compared-to-glycolysis-including-their-intracellular-location-and-required-coenzymes}


#### <span class="org-todo todo ___">[-]</span> Explain the energy needs of gluconeogenesis {#explain-the-energy-needs-of-gluconeogenesis}


### <span class="org-todo todo ___">[-]</span> 14. List the vitamins required for the appropriate functioning of: {#14-dot-list-the-vitamins-required-for-the-appropriate-functioning-of}


#### <span class="org-todo todo ___">[-]</span> Glycolysis {#glycolysis}

-   [Thiamine]({{< relref "thiamine" >}})


#### <span class="org-todo todo ___">[-]</span> Pyruvate DH complex {#pyruvate-dh-complex}

-   [Thiamine]({{< relref "thiamine" >}})


#### <span class="org-todo todo ___">[-]</span> Fatty acid oxidation {#fatty-acid-oxidation}

-   [Pantothenic acid]({{< relref "pantothenic_acid" >}})


#### <span class="org-todo todo ___">[-]</span> TCA cycle {#tca-cycle}

-   [Thiamine]({{< relref "thiamine" >}})
-   [Riboflavin]({{< relref "riboflavin" >}})


#### <span class="org-todo todo ___">[-]</span> Gluconeogenesis {#gluconeogenesis}

-   [Pyridoxine]({{< relref "pyridoxine" >}})
    -   Transamination one way to prepare glucose from AAs


## <span class="org-todo todo TODO">TODO</span> DNA Replication, Transcription, and Translation {#dna-replication-transcription-and-translation}


### <span class="org-todo todo ___">[-]</span> [1. Summarize the central dogma of molecular biology, and cite exceptions to the original model](https://www.dnalc.org/resources/3d/central-dogma.html) {#1-dot-summarize-the-central-dogma-of-molecular-biology-and-cite-exceptions-to-the-original-model}

-   [7.1 The Genetic Code]({{< relref "kaplan_biochemistry_chapter_7" >}})


### <span class="org-todo todo ___">[-]</span> [2. Describe the double-stranded, helical, antiparallel, and topological structure of DNA and how it relates to the processes of DNA replication, transcription, recombination and repair](https://www.osmosis.org/learn/DNA%5Fstructure) {#2-dot-describe-the-double-stranded-helical-antiparallel-and-topological-structure-of-dna-and-how-it-relates-to-the-processes-of-dna-replication-transcription-recombination-and-repair}

-   [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}})


### <span class="org-todo todo ___">[-]</span> [3. Summarize the structure and function of eukaryotic chromosomes and chromatin, including chromatin modifications, telomeres, and centromeres](https://www.osmosis.org/learn/DNA%5Fstructure) {#3-dot-summarize-the-structure-and-function-of-eukaryotic-chromosomes-and-chromatin-including-chromatin-modifications-telomeres-and-centromeres}

-   [6.2 Eukaryotic Chromosome Organization]({{< relref "kaplan_biochemistry_chapter_6" >}})
-   [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}})


### <span class="org-todo todo ___">[-]</span> [4. Compare and contrast the different types of RNA](https://www.khanacademy.org/science/high-school-%20biology/hs-molecular-genetics/hs-rna-and-protein-synthesis/a/hs-rna-and-protein-synthesis-review) {#4-dot-compare-and-contrast-the-different-types-of-rna}

-   [Types of RNA]({{< relref "khan_rna_and_protein_synthesis_review" >}})


### <span class="org-todo todo ___">[-]</span> 5. Summarize the mechanism of DNA replication and replication forks, and describe what is meant by the terms semi-conservative, discontinuous, leading strand, lagging strand, processivity, and fidelity {#5-dot-summarize-the-mechanism-of-dna-replication-and-replication-forks-and-describe-what-is-meant-by-the-terms-semi-conservative-discontinuous-leading-strand-lagging-strand-processivity-and-fidelity}

-   [DNA Replication]({{< relref "osmosis_transcription_translation_and_replication" >}})
-   [ScienceDirect - Processivity]({{< relref "sciencedirect_processivity" >}})
-   _Fidelity_: the ability of polymerases to avoid or correct errors in newly synthesized DNA strands


#### Prep material {#prep-material}

-   <https://www.osmosis.org/learn/DNA%5Freplication>
-   <https://cnx.org/contents/jVCgr5SL@15.43:gqw0bESi@10/14-5-DNA-Replication-in-Eukaryotes>
-   <https://www.dnalc.org/resources/3d/04-mechanism-of-replication-advanced.html>


### <span class="org-todo todo ___">[-]</span> 6. Summarize the major types of DNA damage caused by replication errors, ionizing radiation, and reactive oxygen species {#6-dot-summarize-the-major-types-of-dna-damage-caused-by-replication-errors-ionizing-radiation-and-reactive-oxygen-species}

-   [6.4 DNA Repair]({{< relref "kaplan_biochemistry_chapter_6" >}})
-   [ROS and the DNA damage response in cancer]({{< relref "ros_and_the_dna_damage_response_in_cancer" >}})


#### Prep material {#prep-material}

-   <https://www.osmosis.org/learn/DNA%5Fdamage%5Fand%5Frepair>


### <span class="org-todo todo ___">[-]</span> 7. Compare and contrast polymerase proofreading, base excision repair, nucleotide excision repair, mismatch repair, and recombination {#7-dot-compare-and-contrast-polymerase-proofreading-base-excision-repair-nucleotide-excision-repair-mismatch-repair-and-recombination}

-   [Khan - DNA Proofreading and Repair]({{< relref "khan_dna_proofreading_and_repair" >}})


#### Prep material {#prep-material}

-   <https://www.khanacademy.org/science/high-school-biology/%20hs-molecular-genetics/hs-discovery-and-structure-of-dna/a/dna-proofreading-and-repair>


### <span class="org-todo todo ___">[ ]</span> 9. [Describe the universal features of the genetic code and describe its biological relevance](https://www.khanacademy.org/science/high-school-biology/%20hs-molecular-genetics/hs-rna-and-protein-synthesis/a/the-genetic-code) {#9-dot-describe-the-universal-features-of-the-genetic-code-and-describe-its-biological-relevance}


### <span class="org-todo todo ___">[ ]</span> [10. Describe the function of start codon, stop codons, and open reading frames](https://www.khanacademy.org/science/high-school-biology/%20hs-molecular-genetics/hs-rna-and-protein-synthesis/a/the-genetic-code) {#10-dot-describe-the-function-of-start-codon-stop-codons-and-open-reading-frames}


### <span class="org-todo todo ___">[ ]</span> [11. Use the genetic code to predict the amino acid sequence of a protein for a given nucleic acid sequence and demonstrate how nucleotide mutations can lead to alterations in the primary structure of a protein](https://www.dnalc.org/resources/3d/10-triplet-code.html) {#11-dot-use-the-genetic-code-to-predict-the-amino-acid-sequence-of-a-protein-for-a-given-nucleic-acid-sequence-and-demonstrate-how-nucleotide-mutations-can-lead-to-alterations-in-the-primary-structure-of-a-protein}


### <span class="org-todo todo ___">[ ]</span> 11. Summarize the initiation, elongation, and termination of transcription, comparing and contrasting these processes in eukaryotic and prokaryotic cells. {#11-dot-summarize-the-initiation-elongation-and-termination-of-transcription-comparing-and-contrasting-these-processes-in-eukaryotic-and-prokaryotic-cells-dot}


#### Prep material {#prep-material}

-   <https://www.osmosis.org/learn/Transcription>
-   <https://cnx.org/contents/jVCgr5SL@15.43:PxuX1oI%5F@11/15-2-Prokaryotic-Transcription>
-   <https://cnx.org/contents/jVCgr5SL@15.43:Er923r9q@12/15-3-Eukaryotic-Transcription>


### <span class="org-todo todo ___">[-]</span> [12. Describe the prokaryotic lac operon and summarize the positive and negative regulation of lac operon transcription.](https://www.osmosis.org/learn/Lac%5Foperon) {#12-dot-describe-the-prokaryotic-lac-operon-and-summarize-the-positive-and-negative-regulation-of-lac-operon-transcription-dot}

-   [Osmosis - Lac operon]({{< relref "osmosis_lac_operon" >}})


### <span class="org-todo todo ___">[ ]</span> 13. Describe the post-transcriptional processing of eukaryotic mRNA.]] {#13-dot-describe-the-post-transcriptional-processing-of-eukaryotic-mrna-dot}


### <span class="org-todo todo ___">[ ]</span> 14. Define epigenetics. Summarize the effect of covalent modification of chromatin on gene transcription (including methylation, histone acetylation and phosphorylation).]] {#14-dot-define-epigenetics-dot-summarize-the-effect-of-covalent-modification-of-chromatin-on-gene-transcription--including-methylation-histone-acetylation-and-phosphorylation--dot}


### <span class="org-todo todo ___">[ ]</span> 15. Summarize the three steps of translation: initiation, elongation, and termination. Compare and contrast these processes and their regulation in eukaryotic and prokaryotic cells {#15-dot-summarize-the-three-steps-of-translation-initiation-elongation-and-termination-dot-compare-and-contrast-these-processes-and-their-regulation-in-eukaryotic-and-prokaryotic-cells}


#### Material {#material}

-   <https://www.osmosis.org/learn/Translation>
-   <https://www.researchgate.net/publication/314214643%5FDifference%5FBetween%5FProkaryotic%5Fand%5FEukaryotic%5FTranslation>


## <span class="org-todo todo TODO">TODO</span> General Biology {#general-biology}


### <span class="org-todo todo ___">[ ]</span> 1. Name and be able to identify: {#1-dot-name-and-be-able-to-identify}


#### <span class="org-todo todo ___">[-]</span> Four classes of biomolecules {#four-classes-of-biomolecules}

-   [Biological Molecules]({{< relref "crashcourse_biological_molecules" >}})


#### <span class="org-todo todo ___">[ ]</span> Provide three common functional roles for each {#provide-three-common-functional-roles-for-each}


#### <span class="org-todo todo ___">[ ]</span> Provide three specific examples for each {#provide-three-specific-examples-for-each}


### <span class="org-todo todo ___">[ ]</span> 2. Distinguish between: {#2-dot-distinguish-between}


#### <span class="org-todo todo ___">[ ]</span> DNA {#dna}


#### <span class="org-todo todo ___">[ ]</span> Gene {#gene}


#### <span class="org-todo todo ___">[ ]</span> Chromosome {#chromosome}


#### <span class="org-todo todo ___">[ ]</span> Genome {#genome}


#### <span class="org-todo done _X_">[X]</span> Nucleotide vs. nucleoside {#nucleotide-vs-dot-nucleoside}

-   _Nucleotide_: One base found in DNA
-   _Nucleoside_: Nucleotide but with an -OH on the 5' carbon instead of phosphate group


#### <span class="org-todo done _X_">[X]</span> Purines and pyrimidines {#purines-and-pyrimidines}

-   _Purine_: Heterocyclic aromatic compound containing 4 nitrogen atoms
    -   Two carbon rings - pyrimidine ring fused with imidazole ring
-   _Pyrimidine_: Heterocyclic aromatic compound containing 2 nitrogen atoms
    -   Only one carbon ring


### <span class="org-todo done _X_">[X]</span> 3. Apply the complementary base pairing rule to determine a sequence of a complementary strand of DNA or RNA {#3-dot-apply-the-complementary-base-pairing-rule-to-determine-a-sequence-of-a-complementary-strand-of-dna-or-rna}

-   A -> T/U
-   G -> C


### <span class="org-todo todo ___">[ ]</span> 4. Compare and contrast eukaryotic and prokaryotic cells: organelles, membranes, gene structure and growth {#4-dot-compare-and-contrast-eukaryotic-and-prokaryotic-cells-organelles-membranes-gene-structure-and-growth}


#### Sources {#sources}

-   <https://www.osmosis.org/learn/Cellular%5Fstructure%5Fand%5Ffunction>
-   <https://www.khanacademy.org/test-prep/mcat/cells#eukaryotic-cells>


## <span class="org-todo todo WAIT">WAIT</span> General Chemistry {#general-chemistry}


### <span class="org-todo done _X_">[X]</span> 1. Relate water's chemical structure to its unique properties, and explain how these properties are vital to living systems {#1-dot-relate-water-s-chemical-structure-to-its-unique-properties-and-explain-how-these-properties-are-vital-to-living-systems}

-   [Water]({{< relref "water" >}})


### <span class="org-todo done _X_">[X]</span> 2. Define the relationship between the concentration of hydrogen ions and pH {#2-dot-define-the-relationship-between-the-concentration-of-hydrogen-ions-and-ph}

-   \\(pH = -log[H^{+}]\\)


### <span class="org-todo done _X_">[X]</span> 3. Describe how the structure, or composition, of a buffer functions to resist changes in pH {#3-dot-describe-how-the-structure-or-composition-of-a-buffer-functions-to-resist-changes-in-ph}

-   [Buffer]({{< relref "buffer" >}})


### <span class="org-todo done _X_">[X]</span> 4. Describe the change in free energy of the system for a physical or chemical process in terms of the changes in enthalpy and entropy of the system {#4-dot-describe-the-change-in-free-energy-of-the-system-for-a-physical-or-chemical-process-in-terms-of-the-changes-in-enthalpy-and-entropy-of-the-system}

-   \\(\Delta G = \Delta H - T\Delta S\\)


### <span class="org-todo done _X_">[X]</span> 5. Describe the meaning of a positive value, a negative value, and a value of zero for \\(\Delta G\\) {#5-dot-describe-the-meaning-of-a-positive-value-a-negative-value-and-a-value-of-zero-for-delta-g}

-   [Free Energy]({{< relref "free_energy" >}})


## <span class="org-todo todo TODO">TODO</span> General Biochemistry and Techniques {#general-biochemistry-and-techniques}


### <span class="org-todo done _X_">[X]</span> 1. Define k<sub>d</sub> {#1-dot-define-k}

-   [Malvern - Binding Affinity]({{< relref "malvern_binding_affinity" >}})


### <span class="org-todo todo ___">[ ]</span> 2. [Explain how dialysis works](https://www.freseniuskidneycare.com/ckd-treatment/what-is-dialysis/hemodialysis-machine) {#2-dot-explain-how-dialysis-works}


### <span class="org-todo todo ___">[ ]</span> [3. Describe how the molecules separate in gels containing agarose or SDS-PAGE](https://www.khanacademy.org/science/biology/biotech-dna-technology/dna-sequencing-pcr-electrophoresis/a/gel-electrophoresis) {#3-dot-describe-how-the-molecules-separate-in-gels-containing-agarose-or-sds-page}


### <span class="org-todo done _X_">[X]</span> 4. Distinguish between monoclonal and polyclonal antibodies {#4-dot-distinguish-between-monoclonal-and-polyclonal-antibodies}

-   [Openstax - Polyclonal and Monoclonal Antibody Production]({{< relref "openstax_polyclonal_and_monoclonal_antibody_production" >}})


### <span class="org-todo done _X_">[X]</span> 5. Describe how antibodies are used in an Enzyme-Linked ImmunoSorbent Assay (ELISA) {#5-dot-describe-how-antibodies-are-used-in-an-enzyme-linked-immunosorbent-assay--elisa}

-   [Openstax - EIAs and ELISAs]({{< relref "openstax_eias_and_elisas" >}})


### <span class="org-todo todo ___">[ ]</span> [6. Describe the process and function of PCR](https://www.osmosis.org/learn/Polymerase%5Fchain%5Freaction) {#6-dot-describe-the-process-and-function-of-pcr}


## <span class="org-todo todo TODO">TODO</span> Genetics {#genetics}


### <span class="org-todo done _X_">[X]</span> 1. Define the following terms: {#1-dot-define-the-following-terms}

-   <https://www.osmosis.org/learn/Mendelian%5Fgenetics%5Fand%5Fpunnett%5Fsquares>


#### <span class="org-todo done _X_">[X]</span> True breeding {#true-breeding}

-   Parent always produces offspring with the same genotype as them


#### <span class="org-todo done _X_">[X]</span> Hybridization {#hybridization}

-   Breeding two genetically distinct individuals to produce a hybrid offspring


#### <span class="org-todo done _X_">[X]</span> Monohybrid cross {#monohybrid-cross}

-   A cross between two organisms with different variations at **one** genetic locus of interest


#### <span class="org-todo done _X_">[X]</span> P generation {#p-generation}

-   Parent generation


#### <span class="org-todo done _X_">[X]</span> F1 generation {#f1-generation}

-   First filial generation


#### <span class="org-todo done _X_">[X]</span> F2 generation {#f2-generation}

-   Second filial generation


### <span class="org-todo done _X_">[X]</span> 2. Use a Punnett square to predict the results of a monohybrid cross, stating the phenotypic and genotypic ratios of the F2 generation {#2-dot-use-a-punnett-square-to-predict-the-results-of-a-monohybrid-cross-stating-the-phenotypic-and-genotypic-ratios-of-the-f2-generation}

-   <https://www.osmosis.org/learn/Mendelian%5Fgenetics%5Fand%5Fpunnett%5Fsquares>
-   [Osmosis - Population genetics]({{< relref "osmosis_population_genetics" >}})


### <span class="org-todo done _X_">[X]</span> 4. Distinguish between the following pairs of terms: - <https://www.osmosis.org/learn/Inheritance%5Fpatterns> {#4-dot-distinguish-between-the-following-pairs-of-terms-https-www-dot-osmosis-dot-org-learn-inheritance-patterns}


#### <span class="org-todo done _X_">[X]</span> Dominant/recessive {#dominant-recessive}


#### <span class="org-todo done _X_">[X]</span> Heterozygous/homozygous {#heterozygous-homozygous}


#### <span class="org-todo done _X_">[X]</span> Genotype/phenotype {#genotype-phenotype}


### <span class="org-todo done _X_">[X]</span> 5. Explain how a testcross can be used to determine if an individual with the dominant phenotype is homozygous or heterozygous {#5-dot-explain-how-a-testcross-can-be-used-to-determine-if-an-individual-with-the-dominant-phenotype-is-homozygous-or-heterozygous}

-   <https://www.osmosis.org/learn/Inheritance%5Fpatterns>
-   Cross with recessive:
    -   If homozygous -> all offspring heterozygous + dominant phenotype
    -   If heterozygous -> 25% offspring recessive phenotype


### <span class="org-todo done _X_">[X]</span> 6. Use a Punnett square or probabilities to predict the results of a dihybrid cross and state the phenotypic and genotypic ratios of the F2 generation {#6-dot-use-a-punnett-square-or-probabilities-to-predict-the-results-of-a-dihybrid-cross-and-state-the-phenotypic-and-genotypic-ratios-of-the-f2-generation}

-   <https://www.dnalc.org/resources/genescreen/punnett-empty.html>


### <span class="org-todo done _X_">[X]</span> 8. Explain how phenotypic expression in the heterozygote differs with complete dominance, incomplete dominance, and co-dominance differ {#8-dot-explain-how-phenotypic-expression-in-the-heterozygote-differs-with-complete-dominance-incomplete-dominance-and-co-dominance-differ}

-   <https://www.khanacademy.org/science/high-%20school-biology/hs-classical-genetics/hs-non-mendelian-inheritance/v/co-dominance-and-incomplete-dominance>
-   _Complete dominance_: only one phenotype expressed
    -   Red flower (recessive: white flower)
-   _Incomplete dominance_: both phenotypes merged; hybrid phenotype expressed
    -   Pink flower
-   _Co-dominance_: both phenotypes expressed simultaneously
    -   Red _and_ white flower


### <span class="org-todo done _X_">[X]</span> 12. Interpret a basic pedigree: {#12-dot-interpret-a-basic-pedigree}

-   <https://www.youtube.com/watch?time%5F%20continue=524&v=Gd09V2AkZv4>


#### <span class="org-todo done _X_">[X]</span> Identify the proband {#identify-the-proband}


#### <span class="org-todo done _X_">[X]</span> State the relationships of other family members to the proband {#state-the-relationships-of-other-family-members-to-the-proband}


#### <span class="org-todo done _X_">[X]</span> Describe which individuals are affected by a given heritable condition {#describe-which-individuals-are-affected-by-a-given-heritable-condition}


### <span class="org-todo todo ___">[ ]</span> [3. Describe Mendel's Law of Segregation and the phase of meiosis in which it is applied](https://www.khanacademy.org/science/high-school-biology/%20hs-classical-genetics/hs-introduction-to-heredity/a/the-law-of-segregation) {#3-dot-describe-mendel-s-law-of-segregation-and-the-phase-of-meiosis-in-which-it-is-applied}


### <span class="org-todo todo ___">[ ]</span> [7. State Mendel's law of independent assortment and describe how this law can be explained by the behavior of chromosomes during meiosis](https://www.khanacademy.org/science/%20high-school-biology/hs-classical-genetics/hs-introduction-to-heredity/a/the-law-of-independent-assortment) {#7-dot-state-mendel-s-law-of-independent-assortment-and-describe-how-this-law-can-be-explained-by-the-behavior-of-chromosomes-during-meiosis}


### <span class="org-todo todo ___">[ ]</span> [9. Understand the principles of Hardy-Weinberg Equilibrium](https://www.osmosis.org/learn/Hardy-Weinberg%5Fequilibrium) {#9-dot-understand-the-principles-of-hardy-weinberg-equilibrium}


### <span class="org-todo todo ___">[ ]</span> [10. Be able to calculate allele and genotype frequencies for a population](https://www.osmosis.org/learn/Independent%5Fassortment%5Fof%5Fgenes%5Fand%5Flinkage) {#10-dot-be-able-to-calculate-allele-and-genotype-frequencies-for-a-population}


### <span class="org-todo todo ___">[ ]</span> 11. Describe how the forces of mutation, migration, non-random mating (sexual selection and phenotypic assortment), genetic drift, and natural selection can lead to differences in population variation {#11-dot-describe-how-the-forces-of-mutation-migration-non-random-mating--sexual-selection-and-phenotypic-assortment--genetic-drift-and-natural-selection-can-lead-to-differences-in-population-variation}


#### Sources {#sources}

-   <https://www.youtube.com/watch?v=W0TM4LQmoZY%20>
-   <https://www.khanacademy.org/science/ap-biology/%20ecology-ap/disruptions-to-ecosystems/v/mutation-as-a-source-of-variation>


### <span class="org-todo todo ___">[ ]</span> [Classic modes of inheritance](https://vicportal.med.uvm.edu/bbcswebdav/pid-34468-dt-content-rid-135825%5F1/xid-135825%5F1) {#classic-modes-of-inheritance}


### <span class="org-todo todo ___">[ ]</span> [Inheritance patterns](https://www.osmosis.org/learn/Inheritance%5Fpatterns?from=/usmle-step-1-review/genetics/genetics/population-genetics) {#inheritance-patterns}


### <span class="org-todo todo ___">[ ]</span> [Mitochondrial myopathy](https://www.osmosis.org/learn/Mitochondrial%5Fmyopathy) {#mitochondrial-myopathy}


### <span class="org-todo todo ___">[ ]</span> [Mitochondrial inheritance](https://vicportal.med.uvm.edu/bbcswebdav/pid-34471-dt-content-rid-135826%5F1/xid-135826%5F1) {#mitochondrial-inheritance}


## <span class="org-todo todo TODO">TODO</span> Information Literacy {#information-literacy}


### <span class="org-todo todo ___">[ ]</span> 1. Identify the elements of a citation {#1-dot-identify-the-elements-of-a-citation}


#### Prep material {#prep-material}

<!--list-separator-->

-  <https://www.ncbi.nlm.nih.gov/books/NBK7282/>

<!--list-separator-->

-  <http://library.uvm.edu/Guide-on-the-Side/tutorial/bcor-12-reading-citations>


### <span class="org-todo todo ___">[ ]</span> 2. Describe how a library catalog is used to find books, journals, etc {#2-dot-describe-how-a-library-catalog-is-used-to-find-books-journals-etc}


### <span class="org-todo todo ___">[ ]</span> 3. List the appropriate databases for medical literature searches {#3-dot-list-the-appropriate-databases-for-medical-literature-searches}


### <span class="org-todo todo ___">[ ]</span> 4. Classify types of scholarly articles {#4-dot-classify-types-of-scholarly-articles}


### <span class="org-todo todo ___">[ ]</span> [Science of Learning](https://vicportal.med.uvm.edu/bbcswebdav/pid-34202-dt-content-rid-134645%5F1/xid-134645%5F1) {#science-of-learning}


#### <span class="org-todo todo ___">[ ]</span> Understand the foundations of memory encoding, storage, and retrieval in the context of medical student learning {#understand-the-foundations-of-memory-encoding-storage-and-retrieval-in-the-context-of-medical-student-learning}


#### <span class="org-todo todo ___">[ ]</span> Understand, remember, and apply the six evidence-based cognitive learning strategies {#understand-remember-and-apply-the-six-evidence-based-cognitive-learning-strategies}


#### <span class="org-todo todo ___">[ ]</span> Understand self-regulated learning and metacognition as these concepts apply to planning, monitoring, and controlling processes of effective and strategic learning {#understand-self-regulated-learning-and-metacognition-as-these-concepts-apply-to-planning-monitoring-and-controlling-processes-of-effective-and-strategic-learning}


#### <span class="org-todo todo ___">[ ]</span> Differentiate between lower=order and higher-order cognitive processes as they relate to applying the Science of Learning {#differentiate-between-lower-order-and-higher-order-cognitive-processes-as-they-relate-to-applying-the-science-of-learning}


## <span class="org-todo todo TODO">TODO</span> Organic Chemistry {#organic-chemistry}


### <span class="org-todo done _X_">[X]</span> 1. Identify the functional groups present in an organic compounds, given its structure {#1-dot-identify-the-functional-groups-present-in-an-organic-compounds-given-its-structure}

-   [Functional Group]({{< relref "functional_group" >}})


### <span class="org-todo todo ___">[ ]</span> 2. Explain why the properties of a given organic compound are largely dependent on the functional group or groups present in a compound {#2-dot-explain-why-the-properties-of-a-given-organic-compound-are-largely-dependent-on-the-functional-group-or-groups-present-in-a-compound}


### <span class="org-todo done _X_">[X]</span> 3. Differentiate chiral and achiral molecules {#3-dot-differentiate-chiral-and-achiral-molecules}

-   [Chirality]({{< relref "chirality" >}})


## <span class="org-todo todo TODO">TODO</span> Proteins and Enzymes {#proteins-and-enzymes}


### <span class="org-todo done _X_">[X]</span> 1. Recognize the 20 amino acids and classify them based on the characteristics of their side chains {#1-dot-recognize-the-20-amino-acids-and-classify-them-based-on-the-characteristics-of-their-side-chains}

-   (acidic, basic, hydrophobic/non-polar, hydrophilic/polar)
-   [Amino Acid]({{< relref "amino_acid" >}})


### <span class="org-todo todo ___">[ ]</span> 2. [Describe how amino acids are joined by peptide bonds to make a protein molecule and know the kinds of reactions involved in making and breaking these bonds](https://www.khanacademy.org/science/biology/macromolecules/proteins-and-amino-acids/a/introduction-to-proteins-and-amino-acids) {#2-dot-describe-how-amino-acids-are-joined-by-peptide-bonds-to-make-a-protein-molecule-and-know-the-kinds-of-reactions-involved-in-making-and-breaking-these-bonds}


### <span class="org-todo todo ___">[ ]</span> 3. Describe the bonds and forces that contribute to the conformation of proteins and the interaction of proteins with other biomolecules {#3-dot-describe-the-bonds-and-forces-that-contribute-to-the-conformation-of-proteins-and-the-interaction-of-proteins-with-other-biomolecules}


#### <span class="org-todo done _X_">[X]</span> Peptide, disulfide, and hydrogen bonds {#peptide-disulfide-and-hydrogen-bonds}

-   [1.3 Peptide Bond Formation and Hydrolysis]({{< relref "kaplan_biochemistry_chapter_1" >}})
-   [1.5 Tertiary and Quaternary Protein Structure]({{< relref "kaplan_biochemistry_chapter_1" >}})


#### <span class="org-todo todo ___">[ ]</span> Hydrophobic, dipole-dipole, van der Walls and electrostic interactions {#hydrophobic-dipole-dipole-van-der-walls-and-electrostic-interactions}


### <span class="org-todo todo ___">[-]</span> 4. Be able to describe the four levels of protein structure (primary, secondary, tertiary, quaternary) including: {#4-dot-be-able-to-describe-the-four-levels-of-protein-structure--primary-secondary-tertiary-quaternary--including}

-   [Amoeba Sisters - Protein Structure and Folding]({{< relref "amoeba_sisters_protein_structure_and_folding" >}})


#### <span class="org-todo done _X_">[X]</span> What types of bonding interactions hold each level together {#what-types-of-bonding-interactions-hold-each-level-together}


#### <span class="org-todo todo ___">[ ]</span> How a protein's structure relates to its cellular function {#how-a-protein-s-structure-relates-to-its-cellular-function}


### <span class="org-todo todo ___">[ ]</span> 5[. Compare and contrast the difference between conservative and non-conservative amino acid substitutions and be able to identify and assess invariant positions](https://www.khanacademy.org/test-prep/mcat/%20biomolecules/genetic-mutations/v/the-different-types-of-mutations) {#5-dot-compare-and-contrast-the-difference-between-conservative-and-non-conservative-amino-acid-substitutions-and-be-able-to-identify-and-assess-invariant-positions}


### <span class="org-todo done _X_">[X]</span> 6. Define and describe the roles of the following enzyme-related terms: {#6-dot-define-and-describe-the-roles-of-the-following-enzyme-related-terms}

-   [Cofactors and coenzymes]({{< relref "kaplan_biochemistry_chapter_2" >}})


#### <span class="org-todo done _X_">[X]</span> Coenzyme {#coenzyme}


#### <span class="org-todo done _X_">[X]</span> Cofactor {#cofactor}


#### <span class="org-todo done _X_">[X]</span> Prosthetic group {#prosthetic-group}


#### <span class="org-todo done _X_">[X]</span> Isozymes {#isozymes}

-   [Enzyme]({{< relref "enzyme" >}})


### <span class="org-todo done _X_">[X]</span> 7. Describe the type of reactions these enzymes potentiate: {#7-dot-describe-the-type-of-reactions-these-enzymes-potentiate}

-   [Enzyme classifications]({{< relref "kaplan_biochemistry_chapter_2" >}})


#### Carboxylates {#carboxylates}


#### Dehydrogenases {#dehydrogenases}


#### Isomerases {#isomerases}


#### Kinases {#kinases}


#### Phosphatases {#phosphatases}


#### Proteases {#proteases}


### <span class="org-todo todo ___">[ ]</span> 8. Understand the effect of enzymes on the activation energy of reactions, and why this makes these chemical reactions proceed more quickly {#8-dot-understand-the-effect-of-enzymes-on-the-activation-energy-of-reactions-and-why-this-makes-these-chemical-reactions-proceed-more-quickly}


### <span class="org-todo done _X_">[X]</span> 9. Define initial velocity (v<sub>o</sub>) and explain the effect of substrate concentration on enzyme velocity for a single subunit enzyme {#9-dot-define-initial-velocity--v--and-explain-the-effect-of-substrate-concentration-on-enzyme-velocity-for-a-single-subunit-enzyme}

-   [Michaelis-Menten Kinetics]({{< relref "michaelis_menten_kinetics" >}})


### <span class="org-todo done _X_">[X]</span> 10. Define the kinetic parameters K<sub>M</sub>, k<sub>cat</sub>, V<sub>max</sub>, and catalytic efficiency (K<sub>cat</sub>/K<sub>M</sub>) and understand the relationships between them {#10-dot-define-the-kinetic-parameters-k-and-understand-the-relationships-between-them}

-   [The Michaelis-Menten Model Accounts for the Kinetic Properties of Many Enzymes]({{< relref "the_michaelis_menten_model_accounts_for_the_kinetic_properties_of_many_enzymes" >}})


### <span class="org-todo done _X_">[X]</span> 12. Interpret Michaelis-Menten and Lineweaver-Burk plots {#12-dot-interpret-michaelis-menten-and-lineweaver-burk-plots}

-   [Michaelis-Menten Kinetics]({{< relref "michaelis_menten_kinetics" >}})
-   [Lineweaver-Burk Plot]({{< relref "lineweaver_burk_plot" >}})


### <span class="org-todo done _X_">[X]</span> 13. Distinguish between reversible and irreversible inhibitors {#13-dot-distinguish-between-reversible-and-irreversible-inhibitors}

-   [Regulatory molecules]({{< relref "khan_enzyme_regulation" >}})


### <span class="org-todo todo ___">[-]</span> 14. Explain how competitive, noncompetitive (mixed) and uncompetitive inhibitors react with enzymes and how that affects their kinetic parameters {#14-dot-explain-how-competitive-noncompetitive--mixed--and-uncompetitive-inhibitors-react-with-enzymes-and-how-that-affects-their-kinetic-parameters}

-   [Regulatory molecules]({{< relref "khan_enzyme_regulation" >}})


## <span class="org-todo todo WAIT">WAIT</span> Statistics {#statistics}


### <span class="org-todo done _X_">[X]</span> 1. Define, calculate, and interpret descriptive statistics concepts: mean, median, mode, range, and standard deviation {#1-dot-define-calculate-and-interpret-descriptive-statistics-concepts-mean-median-mode-range-and-standard-deviation}

-   [Biostatistics]({{< relref "biostatistics" >}})


### <span class="org-todo done _X_">[X]</span> 2. Describe the influence that symmetric, skewed, and bimodal distributions have on the mean, median, and mode {#2-dot-describe-the-influence-that-symmetric-skewed-and-bimodal-distributions-have-on-the-mean-median-and-mode}

-   [Biostatistics]({{< relref "biostatistics" >}})


### <span class="org-todo done _X_">[X]</span> 3. Define and indicate how you would apply the following: {#3-dot-define-and-indicate-how-you-would-apply-the-following}

-   [Biostatistics]({{< relref "biostatistics" >}})


#### <span class="org-todo done _X_">[X]</span> Null hypothesis {#null-hypothesis}


#### <span class="org-todo done _X_">[X]</span> Level of significance {#level-of-significance}


#### <span class="org-todo done _X_">[X]</span> P-value {#p-value}


#### <span class="org-todo done _X_">[X]</span> Statistical significance {#statistical-significance}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
