+++
title = "Fraction of inspired oxygen"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Fraction of [Oxygen]({{< relref "oxygen" >}}) (by volume) in the [inspired]({{< relref "inspiration" >}}) air


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration**

    <!--list-separator-->

    -  [Fraction of inspired oxygen]({{< relref "fraction_of_inspired_oxygen" >}})

        -   Fraction of [Oxygen]({{< relref "oxygen" >}}) (by volume) in the [inspired]({{< relref "inspiration" >}}) air
        -   Room air = 21%


### Unlinked references {#unlinked-references}

[Show unlinked references]
