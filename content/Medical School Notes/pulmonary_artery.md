+++
title = "Pulmonary artery"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Brings deoxygenated blood from the [Heart]({{< relref "heart" >}}) to the [lungs]({{< relref "lung" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Pulmonary trunk]({{< relref "pulmonary_trunk" >}}) {#pulmonary-trunk--pulmonary-trunk-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Gives rise to left and right [pulmonary arteries]({{< relref "pulmonary_artery" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Ductus arteriosus]({{< relref "ductus_arteriosus" >}}): connects [pulmonary artery]({{< relref "pulmonary_artery" >}}) and [aorta]({{< relref "aorta" >}})

    ---


#### [Ductus arteriosus]({{< relref "ductus_arteriosus" >}}) {#ductus-arteriosus--ductus-arteriosus-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Connects [pulmonary artery]({{< relref "pulmonary_artery" >}}) and [aorta]({{< relref "aorta" >}})

    ---


#### [Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.]({{< relref "describe_the_common_clinical_procedures_involving_the_heart_include_a_description_of_the_procedure_s_and_the_structure_s_that_are_accessed_or_repaired" >}}) {#describe-the-common-clinical-procedures-involving-the-heart-dot-include-a-description-of-the-procedure--s--and-the-structure--s--that-are-accessed-or-repaired-dot--describe-the-common-clinical-procedures-involving-the-heart-include-a-description-of-the-procedure-s-and-the-structure-s-that-are-accessed-or-repaired-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Clinical procedures related to heart structures: > Cardiac catheterization:**

    Radiopaque catheter inserted into peripheral vein (e.g. femoral vein) and passed under fluoroscopic control into the [right atrium]({{< relref "right_atrium" >}}), [right ventricle]({{< relref "right_ventricle" >}}), [pulmonary trunk]({{< relref "pulmonary_trunk" >}}) and [pulmonary arteries]({{< relref "pulmonary_artery" >}}) respectively

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
