+++
title = "Brachial plexus"
author = ["Arif Ahsan"]
date = 2021-09-17T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Diagrams {#diagrams}



### Realistic {#realistic}

{{< figure src="/ox-hugo/_20210917_154654screenshot.png" >}}


### Road map {#road-map}

{{< figure src="/ox-hugo/_20210917_170218screenshot.png" width="800" >}}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the neurovasculature of the intercostal spaces.]({{< relref "describe_the_neurovasculature_of_the_intercostal_spaces" >}}) {#describe-the-neurovasculature-of-the-intercostal-spaces-dot--describe-the-neurovasculature-of-the-intercostal-spaces-dot-md}

<!--list-separator-->

-  **🔖 From Accessing an Intercostal Space > Intercostal Nerves**

    _1st intercostal nerve_ contributes to the [brachial plexus]({{< relref "brachial_plexus" >}}) as well as innervating the region of the first intercostal space

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
