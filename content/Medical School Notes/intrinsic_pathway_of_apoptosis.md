+++
title = "Intrinsic pathway of apoptosis"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   One pathway of [apoptosis]({{< relref "define_apoptosis_and_necrosis" >}})


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family**

    <!--list-separator-->

    -  Bcl2 proteins regulate the [intrinsic pathway]({{< relref "intrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1026)

        -   Bcl2 proteins are a major class of intracellular regulators of the intrinsic pathway of apoptosis
            -   Controls the release of [cytochrome c]({{< relref "cytochrome_c" >}}) and other intermembrane mitochondrial proteins into the cytosol
            -   Some are _pro-apoptotic_ -> promote apoptosis by enhancing release
            -   Some are _anti-apoptotic_ -> inhibit apoptosis by blocking release
        -   Pro- and anti-apoptotic Bcl2 proteins can bind to each other in various combinations to form heterodimers -> can **inhibit each other's function**
            -   Largely determines whether a mammalian cell lives or dies by the intrinsic pathway of apoptosis

        <!--list-separator-->

        -  [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}})

            -   **Pro-apoptotic** effector Bcl2 family proteins
            -   **At least one of these is required** for the intrinsic pathway of apoptosis to operate

        <!--list-separator-->

        -  [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) and [BclX<sub>L</sub>]({{< relref "bclx_l" >}})

            -   **Anti-apoptotic**
            -   Bind to pro-apoptotic Bcl2 family proteins -> inhibit apoptosis

        <!--list-separator-->

        -  [BH3-only proteins]({{< relref "bh3_only_protein" >}})

            -   Cell either produces or activates BH3-only proteins in response to an apoptotic stimulus
            -   Provide the crucial link between apoptotic stimuli and the intrinsic pathway of apoptosis
                -   Different stimuli activates different BH3-only proteins
            -   Thought to promote apoptotis mainly by inhibiting anti-apoptotic Bcl2 family proteins
            -   BH3 domain binds to a long hydrophobic groove on anti-apoptotic Bcl2 family proteins -> neutralizes their activity
                -   Enables the aggregation of [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}}) on the surface of mitochondria -> triggers release of intermembrane mitochondrial proteins -> induction of apoptosis
            -   Some bind to Bax and Bak directly to stimulate their aggregation

            <!--list-separator-->

            -  [Bid]({{< relref "bid" >}})

                -   Normally inactive
                -   When [death receptors]({{< relref "death_receptor" >}}) activate the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}), [Caspase-8]({{< relref "caspase_8" >}}) cleaves Bid -> activates Bid -> translocates to outer mitochondrial membrane -> inhibits anti-apoptotic Bcl2 family proteins -> **amplification of death signal**

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Cytochrome c and Apaf1**

    <!--list-separator-->

    -  The [intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}}) depends on mitochondria (p. 1025)

        -   Cytochrome c is a water-soluble component of the mitochondrial electron transport chain
        -   When released into the cytosol, cytochrome c binds to [Apaf1]({{< relref "apoptotic_protease_activating_factor_1" >}}) -> Apaf1 oligomerizes into a wheel-like heptamer called an [apoptosome]({{< relref "apoptosome" >}}) -> recruits [caspase-9]({{< relref "caspase_9" >}}) -> activation of downstream executioner caspases -> induction of apoptosis


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-27 Mon] </span></span> > Cell Death > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the intrinsic pathway of apoptosis and the stimuli leading to this process]({{< relref "intrinsic_pathway_of_apoptosis" >}})


#### [Cytochrome c]({{< relref "cytochrome_c" >}}) {#cytochrome-c--cytochrome-c-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [Intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}})

    ---


#### [Caspase]({{< relref "caspase" >}}) {#caspase--caspase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Involved in both [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}) and [Intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}})

    ---


#### [Bid]({{< relref "bid" >}}) {#bid--bid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Links the [intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}}) and [extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}})

    ---


#### [B-cell lymphoma 2 family]({{< relref "b_cell_lymphoma_2_family" >}}) {#b-cell-lymphoma-2-family--b-cell-lymphoma-2-family-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Regulate the [intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
