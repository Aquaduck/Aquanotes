+++
title = "Hydroxycarbaminde"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept", "drug"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Treating sickle cell anemia]({{< relref "treating_sickle_cell_anemia" >}}) {#treating-sickle-cell-anemia--treating-sickle-cell-anemia-dot-md}

<!--list-separator-->

-  **🔖 Treatment > Drugs**

    [Hydroxyurea]({{< relref "hydroxycarbaminde" >}}) should be considered standard care, but is grossly underutilized

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
