+++
title = "Carboxylic Acid"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Lumen - Functional Group Names, Properties, and Reactions]({{< relref "lumen_functional_group_names_properties_and_reactions" >}}) {#lumen-functional-group-names-properties-and-reactions--lumen-functional-group-names-properties-and-reactions-dot-md}

<!--list-separator-->

-  **🔖 Functional Groups**

    <!--list-separator-->

    -  [Carboxylic Acids]({{< relref "carboxylic_acid" >}})

        <!--list-separator-->

        -  Physical Properties

            -   Act as **both hydrogen bond acceptors and donors**
                -   H bond acceptor <- carbonyl group
                -   H bond donor <- hydroxyl group
            -   Tendency to "self-associate" -> exist in dimeric pairs when in nonpolar media
            -   Resonance stabilized
                -   Increased acidity and higher boiling points than other acids
            -   **Polar**
            -   **Weak acids** -> do not fully dissociate in neutral aqueous solution


#### [Kaplan Organic Chemistry Chapter 1]({{< relref "kaplan_organic_chemistry_chapter_1" >}}) {#kaplan-organic-chemistry-chapter-1--kaplan-organic-chemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 1.4 Carboxylic Acids and Derivatives**

    <!--list-separator-->

    -  [Carboxylic Acids]({{< relref "carboxylic_acid" >}})

        -   Contain both a carbonyl group and a hydroxyl group on a terminal carbon
        -   Terminal functional group -> associated carbon numbered 1 unless superceded
        -   The most oxidized functional group on the MCAT
            -   3 bonds to oxygen
            -   Thus, often the highest priority functional group

        {{< figure src="/ox-hugo/_20210714_192616screenshot.png" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
