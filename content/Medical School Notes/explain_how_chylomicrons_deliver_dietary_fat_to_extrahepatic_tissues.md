+++
title = "Explain how chylomicrons deliver dietary fat to extrahepatic tissues."
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}


### Delivering dietary fat to extrahepatic tissues via [chylomicrons]({{< relref "chylomicron" >}}) {#delivering-dietary-fat-to-extrahepatic-tissues-via-chylomicrons--chylomicron-dot-md}

-   The formed chylomicrons are then **exocytosed into the lymphatics** -> **enter venous circulation**
-   Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues
    -   [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**
        -   Done through its effects on gene transcription
    -   [Insulin]({{< relref "insulin" >}}) also **increases glucose uptake in skeletal muscle and adipose tissue**
-   LPL will cleave the triacylglycerol to **either** _three [fatty acids]({{< relref "fatty_acid" >}}) and glycerol_ or _two fatty acids and a [monoacylglycerol]({{< relref "monoacylglycerol" >}})_
    -   Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})
-   [Fatty acids]({{< relref "fatty_acid" >}}) will bind to various _fatty acid binding proteins_ -> able to traverse the cell and enter the tissue beneath
    -   In muscle, fatty acids will be oxidized for energy and/or stored
    -   In lactating [mammary glands]({{< relref "mammary_gland" >}}), fatty acids + monoacylglycerol become components of [milk]({{< relref "milk" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-25 Mon] </span></span> > Muscle Intermediary Metabolism II > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
