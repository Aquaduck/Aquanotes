+++
title = "Describe the structure of the thymus (include changes with age.)"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}}) {#from-lymphatic-and-immune-systems-ppt--lymphatic-and-immune-systems-ppt-dot-md}


### [Thymus]({{< relref "thymus" >}}) {#thymus--thymus-dot-md}

-   The function of the thymus is to support [T-lymphocyte]({{< relref "t_lymphocyte" >}}) development (Stage 1), but remove autoreactive T-cells
-   The thymus has a central, pale, [multilobate medulla]({{< relref "medulla_of_the_thymus" >}}) covered by a dark [cortex]({{< relref "cortex_of_the_thymus" >}})
    -   There is a dense CT capsule, but **no lymphatics or sinuses**
-   Small but easily identifiable in the **central, anterior chest** during the first 10-12 years of life
    -   The thymus involutes in adults and is mostly replaced with [adipose cells]({{< relref "adipose_cell" >}}) -> makes it difficult to locate despite remaining active
-   The thymus **does not contain B-cell follicles**


#### [Thymus epithelium]({{< relref "epithelium_of_the_thymus" >}}) {#thymus-epithelium--epithelium-of-the-thymus-dot-md}

-   Unlike most epithelium, thymic epithelium **loses its connection with the surface endoderm**
-   Thymus epithelium forms a blood/thymic barrier
    -   A basement membrane separates the thymus from its capsule and from the blood vessels within the thymus
-   Internal part of the thymus is a sponge-like network of epithelial cells filled with [lymphocytes]({{< relref "lymphocyte" >}})
-   Epithelial cells in the [Cortex of the thymus]({{< relref "cortex_of_the_thymus" >}}) assist precursor T-cells to rearrange their [TCR]({{< relref "t_cell_receptor" >}}) genes and become naive T-cells
    -   In the [Medulla of the thymus]({{< relref "medulla_of_the_thymus" >}}) epithelial cells direct elimination of autoreactive lymphocytes and help [T-cell]({{< relref "t_lymphocyte" >}}) differentiation into helper or suppressor cells
-   Thymus epithelial cells form **broad loose sheets of cells** (with desmosomes) that are **heavily infiltrated by [lymphocytes]({{< relref "lymphocyte" >}}) in between the epithelial cells**


#### [Cortex of the thymus]({{< relref "cortex_of_the_thymus" >}}) {#cortex-of-the-thymus--cortex-of-the-thymus-dot-md}

{{< figure src="/ox-hugo/_20211009_165706screenshot.png" caption="Figure 1: Cortex of the thymus" width="500" >}}

-   Many more [lymphocytes]({{< relref "lymphocyte" >}}) than epithelial cells
-   [T-lymphocytes]({{< relref "t_lymphocyte" >}}) are rearranging their [TCR]({{< relref "t_cell_receptor" >}})
-   Many of the lymphocytes are unsuccessful -> die and are engulfed by [macrophages]({{< relref "macrophage" >}})
    -   Seen as **white holes in the dark cortex**


#### [Medulla of the thymus]({{< relref "medulla_of_the_thymus" >}}) {#medulla-of-the-thymus--medulla-of-the-thymus-dot-md}

{{< figure src="/ox-hugo/_20211009_165743screenshot.png" caption="Figure 2: Medulla of the thymus (and a bit of cortex and capsule in the upper right)" width="700" >}}

-   Epithelial cells are more prominant than in the cortex
    -   Cause the pallor
-   Function of the [epithelial cells]({{< relref "epithelium_of_the_thymus" >}}):
    -   Test the new [T-cells]({{< relref "t_lymphocyte" >}}) for self-reactivity and HLA molecule recognition
        -   Eliminate those with innapropriate reactivity
    -   Help [T-lymphocytes]({{< relref "t_lymphocyte" >}}) develop into helper or suppressor cells
-   Some medullary epithelial cells form [Hassall's corpuscles]({{< relref "hassall_s_corpuscle" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon] </span></span> > Lymphatic and Immune Systems > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
