+++
title = "Describe the concept and determinants of blood pressure."
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-18 Mon] </span></span> > Regional and Peripheral Circulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Describe the concept and determinants of blood pressure.]({{< relref "describe_the_concept_and_determinants_of_blood_pressure" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
