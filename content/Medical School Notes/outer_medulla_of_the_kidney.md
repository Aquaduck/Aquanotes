+++
title = "Outer medulla of the kidney"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Part of the [Kidney]({{< relref "kidney" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Ascending thick limb of the loop of Henle**

    Marks the border between the [inner]({{< relref "inner_medulla_of_the_kidney" >}}) and [outer medulla]({{< relref "outer_medulla_of_the_kidney" >}})

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Descending thin limb of the loop of Henle**

    The descending thin limbs **begin at the same level in all nephrons** - the point where they connect to proximal straight tubule in the [outer medulla]({{< relref "outer_medulla_of_the_kidney" >}})

    ---


#### [Ascending thick limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}}) {#ascending-thick-limb-of-the-loop-of-henle--ascending-thick-limb-of-the-loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Marks the border between the [Inner medulla of the kidney]({{< relref "inner_medulla_of_the_kidney" >}}) and the [Outer medulla of the kidney]({{< relref "outer_medulla_of_the_kidney" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
