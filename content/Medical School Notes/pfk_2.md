+++
title = "PFK-2"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}}) {#list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated--list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated-dot-md}

<!--list-separator-->

-  **🔖 Examples in glucose catabolism > Covalent modification**

    [PFK-2]({{< relref "pfk_2" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Glycolysis**

    <!--list-separator-->

    -  [6-Phosphofructo-2-kinase]({{< relref "pfk_2" >}}) (PFK-2)

        -   Liver enzyme that expresses **both** a _kinase_ & _phosphatase_ activity
        -   The product, [fructose-2,6-bisphosphate]({{< relref "fructose_2_6_bisphosphate" >}}), is formed when the kinase catalytic site is active
            -   This product **uniquely regulates glycolysis vs. gluconeogenesis**
                -   Stimulates PFK-1 -> stimulates glycolysis
                -   Inhibits fructose-1,6-bisphosphatase -> inhibits gluconeogenesis
        -   The _kinase_ component is a [PKA]({{< relref "protein_kinase_a" >}}) substrate
            -   **Inactivated by phosphorylation** -> **increases activity** of the _phosphatase_ component
        -   In the fasting state (gluconeogenesis favored in liver) -> [[F-2,6-bP]({{< relref "fructose_2_6_bisphosphate" >}})] is limited -> **PFK-1 not stimulated** -> further favoring of gluconeogenesis


#### [Identify the fructose metabolite that reciprocally regulates the glycolytic and gluconeogenic pathways within the liver and the enzyme that catalyzes its formation]({{< relref "identify_the_fructose_metabolite_that_reciprocally_regulates_the_glycolytic_and_gluconeogenic_pathways_within_the_liver_and_the_enzyme_that_catalyzes_its_formation" >}}) {#identify-the-fructose-metabolite-that-reciprocally-regulates-the-glycolytic-and-gluconeogenic-pathways-within-the-liver-and-the-enzyme-that-catalyzes-its-formation--identify-the-fructose-metabolite-that-reciprocally-regulates-the-glycolytic-and-gluconeogenic-pathways-within-the-liver-and-the-enzyme-that-catalyzes-its-formation-dot-md}

The metabolite is [Fructose-2,6-bisphosphate]({{< relref "fructose_2_6_bisphosphate" >}}) and it is formed by [PFK-2]({{< relref "pfk_2" >}})

---


#### [Fructose-2,6-bisphosphate (feed-forward regulation)]({{< relref "fructose_2_6_bisphosphate_feed_forward_regulation" >}}) {#fructose-2-6-bisphosphate--feed-forward-regulation----fructose-2-6-bisphosphate-feed-forward-regulation-dot-md}

<!--list-separator-->

-  **🔖 Synthesis**

    <!--list-separator-->

    -  [PFK-2]({{< relref "pfk_2" >}}) + [FBPase-2]({{< relref "fructose_bisposphatase_2" >}})

        -   Two domains of the same bifunctional enzyme


### Unlinked references {#unlinked-references}

[Show unlinked references]
