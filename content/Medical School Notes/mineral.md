+++
title = "Mineral"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Absorption (p. 1144)**

    <!--list-separator-->

    -  [Mineral]({{< relref "mineral" >}}) absorption

        <!--list-separator-->

        -  [Calcium]({{< relref "calcium" >}})

            -   Blood levels of ionic calcium determine the absorption of dietary calcium
            -   When blood levels of ionic calcium drop -> [PTH]({{< relref "parathyroid_hormone" >}}) secreted -> stimulates **release of calcium ions from bone matrices** + **increases reabsorption of calcium by [kidneys]({{< relref "kidney" >}})**
            -   [PTH]({{< relref "parathyroid_hormone" >}}) upregulates the activation of [vitamin D]({{< relref "vitamin_d" >}}) in the [kidney]({{< relref "kidney" >}}) -> facilitates intestinal calcium ion absorption


### Unlinked references {#unlinked-references}

[Show unlinked references]
