+++
title = "Endochondral bone formation"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone formation**

    <!--list-separator-->

    -  [Endochondral bone formation]({{< relref "endochondral_bone_formation" >}})

        -   Most bones develop this way (e.g. long bones)
        -   There are **five recognizable zones** that occur in sequence from the [Epiphysis]({{< relref "epiphysis" >}}) into the [Metaphysis]({{< relref "metaphysis" >}})
            1.  Resting zone
            2.  Proliferating zone
            3.  Hypertrophic zone
            4.  Cartilage calcification zone
            5.  Ossification zone

        {{< figure src="/ox-hugo/_20210930_150657screenshot.png" caption="Figure 1: Five zones of bone development in sequence (left to right)" width="700" >}}

        <!--list-separator-->

        -  Steps

            1.  [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) first forms a model of the bone that is then converted to bone
            2.  A bone collar appears around the shaft as the center (destined to be the [Diaphysis]({{< relref "diaphysis" >}})) of the "cartilage model" and is ossified
                -   Called the _primary center of ossification_
                -   Ossification center expands, converting most of the cartilage (except the epiphyses) to bone with an expanding marrow cavity
            3.  Secondary centers of ossification are initiated in the middle of each [Epiphysis]({{< relref "epiphysis" >}})
            4.  A region of cartilage is preserved between the [Epiphysis]({{< relref "epiphysis" >}}) and the [Diaphysis]({{< relref "diaphysis" >}}) -> becomes the [Growth plate]({{< relref "epiphyseal_plate" >}})
            5.  A region of cartilage at the ends is preserved to form the articular surfaces
            6.  Cartilage of [Growth plate]({{< relref "epiphyseal_plate" >}}) proliferates to lengthen the bone
            7.  As proliferating cartilage of the [Growth plate]({{< relref "epiphyseal_plate" >}}) grows away from the [Diaphysis]({{< relref "diaphysis" >}}) (extending the length of the bone) -> cartilage closest to the diaphysis is **converted to ossified bone**


### Unlinked references {#unlinked-references}

[Show unlinked references]
