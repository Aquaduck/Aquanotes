+++
title = "Methemoglobin"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [Hemoglobin]({{< relref "hemoglobin" >}}) but with ferric (3+) iron instead of ferrous (2+) iron


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Methemoglobin]({{< relref "methemoglobin" >}}): [Iron]({{< relref "iron" >}}) is ferric (3+) instead of ferrous (2+)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
