+++
title = "Compare and contrast sample and population"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}


### [Population]({{< relref "population" >}}) {#population--population-dot-md}

-   The total number of inhabitants in a region from which a sample is drawn for statistical measurement


### [Sample]({{< relref "sample" >}}) {#sample--sample-dot-md}

-   A group of people that is representative of a larger population


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > The Practice of Evidence-Based Medicine: Inferential Statistics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Compare and contrast sample and population]({{< relref "compare_and_contrast_sample_and_population" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
