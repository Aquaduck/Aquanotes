+++
title = "Arp 2/3 complex"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   An [actin-related protein]({{< relref "actin_related_protein" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Provide examples of how actin-binding proteins regulate actin filament function.]({{< relref "provide_examples_of_how_actin_binding_proteins_regulate_actin_filament_function" >}}) {#provide-examples-of-how-actin-binding-proteins-regulate-actin-filament-function-dot--provide-examples-of-how-actin-binding-proteins-regulate-actin-filament-function-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Actin-nucleating factors accelerate polymerization and generate branched or straight filaments (p. 906)**

    [Arp 2/3 complex]({{< relref "arp_2_3_complex" >}}) nucleates actin filament growth from the **minus end**, allowing rapid elongation at the plus end

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
