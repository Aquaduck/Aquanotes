+++
title = "Methylmalonyl-CoA mutase"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Catalyzes [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) isomerization to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via
-   Dependent on vitamin [B12]({{< relref "cobalamin" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [List the products of the ß-oxidation of odd-chain fatty acids.]({{< relref "list_the_products_of_the_ß_oxidation_of_odd_chain_fatty_acids" >}}) {#list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot--list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Products of beta-oxidation of odd-chain fatty acids**

    [Propionyl-CoA]({{< relref "propionyl_coa" >}}) -([Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}}))> [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) -> [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) -([Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}}))> [succinyl-CoA]({{< relref "succinyl_coa" >}}) -> TCA cycle

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 L-methylmalonyl-CoA > From Fat Metabolism in Muscle & Adipose Tissue**

    L-methylmalonyl-CoA converted to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via [B12]({{< relref "cobalamin" >}}) dependent isomerization catalyzed by [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})

    ---

<!--list-separator-->

-  **🔖 Beta-oxidation > From Fat Metabolism in Muscle & Adipose Tissue**

    **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Fatty acid catabolism > Oxidation phase**

    **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**

    ---


#### [Detail the vitamin requirements for the metabolism of propionyl-CoA.]({{< relref "detail_the_vitamin_requirements_for_the_metabolism_of_propionyl_coa" >}}) {#detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot--detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Vitamin requirements for the metabolism of propionyl-CoA**

    [B12]({{< relref "cobalamin" >}}): Isomerization of [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
