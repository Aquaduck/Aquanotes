+++
title = "Cell cycle"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [M-phase]({{< relref "m_phase" >}}) {#m-phase--m-phase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A phase in the [cell cycle]({{< relref "cell_cycle" >}}) where [mitosis]({{< relref "mitosis" >}}) occurs

    ---


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes**

    6 billion replicated during [S phase]({{< relref "s_phase" >}}) of the [cell cycle]({{< relref "cell_cycle" >}})

    ---


#### [G2/M transition checkpoint]({{< relref "g2_m_transition_checkpoint" >}}) {#g2-m-transition-checkpoint--g2-m-transition-checkpoint-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A checkpoint in the [cell cycle]({{< relref "cell_cycle" >}}) that triggers early [mitosis]({{< relref "mitosis" >}})

    ---


#### [Cyclin-dependent kinase]({{< relref "cyclin_dependent_kinase" >}}) {#cyclin-dependent-kinase--cyclin-dependent-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Regulate [cell cycle]({{< relref "cell_cycle" >}}) progression by modulating activity of other proteins via phosphorylation

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
