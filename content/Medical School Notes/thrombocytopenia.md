+++
title = "Thrombocytopenia"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   Too few [platelets]({{< relref "thrombocyte" >}}) are formed, potentially leading to excessive bleeding


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Disorders of Platelets (p. 813)**

    [Thrombocytopenia]({{< relref "thrombocytopenia" >}}): too few platelets

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
