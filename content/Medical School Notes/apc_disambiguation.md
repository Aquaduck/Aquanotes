+++
title = "APC disambiguation"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## APC can refer to: {#apc-can-refer-to}

-   [Antigen presenting cell]({{< relref "antigen_presenting_cell" >}})
-   [Anaphase promoting complex/cyclosome]({{< relref "anaphase_promoting_complex_cyclosome" >}})
-   [Adenomatous polyposis coli]({{< relref "adenomatous_polyposis_coli" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
