+++
title = "Macula densa"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Mark the end of the [thick ascending limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}}) and the beginning of the [distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}})
-   Contribute to the control of [glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}) and to the control of [renin]({{< relref "renin" >}}) secretion


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Distal convoluted tubule**

    Marked by the [Macula densa]({{< relref "macula_densa" >}}) cells at the vascular pole of the Bowman's capsule

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Ascending thick limb of the loop of Henle**

    [Macula densa]({{< relref "macula_densa" >}}) cells found at this point between the two arterioles and marks the end of the ascending thick limb of the loop of Henle

    ---


#### [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}}) {#explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result--explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Juxtaglomerular apparatus (p. 21)**

    <!--list-separator-->

    -  [Macula densa]({{< relref "macula_densa" >}}) cells

        -   Detectors of flow rate and composition of fluid in nephron at th every end of the [thick ascending limb]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}})
        -   Contribute to the control of [glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}) and to the control of [renin]({{< relref "renin" >}}) secretion


#### [Ascending thick limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}}) {#ascending-thick-limb-of-the-loop-of-henle--ascending-thick-limb-of-the-loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Contain [macula densa]({{< relref "macula_densa" >}}) cells by the juxtaglomerular apparatus

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration**

    <!--list-separator-->

    -  [Macula densa]({{< relref "macula_densa" >}})

        -   A specialized group of tall cuboidal cells in the [distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}}) of the kidney
        -   Part of the [Juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}})
        -   Senses [Sodium]({{< relref "sodium" >}}) and Chloride levels and controls [Renin]({{< relref "renin" >}}) release, glomerular filtration, and renal blood flow via signaling pathways
            -   Includes COX-2 mediated prostaglandin secretion (_tubuloglomerular feedback_)

        <!--list-separator-->

        -  Feedback mechanisms

            -   In response to **increased NaCl**
                -   Triggers release of [Adenosine]({{< relref "adenosine" >}}) -> vasoconstriction of the [afferent arteriole]({{< relref "afferent_arteriole_of_the_kidney" >}}) -> **decrease in glomerular filtration rate**
            -   In reponse to **decreased NaCl**
                -   Triggers release of [Renin]({{< relref "renin" >}}) -> vasoconstriction of the [efferent arteriole]({{< relref "efferent_arteriole_of_the_kidney" >}}) -> **increase in glomerular filtration rate**


### Unlinked references {#unlinked-references}

[Show unlinked references]
