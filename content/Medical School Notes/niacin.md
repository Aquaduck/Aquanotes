+++
title = "Niacin"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   AKA _Vitamin B<sub>3</sub>_
-   [B vitamin]({{< relref "b_vitamin" >}})


## From First Aid Section II Biochemistry Nutrition {#from-first-aid-section-ii-biochemistry-nutrition}


### Function {#function}

-   Constituent of [NAD<sup>+</sup>]({{< relref "nadh" >}}), NADP<sup>​+</sup>
-   Derived from [tryptophan]({{< relref "tryptophan" >}})
-   Synthesis requires [vitamins B<sub>2</sub>]({{< relref "riboflavin" >}}) and [B<sub>6</sub>]({{< relref "pyridoxine" >}})
-   **N\*AD derived from \*N\*iacin**


## Backlinks {#backlinks}


### 9 linked references {#9-linked-references}


#### [Pyridoxine]({{< relref "pyridoxine" >}}) {#pyridoxine--pyridoxine-dot-md}

<!--list-separator-->

-  **🔖 From First Aid 2015 > Function**

    [Niacin]({{< relref "niacin" >}})

    ---


#### [NADH]({{< relref "nadh" >}}) {#nadh--nadh-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Niacin]({{< relref "niacin" >}}) is a component

    ---


#### [List the vitamins, both by number and by name, and indicate, in broad strokes, why they are important to the metabolic processes and/or pathways specified above]({{< relref "list_the_vitamins_both_by_number_and_by_name_and_indicate_in_broad_strokes_why_they_are_important_to_the_metabolic_processes_and_or_pathways_specified_above" >}}) {#list-the-vitamins-both-by-number-and-by-name-and-indicate-in-broad-strokes-why-they-are-important-to-the-metabolic-processes-and-or-pathways-specified-above--list-the-vitamins-both-by-number-and-by-name-and-indicate-in-broad-strokes-why-they-are-important-to-the-metabolic-processes-and-or-pathways-specified-above-dot-md}

<!--list-separator-->

-  [B3]({{< relref "niacin" >}})

    -   NAD+
    -   Redox
    -   PDH complex, alpha-KG, many more


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    [Malate DH]({{< relref "malate_dehydrogenase" >}}) requires [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---

<!--list-separator-->

-  **🔖 Fatty acid oxidation**

    [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---

<!--list-separator-->

-  **🔖 TCA cycle**

    [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---

<!--list-separator-->

-  **🔖 Pyruvate DH complex**

    [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---

<!--list-separator-->

-  **🔖 Glycolysis**

    [NADH]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By vitamin**

    <!--list-separator-->

    -  [Niacin]({{< relref "niacin" >}}) (B3)

        -   Pyruvate DH complex
        -   alpha-KG DH
        -   Malate DH
        -   Isocitrate DH


### Unlinked references {#unlinked-references}

[Show unlinked references]
