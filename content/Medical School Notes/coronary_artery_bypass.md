+++
title = "Coronary artery bypass"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.]({{< relref "describe_the_common_clinical_procedures_involving_the_heart_include_a_description_of_the_procedure_s_and_the_structure_s_that_are_accessed_or_repaired" >}}) {#describe-the-common-clinical-procedures-involving-the-heart-dot-include-a-description-of-the-procedure--s--and-the-structure--s--that-are-accessed-or-repaired-dot--describe-the-common-clinical-procedures-involving-the-heart-include-a-description-of-the-procedure-s-and-the-structure-s-that-are-accessed-or-repaired-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Clinical procedures related to heart structures:**

    <!--list-separator-->

    -  [Coronary artery bypass]({{< relref "coronary_artery_bypass" >}})

        -   Segment of artery or vein is connected to ascending aorta or to proximal part of a coronary artery -> connected to coronary artery distal to stenosis
        -   Great saphenous vein is commonly harvested for coronary bypass because:
            1.  Has a diameter equal to or greater than that of oronary arteries
            2.  Can be easily dissected from lower limb
            3.  Offers relatively lengthy portion with minimum occurrence of valves or branching
        -   Use of radial artery in bypass surgery has become more common
        -   Coronary bypass graft shunts blood from aorta to a steonic coronary artery -> increase flow distal to obstrution
            -   i.e. detour around stenotic area
        -   Revascularization of myocardium may also be achieved by surgically anastomosing an internal thoracic artery with a coronary artery


### Unlinked references {#unlinked-references}

[Show unlinked references]
