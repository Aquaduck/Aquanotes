+++
title = "Diaphragm"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Parietal pleura]({{< relref "parietal_pleura" >}}) {#parietal-pleura--parietal-pleura-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The outer layer that connects to the thoracic wall, the [mediastinum]({{< relref "mediastinum" >}}), and the [diaphragm]({{< relref "diaphragm" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Embryology > Cardiac**

    [Septum transversum]({{< relref "septum_transversum" >}}) leads to [diaphragm]({{< relref "diaphragm" >}}) and liver cords

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
