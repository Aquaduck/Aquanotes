+++
title = "Kaplan Organic Chemistry Chapter 2"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
tags = ["medschool", "kaplan", "source"]
draft = false
+++

## Introduction {#introduction}

-   Isomers have the same molecular formula but different structures


## 2.1 Structural Isomers {#2-dot-1-structural-isomers}

{{< figure src="/ox-hugo/_20210707_204458screenshot.png" >}}

-   Aka <span class="underline">constitutional isomers</span>
-   Structural isomers are the **least similar of all the [isomers]({{< relref "isomers" >}})**
    -   Different chemical and physical properties

{{< figure src="/ox-hugo/_20210707_204706screenshot.png" caption="Figure 1: Example of structural isomers for C<sub>6</sub>H<sub>14</sub>" >}}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
