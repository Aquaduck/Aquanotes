+++
title = "Mitochondrion"
author = ["Arif Ahsan"]
date = 2021-07-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   An [organelle]({{< relref "organelle" >}})
-   Site of [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [β-oxidation]({{< relref "β_oxidation" >}}) {#β-oxidation--β-oxidation-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    First stage in the oxidation of [fatty acids]({{< relref "fatty_acid" >}}) in the [mitochondria]({{< relref "mitochondrion" >}})

    ---


#### [Second mitochondria-derived activator of caspases]({{< relref "second_mitochondria_derived_activator_of_caspases" >}}) {#second-mitochondria-derived-activator-of-caspases--second-mitochondria-derived-activator-of-caspases-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [mitochondrial]({{< relref "mitochondrion" >}}) protein

    ---


#### [Malonyl-CoA]({{< relref "malonyl_coa" >}}) {#malonyl-coa--malonyl-coa-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inhibits [CPT1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) -> prevents synthesized fatty acids (>C12) from entering the [Mitochondria]({{< relref "mitochondrion" >}}) and being oxidized

    ---


#### [Lippincott Bioenergetics and Oxidative Phosphorylation]({{< relref "lippincott_bioenergetics_and_oxidative_phosphorylation" >}}) {#lippincott-bioenergetics-and-oxidative-phosphorylation--lippincott-bioenergetics-and-oxidative-phosphorylation-dot-md}

<!--list-separator-->

-  **🔖 Electron Transport Chain**

    <!--list-separator-->

    -  [Mitochondrion]({{< relref "mitochondrion" >}})

        -   [ETC]({{< relref "oxidative_phosphorylation" >}}) is present in the inner mitochondrial membrane

        <!--list-separator-->

        -  Membranes of the mitochondrion

            -   _Outer membrane_ contains special pores -> freely permeable to most ions and small molecules
            -   _Inner membrane_ **impermeable** to most **small ions** (H<sup>+</sup>, Na<sup>​+</sup>, K<sup>​+</sup>) and **small molecules** (ATP, ADP, pyruvate)

        <!--list-separator-->

        -  Matrix of the mitochondrion

            -   Includes enzymes responsible for:
                -   Oxidation of
                    -   Pyruvate
                    -   Amino acids
                    -   Fatty acids (via \\(\Beta\\)-oxidation)
                -   the TCA cycle
            -   Synthesis of glucose, urea, and heme occur partially here


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles**

    <!--list-separator-->

    -  [Mitochondria]({{< relref "mitochondrion" >}})

        <!--list-separator-->

        -  Structure

            -   Rod-shaped
            -   Have an outer membrane and inner membrane
            -   Subdivided into an _intermembrane space_ between the two membranes and an inner _matrix_
            -   Granules w/in the matrix bind the divalent cations Mg<sup>2+</sup> and Ca<sup>2+</sup>

        <!--list-separator-->

        -  Enzymes and genetic apparatus

            -   Mitochondria contain the following:
                1.  All of the enzymes of the [TCA]({{< relref "citric_acid_cycle" >}}) cycle in the matrix (except for succinate DH which is in the inner membrane)
                2.  _ATP synthase_
                    -   Consists of head portion and a transmembrane H ^{+} carrier
                    -   Involved in [coupling oxidation to phosphorylation]({{< relref "oxidative_phosphorylation" >}}) of ADP to ATP
                3.  Genetic apparatus in the matrix containing:
                    -   circular DNA
                    -   mRNA
                    -   tRNA
                    -   rRNA (w/ limited coding capacity)
                    -   **Most mitochondrial proteins are encoded by nuclear DNA**

        <!--list-separator-->

        -  Origin and proliferation

            -   Mitochondria may have originated as _symbionts_ (intracellular parasites)
                -   Anaerobic eukaryotic cells endocytosed aerobic microorganisms that evolved into mitochondria, which function in oxidative processes
            -   Mitchondria proliferate by _fission_ (division) of preexisting mitochondria
                -   Typically have a 10-day life span
                -   Proteins needed to sustain mitochondria are imported into them from the cytosol

        <!--list-separator-->

        -  Mitochondrial ATP synthesis

            -   Mitochondria synthesize ATP via the Krebs cycle
            -   ATP also synthesized via [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}})

        <!--list-separator-->

        -  Condensed mitochondria

            -   Result from a conformational change in the typical form in response to **uncoupling of oxidation from phosphorylation**
            -   Size of **matrix decreased** -> matrix made **more dense**
            -   Size of **intermembrane space enlarged**
            -   Present in _brown fat cells_
                -   Produce heat rather than ATP
                    -   Special transport protein in inner membrane uncouples respiration from ATP synthesis
            -   Mitochondria swell in response to calcium, phosphate, and thyroxine -> induce increase in water uptake and uncoupling of phosphorylation
                -   [ATP]({{< relref "atp" >}}) reverses the swelling


### Unlinked references {#unlinked-references}

[Show unlinked references]
