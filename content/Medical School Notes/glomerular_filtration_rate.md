+++
title = "Glomerular filtration rate"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Macula densa]({{< relref "macula_densa" >}}) {#macula-densa--macula-densa-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Contribute to the control of [glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}) and to the control of [renin]({{< relref "renin" >}}) secretion

    ---


#### [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}}) {#explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result--explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Juxtaglomerular apparatus (p. 21) > Macula densa cells**

    Contribute to the control of [glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}) and to the control of [renin]({{< relref "renin" >}}) secretion

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    [Glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}): **the volume of filtrate formed per unit of time**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
