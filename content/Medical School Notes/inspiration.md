+++
title = "Inspiration"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The part of [ventilation]({{< relref "ventilation" >}}) whewre air is taken into the [lungs]({{< relref "lung" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Vital capacity]({{< relref "vital_capacity" >}}) {#vital-capacity--vital-capacity-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The maximum volume of air that can be expired after maximal [inspiration]({{< relref "inspiration" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Heart sound is split during [inspiration]({{< relref "inspiration" >}}) because the [pulmonic valve]({{< relref "pulmonary_valve" >}}) closing is slightly later than [aortic valve]({{< relref "aortic_valve" >}}) closing

    ---


#### [Hering-Breuer inflation reflex]({{< relref "hering_breuer_inflation_reflex" >}}) {#hering-breuer-inflation-reflex--hering-breuer-inflation-reflex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inhibits [Inspiration]({{< relref "inspiration" >}}) to prevent overinflation of the [lungs]({{< relref "lung" >}})

    ---


#### [Fraction of inspired oxygen]({{< relref "fraction_of_inspired_oxygen" >}}) {#fraction-of-inspired-oxygen--fraction-of-inspired-oxygen-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Fraction of [Oxygen]({{< relref "oxygen" >}}) (by volume) in the [inspired]({{< relref "inspiration" >}}) air

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Fraction of inspired oxygen**

    Fraction of [Oxygen]({{< relref "oxygen" >}}) (by volume) in the [inspired]({{< relref "inspiration" >}}) air

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
