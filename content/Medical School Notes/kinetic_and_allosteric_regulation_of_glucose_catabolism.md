+++
title = "Kinetic and Allosteric Regulation of Glucose Catabolism"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

_Adapted from Lippincott Illustrated Reviews: Biochemistry, 7e Chapter 5: Enzymes_
Updated version with more enzymes uploaded for August 30th workshop called "Enzymes Regulating Intermediary Carbohydrate & Fat Metabolism"


## Allosteric enzymes {#allosteric-enzymes}

-   Allosteric enzymes are regulated by molecules called effectors
    -   Effectors bind **noncovalently** at a site **other than the active site**
-   Allosteric enzymes are more sensitive in their response to changes in substrate concentrations
    -   Display a "switch-like" transition from low-to-high reaction rates as ↑[substrate] -> **sigmoidal curve**
-   Almost always composed of multiple subunits
    -   Regulatory (allosteric) site distinct from substrate-binding site
        -   May be located on a subunit that is not itself catalytic
-   _Negative effectors_: inhibit enzyme activity
-   _Positive effectors_: increase enzyme activity


### Homotropic effectors {#homotropic-effectors}

-   When the **substrate itself is the effector**
-   Most often a **positive effector**
    -   Presence of substrate at one site on the enzyme -> enhances catalytic properties of other substrate-binding sites -> **Cooperativity**
-   Show a **sigmoidal curve** when V<sub>o</sub> is plotted against [substrate]
    -   Contrasts with the hyperbola of enzymes following [Michaelis-Menten kinetics]({{< relref "michaelis_menten_kinetics" >}})


### Heterotrophic effectors {#heterotrophic-effectors}

-   **Different from substrate**
-   _Feedback inhibition_ provides cell w/ appropriate amounts of product needed by **regulating flow of substrate molecules through pathway that synthesizes that product**

{{< figure src="/ox-hugo/_20210819_154506screenshot.png" caption="Figure 1: Feedback inhibition of a metabolic pathway" width="500" >}}


## [Glycolysis]({{< relref "glycolysis" >}}) {#glycolysis--glycolysis-dot-md}

-   Occurs **primarily in the fed state**
-   All tissues **except [liver]({{< relref "liver" >}})** use glucose via glycolysis as an energy source in the fed state
    -   [Brain]({{< relref "brain" >}}) has **obligate requirement** for glucose
    -   Liver uses glucose to **initially replenish its glycogen stores**
        -   Secondarily will generate acetyl-CoA via glycolysis and pyruvate DH complex for use in cholesterol synthesis
        -   Liver **prefers to use amino acids** provided by dietary protein as energy source in fed state


### [Hexokinases]({{< relref "hexokinase" >}}) I-III {#hexokinases--hexokinase-dot-md--i-iii}

-   In most tissues (other than pancreas + liver), the first reaction in glycolytic pathway is catalyzed by one of the isozymes of hexokinase
-   Hexokinase catalyzes the **first** of three highly-regulated **irreversible** glycolytic reactions
    -   Hexokinase catalyzes ATP-dependent phosphorylation of glucose to [G6P]({{< relref "glucose_6_phosphate" >}})
        -   G6P cannot diffuse out of the cell -> allows for continuous glucose uptake by cells
-   Hexokinase enzymes are **inhibited by [G6P]({{< relref "glucose_6_phosphate" >}}), the reaction product**
    -   Low K<sub>m</sub> (high affinity) for glucose
    -   Low V<sub>max</sub> (low capacity) for glucose phosphorylation
        -   Allows for efficient phosphorylation and metabolism of glucose even w/ low tissue concentrations
    -   This makes them **allosteric enzymes**


### [Glucokinase]({{< relref "glucokinase" >}}) (hexokinase IV) {#glucokinase--glucokinase-dot-md----hexokinase-iv}

-   **Restricted to liver parenchymal cells and β-cells of pancreas**
-   Has cooperativity with [glucose]
-   Has a **much higher K<sub>m</sub> and V<sub>max</sub>**
    -   Low affinity for glucose ensures that when [glucose] is high, it will be trapped in the liver
        -   Whereas when [glucose] is low, it **will not be recognized as a substrate**
        -   This prevents [hyperglycemia]({{< relref "hyperglycemia" >}})
-   **Not subject to regulation by product inhibition**
    -   Regulated by glucokinase regulatory protein (GKRP) (_outside scope of FoCS_)


### [6-Phosphofructo-1-kinase]({{< relref "6_phosphofructo_1_kinase" >}}) (PFK-1) {#6-phosphofructo-1-kinase--6-phosphofructo-1-kinase-dot-md----pfk-1}

-   Third enzyme in glycolysis
-   **Catalyzes the rate-limiting, committed step of glycolysis**
-   The **second** irreversible glycolytic reaction
-   **Allosterically regulated** by **energy charge** of the cell
    -   ↑[ATP] **inhibits** PFK-1
    -   ↑[AMP] **stimulates** PFK-1
        -   This is because adenylate cyclase is a cytoplasmic enzyme and catalyzed in the reaction \\(2 ADP -> ATP + AMP\\)
            -   AMP:ATP ratio is the more sensitive indicator of energy charge
-   **Elevated levels of citrate** also allosterically **inhibits** PFK-1
    -   This is due to high [ATP] + [acetyl-CoA] in the mitochondria
    -   Slows [glycolysis]({{< relref "glycolysis" >}}) and allows for the synthesis of either [glycogen]({{< relref "glycogen" >}}) or fatty acid synthesis


### [Pyruvate kinase]({{< relref "pyruvate_kinase" >}}) {#pyruvate-kinase--pyruvate-kinase-dot-md}

-   Final enzyme in the glycolytic pathway
-   Catalyzes the third irreversible + second substrate-level phosphorylation reaction
-   Produces [pyruvate]({{< relref "pyruvate" >}}) from [phosphoenolpyruvate]({{< relref "phosphoenolpyruvate" >}}) (PEP)
-   **Allosterically inhibited** by [ATP]({{< relref "atp" >}}) in all tissues
-   When glucose is plentiful and PFK-1 is active, fructose 1,6-bisphosphate will feed forward and activate the enzyme
-   Deficiencies in the red blood cell isozyme causes hemolytic anemia


#### The liver isozyme is uniquely regulated {#the-liver-isozyme-is-uniquely-regulated}

-   Allosterically inhibited by ATP, _alanine_, and _long-chain fatty acids_
    -   All of which are plentiful during gluconeogenesis
-   A substrate of protein kinase A
    -   **Downregulated** by phosphorylation


### [6-Phosphofructo-2-kinase]({{< relref "pfk_2" >}}) (PFK-2) {#6-phosphofructo-2-kinase--pfk-2-dot-md----pfk-2}

-   Liver enzyme that expresses **both** a _kinase_ & _phosphatase_ activity
-   The product, [fructose-2,6-bisphosphate]({{< relref "fructose_2_6_bisphosphate" >}}), is formed when the kinase catalytic site is active
    -   This product **uniquely regulates glycolysis vs. gluconeogenesis**
        -   Stimulates PFK-1 -> stimulates glycolysis
        -   Inhibits fructose-1,6-bisphosphatase -> inhibits gluconeogenesis
-   The _kinase_ component is a [PKA]({{< relref "protein_kinase_a" >}}) substrate
    -   **Inactivated by phosphorylation** -> **increases activity** of the _phosphatase_ component
-   In the fasting state (gluconeogenesis favored in liver) -> [[F-2,6-bP]({{< relref "fructose_2_6_bisphosphate" >}})] is limited -> **PFK-1 not stimulated** -> further favoring of gluconeogenesis


## [Pyruvate]({{< relref "pyruvate" >}}) metabolism {#pyruvate--pyruvate-dot-md--metabolism}


### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

-   The mitochondrial PDH complex is allosterically regulated by product inhibition by acetyl-CoA & NADH against their respective enzymes, as well as by covalent modification


#### [Pyruvate DH kinase]({{< relref "pyruvate_dh_kinase" >}}) {#pyruvate-dh-kinase--pyruvate-dh-kinase-dot-md}

-   Phosphorylates Ser residues on the first enzyme of the PDH complex (E1)
    -   Results in significant **down-regulation of its activity**
-   Allosterically stimulated by the products of the reaction
-   Allosterically inhibited by its substrates
-   Also regulated by the energy charge <- kinase allosterically inhibited by ADP


#### [Pyruvate DH phosphatase]({{< relref "pyruvate_dh_phosphatase" >}}) {#pyruvate-dh-phosphatase--pyruvate-dh-phosphatase-dot-md}

-   Activated by high concentrations of Ca<sup>2+</sup> and Mg<sup>2​+</sup> which **reverses the effects of PDH kinase** (i.e. dephosphorylates PDH)
    -   This **ensures glucose utilization**
-   Activation by Ca<sup>2+</sup> primarily in skeletal muscle
    -   Contraction leads to Ca<sup>2+</sup> release from cellular stores
-   Activating effect of Mg<sup>2+</sup> due to **low mitochondrial concentrations of ATP**


### [Alanine aminotransferase]({{< relref "alanine_aminotransferase" >}}) {#alanine-aminotransferase--alanine-aminotransferase-dot-md}

-   A cytosolic enzyme
-   Catalyzes the readily reversible transfer of an amino group from alanine to α-ketoglutarate -> form pyruvate and glutamate
    -   Allows amino acids to be used as gluconeogenic precursors


### NAD<sup>+</sup>-linked [lactate dehydrogenase]({{< relref "lactate_dehydrogenase" >}}) {#nad-linked-lactate-dehydrogenase--lactate-dehydrogenase-dot-md}

-   A cytosolic enzyme
-   Catalyzes a **freely reversible** redox reaction between _lactate_ and _pyruvate_
-   **The only way in which lactate is utilized physiologically**
    -   Lactate released from anaerobic glyolysis occuring in RBCs + exercising muscle -> taken up by liver + used as a gluconeogenic substrate


### [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) {#pyruvate-carboxylase--pyruvate-carboxylase-dot-md}

-   [Biotin]({{< relref "biotin" >}})-requiring mitochondrial enzyme
-   Catalyzes ATP-dependent carboxylation of pyruvate to oxaloacetate
    -   Then converted to PEP by PEP carboxylase for use in gluconeogenesis
-   Allosterically activated by acetyl-CoA
    -   The ability of Acetyl-CoA to inhibit PDH complex and activate pyruvate carboxylase ensures that **the pyruvate generated in the liver mitochondria will be used as a gluconeogenic substrate**


## [Citric Acid Cycle]({{< relref "citric_acid_cycle" >}}) {#citric-acid-cycle--citric-acid-cycle-dot-md}


### [Citrate synthase]({{< relref "citrate_synthase" >}}) {#citrate-synthase--citrate-synthase-dot-md}

-   **Not allosterically regulated**
    -   [Citrate]({{< relref "citrate" >}}) itself inhibits the enzyme by competing with [oxaloacetate]({{< relref "oxaloacetate" >}})
-   [Succinyl-CoA]({{< relref "succinyl_coa" >}}) is a strong inhibitor of citrate production
    -   Strictly competitive with [acetyl-CoA]({{< relref "acetyl_coa" >}})
    -   Noncompetitive with [oxaloacetate]({{< relref "oxaloacetate" >}})


### NAD<sup>+</sup>-linked [isocitrate dehydrogenase]({{< relref "isocitrate_dehydrogenase" >}}) {#nad-linked-isocitrate-dehydrogenase--isocitrate-dehydrogenase-dot-md}

-   Catalyzes the **rate limiting and committed step of the TCA cycle**
-   Allosterically regulated by the _energy charge_ of the cell
    -   ATP inhibits
    -   ADP stimulates
-   [ADP]({{< relref "adenosine_diphosphate" >}}) is the master energy sensor in the mitochondria
    -   Due to the absence of the appropriate adenylate cyclase isozyme
    -   **↑[ADP] signals an energy deficit**
        -   Because ADP is the substrate of ATP synthesis via ATP synthase and oxidative phosphorylation
    -   [AMP]({{< relref "adenosine_monophosphate" >}}) is master switch in the [cytoplasm]({{< relref "cytoplasm" >}})


### NAD<sup>+</sup>-linked [α-ketoglutarate dehydrogenase]({{< relref "α_ketoglutarate_dehydrogenase" >}}) {#nad-linked-α-ketoglutarate-dehydrogenase--α-ketoglutarate-dehydrogenase-dot-md}

-   Allostericaly regulated by **product inhibition**
    -   i.e. succinyl-CoA and NADH
-   Mechanism is **analogous to that of the [pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}})**
    -   Consists of 3 enzymes
        -   _E1_ accepts a 5C acid rather than a 3C acid
        -   _E2_ and _E3_ are identical to those found in the pyruvate DH complex
        -   α-ketoglutarate DH is NOT subject to covalent modification by a tightly-associated kinase


### NAD<sup>+</sup>-linked [malate dehydrogenase]({{< relref "malate_dehydrogenase" >}}) {#nad-linked-malate-dehydrogenase--malate-dehydrogenase-dot-md}

-   Required for the **reversible oxidation of malate to oxaloacetate**
    -   Completes the TCA cycle
    -   **Critical reaction in gluconeogenesis**
        -   Malate - α-ketoglutarate transporter moves oxaloacetate from the mitosol to the cytosol as a **gluconeogenic precursor**


### FAD-linked [Succinate dehydrogenase]({{< relref "succinate_dehydrogenase" >}}) {#fad-linked-succinate-dehydrogenase--succinate-dehydrogenase-dot-md}

-   The **only FAD-linked dehydrogenase** in the TCA cycle
-   Embedded in inner mitochondrial membrane -> also called _Complex II of ETC_
    -   Electrons generated by oxidation of succinate -> fumarate passed to [Coenzyme Q]({{< relref "coenzyme_q" >}})
        -   Therefore, Succinate DH **is regulated by the availability of FAD**


## Fatty acid metabolism {#fatty-acid-metabolism}


### [Fatty acyl-CoA synthetase]({{< relref "fatty_acyl_coa_synthetase" >}}) {#fatty-acyl-coa-synthetase--fatty-acyl-coa-synthetase-dot-md}

-   Required to "activate" cytoplasmic fatty acids for entry into mitochondria for [β-oxidation]({{< relref "β_oxidation" >}})
-   Catalyzes formation of a fatty acyl-CoA


### [Acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}}) {#acetyl-coa-carboxylase--acetyl-coa-carboxylase-dot-md}

-   **Anabolic** cytosolic enzyme
-   Catalyzes **rate-limiting and committed step of fatty acid synthesis**
    -   ATP-dependent carboxylation of acetyl-CoA to malonyl-CoA
    -   [Malonyl-CoA]({{< relref "malonyl_coa" >}}) binds to and inhibits [carnitine palmitoyl acyltransferase 1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) (CPT1) -> prevents synthesized fatty acids from entering mitochondria and being oxidized
-   Requires [biotin]({{< relref "biotin" >}})
-   [PKA]({{< relref "protein_kinase_a" >}}) substrate and **downregulated by phosphorylation**
    -   Dephosphorylation by _insulin-induced phosphoprotein phosphatases_ renews its activity


### [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}}) {#lipoprotein-lipase--lipoprotein-lipase-dot-md}

-   Expressed on _capillary endothelium_ primarily of muscle, adipose tissue and lactating mammary glands
-   **Catalyzes lipolysis of triglycerides circulating in [chylomicrons]({{< relref "chylomicron" >}})**
    -   _Chylomicron_ lipoprotein originating in gut that carries dietary fat and cholesterol to their appropriate destinations
-   Also targets [very low-density lipoprotein]({{< relref "very_low_density_lipoprotein" >}}) (VLDL)
    -   VLDL carries triglycerides and cholesterol from the liver to extrahepatic tissues
-   Both aforementioned lipoproteins carry [apoCII]({{< relref "apocii" >}}) on their surface -> **facilitates interaction of lipoprotein lipase with lipoprotein**
-   **Induced by insulin in response to macronutrient digestion and formation of chylomicrons**


### [Hormone-sensitive lipase]({{< relref "hormone_sensitive_lipase" >}}) {#hormone-sensitive-lipase--hormone-sensitive-lipase-dot-md}

-   **Catabolic** enzyme
-   Undergoes reciptrocal regulation with intracellular [acetyl-CoA carboxylase]({{< relref "acetyl_coa_carboxylase" >}})
    -   [PKA]({{< relref "protein_kinase_a" >}}) substrate
        -   Up-regulated by phosphorylation
        -   Down-regulated by dephosphorylation
-   The **second** of three lipases **required for the complete hydrolysis of triglycerides** to 3 fatty acids and glycerol in adipose tissue
    -   **The isozyme restricted to adipose tissue is ONLY active in the fasting state**
        -   Consequently, glycerol and fatty acids produced via lipolysis are released from the adipocytes
            -   Glycerol taken up by liver -> used as [gluconeogenic]({{< relref "gluconeogenesis" >}}) substrate
            -   Released fatty acids -> taken-up by rapidly metabolizing tissues (e.g. liver) -> use fatty acids as energy source to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})


### [CPT1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) and [CPT2]({{< relref "carnitine_palmitoyl_acyltransferase_2" >}}) {#cpt1--carnitine-palmitoyl-acyltransferase-1-dot-md--and-cpt2--carnitine-palmitoyl-acyltransferase-2-dot-md}

-   **Shuttle fatty acids (>12C) into mitochondrial matrix**
-   Use of [carnitine]({{< relref "carnitine" >}}) in this shuttle ensures that the mitochondrial and cytosolic coenzyme A pools remain intact
-   **CPT 1 is blocked by malonyl-CoA** -> prevents fatty acids that are being synthesized in cytoplasm from entering the mitochondria for oxidation


### FAD<sup>+</sup>-short-, medium- and long-chain [acyl-CoA dehydrogenases]({{< relref "acyl_coa_dehydrogenase" >}}) (ACADs) {#fad-short-medium-and-long-chain-acyl-coa-dehydrogenases--acyl-coa-dehydrogenase-dot-md----acads}

-   **Catalyze the initial step in each cycle of fatty acid β-oxidation**
    -   Results in the introduction of a _trans_ double-bond between C2 (αC) and C3 (βC) of the acyl-CoA substrate
-   All types of ACADs are mechanistically similar and **require FAD as a coenzyme**


## [Gluconeogenesis]({{< relref "gluconeogenesis" >}}) {#gluconeogenesis--gluconeogenesis-dot-md}

-   Anabolic process (i.e. making glucose) that occurs in the **fasting [liver]({{< relref "liver" >}})**
-   Most steps catalyzed by **reversible** glycolytic enzymes
    -   4 enzymes required to replace the 3 irreversible steps of glycolysis


### [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) {#pyruvate-carboxylase--pyruvate-carboxylase-dot-md}


### [PEP carboxykinase]({{< relref "pep_carboxykinase" >}}) {#pep-carboxykinase--pep-carboxykinase-dot-md}

-   Catalyzes a **GTP-dependent** reaction
-   **Converts [OAA]({{< relref "oxaloacetate" >}}) into [PEP]({{< relref "phosphoenolpyruvate" >}})**
-   This + [Pyruvate carboxylase]({{< relref "pyruvate_carboxylase" >}}) required to convert pyruvate into PEP
    -   **Reverses action of pyruvate kinase**


### [Fructose 1,6-bisphosphatase]({{< relref "fructose_1_6_bisphosphatase" >}}) {#fructose-1-6-bisphosphatase--fructose-1-6-bisphosphatase-dot-md}

-   Catalyzes **hydrolysis of F-1,6-bP to F6P**
-   Reverses [PFK-1]({{< relref "6_phosphofructo_1_kinase" >}})
-   Allosterically inhibited by F-2,6-bP and AMP


### [Glucose-6-phosphatase]({{< relref "glucose_6_phosphatase" >}}) {#glucose-6-phosphatase--glucose-6-phosphatase-dot-md}

-   Catalyzes **hydrolysis of G6P to free glucose for release from liver**
-   Reverses [glucokinase]({{< relref "glucokinase" >}})


### NAD<sup>+</sup>-linked lactate dehydrogenase {#nad-linked-lactate-dehydrogenase}

-   Converts lactate to pyruvate for entry into gluconeogenesis


### Alanine aminotransferase {#alanine-aminotransferase}

-   Catalyzes conversion of alanine to pyruvate for entry into gluconeogenesis


## Additional important enzymes {#additional-important-enzymes}


### [Adenylate kinase]({{< relref "adenylate_kinase" >}}) {#adenylate-kinase--adenylate-kinase-dot-md}

-   Cytoplasmic enzyme
-   Catalyzes **phosphoryl transfer between two ADP molecules to yield ATP and AMP**
-   **AMP:ATP ratio is the more sensitive indicator of energy charge**
    -   AMP signals that cell's energy stores are depleted


### Adenylate cyclase {#adenylate-cyclase}


### [Protein kinase A]({{< relref "protein_kinase_a" >}}) (PKA) {#protein-kinase-a--protein-kinase-a-dot-md----pka}

-   Ser/Thr kinase
-   Effector enzyme resulting from glucagon and epinephrine induced signaling events
    -   Glucagon or epinephrine binds to respective membrane receptors -> [adenylate cyclase]({{< relref "adenylate_cyclase" >}}) catalyzed conversion of ATP to cAMP
        -   cAMP activates PKA by binding to its regulatory subunits -> release of catalytic subunits


### [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}}) {#camp-phosphodiesterase--camp-phosphodiesterase-dot-md}

-   **Hydrolyzes cAMP to 6'AMP**
    -   5'AMP **does not signal** -> eliminates second messenger in glucagon signaling required to activate PKA
        -   **PKA activity down-regulated**


### [Phosphoprotein phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) {#phosphoprotein-phosphatases--phosphoprotein-phosphatase-dot-md}

-   Activated + induced by [insulin]({{< relref "insulin" >}})
-   **Reverses action (dephosphorylates) of PKA and other kinases**


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}}) {#list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated--list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated-dot-md}

<!--list-separator-->

-  **🔖 Summary of major regulators of intermediary metabolism**

    Note: First two mechanisms detailed in JumpStart, Allosteric control covered in [Kinetic and Allosteric Regulation of Glucose Regulation]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}})

    ---


#### [Intermediary Metabolism and its Regulation]({{< relref "intermediary_metabolism_and_its_regulation" >}}) {#intermediary-metabolism-and-its-regulation--intermediary-metabolism-and-its-regulation-dot-md}

<!--list-separator-->

-  **🔖 Summary of major regulators of intermediary metabolism**

    Note: First two mechanisms detailed in JumpStart, Allosteric control covered in [Kinetic and Allosteric Regulation of Glucose Regulation]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
