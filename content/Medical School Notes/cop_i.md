+++
title = "COP-I"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Coated vesicles > Clathrin-coated vesicles > Function**

    [COP-I]({{< relref "cop_i" >}}) facilitates **retrograde transport** - path above but **in reverse**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
