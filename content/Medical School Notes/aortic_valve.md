+++
title = "Aortic valve"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Left ventricle]({{< relref "left_ventricle" >}}) {#left-ventricle--left-ventricle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Ejects oxygenated [blood]({{< relref "blood" >}}) to the [aorta]({{< relref "aorta" >}}) through the [aortic valve]({{< relref "aortic_valve" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Heart sound is split during [inspiration]({{< relref "inspiration" >}}) because the [pulmonic valve]({{< relref "pulmonary_valve" >}}) closing is slightly later than [aortic valve]({{< relref "aortic_valve" >}}) closing

    ---


#### [Describe the anatomical locations for auscultation of the heart valves.]({{< relref "describe_the_anatomical_locations_for_auscultation_of_the_heart_valves" >}}) {#describe-the-anatomical-locations-for-auscultation-of-the-heart-valves-dot--describe-the-anatomical-locations-for-auscultation-of-the-heart-valves-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    [Aortic valve]({{< relref "aortic_valve" >}}): Second right intercostal space at the edge of the sternum

    ---


#### [Aortic sinus]({{< relref "aortic_sinus" >}}) {#aortic-sinus--aortic-sinus-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A space immediately above the [aortic valve]({{< relref "aortic_valve" >}}), between an aortic wall leaflet and the wall of the [ascending aorta]({{< relref "ascending_aorta" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
