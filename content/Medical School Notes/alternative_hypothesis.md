+++
title = "Alternative hypothesis"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Counterpart to the [null hypothesis]({{< relref "null_hypothesis" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Explain the meaning of a p-value less than 0.05]({{< relref "explain_the_meaning_of_a_p_value_less_than_0_05" >}}) {#explain-the-meaning-of-a-p-value-less-than-0-dot-05--explain-the-meaning-of-a-p-value-less-than-0-05-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > P-value**

    **It is not possible to prove [H<sub>1</sub>]({{< relref "alternative_hypothesis" >}}) true**, but having a p-value lower than [significance level]({{< relref "significance_level" >}}) -> very unlikely that [H<sub>0</sub>]({{< relref "null_hypothesis" >}}) is correct

    ---


#### [Compare and contrast the null hypothesis and the alternative hypothesis]({{< relref "compare_and_contrast_the_null_hypothesis_and_the_alternative_hypothesis" >}}) {#compare-and-contrast-the-null-hypothesis-and-the-alternative-hypothesis--compare-and-contrast-the-null-hypothesis-and-the-alternative-hypothesis-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Alternative hypothesis]({{< relref "alternative_hypothesis" >}}) (H<sub>1</sub>)

        -   The assumption that there **is a relationship** between two measured variables or **a significant difference** between two studied populations
        -   The counterpart to the null hypothesis


### Unlinked references {#unlinked-references}

[Show unlinked references]
