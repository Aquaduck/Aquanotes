+++
title = "Population"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Is a sampling of a [sample]({{< relref "sample" >}})


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [T-test]({{< relref "t_test" >}}) {#t-test--t-test-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Calculates the difference between the [means]({{< relref "mean" >}}) of two [samples]({{< relref "sample" >}}) _or_ between a sample and a [population]({{< relref "population" >}})/value subject to change

    ---


#### [Sample]({{< relref "sample" >}}) {#sample--sample-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Makes an _inference_ about a [population]({{< relref "population" >}})

    ---


#### [Population mean]({{< relref "population_mean" >}}) {#population-mean--population-mean-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Mean]({{< relref "mean" >}}) of a [Population]({{< relref "population" >}})

    ---


#### [Interpret confidence intervals around a sample size or calculated relative risk or odds ratio]({{< relref "interpret_confidence_intervals_around_a_sample_size_or_calculated_relative_risk_or_odds_ratio" >}}) {#interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio--interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Confidence interval**

    Provide a way to determine a [Population]({{< relref "population" >}}) measurement or a value that is subject to change from a [Sample]({{< relref "sample" >}}) measurement

    ---


#### [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}}) {#explain-the-t-test-and-the-factors-that-influence-t-values--explain-the-t-test-and-the-factors-that-influence-t-values-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > T-test**

    Calculates the difference between the [means]({{< relref "sample_mean" >}}) of two samples _or_ between a sample and a [population]({{< relref "population" >}})/value subject to change

    ---


#### [Compare and contrast sample and population]({{< relref "compare_and_contrast_sample_and_population" >}}) {#compare-and-contrast-sample-and-population--compare-and-contrast-sample-and-population-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Population]({{< relref "population" >}})

        -   The total number of inhabitants in a region from which a sample is drawn for statistical measurement


### Unlinked references {#unlinked-references}

[Show unlinked references]
