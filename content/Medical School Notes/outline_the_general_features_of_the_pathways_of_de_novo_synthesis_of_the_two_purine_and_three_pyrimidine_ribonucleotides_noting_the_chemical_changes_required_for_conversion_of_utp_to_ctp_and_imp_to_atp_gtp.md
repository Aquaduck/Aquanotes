+++
title = "Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP."
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Lippincott's Illustrated Reviews: Biochemistry, 5e]({{< relref "lippincott_s_illustrated_reviews_biochemistry_5e" >}}) {#from-lippincott-s-illustrated-reviews-biochemistry-5e--lippincott-s-illustrated-reviews-biochemistry-5e-dot-md}


### Synthesis of [purine]({{< relref "purine" >}}) nucleotides (p. 291) {#synthesis-of-purine--purine-dot-md--nucleotides--p-dot-291}

-   The purine ring is constructed primarily in the [liver]({{< relref "liver" >}})
-   [Ribose 5-phosphate]({{< relref "ribose_5_phosphate" >}}) from the [PPP]({{< relref "pentose_phosphate_pathway" >}}) is used as a starting molecule


#### Synthesis of [5-phosphoribosyl-1-pyrophosphate]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) (PRPP) {#synthesis-of-5-phosphoribosyl-1-pyrophosphate--5-phosphoribosyl-1-pyrophosphate-dot-md----prpp}

-   PRPP is an "activated pentose" that participates in the **synthesis and salvage of purines and pyrimidines**
-   PRPP is synthesized from ATP and ribose 5-phosphate and catalyzed by [PRPP synthetase]({{< relref "prpp_synthetase" >}})
    -   PRPP-synthetase is activated by inorganic phosphate and inhibited by purine nucleotides (end-product inhibition)


#### Synthesis of [5'-phosphoribosylamine]({{< relref "5_phosphoribosylamine" >}}) {#synthesis-of-5-phosphoribosylamine--5-phosphoribosylamine-dot-md}

-   5'-phosphorybosylamine (PRA) is synthesized from [PRPP]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) and glutamine
-   **Amide group of glutamine** replaces the pyrophosphate group attached to **carbon 1** of PRPP
-   Synthesized by [ATase]({{< relref "amidophosphoribosyl_transferase" >}})
    -   ATase inhibited by AMP and GMP (end product inhibition)
-   **Committed step in purine biosynthesis**
    -   [PRPP]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) controls the rate of this reaction


#### Synthesis of [Inosine monophosphate]({{< relref "inosine_monophosphate" >}}), the "parent" purine nucleotide {#synthesis-of-inosine-monophosphate--inosine-monophosphate-dot-md--the-parent-purine-nucleotide}

-   The next **nine steps** in purine nucleotide biosynthesis lead to the synthesis of IMP
-   Requires ATP as an energy source


#### Conversion of [IMP]({{< relref "inosine_monophosphate" >}}) to AMP and GMP {#conversion-of-imp--inosine-monophosphate-dot-md--to-amp-and-gmp}

-   Two-step energy-requiring pathway
-   Synthesis of **AMP requires GTP** as an energy source
-   Synthesis of **GMP requires ATP** as an energy source


### [Pyrimidine]({{< relref "pyrimidine" >}}) synthesis and degradation {#pyrimidine--pyrimidine-dot-md--synthesis-and-degradation}

-   The pyrimidine ring is synthesized **before** being attached to [ribose 5-phosphate]({{< relref "ribose_5_phosphate" >}}) (donated by PRPP)


#### Synthesis of [carbamoyl phosphate]({{< relref "carbamoyl_phosphate" >}}) {#synthesis-of-carbamoyl-phosphate--carbamoyl-phosphate-dot-md}

-   Synthesized from glutamine and carbon dioxide
-   Catalyzed by [carbamoyl phosphate synthetase II]({{< relref "carbamoyl_phosphate_synthetase_ii" >}})
    -   CPS II is inhibited by UTP (end-product inhibition)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-05 Tue] </span></span> > Gout > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP.]({{< relref "outline_the_general_features_of_the_pathways_of_de_novo_synthesis_of_the_two_purine_and_three_pyrimidine_ribonucleotides_noting_the_chemical_changes_required_for_conversion_of_utp_to_ctp_and_imp_to_atp_gtp" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
