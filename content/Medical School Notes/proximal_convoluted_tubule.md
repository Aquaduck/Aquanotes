+++
title = "Proximal convoluted tubule"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The first component of the [proximal tubule]({{< relref "proximal_tubule" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Proximal tubule**

    ([Proximal convoluted tubule]({{< relref "proximal_convoluted_tubule" >}}) + [Proximal straight tubule]({{< relref "proximal_straight_tubule" >}}))

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
