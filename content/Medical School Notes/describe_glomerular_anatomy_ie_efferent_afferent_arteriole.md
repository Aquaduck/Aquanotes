+++
title = "Describe glomerular anatomy (ie efferent/afferent arteriole)"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}}) {#from-vander-s-renal-physiology--vander-s-renal-physiology-dot-md}


### The [Renal corpuscle]({{< relref "renal_corpuscle" >}}) (p. 17) {#the-renal-corpuscle--renal-corpuscle-dot-md----p-dot-17}

-   Hollow sphere ([Bowman capsule]({{< relref "bowman_capsule" >}})) composed of epithelial cells
-   Filled with a compact tuft of interconnected capillary loops called the [Glomerulus]({{< relref "glomerulus" >}})
-   Two closely spaced arterioles penetrate the [Bowman capsule]({{< relref "bowman_capsule" >}}) at a region called the _vascular pole_:
    1.  [Afferent arteriole of the kidney]({{< relref "afferent_arteriole_of_the_kidney" >}}): brings blood into the capillaries of the glomerulus
    2.  [Efferent arteriole of the kidney]({{< relref "efferent_arteriole_of_the_kidney" >}}): drains blood from the capillaries of the glomerulus
-   [Mesangial cell]({{< relref "mesangial_cell" >}}): act as phagocytes -> remove trapped material from basement membrane of the capillaries in the [Glomerulus]({{< relref "glomerulus" >}})
    -   Mesangial cells also contain large numbers of myofilaments -> contract in response to a variety of stimuli similar to vascular smooth muscle cells
-   [Bowman's space]({{< relref "bowman_s_space" >}}): Space within [Bowman capsule]({{< relref "bowman_capsule" >}}) that is not occupied by capillaries and mesangial cells
    -   It is here where fluid filters from the glomerular capillaries before flowing into the first portion of the tubule opposite the vascular pole


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > Renal Physiology: Glomerular Filtration > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
