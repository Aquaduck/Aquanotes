+++
title = "Ester"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Lumen - Functional Group Names, Properties, and Reactions]({{< relref "lumen_functional_group_names_properties_and_reactions" >}}) {#lumen-functional-group-names-properties-and-reactions--lumen-functional-group-names-properties-and-reactions-dot-md}

<!--list-separator-->

-  **🔖 Functional Groups**

    <!--list-separator-->

    -  [Esters]({{< relref "ester" >}})

        -   Most naturally occurring fats and oils are the fatty acid esters of glycerol
        -   Typically fragrant

        <!--list-separator-->

        -  Structure and Bonding

            -   Carbonyl center -> sp2 hybridization
                -   120<sup>o</sup>
            -   Structurally flexible
                -   Rotation abou C-O-C bond lower energy barrier
            -   Low polarity
            -   (esters) Compared to [amides]({{< relref "amide" >}}):
                -   Less rigid/more structurally flexible
                    -   Lower melting point
                -   More volatile
                    -   Lower boiling point
            -   Essentially non-acidic except in presence of very strong bases

        <!--list-separator-->

        -  Physical Properties and Characterization

            -   Hydrogen bond acceptors
                -   **Cannot be donors**
            -   Esters do not self-associate
                -   No hydrogens

        <!--list-separator-->

        -  Reactivity

            -   React with nucleophiles **at the carbonyl carbon**
                -   Weakly electrophilic, but attacked by **strong nucleophiles**
            -   Electrophilicity can increase if protonated
            -   C-H bonds adjacent to carbonyl are weakly acidic, but undergo deprotonation with strong bases
                -   Usually initiates condensation reactions


#### [Kaplan Organic Chemistry Chapter 1]({{< relref "kaplan_organic_chemistry_chapter_1" >}}) {#kaplan-organic-chemistry-chapter-1--kaplan-organic-chemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 1.4 Carboxylic Acids and Derivatives**

    <!--list-separator-->

    -  [Esters]({{< relref "ester" >}})

        -   Replace -OH of carboxylic acid with -OR (where R is a hydrocarbon chain)
        -   Naming convention based on those for carboxylic acids but in 2 parts:
            1.  First term is the **alkyl name of the esterifying group (R)**
            2.  Second term is name of the parent acid, replacing _-oic_ with _-oate_

        {{< figure src="/ox-hugo/_20210714_194652screenshot.png" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
