+++
title = "Inosine"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    [Purine nucleoside phosphorylase]({{< relref "purine_nucleoside_phosphorylase" >}}) converts [Inosine]({{< relref "inosine" >}}) and [guanosine]({{< relref "guanosine" >}}) into their respective purine bases, [hypoxanthine]({{< relref "hypoxanthine" >}}) and [guanine]({{< relref "guanine" >}})

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    Alternatively, an amino group is removed from [adenosine]({{< relref "adenosine" >}}) -> produces [inosine]({{< relref "inosine" >}}) (hypoxanthine-ribose) by [adenosine deaminase]({{< relref "adenosine_deaminase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
