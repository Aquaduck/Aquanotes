+++
title = "DNA Polymerase"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 13 linked references {#13-linked-references}


#### [ScienceDirect - Processivity]({{< relref "sciencedirect_processivity" >}}) {#sciencedirect-processivity--sciencedirect-processivity-dot-md}

<!--list-separator-->

-  **🔖 Processivity**

    **One of the most important properties of [DNA polymerases]({{< relref "dna_polymerase" >}})**

    ---


#### [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}}) {#osmosis-transcription-translation-and-replication--osmosis-transcription-translation-and-replication-dot-md}

<!--list-separator-->

-  **🔖 Genetic Mutations and Repair > DNA damage > Double stranded breaks > Repair mechanisms**

    MRN protein complex binds to each end and removes affected nucleotides -> [DNA polymerase]({{< relref "dna_polymerase" >}}) copies genetic information from sister chromatid

    ---

<!--list-separator-->

-  **🔖 Genetic Mutations and Repair > DNA damage > Single strand damage**

    [DNA polymerase]({{< relref "dna_polymerase" >}}) rebuilds segment

    ---

<!--list-separator-->

-  **🔖 DNA Replication > Process > Termination**

    [DNA polymerase]({{< relref "dna_polymerase" >}}) leaves strand at telomere (TTAGGG nucleotide sequences)

    ---

<!--list-separator-->

-  **🔖 DNA Replication > Process > Elongation**

    _RNA primase_ creates multiple RNA primers -> randomly bind -> [DNA polymerase]({{< relref "dna_polymerase" >}}) adds complementary nucleotides in 3->5 direction

    ---


#### [Khan - DNA Proofreading and Repair]({{< relref "khan_dna_proofreading_and_repair" >}}) {#khan-dna-proofreading-and-repair--khan-dna-proofreading-and-repair-dot-md}

<!--list-separator-->

-  **🔖 DNA damage repair mechanisms > Excision repair > Nucleotide excision repair**

    A _[DNA polymerase]({{< relref "dna_polymerase" >}})_ replaces the missing DNA

    ---

<!--list-separator-->

-  **🔖 Mismatch repair > Process**

    Missing patch replaced with correct nucleotides by a _[DNA polymerase]({{< relref "dna_polymerase" >}})_

    ---

<!--list-separator-->

-  **🔖 Proofreading**

    Done by _[DNA polymerases]({{< relref "dna_polymerase" >}})_ during replication

    ---


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Steps in replication**

    Using the primer, [DNA polymerase]({{< relref "dna_polymerase" >}}) can start synthesis

    ---

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes**

    **14 [DNA polymerases]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})**

    ---


#### [DNA Polymerase-δ]({{< relref "dna_polymerase_δ" >}}) {#dna-polymerase-δ--dna-polymerase-δ-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A type of [DNA Polymerase]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})

    ---


#### [DNA Polymerase-ɛ]({{< relref "dna_polymerase_ɛ" >}}) {#dna-polymerase-ɛ--dna-polymerase-ɛ-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A type of [DNA Polymerase]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})

    ---


#### [DNA Polymerase-ɑ]({{< relref "dna_polymerase_ɑ" >}}) {#dna-polymerase-ɑ--dna-polymerase-ɑ-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    A type of [DNA Polymerase]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
