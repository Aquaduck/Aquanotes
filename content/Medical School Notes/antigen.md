+++
title = "Antigen"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Activation and actions - T and B lymphocytes > B lymphocyte**

    Activated by **recognition of intact foreign molecules ([antigens]({{< relref "antigen" >}}))**

    ---


#### [Antigen presenting cell]({{< relref "antigen_presenting_cell" >}}) {#antigen-presenting-cell--antigen-presenting-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Presents partially-digested [antigens]({{< relref "antigen" >}}) to [T lymphocyte]({{< relref "t_lymphocyte" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
