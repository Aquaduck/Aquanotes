+++
title = "Amide"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Lumen - Functional Group Names, Properties, and Reactions]({{< relref "lumen_functional_group_names_properties_and_reactions" >}}) {#lumen-functional-group-names-properties-and-reactions--lumen-functional-group-names-properties-and-reactions-dot-md}

<!--list-separator-->

-  **🔖 Functional Groups > Esters > Structure and Bonding**

    (esters) Compared to [amides]({{< relref "amide" >}}):

    ---


#### [Kaplan Organic Chemistry Chapter 1]({{< relref "kaplan_organic_chemistry_chapter_1" >}}) {#kaplan-organic-chemistry-chapter-1--kaplan-organic-chemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 1.4 Carboxylic Acids and Derivatives**

    <!--list-separator-->

    -  [Amides]({{< relref "amide" >}})

        -   Replace -OH with _amino group_ (nitrogen-containing group)
            -   Amino N can be bonded to 0, 1, or 2 alkyl groups
        -   Naming is same as esters, except that the suffix becomes _-amide_

        <_20210714_194918screenshot.png>


### Unlinked references {#unlinked-references}

[Show unlinked references]
