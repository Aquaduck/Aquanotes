+++
title = "Circumflex artery"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Branch of the [left coronary artery]({{< relref "left_coronary_artery" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}}) {#describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot--describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    Occlusion of [circumflex artery]({{< relref "circumflex_artery" >}}) -> infarction of posterolateral wall of left ventricle

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
