+++
title = "Nitric oxide synthase"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the role of gases in cell signaling with special emphasis on nitric oxide and its basic mechanism of action.]({{< relref "describe_the_role_of_gases_in_cell_signaling_with_special_emphasis_on_nitric_oxide_and_its_basic_mechanism_of_action" >}}) {#describe-the-role-of-gases-in-cell-signaling-with-special-emphasis-on-nitric-oxide-and-its-basic-mechanism-of-action-dot--describe-the-role-of-gases-in-cell-signaling-with-special-emphasis-on-nitric-oxide-and-its-basic-mechanism-of-action-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Gases as second messengers - NO > Mechanism of action**

    Receptor activates [NO synthase]({{< relref "nitric_oxide_synthase" >}}) -> synthesizes NO

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
