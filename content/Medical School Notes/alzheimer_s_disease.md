+++
title = "Alzheimer's disease"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amyloid-beta 4A precursor protein]({{< relref "amyloid_beta_4a_precursor_protein" >}}) {#amyloid-beta-4a-precursor-protein--amyloid-beta-4a-precursor-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Generates amyloid beta, a component of the amyloid plaques found in brains of [Alzheimer's disease]({{< relref "alzheimer_s_disease" >}}) patients

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
