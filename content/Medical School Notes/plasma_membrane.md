+++
title = "Plasma membrane"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   An [organelle]({{< relref "organelle" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Sarcolemma]({{< relref "sarcolemma" >}}) {#sarcolemma--sarcolemma-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The [plasma membrane]({{< relref "plasma_membrane" >}}) of [skeletal muscle cells]({{< relref "skeletal_muscle_cell" >}})

    ---


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase**

    Both glucagon and epinephrine bind to specific [G protein-coupled receptors]({{< relref "g_protein_coupled_receptors" >}}) (GPCRs) on the [plasma membrane]({{< relref "plasma_membrane" >}})

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Structure of Skeletal Muscle (p. 109) > Skeletal muscle cells**

    Skeletal muscle cell membrane is called a [sarcolemma]({{< relref "sarcolemma" >}}) (variation of [plasmalemma]({{< relref "plasma_membrane" >}}) - another name for cell membrane)

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles**

    <!--list-separator-->

    -  [Plasma membrane]({{< relref "plasma_membrane" >}})

        -   Envelops cell and forms boundary between it and adjacent structures


### Unlinked references {#unlinked-references}

[Show unlinked references]
