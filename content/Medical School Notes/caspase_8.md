+++
title = "Caspase-8"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [Initiator caspase]({{< relref "initiator_caspase" >}})
-   Activates [Bid]({{< relref "bid" >}}) via cleavage
-   Dimerizes with [FLIP]({{< relref "flice_inhibitor_protein" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})
-   Cleaves and activates [Caspase-3]({{< relref "caspase_3" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Procaspase-8]({{< relref "procaspase_8" >}}) {#procaspase-8--procaspase-8-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Zymogen of [Caspase-8]({{< relref "caspase_8" >}})

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026) > BH3-only proteins > Bid**

    When [death receptors]({{< relref "death_receptor" >}}) activate the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}), [Caspase-8]({{< relref "caspase_8" >}}) cleaves Bid -> activates Bid -> translocates to outer mitochondrial membrane -> inhibits anti-apoptotic Bcl2 family proteins -> **amplification of death signal**

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > FLIP > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    FLIP dimerizes with [caspase-8]({{< relref "caspase_8" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Fas > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    Fas activated by [Fas ligand]({{< relref "fas_ligand" >}}) -> death domains on Fas death receptors bind intracellular adaptor proteins -> bind [initiator caspases]({{< relref "initiator_caspase" >}}) (primarily [caspase-8]({{< relref "caspase_8" >}})) -> formation of a [death-inducing signaling complex]({{< relref "death_inducing_signaling_complex" >}}) (DISC)

    ---


#### [FLICE-inhibitor protein]({{< relref "flice_inhibitor_protein" >}}) {#flice-inhibitor-protein--flice-inhibitor-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Dimerizes with [Caspase-8]({{< relref "caspase_8" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
