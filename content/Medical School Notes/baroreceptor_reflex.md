+++
title = "Baroreceptor reflex"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Input to the Cardiovascular Center (p.879) > Baroreceptor**

    [Baroreceptor reflex]({{< relref "baroreceptor_reflex" >}}): Cardiac center monitoring of baroreceptor firing to maintain cardiac homeostasis

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
