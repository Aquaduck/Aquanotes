+++
title = "Muscle"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Striated muscle]({{< relref "striated_muscle" >}}) {#striated-muscle--striated-muscle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A subtype of [muscle]({{< relref "muscle" >}})

    ---


#### [Smooth muscle]({{< relref "smooth_muscle" >}}) {#smooth-muscle--smooth-muscle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A subtype of [muscle]({{< relref "muscle" >}})

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Albumin-bound fatty acids > From Fat Metabolism in Muscle & Adipose Tissue**

    E.g. [muscle]({{< relref "muscle" >}}) and [kidney]({{< relref "kidney" >}})

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  Mobilization of stored triacylglycerol in [muscle]({{< relref "muscle" >}})

        -   Skeletal muscle obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose
            -   These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [Epinephrine]({{< relref "epinephrine" >}})

        {{< figure src="/ox-hugo/_20211024_191932screenshot.png" caption="Figure 1: Fatty acid mobilization mediated via skeletal muscle contraction vs. epinephrine" width="500" >}}


#### [Glucose 6-phosphate]({{< relref "glucose_6_phosphate" >}}) {#glucose-6-phosphate--glucose-6-phosphate-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Signals [muscle]({{< relref "muscle" >}}) cells to start [synthesizing]({{< relref "glycogenesis" >}}) [glycogen]({{< relref "glycogen" >}})

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology**

    <!--list-separator-->

    -  Overview - [Muscle]({{< relref "muscle" >}})

        -   Muscle is classified into two types: _[striated]({{< relref "striated_muscle" >}})_ and _[smooth]({{< relref "smooth_muscle" >}})_
            -   Striated is further subdivided into _[skeletal]({{< relref "skeletal_muscle" >}})_ and _[cardiac]({{< relref "cardiac_muscle" >}})_ muscles
        -   Muscle cells possess _contractile filaments_ whose major components are [actin]({{< relref "actin" >}}) and [myosin]({{< relref "myosin" >}})


#### [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}}) {#describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot--describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  Regulation of the mobilization of [intramuscular]({{< relref "muscle" >}}) fat

        -   Skeletal muscle obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose
            -   These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [Epinephrine]({{< relref "epinephrine" >}})

        {{< figure src="/ox-hugo/_20211024_203003screenshot.png" caption="Figure 2: Fatty acid mobilization mediated via skeletal muscle contraction vs. epinephrine" width="500" >}}

        <!--list-separator-->

        -  Role of [skeletal muscle]({{< relref "skeletal_muscle" >}}) contraction

            1.  Nerve impulse via [acetylcholine]({{< relref "acetylcholine" >}}) to acetylcholine receptor
            2.  Depolarization
            3.  Stimulation of ER to release [Calcium]({{< relref "calcium" >}})
            4.  Activation of [PKC]({{< relref "protein_kinase_c" >}})
            5.  Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol

        <!--list-separator-->

        -  Role of [epinephrine]({{< relref "epinephrine" >}})

            1.  Epinephrine binds to beta-adrenergic receptor
            2.  Activation of G-protein
            3.  Activation of adenylate cyclase
            4.  Convertion of ATP to cAMP
            5.  Activation of [PKA]({{< relref "protein_kinase_a" >}})
            6.  Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol


### Unlinked references {#unlinked-references}

[Show unlinked references]
