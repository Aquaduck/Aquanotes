+++
title = "Residual volume"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Volume of air that remains in the [lungs]({{< relref "lung" >}}) after a maximal [exhalation]({{< relref "exhalation" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Total lung capacity]({{< relref "total_lung_capacity" >}}) {#total-lung-capacity--total-lung-capacity-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Addition of [Vital capacity]({{< relref "vital_capacity" >}}) and [residual volume]({{< relref "residual_volume" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration**

    <!--list-separator-->

    -  [Residual volume]({{< relref "residual_volume" >}})

        -   Volume of air that remains in the lungs **after a maximal exhalation**

<!--list-separator-->

-  **🔖 Respiration > Total lung capacity**

    [RV]({{< relref "residual_volume" >}}) + [VC]({{< relref "vital_capacity" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
