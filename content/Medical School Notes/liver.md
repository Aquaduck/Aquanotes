+++
title = "Liver"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   The liver is an [organ]({{< relref "organ" >}})


## Backlinks {#backlinks}


### 18 linked references {#18-linked-references}


#### [Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP.]({{< relref "outline_the_general_features_of_the_pathways_of_de_novo_synthesis_of_the_two_purine_and_three_pyrimidine_ribonucleotides_noting_the_chemical_changes_required_for_conversion_of_utp_to_ctp_and_imp_to_atp_gtp" >}}) {#outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot--outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Synthesis of purine nucleotides (p. 291)**

    The purine ring is constructed primarily in the [liver]({{< relref "liver" >}})

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Plasma Proteins (p. 796) > Albumin**

    Manufactured by the [Liver]({{< relref "liver" >}})

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Albumin-bound fatty acids > From Fat Metabolism in Muscle & Adipose Tissue**

    **Released [glycerol]({{< relref "glycerol" >}}) is used primarly by the [liver]({{< relref "liver" >}}) as a [gluconeogenic]({{< relref "gluconeogenesis" >}}) substrate**

    ---

<!--list-separator-->

-  **🔖 Albumin-bound fatty acids > From Fat Metabolism in Muscle & Adipose Tissue**

    Fasting state -> **fatty acids enter the [liver]({{< relref "liver" >}}) as the energy source needed to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})**

    ---

<!--list-separator-->

-  **🔖 Lipoprotein lipase > From Fat Metabolism in Muscle & Adipose Tissue**

    **LPL is not expressed by the [liver]({{< relref "liver" >}}) or the [brain]({{< relref "brain" >}})** -> NO dietary fat reaches either of those tissues

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    Anabolic process (i.e. making glucose) that occurs in the **fasting [liver]({{< relref "liver" >}})**

    ---

<!--list-separator-->

-  **🔖 Glycolysis**

    All tissues **except [liver]({{< relref "liver" >}})** use glucose via glycolysis as an energy source in the fed state

    ---


#### [Glycerol]({{< relref "glycerol" >}}) {#glycerol--glycerol-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Taken up by the [liver]({{< relref "liver" >}}) following cleavage of [Triacylglycerol]({{< relref "triacylglycerol" >}}) via [LPL]({{< relref "lipoprotein_lipase" >}})

    ---


#### [Glucokinase]({{< relref "glucokinase" >}}) {#glucokinase--glucokinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Restricted to [liver]({{< relref "liver" >}}) parenchymal cells and β-cells of [pancreas]({{< relref "pancreas" >}})

    ---


#### [Folate]({{< relref "folate" >}}) {#folate--folate-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Small reserve pool stored primarily in the [liver]({{< relref "liver" >}})

    ---


#### [Fibrinogen]({{< relref "fibrinogen" >}}) {#fibrinogen--fibrinogen-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Produced by the [liver]({{< relref "liver" >}})

    ---


#### [Fatty acid]({{< relref "fatty_acid" >}}) {#fatty-acid--fatty-acid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Fasting state -> fatty acids enter the [liver]({{< relref "liver" >}}) as the energy source needed to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})

    ---


#### [Explain why adipose tissue is referred to as a fat depot.]({{< relref "explain_why_adipose_tissue_is_referred_to_as_a_fat_depot" >}}) {#explain-why-adipose-tissue-is-referred-to-as-a-fat-depot-dot--explain-why-adipose-tissue-is-referred-to-as-a-fat-depot-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    **LPL is not expressed by the [liver]({{< relref "liver" >}}) or the [brain]({{< relref "brain" >}})** -> NO dietary fat reaches either of those tissues

    ---


#### [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}}) {#explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot--explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Delivering dietary fat to extrahepatic tissues via chylomicrons**

    Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})

    ---


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Two arms of the immune system > Innate system**

    These are synthesized by the [liver]({{< relref "liver" >}})

    ---


#### [Cobalamin]({{< relref "cobalamin" >}}) {#cobalamin--cobalamin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Very large reserve pool (several years) stored primarily in the [liver]({{< relref "liver" >}})

    ---


#### [Angiotensinogen]({{< relref "angiotensinogen" >}}) {#angiotensinogen--angiotensinogen-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Produced by the [Liver]({{< relref "liver" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
