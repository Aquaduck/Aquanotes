+++
title = "Guyton and Hall Textbook of Medical Physiology"
author = ["John E. Hall"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Be familiar with the unique features of the pulmonary circulation.]({{< relref "be_familiar_with_the_unique_features_of_the_pulmonary_circulation" >}}) {#be-familiar-with-the-unique-features-of-the-pulmonary-circulation-dot--be-familiar-with-the-unique-features-of-the-pulmonary-circulation-dot-md}

<!--list-separator-->

-  From [Guyton and Hall Textbook of Medical Physiology]({{< relref "guyton_and_hall_textbook_of_medical_physiology" >}})

    <!--list-separator-->

    -  [Zones]({{< relref "west_s_zones_of_the_lung" >}}) 1, 2, and 3 of pulmonary blood flow (p. 510)

        {{< figure src="/ox-hugo/_20211027_162440screenshot.png" caption="Figure 1: Relationship of arteriole, alveolar, and venous pressure in the lung zones" width="400" >}}

        <!--list-separator-->

        -  Zone 1

            -   **No blood flow during all portions of the cardiac cycle**
            -   Local _alveolar capillary pressure_ in this area of the lung **never rises higher** than the _alveolar air pressure_ during any part of the cardiac cycle

        <!--list-separator-->

        -  Zone 2

            -   **Intermittent blood flow only during peaks of pulmonary arterial pressure**
            -   _Systolic pressure_ is greater than _alveolar air pressure_, but _diastolic pressure_ is less than the _alveolar air pressure_

        <!--list-separator-->

        -  Zone 3

            -   **Continous blood flow**
            -   _Alveolar capillary pressure_ **remains greater** than the _alveolar air pressure_ during **the entire cardiac cycle**


### Unlinked references {#unlinked-references}

[Show unlinked references]
