+++
title = "Phosphatidylinositol kinase"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Discuss receptor tyrosine kinases and tyrosine-associated receptor signal pathways]({{< relref "discuss_receptor_tyrosine_kinases_and_tyrosine_associated_receptor_signal_pathways" >}}) {#discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways--discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Receptor protein tyrosine kinases (RPTKs) > Two key pathways**

    <!--list-separator-->

    -  [Phosphatidylinositol kinase]({{< relref "phosphatidylinositol_kinase" >}}) (PI-3 Kinase)

        -   Involves [PIP<sub>2</sub> signalling]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}})
        -   Controls (e.g.):
            -   Growth/survival of cells
            -   Apoptosis
            -   Many more


### Unlinked references {#unlinked-references}

[Show unlinked references]
