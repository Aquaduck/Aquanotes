+++
title = "Clinically Oriented Anatomy"
author = ["\"More", "Keith L.\" \"Dalley", "Arthur F.\""]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 15 linked references {#15-linked-references}


#### [List the two landmarks for approximating dermatome levels in the thorax.]({{< relref "list_the_two_landmarks_for_approximating_dermatome_levels_in_the_thorax" >}}) {#list-the-two-landmarks-for-approximating-dermatome-levels-in-the-thorax-dot--list-the-two-landmarks-for-approximating-dermatome-levels-in-the-thorax-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  [Dermatome]({{< relref "dermatome" >}})

        -   A _dermatome_ is an area of skin innervated by a single pair of spinal nerves
        -   In thorax, skin overlies an intercostal space
            -   _Dorsal ramus_ of a spinal nerve innervates the skin over a small regoin of the back
            -   _Ventral ramus_ innervates rest of the dermatome all the way to midline anteriorly
        -   Cell bodies of all sensory axons for one dermatome are in the _dorsal root ganglia_ for that spinal level
        -   **Two landmarks are useful for approximating dermatome levels in the thorax:**
            1.  Nipple = T4
            2.  Xiphoid process = T6


#### [Describe the vessels supplying the conduction system of the heart and the clinical presentation following damage to these.]({{< relref "describe_the_vessels_supplying_the_conduction_system_of_the_heart_and_the_clinical_presentation_following_damage_to_these" >}}) {#describe-the-vessels-supplying-the-conduction-system-of-the-heart-and-the-clinical-presentation-following-damage-to-these-dot--describe-the-vessels-supplying-the-conduction-system-of-the-heart-and-the-clinical-presentation-following-damage-to-these-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  Coronary occlusion and conducting system of [Heart]({{< relref "heart" >}})

        -   Damage to conducting system of heart -> disturbances of [cardiac muscle]({{< relref "cardiac_muscle" >}}) contraction
        -   LAD gives rise to septal branches supplying AV bundle in most people
        -   Branches of RCA supply both SA and AV nodes (60% of the time, 40% of people have supply from LCA instead) -> conducting system affected by occlusion -> heart block
            -   Ventricles will begin to contract independently at their own rate of 25-30 times per minute (much slower than normal)
            -   Atria continue to contract at normal rate if SA node is spared, but impulse from SA does not reach ventricles
        -   Damage to one bundle branch -> bundle-branch block
            -   Excitation passes along unaffected branch and causes a normally timed systole **of that ventricle only**
            -   Impulse then spreads to other ventricle via myogenic (muscle propagated) conduction -> **late asynchronous contraction**
            -   In this case, a cardiac pacemaker may be implanted to increase the ventricular rate of contraction to 70-80 per minute


#### [Describe the relevant anatomy and disease etiology and where applicable, the clinical presentation of disorders associated with the superior mediastinum.]({{< relref "describe_the_relevant_anatomy_and_disease_etiology_and_where_applicable_the_clinical_presentation_of_disorders_associated_with_the_superior_mediastinum" >}}) {#describe-the-relevant-anatomy-and-disease-etiology-and-where-applicable-the-clinical-presentation-of-disorders-associated-with-the-superior-mediastinum-dot--describe-the-relevant-anatomy-and-disease-etiology-and-where-applicable-the-clinical-presentation-of-disorders-associated-with-the-superior-mediastinum-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  Aneurysm of the Ascending Aorta

        -   Distal part of ascending aorta receives a strong thrust of blood when the left ventricle contracts
            -   Wall is not yet reinforced by fibrous pericardium -> aneurysm may develop
        -   Evident on chest film or MR angiogram as **an enlarged area of ascending aorta silhouette**
        -   Chest pain that radiates to back
        -   May exert pressure on trachea, esophagus, and recurrent laryngeal nerve -> difficulty in breathing and swallowing

    <!--list-separator-->

    -  Coarctation of the Aorta

        -   Arch of aorta or thoracic aorta has stenosis -> diminishes caliber of aortic lumen -> obstructed blood flow to inferior body
        -   Most common site is near [ligamentum arteriosum]({{< relref "ligamentum_arteriosum" >}})
        -   When coarctation is inferior to this site (_postductal coarctation_) -> good collateral circulation develops between proximal and distal parts of aorta through intercostal and internal thoracic arteries
            -   Compatible with many years of life because of collateral circulation

    <!--list-separator-->

    -  Injury to [recurrent laryngeal nerve]({{< relref "recurrent_laryngeal_nerve" >}})

        -   Recurrent laryngeal nerve supplies all intrinsic muscle of larynx except one
            -   Any diagnostic procedure or disease in superior mediastinum may injure these nerves -> affect voice
        -   Left recurrent laryngeal nerve winds around arch of aorta and ascends between trachea and esophagus -> may be involved in:
            1.  Bronchogenic or esophageal carcinoma
            2.  Enlargement of mediastinal lymph nodes
            3.  Aneurysm of arch of aorta
                -   Nerve may be stretched by the dilated arch


#### [Describe the procedure of thoracocentesis, highlighting the rationale behind the use of the technique.]({{< relref "describe_the_procedure_of_thoracocentesis_highlighting_the_rationale_behind_the_use_of_the_technique" >}}) {#describe-the-procedure-of-thoracocentesis-highlighting-the-rationale-behind-the-use-of-the-technique-dot--describe-the-procedure-of-thoracocentesis-highlighting-the-rationale-behind-the-use-of-the-technique-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  [Thoracocentesis]({{< relref "thoracocentesis" >}})

        -   Used when you need to insert a hypodermic needle through an intercostal space into the pleural cavity to obtain a sample of fluid or to remove blood or pus
        -   To avoid damage ot intercostal nerve and vessels -> needl einserted **superior to rib**, high enough to avoid collateral branches
        -   Needle passes through intercostal muscles and costal parietal pleura into pleural cavity
        -   When patient in upright position -> intrapleural fluid accumulates in [costodiaphragmatic recess]({{< relref "costodiaphragmatic_recess" >}})
        -   Inserting needle into 9th intercostal space in midaxillary line during expiration -> **aovids inferior border of lung**
        -   **Needle should be angled upward** to avoid penetrating deep side of recess (thin layer of diaphragmatic parietal pleura and diaphragm overlying the liver)

    <!--list-separator-->

    -  Insertion of a Chest Tube

        -   Major amounts of air, blood, serous fluid, and/or pus in pleural cavity typically remove dby insertion of chest tube
        -   Short incision made in 5th or 6th intercostal space in midaxillary line
        -   Tube may be directed **superiorly** toward cervical pleura for **air removal**
        -   Tube may be directed **inferiorly** towards [Costodiaphragmatic recess]({{< relref "costodiaphragmatic_recess" >}}) for **fluid drainage**
        -   Extracorporeal (outside body) end of tube connected to an underwater drainage system w/ controlled suction -> **prevent air form being sucked back into pleural cavity**
        -   Removal of air allows reinflation of a collapsed lung
        -   Failure to remove fluid -> development of resistant fibrous covering over lung that inhibits expansion unless it is peeled off (lung decoritcation)


#### [Describe the presentation of pneumothorax, hydrothorax and hemothorax.]({{< relref "describe_the_presentation_of_pneumothorax_hydrothorax_and_hemothorax" >}}) {#describe-the-presentation-of-pneumothorax-hydrothorax-and-hemothorax-dot--describe-the-presentation-of-pneumothorax-hydrothorax-and-hemothorax-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  [Pneumothorax]({{< relref "pneumothorax" >}})

        -   Entry of air into pleural cavity
        -   Usually from penetrating wound of parietal pleura
        -   Fractured ribs may also tear visceral pleural and lung

    <!--list-separator-->

    -  [Hydrothorax]({{< relref "hydrothorax" >}})

        -   Accumulation of a significant amount of fluid in pleural cavity
        -   Can result from pleural effusion

    <!--list-separator-->

    -  [Hemothorax]({{< relref "hemothorax" >}})

        -   Blood enters pleural cavity
        -   Results more commonly from injury to a major intercostal or internal thoracic vessel than from laceration of a lung

    <!--list-separator-->

    -  [Hemopneumothorax]({{< relref "hemopneumothorax" >}})

        -   Fluid **and** air accumulate in pleural cavity
        -   Visible on radiograph as an air-fluid level or interface


#### [Describe the meaning of pleural recesses and describe the locations of the costodiaphragmatic and costmediastinal recesses.]({{< relref "describe_the_meaning_of_pleural_recesses_and_describe_the_locations_of_the_costodiaphragmatic_and_costmediastinal_recesses" >}}) {#describe-the-meaning-of-pleural-recesses-and-describe-the-locations-of-the-costodiaphragmatic-and-costmediastinal-recesses-dot--describe-the-meaning-of-pleural-recesses-and-describe-the-locations-of-the-costodiaphragmatic-and-costmediastinal-recesses-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  [Pleural recesses]({{< relref "pleural_recess" >}})

        -   Several parts of thorax where lung (and consequently the visceral pleura) is **not in contact with the parietal pleura** -> _Pleural reflections_

        <!--list-separator-->

        -  [Costodiaphragmatic recesses]({{< relref "costodiaphragmatic_recess" >}})

            -   There are two - right and left
            -   Spaces between lower border of lung and pleural reflections from chest wall onto diaphgram
            -   Potential spaces where flui dmay accumulate
                -   Excess fluid may be drained (pleural tap or thoracentesis) from pleural cavity **without penetrating lungs**
            -   **Only in forced inspiration do the lungs expand into these recesses**

        <!--list-separator-->

        -  [Costomediastinal recess]({{< relref "costomediastinal_recess" >}})

            -   On left side between anterior chest wall and pericardial sac
            -   heart lies close to anterior thoracic wall and costal pleura is close to the mediastinal pleura


#### [Describe the events leading to lung collapse.]({{< relref "describe_the_events_leading_to_lung_collapse" >}}) {#describe-the-events-leading-to-lung-collapse-dot--describe-the-events-leading-to-lung-collapse-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  Lung collapse

        -   If a penetrating wound opens through the thoracic wall or surface of lungs -> air sucked into pleural cavity because of negative pressure
        -   Surface tension adhering visceral to parietal pleura broken -> lung collapse -> expellation of most of its air because of elastic recoil
            -   Pleural cavity (normally a potential space) becomes a **real space**
        -   Pulmonary cavity **doesn't increase in size during inspiration**
            -   Evident radiographically on affected side by:
                1.  Elevation of diaphragm above usual level
                2.  Intercostal space narrowing
                3.  Displacement of mediastinum towards affected side


#### [Describe the etiology of pleuritis.]({{< relref "describe_the_etiology_of_pleuritis" >}}) {#describe-the-etiology-of-pleuritis-dot--describe-the-etiology-of-pleuritis-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  [Pleuritis]({{< relref "pleuritis" >}})

        -   During inspiration and expiration, sliding of normally smooth, moist pleurae makes no detectable sound during ascultation of lungs
        -   Inflammatoin of pleura (pleuritic) makes lung surfaces rough
            -   Sounds like clump of hairs being rolled between fingers
        -   Inflamed surfaces of pleura may also cause parietal and visceral layers of pleura to adhere (pleural adhesion)
        -   Acute pleuritis marked by **sharp, stabbing pain especially on exertion**
        -   Typically a result of infection, [cancer]({{< relref "cancer" >}}), and [congestive heart failure]({{< relref "congestive_heart_failure" >}})
        -   Typical complaints: chest pain, SOB, and pain in shoulder


#### [Describe the etiology of herpes zoster as well as the clinical presentation of the disease.]({{< relref "describe_the_etiology_of_herpes_zoster_as_well_as_the_clinical_presentation_of_the_disease" >}}) {#describe-the-etiology-of-herpes-zoster-as-well-as-the-clinical-presentation-of-the-disease-dot--describe-the-etiology-of-herpes-zoster-as-well-as-the-clinical-presentation-of-the-disease-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  [Herpes zoster]({{< relref "herpes_zoster" >}})

        -   Caused by _Varicella zoster_ [virus]({{< relref "virus" >}})
        -   Virus remains dormant in the _dorsal root gangla_ -> reactivates later in life as "shingles" within dermatome supplied by a particular DRG
        -   After invading a ganglion, virus produces a sharp burning pain in [Dermatome]({{< relref "dermatome" >}})
            -   Affected skin area becomes red -> **stripe dermatome pattern**
            -   Vesicular eruptions appear
        -   Pain can precede or follow skin eruptions
        -   Weakness form motor involvement occurs in up to 5% of people
            -   Typically occurs in same myotomal distribution as dermatomal pain
        -   Can present with sensory issues (e.g. burning, itching) and a blistering rash within that dermatome


#### [Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.]({{< relref "describe_the_common_clinical_procedures_involving_the_heart_include_a_description_of_the_procedure_s_and_the_structure_s_that_are_accessed_or_repaired" >}}) {#describe-the-common-clinical-procedures-involving-the-heart-dot-include-a-description-of-the-procedure--s--and-the-structure--s--that-are-accessed-or-repaired-dot--describe-the-common-clinical-procedures-involving-the-heart-include-a-description-of-the-procedure-s-and-the-structure-s-that-are-accessed-or-repaired-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  Clinical procedures related to [heart]({{< relref "heart" >}}) structures:

        <!--list-separator-->

        -  [Cardiac catheterization]({{< relref "cardiac_catheterization" >}}):

            -   Radiopaque catheter inserted into peripheral vein (e.g. femoral vein) and passed under fluoroscopic control into the [right atrium]({{< relref "right_atrium" >}}), [right ventricle]({{< relref "right_ventricle" >}}), [pulmonary trunk]({{< relref "pulmonary_trunk" >}}) and [pulmonary arteries]({{< relref "pulmonary_artery" >}}) respectively
            -   Intracardiac pressures can be recorded and blood samples may be removed
            -   If radopaque contrast medium is injected -> can be followed through heart and great vessels using serially exposed X-ray films

        <!--list-separator-->

        -  [Coronary angiography]({{< relref "coronary_angiography" >}})

            -   Coronary arteries visualized with coronary arteriograms
            -   Long narrow catheter passed into ascending aorta via femoral artery
            -   Under fluoroscopic control, tip of catheter placed just inside opening of a coronary artery
            -   Small injection of radiopaque contrast material made -> cineradiographs taken to show lumen of artery and its branches, as well as any stenotic areas present
            -   Noninvasive CT or MR angiography is replacing invasive conventional methods

        <!--list-separator-->

        -  [Coronary artery bypass]({{< relref "coronary_artery_bypass" >}})

            -   Segment of artery or vein is connected to ascending aorta or to proximal part of a coronary artery -> connected to coronary artery distal to stenosis
            -   Great saphenous vein is commonly harvested for coronary bypass because:
                1.  Has a diameter equal to or greater than that of oronary arteries
                2.  Can be easily dissected from lower limb
                3.  Offers relatively lengthy portion with minimum occurrence of valves or branching
            -   Use of radial artery in bypass surgery has become more common
            -   Coronary bypass graft shunts blood from aorta to a steonic coronary artery -> increase flow distal to obstrution
                -   i.e. detour around stenotic area
            -   Revascularization of myocardium may also be achieved by surgically anastomosing an internal thoracic artery with a coronary artery


#### [Describe the clinical significance of the transverse pericardial sinus.]({{< relref "describe_the_clinical_significance_of_the_transverse_pericardial_sinus" >}}) {#describe-the-clinical-significance-of-the-transverse-pericardial-sinus-dot--describe-the-clinical-significance-of-the-transverse-pericardial-sinus-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  Surgical significance of the [transverse pericardial sinus]({{< relref "transverse_pericardial_sinus" >}})

        -   After pericardial sac is opened anteriorly, a finger can be passed through the transverse pericardial sinus posterior to ascending aorta and pulmonary trunk
        -   By passing a surgical clamp or a ligature around these large vessels, inserting tubes of a coronary bypass machine, and then tightening ligature, surgeons can **stop or divert circulation of blood in these arteries while performing cardiac surgery**


#### [Describe the causes of pulmonary emboli and the clinical consequences associated with it.]({{< relref "describe_the_causes_of_pulmonary_emboli_and_the_clinical_consequences_associated_with_it" >}}) {#describe-the-causes-of-pulmonary-emboli-and-the-clinical-consequences-associated-with-it-dot--describe-the-causes-of-pulmonary-emboli-and-the-clinical-consequences-associated-with-it-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  [Pulmonary embolism]({{< relref "pulmonary_embolism" >}})

        -   Obstruction of a pulmonary artery by a blood clot (_embolus_)
            -   Common cause of morbidity and mortality
        -   Forms when a blood clot, fat globule, or air bubble travels in blood to lungs after a compound fracture
            -   Embolus pases through right side of heart -> lung throug hpulmonary artery -> blocked pulmonary artery or branch
        -   Immediate result -> partial or complete obstruction of blood flow to lung
            -   Results in a section or whole lung that is ventilated but not perfused with blood
        -   Large embolus -> major decrease in oxygenation of blood -> patient suffers **acute respiratory distress**
            -   Volume of blood arriving from systemic circuit cannot be pushed through pulmonary circuit -> **right side of heart acutely dilated**
            -   Death may accur in a few minutes
        -   Medium-size embolus -> block artery supplying bronchopulmonary segment -> pulmonary infarct
        -   In physically active people, collateral circulation often exists and develops further when there is a PE -> less likely for infarction to occur
        -   When an area of visceral pleura is also deprived of blood -> becomes inflamed (_pleuritic_) and irritates or becomes fused to sensitive parietal pleura -> pain
            -   Pain referred to cutaneous distribution of intercostal nerves to thoracic wall or, inc ase of inferior nerves,to anterio abdominal wall


#### [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}}) {#describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot--describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    -   When a coronary artery becomes stenotic (narrowed) or occluded (closed off) -> region of heart supplied by that artery as [ischemia]({{< relref "ischemia" >}}) (lack of oxygen)
    -   Severe enough ischemia + lasts long enough -> [myocardial infarction]({{< relref "myocardial_infarction" >}})
        -   If patient survives -> ECG changes and appearance of myocardial enzymes in peripheral blood
    -   [Right ventricle]({{< relref "right_ventricle" >}}) + [right atria]({{< relref "right_atrium" >}}) do not develop as much pressure -> less oxygen demand -> more rarely infarcted
    -   In general, occlusion of the [anterior interventricular artery]({{< relref "anterior_interventricular_artery" >}}) -> infarction of anteroapical portion of [left ventricle]({{< relref "left_ventricle" >}})
    -   Occlusion of [circumflex artery]({{< relref "circumflex_artery" >}}) -> infarction of posterolateral wall of left ventricle
    -   Total occlusion of [left coronary artery]({{< relref "left_coronary_artery" >}}) is **usually fatal**
    -   Occlusion of [right coronary artery]({{< relref "right_coronary_artery" >}}) -> infarct to diaphgragmatic surface of right and left ventricles
    -   **If left coronary artery is dominant** -> right coronary artery occlusion **does not usually result in infarction**

    <!--list-separator-->

    -  [Atherosclerosis]({{< relref "atherosclerosis" >}}) and [MI]({{< relref "myocardial_infarction" >}})

        -   Atherosclerotic process: lipid deposits in tunica intima (innermost vascular lining) of the [coronary arteries]({{< relref "coronary_artery" >}})
        -   Begins in early adulthood -> slowly results in stenosis of lumina of arteries
        -   Collateral channels connecting one coronary artery with the other expand -> initially permits adequate perfusion during relative inactivity
            -   Not enough when heart needs to perform increased amount of work (e.g. strenous exercise)


#### [Describe the anatomical locations for auscultation of the heart valves.]({{< relref "describe_the_anatomical_locations_for_auscultation_of_the_heart_valves" >}}) {#describe-the-anatomical-locations-for-auscultation-of-the-heart-valves-dot--describe-the-anatomical-locations-for-auscultation-of-the-heart-valves-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    -   [Pulmonary valve]({{< relref "pulmonary_valve" >}}): Second intercostal space just to the left of the sternum
    -   [Aortic valve]({{< relref "aortic_valve" >}}): Second right intercostal space at the edge of the sternum
    -   [Tricuspid valve]({{< relref "tricuspid_valve" >}}): Over left half of inferior end of body of sternum
    -   [Bicuspid valve]({{< relref "bicuspid_valve" >}}): Fifth left intercostal space, approximately in the mid-clavicular line


#### [Describe lymphatic flow in the breast.]({{< relref "describe_lymphatic_flow_in_the_breast" >}}) {#describe-lymphatic-flow-in-the-breast-dot--describe-lymphatic-flow-in-the-breast-dot-md}

<!--list-separator-->

-  From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}})

    <!--list-separator-->

    -  [Breast]({{< relref "breast" >}}) lymphatics

        -   The vast majority of lymph (75%) from the breast **is directed towards** _axillary nodes_
            -   Remaining lymph (especially from medial quadrants) directed to _parasternal nodes_
        -   Some drainage from the inferior region flows along the anterior abdominal wall to the inguinal nodes in the groin

    <!--list-separator-->

    -  [Breast]({{< relref "breast" >}}) quadrants

        {{< figure src="/ox-hugo/_20211028_153603screenshot.png" caption="Figure 1: Four quadrants of the breast" >}}

    <!--list-separator-->

    -  Metastasis of [Breast Cancer]({{< relref "breast_cancer" >}})

        -   Carcinomas of the brast are malignant tumors
            -   Usually adenocarcinomas (glandular cancer) arising from epithelial cells of lactiferous ducts in mamary gland lobules
        -   ~60% of malignant breast tumors arise from _superior lateral quadrant_ -> tumor cells that break away picked up by lymph and travel to lymph nodes in _axillary region_
        -   Breast cancer typically spreads form breast by lymphatic vessels
            -   Most common site is _axillary node_
        -   Surgical removal of axillary lymph nodes can disrupt lymph flow from upper limb -> lymphedema in affected limb

    <!--list-separator-->

    -  Skin changes in [breast cancer]({{< relref "breast_cancer" >}})

        -   Lymphedema
        -   "Pitting" or "dimpling" along skin of breast
        -   Presence of lump under skin
        -   Rash-like color changes
        -   Mammography is primarily used for screening
            -   In conventional mammography, denser structures appear light


### Unlinked references {#unlinked-references}

[Show unlinked references]
