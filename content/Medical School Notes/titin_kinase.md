+++
title = "Titin kinase"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Discuss the role of titin as a component of mechano-chemical signal transduction in skeletal muscle.]({{< relref "discuss_the_role_of_titin_as_a_component_of_mechano_chemical_signal_transduction_in_skeletal_muscle" >}}) {#discuss-the-role-of-titin-as-a-component-of-mechano-chemical-signal-transduction-in-skeletal-muscle-dot--discuss-the-role-of-titin-as-a-component-of-mechano-chemical-signal-transduction-in-skeletal-muscle-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Titin**

    **Has ser/thr kinase within its structure** - [Titin kinase]({{< relref "titin_kinase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
