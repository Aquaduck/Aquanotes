+++
title = "Diaphysis"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Mid-shaft of [long bone]({{< relref "tubular_bone" >}})


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Nutrient canal]({{< relref "nutrient_canal" >}}) {#nutrient-canal--nutrient-canal-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Travels through the [Diaphysis]({{< relref "diaphysis" >}})

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone formation > Endochondral bone formation > Steps**

    As proliferating cartilage of the [Growth plate]({{< relref "epiphyseal_plate" >}}) grows away from the [Diaphysis]({{< relref "diaphysis" >}}) (extending the length of the bone) -> cartilage closest to the diaphysis is **converted to ossified bone**

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone formation > Endochondral bone formation > Steps**

    A region of cartilage is preserved between the [Epiphysis]({{< relref "epiphysis" >}}) and the [Diaphysis]({{< relref "diaphysis" >}}) -> becomes the [Growth plate]({{< relref "epiphyseal_plate" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone formation > Endochondral bone formation > Steps**

    A bone collar appears around the shaft as the center (destined to be the [Diaphysis]({{< relref "diaphysis" >}})) of the "cartilage model" and is ossified

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Parts of a tubular bone**

    [Nutrient canal]({{< relref "nutrient_canal" >}}) goes through the cortex of the [Diaphysis]({{< relref "diaphysis" >}}) -> blood vessel travels to nourish the marrow cavity

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Parts of a tubular bone**

    [Diaphysis]({{< relref "diaphysis" >}}):  mid-shaft of bone

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
