+++
title = "Cardiac catheterization"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.]({{< relref "describe_the_common_clinical_procedures_involving_the_heart_include_a_description_of_the_procedure_s_and_the_structure_s_that_are_accessed_or_repaired" >}}) {#describe-the-common-clinical-procedures-involving-the-heart-dot-include-a-description-of-the-procedure--s--and-the-structure--s--that-are-accessed-or-repaired-dot--describe-the-common-clinical-procedures-involving-the-heart-include-a-description-of-the-procedure-s-and-the-structure-s-that-are-accessed-or-repaired-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Clinical procedures related to heart structures:**

    <!--list-separator-->

    -  [Cardiac catheterization]({{< relref "cardiac_catheterization" >}}):

        -   Radiopaque catheter inserted into peripheral vein (e.g. femoral vein) and passed under fluoroscopic control into the [right atrium]({{< relref "right_atrium" >}}), [right ventricle]({{< relref "right_ventricle" >}}), [pulmonary trunk]({{< relref "pulmonary_trunk" >}}) and [pulmonary arteries]({{< relref "pulmonary_artery" >}}) respectively
        -   Intracardiac pressures can be recorded and blood samples may be removed
        -   If radopaque contrast medium is injected -> can be followed through heart and great vessels using serially exposed X-ray films


### Unlinked references {#unlinked-references}

[Show unlinked references]
