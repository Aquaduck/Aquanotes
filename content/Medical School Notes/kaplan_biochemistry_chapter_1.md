+++
title = "Kaplan Biochemistry Chapter 1"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Chapter 1 {#chapter-1}


### 1.1 Amino Acids Found in Proteins {#1-dot-1-amino-acids-found-in-proteins}

-   Molecules that contain two functional groups: an amino group (-NH<sub>2</sub>) and a carboxyl group (-COOH)
-   <span class="underline">ɑ-amino acids</span>: amino group and carboxyl group are both bound to ɑ-carbon
-   The **side chains** (R groups) of [AAs]({{< relref "amino_acid" >}}) determine their chemical properties


#### Stereochemistry of AAs {#stereochemistry-of-aas}

-   For most [AAs]({{< relref "amino_acid" >}}), the ɑ-carbon is a [chiral]({{< relref "chirality" >}}) (or stereogenic) center
    -   Thus, most AAs are optically active
    -   The **one exception** is **Glycine**, which has a hydrogen atom as its R group making it achiral
-   All [AAs]({{< relref "amino_acid" >}}) **except for cysteine** have an (S) absolute configuration
-   [L-amino acids]({{< relref "amino_acid" >}}) are the only ones found in eukaryotic proteins


#### Structures of the Amino Acids {#structures-of-the-amino-acids}

<!--list-separator-->

-  Nonpolar, Nonaromatic Side Chains     :ATTACH:

    {{< figure src="/ox-hugo/_20210707_145429screenshot.png" >}}

    -   Seven amino acids fall into this category
        1.  Glycine
        2.  Alanine
        3.  Valine
        4.  Leucine
        5.  Isoleucine
        6.  Methionine
        7.  Proline
    -   Methionine contains a sulfur atom, but because of the attached methyl-group it is still considered relatively nonpolar
    -   Proline forms a cyclic amino acid

<!--list-separator-->

-  Aromatic Side Chains     :ATTACH:

    {{< figure src="/ox-hugo/_20210707_145618screenshot.png" >}}

    -   Three [amino acids]({{< relref "amino_acid" >}}) have uncharged aromatic side chains
        1.  Tryptophan
            -   Largest of three
            -   Double-ring system that contains a nitrogen atom
        2.  Phenylalanine
            -   Smallest of three
            -   Benzyl side chain (benzene ring + -CH<sub>2</sub>- group)
        3.  Tyrosine
            -   Add an -OH group to phenylalanine
            -   Because of this, tyrosine is **relatively polar** unlike the other two

<!--list-separator-->

-  Polar Side Chains     :ATTACH:

    {{< figure src="/ox-hugo/_20210707_145654screenshot.png" >}}

    -   Five [amino acids]({{< relref "amino_acid" >}}) have side chains that are polar _but not aromatic_
        1.  Serine
            -   -OH group present -> **polar**
        2.  Threonine
            -   -OH group present -> **polar**
        3.  Asparagine
            -   Amide side chain
        4.  Glutamine
            -   Amide side chain
        5.  Cysteine
            -   has a thiol (-SH) group
            -   S-H bond _weaker_ than O-H bond
                -   b/c sulfur is larger and less electronegative
            -   Thiol group prone to oxidation
    -   Unlike the amino group in all [AAs]({{< relref "amino_acid" >}}), the **amide nitrogens do not gain or lose proteins with changes to PH -> do not become charged**

<!--list-separator-->

-  Negatively Charged (Acidic) Side Chains     :ATTACH:

    -   Only **2** of the 20 [amino acids]({{< relref "amino_acid" >}}) have negative charges on their side chains
        1.  Aspartic acid (Aspartate)
        2.  Glutamic acid (Glutamate)

    **On the MCAT you're more likely to see anion names**

    {{< figure src="/ox-hugo/_20210707_201333screenshot.png" >}}

<!--list-separator-->

-  Positively Charged (Basic) Side Chains     :ATTACH:

    -   3 [amino acids]({{< relref "amino_acid" >}}) have side chains that have positively charged nitrogen atoms
        1.  Lysine
            -   Terminal primary amino group
        2.  Arginine
            -   Three N atoms in side chain -> positive charged delocalized between them
        3.  Histidine
            -   Aromatic ring with two N atoms (called _imidazole_)
                -   At physiologic pH, one N atom is protonated
                -   Under acidic conditions, **both** N atoms are protonated

    {{< figure src="/ox-hugo/_20210707_201907screenshot.png" >}}


### 1.3 Peptide Bond Formation and Hydrolysis {#1-dot-3-peptide-bond-formation-and-hydrolysis}


#### Overview {#overview}

-   _Peptide bonds_: amide bond forms between the carboxylic acid of one [AA]({{< relref "amino_acid" >}}) and the amino group of another AA

{{< figure src="/ox-hugo/_20210716_123221screenshot.png" width="400" >}}


### 1.5 Tertiary and Quaternary Protein Structure {#1-dot-5-tertiary-and-quaternary-protein-structure}

-   _Disulfide bond_: bond formed between two cysteine molecules when they are oxidized to cystine
    -   Create loops in the protein chain
    -   Requires the loss of two protons and two electrons (oxidation)

{{< figure src="/ox-hugo/_20210716_124401screenshot.png" >}}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 3. Describe the bonds and forces that contribute to the conformation of proteins and the interaction of proteins with other biomolecules > Peptide, disulfide, and hydrogen bonds**

    [1.5 Tertiary and Quaternary Protein Structure]({{< relref "kaplan_biochemistry_chapter_1" >}})

    ---

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 3. Describe the bonds and forces that contribute to the conformation of proteins and the interaction of proteins with other biomolecules > Peptide, disulfide, and hydrogen bonds**

    [1.3 Peptide Bond Formation and Hydrolysis]({{< relref "kaplan_biochemistry_chapter_1" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
