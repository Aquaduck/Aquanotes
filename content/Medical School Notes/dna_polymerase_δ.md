+++
title = "DNA Polymerase-δ"
author = ["Arif Ahsan"]
date = 2021-08-17T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   A type of [DNA Polymerase]({{< relref "dna_polymerase" >}}) in [eukaryotes]({{< relref "eukaryote" >}})
-   Has **3'-5' exonuclease activity**


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Steps in replication**

    [DNA Pol-δ]({{< relref "dna_polymerase_δ" >}})  synthesizes lagging strand

    ---

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes**

    [pol ẟ]({{< relref "dna_polymerase_δ" >}}) (delta)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
