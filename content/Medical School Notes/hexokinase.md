+++
title = "Hexokinase"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Catalyzes ATP-dependent phosphorylation of [glucose]({{< relref "glucose" >}}) to [G6P]({{< relref "glucose_6_phosphate" >}}) to trap it within a cell
-   [Allosterically]({{< relref "allosteric_regulation" >}}) regulated
    -   Inhibited by G6P


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}}) {#list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated--list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated-dot-md}

<!--list-separator-->

-  **🔖 Examples in glucose catabolism > Product inhibition**

    [Hexokinase]({{< relref "hexokinase" >}}) inhibited by [G6P]({{< relref "glucose_6_phosphate" >}})

    ---

<!--list-separator-->

-  **🔖 Examples in glucose catabolism > Substrate availability**

    [Hexokinase]({{< relref "hexokinase" >}}) needs glucose

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Glycolysis**

    <!--list-separator-->

    -  [Hexokinases]({{< relref "hexokinase" >}}) I-III

        -   In most tissues (other than pancreas + liver), the first reaction in glycolytic pathway is catalyzed by one of the isozymes of hexokinase
        -   Hexokinase catalyzes the **first** of three highly-regulated **irreversible** glycolytic reactions
            -   Hexokinase catalyzes ATP-dependent phosphorylation of glucose to [G6P]({{< relref "glucose_6_phosphate" >}})
                -   G6P cannot diffuse out of the cell -> allows for continuous glucose uptake by cells
        -   Hexokinase enzymes are **inhibited by [G6P]({{< relref "glucose_6_phosphate" >}}), the reaction product**
            -   Low K<sub>m</sub> (high affinity) for glucose
            -   Low V<sub>max</sub> (low capacity) for glucose phosphorylation
                -   Allows for efficient phosphorylation and metabolism of glucose even w/ low tissue concentrations
            -   This makes them **allosteric enzymes**


#### [Hypoxia-inducible factor 1]({{< relref "hypoxia_inducible_factor_1" >}}) {#hypoxia-inducible-factor-1--hypoxia-inducible-factor-1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Stimulates production of [Hexokinase]({{< relref "hexokinase" >}})

    ---


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 HIF-1 target genes involved in glucose and energy metabolism**

    HIF-1 activates the transcription of _HK1_ and _HK2_ -> encode [Hexokinase]({{< relref "hexokinase" >}}) (**upregulation of glycolytic enzymes**)

    ---


#### [Glucokinase]({{< relref "glucokinase" >}}) {#glucokinase--glucokinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A special [Hexokinase]({{< relref "hexokinase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
