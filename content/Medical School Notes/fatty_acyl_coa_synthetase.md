+++
title = "Fatty acyl-CoA synthetase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  [Fatty acyl-CoA synthetase]({{< relref "fatty_acyl_coa_synthetase" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   In the **investment phase of fatty acid oxidation**
        -   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
            -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
            -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism**

    <!--list-separator-->

    -  [Fatty acyl-CoA synthetase]({{< relref "fatty_acyl_coa_synthetase" >}})

        -   Required to "activate" cytoplasmic fatty acids for entry into mitochondria for [β-oxidation]({{< relref "β_oxidation" >}})
        -   Catalyzes formation of a fatty acyl-CoA


### Unlinked references {#unlinked-references}

[Show unlinked references]
