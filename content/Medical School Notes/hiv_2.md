+++
title = "HIV-2"
author = ["Arif Ahsan"]
date = 2021-08-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [HIV]({{< relref "human_immunodeficiency_virus" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [HIV Infection and AIDS]({{< relref "hiv_infection_and_aids" >}}) {#hiv-infection-and-aids--hiv-infection-and-aids-dot-md}

<!--list-separator-->

-  **🔖 Practice essentials**

    [HIV]({{< relref "human_immunodeficiency_virus" >}}) disease is caused by infection with [HIV-1]({{< relref "hiv_1" >}}) or [HIV-2]({{< relref "hiv_2" >}}), which are retroviruses in the _Retroviridae_ family, _Lentivirus_ genus

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
