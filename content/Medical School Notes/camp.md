+++
title = "cAMP"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Name the second messenger produced in response to glucagon-induced cellular signaling and how it functions to elicit the appropriate intracellular response]({{< relref "name_the_second_messenger_produced_in_response_to_glucagon_induced_cellular_signaling_and_how_it_functions_to_elicit_the_appropriate_intracellular_response" >}}) {#name-the-second-messenger-produced-in-response-to-glucagon-induced-cellular-signaling-and-how-it-functions-to-elicit-the-appropriate-intracellular-response--name-the-second-messenger-produced-in-response-to-glucagon-induced-cellular-signaling-and-how-it-functions-to-elicit-the-appropriate-intracellular-response-dot-md}

<!--list-separator-->

-  Function of [cAMP]({{< relref "camp" >}})

    1.  Upon activation by glucagon -> cAMP binds to two regulatory subunits (R) of inactive [PKA]({{< relref "protein_kinase_a" >}}) -> releases two catalytically-active subunits + activation of PKA -> phosphorylation of substrates -> activation of enzymes involved in catabolism

    [cAMP]({{< relref "camp" >}}) is the "second messenger" = the first intracellular effector formed in response to the first messenger [glucagon]({{< relref "glucagon" >}})

    ---


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects > Reversal of glucagon's actions**

    [cAMP]({{< relref "camp" >}}) can be rapidly hydrolyzed to 5'-AMP by [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}}) through cleavage of cyclic 3',5'-phosphodiester bond

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase > Takeaways**

    [cAMP]({{< relref "camp" >}}) binds to the two regulatory subunits (R) of the inactive [PKA]({{< relref "protein_kinase_a" >}}) -> release of two catalytically-active subunits -> activation of PKA

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase > Takeaways**

    Activation of adenylate-cyclase -> converts ATP to [cAMP]({{< relref "camp" >}})

    ---


#### [Describe the cyclic-AMP (cAMP) second messenger signal pathway from hormone binding to activation of protein kinase-A.]({{< relref "describe_the_cyclic_amp_camp_second_messenger_signal_pathway_from_hormone_binding_to_activation_of_protein_kinase_a" >}}) {#describe-the-cyclic-amp--camp--second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot--describe-the-cyclic-amp-camp-second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  [cAMP]({{< relref "camp" >}}) Activation

        1.  Signal activates receptor -> **conformational change in receptor**
        2.  G<sub>s</sub> complex activated -> activates [adenylyl cyclase]({{< relref "adenylate_cyclase" >}}) -> formation of cAMP


### Unlinked references {#unlinked-references}

[Show unlinked references]
