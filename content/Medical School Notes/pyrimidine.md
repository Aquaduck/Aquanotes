+++
title = "Pyrimidine"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP.]({{< relref "outline_the_general_features_of_the_pathways_of_de_novo_synthesis_of_the_two_purine_and_three_pyrimidine_ribonucleotides_noting_the_chemical_changes_required_for_conversion_of_utp_to_ctp_and_imp_to_atp_gtp" >}}) {#outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot--outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e**

    <!--list-separator-->

    -  [Pyrimidine]({{< relref "pyrimidine" >}}) synthesis and degradation

        -   The pyrimidine ring is synthesized **before** being attached to [ribose 5-phosphate]({{< relref "ribose_5_phosphate" >}}) (donated by PRPP)

        <!--list-separator-->

        -  Synthesis of [carbamoyl phosphate]({{< relref "carbamoyl_phosphate" >}})

            -   Synthesized from glutamine and carbon dioxide
            -   Catalyzed by [carbamoyl phosphate synthetase II]({{< relref "carbamoyl_phosphate_synthetase_ii" >}})
                -   CPS II is inhibited by UTP (end-product inhibition)


### Unlinked references {#unlinked-references}

[Show unlinked references]
