+++
title = "Osmosis - Glycolysis"
author = ["Arif Ahsan"]
date = 2021-07-19T00:00:00-04:00
tags = ["medschool", "osmosis", "videos", "source"]
draft = false
+++

Link: <https://www.osmosis.org/learn/Glycolysis>


## [Video]({{< relref "glycolysis" >}}) {#video--glycolysis-dot-md}

-   _Glycolysis_ - glucose converted into 2x pyruvate -> forms ATP
-   Glycolysis can occur **even in low oxygen**
-   Two phases
    1.  Energy consuming phase
    2.  Energy producing phase


### Pathway {#pathway}

1.  Glucose enters bloodstream via small intestine
2.  Stimulates pancreas to secrete insulin from beta-islet cells

    {{< figure src="/ox-hugo/_20210719_120939screenshot.png" width="500" >}}
3.  Glucose transporters (GLUT) transport glucose into cell
4.  Kinases phosphorylate glucose -> **can't easily diffuse out of cell**

    -   **-1 ATP**
    -   Hexokinase/glucokinase convert glucose to **g6p** - glucose w/ phosphate on 6th carbon
        -   _Hexokinase_: found in all cells
        -   _Glucokinase_: induced by insulin
    -   Reaction is **irreversible**

    {{< figure src="/ox-hugo/_20210719_121313screenshot.png" width="500" >}}
5.  g6p converted to **fructose-6-phosphate** (f6p) via _phosphoglucoisomerase_
6.  _Phosphofructokinase-1_ (_PFK1_) phosphorylates f6p into **fructose 1,6-bisphosphate**

    -   **Irreversible**
    -   **Rate-limiting**
        -   Cells regulate this via _PFK2_ -> phosphorylates **2nd** carbon -> **fructose 2,6-bisphosphate**
            -   Activated by _insulin_
                -   **Increases [PFK1]**
            -   Inhibited by _glucagon_, _ATP_ and _citrate_
                -   Decreases [PFK1]
    -   **-1 ATP**
    -   **Determines speed of glycolysis**

    {{< figure src="/ox-hugo/_20210719_122031screenshot.png" >}}

7.  _Aldolase_ converts F-1,6-B -> **G3P** (glyceraldehyde 3-phosphate) + **DHAP** (dihydroacetone-phosphate)
    -   DHAP converted via _isomerase_ into **G3P** -> **2x G3P produced per 1 molecule of glucose**
8.  _G3P-dehydrogenase_ phosphorylates G3P -> **1,3-BPG** (1,3-Biphosphoglycerate)

    -   Removes 1H from G3P -> gives it to NAD<sup>+</sup> to form NADH

    {{< figure src="/ox-hugo/_20210719_123639screenshot.png" width="500" >}}
9.  _Phosphoglycerate kinase_ converts 1,3-BPG -> **3-phosphoglycerate**
    -   Produces 1 ATP per molecule -> **+2 ATP**
10. _Mutase_ converts 3-phosphoglycerate -> **2-phosphoglycerate**
11. _Enolase_ converts 2-phosphoglycerate -> **PEP** (phosphoenolpyruvate)

    -   Uses up 1 H<sub>2</sub>O

    {{< figure src="/ox-hugo/_20210719_124002screenshot.png" width="500" >}}
12. _Pyruvate kinase_ converts PEP -> **pyruvate**

    -   **+2 ATP**
    -   _upregulated_ by F-1,6-B
    -   _downregulated_ by ATP and Alanine
        -   Alanine produced from skeletal muscle breakdown during fasting -> signals that more glucose is needed, prevents from being converted into ATP

    {{< figure src="/ox-hugo/_20210719_124325screenshot.png" width="500" >}}


### Anaerobic respiration {#anaerobic-respiration}

-   _Lactate dehydrogenase_ can convert pyruvate + NADH -> lactate + NAD<sup>+</sup>
    -   NAD<sup>+</sup> needed to keep glycolysis going
    -   Lactate removed by **kidneys**

{{< figure src="/ox-hugo/_20210719_124525screenshot.png" width="500" >}}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
