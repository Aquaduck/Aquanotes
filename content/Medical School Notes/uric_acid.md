+++
title = "Uric acid"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The final product of human [purine]({{< relref "purine" >}}) degradation


## Backlinks {#backlinks}


### 8 linked references {#8-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Diseases associated with purine degradation > Gout**

    <!--list-separator-->

    -  Overproduction of [uric acid]({{< relref "uric_acid" >}})

        -   Less common cause of [Gout]({{< relref "gout" >}})
        -   Mutations in X-linked [PRPP synthetase]({{< relref "prpp_synthetase" >}}) gene -> increased V<sub>max</sub> for the production of PRPP + lower K<sub>m</sub> for ribose 5-phosphate, or decresaed sensitivity to purine nucleotides (allosteric inhibitors)
            -   Increased availability of PRPP -> **increased purine production** -> **elevated levels of plasma uric acid**

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Diseases associated with purine degradation > Gout**

    <!--list-separator-->

    -  Underexcretion of [uric acid]({{< relref "uric_acid" >}})

        -   The cause of the vast majority of [Gout]({{< relref "gout" >}}) cases

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Diseases associated with purine degradation > Gout**

    Characterized by high levels of [Uric acid]({{< relref "uric_acid" >}}) in the blood ([hyperuricemia]({{< relref "hyperuricemia" >}})) as a result of either **overproduction** or **underexcretion** of **uric acid**

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    Hypoxanthine is oxidized by [xanthine oxidase]({{< relref "xanthine_oxidase" >}}) to [Xanthine]({{< relref "xanthine" >}}) -> further oxidized by xanthine oxidase to [Uric acid]({{< relref "uric_acid" >}})

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296)**

    <!--list-separator-->

    -  Formation of [uric acid]({{< relref "uric_acid" >}})

        -   Steps:
            1.  An amino group is removed from [AMP]({{< relref "adenosine_monophosphate" >}}) to produce [IMP]({{< relref "inosine_monophosphate" >}}) by [AMP deaminase]({{< relref "amp_deaminase" >}})
                -   Alternatively, an amino group is removed from [adenosine]({{< relref "adenosine" >}}) -> produces [inosine]({{< relref "inosine" >}}) (hypoxanthine-ribose) by [adenosine deaminase]({{< relref "adenosine_deaminase" >}})
            2.  [IMP]({{< relref "inosine_monophosphate" >}}) and [GMP]({{< relref "guanosine_monophosphate" >}}) are converted into their nucleoside forms (inosine and guanosine) by the action of [5'-nucleotidase]({{< relref "5_nucleotidase" >}})
            3.  [Purine nucleoside phosphorylase]({{< relref "purine_nucleoside_phosphorylase" >}}) converts [Inosine]({{< relref "inosine" >}}) and [guanosine]({{< relref "guanosine" >}}) into their respective purine bases, [hypoxanthine]({{< relref "hypoxanthine" >}}) and [guanine]({{< relref "guanine" >}})
            4.  [Guanine]({{< relref "guanine" >}}) is deaminated to form [xanthine]({{< relref "xanthine" >}})
            5.  Hypoxanthine is oxidized by [xanthine oxidase]({{< relref "xanthine_oxidase" >}}) to [Xanthine]({{< relref "xanthine" >}}) -> further oxidized by xanthine oxidase to [Uric acid]({{< relref "uric_acid" >}})

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Degradation of dietary nucleic acids in the small intestine**

    Most of the [Uric acid]({{< relref "uric_acid" >}}) enters the blood -> excreted in urine

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Degradation of dietary nucleic acids in the small intestine**

    They are instead converted to [uric acid]({{< relref "uric_acid" >}}) in intestinal mucosal cells

    ---


#### [Hyperuricemia]({{< relref "hyperuricemia" >}}) {#hyperuricemia--hyperuricemia-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    High levels of [Uric acid]({{< relref "uric_acid" >}}) in the blood

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
