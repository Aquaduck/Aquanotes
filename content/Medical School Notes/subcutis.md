+++
title = "Subcutis"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}}) {#know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot--know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Overview of Basic Skin > 3 layers**

    <!--list-separator-->

    -  [Subcutis]({{< relref "subcutis" >}})

        -   Mostly fat with some connective tissue
        -   Provides cushioning and mobility
        -   Energy storage (fat)

        {{< figure src="/ox-hugo/_20210921_162537screenshot.png" caption="Figure 1: Subcutis directly under reticular dermis. Made mostly of adipose cells (white bubbles)" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
