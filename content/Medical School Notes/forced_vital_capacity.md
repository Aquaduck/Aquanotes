+++
title = "Forced vital capacity"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Can be a way to measure [VC]({{< relref "vital_capacity" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Tiffeneau-Pinelli index]({{< relref "tiffeneau_pinelli_index" >}}) {#tiffeneau-pinelli-index--tiffeneau-pinelli-index-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Ratio of [FEV1]({{< relref "fev1" >}}) to [FVC]({{< relref "forced_vital_capacity" >}}) as a percentage

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Vital capacity**

    <!--list-separator-->

    -  [Forced vital capacity]({{< relref "forced_vital_capacity" >}}) (FVC)

        -   Maximum volume of air that can be **forcefully expired** after maximal inspiration


### Unlinked references {#unlinked-references}

[Show unlinked references]
