+++
title = "List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## [Triacylglycerol]({{< relref "triacylglycerol" >}}) {#triacylglycerol--triacylglycerol-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   The **insolubility of triacylglycerols** poses a significant problem for both:
    -   **The absorption and utilization of dietary lipids**
    -   **The mobilization of triacylglycerls stored in adipocytes**


#### Pathway of triacylglycerol breakdown {#pathway-of-triacylglycerol-breakdown}

-   Absorption of dietary fats is dependent on the **presence of amphipathic bile salts** -> creation of small _micelles_ of hydrophobic fat
-   Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by _enterocytes_ (cells of the intestinal lining)
    -   Digested triacylglycerol (except for short- and medium-chain fatty acids) are **resynthesized in the ER** along with phospholipids and cholesterol esters
        -   Short- and medium-chain fatty acids **diffuse directly across the enterocyte into portal capillaries**
-   Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
    -   Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB-48
        -   Molecules of apoCII and apoCIII are also integrated
-   The formed chylomicrons are then **exocytosed into the lymphatics** -> **enter venous circulation**
-   Chylomicrons indicate a fed state -> [insulin]({{< relref "insulin" >}}) regulates uptake of [fat]({{< relref "dietary_lipid" >}}) into tissues
    -   [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**
        -   Done through its effects on gene transcription
    -   [Insulin]({{< relref "insulin" >}}) also **increases glucose uptake in skeletal muscle and adipose tissue**
-   LPL will cleave the triacylglycerol to **either** _three [fatty acids]({{< relref "fatty_acid" >}}) and glycerol_ or _two fatty acids and a [monoacylglycerol]({{< relref "monoacylglycerol" >}})_
    -   Fatty acids and monoacylglycerol will diffuse freely into the endothelial cell -> [glycerol]({{< relref "glycerol" >}}) taken up by the [liver]({{< relref "liver" >}})
-   [Fatty acids]({{< relref "fatty_acid" >}}) will bind to various _fatty acid binding proteins_ -> able to traverse the cell and enter the tissue beneath
    -   In muscle, fatty acids will be oxidized for energy and/or stored
    -   In lactating [mammary glands]({{< relref "mammary_gland" >}}), fatty acids + monoacylglycerol become components of [milk]({{< relref "milk" >}})


#### Mobilization of triacylglycerol stored in [adipocytes]({{< relref "adipose_cell" >}}) {#mobilization-of-triacylglycerol-stored-in-adipocytes--adipose-cell-dot-md}

-   **Triacylglycerol energy reserve is stored in adipocytes in the fed state**
    -   **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**
        -   Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})
        -   Therefore, **cAMP-mediated activation of PKA is required**
-   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
    -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
-   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
-   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol
-   **Ultimately, 3 fatty acids are mobilized/liberated and glycerol is formed**
    -   Fatty acids exit adipose tissue via diffusoin and bind reversibly to plasma [albumin]({{< relref "albumin" >}})
    -   Glycerol taken up by the liver


#### Mobilization of stored triacylglycerol in [muscle]({{< relref "muscle" >}}) {#mobilization-of-stored-triacylglycerol-in-muscle--muscle-dot-md}

-   Skeletal muscle obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose
    -   These fat stores can be mobilized in the muscle cell via skeletal muscle contractions or in a stressed situation by [Epinephrine]({{< relref "epinephrine" >}})

{{< figure src="/ox-hugo/_20211024_191932screenshot.png" caption="Figure 1: Fatty acid mobilization mediated via skeletal muscle contraction vs. epinephrine" width="500" >}}


#### [Fatty acid catabolism]({{< relref "lipolysis" >}}) {#fatty-acid-catabolism--lipolysis-dot-md}

-   Can be divided into **3 stages**:
    1.  _Investment_ to prepare them for ->
    2.  _Transport_ into the mitochondrial matrix wherein they will undergo ->
    3.  _Oxidation_

{{< figure src="/ox-hugo/_20211024_192624screenshot.png" width="700" >}}

<!--list-separator-->

-  Investment phase

    -   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
        -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
        -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**

<!--list-separator-->

-  Transport phase

    -   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_
    -   Fatty aycl-carnitine ester is formed by the action of _carnitine acyltransferase I_ once inside the inner membrane space
        -   Becomes a substrate for _carnitine acyltransferase II_ -> catalyzes transfer of acyl to CoA-SH in matrix -> release carnitine for a second round of fatty acid transport
    -   **Rate-limiting and commits the fatty acyl-CoA to oxidation in the matrix**

    {{< figure src="/ox-hugo/_20211024_193058screenshot.png" width="700" >}}

<!--list-separator-->

-  Oxidation phase

    -   Occurs in three stages:
        1.  Beta-oxidation
        2.  Oxidation of acetyl-CoA in TCA cycle -> 3 NADH, 1 FADH<sub>2</sub>, 1 GTP
        3.  ETC -> 9 ATP
            -   Electrons from 3 NADH and 1 FADH<sub>2</sub>
    -   **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**
        -   Increased [propionic acid] = _biotin deficiency_
        -   Increased [methylmalonic acid] = _vitamin B12 deficiency_
        -   Leads to **mental deficits**


## Gastrointestinal [lipase]({{< relref "lipase" >}}) {#gastrointestinal-lipase--lipase-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by _enterocytes_ (cells of the intestinal lining)


## [Chylomicrons]({{< relref "chylomicron" >}}) {#chylomicrons--chylomicron-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   The **carriers of [dietary lipids]({{< relref "dietary_lipid" >}}) in the [blood]({{< relref "blood" >}})**
-   Molecules of [apo-CII]({{< relref "apolipoprotein_cii" >}}) and [apo-CIII]({{< relref "apolipoprotein_ciii" >}}) are also integrated into the chylomicron "membrane"
-   Formed chylomicrons are **exocytosed into the lymphatics** -> **enter venous circulation**
-   Chylomicrons **indicate a fed state**
-   [Skeletal muscle]({{< relref "skeletal_muscle" >}}) obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose


## Apolipoproteins {#apolipoproteins}


## apoB48 & apoCII {#apob48-and-apocii}


### [ApoB48]({{< relref "apolipoprotein_b_48" >}}) {#apob48--apolipoprotein-b-48-dot-md}


#### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   Newly synthesized triacylglycerol + cholesteryl esters are very hydrophobic -> aggregate in aqueous cytoplasm
    -   Must be **packaged** as components of lipid droplets ([lipoproteins]({{< relref "lipoprotein" >}})) surrounded by a thin unilamellar, amphipathic layer composed of phospholipids, unesterified cholesterol and a molecule of apoB48
-   **The structural protein of [chylomicrons]({{< relref "chylomicron" >}})**


#### From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}

-   Mediates the secretion of [Chylomicron]({{< relref "chylomicron" >}}) particles that originate from the intestine into the lymphatics
-   A component of [chylomicrons]({{< relref "chylomicron" >}})


### [ApoCII]({{< relref "apocii" >}}) {#apocii--apocii-dot-md}


#### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   ApoCII plays a **crucial role in delivering dietary fat to extrahepatic tissues**
-   ApoCII anchors the chylomicron to the LPL molecules expressed by capillary endothelial cells


#### From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}

-   Cofactor for [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})
-   A component of [chylomicrons]({{< relref "chylomicron" >}}), [VLDL]({{< relref "very_low_density_lipoprotein" >}}), and [HDL]({{< relref "high_density_lipoprotein" >}})


## [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}}) {#lipoprotein-lipase--lipoprotein-lipase-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**
    -   Done through its effects on gene transcription
-   LPL will cleave the triacylglycerol to **either** _three fatty acids and glycerol_ or _two fatty acids and a monoacylglycerol_
-   The LPL isozyme of adipose tissue has the **largest K<sub>m</sub> -> [adipose tissue's]({{< relref "adipose_cell" >}}) uptake of [dietary fat]({{< relref "dietary_lipid" >}}) is a last resort**
    -   **Adipose tissue stores fat** -> least likely to utilize it
    -   **LPL is not expressed by the [liver]({{< relref "liver" >}}) or the [brain]({{< relref "brain" >}})** -> NO dietary fat reaches either of those tissues
-   Tethering the LPL to the cell via a long polysaccharide chain consisting of _heparan sulfates_ -> **significantly improves the likelihood of an [apoCII]({{< relref "apocii" >}}):LPL interaction within the capillary**


## [Perilipin]({{< relref "perilipin" >}}) {#perilipin--perilipin-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
    -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
-   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of 2,3-DAG**


## [Hormone-sensitive lipase]({{< relref "hormone_sensitive_lipase" >}}) {#hormone-sensitive-lipase--hormone-sensitive-lipase-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
-   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol


## [Fatty acyl-CoA]({{< relref "fatty_acyl_coa" >}}) {#fatty-acyl-coa--fatty-acyl-coa-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   In the **investment phase of fatty acid oxidation**
-   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
    -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
    -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**


## [Fatty acyl-CoA synthetase]({{< relref "fatty_acyl_coa_synthetase" >}}) {#fatty-acyl-coa-synthetase--fatty-acyl-coa-synthetase-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   In the **investment phase of fatty acid oxidation**
-   Fatty acids are first activated by _acyl-CoA synthetases_ in the outer mitochondrial membrane
    -   Catalyze the formation of a thioester linkage between the fatty acid and CoA-SH to form a fatty acyl-CoA
    -   This is coupled to the cleavage of ATP -> AMP + PP<sub>i</sub> -> **two ATP molecules required to drive this reaction**


## [Carnitine]({{< relref "carnitine" >}}) {#carnitine--carnitine-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_


## [Albumin]({{< relref "albumin" >}})-bound fatty acids {#albumin--albumin-dot-md--bound-fatty-acids}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   **Two major purposes:**
    1.  Half-life of albumin-bound fatty acid is **~3 min -> taken up and oxidized by rapidly metabolizing tissues due to fasting state**
        -   E.g. [muscle]({{< relref "muscle" >}}) and [kidney]({{< relref "kidney" >}})
    2.  Fasting state -> **fatty acids enter the [liver]({{< relref "liver" >}}) as the energy source needed to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})**
        -   **Released [glycerol]({{< relref "glycerol" >}}) is used primarly by the [liver]({{< relref "liver" >}}) as a [gluconeogenic]({{< relref "gluconeogenesis" >}}) substrate**


## [Beta-oxidation]({{< relref "β_oxidation" >}}) {#beta-oxidation--β-oxidation-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   First stage of oxidation of [fatty acids]({{< relref "fatty_acid" >}})
-   Oxidation of the fatty acid at the beta-carbon, followed by a hydration, an oxidation, and a thiolysis
-   Removes carbons 1 and 2 as a molecule of acetyl-CoA
-   One round of beta-oxidation -> 1 molecule of NADH and FADH<sub>2</sub>
-   Odd-chain fatty acids produce acetyl-CoA **and** propionyl-CoA
-   **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**
    -   Increased [propionic acid] = _biotin deficiency_
    -   Increased [methylmalonic acid] = _vitamin B12 deficiency_
    -   Leads to **mental deficits**


## [Carnitine]({{< relref "carnitine" >}}) {#carnitine--carnitine-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   Passage of fatty acyl-CoA across the inner mitochondrial membrane requires _carnitine_


## Carnitine acyltransferase I (CAT-1) and CAT-II {#carnitine-acyltransferase-i--cat-1--and-cat-ii}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}


#### [CAT-1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) {#cat-1--carnitine-palmitoyl-acyltransferase-1-dot-md}

-   [Fatty acyl-carnitine ester]({{< relref "fatty_acyl_carnitine_ester" >}}) is formed by the action of _carnitine acyltransferase I_ once inside the inner membrane space


#### [CAT-2]({{< relref "carnitine_palmitoyl_acyltransferase_2" >}}) {#cat-2--carnitine-palmitoyl-acyltransferase-2-dot-md}

-   [Fatty acyl-carnitine ester]({{< relref "fatty_acyl_carnitine_ester" >}}) becomes a substrate for _carnitine acyltransferase II_ -> catalyzes transfer of acyl to CoA-SH in matrix -> release [carnitine]({{< relref "carnitine" >}}) for a second round of fatty acid transport


## FAD- and NAD<sup>+</sup>-linked dehydrogenases {#fad-and-nad-linked-dehydrogenases}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   Recognize very long-, long-, middle-, or small-chain fatty acids as substrates -> carry out oxidation reactions in [beta-oxidation]({{< relref "β_oxidation" >}})


## [Propionyl-CoA]({{< relref "propionyl_coa" >}}) {#propionyl-coa--propionyl-coa-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   Produced via the beta-oxidation of odd-chain fatty acids
-   Undergoes an ATP- and biotin-dependent carboxylation reaction to form [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) -> converted to L-methylmalonyl-CoA


## [Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}}) {#propionyl-coa-carboxylase--propionyl-coa-carboxylase-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   Catalyzes the formation of D-methylmalonyl-CoA from [Propionyl-CoA]({{< relref "propionyl_coa" >}})


## [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) {#l-methylmalonyl-coa--l-methylmalonyl-coa-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   Propionyl-CoA undergoes an ATP- and biotin-dependent carboxylation reaction to form D-methylmalonyl-CoA -> converted to L-methylmalonyl-CoA
-   L-methylmalonyl-CoA converted to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via [B12]({{< relref "cobalamin" >}}) dependent isomerization catalyzed by [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})
    -   Succinyl-CoA then oxidized in TCA cycle


## [Methylmalonic acid]({{< relref "methylmalonic_acid" >}}) {#methylmalonic-acid--methylmalonic-acid-dot-md}


### From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}}) {#from-fat-metabolism-in-muscle-and-adipose-tissue--fat-metabolism-in-muscle-adipose-tissue-dot-md}

-   Increased [methylmalonic acid] is indicative of a [vitamin B12]({{< relref "cobalamin" >}}) deficiency


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-25 Mon] </span></span> > Muscle Intermediary Metabolism II > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the features and functions of the following:]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}})

        <!--list-separator-->

        -  Triacylglycerol

        <!--list-separator-->

        -  Gastrointestinal lipase

        <!--list-separator-->

        -  Chylomicrons

        <!--list-separator-->

        -  Apolipoproteins

        <!--list-separator-->

        -  apoB48 & apoCII

        <!--list-separator-->

        -  lipoprotein lipase

        <!--list-separator-->

        -  Perilipin

        <!--list-separator-->

        -  Hormone-sensitive lipase

        <!--list-separator-->

        -  Fatty acyl-CoA

        <!--list-separator-->

        -  Fatty acyl-CoA synthetase

        <!--list-separator-->

        -  Carnitine

        <!--list-separator-->

        -  Albumin-bound fatty acids

        <!--list-separator-->

        -  Beta-oxidation

        <!--list-separator-->

        -  Carnitine

        <!--list-separator-->

        -  Carnitine acyltransferase I (CAT-1) and CAT-II

        <!--list-separator-->

        -  FAD- and NAD<sup>+</sup>-linked dehydrogenases

        <!--list-separator-->

        -  Propionyl-CoA

        <!--list-separator-->

        -  Propionyl-CoA carboxylase

        <!--list-separator-->

        -  L-methymalonyl-CoA

        <!--list-separator-->

        -  Methylmalonic acid


### Unlinked references {#unlinked-references}

[Show unlinked references]
