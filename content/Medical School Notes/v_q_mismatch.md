+++
title = "V/Q mismatch"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   An imbalance in the [V/Q ratio]({{< relref "v_q_ratio" >}})
-   Characterized by an increased [A-a gradient]({{< relref "a_a_gradient" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Define the difference between V/Q for a shunt vs. V/Q for a dead space.]({{< relref "define_the_difference_between_v_q_for_a_shunt_vs_v_q_for_a_dead_space" >}}) {#define-the-difference-between-v-q-for-a-shunt-vs-dot-v-q-for-a-dead-space-dot--define-the-difference-between-v-q-for-a-shunt-vs-v-q-for-a-dead-space-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [V/Q mismatch]({{< relref "v_q_mismatch" >}})

        -   An imbalance between total lung ventilation (airflow; _V_) and total lung perfusion (blood flow; _Q_)
        -   Characterized by an increased A-a gradient
        -   **The most common cause of [hypoxemia]({{< relref "hypoxemia" >}})**

        <!--list-separator-->

        -  Increased V/Q ratio

            -   **Indicative of [dead space]({{< relref "physiological_dead_space" >}})**: Volume of inspired air that does not participate in gas exchange during [ventilation]({{< relref "ventilation" >}})
                -   [Anatomic dead space]({{< relref "anatomic_dead_space" >}}): the volume of air in the [conducting zone]({{< relref "conducting_zone" >}}) (e.g. mouth, trachea)
                -   [Alveolar dead space]({{< relref "alveolar_dead_space" >}}): the sum of hte volumes of alveoli that do not participate in gas exchange (mainly apex of lungs)
                    -   These alveoli are ventilated **but not perfused**
            -   Causes:
                -   Blood flow obstruction (e.g. pulmonary embolism)
                -   Exercise
                    -   Cardiac output increases -> vasodilation of apical capillaries


### Unlinked references {#unlinked-references}

[Show unlinked references]
