+++
title = "Cancer"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Telomerase and aging**

    [Cancer]({{< relref "cancer" >}}) is characterized by uncontrolled cell division of abnormal cells

    ---


#### [Describe the etiology of pleuritis.]({{< relref "describe_the_etiology_of_pleuritis" >}}) {#describe-the-etiology-of-pleuritis-dot--describe-the-etiology-of-pleuritis-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Pleuritis**

    Typically a result of infection, [cancer]({{< relref "cancer" >}}), and [congestive heart failure]({{< relref "congestive_heart_failure" >}})

    ---


#### [Breast cancer]({{< relref "breast_cancer" >}}) {#breast-cancer--breast-cancer-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Cancer]({{< relref "cancer" >}}) of the [Breast]({{< relref "breast" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
