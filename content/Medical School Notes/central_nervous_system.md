+++
title = "Central nervous system"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Tau protein]({{< relref "tau_protein" >}}) {#tau-protein--tau-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Abundant in neurons of the [CNS]({{< relref "central_nervous_system" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
