+++
title = "Cdc25"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Removes phosphates added by [Wee1]({{< relref "wee1" >}}) to reactivate the [cyclin-CDK complex]({{< relref "cyclin_cdk_complex" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Dephosphorylation Activates M-Cdk at the Onset of Mitosis**

    [Cdc25]({{< relref "cdc25" >}}) can be activated (in part) by M-Cdk

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Dephosphorylation Activates M-Cdk at the Onset of Mitosis**

    [Cdc25]({{< relref "cdc25" >}}) is phosphorylated -> **activated** -> **removes the inhibitory phosphates that restrain M-Cdk**

    ---

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop > Activation/inactivation mechanisms for CDKs > Inactivation of CDKs**

    Removal of these phosphates by [Cdc25]({{< relref "cdc25" >}}) activates the cyclin-CDK complex

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
