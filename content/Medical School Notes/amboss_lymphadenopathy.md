+++
title = "Amboss - Lymphadenopathy"
author = ["Arif Ahsan"]
date = 2021-08-23T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Summary {#summary}

-   Enlargement of [lymph nodes]({{< relref "lymph_node" >}})
-   Most ocmmonly occurs during benign, inflammatory processes


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
