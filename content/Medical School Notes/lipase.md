+++
title = "Lipase"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   An enzyme responsible for the breakdown of fatty acids


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  Gastrointestinal [lipase]({{< relref "lipase" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Triacylglycerol in micelles is then cleaved by lipases into mono- and di-acylglycerol, free fatty acids and glycerol -> absorbed by _enterocytes_ (cells of the intestinal lining)


#### [Hormone-sensitive lipase]({{< relref "hormone_sensitive_lipase" >}}) {#hormone-sensitive-lipase--hormone-sensitive-lipase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [lipase]({{< relref "lipase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
