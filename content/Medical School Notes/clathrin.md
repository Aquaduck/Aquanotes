+++
title = "Clathrin"
author = ["Arif Ahsan"]
date = 2021-09-02T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the process of neuromuscular transmission.]({{< relref "describe_the_process_of_neuromuscular_transmission" >}}) {#describe-the-process-of-neuromuscular-transmission-dot--describe-the-process-of-neuromuscular-transmission-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Conduction of a nerve impulse across a myoneural junction (p. 116)**

    Membranes of the emptied synaptic vesicles are recycled via [clathrin]({{< relref "clathrin" >}})-coated endocytic vesicles

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Coated vesicles > Clathrin-coated vesicles > Structure**

    _[Clathrin]({{< relref "clathrin" >}})_: consists of three large and three small polypeptine chains that form a three-legged structure (_triskelion_)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
