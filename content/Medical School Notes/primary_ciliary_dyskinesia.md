+++
title = "Primary ciliary dyskinesia"
author = ["Arif Ahsan"]
date = 2021-09-15T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## From [Kartagener's syndrome: A case series]({{< relref "kartagener_s_syndrome_a_case_series" >}}) {#from-kartagener-s-syndrome-a-case-series--kartagener-s-syndrome-a-case-series-dot-md}


### Discussion {#discussion}

-   PCD is a phenotypically and genetically heterogenous condition
-   The primary defect is in the **ultrastructure or function of cilia**
    -   Involve the outer [dynein]({{< relref "dynein" >}}) arms, inner dynein arms, or both


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kartagener syndrome]({{< relref "kartagener_syndrome" >}}) {#kartagener-syndrome--kartagener-syndrome-dot-md}

<!--list-separator-->

-  **🔖 From Kartagener's syndrome: A case series > Introduction**

    **Kartagener's syndrome (KS)** is a subset of a larger group of ciliary motility disorders called [primary ciliary dyskinesias]({{< relref "primary_ciliary_dyskinesia" >}}) (PCDs)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
