+++
title = "Osmoreceptor"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Specialized cells within the [hypothalamus]({{< relref "hypothalamus" >}}) that are particularly sensitive to the concentration of sodium ions and other solutes
-   Signal the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) to release [ADH]({{< relref "antidiuretic_hormone" >}}) in response to high blood osmolarity


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Antidiuretic Hormone (ADH) (p.746)**

    As blood osmolarity decreases -> hypothalamic [osmoreceptors]({{< relref "osmoreceptor" >}}) sense change -> decrease secretion of ADH -> less water reabsorbed from urine

    ---

<!--list-separator-->

-  **🔖 Antidiuretic Hormone (ADH) (p.746)**

    In response to **high blood osmolarity** -> [osmoreceptors]({{< relref "osmoreceptor" >}}) signal the [posterior pituitary]({{< relref "posterior_pituitary" >}}) to release ADH

    ---

<!--list-separator-->

-  **🔖 Antidiuretic Hormone (ADH) (p.746)**

    Blood osmolarity is constantly monitored by [osmoreceptors]({{< relref "osmoreceptor" >}}) - specialized cells within the hypothalamus that are particularly sensitive to the concentration of sodium ions and other solutes

    ---


#### [Antidiuretic hormone]({{< relref "antidiuretic_hormone" >}}) {#antidiuretic-hormone--antidiuretic-hormone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Secreted by the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) in response to [osmoreceptors]({{< relref "osmoreceptor" >}}) in the [Hypothalamus]({{< relref "hypothalamus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
