+++
title = "GLUT-5"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Overview of the most important glucose transporters]({{< relref "overview_of_the_most_important_glucose_transporters" >}}) {#overview-of-the-most-important-glucose-transporters--overview-of-the-most-important-glucose-transporters-dot-md}

|                                  |                              |                      |    |
|----------------------------------|------------------------------|----------------------|----|
| [GLUT5]({{< relref "glut_5" >}}) | Small intestinal enterocytes | Fructose transporter | No |

---


### Unlinked references {#unlinked-references}

[Show unlinked references]
