+++
title = "Terminal cisternae"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Enlarged areas of [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) surrounding [t tubules]({{< relref "transverse_tubule" >}})
-   They are the ends of [L tubules]({{< relref "l_tubule" >}})
-   Store [Calcium]({{< relref "calcium" >}}) and release it when an action potential courses down the t tubules


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Terminal cisternae]({{< relref "terminal_cisternae" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
