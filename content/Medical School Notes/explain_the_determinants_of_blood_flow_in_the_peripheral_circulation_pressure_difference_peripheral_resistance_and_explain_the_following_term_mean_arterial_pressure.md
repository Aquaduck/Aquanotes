+++
title = "Explain the determinants of blood flow in the peripheral circulation (pressure difference, peripheral resistance) and explain the following term: mean arterial pressure."
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-18 Mon] </span></span> > Regional and Peripheral Circulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Explain the determinants of blood flow in the peripheral circulation (pressure difference, peripheral resistance) and explain the following term: mean arterial pressure.]({{< relref "explain_the_determinants_of_blood_flow_in_the_peripheral_circulation_pressure_difference_peripheral_resistance_and_explain_the_following_term_mean_arterial_pressure" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
