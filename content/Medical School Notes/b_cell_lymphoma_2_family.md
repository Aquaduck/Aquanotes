+++
title = "B-cell lymphoma 2 family"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Regulate the [intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}})


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Smac, a Mitochondrial Protein that Promotes Cytochrome c–Dependent Caspase Activation by Eliminating IAP Inhibition]({{< relref "smac_a_mitochondrial_protein_that_promotes_cytochrome_c_dependent_caspase_activation_by_eliminating_iap_inhibition" >}}) {#smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition--smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition-dot-md}

<!--list-separator-->

-  **🔖 Discussion > Regulation of SMAC**

    This process is likely to be controlled by the [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}}) of proteins

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}})

        <!--list-separator-->

        -  Bcl2 proteins regulate the [intrinsic pathway]({{< relref "intrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1026)

            -   Bcl2 proteins are a major class of intracellular regulators of the intrinsic pathway of apoptosis
                -   Controls the release of [cytochrome c]({{< relref "cytochrome_c" >}}) and other intermembrane mitochondrial proteins into the cytosol
                -   Some are _pro-apoptotic_ -> promote apoptosis by enhancing release
                -   Some are _anti-apoptotic_ -> inhibit apoptosis by blocking release
            -   Pro- and anti-apoptotic Bcl2 proteins can bind to each other in various combinations to form heterodimers -> can **inhibit each other's function**
                -   Largely determines whether a mammalian cell lives or dies by the intrinsic pathway of apoptosis

            <!--list-separator-->

            -  [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}})

                -   **Pro-apoptotic** effector Bcl2 family proteins
                -   **At least one of these is required** for the intrinsic pathway of apoptosis to operate

            <!--list-separator-->

            -  [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) and [BclX<sub>L</sub>]({{< relref "bclx_l" >}})

                -   **Anti-apoptotic**
                -   Bind to pro-apoptotic Bcl2 family proteins -> inhibit apoptosis

            <!--list-separator-->

            -  [BH3-only proteins]({{< relref "bh3_only_protein" >}})

                -   Cell either produces or activates BH3-only proteins in response to an apoptotic stimulus
                -   Provide the crucial link between apoptotic stimuli and the intrinsic pathway of apoptosis
                    -   Different stimuli activates different BH3-only proteins
                -   Thought to promote apoptotis mainly by inhibiting anti-apoptotic Bcl2 family proteins
                -   BH3 domain binds to a long hydrophobic groove on anti-apoptotic Bcl2 family proteins -> neutralizes their activity
                    -   Enables the aggregation of [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}}) on the surface of mitochondria -> triggers release of intermembrane mitochondrial proteins -> induction of apoptosis
                -   Some bind to Bax and Bak directly to stimulate their aggregation

                <!--list-separator-->

                -  [Bid]({{< relref "bid" >}})

                    -   Normally inactive
                    -   When [death receptors]({{< relref "death_receptor" >}}) activate the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}), [Caspase-8]({{< relref "caspase_8" >}}) cleaves Bid -> activates Bid -> translocates to outer mitochondrial membrane -> inhibits anti-apoptotic Bcl2 family proteins -> **amplification of death signal**


#### [BH3-only protein]({{< relref "bh3_only_protein" >}}) {#bh3-only-protein--bh3-only-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The largest subclass of [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}}) proteins

    ---


#### [BclX<sub>L</sub>]({{< relref "bclx_l" >}}) {#bclx--bclx-l-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A member of the [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}}) of proteins

    ---


#### [Bax]({{< relref "bax" >}}) {#bax--bax-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    One of the main pro-apoptotic effector [Bcl2]({{< relref "b_cell_lymphoma_2_family" >}}) family proteins in mammalian cells

    ---


#### [Bak]({{< relref "bak" >}}) {#bak--bak-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    One of the main pro-apoptotic effector [Bcl2]({{< relref "b_cell_lymphoma_2_family" >}}) family proteins

    ---


#### [B-cell lymphoma 2]({{< relref "b_cell_lymphoma_2" >}}) {#b-cell-lymphoma-2--b-cell-lymphoma-2-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A member of the [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
