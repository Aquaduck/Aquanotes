+++
title = "Tau protein"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   An insoluble [microtubule]({{< relref "microtubule" >}})-associated protein that maintains the stability of microtubules in [axons]({{< relref "axon" >}})
-   Abundant in neurons of the [CNS]({{< relref "central_nervous_system" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
