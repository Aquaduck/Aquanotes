+++
title = "MAP2"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}}) {#from-molecular-biology-of-the-cell-sixth-edition--molecular-biology-of-the-cell-sixth-edition-dot-md}


### Microtubule-binding proteins modulate filament dynamics and organization {#microtubule-binding-proteins-modulate-filament-dynamics-and-organization}

-   Due to its long projecting domain, MAP2 **forms bundles of stable microtubules** that are kept widely spaced and is thus confined to neuron cell bodies and its [dendrites]({{< relref "dendrite" >}})


## Concept links {#concept-links}

-   A type of [microtubule associated protein]({{< relref "microtubule_associated_protein" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [TNF alpha and the TNF receptor superfamily: structure-function relationship(s)]({{< relref "tnf_alpha_and_the_tnf_receptor_superfamily_structure_function_relationship_s" >}}) {#tnf-alpha-and-the-tnf-receptor-superfamily-structure-function-relationship--s----tnf-alpha-and-the-tnf-receptor-superfamily-structure-function-relationship-s-dot-md}

<!--list-separator-->

-  **🔖 TNF-α mechanism(s) of action**

    [MAP2]({{< relref "map2" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
