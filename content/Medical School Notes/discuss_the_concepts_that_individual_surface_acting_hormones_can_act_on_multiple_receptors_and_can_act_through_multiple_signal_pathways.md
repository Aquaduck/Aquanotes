+++
title = "Discuss the concepts that individual surface-acting hormones can act on multiple receptors; and can act through multiple signal pathways."
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

[Death of Monotheism]({{< relref "cell_signalling_iii_underlying_design_features" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-31 Tue] </span></span> > Cell Signaling III > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Discuss the concepts that individual surface-acting hormones can act on multiple receptors; and can act through multiple signal pathways.]({{< relref "discuss_the_concepts_that_individual_surface_acting_hormones_can_act_on_multiple_receptors_and_can_act_through_multiple_signal_pathways" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
