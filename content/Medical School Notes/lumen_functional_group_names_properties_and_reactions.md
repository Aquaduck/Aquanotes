+++
title = "Lumen - Functional Group Names, Properties, and Reactions"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "lumen"]
draft = false
+++

## Overview {#overview}

-   Functional groups refer to specific atoms bonded in a certain arrangement that give ac ompound certain physical and chemical properties


## Functional Groups {#functional-groups}


### [Alcohols]({{< relref "alcohol" >}}) {#alcohols--alcohol-dot-md}


#### Structure and Physical Properties {#structure-and-physical-properties}

-   Bent shape
    -   Due to electron repulsion and steric bulk of substituents on central oxygen atom
-   **Polar**
    -   High electronegativity of oxygen vs. carbon -> shortening and strengthening of -OH bond
-   Can form hydrogen bonds
    -   Boiling point higher than those of parent molecule because of this
-   Often undergo deprotonation in presence of strong base
    -   Results in formation of alkoxide salt and water molecule
-   **Not considered good leaving groups**
    -   Participation in nucleophilic substitution rxns instigated by _protonation of oxygen atom_ -> formation of -OH<sub>2</sub>, a better leaving group


### [Ethers]({{< relref "ether" >}}) {#ethers--ether-dot-md}


#### Structure of Ethers {#structure-of-ethers}

-   Oxygen atom connected to two alkyl or aryl groups: \\(R-O-R\\)
-   104.5<sup>o</sup> bond angles
-   Oxygen of ether more electronegative than carbons
    -   **alpha hydrogens more acidic than in regular hydrocarbons**


#### Nomenclature of Ethers {#nomenclature-of-ethers}

-   Most common way is to write two alkyl groups in alphabetical order followed by _ether_
-   Formal IUPAC way: _short alkyl chain_-_oxy_-_long alkyl chain_
-   In cyclic ethers, the stem of the compound is known as an _oxacycloalkane_


#### Properties of Ethers {#properties-of-ethers}

-   Ethers are **nonpolar**
    -   Because of alkyl group on either side
-   Bulky alkyl group prevents hydrogen bonding to oxygen
    -   Low boiling points compared to alcohols of similar molecular weight
    -   As alkyl chain becomes longer, difference in boiling points becomes smaller
        -   Due to increased Van der Waals interactions as # of carbons (+ electrons) increases
-   Ethers are **more polar than alkanes**, but **less polar than esters, alcohols, and amides**


#### Reactions {#reactions}

-   **Ethers have relatively low chemical reactivity**
-   Resist undergoing hydrolysis
-   Can be **cleaved by acids -> formation of alkyl halide and an alcohol**


### Aldehydes and Ketones {#aldehydes-and-ketones}


#### [Ketones]({{< relref "ketone" >}}) {#ketones--ketone-dot-md}

-   Ketone carbon is sp2 hybridized -> **trigonal planar geometry**
    -   120<sup>o</sup> bond angles
-   **Polar**
    -   Can form hydrogen bonds
-   Not usually hydrogen bond donors -> often more voltaile than alcohols and carboxylic acids
-   Can participate in _keto-enol tautomerism_


#### [Aldehydes]({{< relref "aldehyde" >}}) {#aldehydes--aldehyde-dot-md}

-   sp2 hybridized -> **trigonal planar geometry**


#### Similarities between [aldehydes]({{< relref "aldehyde" >}}) and [ketones]({{< relref "ketone" >}}) {#similarities-between-aldehydes--aldehyde-dot-md--and-ketones--ketone-dot-md}

-   Both exist in equilibrium with their enol forms:

{{< figure src="/ox-hugo/_20210714_215454screenshot.png" width="600" >}}

-   Keto form predominates at equilibrium
-   Deprotonated enolate is a **strong nucleophile**


### [Carboxylic Acids]({{< relref "carboxylic_acid" >}}) {#carboxylic-acids--carboxylic-acid-dot-md}


#### Physical Properties {#physical-properties}

-   Act as **both hydrogen bond acceptors and donors**
    -   H bond acceptor <- carbonyl group
    -   H bond donor <- hydroxyl group
-   Tendency to "self-associate" -> exist in dimeric pairs when in nonpolar media
-   Resonance stabilized
    -   Increased acidity and higher boiling points than other acids
-   **Polar**
-   **Weak acids** -> do not fully dissociate in neutral aqueous solution


### [Esters]({{< relref "ester" >}}) {#esters--ester-dot-md}

-   Most naturally occurring fats and oils are the fatty acid esters of glycerol
-   Typically fragrant


#### Structure and Bonding {#structure-and-bonding}

-   Carbonyl center -> sp2 hybridization
    -   120<sup>o</sup>
-   Structurally flexible
    -   Rotation abou C-O-C bond lower energy barrier
-   Low polarity
-   (esters) Compared to [amides]({{< relref "amide" >}}):
    -   Less rigid/more structurally flexible
        -   Lower melting point
    -   More volatile
        -   Lower boiling point
-   Essentially non-acidic except in presence of very strong bases


#### Physical Properties and Characterization {#physical-properties-and-characterization}

-   Hydrogen bond acceptors
    -   **Cannot be donors**
-   Esters do not self-associate
    -   No hydrogens


#### Reactivity {#reactivity}

-   React with nucleophiles **at the carbonyl carbon**
    -   Weakly electrophilic, but attacked by **strong nucleophiles**
-   Electrophilicity can increase if protonated
-   C-H bonds adjacent to carbonyl are weakly acidic, but undergo deprotonation with strong bases
    -   Usually initiates condensation reactions


### [Amines]({{< relref "amine" >}}) {#amines--amine-dot-md}


#### Structure {#structure}

-   Basic nitrogen atom with lone pair of electrons bound to up to 3 substituents, the rest being hydrogens
    -   **1<sup>o</sup> amines**: One substituent
    -   **2<sup>o</sup> amines**: Two substituents
    -   **3<sup>o</sup> amines**: Three substituents
    -   Possible to have four substituents on the nitrogen, making it an ammonium cation with charged nitrogen center


#### Physical properties {#physical-properties}

-   **Can form hydrogen bonds**
-   Somewhat soluble in water
    -   Solubility decreases with increase in carbon atoms
-   _Aliphatic amine_: amine connected to alkyl chain
    -   Soluble in organic polar solvents
-   Aromatic amines donate lone electron pair to benzene ring -> decreased hydrgeon bonding
    -   Decreased solubility in water and higher boiling point


#### Acidity and Alkalinity {#acidity-and-alkalinity}

-   \\(NHRR'\\) and \\(NR'R''R'''\\) amines are chiral molecules
    -   Low barrier for inversion -> cannot be resolved optically
-   **Amines are bases**
    -   Basicity depends on electronic properties of substituents
        -   Alkyl groups _increase_ basicity
        -   Aryl groups _decrease_ basicity
        -   Aromatic rings _decrease_ basicity
            -   Because of delocalized lone pair
-   Solubility order of ammoniums: **1<sup>o</sup>** RNH<sub>3</sub><sup>+</sup> > **2<sup>o</sup>** R<sub>2</sub>NH<sub>2</sub><sup>​+</sup> > **3<sup>o</sup>** R<sub>3</sub>NH<sup>​+</sup> > Quaternary ammonium


#### Reactivity {#reactivity}

-   **Very reactive** due to basicity and nucleophilicity
-   Most 1<sup>o</sup> amines are good ligands -> form coordination complexes w/ metal ions
-   Imine formation:
    ![](/ox-hugo/_20210714_222815screenshot.png)


#### Applications of Amines {#applications-of-amines}

-   Many important biological molecules are amine-based e.g. neurotransmitters and amino acids


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
