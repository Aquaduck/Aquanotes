+++
title = "Describe the molecular defect that leads to CF."
author = ["Arif Ahsan"]
date = 2021-10-17T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cystic fibrosis in the year 2020: A disease with a new face]({{< relref "cystic_fibrosis_in_the_year_2020_a_disease_with_a_new_face" >}}) {#from-cystic-fibrosis-in-the-year-2020-a-disease-with-a-new-face--cystic-fibrosis-in-the-year-2020-a-disease-with-a-new-face-dot-md}


### [Cystic fibrosis]({{< relref "cystic_fibrosis" >}}) {#cystic-fibrosis--cystic-fibrosis-dot-md}

-   Cystic fibrosis is caused by mutations in the [cystic fibrosis transmembrane conductance regulator]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}})
-   [CFTR]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}}) is an anion channel that:
    -   Conducts conducting chloride and [bicarbonate]({{< relref "bicarbonate" >}}) at the apical membrane of different epithelia
    -   Regulates water and ion transport
    -   Maintains epithelial surface hydration
-   [Bicarbonate]({{< relref "bicarbonate" >}}) release in the airway is important for the proper unfolding of [mucins]({{< relref "mucin" >}}) and defending against bacteria
-   [Bicarbonate]({{< relref "bicarbonate" >}}) in the intestines is needed to buffer gastric acidity and enable the activation of pancreatic enzymes


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-19 Tue] </span></span> > Cystic Fibrosis Small Group > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Describe the molecular defect that leads to CF.]({{< relref "describe_the_molecular_defect_that_leads_to_cf" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
