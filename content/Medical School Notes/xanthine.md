+++
title = "Xanthine"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    Hypoxanthine is oxidized by [xanthine oxidase]({{< relref "xanthine_oxidase" >}}) to [Xanthine]({{< relref "xanthine" >}}) -> further oxidized by xanthine oxidase to [Uric acid]({{< relref "uric_acid" >}})

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    [Guanine]({{< relref "guanine" >}}) is deaminated to form [xanthine]({{< relref "xanthine" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
