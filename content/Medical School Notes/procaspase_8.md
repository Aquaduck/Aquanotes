+++
title = "Procaspase-8"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Zymogen of [Caspase-8]({{< relref "caspase_8" >}})
-   Composed of [FLICE]({{< relref "fadd_like_interleukin_1b_converting_enzyme" >}}), MACH, and Mch5
-   Recruited to the [DISC]({{< relref "death_inducing_signaling_complex" >}}) and proteolysed into mature Caspase-8


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
