+++
title = "Albumin"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Podocyte]({{< relref "podocyte" >}}) {#podocyte--podocyte-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Integrity of slit diaphragms of podocytes essential to prevent excessive leak of [albumin]({{< relref "albumin" >}})

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Plasma Proteins (p. 796)**

    <!--list-separator-->

    -  [Albumin]({{< relref "albumin" >}})

        -   **The most abundant plasma protein**
        -   Manufactured by the [Liver]({{< relref "liver" >}})
        -   Albumin functions as a binding protein -> **transports fatty acids and steroid hormones**
        -   **The most significant contributor to osmotic pressure of blood**
            -   Albumin's presence holds water inside blood vessels and draws water from the tissues across blood vessel walls and into the bloodstream
                -   Low serum albumin leads to [edema]({{< relref "edema" >}})


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  [Albumin]({{< relref "albumin" >}})-bound fatty acids

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   **Two major purposes:**
            1.  Half-life of albumin-bound fatty acid is **~3 min -> taken up and oxidized by rapidly metabolizing tissues due to fasting state**
                -   E.g. [muscle]({{< relref "muscle" >}}) and [kidney]({{< relref "kidney" >}})
            2.  Fasting state -> **fatty acids enter the [liver]({{< relref "liver" >}}) as the energy source needed to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})**
                -   **Released [glycerol]({{< relref "glycerol" >}}) is used primarly by the [liver]({{< relref "liver" >}}) as a [gluconeogenic]({{< relref "gluconeogenesis" >}}) substrate**

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Mobilization of triacylglycerol stored in adipocytes**

    Fatty acids exit adipose tissue via diffusoin and bind reversibly to plasma [albumin]({{< relref "albumin" >}})

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Glomerular filtration (p. 33)**

    **Integrity of slit diaphragms of podocytes essential to prevent excessive leak of [albumin]({{< relref "albumin" >}})**

    ---


#### [Explain how fatty acids released from adipocyte stores are utilized.]({{< relref "explain_how_fatty_acids_released_from_adipocyte_stores_are_utilized" >}}) {#explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot--explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > How fatty acids released from adipocyte stores are utilized**

    Fatty acids exit adipose tissue via diffusion and bind reversibly to plasma [albumin]({{< relref "albumin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
