+++
title = "Molecular analysis and intestinal expression of SAR1 genes and proteins in Anderson's disease (Chylomicron retention disease)"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Chylomicron retention disease]({{< relref "chylomicron_retention_disease" >}}) {#chylomicron-retention-disease--chylomicron-retention-disease-dot-md}

<!--list-separator-->

-  From [Molecular analysis and intestinal expression of SAR1 genes and proteins in Anderson's disease (Chylomicron retention disease)]({{< relref "molecular_analysis_and_intestinal_expression_of_sar1_genes_and_proteins_in_anderson_s_disease_chylomicron_retention_disease" >}})

    -   Results from a mutation in the [SAR1B gene]({{< relref "sar1b_gene" >}}) which encodes the [SAR1B protein]({{< relref "sar1b_protein" >}})
        -   A small GTPase involved [COPII]({{< relref "cop_ii" >}})-dependent transport of proteins from the [ER]({{< relref "endoplasmic_reticulum" >}}) to the [Golgi]({{< relref "golgi_apparatus" >}})
            -   Sar1/COPII protein complex required for fusion of the specific chylomicron transport vesicle _Pre-chylomicron transport vesicle (PCTV)_ with the Golgi
            -   The mutation here prevents this vesicle from fusing with the Golgi, resulting in the chylomicrons being sent back to the ER and collecting there


### Unlinked references {#unlinked-references}

[Show unlinked references]
