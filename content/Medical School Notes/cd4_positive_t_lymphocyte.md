+++
title = "CD4-positive T lymphocyte"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Several effector actions:
    1.  Assist activated [B lymphocyte]({{< relref "b_lymphocyte" >}})s
    2.  Enhance [macrophages]({{< relref "macrophage" >}})
    3.  Regulate the immune reaction


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Germinal center]({{< relref "germinal_center" >}}) {#germinal-center--germinal-center-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Where [helper T cells]({{< relref "cd4_positive_t_lymphocyte" >}}) cause [B-cells]({{< relref "b_lymphocyte" >}}) to go through [somatic hypermutation]({{< relref "somatic_hypermutation" >}}) -> create more of a specific [antibody]({{< relref "immunoglobulin" >}})

    ---


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Activation and actions - T and B lymphocytes > T lymphocytes**

    The first three are performed by [CD4-positive (helper/suppressor) T-cells]({{< relref "cd4_positive_t_lymphocyte" >}}), and the last is done by [CD8-positive (cytotoxic) T-cells]({{< relref "cd8_positive_t_lymphocyte" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
