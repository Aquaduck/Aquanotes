+++
title = "Costomediastinal recess"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A [Pleural recess]({{< relref "pleural_recess" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the meaning of pleural recesses and describe the locations of the costodiaphragmatic and costmediastinal recesses.]({{< relref "describe_the_meaning_of_pleural_recesses_and_describe_the_locations_of_the_costodiaphragmatic_and_costmediastinal_recesses" >}}) {#describe-the-meaning-of-pleural-recesses-and-describe-the-locations-of-the-costodiaphragmatic-and-costmediastinal-recesses-dot--describe-the-meaning-of-pleural-recesses-and-describe-the-locations-of-the-costodiaphragmatic-and-costmediastinal-recesses-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Pleural recesses**

    <!--list-separator-->

    -  [Costomediastinal recess]({{< relref "costomediastinal_recess" >}})

        -   On left side between anterior chest wall and pericardial sac
        -   heart lies close to anterior thoracic wall and costal pleura is close to the mediastinal pleura


### Unlinked references {#unlinked-references}

[Show unlinked references]
