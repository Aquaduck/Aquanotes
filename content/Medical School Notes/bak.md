+++
title = "Bak"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   One of the main pro-apoptotic effector [Bcl2]({{< relref "b_cell_lymphoma_2_family" >}}) family proteins


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026) > BH3-only proteins**

    Enables the aggregation of [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}}) on the surface of mitochondria -> triggers release of intermembrane mitochondrial proteins -> induction of apoptosis

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026)**

    <!--list-separator-->

    -  [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}})

        -   **Pro-apoptotic** effector Bcl2 family proteins
        -   **At least one of these is required** for the intrinsic pathway of apoptosis to operate


### Unlinked references {#unlinked-references}

[Show unlinked references]
