+++
title = "Retinoblastoma protein family"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Inhibits [E2F]({{< relref "e2f" >}}) activity


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Mitogens Stimulate G<sub>1</sub>-Cdk and G1/S-Cdk activities (p. 1012) > Mitogens interact with cell-surface receptors to trigger multiple intracellular signaling pathways**

    <!--list-separator-->

    -  [Rb family]({{< relref "retinoblastoma_protein_family" >}})

        -   When cells are stimulated to divide by mitogens -> active G<sub>1</sub>/Cdk accumulates -> **phosphorylates** Rb -> reduces Rb binding to E2F
            -   Allows E2F to activate expression of target genes
        -   Loss of both copies of the Rb gene -> excessive proliferation of some cells in the developing retina
            -   Suggests that **Rb is particularly important for restraining cell division in this tissue**

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Mitogens Stimulate G<sub>1</sub>-Cdk and G1/S-Cdk activities (p. 1012) > Mitogens interact with cell-surface receptors to trigger multiple intracellular signaling pathways > E2F proteins**

    In the absence of mitogenic stimulation, E2F-dependent gene expression is inhibited by members of the [Retinoblastoma protein family]({{< relref "retinoblastoma_protein_family" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
