+++
title = "β-oxidation"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   First stage in the oxidation of [fatty acids]({{< relref "fatty_acid" >}}) in the [mitochondria]({{< relref "mitochondrion" >}})
-   Removes carbons 1 and 2 as a molecule of [acetyl-CoA]({{< relref "acetyl_coa" >}})
-   For odd-chained fatty acids, produces 1 acetyl-CoA and 1 [Propionyl-CoA]({{< relref "propionyl_coa" >}})


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Peroxisome]({{< relref "peroxisome" >}}) {#peroxisome--peroxisome-dot-md}

<!--list-separator-->

-  **🔖 From First Aid**

    [Beta-oxidation]({{< relref "β_oxidation" >}}) of very-long-chain fatty acids (VLCFA)

    ---


#### [List the products of the ß-oxidation of odd-chain fatty acids.]({{< relref "list_the_products_of_the_ß_oxidation_of_odd_chain_fatty_acids" >}}) {#list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot--list-the-products-of-the-ß-oxidation-of-odd-chain-fatty-acids-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  Products of [beta-oxidation]({{< relref "β_oxidation" >}}) of odd-chain fatty acids

        -   Initial products of odd-chain fatty acid beta-oxidation are [acetyl-CoA]({{< relref "acetyl_coa" >}}) and [propionyl-CoA]({{< relref "propionyl_coa" >}})
        -   Acetyl-CoA goes to TCA
        -   [Propionyl-CoA]({{< relref "propionyl_coa" >}}) -([Propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}}))> [D-methylmalonyl-CoA]({{< relref "d_methylmalonyl_coa" >}}) -> [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) -([Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}}))> [succinyl-CoA]({{< relref "succinyl_coa" >}}) -> TCA cycle


#### [List the glucose-utilizing pathways that predominate in:]({{< relref "list_the_glucose_utilizing_pathways_that_predominate_in" >}}) {#list-the-glucose-utilizing-pathways-that-predominate-in--list-the-glucose-utilizing-pathways-that-predominate-in-dot-md}

<!--list-separator-->

-  **🔖 Adipose**

    [Fatty acid oxidation]({{< relref "β_oxidation" >}})

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 FAD- and NAD<sup>+</sup>-linked dehydrogenases > From Fat Metabolism in Muscle & Adipose Tissue**

    Recognize very long-, long-, middle-, or small-chain fatty acids as substrates -> carry out oxidation reactions in [beta-oxidation]({{< relref "β_oxidation" >}})

    ---

<!--list-separator-->

-  [Beta-oxidation]({{< relref "β_oxidation" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   First stage of oxidation of [fatty acids]({{< relref "fatty_acid" >}})
        -   Oxidation of the fatty acid at the beta-carbon, followed by a hydration, an oxidation, and a thiolysis
        -   Removes carbons 1 and 2 as a molecule of acetyl-CoA
        -   One round of beta-oxidation -> 1 molecule of NADH and FADH<sub>2</sub>
        -   Odd-chain fatty acids produce acetyl-CoA **and** propionyl-CoA
        -   **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**
            -   Increased [propionic acid] = _biotin deficiency_
            -   Increased [methylmalonic acid] = _vitamin B12 deficiency_
            -   Leads to **mental deficits**


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  [Fatty acid oxidation]({{< relref "β_oxidation" >}})

    -   [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)
    -   [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Fatty acyl-CoA synthetase**

    Required to "activate" cytoplasmic fatty acids for entry into mitochondria for [β-oxidation]({{< relref "β_oxidation" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
