+++
title = "Tropomyosin"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Pericyte]({{< relref "pericyte" >}}) {#pericyte--pericyte-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology**

    Contain [actin]({{< relref "actin" >}}), [myosin]({{< relref "myosin" >}}), and [tropomyosin]({{< relref "tropomyosin" >}}) -> suggests they play a role in contraction

    ---


#### [Explain the role of calcium in striated muscle contraction and describe its regulation.]({{< relref "explain_the_role_of_calcium_in_striated_muscle_contraction_and_describe_its_regulation" >}}) {#explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot--explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > A sudden rise in cytosolic Ca<sup>2+</sup> concentration initiates muscle contraction (p. 920)**

    Muscle form of [tropomyosin]({{< relref "tropomyosin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
