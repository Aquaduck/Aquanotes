+++
title = "Perilipin"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [PKA]({{< relref "protein_kinase_a" >}})-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Protein kinase A]({{< relref "protein_kinase_a" >}}) {#protein-kinase-a--protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    PKA-catalyzed phosphorylation of [perilipin]({{< relref "perilipin" >}}) and [HSL]({{< relref "hormone_sensitive_lipase" >}}) is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  [Perilipin]({{< relref "perilipin" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
            -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
        -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of 2,3-DAG**


### Unlinked references {#unlinked-references}

[Show unlinked references]
