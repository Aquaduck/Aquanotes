+++
title = "Apolipoprotein CIII"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Chylomicrons > From Fat Metabolism in Muscle & Adipose Tissue**

    Molecules of [apo-CII]({{< relref "apolipoprotein_cii" >}}) and [apo-CIII]({{< relref "apolipoprotein_ciii" >}}) are also integrated into the chylomicron "membrane"

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
