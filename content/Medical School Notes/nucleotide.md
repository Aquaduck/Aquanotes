+++
title = "Nucleotide"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Nucleoside]({{< relref "nucleoside" >}}) {#nucleoside--nucleoside-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [nucleotide]({{< relref "nucleotide" >}}) bound to a [ribose]({{< relref "ribose" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
