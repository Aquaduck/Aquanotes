+++
title = "Antigen presenting cell"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Presents partially-digested [antigens]({{< relref "antigen" >}}) to [T lymphocyte]({{< relref "t_lymphocyte" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}}) {#list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot--list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymph nodes > Parts of the lymph node:**

    Comprised of [T-lymphocytes]({{< relref "t_lymphocyte" >}}) with scattered [APCs]({{< relref "antigen_presenting_cell" >}})

    ---


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Activation and actions - T and B lymphocytes > T lymphocytes**

    Activated by **recognition of a partially-digested foreign peptide displayed by [antigen presenting cells]({{< relref "antigen_presenting_cell" >}})**

    ---


#### [APC disambiguation]({{< relref "apc_disambiguation" >}}) {#apc-disambiguation--apc-disambiguation-dot-md}

<!--list-separator-->

-  **🔖 APC can refer to:**

    [Antigen presenting cell]({{< relref "antigen_presenting_cell" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
