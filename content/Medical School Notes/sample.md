+++
title = "Sample"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Makes an _inference_ about a [population]({{< relref "population" >}})


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [T-test]({{< relref "t_test" >}}) {#t-test--t-test-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Calculates the difference between the [means]({{< relref "mean" >}}) of two [samples]({{< relref "sample" >}}) _or_ between a sample and a [population]({{< relref "population" >}})/value subject to change

    ---


#### [Statistical power]({{< relref "statistical_power" >}}) {#statistical-power--statistical-power-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Positively correlates with [sample size]({{< relref "sample" >}}) and [accuracy]({{< relref "accuracy" >}})

    ---


#### [Sample mean]({{< relref "sample_mean" >}}) {#sample-mean--sample-mean-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Mean]({{< relref "mean" >}}) of a [Sample]({{< relref "sample" >}})

    ---


#### [Population]({{< relref "population" >}}) {#population--population-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Is a sampling of a [sample]({{< relref "sample" >}})

    ---


#### [Interpret confidence intervals around a sample size or calculated relative risk or odds ratio]({{< relref "interpret_confidence_intervals_around_a_sample_size_or_calculated_relative_risk_or_odds_ratio" >}}) {#interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio--interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Confidence interval**

    Provide a way to determine a [Population]({{< relref "population" >}}) measurement or a value that is subject to change from a [Sample]({{< relref "sample" >}}) measurement

    ---


#### [Compare and contrast sample and population]({{< relref "compare_and_contrast_sample_and_population" >}}) {#compare-and-contrast-sample-and-population--compare-and-contrast-sample-and-population-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Sample]({{< relref "sample" >}})

        -   A group of people that is representative of a larger population


### Unlinked references {#unlinked-references}

[Show unlinked references]
