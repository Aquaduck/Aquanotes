+++
title = "B vitamin"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   A class of water-soluble [vitamins]({{< relref "vitamin" >}}) that play an important role in [cell metabolism]({{< relref "metabolism" >}}) and synthesis of [red blood cells]({{< relref "red_blood_cell" >}})


## Backlinks {#backlinks}


### 8 linked references {#8-linked-references}


#### [Thiamine]({{< relref "thiamine" >}}) {#thiamine--thiamine-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    [B vitamin]({{< relref "b_vitamin" >}})

    ---


#### [Riboflavin]({{< relref "riboflavin" >}}) {#riboflavin--riboflavin-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    [B vitamin]({{< relref "b_vitamin" >}})

    ---


#### [Pyridoxine]({{< relref "pyridoxine" >}}) {#pyridoxine--pyridoxine-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    [B vitamin]({{< relref "b_vitamin" >}})

    ---


#### [Pantothenic acid]({{< relref "pantothenic_acid" >}}) {#pantothenic-acid--pantothenic-acid-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    [B vitamin]({{< relref "b_vitamin" >}})

    ---


#### [Niacin]({{< relref "niacin" >}}) {#niacin--niacin-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    [B vitamin]({{< relref "b_vitamin" >}})

    ---


#### [Folate]({{< relref "folate" >}}) {#folate--folate-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    [B vitamin]({{< relref "b_vitamin" >}})

    ---


#### [Cobalamin]({{< relref "cobalamin" >}}) {#cobalamin--cobalamin-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    [B vitamin]({{< relref "b_vitamin" >}})

    ---


#### [Biotin]({{< relref "biotin" >}}) {#biotin--biotin-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    [B vitamin]({{< relref "b_vitamin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
