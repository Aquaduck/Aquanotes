+++
title = "Lamin A"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [Lamin]({{< relref "lamin" >}}) protein


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}}) {#rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot--rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot-md}

<!--list-separator-->

-  **🔖 From Protein farnesylation and disease > Isoprenylation and postprenylation of nuclear lamins**

    Consequently, no mature [lamin A]({{< relref "lamin_a" >}}) is formed -> accumulation of a **farnesylated mutant prelamin A** ([progerin]({{< relref "progerin" >}}))

    ---

<!--list-separator-->

-  **🔖 From Protein farnesylation and disease > Nuclear lamins**

    <!--list-separator-->

    -  [A-type lamins]({{< relref "lamin_a" >}})

        -   Products of a single gene termed [LMNA]({{< relref "lmna" >}})
        -   [Prelamin A]({{< relref "prelamin_a" >}}) is the precursor protein of mature lamin A and possesses a COOH terminal CAAX motif -> the site of post-translational modifications
        -   Plays a major role in **nuclear envelope architecture** and a functional role in **heterochromatin organization, cell cycle, differentiation dynamics, and transcriptional regulation**
        -   Undergoes post-translational modification via [farnesylation]({{< relref "farnesylation" >}})


#### [Prelamin A]({{< relref "prelamin_a" >}}) {#prelamin-a--prelamin-a-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Precursor to mature [lamin A]({{< relref "lamin_a" >}})

    ---


#### [LMNA]({{< relref "lmna" >}}) {#lmna--lmna-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Gene that codes for [A-type lamins]({{< relref "lamin_a" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
