+++
title = "Protein kinase A"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Activated by [glucagon]({{< relref "glucagon" >}})
-   Inhibited by [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}})
-   PKA-catalyzed phosphorylation of [perilipin]({{< relref "perilipin" >}}) and [HSL]({{< relref "hormone_sensitive_lipase" >}}) is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})


## Backlinks {#backlinks}


### 24 linked references {#24-linked-references}


#### [Perilipin]({{< relref "perilipin" >}}) {#perilipin--perilipin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [PKA]({{< relref "protein_kinase_a" >}})-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [Name the second messenger produced in response to glucagon-induced cellular signaling and how it functions to elicit the appropriate intracellular response]({{< relref "name_the_second_messenger_produced_in_response_to_glucagon_induced_cellular_signaling_and_how_it_functions_to_elicit_the_appropriate_intracellular_response" >}}) {#name-the-second-messenger-produced-in-response-to-glucagon-induced-cellular-signaling-and-how-it-functions-to-elicit-the-appropriate-intracellular-response--name-the-second-messenger-produced-in-response-to-glucagon-induced-cellular-signaling-and-how-it-functions-to-elicit-the-appropriate-intracellular-response-dot-md}

<!--list-separator-->

-  **🔖 Function of cAMP**

    Upon activation by glucagon -> cAMP binds to two regulatory subunits (R) of inactive [PKA]({{< relref "protein_kinase_a" >}}) -> releases two catalytically-active subunits + activation of PKA -> phosphorylation of substrates -> activation of enzymes involved in catabolism

    ---


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Insulin's metabolic effects**

    Hydrolyzes cAMP to 5'-AMP -> eliminate second messenger activating PKA -> [PKA]({{< relref "protein_kinase_a" >}}) activity significantly diminished

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Insulin's metabolic effects**

    Insulin imediately activates [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) -> remove any phosphates introduced by [PKA]({{< relref "protein_kinase_a" >}}) -> **reverses processes**

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects > Reversal of glucagon's actions**

    Phosphate groups added to [PKA]({{< relref "protein_kinase_a" >}}) substrates are removed by protein [phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) via hydrolytic cleavage of phosphate esters

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    [PKA]({{< relref "protein_kinase_a" >}}) also phosphorylates and inactivates the liver pyruvate kinase isoenzyme -> ensures that when PEP is formed from pyruvate, it remains a substrate for enolase

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    [PKA]({{< relref "protein_kinase_a" >}})-mediated phosphorylation of [CREB]({{< relref "camp_response_element_binding_protein" >}}) -> increased transcription of [gluconeogenic]({{< relref "gluconeogenesis" >}}) enzymes if liver glycogen stores become depleted

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    This is because [PKA]({{< relref "protein_kinase_a" >}}) phosphorylates and activates a key glycogen-degrading enzyme ([glycogen phosphorylase kinase]({{< relref "glycogen_phosphorylase_kinase" >}})), and also phosphorylates and inactivates the rate-limiting enzyme in glycogen synthesis ([glycogen synthase]({{< relref "glycogen_synthase" >}}))

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase > Takeaways**

    [PKA]({{< relref "protein_kinase_a" >}}) enters nucleus and phosphorylates [cAMP response element binding protein]({{< relref "camp_response_element_binding_protein" >}}) (CREB)

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase > Takeaways**

    [cAMP]({{< relref "camp" >}}) binds to the two regulatory subunits (R) of the inactive [PKA]({{< relref "protein_kinase_a" >}}) -> release of two catalytically-active subunits -> activation of PKA

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Mobilization of triacylglycerol stored in adipocytes**

    Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Additional important enzymes**

    <!--list-separator-->

    -  [Protein kinase A]({{< relref "protein_kinase_a" >}}) (PKA)

        -   Ser/Thr kinase
        -   Effector enzyme resulting from glucagon and epinephrine induced signaling events
            -   Glucagon or epinephrine binds to respective membrane receptors -> [adenylate cyclase]({{< relref "adenylate_cyclase" >}}) catalyzed conversion of ATP to cAMP
                -   cAMP activates PKA by binding to its regulatory subunits -> release of catalytic subunits

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Hormone-sensitive lipase**

    [PKA]({{< relref "protein_kinase_a" >}}) substrate

    ---

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Acetyl-CoA carboxylase**

    [PKA]({{< relref "protein_kinase_a" >}}) substrate and **downregulated by phosphorylation**

    ---

<!--list-separator-->

-  **🔖 Glycolysis > 6-Phosphofructo-2-kinase (PFK-2)**

    The _kinase_ component is a [PKA]({{< relref "protein_kinase_a" >}}) substrate

    ---


#### [Hormone-sensitive lipase]({{< relref "hormone_sensitive_lipase" >}}) {#hormone-sensitive-lipase--hormone-sensitive-lipase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [PKA]({{< relref "protein_kinase_a" >}})-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [Fructose-2,6-bisphosphate (feed-forward regulation)]({{< relref "fructose_2_6_bisphosphate_feed_forward_regulation" >}}) {#fructose-2-6-bisphosphate--feed-forward-regulation----fructose-2-6-bisphosphate-feed-forward-regulation-dot-md}

<!--list-separator-->

-  **🔖 Synthesis**

    <!--list-separator-->

    -  [PKA]({{< relref "protein_kinase_a" >}})

        -   Phosphorylates aforementioned bifunctional enzyme -> determines which of two domains is active

        <!--list-separator-->

        -  Dephosphorylated state

            -   ↑ PFK-2 activity -> ↑ F-2,6-bP

        <!--list-separator-->

        -  Phosphorylated state

            -   ↑ FBPase-2 activity -> ↓ F-2,6-bP


#### [Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important.]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}}) {#describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot--describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Off Signals > Several pathways in the cell to turn off a signal: > Inactivate the receptor**

    [PKA]({{< relref "protein_kinase_a" >}}) has an additional action: **phosphorylation of the ser/thr residue of the receptor itself** -> turns receptor off

    ---


#### [Describe the role of protein kinase A in mobilizing fat from adipocyte stores.]({{< relref "describe_the_role_of_protein_kinase_a_in_mobilizing_fat_from_adipocyte_stores" >}}) {#describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot--describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > The role of PKA in mobilizing fat from adipocytes**

    Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})

    ---

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  The role of [PKA]({{< relref "protein_kinase_a" >}}) in mobilizing fat from [adipocytes]({{< relref "adipose_cell" >}})

        -   **Triacylglycerol energy reserve is stored in adipocytes in the fed state**
            -   **Decreased concentrations of [insulin]({{< relref "insulin" >}}) are essential for the successful initiation of this process**
                -   Activation of both this process & major enzymes required for [lipolysis]({{< relref "lipolysis" >}}) is accomplished by [PKA]({{< relref "protein_kinase_a" >}})
                -   Therefore, **cAMP-mediated activation of PKA is required**
        -   _Perilipin_ (aka _lipid droplet-associated protein_) coats the surface of intracellular lipid droplets -> protects them from activate intracellular lipases
            -   **PKA-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored fatty acids**
        -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
        -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol


#### [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}}) {#describe-the-intercellular-response-to-insulin-induced-cellular-signaling--describe-the-intercellular-response-to-insulin-induced-cellular-signaling-dot-md}

<!--list-separator-->

-  **🔖 Insulin's metabolic effects**

    Hydrolyzes cAMP to 5'-AMP -> eliminate second messenger activating PKA -> [PKA]({{< relref "protein_kinase_a" >}}) activity significantly diminished

    ---

<!--list-separator-->

-  **🔖 Insulin's metabolic effects**

    Insulin imediately activates [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) -> remove any phosphates introduced by [PKA]({{< relref "protein_kinase_a" >}}) -> **reverses processes**

    ---


#### [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}}) {#describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot--describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Regulation of the mobilization of intramuscular fat > Role of epinephrine**

    Activation of [PKA]({{< relref "protein_kinase_a" >}})

    ---


#### [Describe the cyclic-AMP (cAMP) second messenger signal pathway from hormone binding to activation of protein kinase-A.]({{< relref "describe_the_cyclic_amp_camp_second_messenger_signal_pathway_from_hormone_binding_to_activation_of_protein_kinase_a" >}}) {#describe-the-cyclic-amp--camp--second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot--describe-the-cyclic-amp-camp-second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  Activation of [Protein kinase A]({{< relref "protein_kinase_a" >}}) (PKA)

        <ol class="org-ol">
        <li value="20">cAMP activates PKA</li>
        </ol>


### Unlinked references {#unlinked-references}

[Show unlinked references]
