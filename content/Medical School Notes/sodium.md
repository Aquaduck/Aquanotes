+++
title = "Sodium"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Renin-angiotensin-aldosterone mechanism]({{< relref "renin_angiotensin_aldosterone_mechanism" >}}) {#renin-angiotensin-aldosterone-mechanism--renin-angiotensin-aldosterone-mechanism-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Ultimately leads to the reabsorption of [sodium]({{< relref "sodium" >}}) by the [kidneys]({{< relref "kidney" >}})

    ---


#### [Loop of Henle]({{< relref "loop_of_henle" >}}) {#loop-of-henle--loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Component of the [Nephron]({{< relref "nephron" >}}) responsible for reabsorption of [water]({{< relref "water" >}}) and [sodium]({{< relref "sodium" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Sodium]({{< relref "sodium" >}}) is a direct determinant of blood volume

    ---


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Aldosterone**

    Increases the reabsorption of [sodium]({{< relref "sodium" >}}) into the [blood]({{< relref "blood" >}}) by the [kidneys]({{< relref "kidney" >}}) -> **increases reabsorption of water** -> increases blood volume -> raises blood pressure

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    E.g. [Sodium]({{< relref "sodium" >}}), [Potassium]({{< relref "potassium" >}}), [Chloride]({{< relref "chloride" >}}), [Bicarbonate]({{< relref "bicarbonate" >}}), [Glucose]({{< relref "glucose" >}}), [Urea]({{< relref "urea" >}}), [amino acids]({{< relref "amino_acid" >}}), peptides such as [Insulin]({{< relref "insulin" >}}) and [ADH]({{< relref "antidiuretic_hormone" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Macula densa**

    Senses [Sodium]({{< relref "sodium" >}}) and Chloride levels and controls [Renin]({{< relref "renin" >}}) release, glomerular filtration, and renal blood flow via signaling pathways

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
