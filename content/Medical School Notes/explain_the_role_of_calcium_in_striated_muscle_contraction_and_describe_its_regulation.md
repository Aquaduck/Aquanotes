+++
title = "Explain the role of calcium in striated muscle contraction and describe its regulation."
author = ["Arif Ahsan"]
date = 2021-09-19T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}}) {#from-molecular-biology-of-the-cell-sixth-edition--molecular-biology-of-the-cell-sixth-edition-dot-md}


### A sudden rise in cytosolic Ca<sup>2+</sup> concentration initiates muscle contraction (p. 920) {#a-sudden-rise-in-cytosolic-ca-concentration-initiates-muscle-contraction--p-dot-920}

-   Incoming AP activates a calcium channel in the T-tubule membrane -> calcium influx -> opening of calcium release channels in [sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}})
    -   Calcium is then rapidly pumped back into SR by [calcium atpase]({{< relref "calcium_atpase" >}}), an abundant ATP-dependent calcium pump -> [myofibrils]({{< relref "myofibril" >}}) relax
-   Calcium dependence of vertebrate skeletal muscle contraction due entirely to a set of specialized accessory proteins closely associated with [actin]({{< relref "actin" >}}) thin filaments:
    -   Muscle form of [tropomyosin]({{< relref "tropomyosin" >}})
    -   [Troponin]({{< relref "troponin" >}})
        -   A complex of three polypeptides:
            -   Troponin T (Tropomyosin-binding)
            -   Troponin I (Inhibitory)
            -   Troponin C (Calcium binding)
                -   Closely related to [calmodulin]({{< relref "calmodulin" >}})
        -   Troponin I binds to actin as well as to troponin T
            -   In resting muscle, this complex pulls ropomyosin out of its normal binding groove into a position along the [actin]({{< relref "actin" >}}) filament that **interferes with the binding of myosin heads** -> **no force generation**
        -   When calcium levels rise, troponin C binds to calcium -> causes troponin I to release actin
            -   Allows tropomyosin to slip back into normal position -> allows myosin head binding to actin


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-15 Wed] </span></span> > Contractile Regulation > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Explain the role of calcium in striated muscle contraction and describe its regulation.]({{< relref "explain_the_role_of_calcium_in_striated_muscle_contraction_and_describe_its_regulation" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
