+++
title = "T lymphocyte"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   One of two [lymphocytes]({{< relref "lymphocyte" >}}) as part of the [adaptive immune system]({{< relref "adaptive_immunity" >}})


## Backlinks {#backlinks}


### 16 linked references {#16-linked-references}


#### [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}}) {#list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot--list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymph nodes > Parts of the lymph node:**

    Comprised of [T-lymphocytes]({{< relref "t_lymphocyte" >}}) with scattered [APCs]({{< relref "antigen_presenting_cell" >}})

    ---


#### [DiGeorge syndrome]({{< relref "digeorge_syndrome" >}}) {#digeorge-syndrome--digeorge-syndrome-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A congenital [T-cell]({{< relref "t_lymphocyte" >}}) deficiency

    ---


#### [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}}) {#describe-the-structure-of-the-thymus--include-changes-with-age-dot----describe-the-structure-of-the-thymus-include-changes-with-age-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Medulla of the thymus**

    Help [T-lymphocytes]({{< relref "t_lymphocyte" >}}) develop into helper or suppressor cells

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Medulla of the thymus**

    Test the new [T-cells]({{< relref "t_lymphocyte" >}}) for self-reactivity and HLA molecule recognition

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Cortex of the thymus**

    [T-lymphocytes]({{< relref "t_lymphocyte" >}}) are rearranging their [TCR]({{< relref "t_cell_receptor" >}})

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Thymus epithelium**

    In the [Medulla of the thymus]({{< relref "medulla_of_the_thymus" >}}) epithelial cells direct elimination of autoreactive lymphocytes and help [T-cell]({{< relref "t_lymphocyte" >}}) differentiation into helper or suppressor cells

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus**

    The function of the thymus is to support [T-lymphocyte]({{< relref "t_lymphocyte" >}}) development (Stage 1), but remove autoreactive T-cells

    ---


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen**

    Surrounding central arterioles in the spleen are an aggregate of [T-lymphocytes]({{< relref "t_lymphocyte" >}}) called a [periarteriole lymphoid sheath]({{< relref "periarteriole_lymphoid_sheath" >}}) (PALS)

    ---


#### [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}}) {#describe-the-major-actions-occurring-in-these-regions--with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot----describe-the-major-actions-occurring-in-these-regions-with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Follicles**

    If a previously activated [T-cell]({{< relref "t_lymphocyte" >}}) from the paracortex recognizes the peptide -> T-cell helps B-cell form a [germinal center]({{< relref "germinal_center" >}}) (i.e. [secondary follicle]({{< relref "secondary_follicle" >}})) and try to **make a more specific antibody through hypermutation of the Ig gene**

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Paracortex**

    These two cell types interact -> testing to see if any [T-cells]({{< relref "t_lymphocyte" >}}) recognize the peptide displayed by the [dendritic cells]({{< relref "dendritic_cell" >}})

    ---


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Summary of actions of B and T lymphocytes**

    <!--list-separator-->

    -  [T lymphocytes]({{< relref "t_lymphocyte" >}})

        -   T-cell selection in the [thymus]({{< relref "thymus" >}}) reduces the likelihood of turning the immune system on yourself
        -   T-cells are activated **first**

        <!--list-separator-->

        -  Life cycle

            1.  Rearrangement (VDJ recombination) of the TCR in [thymus]({{< relref "thymus" >}}) with extensive selection
            2.  Naive and memory cells circulate and migrate in tissues (especially lymphoid tissue) looking for cognate antigen
            3.  Recognition of peptide antigen presented by APC -> activate and proliferate in paracortex of lymph nodes
                -   No further mutation of TCR
            4.  Effector actions
            5.  Many effector cells die after threat abates, some survive as memory cells and return to step 2

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Summary of actions of B and T lymphocytes > B lymphocytes**

    Usually dependent on an activated [T lymphocyte]({{< relref "t_lymphocyte" >}}) to help them become activated

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Summary of actions of B and T lymphocytes**

    Both [T-cells]({{< relref "t_lymphocyte" >}}) and [B-cells]({{< relref "b_lymphocyte" >}}) derived from [hematopoietic stem cells]({{< relref "hematopoietic_stem_cell" >}}) of the bone marrow

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Activation and actions - T and B lymphocytes**

    <!--list-separator-->

    -  [T lymphocytes]({{< relref "t_lymphocyte" >}})

        -   Activated by **recognition of a partially-digested foreign peptide displayed by [antigen presenting cells]({{< relref "antigen_presenting_cell" >}})**
            -   APCs are special phagocytic cells (similar to [macrophages]({{< relref "macrophage" >}})) that sample interstitial fluid and present partially digested peptides to T lymphocytes
        -   T cells have several effector actions
            1.  Assist activated [B-cells]({{< relref "b_lymphocyte" >}}) to improve binding of their receptor and differentiation into plasma cells
            2.  Enhance the phagocytic actions of [macrophages]({{< relref "macrophage" >}})
            3.  Regulation of the immune reaction
            4.  Directly destroy infected host cells (cytotoxicity)
        -   The first three are performed by [CD4-positive (helper/suppressor) T-cells]({{< relref "cd4_positive_t_lymphocyte" >}}), and the last is done by [CD8-positive (cytotoxic) T-cells]({{< relref "cd8_positive_t_lymphocyte" >}})
            -   CD4 and CD8 are surface molecules that define these T-cell subsets


#### [Antigen presenting cell]({{< relref "antigen_presenting_cell" >}}) {#antigen-presenting-cell--antigen-presenting-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Presents partially-digested [antigens]({{< relref "antigen" >}}) to [T lymphocyte]({{< relref "t_lymphocyte" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Immunology > DiGeorge syndrome > Clinical features: > Thymus aplasia/hypoplasia**

    Recurent infections due to [T-cell]({{< relref "t_lymphocyte" >}}) deficiency

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
