+++
title = "Bone"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 14 linked references {#14-linked-references}


#### [Woven bone]({{< relref "woven_bone" >}}) {#woven-bone--woven-bone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Initial weak [bone]({{< relref "bone" >}})

    ---


#### [Tubular bone]({{< relref "tubular_bone" >}}) {#tubular-bone--tubular-bone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of [bone]({{< relref "bone" >}})

    ---


#### [Spongy bone]({{< relref "spongy_bone" >}}) {#spongy-bone--spongy-bone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Porous [bone]({{< relref "bone" >}}) composed of thin [trabeculae]({{< relref "trabeculae" >}})

    ---


#### [Periosteum]({{< relref "periosteum" >}}) {#periosteum--periosteum-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Dense irregular CT covering the outer [bone]({{< relref "bone" >}})

    ---


#### [Osteoid]({{< relref "osteoid" >}}) {#osteoid--osteoid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Complete protein matrix of [Bone]({{< relref "bone" >}})

    ---


#### [Osteocyte]({{< relref "osteocyte" >}}) {#osteocyte--osteocyte-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Entombed in [Bone]({{< relref "bone" >}})

    ---


#### [Osteoclast]({{< relref "osteoclast" >}}) {#osteoclast--osteoclast-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Resorbs [Bone]({{< relref "bone" >}}) and digests the [Osteoid]({{< relref "osteoid" >}})

    ---


#### [Osteoblast]({{< relref "osteoblast" >}}) {#osteoblast--osteoblast-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Lay down new [Osteoid]({{< relref "osteoid" >}}) and calcify it to form new [Bone]({{< relref "bone" >}})

    ---


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work**

    <!--list-separator-->

    -  Cytologic components of mature [bone]({{< relref "bone" >}})

        -   Three types of cells in addition to progenitor cells in endosteum:
            1.  [Osteoclasts]({{< relref "osteoclast" >}}) - resorb bone and digest the osteoid
                -   Multinucleated cells that reside in [Howship's lacunae]({{< relref "howship_s_lacunae" >}}) (irregular pits) in the bone formed by digestion of the osteoid
            2.  [Osteoblasts]({{< relref "osteoblast" >}}) - lay down new osteoid and calcify it to form new bone
                -   Sometimes line up in rows (almost look like a columnar epithelium) at the edge, adding more osteoid
                -   **Always immediately adjacent to the bone**
            3.  [Osteocytes]({{< relref "osteocyte" >}}) - emtombed in bone but highly metabolically active in [calcium]({{< relref "calcium" >}}) homeostasis
                -   Small cells encased deep into the osteoid
                -   Osteocytes communicate with one another through cell processes extending through tiny channels called [canaliculi]({{< relref "canaliculi" >}})
                -   Involved in day-to-day control of calcium levels
                -   Respond to the hormones [parathyroid hormone]({{< relref "parathyroid_hormone" >}}) and [calcitonin]({{< relref "calcitonin" >}})

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work**

    <!--list-separator-->

    -  Histologic components of mature [bone]({{< relref "bone" >}})

        -   Mature bone is highly organized dense regular CT
            -   90% of the protein is [Type 1 Collagen]({{< relref "type_1_collage" >}})
        -   Complete protein matrix of bone is called the [osteoid]({{< relref "osteoid" >}})
        -   [Calcium]({{< relref "calcium" >}}) is bound to the bone in a crystlaline form called [calcium hydroxyapatite]({{< relref "calcium_hydroxyapatite" >}})
        -   Mature bone is laid down in parallel layers called [lamellae]({{< relref "lamellae" >}})
            -   External circumferential lamella just underneath the [Periosteum]({{< relref "periosteum" >}})
            -   Internal circumferential lamella just inside the cortex
            -   Lamellae of bone are also laid down on the [Trabeculae]({{< relref "trabeculae" >}}) of [Cancellous bone]({{< relref "spongy_bone" >}})
            -   Dense cortical bone between internal and external lamellae consists of [osteons]({{< relref "osteon" >}})
                -   Small tubular structure comprised of multiple concentric lamellae

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work**

    <!--list-separator-->

    -  Organization of [bones]({{< relref "bone" >}})

        -   Bone is mostly hollow
            -   The dense outer part is called _cortical_ or _compact_ bone
        -   Porous bone is called _[spongy]({{< relref "spongy_bone" >}})_ or _cancellous_ bone
            -   Composed of thin _[trabeculae]({{< relref "trabeculae" >}})_ of bone
        -   Remaining non-bony inside part is called [bone marrow]({{< relref "bone_marrow" >}}) or _medullary cavity_
            -   [Red marrow]({{< relref "red_marrow" >}}) contains hematopoietic cells
            -   [Yellow marrow]({{< relref "yellow_marrow" >}}) contains only adipocytes
        -   Outer bone covered by a thin layer of dense irregular CT called the [periosteum]({{< relref "periosteum" >}})
            -   Encases **all bone except at joints** where there is a thin layer of cartilage
        -   Cortical bone and trabeculae are lined by an extremely thin layer of CT called [endosteum]({{< relref "endosteum" >}})
            -   Contains progenitor cells for bone
            -   Endosteum is typically not clearly visible on H&E sections


#### [Lamellae]({{< relref "lamellae" >}}) {#lamellae--lamellae-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A parallel layer of mature [Bone]({{< relref "bone" >}})

    ---


#### [Endosteum]({{< relref "endosteum" >}}) {#endosteum--endosteum-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Contains progenitor cells for [bone]({{< relref "bone" >}})

    ---


#### [Calcium hydroxyapatite]({{< relref "calcium_hydroxyapatite" >}}) {#calcium-hydroxyapatite--calcium-hydroxyapatite-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Crystalline form of [Calcium]({{< relref "calcium" >}}) found in [Bone]({{< relref "bone" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
