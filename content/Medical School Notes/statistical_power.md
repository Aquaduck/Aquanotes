+++
title = "Statistical power"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The probability of correctly rejecting the [null hypothesis]({{< relref "null_hypothesis" >}})
-   Complementary to the [type 2 error]({{< relref "type_ii_error" >}}) rate
-   Positively correlates with [sample size]({{< relref "sample" >}}) and [accuracy]({{< relref "accuracy" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Define statistical power and explain how it affects the interpretation of negative results]({{< relref "define_statistical_power_and_explain_how_it_affects_the_interpretation_of_negative_results" >}}) {#define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results--define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Statistical power]({{< relref "statistical_power" >}})

        -   The probability of correctly rejecting the [null hypothesis]({{< relref "null_hypothesis" >}})
        -   Complementary to the [type 2 error]({{< relref "type_ii_error" >}}) rate
        -   Positively correlates with sample size and magnitude of association of interest
            -   i.e. **increasing sample size = increasing statistical power**
            -   Correlates with measurement [accuracy]({{< relref "accuracy" >}})
        -   Most studies aim to achieve **80% statistical power**


### Unlinked references {#unlinked-references}

[Show unlinked references]
