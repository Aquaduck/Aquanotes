+++
title = "Describe the inositol phosphatide second messenger system"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Important members {#important-members}


#### Membrane phosphatidylinositol {#membrane-phosphatidylinositol}

<!--list-separator-->

-  [PIP<sub>2</sub>]({{< relref "phosphatidylinositol" >}})

    -   Naturally occuring membrane phospholipid
    -   Composed of a glycerol backbone attached to two fatty acids and the sugar _inositol_ via a covalently bound phosphate
        -   The inositol is bound to 2 phosphate groups


#### [G-proteins]({{< relref "g_protein" >}}) {#g-proteins--g-protein-dot-md}

-   G<sub>q</sub> activated when ligand binds to GPCR
-   Behaves like G<sub>s</sub> or G<sub>i</sub>
-   Activates phospholipase C-β


#### Phospholipase {#phospholipase}

<!--list-separator-->

-  [Phospholipase C-β]({{< relref "phospholipase_c_β" >}})

    -   Cleaves the glycerol-phosphate bond in PIP<sub>2</sub> -> produces DAG (diacylglycerol) and IP<sub>3</sub>


#### [Inositol-tris-phosphate]({{< relref "inositol_tris_phosphate" >}}) (IP<sub>3</sub>) {#inositol-tris-phosphate--inositol-tris-phosphate-dot-md----ip}

-   Created through Phospholipase C-β cleavage of PIP<sub>2</sub>


#### [Diacylglycerol]({{< relref "diacylglycerol" >}}) (DAG) {#diacylglycerol--diacylglycerol-dot-md----dag}

-   Created through Phospholipase C-β cleavage of PIP<sub>2</sub>


#### [Protein kinase C]({{< relref "protein_kinase_c" >}}) {#protein-kinase-c--protein-kinase-c-dot-md}

-   Activated by DAG
    -   Reinforced by increased intracellular levels of calcium
-   Phosphorylates ser/thr residues on target proteins
    -   Results in gene regulation of genes involved with hypertrophy and proliferation (growth and differentiation)


#### Calcium {#calcium}

-   Calcium is released from the ER in response to IP<sub>3</sub>
-   Binds to calmodulin


#### [Calmodulin]({{< relref "calmodulin" >}}) {#calmodulin--calmodulin-dot-md}

-   Activated by calcium release from ER
-   Functions to activate a series of calmodulin kinases -> **phosphorylation of target proteins\*** on ser/thr residues


### Sequence of events {#sequence-of-events}

1.  Ligand binds to receptor
2.  G<sub>q</sub> complex activated (behaves just like G<sub>s</sub> or G<sub>i</sub>)
3.  G<sub>q</sub> activates phospholipase C-β
4.  Formation of DAG and IP<sub>3</sub>
    1.  IP<sub>3</sub> travels to intracellular membranes of the ER -> calcium release -> calcium binds to [calmodulin]({{< relref "calmodulin" >}})
    2.  DAG recruits [protein kinase-C]({{< relref "protein_kinase_c" >}}) to the plasma membrane -> partial PKC activation
        -   PKC activation reinforced by increased intracellular levels of calcium
            -   Note: **calcium does not replace the need for prior DAG activation**
5.  PKC phosphorylates its target on the ser/thr residue


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}), including the role of:

        <!--list-separator-->

        -  Membrane phosphatidylinositol

        <!--list-separator-->

        -  G-proteins

        <!--list-separator-->

        -  Phospholipase

        <!--list-separator-->

        -  Inositol-tris-phosphate

        <!--list-separator-->

        -  Diacylglycerol

        <!--list-separator-->

        -  Protein kinase C

        <!--list-separator-->

        -  Calcium

        <!--list-separator-->

        -  Calmodulin


#### [Discuss receptor tyrosine kinases and tyrosine-associated receptor signal pathways]({{< relref "discuss_receptor_tyrosine_kinases_and_tyrosine_associated_receptor_signal_pathways" >}}) {#discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways--discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Receptor protein tyrosine kinases (RPTKs) > Two key pathways > Phosphatidylinositol kinase (PI-3 Kinase)**

    Involves [PIP<sub>2</sub> signalling]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
