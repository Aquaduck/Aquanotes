+++
title = "Kaplan Biochemistry Chapter 7"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "kaplan", "source"]
draft = false
+++

## 7.1 The Genetic Code {#7-dot-1-the-genetic-code}

-   _Central dogma of molecular biology_: the transfer of genetic information into [proteins]({{< relref "protein" >}})

{{< figure src="/ox-hugo/_20210721_145718screenshot.png" caption="Figure 1: The Central Dogma of Molecular Biology" >}}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 1. Summarize the central dogma of molecular biology, and cite exceptions to the original model**

    [7.1 The Genetic Code]({{< relref "kaplan_biochemistry_chapter_7" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
