+++
title = "6-Phosphofructo-1-kinase"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Catalyzes the rate-limiting, committed (third) step of [glycolysis]({{< relref "glycolysis" >}})
-   [Allosterically]({{< relref "allosteric_regulation" >}}) regulated
    -   Inhibited by [[ATP]({{< relref "atp" >}})]
    -   Stimulated by [[AMP]({{< relref "adenosine_monophosphate" >}})]
    -   Inhibited by [citrate]({{< relref "citrate" >}})
-   Slows glycolysis and allows for the synthesis of either [glycogen]({{< relref "glycogen" >}}) or fatty acid synthesis


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [List the five major mechanisms by which intermediary metabolism is regulated]({{< relref "list_the_five_major_mechanisms_by_which_intermediary_metabolism_is_regulated" >}}) {#list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated--list-the-five-major-mechanisms-by-which-intermediary-metabolism-is-regulated-dot-md}

<!--list-separator-->

-  **🔖 Examples in glucose catabolism > Allosteric control**

    [PFK-1]({{< relref "6_phosphofructo_1_kinase" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis > Fructose 1,6-bisphosphatase**

    Reverses [PFK-1]({{< relref "6_phosphofructo_1_kinase" >}})

    ---

<!--list-separator-->

-  **🔖 Glycolysis**

    <!--list-separator-->

    -  [6-Phosphofructo-1-kinase]({{< relref "6_phosphofructo_1_kinase" >}}) (PFK-1)

        -   Third enzyme in glycolysis
        -   **Catalyzes the rate-limiting, committed step of glycolysis**
        -   The **second** irreversible glycolytic reaction
        -   **Allosterically regulated** by **energy charge** of the cell
            -   ↑[ATP] **inhibits** PFK-1
            -   ↑[AMP] **stimulates** PFK-1
                -   This is because adenylate cyclase is a cytoplasmic enzyme and catalyzed in the reaction \\(2 ADP -> ATP + AMP\\)
                    -   AMP:ATP ratio is the more sensitive indicator of energy charge
        -   **Elevated levels of citrate** also allosterically **inhibits** PFK-1
            -   This is due to high [ATP] + [acetyl-CoA] in the mitochondria
            -   Slows [glycolysis]({{< relref "glycolysis" >}}) and allows for the synthesis of either [glycogen]({{< relref "glycogen" >}}) or fatty acid synthesis


### Unlinked references {#unlinked-references}

[Show unlinked references]
