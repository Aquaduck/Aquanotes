+++
title = "Acute chest syndrome"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS (Sickle Cell Case)]({{< relref "focs_sickle_cell_case" >}}) {#focs--sickle-cell-case----focs-sickle-cell-case-dot-md}

<!--list-separator-->

-  **🔖 Clinical features of SCD > Vaso-occlusive complications/events**

    **[Acute chest syndrome]({{< relref "acute_chest_syndrome" >}}) (ACS)**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
