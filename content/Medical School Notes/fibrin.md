+++
title = "Fibrin"
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Fibrinolysis (p. 818)**

    Inactive protein plasminogen converted to [plasmin]({{< relref "plasmin" >}}) -> gradually breaks down [fibrin]({{< relref "fibrin" >}}) of the clot

    ---


#### [Fibrinogen]({{< relref "fibrinogen" >}}) {#fibrinogen--fibrinogen-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Precursor to [fibrin]({{< relref "fibrin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
