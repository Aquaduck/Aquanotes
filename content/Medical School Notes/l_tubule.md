+++
title = "L tubule"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Terminal cisternae]({{< relref "terminal_cisternae" >}}) {#terminal-cisternae--terminal-cisternae-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    They are the ends of [L tubules]({{< relref "l_tubule" >}})

    ---


#### [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) {#sarcoplasmic-reticulum--sarcoplasmic-reticulum-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Muscle cell [SER]({{< relref "smooth_endoplasmic_reticulum" >}}), which forms a network of [L-tubules]({{< relref "l_tubule" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    They are the ends of [L tubules]({{< relref "l_tubule" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
