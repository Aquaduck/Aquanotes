+++
title = "Michaelis-Menten Kinetics"
author = ["Arif Ahsan"]
date = 2021-07-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   **Michaelis-Menten kinetics** models [enzyme]({{< relref "enzyme" >}}) kinetics
-   It takes the form of an equation describing the rate of enzymatic reactions by relating reaction rate (\\(\nu\\)) to the concentration of a substrate \\([S]\\):
    -   \\(\nu = V\_{max}\frac{[S]}{K\_{M} + [S]}\\)


## Michaelis constant {#michaelis-constant}

-   K<sub>M</sub>: the concentration of substrate where \\(V\_{o} = \frac{1}{2}V\_{max}\\)
-   \\(V\_{o}\\) is the rate of catalysis = number of moles of product formed per second


## Resources {#resources}

-   [The Michaelis-Menten Model Accounts for the Kinetic Properties of Many Enzymes](https://www.ncbi.nlm.nih.gov/books/NBK22430/)


## Backlinks {#backlinks}


### 8 linked references {#8-linked-references}


#### [The Michaelis-Menten Model Accounts for the Kinetic Properties of Many Enzymes]({{< relref "the_michaelis_menten_model_accounts_for_the_kinetic_properties_of_many_enzymes" >}}) {#the-michaelis-menten-model-accounts-for-the-kinetic-properties-of-many-enzymes--the-michaelis-menten-model-accounts-for-the-kinetic-properties-of-many-enzymes-dot-md}

<!--list-separator-->

-  [Overview]({{< relref "michaelis_menten_kinetics" >}})

    -   \\(V\_{o}\\) is the rate of catalysis = number of moles of product formed per second

    <!--list-separator-->

    -  The Significance of K<sub>M</sub> and V<sub>max</sub> Values

        -   K<sub>cat</sub> = _turnover number_ of an enzyme
            -   The number of substrate molecules converted into product by an enzyme molecule in a unit time when the enzyme is fully saturated with substrate
            -   Equal to the kinetic constant k<sub>2</sub>
            -   The maximal rate V<sub>max</sub> reveals the turnover number of an enzyme if the concentration of active sites [E]<sub>T</sub> is known
                -   \\(V\_{max} = K\_{2}[E]\_{t}\\) -> \\(k\_{2} = \frac{V\_{max}}{[E]\_T}\\)

    <!--list-separator-->

    -  Kinetic Perfection in Enzymatic Catalysis: The k<sub>cat</sub>/K<sub>M</sub> Criterion

        -   Under physiological conditions, the [S]/K<sub>M</sub> ratio is typically between _0.01_ and _1.0_
        -   When [S] >> K<sub>M</sub> -> V<sub>o</sub> = k<sub>cat</sub>
        -   When [S] << K<sub>M</sub>...
            -   Most active sites are unoccupied -> enzymatic rate is much less than k<sub>cat</sub>
            -   \\(V\_{o} = \frac{k\_{cat}}{K\_{M}}[E][S]\\) -> [E] nearly equal to [E]<sub>T</sub> -> \\(V\_{o} = \frac{k\_{cat}}{K\_{M}}[S][E]\_{T}\\)


#### [The Equations of Enzyme Kinetics]({{< relref "the_equations_of_enzyme_kinetics" >}}) {#the-equations-of-enzyme-kinetics--the-equations-of-enzyme-kinetics-dot-md}

<!--list-separator-->

-  **🔖 Lineweaver-Burk plot**

    Provides a useful graphical method for analysis of the [Michaelis-Menten]({{< relref "michaelis_menten_kinetics" >}}) equation

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Allosteric enzymes > Homotropic effectors**

    Contrasts with the hyperbola of enzymes following [Michaelis-Menten kinetics]({{< relref "michaelis_menten_kinetics" >}})

    ---


#### [Khan - Steady states and the Michaelis Menten equation]({{< relref "khan_steady_states_and_the_michaelis_menten_equation" >}}) {#khan-steady-states-and-the-michaelis-menten-equation--khan-steady-states-and-the-michaelis-menten-equation-dot-md}

<!--list-separator-->

-  **🔖 Notes**

    <!--list-separator-->

    -  [Michaelis constant]({{< relref "michaelis_menten_kinetics" >}}) (K<sub>M</sub>)     :ATTACH:

        -   What does K<sub>M</sub> mean?
            -   Let's assume \\(K\_{M} = [S]\\)
            -   If we substitute that into the Michaelis-Mentin equation, we get: <br />
                \\(V\_{o} = \frac{V\_{max}[S]}{2[S]}\\) <br />
                Which we can simplify further into <br />
                \\(V\_{o} = \frac{V\_{max}}{2}\\)
            -   Thus, K<sub>M</sub> is the [S] where \\(V\_{o} = \frac{1}{2}V\_{max}\\)

        {{< figure src="/ox-hugo/_20210713_212631screenshot.png" caption="Figure 1: The relationship between K<sub>M</sub> and V<sub>max</sub>" width="400" >}}

<!--list-separator-->

-  **🔖 Notes**

    <!--list-separator-->

    -  Deriving [Michaelis-Menten]({{< relref "michaelis_menten_kinetics" >}}) equation

        Begin with these equations:

        1.  \\(E + S \overset{1}{\rightleftharpoons} ES \overset{2}{\rightarrow} E + P\\)
        2.  \\(Rate\_{1} = Rate\_{-1} + Rate\_{2}\\)

        Swap out Rate values for Rate constant (K) x reactants

        -   \\(K\_{1}[E][S] = K\_{-1}[ES] + K\_{2}[ES]\\)

        The total amount of enzyme in a system includes both bound and unbound enzymes

        -   \\([E]\_{T(otal)} = [E] + [ES]\\)

        Rearrange and substitute in for [E] and factor out common term [ES] on right side of equation

        -   \\(K\_{1}([E]\_{T} - [ES])[S] = [ES\]\(K\_{-1} + K\_{2})\\)

        Expand left side of equation

        -   \\(K\_{1}[E]\_{T}[S] - K\_{1}[ES][S] - [ES\]\(K\_{-1} + K\_{2})\\)

        Divide both sides of equation by K<sub>1</sub>

        -   \\([E]\_{T}[S] - [ES][S] = [ES\]\(\frac{K\_{-1} + K\_{2}}{K\_{1}})\\)

        Rate constants are constant values, so lets define this as one term:

        -   \\(\frac{K\_{-1} + K\_{2}}{K\_{1}} = K\_{M}\\)

        Substitute in K<sub>m</sub> and multiply both sides by [ES][S]

        -   \\([E]\_{t}[S] = [ES]K\_{M} + [ES][S]\\)

        Flip equation around for legibility and factor out common terms

        -   \\([ES\]\(K\_{m} + [S]) = [E]\_{T}[S]\\)

        Divide both sides of equation by \\(K\_{M} + S\\) to move that term to right side

        -   \\([ES] = \frac{[E]\_{T}[S]}{K\_{M} + [S]}\\)

        Remember V<sub>o</sub> is equal to the rate of formation of our product, which is equal to \\(K\_{2}[ES]\\)

        -   \\(V\_{o} = \frac{\Delta P}{\Delta t} = K\_{2}[ES]\\)

        Multiply both sides by \\(K\_{2}\\)

        -   \\(K\_{2}[ES] = \frac{K\_{2}[E]\_{T}[S]}{K\_{M} + [S]}\\)

        If \\(V\_{o} = V\_{max}\\), then \\([E]\_{T} = [ES]\\), then \\(K\_{2}[E]\_{T} = V\_{max}\\)
        Now we substitute in V<sub>o</sub> and V<sub>max</sub>

        -   \\(V\_{o} = \frac{V\_{max}[S]}{K\_{M} + [S]}\\)


#### [Khan - Basics of enzyme kinematics]({{< relref "khan_basics_of_enzyme_kinematics" >}}) {#khan-basics-of-enzyme-kinematics--khan-basics-of-enzyme-kinematics-dot-md}

<!--list-separator-->

-  **🔖 Michaelis-Menten and allosteric enzymes**

    <!--list-separator-->

    -  [Michaelis-Menten Kinetics]({{< relref "michaelis_menten_kinetics" >}})

        -   _Michaelis-Menten enzymes_: enzymes that behave according to Michaelis-Menten kinetics


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 12. Interpret Michaelis-Menten and Lineweaver-Burk plots**

    [Michaelis-Menten Kinetics]({{< relref "michaelis_menten_kinetics" >}})

    ---

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 9. Define initial velocity (v<sub>o</sub>) and explain the effect of substrate concentration on enzyme velocity for a single subunit enzyme**

    [Michaelis-Menten Kinetics]({{< relref "michaelis_menten_kinetics" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
