+++
title = "DNA"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Steps in replication**

    [DNA]({{< relref "dna" >}}) is bound to basic proteins known as [histones]({{< relref "histone" >}}) to form stuctures called [nucleosomes]({{< relref "nucleosome" >}})

    ---


#### [Folate]({{< relref "folate" >}}) {#folate--folate-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Important for the synthesis of nitrogenous bases in [DNA]({{< relref "dna" >}}) and [RNA]({{< relref "rna" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
