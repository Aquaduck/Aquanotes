+++
title = "Discuss the concept of mechanochemical signal transduction in biology."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Mechano-chemical signal transduction {#mechano-chemical-signal-transduction}

-   **Mechanical forces** serve as trigger
-   A physiological example of this is **hearing**
-   Another example is found in **endothelial cells**
    -   Increased shear stress or wall tension -> NO production -> Smooth muscle relaxation as discussed [here]({{< relref "describe_the_role_of_gases_in_cell_signaling_with_special_emphasis_on_nitric_oxide_and_its_basic_mechanism_of_action" >}})


#### Tension affects cell shape {#tension-affects-cell-shape}

-   Rigidity and tension is critical to cell growth and differentiation
    -   The [extracellular matrix]({{< relref "extracellular_matrix" >}}) is a key determinant of cell function - "_dynamic reciprocity_"
        -   Mechanically distorting any one of a cell's elements -> changes in shape/mechanical forces throughout entire cell because of [cytoskeleton]({{< relref "cytoskeleton" >}})
    -   Genes specify extracellular matrix proteins -> regulate genes involved in these functions


#### Role of ion channels {#role-of-ion-channels}

-   Ion channels are tethered to cell membrane
    -   **Cell membrane distortion = ion channel distortion**


#### Exercise {#exercise}

-   An example of mechano-chemical signaling pathways that do not involve the plasma membrane
-   Exercise can cause muscle to grow substantially or change the muscle fiber type
-   Passively stretching a muscle -> **significant increase in glucose transport**
    -   Why exercise is a critical part of therapy for obesity and diabetes
-   Believed that [titin]({{< relref "titin" >}}) is an important component of this pathway


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Discuss the concept of mechanochemical signal transduction in biology.]({{< relref "discuss_the_concept_of_mechanochemical_signal_transduction_in_biology" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
