+++
title = "Type 1 alveolar cell"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the stages of lung development, their approximate times of development, and explain the major changes in each stage.]({{< relref "name_the_stages_of_lung_development_their_approximate_times_of_development_and_explain_the_major_changes_in_each_stage" >}}) {#name-the-stages-of-lung-development-their-approximate-times-of-development-and-explain-the-major-changes-in-each-stage-dot--name-the-stages-of-lung-development-their-approximate-times-of-development-and-explain-the-major-changes-in-each-stage-dot-md}

<!--list-separator-->

-  **🔖 From Embryology - Thorax Part 1: Thoracic Cavity & Lung Development PPT > Phases of lung growth: > Canalicular period (17 - 28 weeks):**

    [Type I alveolar cells]({{< relref "type_1_alveolar_cell" >}}) differentiate from Type II

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
