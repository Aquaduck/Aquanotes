+++
title = "List the two landmarks for approximating dermatome levels in the thorax."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### [Dermatome]({{< relref "dermatome" >}}) {#dermatome--dermatome-dot-md}

-   A _dermatome_ is an area of skin innervated by a single pair of spinal nerves
-   In thorax, skin overlies an intercostal space
    -   _Dorsal ramus_ of a spinal nerve innervates the skin over a small regoin of the back
    -   _Ventral ramus_ innervates rest of the dermatome all the way to midline anteriorly
-   Cell bodies of all sensory axons for one dermatome are in the _dorsal root ganglia_ for that spinal level
-   **Two landmarks are useful for approximating dermatome levels in the thorax:**
    1.  Nipple = T4
    2.  Xiphoid process = T6


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the two landmarks for approximating dermatome levels in the thorax.]({{< relref "list_the_two_landmarks_for_approximating_dermatome_levels_in_the_thorax" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
