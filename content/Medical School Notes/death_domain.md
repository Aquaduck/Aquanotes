+++
title = "Death domain"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [TNFR1-associated death domain protein]({{< relref "tnfr1_associated_death_domain_protein" >}}) {#tnfr1-associated-death-domain-protein--tnfr1-associated-death-domain-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Contains a [Death domain]({{< relref "death_domain" >}})

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Death receptors > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    An intracellular [death domain]({{< relref "death_domain" >}}) -> required for receptor to activate [apoptosis]({{< relref "apoptosis" >}})

    ---


#### [Fas-associated death domain]({{< relref "fas_associated_death_domain" >}}) {#fas-associated-death-domain--fas-associated-death-domain-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Contains a [Death domain]({{< relref "death_domain" >}})

    ---


#### [Death receptor]({{< relref "death_receptor" >}}) {#death-receptor--death-receptor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    "[Death domains]({{< relref "death_domain" >}})" (characteristic feature of DRs) activate [caspases]({{< relref "caspase" >}}) as part of the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
