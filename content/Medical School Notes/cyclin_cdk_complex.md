+++
title = "Cyclin-CDK complex"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A complex formed between [cyclin]({{< relref "cyclin" >}}) and [CDK]({{< relref "cdk_activating_kinase" >}}), activating enzymatic function


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Wee1]({{< relref "wee1" >}}) {#wee1--wee1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inactivates [cyclin-CDK complex]({{< relref "cyclin_cdk_complex" >}}) by phosphorylating two closely spaced sites above the active site

    ---


#### [Cdc25]({{< relref "cdc25" >}}) {#cdc25--cdc25-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Removes phosphates added by [Wee1]({{< relref "wee1" >}}) to reactivate the [cyclin-CDK complex]({{< relref "cyclin_cdk_complex" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
