+++
title = "Kaplan Biochemistry Chapter 10"
author = ["Arif Ahsan"]
date = 2021-07-27T00:00:00-04:00
tags = ["medschool", "kaplan", "source"]
draft = false
+++

## 10.1 Acetyl-CoA {#10-dot-1-acetyl-coa}


### Intro {#intro}

-   Occurs in the **mitochondria**
-   Main function: \*oxidation of acetyl-Coa to CO<sub>2</sub> and H<sub>2</sub>O
-   Produces NADH and FADH<sub>2</sub>


### Methods of Forming Acetyl-CoA {#methods-of-forming-acetyl-coa}

{{< figure src="/ox-hugo/_20210727_115922screenshot.png" caption="Figure 1: Overall reaction of pyruvate dehydrogenase complex" >}}

-   Pyruvate enters mitochondrion via active transport
    -   oxidized and decarboxylated -> **forms Acetyl-CoA** in the _pyruvate dehydrogenase complex_ in the **mitochondrial matrix**
-   **Produces NADH**


#### Pyruvate dehydrogenase complex [enzymes]({{< relref "enzyme" >}}): {#pyruvate-dehydrogenase-complex-enzymes--enzyme-dot-md}

1.  _Pyruvate dehydrogenase (PDH)_:
    -   Pyruvate is oxidized -> CO<sub>2</sub>
    -   Remaining 2-carbon molecule binds covalently to _thiamine pyrophosphate_ (vitamin B<sub>1</sub>, TPP)
    -   TPP is a coenzyme held by noncovalent interactions to PDH
    -   Mg<sup>2+</sup> is also required
2.  _Dihydrolipoyl transacetylase_:
    -   2-carbon molecule bonded to TPP oxidized and transferred to _[lipoic acid]({{< relref "lipoic_acid" >}})_, a coenzyme
        -   Lipoic acid's disulfide group oxidizes -> creates acetyl group bonded to lipoic acid via thioester linkage
        -   Then, catalyzes CoA-SH interaction w/ newly formed thioester link
            -   Transfer of acetyl group to form acetyl-CoA, leaving lipoic acid reduced
3.  _Dihydrolipoyl dehydrogenase_:
    -   FAD used as coenzyme to **reoxidize [lipoic acid]({{< relref "lipoic_acid" >}})** -> allows lipoic acid to be reused
        -   FAD reduced to FADH<sub>2</sub>
        -   In subsequent reactions, FADH<sub>2</sub> reoxidized to FAD while NAD<sup>+</sup> reduced to NADH

{{< figure src="/ox-hugo/_20210727_120744screenshot.png" caption="Figure 2: Mechanism of pyruvate dehydrogenase" >}}


## 10.2 Reactions of the [Citric Acid Cycle]({{< relref "citric_acid_cycle" >}}) {#10-dot-2-reactions-of-the-citric-acid-cycle--citric-acid-cycle-dot-md}

-   Takes place in the **mitochondrial matrix**

{{< figure src="/ox-hugo/_20210727_121307screenshot.png" caption="Figure 3: The Citric Acid Cycle" >}}


### Key Reactions {#key-reactions}


#### 1) Citrate Formation {#1-citrate-formation}

-   Reaction:
    1.  Acetyl-CoA and _oxaloacetate_ undergo condensation reaction -> form intermediate _citryl-CoA_
    2.  citryl-CoA hydrolyzed -> **citrate + CoA-SH**
    3.  Catalyzed by _citrate synthase_

{{< figure src="/ox-hugo/_20210727_121505screenshot.png" caption="Figure 4: Citrate formation" >}}


#### 2) Citrate Isomerized to Isocitrate {#2-citrate-isomerized-to-isocitrate}

-   Achiral citrate is isomerized to one of four possible isomers of _isocitrate_
-   Reaction:
    1.  Citrate binds at three points to _[aconitase]({{< relref "enzyme" >}})_
    2.  Water is lost from citrate -> cis-_aconitate_
    3.  Water added back to form _isocitrate_
-   Enzyme required Fe<sup>2+</sup>

{{< figure src="/ox-hugo/_20210727_122245screenshot.png" caption="Figure 5: Citrate isomerized to isocitrate" >}}


#### 3) \\(\alpha\\)-Ketoglutarate and CO<sub>2</sub> Formation {#3-alpha-ketoglutarate-and-co-formation}

-   Reaction:
    1.  Isocitrate oxidized to _oxalosuccinate_ by _[isocitrate dehydrogenase]({{< relref "enzyme" >}})_
    2.  Oxalosuccinate decarboxylated -> _\\(\alpha\\)-ketoglutarate_ and CO<sub>2</sub>
-   **Isocitrate dehydrogenase is the rate-limiting enzyme of the citric acid cycle**
    -   **First of the two carbons is lost here**
    -   **First NADH produced**

{{< figure src="/ox-hugo/_20210727_122909screenshot.png" caption="Figure 6: \\(\alpha\\)-Ketoglutarate and CO<sub>2</sub> Formation" >}}


#### 4) Succinyl-CoA and CO<sub>2</sub> Formation {#4-succinyl-coa-and-co-formation}

-   Carried out by _\\(\alpha\\)-ketoglutarate dehydrogenase complex_
    -   **Similar to PDH Complex**
-   **NAD<sup>+</sup> reduced to NADH**
-   Reaction:
    1.  \\(\alpha\\)-ketoglutarate and CoA come together -> produce CO<sub>2</sub>
        -   This is the **second and last carbon lost from the cycle**

{{< figure src="/ox-hugo/_20210727_123245screenshot.png" caption="Figure 7: Succinyl-CoA and CO<sub>2</sub> Formation" >}}


#### 5) Succinate Formation {#5-succinate-formation}

-   Reaction:
    1.  Hydrolysis of thioester bond on _succinyl-CoA_ yields _succinate_ and CoA-SH
    2.  Coupled to the phosphorylation of GDP to GTP
-   Succinate formation catalyzed by _[succinyl-CoA synthetase]({{< relref "enzyme" >}})_
-   _[nucleosidediphosphate kinase]({{< relref "enzyme" >}})_ catalyzes phosphate transfer from GTP to ADP -> producing ATP
-   **Only time in CAC that ATP is produced directly**
    -   ATP produciton primarily done in within the [ETC]({{< relref "oxidative_phosphorylation" >}})

{{< figure src="/ox-hugo/_20210727_123830screenshot.png" caption="Figure 8: Succinate formation" >}}


#### 6) Fumarate Formation {#6-fumarate-formation}

-   **Only step** of the CAC that **doesn't take place in the mitochondrial matrix**
    -   Occurs in the **inner membrane**
-   Reaction:
    1.  Succinate oxidized -> _fumarate_
        -   Catalyzed by _[succinate dehydrogenase]({{< relref "enzyme" >}})_
            -   Considered a _flavoprotein_ because it is **covalently bonded to FAD** - electron acceptor in this reaction
    2.  **FAD reduced to FADH<sub>2</sub>**
        -   NADH not used because succinate is not a good enough reducing agent to reduce NAD<sup>+</sup>
    3.  Each molecule of FADH<sub>2</sub> passes electrons to the [ETC]({{< relref "oxidative_phosphorylation" >}}) -> **production of 1.5 ATP**
        -   NADH would give rise to 2.5 ATP


#### 7) Malate Formation {#7-malate-formation}

-   Reaction:
    1.  _[fumarase]({{< relref "enzyme" >}})_ catalyzes hydrolysis of **alkene bond in fumarate** -> _malate_


#### 8) Oxaloacetate Formed Anew {#8-oxaloacetate-formed-anew}

-   Reaction:
    1.  _[malate dehydrogenase]({{< relref "enzyme" >}})_ catalyzes the oxidation of malate to _oxaloacetate_
    2.  **NAD<sup>+</sup> reduced to NADH**

{{< figure src="/ox-hugo/_20210727_124400screenshot.png" caption="Figure 9: The final steps of the Citric Acid Cycle" >}}


### Net Results and ATP Yield {#net-results-and-atp-yield}

-   Steps **3, 4, 8** each produce one NADH
-   Step **6** forms one FADH<sub>2</sub>
-   Step **5** yields one GTP -> ATP

{{< figure src="/ox-hugo/_20210727_124552screenshot.png" caption="Figure 10: Total amount of chemical energy harvested per pyruvate" >}}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
