+++
title = "Osmosis"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

Notes from osmosis that haven't been attached to a learning objective yet


## Action potentials in [myocytes]({{< relref "cardiac_muscle_cell" >}}) {#action-potentials-in-myocytes--cardiac-muscle-cell-dot-md}


### Phase 0 - depolarization {#phase-0-depolarization}

-   **Rapid influx of** _sodium_ into the cell -> _inward current_
    -   Responsible for **rapid depolarization**


### Phase 1 {#phase-1}

-   _Sodium_ **current stops**
-   _Potassium_ **slowly** flows out of cell
-   Depolarization stops, repolarization begins


### Phase 2 {#phase-2}

-   _Calcium_ **moves into the cell**
    -   Balances _potassium_ current moving out of cell -> **plateau** (charges are balanced)


### Phase 3 {#phase-3}

-   _Calcium_ current stops\*
-   _Potassium_ **continues to move** out of cell -> **repolarization continues**


### Phase 4 {#phase-4}

-   _Potassium_ current moving out of cell approaches equilibrium
    -   _Sodium_ + _calcium_ current moving into cell balances this -> **resting membrane potential achieved**


## Action potentials in [pacemaker cells]({{< relref "pacemaker_cell" >}}) {#action-potentials-in-pacemaker-cells--pacemaker-cell-dot-md}

-   Note: Phase 1 and 2 are **absent in pacemaker cells** -> **no plateau**


### Phase 4 {#phase-4}

-   _Sodium_ **moves into cell** through [funny channels]({{< relref "funny_channel" >}})
    -   Funny channels open in response to hyperpolarization
-   **Slowly depolarizes cell** until threshold met
    -   **Responsible for instability of resting membrane potential**


### Phase 0 {#phase-0}

-   **Strong inward** _calcium_ **movement**
-   Causes **rapid depolarization**


### Phase 3 {#phase-3}

-   **Strong** _potassium_ **current moves out of cell**
    -   Responsible for **repolarization**


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
