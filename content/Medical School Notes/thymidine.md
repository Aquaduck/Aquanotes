+++
title = "Thymidine"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Methotrexate]({{< relref "methotrexate" >}}) {#methotrexate--methotrexate-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Decreases [thymidine]({{< relref "thymidine" >}}) synthesis

    ---


#### [Describe the mechanism of action of the listed anti-neoplastics in the context of their specific actions and effect during cell division.]({{< relref "describe_the_mechanism_of_action_of_the_listed_anti_neoplastics_in_the_context_of_their_specific_actions_and_effect_during_cell_division" >}}) {#describe-the-mechanism-of-action-of-the-listed-anti-neoplastics-in-the-context-of-their-specific-actions-and-effect-during-cell-division-dot--describe-the-mechanism-of-action-of-the-listed-anti-neoplastics-in-the-context-of-their-specific-actions-and-effect-during-cell-division-dot-md}

<!--list-separator-->

-  **🔖 From Anti-neoplastics powerpoint > By Mechanism of Action**

    |                 |                                                          |                                                     |                  |
    |-----------------|----------------------------------------------------------|-----------------------------------------------------|------------------|
    | Antimetabolites | [5-fluorouracil]({{< relref "5_fluorouracil" >}}) (5-FU) | ↓ [thymidine]({{< relref "thymidine" >}}) synthesis | Myelosuppression |

    ---

<!--list-separator-->

-  **🔖 From Anti-neoplastics powerpoint > By Mechanism of Action**

    |                 |                                               |                                                     |                  |
    |-----------------|-----------------------------------------------|-----------------------------------------------------|------------------|
    | Antimetabolites | [Methotrexate]({{< relref "methotrexate" >}}) | ↓ [thymidine]({{< relref "thymidine" >}}) synthesis | Myelosuppression |

    ---


#### [5-fluorouracil]({{< relref "5_fluorouracil" >}}) {#5-fluorouracil--5-fluorouracil-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Decreases [thymidine]({{< relref "thymidine" >}}) synthesis

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
