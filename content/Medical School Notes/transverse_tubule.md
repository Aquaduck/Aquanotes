+++
title = "Transverse tubule"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Invaginations formed from [sarcolemma]({{< relref "sarcolemma" >}}) of skeletal muscle cells which extend into the cell


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Terminal cisternae]({{< relref "terminal_cisternae" >}}) {#terminal-cisternae--terminal-cisternae-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Enlarged areas of [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) surrounding [t tubules]({{< relref "transverse_tubule" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Store [Calcium]({{< relref "calcium" >}}) and release it when an action potential courses down the [t tubules]({{< relref "transverse_tubule" >}})

    ---

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Enlarged areas of [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) surrounding [t tubules]({{< relref "transverse_tubule" >}})

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Structure of Skeletal Muscle (p. 109) > Skeletal muscle cells**

    Form deep tubular invaginations called [Transverse (T) tubules]({{< relref "transverse_tubule" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
