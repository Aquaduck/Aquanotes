+++
title = "Hormone"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems**

    [Hormones]({{< relref "hormone" >}}) can be thought of as **signals**, and their receptors as **signal detectors**

    ---

<!--list-separator-->

-  **🔖 Intercellular communication**

    In energy metabolism, most important route is chemical signaling between cells mediated by blood-borne [hormones]({{< relref "hormone" >}})

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Major physiological functions of the kidney**

    <!--list-separator-->

    -  [Hormone]({{< relref "hormone" >}}) synthesis

        -   [Erythropoietin]({{< relref "erythropoietin" >}})
        -   [Calciferol]({{< relref "vitamin_d" >}})
        -   [Prostaglandins]({{< relref "prostaglandin" >}})
        -   [Dopamine]({{< relref "dopamine" >}})
        -   [Renin]({{< relref "renin" >}}) **<- focus mostly on this one in class**


### Unlinked references {#unlinked-references}

[Show unlinked references]
