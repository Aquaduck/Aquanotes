+++
title = "Cell Cycle and Regulation Workshop"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  From [Cell Cycle and Regulation Workshop]({{< relref "cell_cycle_and_regulation_workshop" >}})

    <!--list-separator-->

    -  CDKs and Cyclins

        {{< figure src="/ox-hugo/_20210921_171730screenshot.png" caption="Figure 1: Activation and inactivation of Cyclin-CDK complexes drive cell progression" width="800" >}}

        <!--list-separator-->

        -  [Cyclin-dependent kinases]({{< relref "cyclin_dependent_kinase" >}}) (CDKs)

            -   Regulate cell cycle progression by modulating activity of other proteins via phosphorylation

        <!--list-separator-->

        -  [Cyclins]({{< relref "cyclin" >}})

            -   Family of unstable regulatory subunits of CDKs
            -   Synthesized and degraded at specific times during the cell cycle
            -   Function as **"cell cycle clock"**
            -   Dictate level of activity of [CDKs]({{< relref "cyclin_dependent_kinase" >}}) at specific points in the cell cycle

        <!--list-separator-->

        -  Different CDKs function at different phases of the cell cycle

            | Phase   | CDK/cyclin    | Function                               |
            |---------|---------------|----------------------------------------|
            | G1      | CDK4/cyclin D | pRB phosphorylation, activation of E2F |
            | G1/S    | CDK2/cyclin E | Histone gene expression                |
            |         |               | Centrosome duplication                 |
            |         |               | Origin activation                      |
            | S phase | CDK2/cyclin A | Initation of DNA synthesis             |
            | G2/M    | CDK1/cyclin B | Mitosis                                |

    <!--list-separator-->

    -  Activation/inactivation mechanisms for [CDKs]({{< relref "cyclin_dependent_kinase" >}})

        <!--list-separator-->

        -  Activation of CDKs

            -   CDKs are **activated** by [cyclin]({{< relref "cyclin" >}}) binding and phosphorylation of the T-loop
                -   _Inactive state_
                    -   Active site blocked by T-loop (shown in red)
                -   _Partially active_
                    -   Binding of cyclin -> T-loop moves out of active site -> partial activation
                -   _Fully active_
                    -   Phosphorylation of CDK2 by [CAK]({{< relref "cdk_activating_kinase" >}}) at a threonine residue in T-loop further activates CDK by **changing shape of T-loop** -> improves ability of CDK to bind substrates

            {{< figure src="/ox-hugo/_20210921_173559screenshot.png" caption="Figure 2: Structural basis of CDK activation based on 3D structures of human CDK2 and cyclin A" width="500" >}}

            -   Protein degradation also provides both positive and negative regulation to CDKs

        <!--list-separator-->

        -  Inactivation of CDKs

            -   CDKs are also **inactivated** by phosphorylation
                -   Active cyclin-CDK complex is turned off when [Wee1]({{< relref "wee1" >}}) phosphorylates two closely spaced sites above the active site
                    -   Removal of these phosphates by [Cdc25]({{< relref "cdc25" >}}) activates the cyclin-CDK complex

            {{< figure src="/ox-hugo/_20210921_174512screenshot.png" width="400" >}}

            -   The [Anaphase promoting complex/cyclosome]({{< relref "anaphase_promoting_complex_cyclosome" >}}) (APC/C) **inactivates** CDKs in [M-phase]({{< relref "m_phase" >}}) by ubiquitinating [cyclin B]({{< relref "cyclin_b" >}})

    <!--list-separator-->

    -  Loading of cohesin to link replicated sister chromosomes


### Unlinked references {#unlinked-references}

[Show unlinked references]
