+++
title = "Haldane effect"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The [carbon dioxide]({{< relref "carbon_dioxide" >}}) affinity of [Hemoglobin]({{< relref "hemoglobin" >}}) is **inversely proportional** to the [oxygenation]({{< relref "oxygen" >}}) of hemoglobin


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology**

    <!--list-separator-->

    -  [Haldane effect]({{< relref "haldane_effect" >}})

        -   The carbon dioxide affinity of [Hemoglobin]({{< relref "hemoglobin" >}}) is **inversely proportional** to the oxygenation of hemoglobin


### Unlinked references {#unlinked-references}

[Show unlinked references]
