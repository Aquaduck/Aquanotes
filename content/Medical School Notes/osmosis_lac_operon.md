+++
title = "Osmosis - Lac operon"
author = ["Arif Ahsan"]
date = 2021-07-25T00:00:00-04:00
tags = ["medschool", "osmosis", "source"]
draft = false
+++

## Structural genes {#structural-genes}

-   _lacZ_: \\(\beta\\)-galactosidase (aka lactase)
-   _lacY_: \\(\beta\\)-galactosidase permease
-   _lacA_: \\(\beta\\)-galactosidase transacetylase


## Regulation {#regulation}

-   ↑ glucose -> repressor **stays bound** to operator, blocking RNA polymerase
    -   catabolite activator protein inhibits transcription
-   ↓ glucose -> repressor **falls off**
    -   catabolite activator protein stimulates transcription


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 12. Describe the prokaryotic lac operon and summarize the positive and negative regulation of lac operon transcription.**

    [Osmosis - Lac operon]({{< relref "osmosis_lac_operon" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
