+++
title = "Epidermis"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Stratum spinosum]({{< relref "stratum_spinosum" >}}) {#stratum-spinosum--stratum-spinosum-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Spiny layer of the [epidermis]({{< relref "epidermis" >}})

    ---


#### [Stratum granulosum]({{< relref "stratum_granulosum" >}}) {#stratum-granulosum--stratum-granulosum-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Granular layer of the [epidermis]({{< relref "epidermis" >}})

    ---


#### [Stratum corneum]({{< relref "stratum_corneum" >}}) {#stratum-corneum--stratum-corneum-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Keratin layer of the [epidermis]({{< relref "epidermis" >}})

    ---


#### [Stratum basale]({{< relref "stratum_basale" >}}) {#stratum-basale--stratum-basale-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The lowest layer of the [epidermis]({{< relref "epidermis" >}}) where cell division occurs

    ---


#### [Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}}) {#name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot--name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint**

    <!--list-separator-->

    -  [Epidermis]({{< relref "epidermis" >}}) - strata

        <!--list-separator-->

        -  [Stratum basale]({{< relref "stratum_basale" >}})

            -   Single layer of **cuboidal cells** attached to the basement membrane
            -   New cells are generated from this layer (stem cells)

        <!--list-separator-->

        -  [Stratum spinosum]({{< relref "stratum_spinosum" >}}) - "spiny layer"

            -   Named for the spiny cell-to-cell bridges that are sometimes visible
                -   Correspond to [desmosomes]({{< relref "desmosome" >}})

        <!--list-separator-->

        -  [Stratum granulosum]({{< relref "stratum_granulosum" >}}) - granular layer

            -   [Keratinocytes]({{< relref "keratinocyte" >}}) start to degenerate and make keratin precursors
                -   Keratin-precursors accumulate in the cytoplasm as _keratin-hyaline granules_

        <!--list-separator-->

        -  [Stratum corneum]({{< relref "stratum_corneum" >}}) - keratin layer

            -   Skeletal remains of dead cells that are still held together by desmosomes
            -   [Keratin]({{< relref "keratin" >}}) and thick plasma membranes form a waxy barrier


#### [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}}) {#know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot--know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Overview of Basic Skin > 3 layers**

    <!--list-separator-->

    -  [Epidermis]({{< relref "epidermis" >}})

        <!--list-separator-->

        -  Overview

            -   Keratinized **stratified squamous** epithelium
            -   Protects from UV damage
            -   Waterproof

            {{< figure src="/ox-hugo/_20210921_155711screenshot.png" width="800" >}}

        <!--list-separator-->

        -  Components

            -   Contain [keratinocytes]({{< relref "keratinocyte" >}}) - cells that make large amounts of [keratin]({{< relref "keratin" >}})
                -   Held together by [desmosomes]({{< relref "desmosome" >}})
                    -   Membrane proteins responsible for cell-to-cell adhesion
                    -   Keep the skin from falling apart
                    -   The reason why skin falls off in sheets (e.g. sunburn)
            -   Rests on a [basement membrane]({{< relref "basement_membrane" >}})
                -   Bottom layer of keratinocytes attached by [hemidesmosomes]({{< relref "hemidesmosome" >}})
                    -   Membrane proteins that adhere cells to the basement membrane


#### [Hemidesmosome]({{< relref "hemidesmosome" >}}) {#hemidesmosome--hemidesmosome-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Adheres cells of the [epidermis]({{< relref "epidermis" >}}) to the [basement membrane]({{< relref "basement_membrane" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
