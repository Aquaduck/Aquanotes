+++
title = "Troponin"
author = ["Arif Ahsan"]
date = 2021-09-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain the role of calcium in striated muscle contraction and describe its regulation.]({{< relref "explain_the_role_of_calcium_in_striated_muscle_contraction_and_describe_its_regulation" >}}) {#explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot--explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > A sudden rise in cytosolic Ca<sup>2+</sup> concentration initiates muscle contraction (p. 920)**

    [Troponin]({{< relref "troponin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
