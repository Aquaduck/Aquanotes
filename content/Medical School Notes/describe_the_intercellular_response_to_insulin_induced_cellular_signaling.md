+++
title = "Describe the intercellular response to insulin-induced cellular signaling"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#from-metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}


## [Insulin]({{< relref "insulin" >}})'s metabolic effects {#insulin--insulin-dot-md--s-metabolic-effects}

-   Insulin does not bind to a GPCR
    -   Instead, binds to specific, high-affinity receptors in cell membranes of **liver, striated muscle, adipose tissue, and kidney** -> considered "insulin-sensitive" tissues
-   Insulin-induced changes in activity of several enzymes occur over minutes to hours and reflect changes in phosphorylation states of existing proteins
    -   Over hours to days -> increases **amount** of many enzymes
        -   Reflect an increase in gene expression through increased transcription mediated by specific regulatory element-binding proteins and subsequent translation
-   Crucial aspect of insulin-induced signaling is **activation of enzymes that eliminate glucagon-mediated changes in enzyme activities**
    -   Insulin imediately activates [protein phosphatases]({{< relref "phosphoprotein_phosphatase" >}}) -> remove any phosphates introduced by [PKA]({{< relref "protein_kinase_a" >}}) -> **reverses processes**
    -   E.g. insulin-induced phosphatases stop glycogenolysis and promote glycogen sysnthesis + storage -> ensures glycogen pools are respored in liver and striated muscle
    -   Restores [pyruvate kinase]({{< relref "pyruvate_kinase" >}}) activity -> stops [gluconeogenesis]({{< relref "gluconeogenesis" >}}) by immediately converting any PEP made back to pyruvate
-   Activates [cAMP-phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})
    -   Hydrolyzes cAMP to 5'-AMP -> eliminate second messenger activating PKA -> [PKA]({{< relref "protein_kinase_a" >}}) activity significantly diminished
-   Promotes storage of other nutrients e.g. triacylglycerol and protein + inhibits their mobilization


### Summary of insulin's overall metabolic effects {#summary-of-insulin-s-overall-metabolic-effects}

1.  ↑ Glucose uptake
2.  ↑ Glycogen and protein synthesis
3.  ↓ Gluconeogenesis
4.  ↓ Glycogenolysis
5.  ↓ Fatty acid mobilization
6.  ↑ Fat synthesis in insulin-sensitive tissues _other than the liver_


## From [GLUT4 Exocytosis]({{< relref "glut4_exocytosis" >}}) {#from-glut4-exocytosis--glut4-exocytosis-dot-md}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
