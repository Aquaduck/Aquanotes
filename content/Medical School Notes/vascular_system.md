+++
title = "Vascular system"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Name and describe the pathways of lymphatic drainage (in a lymph node, in the body).]({{< relref "name_and_describe_the_pathways_of_lymphatic_drainage_in_a_lymph_node_in_the_body" >}}) {#name-and-describe-the-pathways-of-lymphatic-drainage--in-a-lymph-node-in-the-body--dot--name-and-describe-the-pathways-of-lymphatic-drainage-in-a-lymph-node-in-the-body-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymph**

    Lymph gradually moves **centrally** in [lymphatic channels]({{< relref "lymphatic_channel" >}}) where it flows into the [vascular system]({{< relref "vascular_system" >}}) at the **base of the neck**

    ---


#### [Langman's Medical Embryology]({{< relref "langman_s_medical_embryology" >}}) {#langman-s-medical-embryology--langman-s-medical-embryology-dot-md}

<!--list-separator-->

-  **🔖 13: Cardiovascular System > Establishment and Patterning of the Primary Heart Field**

    The [vascular system]({{< relref "vascular_system" >}}) appears in the middle of the **third week** when the embryo is no longer able to satisfy its nutritional requirements by diffusion alone

    ---


#### [Hemorrhage]({{< relref "hemorrhage" >}}) {#hemorrhage--hemorrhage-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    When [blood]({{< relref "blood" >}}) leaves the [vascular system]({{< relref "vascular_system" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
