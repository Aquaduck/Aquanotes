+++
title = "5-phosphoribosyl-1-pyrophosphate"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Outline the general features of the pathways of de novo synthesis of the two purine and three pyrimidine ribonucleotides, noting the chemical changes required for conversion of UTP to CTP and IMP to ATP/GTP.]({{< relref "outline_the_general_features_of_the_pathways_of_de_novo_synthesis_of_the_two_purine_and_three_pyrimidine_ribonucleotides_noting_the_chemical_changes_required_for_conversion_of_utp_to_ctp_and_imp_to_atp_gtp" >}}) {#outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot--outline-the-general-features-of-the-pathways-of-de-novo-synthesis-of-the-two-purine-and-three-pyrimidine-ribonucleotides-noting-the-chemical-changes-required-for-conversion-of-utp-to-ctp-and-imp-to-atp-gtp-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Synthesis of purine nucleotides (p. 291) > Synthesis of 5'-phosphoribosylamine**

    [PRPP]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) controls the rate of this reaction

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Synthesis of purine nucleotides (p. 291) > Synthesis of 5'-phosphoribosylamine**

    5'-phosphorybosylamine (PRA) is synthesized from [PRPP]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) and glutamine

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Synthesis of purine nucleotides (p. 291)**

    <!--list-separator-->

    -  Synthesis of [5-phosphoribosyl-1-pyrophosphate]({{< relref "5_phosphoribosyl_1_pyrophosphate" >}}) (PRPP)

        -   PRPP is an "activated pentose" that participates in the **synthesis and salvage of purines and pyrimidines**
        -   PRPP is synthesized from ATP and ribose 5-phosphate and catalyzed by [PRPP synthetase]({{< relref "prpp_synthetase" >}})
            -   PRPP-synthetase is activated by inorganic phosphate and inhibited by purine nucleotides (end-product inhibition)


### Unlinked references {#unlinked-references}

[Show unlinked references]
