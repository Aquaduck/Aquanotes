+++
title = "Proximal tubule"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The first segment of the [renal tubule]({{< relref "renal_tubule" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Proximal straight tubule]({{< relref "proximal_straight_tubule" >}}) {#proximal-straight-tubule--proximal-straight-tubule-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The second component of the [proximal tubule]({{< relref "proximal_tubule" >}})

    ---


#### [Proximal convoluted tubule]({{< relref "proximal_convoluted_tubule" >}}) {#proximal-convoluted-tubule--proximal-convoluted-tubule-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The first component of the [proximal tubule]({{< relref "proximal_tubule" >}})

    ---


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18)**

    <!--list-separator-->

    -  [Proximal tubule]({{< relref "proximal_tubule" >}})

        ([Proximal convoluted tubule]({{< relref "proximal_convoluted_tubule" >}}) + [Proximal straight tubule]({{< relref "proximal_straight_tubule" >}}))

        -   The first segment of the Renal tubule
        -   Drains [Bowman capsule]({{< relref "bowman_capsule" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
