+++
title = "Fatty acyl-carnitine ester"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Carnitine acyltransferase I (CAT-1) and CAT-II > From Fat Metabolism in Muscle & Adipose Tissue > CAT-2**

    [Fatty acyl-carnitine ester]({{< relref "fatty_acyl_carnitine_ester" >}}) becomes a substrate for _carnitine acyltransferase II_ -> catalyzes transfer of acyl to CoA-SH in matrix -> release [carnitine]({{< relref "carnitine" >}}) for a second round of fatty acid transport

    ---

<!--list-separator-->

-  **🔖 Carnitine acyltransferase I (CAT-1) and CAT-II > From Fat Metabolism in Muscle & Adipose Tissue > CAT-1**

    [Fatty acyl-carnitine ester]({{< relref "fatty_acyl_carnitine_ester" >}}) is formed by the action of _carnitine acyltransferase I_ once inside the inner membrane space

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
