+++
title = "Endothelin"
author = ["Arif Ahsan"]
date = 2021-10-18T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Stimulated by:
    -   [Angiotensin II]({{< relref "angiotensin_ii" >}})
    -   [Bradykinin]({{< relref "bradykinin" >}})
    -   [Epinephrine]({{< relref "epinephrine" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  [Endothelin]({{< relref "endothelin" >}}) (p. 1242)

    -   _Endothelins_ are extremely powerful vasoconstrictors
    -   Produced in the endothelial cells of the renal blood vessels, mesangial cells, and cells of the DCT
    -   Stimulated by:
        -   Angiotensin II
        -   Bradykinin
        -   Epinephrine
    -   **Do not typically influence blood pressure in healthy people**
    -   Diminish GFR by damaging podocytes and potently vasoconstricting afferent and efferent arterioles


### Unlinked references {#unlinked-references}

[Show unlinked references]
