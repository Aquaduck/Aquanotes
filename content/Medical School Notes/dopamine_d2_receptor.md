+++
title = "Dopamine D2 receptor"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Carotid & Aortic bodies (p. 671)**

    The principle transmitter appears to be [dopamine]({{< relref "dopamine" >}}) -> excites the nerve endings through [D<sub>2</sub> receptors]({{< relref "dopamine_d2_receptor" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
