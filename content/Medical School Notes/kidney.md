+++
title = "Kidney"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Participates in [Gluconeogenesis]({{< relref "gluconeogenesis" >}})


### Major functions {#major-functions}

1.  Excretion of metabolic waste and foreign substances
2.  Regulation of water and [electrolyte]({{< relref "electrolyte" >}}) balance
3.  Regulation of extracellular fluid volume
4.  Regulation of [plasma]({{< relref "plasma" >}}) [osmolality]({{< relref "osmolality" >}})
5.  Regulation of [RBC]({{< relref "red_blood_cell" >}}) production
6.  Regulation of vascular resistance
7.  Regulation of acid-base balance
8.  Regulation of [vitamin D]({{< relref "vitamin_d" >}}) production


## Backlinks {#backlinks}


### 14 linked references {#14-linked-references}


#### [Tubular cell]({{< relref "tubular_cell" >}}) {#tubular-cell--tubular-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Cells in the [kidney]({{< relref "kidney" >}})

    ---


#### [Renin-angiotensin-aldosterone mechanism]({{< relref "renin_angiotensin_aldosterone_mechanism" >}}) {#renin-angiotensin-aldosterone-mechanism--renin-angiotensin-aldosterone-mechanism-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Ultimately leads to the reabsorption of [sodium]({{< relref "sodium" >}}) by the [kidneys]({{< relref "kidney" >}})

    ---


#### [Renal cortex]({{< relref "renal_cortex" >}}) {#renal-cortex--renal-cortex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The outermost layer of the [renal parenchyma]({{< relref "kidney" >}})

    ---


#### [Outer medulla of the kidney]({{< relref "outer_medulla_of_the_kidney" >}}) {#outer-medulla-of-the-kidney--outer-medulla-of-the-kidney-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [Kidney]({{< relref "kidney" >}})

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Absorption (p. 1144) > Mineral absorption > Calcium**

    [PTH]({{< relref "parathyroid_hormone" >}}) upregulates the activation of [vitamin D]({{< relref "vitamin_d" >}}) in the [kidney]({{< relref "kidney" >}}) -> facilitates intestinal calcium ion absorption

    ---

<!--list-separator-->

-  **🔖 Absorption (p. 1144) > Mineral absorption > Calcium**

    When blood levels of ionic calcium drop -> [PTH]({{< relref "parathyroid_hormone" >}}) secreted -> stimulates **release of calcium ions from bone matrices** + **increases reabsorption of calcium by [kidneys]({{< relref "kidney" >}})**

    ---

<!--list-separator-->

-  **🔖 Antidiuretic Hormone (ADH) (p.746)**

    The target cells of ADH are located in the [tubular cells]({{< relref "tubular_cell" >}}) of the [kidney]({{< relref "kidney" >}})

    ---


#### [Nephron]({{< relref "nephron" >}}) {#nephron--nephron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The main functional component of the [kidney]({{< relref "kidney" >}})

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Albumin-bound fatty acids > From Fat Metabolism in Muscle & Adipose Tissue**

    E.g. [muscle]({{< relref "muscle" >}}) and [kidney]({{< relref "kidney" >}})

    ---


#### [Inner medulla of the kidney]({{< relref "inner_medulla_of_the_kidney" >}}) {#inner-medulla-of-the-kidney--inner-medulla-of-the-kidney-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [Kidney]({{< relref "kidney" >}})

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology**

    <!--list-separator-->

    -  [Renal]({{< relref "kidney" >}}) functions (p. 10)

        <!--list-separator-->

        -  Excretion of metabolic waste and foreign substances

        <!--list-separator-->

        -  Regulation of [water]({{< relref "water" >}}) and electrolyte balance

        <!--list-separator-->

        -  Regulation of extracellular fluid volume

        <!--list-separator-->

        -  Regulation of [plasma]({{< relref "plasma" >}}) osmolality

        <!--list-separator-->

        -  Regulation of [RBC]({{< relref "red_blood_cell" >}}) production

        <!--list-separator-->

        -  Regulation of vascular resistance

        <!--list-separator-->

        -  Regulation of acid-base balance

        <!--list-separator-->

        -  Regulation of vitamin D production

        <!--list-separator-->

        -  Gluconeogenesis

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  Major physiological functions of the [kidney]({{< relref "kidney" >}})

        <!--list-separator-->

        -  Production of [Urine]({{< relref "urine" >}})

            -   **Excretion** of metabolic waste and end-products of [metabolism]({{< relref "metabolism" >}})
            -   **Regulation** of extracellular fluid volume and [osmolality]({{< relref "osmolality" >}})
            -   **Maintenance** of acid-base balance
            -   **Maintenance** of [electrolyte]({{< relref "electrolyte" >}}) concentrations
            -   **Regulation** of blood pressure and blood volume
            -   Participation in **[gluconeogenesis]({{< relref "gluconeogenesis" >}})** ([glutamine]({{< relref "glutamine" >}}) and [glutamate]({{< relref "glutamate" >}})) and **[ketogenesis]({{< relref "ketogenesis" >}})**

        <!--list-separator-->

        -  [Hormone]({{< relref "hormone" >}}) synthesis

            -   [Erythropoietin]({{< relref "erythropoietin" >}})
            -   [Calciferol]({{< relref "vitamin_d" >}})
            -   [Prostaglandins]({{< relref "prostaglandin" >}})
            -   [Dopamine]({{< relref "dopamine" >}})
            -   [Renin]({{< relref "renin" >}}) **<- focus mostly on this one in class**


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Aldosterone**

    Increases the reabsorption of [sodium]({{< relref "sodium" >}}) into the [blood]({{< relref "blood" >}}) by the [kidneys]({{< relref "kidney" >}}) -> **increases reabsorption of water** -> increases blood volume -> raises blood pressure

    ---

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Renin + Angiotensin**

    Specialized cells in the [kidneys]({{< relref "kidney" >}}) found in the [juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}}) respond to decreased blood flow by secreting _renin_ into the blood

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
