+++
title = "Actin"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [microfilament]({{< relref "microfilament" >}})


## Backlinks {#backlinks}


### 8 linked references {#8-linked-references}


#### [Thin myofilament]({{< relref "thin_myofilament" >}}) {#thin-myofilament--thin-myofilament-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Made of [actin]({{< relref "actin" >}})

    ---


#### [Pericyte]({{< relref "pericyte" >}}) {#pericyte--pericyte-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology**

    Contain [actin]({{< relref "actin" >}}), [myosin]({{< relref "myosin" >}}), and [tropomyosin]({{< relref "tropomyosin" >}}) -> suggests they play a role in contraction

    ---


#### [Fimbrin]({{< relref "fimbrin" >}}) {#fimbrin--fimbrin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An accessory protein for [actin]({{< relref "actin" >}})

    ---


#### [Explain the role of calcium in striated muscle contraction and describe its regulation.]({{< relref "explain_the_role_of_calcium_in_striated_muscle_contraction_and_describe_its_regulation" >}}) {#explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot--explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > A sudden rise in cytosolic Ca<sup>2+</sup> concentration initiates muscle contraction (p. 920)**

    In resting muscle, this complex pulls ropomyosin out of its normal binding groove into a position along the [actin]({{< relref "actin" >}}) filament that **interferes with the binding of myosin heads** -> **no force generation**

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > A sudden rise in cytosolic Ca<sup>2+</sup> concentration initiates muscle contraction (p. 920)**

    Calcium dependence of vertebrate skeletal muscle contraction due entirely to a set of specialized accessory proteins closely associated with [actin]({{< relref "actin" >}}) thin filaments:

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Overview - Muscle**

    Muscle cells possess _contractile filaments_ whose major components are [actin]({{< relref "actin" >}}) and [myosin]({{< relref "myosin" >}})

    ---


#### [Alpha-actinin]({{< relref "alpha_actinin" >}}) {#alpha-actinin--alpha-actinin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An accessory protein for [actin]({{< relref "actin" >}})

    ---


#### [Actin related protein]({{< relref "actin_related_protein" >}}) {#actin-related-protein--actin-related-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    45% identical to [actin]({{< relref "actin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
