+++
title = "Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Off Signals {#off-signals}

-   Good control systems must be able to be **turned off rapidly**
-   The time course of approach to a new steady state is **solely a function of the rate constant for degradation**


#### Several pathways in the cell to turn off a signal: {#several-pathways-in-the-cell-to-turn-off-a-signal}

<!--list-separator-->

-  Initiating signal disappears

    -   At the largest scale **levels of signal molecule itself falls** due to engagement of negative feedback mechanisms

<!--list-separator-->

-  Inactivating transducer - G-protein [GTPase]({{< relref "g_protein_gtpase" >}})

    -   Gα-subunit is a "GTPase time bomb"
    -   **GTPase activity increases shortly after G-protein activation**
        -   Essentially, shortly after activation steps have already been set in motion to inactivate the [G-protein]({{< relref "g_protein" >}})

<!--list-separator-->

-  Get rid of second messenger - [phosphodiesterases]({{< relref "phosphodiesterase" >}}) (PDE)

    -   You can **degrade the intracellular signal molecule** e.g. cAMP degradation via [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})

<!--list-separator-->

-  Dephosphorylation

    -   Phosphatases dephosphorylate PKA targets -> no activity

<!--list-separator-->

-  Inactivate the receptor

    -   [PKA]({{< relref "protein_kinase_a" >}}) has an additional action: **phosphorylation of the ser/thr residue of the receptor itself** -> turns receptor off
        -   Creates "docking sites" -> permits binding of [arrestin]({{< relref "arrestin" >}}) -> inhibits receptor


#### Why is the off signal important? {#why-is-the-off-signal-important}

-   Goal: regulate amount of substance or process in the body **as tightly as possible**
-   Being able to turn a signal on and off rapidly **reduces the error signal**


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important.]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}})


#### [Discuss the mechanisms by which phosphorylation is involved in turning off the cAMP signal pathway.]({{< relref "discuss_the_mechanisms_by_which_phosphorylation_is_involved_in_turning_off_the_camp_signal_pathway" >}}) {#discuss-the-mechanisms-by-which-phosphorylation-is-involved-in-turning-off-the-camp-signal-pathway-dot--discuss-the-mechanisms-by-which-phosphorylation-is-involved-in-turning-off-the-camp-signal-pathway-dot-md}

[Inactivate the receptor]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}})

---


### Unlinked references {#unlinked-references}

[Show unlinked references]
