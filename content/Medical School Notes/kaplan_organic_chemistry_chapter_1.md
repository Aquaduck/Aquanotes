+++
title = "Kaplan Organic Chemistry Chapter 1"
author = ["Arif Ahsan"]
date = 2021-07-12T00:00:00-04:00
tags = ["medschool", "kaplan", "source"]
draft = false
+++

## 1.2 Hydrocarbons and Alcohols {#1-dot-2-hydrocarbons-and-alcohols}


### [Alkanes]({{< relref "alkane" >}}) {#alkanes--alkane-dot-md}

-   Simple hydrocarbon molecules with the formula C<sub>n</sub>H<sub>2n + 2</sub>


#### Naming {#naming}

| Number of Carbons | Name    | Structure                                    |
|-------------------|---------|----------------------------------------------|
| 1                 | Methane | ![](/ox-hugo/_20210712_215044screenshot.png) |
| 2                 | Ethane  | ![](/ox-hugo/_20210712_215412screenshot.png) |
| 3                 | Propane | ![](/ox-hugo/_20210712_215444screenshot.png) |
| 4                 | Butane  | ![](/ox-hugo/_20210712_215503screenshot.png) |
| 5                 | Pentane | ![](/ox-hugo/_20210712_215525screenshot.png) |

And so on...


### [Alkenes and Alkynes]({{< relref "functional_group" >}}) {#alkenes-and-alkynes--functional-group-dot-md}

-   Double and triple bonds, respectively


### [Alcohols]({{< relref "alcohol" >}}) {#alcohols--alcohol-dot-md}


#### Overview {#overview}

-   Named by replacing `-e` at the end of the corresponding alkane with the suffix `-ol`
-   Chain is numbered so that carbon attached to hydroxyl group `(-OH)` gets lowest possible number
-   Hydroxyl group takes precedence over multiple bonds because of the higher oxidation state of the carbon
-   If alcohol is not highest-priority group on molecule, it is named as a hydroxyl substituent `(hydroxy-)`


#### Examples {#examples}

{{< figure src="/ox-hugo/_20210712_215858screenshot.png" >}}


## 1.3 Aldehydes and Ketones {#1-dot-3-aldehydes-and-ketones}

-   Both contain a _carbonyl group_


### [Aldehydes]({{< relref "aldehyde" >}}) {#aldehydes--aldehyde-dot-md}

-   Carbonyl group **at the end of a carbon chain**
-   Usually attached to carbon 1
-   Aldehydes are named by replacing the \\(-e\\) of the parent alkane with the suffix \\(-al\\)

{{< figure src="/ox-hugo/_20210714_193911screenshot.png" >}}


### [Ketones]({{< relref "ketone" >}}) {#ketones--ketone-dot-md}

-   Carbonyl group **in the middle of a carbon chain**
    -   Thus, we always have to assign a number to it
-   Ketones are named by replacing the \\(-e\\) of the parent alkane with the suffix \\(-one\\)
-   When the carbonyl is not the highest priority, its prefix is \\(-oxo\\) or \\(-keto\\)

{{< figure src="/ox-hugo/_20210714_193925screenshot.png" >}}


## 1.4 Carboxylic Acids and Derivatives {#1-dot-4-carboxylic-acids-and-derivatives}


### [Carboxylic Acids]({{< relref "carboxylic_acid" >}}) {#carboxylic-acids--carboxylic-acid-dot-md}

-   Contain both a carbonyl group and a hydroxyl group on a terminal carbon
-   Terminal functional group -> associated carbon numbered 1 unless superceded
-   The most oxidized functional group on the MCAT
    -   3 bonds to oxygen
    -   Thus, often the highest priority functional group

{{< figure src="/ox-hugo/_20210714_192616screenshot.png" >}}


### [Esters]({{< relref "ester" >}}) {#esters--ester-dot-md}

-   Replace -OH of carboxylic acid with -OR (where R is a hydrocarbon chain)
-   Naming convention based on those for carboxylic acids but in 2 parts:
    1.  First term is the **alkyl name of the esterifying group (R)**
    2.  Second term is name of the parent acid, replacing _-oic_ with _-oate_

{{< figure src="/ox-hugo/_20210714_194652screenshot.png" >}}


### [Amides]({{< relref "amide" >}}) {#amides--amide-dot-md}

-   Replace -OH with _amino group_ (nitrogen-containing group)
    -   Amino N can be bonded to 0, 1, or 2 alkyl groups
-   Naming is same as esters, except that the suffix becomes _-amide_

{{< figure src="/ox-hugo/_20210714_194918screenshot.png" >}}


### [Anhydrides]({{< relref "anhydride" >}}) {#anhydrides--anhydride-dot-md}

-   Two carboxylic acids joined together via removal of water
-   Many are cyclic
    -   May result from intramolecular dehydration of a dicarboxylic acid
-   **If anhydride formed from only one type of carboxylic acid**: named by replacing _acid_ with _anhydride_ in the name of the corresponding carboxylic acid
-   \*If anhydride not symmetrical, both carboxylic acids are named (without the _acid_) before _anhydride_ is added to the name
    ![](/ox-hugo/_20210714_205613screenshot.png)


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
