+++
title = "Osteoclast"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Resorbs [Bone]({{< relref "bone" >}}) and digests the [Osteoid]({{< relref "osteoid" >}})
-   Resides in [Howship's lacunae]({{< relref "howship_s_lacunae" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    A wave of [osteoblasts]({{< relref "osteoblast" >}}) follow the [osteoclasts]({{< relref "osteoclast" >}}) -> lay down concentric rings of [Osteoid]({{< relref "osteoid" >}}) -> osteoblasts differentiate into [osteocytes]({{< relref "osteocyte" >}}) -> encased in bone by the next wave of osteoblasts

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    A group of [osteoclasts]({{< relref "osteoclast" >}}) at the "cutting" cone resorb a round channel of bone about 200 microns in diameter while traveling parallel to the direction of the major stress on the bone

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Cytologic components of mature bone**

    [Osteoclasts]({{< relref "osteoclast" >}}) - resorb bone and digest the osteoid

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
