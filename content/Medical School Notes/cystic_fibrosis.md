+++
title = "Cystic fibrosis"
author = ["Arif Ahsan"]
date = 2021-10-17T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Rationalize potential drug action with molecular defects and genomics.]({{< relref "rationalize_potential_drug_action_with_molecular_defects_and_genomics" >}}) {#rationalize-potential-drug-action-with-molecular-defects-and-genomics-dot--rationalize-potential-drug-action-with-molecular-defects-and-genomics-dot-md}

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face**

    <!--list-separator-->

    -  Drugs treating [cystic fibrosis]({{< relref "cystic_fibrosis" >}})

        -   Three oral drugs approved to treat cystic fibrosis:
            1.  _Ivacaftor_
                -   Potentiator: improves [CFTR]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}}) channel opening -> more ions flow through pore
            2.  _Tezacaftor_
                -   Corrector: improve CFTR protein folding and trafficking -> more mature CFTR appears at cell membrane
            3.  _Lumacaftor_
                -   Same as _Tezacaftor_
        -   Treatment regimens:
            1.  Kalydeco (ivacaftor)
            2.  Orkambi (lumacaftor + ivacaftor)
            3.  Symkevi, Symdeko (tezacaftor + ivacaftor)


#### [Explain how the defect leads to dysfunction in the lungs.]({{< relref "explain_how_the_defect_leads_to_dysfunction_in_the_lungs" >}}) {#explain-how-the-defect-leads-to-dysfunction-in-the-lungs-dot--explain-how-the-defect-leads-to-dysfunction-in-the-lungs-dot-md}

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face**

    <!--list-separator-->

    -  [CF]({{< relref "cystic_fibrosis" >}}) class mutations of [CFTR]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}})

        |                    | Class 1          | Class 2        | Class 3                  | Class 4                  | Class 5          | Class 6           | Class 7     |
        |--------------------|------------------|----------------|--------------------------|--------------------------|------------------|-------------------|-------------|
        | CFTR defect        | No protein       | No traffic     | Impaired gating          | Decreased conductance    | Less protein     | Less stable       | No mRNA     |
        | Corrective therapy | Rescue synthesis | Rescue traffic | Restore channel activity | Restore channel activity | Correct splicing | Promote stability | Unrescuable |

        -   **Note**: Mutation classification is an oversimplification, as most mutations often contain traits of multiple classes

        <!--list-separator-->

        -  Class I Mutations

            -   Near absence of CFTR protein
            -   Mainly stop codon mutations and frameshift mutations -> premature stop codons

        <!--list-separator-->

        -  Class II Mutations

            -   Defective processing and trafficking of the CFTR protein -> degraded in proteasome -> amount of CFTR protein at apical membrane severely reduced

        <!--list-separator-->

        -  Class III Mutations

            -   CFTR protein reaches cell membrane, but defective regulation of CFTR gating -> no channel opening

        <!--list-separator-->

        -  Class IV Mutations

            -   Impaired conductance of the CFTR channel -> fewer ions passing through open channel pore

        <!--list-separator-->

        -  Class V Mutations

            -   Often alternative splice mutations
            -   A reduced amount of normal CFTR protein

        <!--list-separator-->

        -  Class VI Mutations

            -   Unstable CFTR protein -> prematurely recycled from apical membrane -> degraded in lysosomes

        <!--list-separator-->

        -  Class VII Mutations

            -   Large deletions and frameshift mutations not easily amenable to pharmacotherapy


#### [Describe the molecular defect that leads to CF.]({{< relref "describe_the_molecular_defect_that_leads_to_cf" >}}) {#describe-the-molecular-defect-that-leads-to-cf-dot--describe-the-molecular-defect-that-leads-to-cf-dot-md}

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face**

    <!--list-separator-->

    -  [Cystic fibrosis]({{< relref "cystic_fibrosis" >}})

        -   Cystic fibrosis is caused by mutations in the [cystic fibrosis transmembrane conductance regulator]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}})
        -   [CFTR]({{< relref "cystic_fibrosis_transmembrane_conductance_regulator" >}}) is an anion channel that:
            -   Conducts conducting chloride and [bicarbonate]({{< relref "bicarbonate" >}}) at the apical membrane of different epithelia
            -   Regulates water and ion transport
            -   Maintains epithelial surface hydration
        -   [Bicarbonate]({{< relref "bicarbonate" >}}) release in the airway is important for the proper unfolding of [mucins]({{< relref "mucin" >}}) and defending against bacteria
        -   [Bicarbonate]({{< relref "bicarbonate" >}}) in the intestines is needed to buffer gastric acidity and enable the activation of pancreatic enzymes


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Obstructive lung disease > Etiology**

    Bronchiectasis, [cystic fibrosis]({{< relref "cystic_fibrosis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
