+++
title = "Cyclin B"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A [cyclin]({{< relref "cyclin" >}}) involved in [mitosis]({{< relref "mitosis" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Dephosphorylation Activates M-Cdk at the Onset of Mitosis**

    [Cdk1]({{< relref "cdk1" >}}) forms a complex with [M-cyclin]({{< relref "cyclin_b" >}}) -> formation of M-Cdk complex as the cell approaches mitosis

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Dephosphorylation Activates M-Cdk at the Onset of Mitosis**

    M-Cdk activation begins with the accumulation of [M-cyclin]({{< relref "cyclin_b" >}})

    ---

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop > Activation/inactivation mechanisms for CDKs > Inactivation of CDKs**

    The [Anaphase promoting complex/cyclosome]({{< relref "anaphase_promoting_complex_cyclosome" >}}) (APC/C) **inactivates** CDKs in [M-phase]({{< relref "m_phase" >}}) by ubiquitinating [cyclin B]({{< relref "cyclin_b" >}})

    ---


#### [Anaphase promoting complex/cyclosome]({{< relref "anaphase_promoting_complex_cyclosome" >}}) {#anaphase-promoting-complex-cyclosome--anaphase-promoting-complex-cyclosome-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inactivates [CDKs]({{< relref "cyclin_dependent_kinase" >}}) in [M-phase]({{< relref "m_phase" >}}) by ubiquitinating [cyclin B]({{< relref "cyclin_b" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
