+++
title = "Whooping cough"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the basic mechanisms that cause the symptoms of cholera and whooping cough.]({{< relref "describe_the_basic_mechanisms_that_cause_the_symptoms_of_cholera_and_whooping_cough" >}}) {#describe-the-basic-mechanisms-that-cause-the-symptoms-of-cholera-and-whooping-cough-dot--describe-the-basic-mechanisms-that-cause-the-symptoms-of-cholera-and-whooping-cough-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Pertussis Inhibits G<sub>i</sub>**

    [Whooping cough]({{< relref "whooping_cough" >}}) is caused by Bordetella Pertussis

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
