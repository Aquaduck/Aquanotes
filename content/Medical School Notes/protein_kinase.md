+++
title = "Protein kinase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Phosphoprotein phosphatase]({{< relref "phosphoprotein_phosphatase" >}}) {#phosphoprotein-phosphatase--phosphoprotein-phosphatase-dot-md}

<!--list-separator-->

-  **🔖 Concept link**

    Reverses the action of [protein kinases]({{< relref "protein_kinase" >}})

    ---


#### [Intermediary Metabolism and its Regulation]({{< relref "intermediary_metabolism_and_its_regulation" >}}) {#intermediary-metabolism-and-its-regulation--intermediary-metabolism-and-its-regulation-dot-md}

<!--list-separator-->

-  **🔖 Covalent modification**

    Catalyzed by [Protein kinases]({{< relref "protein_kinase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
