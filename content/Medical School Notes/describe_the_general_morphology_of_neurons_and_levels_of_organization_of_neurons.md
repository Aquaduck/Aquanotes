+++
title = "Describe the general morphology of neurons and levels of organization of neurons."
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}}) {#from-brs-cell-biology-and-histology--brs-cell-biology-histology-dot-md}


### III. Cells of nervous system (p. 127) {#iii-dot-cells-of-nervous-system--p-dot-127}

-   [Neurons]({{< relref "neuron" >}}) consist of a cell body and its processes, which usually include multiple [dendrites]({{< relref "dendrite" >}}) and a single [axon]({{< relref "axon" >}})


#### Morphologic classification of neurons {#morphologic-classification-of-neurons}

-   [Unipolar neurons]({{< relref "unipolar_neuron" >}}) possess a single process but are rare in vertebrates
-   [Bipolar neurons]({{< relref "bipolar_neuron" >}}) possess a single axon and a single dendrite
    -   Present in some sense organs (e.g. vestibular-cochlear mechanism)
-   [Multipolar neurons]({{< relref "multipolar_neuron" >}}) possess a single axon and more than one dendrite
    -   **The most common type of neuron in vertebrates**
-   [Pseudounipolar neurons]({{< relref "pseudounipolar_neuron" >}}) possess a signle process that extends from the cell body and subsequently **branches** into an axon and dendrite
    -   Present in spinal and cranial ganglia
    -   Originate embryologically as bipolar cells -> axon and dendrite fuse into a single process functionally categorized as an axon
    -   Often referred to as just "unipolar" neuron


#### Functional classification of neurons {#functional-classification-of-neurons}

-   [Sensory neurons]({{< relref "sensory_neuron" >}}) **receive stimuli** from the internal and external environments
    -   Conduct impulses **to the CNS** for processing and analysis
-   [Interneurons]({{< relref "interneuron" >}}) connect other neurons in a chain or sequence
    -   Commonly connect sensory and motor neurons
    -   They also regulate signals transmitted to neurons
-   [Motor neurons]({{< relref "motor_neuron" >}}) conduct impulses **from the CNS** to other neurons, muscles, and glands


#### [Neuron]({{< relref "neuron" >}}) structure {#neuron--neuron-dot-md--structure}

<!--list-separator-->

-  Neuronal cell body

    -   Region of a neuron containing the nucleus, various cytoplasmic organelles and inclusions, and cytoskeletal components

    <!--list-separator-->

    -  Nucleus

        -   Large, spherical, pale staining
        -   **Centrally** located in the soma of most neurons
        -   Contains abundant euchromatin and a large nucleolus

    <!--list-separator-->

    -  Cytoplasmic organelles and inclusions

        -   [Nissl bodies]({{< relref "nissl_body" >}}) are composed of polysomes and RER
            -   Appear as clumps under light microscopy
            -   Most abundant in **large motor proteins**
        -   _Golgi complex_ is near the nucleus, and _mitochondria_ are scattered throughout the cytoplasm
        -   _Melanin-containing granules_ are present in some neurons in the CNS and in the dorsal root and sympathetic ganglia
        -   _Lipofuscin-containing granules_ are present in some neurons
            -   Increase in number with age
        -   _Lipid droplets_ occasionally present

    <!--list-separator-->

    -  Cytoskeletal components

        -   [Neurofilaments]({{< relref "neurofilament" >}}) run throughout the soma cytoplasm
            -   Are [intermediate filaments]({{< relref "intermediate_filament" >}}) composed of three intertwining polypeptide chains
        -   _Microtubules_ are also present in the soma cytoplasm
        -   _Microfilaments_ are associated with the plasma membrane

<!--list-separator-->

-  [Dendrites]({{< relref "dendrite" >}})

    -   Receive stimuli from sensory cells, axons, or other neurons -> convert these signals into action potentials transmitted **towards the soma**
    -   Lacks a Golgi complex
    -   Organelles are reduced or absent near terminals **except for mitochondria**, which are abundant
    -   Spines on surface of dendrites increase surface area for synapse formation
        -   Diminish with age and poor neutrition

<!--list-separator-->

-  [Axons]({{< relref "axon" >}})

    -   Conduct impulses **away from** the soma to axon terminals **without any diminuition in their strength**
    -   Originate from the [axon hillock]({{< relref "axon_hillock" >}})
        -   A specialized region of the soma that lacks RER, ribosomes, Golgi cisternae, and Nissle bodies
        -   Contains many microtubules and neurofilaments
            -   [Neurofilaments]({{< relref "neurofilament" >}}) regulate axon diameter
            -   Permits passage of mitochondria and vesicles into the axon
    -   _Axon cytoplasm_ lacks a Golgi complex but contains SER, RER, and elongated mitochondria
    -   Terminate in many small branches ([axon terminals]({{< relref "axon_terminal" >}})) from which impulses are passed to another neuron or type of cell(s)


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-09 Thu] </span></span> > Cells of the Nervous System > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the cellular and ultrastructural morphology of neurons and know the function of neuronal organelles and membrane components.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}})

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-09 Thu] </span></span> > Cells of the Nervous System > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Identify specific classes of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}})

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-09 Thu] </span></span> > Cells of the Nervous System > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Define the ways in which neurons are categorized into groups.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}})

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-09 Thu] </span></span> > Cells of the Nervous System > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the general morphology of neurons and levels of organization of neurons.]({{< relref "describe_the_general_morphology_of_neurons_and_levels_of_organization_of_neurons" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
