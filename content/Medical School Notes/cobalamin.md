+++
title = "Cobalamin"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   AKA _Vitamin B<sub>12</sub>_
-   [B vitamin]({{< relref "b_vitamin" >}})


## From First Aid Section II Biochemistry Nutrition {#from-first-aid-section-ii-biochemistry-nutrition}


### Function {#function}

-   Cofactor for _methionine synthase_ (transfers CH<sub>3</sub> groups as methylcobalamin) and _methylmalonyl-CoA mutase_
-   Important for DNA synthesis
-   Found in animal products
-   Synthesized only by microorganisms
-   Very large reserve pool (several years) stored primarily in the [liver]({{< relref "liver" >}})

    {{< figure src="/ox-hugo/_20210729_230305screenshot.png" width="500" >}}


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}}) {#methylmalonyl-coa-mutase--methylmalonyl-coa-mutase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Dependent on vitamin [B12]({{< relref "cobalamin" >}})

    ---


#### [Methylmalonic acid]({{< relref "methylmalonic_acid" >}}) {#methylmalonic-acid--methylmalonic-acid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Increased [methylmalonic acid] is indicative of a [vitamin B12]({{< relref "cobalamin" >}}) deficiency

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Methylmalonic acid > From Fat Metabolism in Muscle & Adipose Tissue**

    Increased [methylmalonic acid] is indicative of a [vitamin B12]({{< relref "cobalamin" >}}) deficiency

    ---

<!--list-separator-->

-  **🔖 L-methylmalonyl-CoA > From Fat Metabolism in Muscle & Adipose Tissue**

    L-methylmalonyl-CoA converted to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via [B12]({{< relref "cobalamin" >}}) dependent isomerization catalyzed by [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})

    ---

<!--list-separator-->

-  **🔖 Beta-oxidation > From Fat Metabolism in Muscle & Adipose Tissue**

    **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Fatty acid catabolism > Oxidation phase**

    **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**

    ---


#### [Detail the vitamin requirements for the metabolism of propionyl-CoA.]({{< relref "detail_the_vitamin_requirements_for_the_metabolism_of_propionyl_coa" >}}) {#detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot--detail-the-vitamin-requirements-for-the-metabolism-of-propionyl-coa-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Vitamin requirements for the metabolism of propionyl-CoA**

    [B12]({{< relref "cobalamin" >}}): Isomerization of [L-methylmalonyl-CoA]({{< relref "l_methylmalonyl_coa" >}}) to [succinyl-CoA]({{< relref "succinyl_coa" >}}) via [Methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
