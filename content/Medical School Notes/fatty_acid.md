+++
title = "Fatty acid"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The main structural component of [lipids]({{< relref "lipid" >}})
-   Fasting state -> fatty acids enter the [liver]({{< relref "liver" >}}) as the energy source needed to drive [gluconeogenesis]({{< relref "gluconeogenesis" >}})


## Backlinks {#backlinks}


### 15 linked references {#15-linked-references}


#### [β-oxidation]({{< relref "β_oxidation" >}}) {#β-oxidation--β-oxidation-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    First stage in the oxidation of [fatty acids]({{< relref "fatty_acid" >}}) in the [mitochondria]({{< relref "mitochondrion" >}})

    ---


#### [Protein kinase A]({{< relref "protein_kinase_a" >}}) {#protein-kinase-a--protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    PKA-catalyzed phosphorylation of [perilipin]({{< relref "perilipin" >}}) and [HSL]({{< relref "hormone_sensitive_lipase" >}}) is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [Perilipin]({{< relref "perilipin" >}}) {#perilipin--perilipin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [PKA]({{< relref "protein_kinase_a" >}})-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Beta-oxidation > From Fat Metabolism in Muscle & Adipose Tissue**

    First stage of oxidation of [fatty acids]({{< relref "fatty_acid" >}})

    ---

<!--list-separator-->

-  **🔖 Perilipin > From Fat Metabolism in Muscle & Adipose Tissue**

    Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of 2,3-DAG**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Mobilization of triacylglycerol stored in adipocytes**

    Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    [Fatty acids]({{< relref "fatty_acid" >}}) will bind to various _fatty acid binding proteins_ -> able to traverse the cell and enter the tissue beneath

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    LPL will cleave the triacylglycerol to **either** _three [fatty acids]({{< relref "fatty_acid" >}}) and glycerol_ or _two fatty acids and a [monoacylglycerol]({{< relref "monoacylglycerol" >}})_

    ---


#### [Lipolysis]({{< relref "lipolysis" >}}) {#lipolysis--lipolysis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The breakdown of [triglycerides]({{< relref "triacylglycerol" >}}) into [glycerol]({{< relref "glycerol" >}}) and [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [Hormone-sensitive lipase]({{< relref "hormone_sensitive_lipase" >}}) {#hormone-sensitive-lipase--hormone-sensitive-lipase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [PKA]({{< relref "protein_kinase_a" >}})-catalyzed phosphorylation of perilipin and HSL is essential for mobilizing the stored [fatty acids]({{< relref "fatty_acid" >}})

    ---


#### [Explain how fatty acids released from adipocyte stores are utilized.]({{< relref "explain_how_fatty_acids_released_from_adipocyte_stores_are_utilized" >}}) {#explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot--explain-how-fatty-acids-released-from-adipocyte-stores-are-utilized-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > How fatty acids released from adipocyte stores are utilized**

    Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**

    ---

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue**

    <!--list-separator-->

    -  How [fatty acids]({{< relref "fatty_acid" >}}) released from [adipocyte]({{< relref "adipose_cell" >}}) stores are utilized

        -   Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**
        -   Phosphorylation of HSL -> cleaves 2,3-DAG to a fatty acid + monoacylglycerol -> monoacylglycerol lipase releases fatty acid -> formation of glycerol
        -   **Ultimately, 3 fatty acids are mobilized/liberated and glycerol is formed**
            -   Fatty acids exit adipose tissue via diffusion and bind reversibly to plasma [albumin]({{< relref "albumin" >}})
            -   Glycerol taken up by the liver


#### [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}}) {#explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot--explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Delivering dietary fat to extrahepatic tissues via chylomicrons**

    [Fatty acids]({{< relref "fatty_acid" >}}) will bind to various _fatty acid binding proteins_ -> able to traverse the cell and enter the tissue beneath

    ---

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Delivering dietary fat to extrahepatic tissues via chylomicrons**

    LPL will cleave the triacylglycerol to **either** _three [fatty acids]({{< relref "fatty_acid" >}}) and glycerol_ or _two fatty acids and a [monoacylglycerol]({{< relref "monoacylglycerol" >}})_

    ---


#### [Describe the role of protein kinase A in mobilizing fat from adipocyte stores.]({{< relref "describe_the_role_of_protein_kinase_a_in_mobilizing_fat_from_adipocyte_stores" >}}) {#describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot--describe-the-role-of-protein-kinase-a-in-mobilizing-fat-from-adipocyte-stores-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > The role of PKA in mobilizing fat from adipocytes**

    Phosphorylation of perilipin -> allows triglyceride lipase access to triacylglycerol within lipid droplet -> **catalyzes release of a [fatty acid]({{< relref "fatty_acid" >}}) + formation of [2,3-DAG]({{< relref "2_3_diacylglycerol" >}})**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
