+++
title = "Team-Based Learning"
author = ["Arif Ahsan"]
date = 2021-07-12T00:00:00-04:00
tags = ["medschool"]
draft = false
+++

## Introductions {#introductions}


### Faculty {#faculty}

-   Dr. Stephen Everse
-   Dr. Paula Tracy


### M2 TAs {#m2-tas}

-   Matt Breseman
-   Sadie Casale
-   Amanda Galenkamp
-   Elie Kaadi


### Special Guest M2 TAs for this class {#special-guest-m2-tas-for-this-class}

-   Tayler Drake
-   John Fernan
-   Simran Kalsi
-   W. Evan Shaw


## iRAT and gRAT {#irat-and-grat}

-   _iRAT_: first "pre-quiz" everyone takes individually to test knowledge
    -   Not worth much for grade
-   _gRAT_: work together as group to find best answer
    -   Can change answers, but lose points per every changed answer
-   When 75 of the gRAT cards/sheets have been returned, 3 more minutes announced
-   In standard TBL the facilitator reviews key concepts that Teams struggled on


## Applications {#applications}

-   "Meat" of the TBL
-   A page with ~5 Qs will appear in VICportal or open specific packet in folder
-   At table or Breakout rooms, you have 10-15 min to work on the answers together
-   Decide answer **before** any discussion begins
-   Once Team decides on answers, Team Leader records them and prepares to defend them in the large group
-   If every Team finishes early, we will end the breakout room early


### Application Answers: {#application-answers}

1.  A
2.  B
3.  B \*unsure -> change to D
4.  C \*unsure -> change to D
5.  D


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
