+++
title = "Second mitochondria-derived activator of caspases"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A [mitochondrial]({{< relref "mitochondrion" >}}) protein
-   Binds [IAPs]({{< relref "inhibitor_of_apoptosis" >}}) to free [caspases]({{< relref "caspase" >}}) and activate apoptosis


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Smac, a Mitochondrial Protein that Promotes Cytochrome c–Dependent Caspase Activation by Eliminating IAP Inhibition]({{< relref "smac_a_mitochondrial_protein_that_promotes_cytochrome_c_dependent_caspase_activation_by_eliminating_iap_inhibition" >}}) {#smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition--smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition-dot-md}

<!--list-separator-->

-  **🔖 Discussion**

    <!--list-separator-->

    -  Regulation of [SMAC]({{< relref "second_mitochondria_derived_activator_of_caspases" >}})

        -   SMAC is a mitochondrial protein with a typical amphipathic mitochondrial targeting sequence at its N terminus -> cleaved after mitochondrial entry -> activation
        -   Only mature SMAC has caspase activation promoting activity
            -   Thus, requires a maturation process inside mitochondria before gaining its apoptotic activity
        -   Since SMAC is only released during apoptosis, the major regulatory step for SMAC should be its release from the mitochondria
            -   This process is likely to be controlled by the [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}}) of proteins
            -   This is analogous to the requirement for mitochondrial processing of [cytochrome c]({{< relref "cytochrome_c" >}}), which promotes caspase activation after release from the mitochondrial intermembrnae space

<!--list-separator-->

-  **🔖 Discussion**

    <!--list-separator-->

    -  [SMAC]({{< relref "second_mitochondria_derived_activator_of_caspases" >}}) promotes [procaspase-9]({{< relref "procaspase_9" >}}) activation by countering IAPs

        -   SMAC is released from mitochondria during apoptosis -> neutralizes the inhibitory activity of [IAPs]({{< relref "inhibitor_of_apoptosis" >}})


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  [SMAC]({{< relref "second_mitochondria_derived_activator_of_caspases" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
