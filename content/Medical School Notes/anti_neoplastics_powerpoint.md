+++
title = "Anti-neoplastics powerpoint"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the mechanism of action of the listed anti-neoplastics in the context of their specific actions and effect during cell division.]({{< relref "describe_the_mechanism_of_action_of_the_listed_anti_neoplastics_in_the_context_of_their_specific_actions_and_effect_during_cell_division" >}}) {#describe-the-mechanism-of-action-of-the-listed-anti-neoplastics-in-the-context-of-their-specific-actions-and-effect-during-cell-division-dot--describe-the-mechanism-of-action-of-the-listed-anti-neoplastics-in-the-context-of-their-specific-actions-and-effect-during-cell-division-dot-md}

<!--list-separator-->

-  From [Anti-neoplastics powerpoint]({{< relref "anti_neoplastics_powerpoint" >}})

    <!--list-separator-->

    -  By Mechanism of Action

        | Mechanism of Action                                    | Drugs                                                                          | Mechanism                                                     | Toxicity (Treatment)                 |
        |--------------------------------------------------------|--------------------------------------------------------------------------------|---------------------------------------------------------------|--------------------------------------|
        | Antimetabolites                                        | [Methotrexate]({{< relref "methotrexate" >}})                                  | ↓ [thymidine]({{< relref "thymidine" >}}) synthesis           | Myelosuppression                     |
        | Antimetabolites                                        | [5-fluorouracil]({{< relref "5_fluorouracil" >}}) (5-FU)                       | ↓ [thymidine]({{< relref "thymidine" >}}) synthesis           | Myelosuppression                     |
        | Antimetabolites                                        | [6-mercaptopurine]({{< relref "6_mercaptopurine" >}}) (6-MP) (HGPRT-dependent) | ↓ purine synthesis                                            | Myelosuppression                     |
        | DNA Damage                                             | [Cyclophosphamide]({{< relref "cyclophosphamide" >}})                          | Alkylate/Crosslink DNA                                        | M + hemorrhagic cystitis (MESNA)     |
        | DNA Damage                                             | [Cisplatin]({{< relref "cisplatin" >}})                                        | Alkylate/Crosslink DNA                                        | M + Oto-, nephro- and neuro-toxicity |
        | DNA Damage                                             | [Bleomycin]({{< relref "bleomycin" >}})                                        | Oxidative DNA damage                                          | Pulmonary toxicity and skin effects  |
        | DNA Damage                                             | [Doxorubicin]({{< relref "doxorubicin" >}})                                    | Oxidative DNA damage                                          | M + Cardiotoxicity                   |
        | Topo inhibitors                                        | [Irinotecan]({{< relref "irinotecan" >}})                                      | Inhibit [topoisomerase I]({{< relref "topoisomerase_i" >}})   | M + Diarrhea                         |
        | Topo inhibitors                                        | [Etoposide]({{< relref "etoposide" >}})                                        | Inhibit [topoisomerase II]({{< relref "topoisomerase_ii" >}}) | Myelosuppression                     |
        | Topo inhibitors                                        | [Doxorubicin]({{< relref "doxorubicin" >}})                                    | Inhibit [topoisomerase II]({{< relref "topoisomerase_ii" >}}) | M + Cardiotoxicity                   |
        | [Microtubule]({{< relref "microtubule" >}}) inhibitors | [Vincristine]({{< relref "vincristine" >}})                                    | Inhibits MT formation                                         | Peripheral neuropathy                |
        | [Microtubule]({{< relref "microtubule" >}}) inhibitors | [Paclitaxel]({{< relref "paclitaxel" >}})                                      | Inhibits MT disassembly                                       | M + Peripheral neuropathy            |
        |                                                        |                                                                                |                                                               |                                      |


### Unlinked references {#unlinked-references}

[Show unlinked references]
