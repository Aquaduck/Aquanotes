+++
title = "Glycogen phosphorylase"
author = ["Arif Ahsan"]
date = 2021-09-07T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Used in [glycogenolysis]({{< relref "glycogenolysis" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Pyridoxine]({{< relref "pyridoxine" >}}) {#pyridoxine--pyridoxine-dot-md}

<!--list-separator-->

-  **🔖 From First Aid 2015 > Function**

    [Glycogen phosphorylase]({{< relref "glycogen_phosphorylase" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
