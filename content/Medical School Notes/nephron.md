+++
title = "Nephron"
author = ["Arif Ahsan"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The main functional component of the [kidney]({{< relref "kidney" >}})


## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Renal tubule]({{< relref "renal_tubule" >}}) {#renal-tubule--renal-tubule-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [Nephron]({{< relref "nephron" >}})

    ---


#### [Renal corpuscle]({{< relref "renal_corpuscle" >}}) {#renal-corpuscle--renal-corpuscle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [Nephron]({{< relref "nephron" >}})

    ---


#### [Loop of Henle]({{< relref "loop_of_henle" >}}) {#loop-of-henle--loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Component of the [Nephron]({{< relref "nephron" >}}) responsible for reabsorption of [water]({{< relref "water" >}}) and [sodium]({{< relref "sodium" >}})

    ---


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

The [Nephron]({{< relref "nephron" >}}) is the functional unit of the kidney composed of the [Renal corpuscle]({{< relref "renal_corpuscle" >}}) and a [Renal tubule]({{< relref "renal_tubule" >}})

---


#### [Cortical collecting duct]({{< relref "cortical_collecting_duct" >}}) {#cortical-collecting-duct--cortical-collecting-duct-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [Nephron]({{< relref "nephron" >}}) that follows the [connecting tubule]({{< relref "connecting_tubule" >}})

    ---


#### [Connecting tubule]({{< relref "connecting_tubule" >}}) {#connecting-tubule--connecting-tubule-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [Nephron]({{< relref "nephron" >}}) that follows the [Distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Distal convoluted tubule**

    Segment of the [nephron]({{< relref "nephron" >}}) located between the ascending loop of Henle and the connecting tubule and collecting duct

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
