+++
title = "Afterload"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The force of resistance the ventricle must overcome to empty contents at the beginning of [systole]({{< relref "systole" >}})
-   Changes in afterload will shift the [Frank-Starling]({{< relref "frank_starling_law" >}}) curve


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Aortic stenosis and chronic hypertension are examples of pathologic conditions that affect [afterload]({{< relref "afterload" >}})

    ---

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    The measurement of [systolic]({{< relref "systole" >}}) blood pressure is adequate to clinically measure [afterload]({{< relref "afterload" >}})

    ---


#### [Describe the relationship between ventricular filling and stroke volume on a Frank-Starling curve.]({{< relref "describe_the_relationship_between_ventricular_filling_and_stroke_volume_on_a_frank_starling_curve" >}}) {#describe-the-relationship-between-ventricular-filling-and-stroke-volume-on-a-frank-starling-curve-dot--describe-the-relationship-between-ventricular-filling-and-stroke-volume-on-a-frank-starling-curve-dot-md}

<!--list-separator-->

-  **🔖 From Physiology, Frank Starling Law > Mechanism of the Frank-Starling law**

    Changes in [afterload]({{< relref "afterload" >}}) (the force of resistance the ventricle must overcome to empty contents at the beginning of systole) will also shift the Frank-Starling curve

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
