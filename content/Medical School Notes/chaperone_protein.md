+++
title = "Chaperone protein"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe protein folding, identifying roles for chaperones, BiP, peptidyl prolyl isomerases and protein disulfide isomerases.]({{< relref "describe_protein_folding_identifying_roles_for_chaperones_bip_peptidyl_prolyl_isomerases_and_protein_disulfide_isomerases" >}}) {#describe-protein-folding-identifying-roles-for-chaperones-bip-peptidyl-prolyl-isomerases-and-protein-disulfide-isomerases-dot--describe-protein-folding-identifying-roles-for-chaperones-bip-peptidyl-prolyl-isomerases-and-protein-disulfide-isomerases-dot-md}

<!--list-separator-->

-  **🔖 From Organelles and Trafficking I-II > Folding**

    [Chaperone proteins]({{< relref "chaperone_protein" >}}) bind to:

    ---


#### [Binding immunoglobulin protein]({{< relref "binding_immunoglobulin_protein" >}}) {#binding-immunoglobulin-protein--binding-immunoglobulin-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [chaperone protein]({{< relref "chaperone_protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
