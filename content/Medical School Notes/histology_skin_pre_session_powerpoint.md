+++
title = "Histology - Skin Pre-session Powerpoint"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Understand how the epidermis grows/regenerates.]({{< relref "understand_how_the_epidermis_grows_regenerates" >}}) {#understand-how-the-epidermis-grows-regenerates-dot--understand-how-the-epidermis-grows-regenerates-dot-md}

<!--list-separator-->

-  From [Histology - Skin Pre-session Powerpoint]({{< relref "histology_skin_pre_session_powerpoint" >}})

    <!--list-separator-->

    -  Growth/regeneration

        -   Epidermis grows from **bottom to top**
        -   The [basal layer]({{< relref "stratum_basale" >}}) (stratum basilis or stratum germinativum) is **where cell division occurs**
            -   As cells of the basal layer divide, some cells stay in place to maintain the basal layer while others are pushed up off the basement membrane by the growth of new [keratinocytes]({{< relref "keratinocyte" >}})
                -   Keratinocytes that are pushed off the basement membrane begin to differentiate
                    -   As they reach the middle layer -> degenerate and lose organelles/nuclei
                    -   At the top -> flattened dead keratinocytes that gradually flake off as they are replaced by newer cells
            -   The length of the growth process varies with site and slows with age
                -   In general, epidermis turns over (sheds) about once a month
                    -   Faster in babies (~2 weeks) and slower in elderly


#### [Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}}) {#name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot--name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot-md}

<!--list-separator-->

-  From [Histology - Skin Pre-session Powerpoint]({{< relref "histology_skin_pre_session_powerpoint" >}})

    <!--list-separator-->

    -  [Epidermis]({{< relref "epidermis" >}}) - strata

        <!--list-separator-->

        -  [Stratum basale]({{< relref "stratum_basale" >}})

            -   Single layer of **cuboidal cells** attached to the basement membrane
            -   New cells are generated from this layer (stem cells)

        <!--list-separator-->

        -  [Stratum spinosum]({{< relref "stratum_spinosum" >}}) - "spiny layer"

            -   Named for the spiny cell-to-cell bridges that are sometimes visible
                -   Correspond to [desmosomes]({{< relref "desmosome" >}})

        <!--list-separator-->

        -  [Stratum granulosum]({{< relref "stratum_granulosum" >}}) - granular layer

            -   [Keratinocytes]({{< relref "keratinocyte" >}}) start to degenerate and make keratin precursors
                -   Keratin-precursors accumulate in the cytoplasm as _keratin-hyaline granules_

        <!--list-separator-->

        -  [Stratum corneum]({{< relref "stratum_corneum" >}}) - keratin layer

            -   Skeletal remains of dead cells that are still held together by desmosomes
            -   [Keratin]({{< relref "keratin" >}}) and thick plasma membranes form a waxy barrier


#### [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}}) {#know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot--know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot-md}

<!--list-separator-->

-  From [Histology - Skin Pre-session Powerpoint]({{< relref "histology_skin_pre_session_powerpoint" >}})

    <!--list-separator-->

    -  Overview of Basic Skin

        <!--list-separator-->

        -  3 layers

            <!--list-separator-->

            -  [Epidermis]({{< relref "epidermis" >}})

                <!--list-separator-->

                -  Overview

                    -   Keratinized **stratified squamous** epithelium
                    -   Protects from UV damage
                    -   Waterproof

                    {{< figure src="/ox-hugo/_20210921_155711screenshot.png" width="800" >}}

                <!--list-separator-->

                -  Components

                    -   Contain [keratinocytes]({{< relref "keratinocyte" >}}) - cells that make large amounts of [keratin]({{< relref "keratin" >}})
                        -   Held together by [desmosomes]({{< relref "desmosome" >}})
                            -   Membrane proteins responsible for cell-to-cell adhesion
                            -   Keep the skin from falling apart
                            -   The reason why skin falls off in sheets (e.g. sunburn)
                    -   Rests on a [basement membrane]({{< relref "basement_membrane" >}})
                        -   Bottom layer of keratinocytes attached by [hemidesmosomes]({{< relref "hemidesmosome" >}})
                            -   Membrane proteins that adhere cells to the basement membrane

            <!--list-separator-->

            -  [Dermis]({{< relref "dermis" >}})

                -   Loose ([papillary dermis]({{< relref "papillary_dermis" >}})) and dense ([reticular]({{< relref "reticular_dermis" >}})) irregular connective tissue
                -   Elastin fibers
                -   Rugged foundation for epidermis
                -   Provides strength and elasticity

                {{< figure src="/ox-hugo/_20210921_161941screenshot.png" caption="Figure 1: Papillary dermis directly under the epidermis" >}}

                {{< figure src="/ox-hugo/_20210921_162120screenshot.png" caption="Figure 2: Reticular dermis directly underneath the papillary dermis" >}}

            <!--list-separator-->

            -  [Subcutis]({{< relref "subcutis" >}})

                -   Mostly fat with some connective tissue
                -   Provides cushioning and mobility
                -   Energy storage (fat)

                {{< figure src="/ox-hugo/_20210921_162537screenshot.png" caption="Figure 3: Subcutis directly under reticular dermis. Made mostly of adipose cells (white bubbles)" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
