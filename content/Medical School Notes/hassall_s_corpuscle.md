+++
title = "Hassall's corpuscle"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Irregular, distinctive collections of stratified squamous cells in the [Thymus]({{< relref "thymus" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}}) {#describe-the-structure-of-the-thymus--include-changes-with-age-dot----describe-the-structure-of-the-thymus-include-changes-with-age-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Medulla of the thymus**

    Some medullary epithelial cells form [Hassall's corpuscles]({{< relref "hassall_s_corpuscle" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
