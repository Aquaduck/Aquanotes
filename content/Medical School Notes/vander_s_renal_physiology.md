+++
title = "Vander's Renal Physiology"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}})

    <!--list-separator-->

    -  [Renal]({{< relref "kidney" >}}) functions (p. 10)

        <!--list-separator-->

        -  Excretion of metabolic waste and foreign substances

        <!--list-separator-->

        -  Regulation of [water]({{< relref "water" >}}) and electrolyte balance

        <!--list-separator-->

        -  Regulation of extracellular fluid volume

        <!--list-separator-->

        -  Regulation of [plasma]({{< relref "plasma" >}}) osmolality

        <!--list-separator-->

        -  Regulation of [RBC]({{< relref "red_blood_cell" >}}) production

        <!--list-separator-->

        -  Regulation of vascular resistance

        <!--list-separator-->

        -  Regulation of acid-base balance

        <!--list-separator-->

        -  Regulation of vitamin D production

        <!--list-separator-->

        -  Gluconeogenesis


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}})

    <!--list-separator-->

    -  The [Renal tubule]({{< relref "renal_tubule" >}}) (p. 18)

        {{< figure src="/ox-hugo/_20211031_163629screenshot.png" caption="Figure 1: Components of the nephron." >}}

        -   The _Renal tubule_ begins at and leads out of the [Bowman capsule]({{< relref "bowman_capsule" >}}) on the side **opposite the vascular pole**
        -   Contains segments further divided into subdivisions

        <!--list-separator-->

        -  [Proximal tubule]({{< relref "proximal_tubule" >}})

            ([Proximal convoluted tubule]({{< relref "proximal_convoluted_tubule" >}}) + [Proximal straight tubule]({{< relref "proximal_straight_tubule" >}}))

            -   The first segment of the Renal tubule
            -   Drains [Bowman capsule]({{< relref "bowman_capsule" >}})

        <!--list-separator-->

        -  [Descending thin limb of the loop of Henle]({{< relref "descending_thin_limb_of_the_loop_of_henle" >}})

            -   The descending thin limbs **begin at the same level in all nephrons** - the point where they connect to proximal straight tubule in the [outer medulla]({{< relref "outer_medulla_of_the_kidney" >}})
                -   Marks the border between the oute rand inner stripes of the outer medulla
                -   On the other hand, the **penetrating depth** of each nephron's descending limb **varies**

        <!--list-separator-->

        -  [Ascending thin limb of the loop of Henle]({{< relref "ascending_thin_limb_of_the_loop_of_henle" >}})

            -   Begins at the abrupt hairpin turn from the descending thin limb
            -   In long loops (deeply penetrated), epithelium of the first portion of the ascending limb remains thin **but has a different function than the descending limb**

        <!--list-separator-->

        -  [Ascending thick limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}})

            -   Epithelium thickens
            -   In short loops, the abrupt hairpin turn from the descending thin limb leads **directly** to the ascending thick limb
            -   **All thick ascending limbs begin at the same level**
                -   Marks the border between the [inner]({{< relref "inner_medulla_of_the_kidney" >}}) and [outer medulla]({{< relref "outer_medulla_of_the_kidney" >}})
            -   Each thick ascending limb rises back into the cortex to the [Bowman's capsule]({{< relref "bowman_capsule" >}})
                -   Passes directly between the [afferent]({{< relref "afferent_arteriole_of_the_kidney" >}}) and [efferent]({{< relref "efferent_arteriole_of_the_kidney" >}}) arterioles at the vascular pole
                    -   [Macula densa]({{< relref "macula_densa" >}}) cells found at this point between the two arterioles and marks the end of the ascending thick limb of the loop of Henle

        <!--list-separator-->

        -  [Distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}})

            -   Marked by the [Macula densa]({{< relref "macula_densa" >}}) cells at the vascular pole of the Bowman's capsule
            -   Followed by the [connecting tubule]({{< relref "connecting_tubule" >}}) -> initial collecting tubule of the cortical collecting duct

        <!--list-separator-->

        -  [Cortical collecting duct]({{< relref "cortical_collecting_duct" >}})

            -   First part is the _initial collecting tubule_
            -   Connecting tubules from **several nephrons merge** to form a given cortical collecting duct
            -   All cortical collecting ducks then run downward -> enter the medulla -> become [Outer medullary collecting duct]({{< relref "outer_medullary_collecting_duct" >}}) -> become [Inner medullary collecting duct]({{< relref "inner_medullary_collecting_duct" >}}) -> _papillary collecting ducts_ empties into a [calyx]({{< relref "renal_calyx" >}}) of the [Renal pelvis]({{< relref "renal_pelvis" >}})
                -   Each [renal calyx]({{< relref "renal_calyx" >}}) is continuous with the [ureter]({{< relref "ureter" >}})
                    -   **Urine is not altered after it enters a [calyx]({{< relref "renal_calyx" >}})**


#### [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}}) {#explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result--explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result-dot-md}

<!--list-separator-->

-  From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}})

    <!--list-separator-->

    -  [Juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}}) (p. 21)

        -   Composed of three cell types

        <!--list-separator-->

        -  [Granular cells]({{< relref "granular_cell" >}})

            -   Differentiated smooth muscle cells in the walls of the [afferent arterioles]({{< relref "afferent_arteriole_of_the_kidney" >}})
                -   Can sense blood pressure
            -   Contain secretory granules -> contain [Renin]({{< relref "renin" >}})

        <!--list-separator-->

        -  Extraglomerular [mesangial cells]({{< relref "mesangial_cell" >}})

            -   Morphologically similar to + continuous with the glomerular mesangial cells, **but lie outside the Bowman's capsule**

        <!--list-separator-->

        -  [Macula densa]({{< relref "macula_densa" >}}) cells

            -   Detectors of flow rate and composition of fluid in nephron at th every end of the [thick ascending limb]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}})
            -   Contribute to the control of [glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}) and to the control of [renin]({{< relref "renin" >}}) secretion


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}})

    <!--list-separator-->

    -  Basic renal excretory processes (p. 21)

        {{< figure src="/ox-hugo/_20211031_173409screenshot.png" caption="Figure 2: Fundamental elements of renal function - glomerular filtration, tubular secretion and tubular reabsorption - and the association between the tubule and vasculature in the cortex" >}}

        -   _[Filtration]({{< relref "filtration" >}})_: the process by which [Water]({{< relref "water" >}}) and solutes in the [Blood]({{< relref "blood" >}}) leave the vascular system through the [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) and enter [Bowman's space]({{< relref "bowman_s_space" >}})
            -   [Bowman's space]({{< relref "bowman_s_space" >}}) is topologically **outside the body**
        -   _[Reabsorption]({{< relref "reabsorption" >}})_: the process of moving substances from the lumen across the epithelial layer into the surrounding interstitium
            -   In most cases, reabsorbed substances then move into surrounding blood vessels -> **2-step process**:
                1.  Removal from tubular lumen
                2.  Movement into blood
        -   _[Excretion]({{< relref "excretion" >}})_: exit of the substance from the body

        <!--list-separator-->

        -  Glomerular filtration (p. 23)

            -   [Urine]({{< relref "urine" >}}) formation begins with glomerular filtration: the bulk flow of fluid from the glomerular capillaries into [Bowman's capsule]({{< relref "bowman_capsule" >}})
            -   _Glomerular filtrate_: fluid within the Bowman's capsule
                -   Very similar to blood plasma, but **contains little protein because large plasma proteins are virtually excluded form moving through the [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}})**
                    -   E.g. albumin and globulins are excluded
                    -   Smaller proteins (e.g. peptide hormones) are still able to pass through the barrier, but their combined mass is miniscule
                -   Contains most **inorganic ions** and **low-molecular-weight organic solutes**
                    -   Substances with same concentration in filtrate as in plasma are considered _freely filtered_
                        -   E.g. [Sodium]({{< relref "sodium" >}}), [Potassium]({{< relref "potassium" >}}), [Chloride]({{< relref "chloride" >}}), [Bicarbonate]({{< relref "bicarbonate" >}}), [Glucose]({{< relref "glucose" >}}), [Urea]({{< relref "urea" >}}), [amino acids]({{< relref "amino_acid" >}}), peptides such as [Insulin]({{< relref "insulin" >}}) and [ADH]({{< relref "antidiuretic_hormone" >}})
            -   [Glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}): **the volume of filtrate formed per unit of time**
                -   Normally 180 L/day

    <!--list-separator-->

    -  Glomerular filtration (p. 33)

        -   Filtered fluid must pass through a 3-layered [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}})
            1.  Endothelial cells of the capillaries
                -   Perforated by many large _fenestrae_ ("windows") which are freely permeable to everything in the blood **except cells and platelets**
            2.  Capillary basement membrane
                -   Gel-like acellular meshwork of glycoproteins and proteoglycans
            3.  [Podocytes]({{< relref "podocyte" >}}): epithelial cells that surround capillaries and rest on the capillary basement membrane
                -   Have an unusual octopus-like structure with small "fingers" called _pedicels_
                    -   Coated by a thick layer of extracellular material -> partially occludes the slits
                    -   These interdigitate with the pedicels from other podocytes
                -   _Slit diaphragm_: bridge the slits between pedicals
                    -   Widened versions of tight junctions and adhering junctions of other epithelial cells
                    -   **Spaces between slit diaphragms constitute the path through which filtrate travels to enter the [Bowman's space]({{< relref "bowman_s_space" >}})**
                    -   **Integrity of slit diaphragms of podocytes essential to prevent excessive leak of [albumin]({{< relref "albumin" >}})**
        -   Selectivity of the [Glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) depends on **molecular size** and **electrical charge**

            {{< figure src="/ox-hugo/_20211031_180522screenshot.png" caption="Figure 3: Relationships between molecular weight vs. filtrate and charge vs. filtrate" >}}

            1.  Allows unhindered movement of molecules less than 7000 Da
                -   From 7000 to 70,000, movement is more progressively hindered until there is no movement
            2.  **Negatively charged macromolecules are permitted less** relative to neutral molecules
                -   Surfaces of all three layers of the [Glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) contain fixed polyanions -> **repel negatively charged macromolecules during filtration**
                    -   **Almost all [plasma]({{< relref "plasma" >}}) proteins bear net negative charge -> important restrictive role**


#### [Describe glomerular anatomy (ie efferent/afferent arteriole)]({{< relref "describe_glomerular_anatomy_ie_efferent_afferent_arteriole" >}}) {#describe-glomerular-anatomy--ie-efferent-afferent-arteriole----describe-glomerular-anatomy-ie-efferent-afferent-arteriole-dot-md}

<!--list-separator-->

-  From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}})

    <!--list-separator-->

    -  The [Renal corpuscle]({{< relref "renal_corpuscle" >}}) (p. 17)

        -   Hollow sphere ([Bowman capsule]({{< relref "bowman_capsule" >}})) composed of epithelial cells
        -   Filled with a compact tuft of interconnected capillary loops called the [Glomerulus]({{< relref "glomerulus" >}})
        -   Two closely spaced arterioles penetrate the [Bowman capsule]({{< relref "bowman_capsule" >}}) at a region called the _vascular pole_:
            1.  [Afferent arteriole of the kidney]({{< relref "afferent_arteriole_of_the_kidney" >}}): brings blood into the capillaries of the glomerulus
            2.  [Efferent arteriole of the kidney]({{< relref "efferent_arteriole_of_the_kidney" >}}): drains blood from the capillaries of the glomerulus
        -   [Mesangial cell]({{< relref "mesangial_cell" >}}): act as phagocytes -> remove trapped material from basement membrane of the capillaries in the [Glomerulus]({{< relref "glomerulus" >}})
            -   Mesangial cells also contain large numbers of myofilaments -> contract in response to a variety of stimuli similar to vascular smooth muscle cells
        -   [Bowman's space]({{< relref "bowman_s_space" >}}): Space within [Bowman capsule]({{< relref "bowman_capsule" >}}) that is not occupied by capillaries and mesangial cells
            -   It is here where fluid filters from the glomerular capillaries before flowing into the first portion of the tubule opposite the vascular pole


### Unlinked references {#unlinked-references}

[Show unlinked references]
