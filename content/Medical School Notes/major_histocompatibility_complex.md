+++
title = "Major histocompatibility complex"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name one drug in which severe adverse effects may depend on the presence of a specific genetic variation]({{< relref "name_one_drug_in_which_severe_adverse_effects_may_depend_on_the_presence_of_a_specific_genetic_variation" >}}) {#name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation--name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation-dot-md}

<!--list-separator-->

-  **🔖 From Pharmacogenetics Pre-learning Material > Examples of pharmacogenomic effects**

    Genetic variations in the HLA-A and HLA-B genes of the [MHC]({{< relref "major_histocompatibility_complex" >}}) can predict risk for severe toxicities ot certain drugs

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
