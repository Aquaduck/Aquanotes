+++
title = "Regional and Peripheral Circulation Pre-reading"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Mean Arterial Pressure and Pulse Pressure {#mean-arterial-pressure-and-pulse-pressure}


### Pulse pressure {#pulse-pressure}

-   _Pulse pressure = (peak arterial systolic blood pressure) - (arterial diastolic blood pressure)_ where:
    -   _Systolic pressure_: peak arterial blood pressure
    -   _Diastolic pressure_: minimum arterial blood pressure
    -   Unit: mm Hg


### Mean arterial pressure (MAP) {#mean-arterial-pressure--map}

**NOTE**: mean arterial pressure in proximal aorta != arithmetic mean pressure

-   _MAP_: average pressure over time, including systole and diastole, and can be determined as the integral of the pressure wave
    -   It is important when considering the function of the circulation as a siphone
-   Equation for estimate of mean pressure that is clinically useful:
    -   _Mean Aortic BP = Diastolic BP +_ \\(\frac{Pulse Pressure}{3}\\)
-   **Three factors that affect MAP:**
    1.  Cardiac output (stroke volume and heart rate)
    2.  Rate of peripheral runoff (blood that leaves the aorta and its branches)
    3.  Aortic stiffness (the extent to which aortic blood pressure passively increases when the aortic volume increases)
-   During steady state, with stable cardiac output, total peripheral runoff/time = equal cardiac output
-   If _left ventricular output_ increases to a higher steady level, the output will transiently exceed _peripheral runoff_ -> _aortic volume_ and _mean aortic pressure_ will increase

{{< figure src="/ox-hugo/_20211016_164857screenshot.png" caption="Figure 1: Aortic pulse and mean BP. Diastolic BP here refers to pressure in the aorta. Aortic systolic blood pressure is measured at the peak of pressure development" >}}


## Relationship of Pressure, Flow, and Resistance {#relationship-of-pressure-flow-and-resistance}

-   \\(Q = \frac{P\_{1} - P\_{2}}{R}\\) where:
    -   _Q_: blood flow per unit time
    -   _R_: vascular resistance
-   In the **overall systemic circulation**:
    -   _Q = CO_
    -   _P<sub>1</sub>_: mean **aortic** blood pressure
    -   _P<sub>2</sub>_: mean **right atrial** blood pressure
-   In the **pulmonary circulation**:
    -   _P<sub>1</sub>_: mean **pulmonary artery** blood pressure
    -   _P<sub>2</sub>_: mean **left atrial** blood pressure
-   **Resistance** (in systemic circulation) **resides mostly in the [arterioles]({{< relref "arteriole" >}})**
    -   Includes _peripheral runoff_
        -   **High** resistance -> **impedes** runoff
        -   **Low** resistance -> **increased** runoff
-   Formula for resistance to liquid flowing in a tube: \\(R = \frac{P\_{1} = P\_{2}}{Q}\\) where:
    -   _R_: resistance to flow
    -   _P<sub>1</sub> = P<sub>2</sub>_: pressure drop from point 1 to point 2
    -   _Q_: total flow from point 1 to point 2


## Coronary Blood Flow and the Cardiac Cycle {#coronary-blood-flow-and-the-cardiac-cycle}


### [Systole]({{< relref "systole" >}}) {#systole--systole-dot-md}

-   Coronary arterial inflow varies during the cardiac cycle, **particularly in the left ventricle**
-   Intramyocardial coronary arterial vessels are squeezed by contracting myocardium during systole
-   Reduction of vessel radius + increase in coronary vascular resistance -> **reduced coronary arterial inflow**
-   Aortic pressure is high enough during left ventricular ejection -> blood travels through elevated coronary vascular resistance
-   Variations in blood flow less in right ventricle
    -   Because lower pressure + thinner-walled


### [Diastole]({{< relref "diastole" >}}) {#diastole--diastole-dot-md}

-   **Most coronary artery inflow occurs during diastole** when the myocardium is relaxed
    -   Emphasized in left ventircle because of greater variation in pressure levels + wall tension than right ventricle
-   When heart rate increases in exercise -> **duration of diastole decreases much more than duration of systole**
    -   This is normally not an issue because the coronary arteriolar vasodilation results in adequate coronary blood flow


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
