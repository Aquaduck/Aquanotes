+++
title = "Dendritic cell"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}}) {#describe-the-major-actions-occurring-in-these-regions--with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot----describe-the-major-actions-occurring-in-these-regions-with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Paracortex**

    These two cell types interact -> testing to see if any [T-cells]({{< relref "t_lymphocyte" >}}) recognize the peptide displayed by the [dendritic cells]({{< relref "dendritic_cell" >}})

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Paracortex**

    [Dendritic cells]({{< relref "dendritic_cell" >}}) and antigen enter the node throug the afferent lymphatics

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
