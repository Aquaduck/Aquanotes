+++
title = "FADH2"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A derivative of [riboflavin]({{< relref "riboflavin" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Riboflavin]({{< relref "riboflavin" >}}) {#riboflavin--riboflavin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Component of flavins [FAD]({{< relref "fadh2" >}}) and FMN

    ---


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links > Cofactors/Coenzymes**

    [FAD]({{< relref "fadh2" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid oxidation**

    [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)

    ---

<!--list-separator-->

-  **🔖 TCA cycle**

    [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)

    ---

<!--list-separator-->

-  **🔖 Pyruvate DH complex**

    [FAD]({{< relref "fadh2" >}}) from [Riboflavin]({{< relref "riboflavin" >}}) (B2)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
