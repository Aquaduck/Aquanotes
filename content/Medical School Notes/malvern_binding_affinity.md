+++
title = "Malvern - Binding Affinity"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

Link: <https://www.malvernpanalytical.com/en/products/measurement-type/binding-affinity>


## What is Binding Affinity? {#what-is-binding-affinity}

-   Strength of binding interaction between a single biomolecule to its ligand
    -   e.g. [proteins]({{< relref "protein" >}})
-   Typically measured and reported by the equilibrium dissociation constant \\(K\_{D}\\)
    -   Evaluates and ranks order strengths of bimolecular interactions
-   **The smaller the K<sub>D</sub> value, the greater the binding affinity of the ligand for its targets**
-   Binding affinity is influenced by non-covalent intermolecular interactions such as:
    -   Hydrogen bonding
    -   Electrostatic interactions
    -   Hydrophobic and Van der Waals forces
    -   Presence of other molecules


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 General Biochemistry and Techniques > 1. Define k<sub>d</sub>**

    [Malvern - Binding Affinity]({{< relref "malvern_binding_affinity" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
