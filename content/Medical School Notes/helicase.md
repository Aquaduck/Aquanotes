+++
title = "Helicase"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Steps in replication**

    A [helicase]({{< relref "helicase" >}}) opens up the DNA helix using energy from ATP hydrolysis

    ---

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Steps in replication**

    [Helicase]({{< relref "helicase" >}}) and other proteins are then recruited to start replication

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
