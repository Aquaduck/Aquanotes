+++
title = "Openstax - EIAs and ELISAs"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "papers", "openstax", "source"]
draft = false
+++

## Overview {#overview}

-   _Enzyme immunoassays (EIAs)_ use antibodies to detect the presence of antigens
    -   Similar to western blot, except the tests are conducted in microtiter plates or _in vivo_
-   All involve an antibody molecule whose constant region binds an enzyme, leaving the variable region free to bind its specific antigen


## Enzyme-linked Immunosorbent Assays (ELISAs) {#enzyme-linked-immunosorbent-assays--elisas}


### Direct ELISA {#direct-elisa}

-   While this technique is faster due to using only one antibody, the signal is lower


#### Steps {#steps}

1.  Antigens immobilized in the well of a microtiter plate
2.  Antibody specific for a particular antigen and conjugated to an enzyme is added to each well
3.  If antigen present -> antibody binds
4.  Wash -> add colorless substrate (chromogen)
5.  Presence of enzyme converts the substrate a colored end product


### Sandwich ELISA {#sandwich-elisa}

-   Use antibodies to precisely quantify specific antigen present in a solution


#### Steps {#steps}

1.  Add _primary antibody_ to all the wells of microtiter plate
    -   Antibody sticks to plastic via hydrophobic interactions
2.  After incubation, unbound antibody is washed away
3.  Blocking protein is added to bind the remaining nonspecific protein-binding sites in the well
4.  Some wells receive known amount of antigen to make a standard curve
5.  Primary antibody captures antigen and, following a wash, a _secondary antibody_ is added
    -   This is a polyclonal antibody conjugated to an enzyme
6.  After a final wash, colorless substrate is added -> enzyme converts it to a colored end product
7.  Measure color with spectrophotomoter
    -   Absorbance directly proportional to amount of enzyme = amount of captured antigen

{{< figure src="/ox-hugo/_20210715_234057screenshot.png" width="500" >}}


### Indirect ELISA {#indirect-elisa}

-   Quantifies **antigen-specific antibody** rather than antigen
    -   Can be used to detect the antibodies of many types of pathogens


#### Steps {#steps}

-   Rather than using antibody to capture antigen, indirect ELISA **starts with attaching a known antigen to the bottom of the microtiter plate wells**
-   After blocking unbound sites on the plate, patient serum is added
    -   If antibodies are present (primary antibody), they will bind to the antigen
-   After washing away unbound proteins, secondary antibody w/ conjugated enzyme is directed against the primary antibody
    -   Secondary antibody allows us to **quantify how much antigen-specific antibody** is present in the patient's serum **by the intensity of the color produced from the conjugated enzyme-chromogen reaction**

{{< figure src="/ox-hugo/_20210715_234457screenshot.png" width="500" >}}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 General Biochemistry and Techniques > 5. Describe how antibodies are used in an Enzyme-Linked ImmunoSorbent Assay (ELISA)**

    [Openstax - EIAs and ELISAs]({{< relref "openstax_eias_and_elisas" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
