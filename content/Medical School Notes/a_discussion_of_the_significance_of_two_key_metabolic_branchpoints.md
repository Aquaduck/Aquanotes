+++
title = "A Discussion of the Significance of Two Key Metabolic Branchpoints"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Glucose metabolism varies in different cells and tissues {#glucose-metabolism-varies-in-different-cells-and-tissues}


### RBCs {#rbcs}


#### Fed state {#fed-state}

-   [Insulin]({{< relref "insulin" >}}) has **no effect on glucose uptake into [RBCs]({{< relref "red_blood_cell" >}})** in the **fed state**
    -   Need a constant supply of glucose because they **do not have a mitochondria**
-   _[GLUT-1]({{< relref "glut_1" >}})_: primary glucose transporter in RBCs
    -   Glucose is then used either in:
        -   Anaerobic [glycolysis]({{< relref "glycolysis" >}}) -> generation of ATP
        -   [Pentose phosphate pathway]({{< relref "pentose_phosphate_pathway" >}}) -> production of NADPH


#### Fasting state {#fasting-state}

-   When moving into a late fasting state -> RBCs continue to rely on [blood glucose] provided by liver via gluconeogenesis
    -   **[Pentose phosphate pathway]({{< relref "pentose_phosphate_pathway" >}}) slowed considerably**


### Brain {#brain}


#### Fed state {#fed-state}

-   Neural tissue utilizes both "arms" of the [pentose phosphate pathway]({{< relref "pentose_phosphate_pathway" >}}) as they synthesize nucleotides
-   Require reserves of [NADPH]({{< relref "nadph" >}}) to prevent oxidant injury
-   Also use glucose for glycogen synthesis and [fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})


#### Fasting state {#fasting-state}

-   Brain requires a **constant source of glucose for aerobic [glycolysis]({{< relref "glycolysis" >}}), ATP generation via [oxidative phosphorylation]({{< relref "oxidative_phosphorylation" >}}), and replenishing lost [TCA]({{< relref "citric_acid_cycle" >}}) cycle intermediates**


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
