+++
title = "Herpes zoster"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the etiology of herpes zoster as well as the clinical presentation of the disease.]({{< relref "describe_the_etiology_of_herpes_zoster_as_well_as_the_clinical_presentation_of_the_disease" >}}) {#describe-the-etiology-of-herpes-zoster-as-well-as-the-clinical-presentation-of-the-disease-dot--describe-the-etiology-of-herpes-zoster-as-well-as-the-clinical-presentation-of-the-disease-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Herpes zoster]({{< relref "herpes_zoster" >}})

        -   Caused by _Varicella zoster_ [virus]({{< relref "virus" >}})
        -   Virus remains dormant in the _dorsal root gangla_ -> reactivates later in life as "shingles" within dermatome supplied by a particular DRG
        -   After invading a ganglion, virus produces a sharp burning pain in [Dermatome]({{< relref "dermatome" >}})
            -   Affected skin area becomes red -> **stripe dermatome pattern**
            -   Vesicular eruptions appear
        -   Pain can precede or follow skin eruptions
        -   Weakness form motor involvement occurs in up to 5% of people
            -   Typically occurs in same myotomal distribution as dermatomal pain
        -   Can present with sensory issues (e.g. burning, itching) and a blistering rash within that dermatome


### Unlinked references {#unlinked-references}

[Show unlinked references]
