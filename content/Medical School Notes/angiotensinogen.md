+++
title = "Angiotensinogen"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Produced by the [Liver]({{< relref "liver" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Renin + Angiotensin**

    Converts the plasma protein [angiotensinogen]({{< relref "angiotensinogen" >}}) (produced by the liver) into its active form - [angiotensin I]({{< relref "angiotensin_i" >}})

    ---


#### [Angiotensin I]({{< relref "angiotensin_i" >}}) {#angiotensin-i--angiotensin-i-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Active form of [Angiotensinogen]({{< relref "angiotensinogen" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
