+++
title = "Innate immunity"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Macrophage]({{< relref "macrophage" >}}) {#macrophage--macrophage-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A phagocytic cell of the [innate immune system]({{< relref "innate_immunity" >}})

    ---


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Two arms of the immune system > Adaptive system**

    **Greater specificity and efficacy** than the recepters of the [innate system]({{< relref "innate_immunity" >}})

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Two arms of the immune system**

    <!--list-separator-->

    -  [Innate system]({{< relref "innate_immunity" >}})

        -   Comprised of cells and proteins
        -   The cells (neutrophils and monocytes/macrophages) can engulf and kill bacteria with the help of a large number of plasma protein and receptor molecules encoded in DNA
            -   These are synthesized by the [liver]({{< relref "liver" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
