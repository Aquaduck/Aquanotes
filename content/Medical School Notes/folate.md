+++
title = "Folate"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   AKA _Vitamin B<sub>9</sub>_
-   [B vitamin]({{< relref "b_vitamin" >}})


## From First Aid Section II Biochemistry Nutrition {#from-first-aid-section-ii-biochemistry-nutrition}


### Function {#function}

-   Converted to _tetrahydrofolic acid (THF)_, a coenzyme for 1-carbon transfer/methylation reactions
-   Important for the synthesis of nitrogenous bases in [DNA]({{< relref "dna" >}}) and [RNA]({{< relref "rna" >}})
-   Small reserve pool stored primarily in the [liver]({{< relref "liver" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
