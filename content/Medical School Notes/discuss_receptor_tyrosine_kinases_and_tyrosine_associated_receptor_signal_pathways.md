+++
title = "Discuss receptor tyrosine kinases and tyrosine-associated receptor signal pathways"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### [Receptor protein tyrosine kinases]({{< relref "receptor_protein_tyrosine_kinase" >}}) (RPTKs) {#receptor-protein-tyrosine-kinases--receptor-protein-tyrosine-kinase-dot-md----rptks}

-   **Receptors that are also tyrosine kinases**
-   Involved in **growth** -> think cancer
-   Tyrosine kinase receptor **phosphorylates itself** -> creates a specific kind of binding site (_SH<sub>2</sub>_)
    -   Multiple tyrosines phosphorylated -> **multiple binding sites for additional signal proteins**


#### Examples of growth factor receptors that are tyrosine kinases {#examples-of-growth-factor-receptors-that-are-tyrosine-kinases}

1.  Epidermal Growth Factor (EGF)
2.  Insulin-like Growth Factor-1 (IGF-1)
3.  Nerve Growth Factor (NGF)
4.  Platelet Derived Growth Factor (PDGF)
5.  Insulin


#### Two key pathways {#two-key-pathways}

<!--list-separator-->

-  [Phosphatidylinositol kinase]({{< relref "phosphatidylinositol_kinase" >}}) (PI-3 Kinase)

    -   Involves [PIP<sub>2</sub> signalling]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}})
    -   Controls (e.g.):
        -   Growth/survival of cells
        -   Apoptosis
        -   Many more

<!--list-separator-->

-  [Ras-GTP]({{< relref "ras" >}})

    -   Controls (e.g.):
        -   Endocytosis
        -   Cell cycle
        -   Nuclear transport
        -   Membrane trafficking
        -   Many more

    <!--list-separator-->

    -  Pathway

        1.  RPTK receptors phosphorylated on multiple locations -> activated
        2.  Allows binding of [SH<sub>2</sub>]({{< relref "src_homology_2_domain" >}}) binding proteins -> Ras activation
        3.  Ras activates [MAP-kinase-kinase-kinase]({{< relref "map_kinase_kinase_kinase" >}}) (Raf) -> activates [MAP-kinase-kinase]({{< relref "map_kinase_kinase" >}}) (Mek) -> activates [MAP-kinase]({{< relref "map_kinase" >}}) (Erk)
            -   Abnormal expression due to gene mutation and/or dysregulation -> cancer -> **[oncogenes]({{< relref "oncogene" >}})**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Discuss receptor tyrosine kinases and tyrosine-associated receptor signal pathways]({{< relref "discuss_receptor_tyrosine_kinases_and_tyrosine_associated_receptor_signal_pathways" >}}).


### Unlinked references {#unlinked-references}

[Show unlinked references]
