+++
title = "Epithelium of the thymus"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   There are abundant epithelial cells in the [Thymus]({{< relref "thymus" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}}) {#describe-the-structure-of-the-thymus--include-changes-with-age-dot----describe-the-structure-of-the-thymus-include-changes-with-age-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Medulla of the thymus**

    Function of the [epithelial cells]({{< relref "epithelium_of_the_thymus" >}}):

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus**

    <!--list-separator-->

    -  [Thymus epithelium]({{< relref "epithelium_of_the_thymus" >}})

        -   Unlike most epithelium, thymic epithelium **loses its connection with the surface endoderm**
        -   Thymus epithelium forms a blood/thymic barrier
            -   A basement membrane separates the thymus from its capsule and from the blood vessels within the thymus
        -   Internal part of the thymus is a sponge-like network of epithelial cells filled with [lymphocytes]({{< relref "lymphocyte" >}})
        -   Epithelial cells in the [Cortex of the thymus]({{< relref "cortex_of_the_thymus" >}}) assist precursor T-cells to rearrange their [TCR]({{< relref "t_cell_receptor" >}}) genes and become naive T-cells
            -   In the [Medulla of the thymus]({{< relref "medulla_of_the_thymus" >}}) epithelial cells direct elimination of autoreactive lymphocytes and help [T-cell]({{< relref "t_lymphocyte" >}}) differentiation into helper or suppressor cells
        -   Thymus epithelial cells form **broad loose sheets of cells** (with desmosomes) that are **heavily infiltrated by [lymphocytes]({{< relref "lymphocyte" >}}) in between the epithelial cells**


### Unlinked references {#unlinked-references}

[Show unlinked references]
