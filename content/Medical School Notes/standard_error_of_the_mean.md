+++
title = "Standard error of the mean"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Interpret confidence intervals around a sample size or calculated relative risk or odds ratio]({{< relref "interpret_confidence_intervals_around_a_sample_size_or_calculated_relative_risk_or_odds_ratio" >}}) {#interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio--interpret-confidence-intervals-around-a-sample-size-or-calculated-relative-risk-or-odds-ratio-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Confidence interval**

    [Standard error of the mean]({{< relref "standard_error_of_the_mean" >}}), which needs:

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
