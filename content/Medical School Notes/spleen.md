+++
title = "Spleen"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [White pulp]({{< relref "white_pulp" >}}) {#white-pulp--white-pulp-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A component of the [splenic]({{< relref "spleen" >}}) parenchyma which contains mostly [lymphocytes]({{< relref "lymphocyte" >}})

    ---


#### [Splenic cords]({{< relref "splenic_cords" >}}) {#splenic-cords--splenic-cords-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Sheathed capillaries]({{< relref "sheathed_capillary" >}}) in the [spleen]({{< relref "spleen" >}}) dump blood into here

    ---


#### [Sheathed capillary]({{< relref "sheathed_capillary" >}}) {#sheathed-capillary--sheathed-capillary-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Found in the [Spleen]({{< relref "spleen" >}})

    ---


#### [Red pulp]({{< relref "red_pulp" >}}) {#red-pulp--red-pulp-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A component of the [splenic]({{< relref "spleen" >}}) parenchyma that contains mostly [red blood cells]({{< relref "red_blood_cell" >}})

    ---


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT**

    <!--list-separator-->

    -  [Spleen]({{< relref "spleen" >}})

        -   The spleen is a filter of blood
        -   The spleen has a kidney bean shape and a well-defined CT capsule with CT trabeculae projecting outwards from a central hilum
            -   There are efferent, but **no afferent lymphatics**
        -   The splenic parenchyma contains two interspersed components: [red pulp]({{< relref "red_pulp" >}}) and [white pulp]({{< relref "white_pulp" >}}) (named for their fresh unstained color)
            -   Red pulp is rich in blood
            -   White pulp is mostly lymphocytes
        -   Surrounding central arterioles in the spleen are an aggregate of [T-lymphocytes]({{< relref "t_lymphocyte" >}}) called a [periarteriole lymphoid sheath]({{< relref "periarteriole_lymphoid_sheath" >}}) (PALS)

        <!--list-separator-->

        -  Splenic function

            -   [White pulp]({{< relref "white_pulp" >}}) responds to bloodborne antigens similarly to how the lymph nodes respond to antigen in the lymph
            -   The [red pulp]({{< relref "red_pulp" >}}) filters and digests "particulate" material in the blood
                -   Ranges from unwanted or damaged proteins to senescent cells (especially aged [RBCs]({{< relref "red_blood_cell" >}}))
                -   There is a steady state need to remove damaged cells and protein complexes from the blood
                -   The filtration is accomplished by a unique splenic circulation called an _open circulation_

        <!--list-separator-->

        -  Spleen filtration

            -   _Closed vascular system_: blood flows from the arterial side through the capillaries to the venous side, all while enclosed by an endothelium
            -   _Open vascular system_: smallest vessels are blind-ended capillaries ([sheathed capillaries]({{< relref "sheathed_capillary" >}})) without an endothelium -> the ends are instead surrounded by aggregates of [histiocytes]({{< relref "histiocyte" >}})
                -   Sheathed capillaries dump blood into the interstitial tissues of the spleen - called [splenic cords]({{< relref "splenic_cords" >}}) (aka Cords of Billroth)
            -   Senescent cells and material are removed as they attempt to navigate the [macrophages]({{< relref "macrophage" >}}) of the [sheath]({{< relref "sheathed_capillary" >}}) and [cords]({{< relref "splenic_cords" >}}) -> squeeze between endothelial cells to return to the vascular system in the sinuses of the [red pulp]({{< relref "red_pulp" >}})

        <!--list-separator-->

        -  Splenic flow

            {{< figure src="/ox-hugo/_20211010_173004screenshot.png" caption="Figure 1: Blood flow through the spleen" width="700" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
