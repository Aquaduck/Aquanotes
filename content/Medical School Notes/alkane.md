+++
title = "Alkane"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kaplan Organic Chemistry Chapter 1]({{< relref "kaplan_organic_chemistry_chapter_1" >}}) {#kaplan-organic-chemistry-chapter-1--kaplan-organic-chemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 1.2 Hydrocarbons and Alcohols**

    <!--list-separator-->

    -  [Alkanes]({{< relref "alkane" >}})

        -   Simple hydrocarbon molecules with the formula C<sub>n</sub>H<sub>2n + 2</sub>

        <!--list-separator-->

        -  Naming

            | Number of Carbons | Name    | Structure                        |
            |-------------------|---------|----------------------------------|
            | 1                 | Methane | <_20210712_215044screenshot.png> |
            | 2                 | Ethane  | <_20210712_215412screenshot.png> |
            | 3                 | Propane | <_20210712_215444screenshot.png> |
            | 4                 | Butane  | <_20210712_215503screenshot.png> |
            | 5                 | Pentane | <_20210712_215525screenshot.png> |

            And so on...


### Unlinked references {#unlinked-references}

[Show unlinked references]
