+++
title = "Hypoxemia"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   Decreased [oxygen]({{< relref "oxygen" >}}) content in arterial [blood]({{< relref "blood" >}}) (↓ PaO<sub>2</sub>)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Define the difference between V/Q for a shunt vs. V/Q for a dead space.]({{< relref "define_the_difference_between_v_q_for_a_shunt_vs_v_q_for_a_dead_space" >}}) {#define-the-difference-between-v-q-for-a-shunt-vs-dot-v-q-for-a-dead-space-dot--define-the-difference-between-v-q-for-a-shunt-vs-v-q-for-a-dead-space-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > V/Q mismatch**

    **The most common cause of [hypoxemia]({{< relref "hypoxemia" >}})**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
