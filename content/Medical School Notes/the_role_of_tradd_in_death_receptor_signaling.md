+++
title = "The role of TRADD in death receptor signaling"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) in [TNF]({{< relref "tumor_necrosis_factor" >}})/[TNFR1]({{< relref "tnfr1" >}}) signalling {#tradd--tnfr1-associated-death-domain-protein-dot-md--in-tnf--tumor-necrosis-factor-dot-md--tnfr1--tnfr1-dot-md--signalling}

-   The default signaling pathway activating by the binding of TNF alpha is the induction of [NFκB]({{< relref "nfκb" >}}) -> responsible for **cell survival and growth**
-   TNFR1 induces cell death (apoptosis AND necrosis) **only when [NFκB]({{< relref "nfκb" >}}) activation is impaired**
-   Dependence of TNFR1 signaling on TRADD appears to be **cell-type specific**


## [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) in [TRAIL]({{< relref "apo2_ligand" >}})/TRAILR Signaling {#tradd--tnfr1-associated-death-domain-protein-dot-md--in-trail--apo2-ligand-dot-md--trailr-signaling}

-   Both TRADD and [RIP]({{< relref "receptor_interacting_protein" >}}) were demonstrated to be recruited to the TRAILR complex in the [DISC]({{< relref "death_inducing_signaling_complex" >}})
    -   The recruitment of [FADD]({{< relref "fas_associated_death_domain" >}}) was increased in the absence of TRADD, indicating that:
        1.  Recruitment of RIP to the TRAILR complex is TRADD-dependent
        2.  **The binding of TRADD and FADD to TRAILR is most likely independent and competitive**


## [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) and Other Death Receptors {#tradd--tnfr1-associated-death-domain-protein-dot-md--and-other-death-receptors}

-   [Fas]({{< relref "fas" >}})'s main function is to induce apoptosis via formation of a DISC following binding of [FasL]({{< relref "fas_ligand" >}})
    -   Includes FADD and caspase-8
-   TRADD was never reported as a component of the FAS DISC
    -   In line with this, apoptosis induced by anti-FAS antibody was unaffected in TRADD-deficient mouse embryonic fibroblasts and thymocytes


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
