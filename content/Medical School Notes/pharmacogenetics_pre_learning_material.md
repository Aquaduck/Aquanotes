+++
title = "Pharmacogenetics Pre-learning Material"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Name one drug in which severe adverse effects may depend on the presence of a specific genetic variation]({{< relref "name_one_drug_in_which_severe_adverse_effects_may_depend_on_the_presence_of_a_specific_genetic_variation" >}}) {#name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation--name-one-drug-in-which-severe-adverse-effects-may-depend-on-the-presence-of-a-specific-genetic-variation-dot-md}

<!--list-separator-->

-  From [Pharmacogenetics Pre-learning Material]({{< relref "pharmacogenetics_pre_learning_material" >}})

    <!--list-separator-->

    -  [Drug allergy]({{< relref "drug_allergy" >}})

        -   Immune hypersensitivity reactions mediated by [immunoglobulin E]({{< relref "immunoglobulin_e" >}}) and driven by [mast cells]({{< relref "mast_cell" >}})
        -   **Not pharmacogenomic effects** because specific genetic variations in the patient's germline DNA are not predictive of the response
            -   Depend on prior immune sensitizing exposure

    <!--list-separator-->

    -  Examples of pharmacogenomic effects

        -   Genetic variations in the HLA-A and HLA-B genes of the [MHC]({{< relref "major_histocompatibility_complex" >}}) can predict risk for severe toxicities ot certain drugs
            -   Not all individuals with these higher-risk variations wlil have an adverse reaction -> their risk **as a group** is higher

        <!--list-separator-->

        -  Specific drugs

            | Drug           | Gene (allele) with high risk variations | Adverse reaction                              |
            |----------------|-----------------------------------------|-----------------------------------------------|
            | Azathioprine   | TPMT (loss of function)                 | Life-threatening myelosuppression (up to 14%) |
            | Mercaptopurine |                                         |                                               |
            | Abacavir       | HLA-B (\*57:01)                         | Hypersensitivity reaction (5-8%)              |
            | Carbamazepine  | HLA-A (\*15:02, \*31:01)                | Stevens-Johnson Syndrome (SJS)                |
            | Oxcarbazepine  |                                         | Toxic Epidermal Necrolysis (TEN)              |
            |                |                                         | Eosinophilic Eruption                         |
            | Fluorouracil   | DPYD (multiple)                         | Drug toxicity                                 |
            | Rasburicase    | G6PD (any deficiency)                   | Severe acute hemolytic anemia                 |


#### [List the general treatment objectives underlying pharmacogenomic testing]({{< relref "list_the_general_treatment_objectives_underlying_pharmacogenomic_testing" >}}) {#list-the-general-treatment-objectives-underlying-pharmacogenomic-testing--list-the-general-treatment-objectives-underlying-pharmacogenomic-testing-dot-md}

<!--list-separator-->

-  From [Pharmacogenetics Pre-learning Material]({{< relref "pharmacogenetics_pre_learning_material" >}})

    <!--list-separator-->

    -  Precision (targeted) therapies

        -   Medicines **designed to exploit specific pathogenetic genetic variations** in order to have a therapeutic effect


#### [Indicate whether duplication of an entire CYP gene will increase or decrease expected drug effect, when the encoded P450 enzyme metabolizes a pro-drug to the active drug.]({{< relref "indicate_whether_duplication_of_an_entire_cyp_gene_will_increase_or_decrease_expected_drug_effect_when_the_encoded_p450_enzyme_metabolizes_a_pro_drug_to_the_active_drug" >}}) {#indicate-whether-duplication-of-an-entire-cyp-gene-will-increase-or-decrease-expected-drug-effect-when-the-encoded-p450-enzyme-metabolizes-a-pro-drug-to-the-active-drug-dot--indicate-whether-duplication-of-an-entire-cyp-gene-will-increase-or-decrease-expected-drug-effect-when-the-encoded-p450-enzyme-metabolizes-a-pro-drug-to-the-active-drug-dot-md}

<!--list-separator-->

-  From [Pharmacogenetics Pre-learning Material]({{< relref "pharmacogenetics_pre_learning_material" >}})

    <!--list-separator-->

    -  [Pro-drug]({{< relref "pro_drug" >}})

        -   A drug that is initially **inactive** -> **metabolism activates it**
        -   Duplication of a CYP gene -> **increase in available activity** -> individual classified as an ultrarapid metabolizer
            -   This can lead to **increased risk of overdose** since the increased reactivity competitively inhibits inactivation

    <!--list-separator-->

    -  [Active drug]({{< relref "active_drug" >}})

        -   A drug that is initially **active** -> **metabolism inactivates it**
        -   Duplication of a CYP gene -> **decrease in available activity**
            -   Drug is very quickly inactivated, preventing the drug from reaching therapeutic doses


#### [Identify the five P450 enzyme metabolizer status phenotypes that may guide drug selection or dosing]({{< relref "identify_the_five_p450_enzyme_metabolizer_status_phenotypes_that_may_guide_drug_selection_or_dosing" >}}) {#identify-the-five-p450-enzyme-metabolizer-status-phenotypes-that-may-guide-drug-selection-or-dosing--identify-the-five-p450-enzyme-metabolizer-status-phenotypes-that-may-guide-drug-selection-or-dosing-dot-md}

<!--list-separator-->

-  From [Pharmacogenetics Pre-learning Material]({{< relref "pharmacogenetics_pre_learning_material" >}})

    <!--list-separator-->

    -  Five types of [P450]({{< relref "cytochrome_p450" >}}) enzyme metabolizers

        1.  _Poor metabolizer_: markedly reduced or absent enzymatic activity -> **cannot be relied on to metabolize drugs or pro-drugs**
        2.  _Intermediate metabolizer_: **reduced but still adequate metabolic capacity** depending on the context
        3.  _Normal (extensive) metabolizer_: baseline enzymatic activity -> **do not require any adjustment in dose or drug choice**
        4.  _Rapid metabolizer_: **mild increase in enzymatic activity**
        5.  _Ultrarapid metabolizer_: **greatly increased enzymatic activity**


### Unlinked references {#unlinked-references}

[Show unlinked references]
