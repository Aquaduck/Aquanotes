+++
title = "Infarction"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Myocardial infarction]({{< relref "myocardial_infarction" >}}) {#myocardial-infarction--myocardial-infarction-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Infarction]({{< relref "infarction" >}}) of [Heart]({{< relref "heart" >}}) tissue due to prolonged [Ischemia]({{< relref "ischemia" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology**

    <!--list-separator-->

    -  [Infarction]({{< relref "infarction" >}})

        -   Tissue necrosis that results from insufficient [Blood]({{< relref "blood" >}}) nad [Oxygen]({{< relref "oxygen" >}}) supply to affected region
        -   Etiologies include:
            -   Thromboembolic occlusion
            -   Rupture
            -   Vasoconstriction
            -   Vessel compression


### Unlinked references {#unlinked-references}

[Show unlinked references]
