+++
title = "Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout."
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Lippincott's Illustrated Reviews: Biochemistry, 5e]({{< relref "lippincott_s_illustrated_reviews_biochemistry_5e" >}}) {#from-lippincott-s-illustrated-reviews-biochemistry-5e--lippincott-s-illustrated-reviews-biochemistry-5e-dot-md}


### [Salvage pathway for purines]({{< relref "salvage_pathway_for_purines" >}}) (p. 294) {#salvage-pathway-for-purines--salvage-pathway-for-purines-dot-md----p-dot-294}

-   [Purines]({{< relref "purine" >}}) that result from normal turnover of cellular nucleic acids or the small amount obtained from diet that aren't degraded -> converted to nucleoside triphosphates


#### Conversion of purine bases to nucleotides {#conversion-of-purine-bases-to-nucleotides}

-   Two enzymes are involved:
    1.  [Adenine phosphoribosyltransferase]({{< relref "adenine_phosphoribosyltransferase" >}}) (APRT)
    2.  [Hypoxanthine-guanine phosphoribosyltransferase]({{< relref "hypoxanthine_guanine_phosphoribosyltransferase" >}}) (HGPRT)
-   **Both** use [PRPP]({{< relref "phosphoribosyl_diphosphate" >}}) as the source of ribose 5-phosphate
    -   Hydrolysis by [pyrophosphatase]({{< relref "pyrophosphatase" >}}) makes these reactions **irreversible**


### Degradation of [Purine]({{< relref "purine" >}}) Nucleotides (p. 296) {#degradation-of-purine--purine-dot-md--nucleotides--p-dot-296}


#### Degradation of dietary [nucleic acids]({{< relref "nucleic_acid" >}}) in the [small intestine]({{< relref "small_intestine" >}}) {#degradation-of-dietary-nucleic-acids--nucleic-acid-dot-md--in-the-small-intestine--small-intestine-dot-md}

-   Ribonucleases and deoxyribonucleases are secreted by the [pancreas]({{< relref "pancreas" >}})
    -   Hydrolyze dietary RNA and DNA primarily to [oligonucleotides]({{< relref "oligonucleotide" >}})
    -   [Oligonucleotides]({{< relref "oligonucleotide" >}}) are further hydrolyzed by pancreatic [phosphodiesterases]({{< relref "phosphodiesterase" >}}) -> produces a mixture of 3' and 5'-mononucleotides
-   Dietary purine bases are **not used for the synthesis of tissue nucleic acids**
    -   They are instead converted to [uric acid]({{< relref "uric_acid" >}}) in intestinal mucosal cells
    -   Most of the [Uric acid]({{< relref "uric_acid" >}}) enters the blood -> excreted in urine


#### Formation of [uric acid]({{< relref "uric_acid" >}}) {#formation-of-uric-acid--uric-acid-dot-md}

-   Steps:
    1.  An amino group is removed from [AMP]({{< relref "adenosine_monophosphate" >}}) to produce [IMP]({{< relref "inosine_monophosphate" >}}) by [AMP deaminase]({{< relref "amp_deaminase" >}})
        -   Alternatively, an amino group is removed from [adenosine]({{< relref "adenosine" >}}) -> produces [inosine]({{< relref "inosine" >}}) (hypoxanthine-ribose) by [adenosine deaminase]({{< relref "adenosine_deaminase" >}})
    2.  [IMP]({{< relref "inosine_monophosphate" >}}) and [GMP]({{< relref "guanosine_monophosphate" >}}) are converted into their nucleoside forms (inosine and guanosine) by the action of [5'-nucleotidase]({{< relref "5_nucleotidase" >}})
    3.  [Purine nucleoside phosphorylase]({{< relref "purine_nucleoside_phosphorylase" >}}) converts [Inosine]({{< relref "inosine" >}}) and [guanosine]({{< relref "guanosine" >}}) into their respective purine bases, [hypoxanthine]({{< relref "hypoxanthine" >}}) and [guanine]({{< relref "guanine" >}})
    4.  [Guanine]({{< relref "guanine" >}}) is deaminated to form [xanthine]({{< relref "xanthine" >}})
    5.  Hypoxanthine is oxidized by [xanthine oxidase]({{< relref "xanthine_oxidase" >}}) to [Xanthine]({{< relref "xanthine" >}}) -> further oxidized by xanthine oxidase to [Uric acid]({{< relref "uric_acid" >}})


#### Diseases associated with purine degradation {#diseases-associated-with-purine-degradation}

<!--list-separator-->

-  [Gout]({{< relref "gout" >}})

    -   Characterized by high levels of [Uric acid]({{< relref "uric_acid" >}}) in the blood ([hyperuricemia]({{< relref "hyperuricemia" >}})) as a result of either **overproduction** or **underexcretion** of **uric acid**
    -   [Hyperuricemia]({{< relref "hyperuricemia" >}}) -> deposition of monosodium urate crystlas in the joints -> triggers inflammatory response to the crystals -> _gouty arthritis_

    <!--list-separator-->

    -  Underexcretion of [uric acid]({{< relref "uric_acid" >}})

        -   The cause of the vast majority of [Gout]({{< relref "gout" >}}) cases

    <!--list-separator-->

    -  Overproduction of [uric acid]({{< relref "uric_acid" >}})

        -   Less common cause of [Gout]({{< relref "gout" >}})
        -   Mutations in X-linked [PRPP synthetase]({{< relref "prpp_synthetase" >}}) gene -> increased V<sub>max</sub> for the production of PRPP + lower K<sub>m</sub> for ribose 5-phosphate, or decresaed sensitivity to purine nucleotides (allosteric inhibitors)
            -   Increased availability of PRPP -> **increased purine production** -> **elevated levels of plasma uric acid**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-05 Tue] </span></span> > Gout > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
