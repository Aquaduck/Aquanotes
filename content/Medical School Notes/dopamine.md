+++
title = "Dopamine"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Major physiological functions of the kidney > Hormone synthesis**

    [Dopamine]({{< relref "dopamine" >}})

    ---


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Carotid & Aortic bodies (p. 671)**

    The principle transmitter appears to be [dopamine]({{< relref "dopamine" >}}) -> excites the nerve endings through [D<sub>2</sub> receptors]({{< relref "dopamine_d2_receptor" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
