+++
title = "Ventricular septal defect"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   An abnormal communication between the [left]({{< relref "left_ventricle" >}}) and [right ventricle]({{< relref "right_ventricle" >}}) -> [left to right shunting]({{< relref "left_to_right_shunt" >}}) of blood flow


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}}) {#tetralogy-of-fallot--tetralogy-of-fallot-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) (VSD)

    ---


#### [Overriding aorta]({{< relref "overriding_aorta" >}}) {#overriding-aorta--overriding-aorta-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Aorta]({{< relref "aorta" >}}) is displaced over the [ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) (instead of the left ventricle).

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Tetralogy of Fallot > Overview**

    <!--list-separator-->

    -  [Ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) (VSD)

        -   An abnormal communication between the left and right ventricle -> [left to right shunting]({{< relref "left_to_right_shunt" >}}) of blood flow
        -   The most common congenital heart defect
        -   Manifests as a loud, harsh holosystolic murmur best appreciated at the left sternal border.


### Unlinked references {#unlinked-references}

[Show unlinked references]
