+++
title = "Tetralogy of Fallot"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   The simultaneous occurrence of four [cardiac]({{< relref "heart" >}}) defects:
    1.  [Right ventricular outflow tract obstruction]({{< relref "right_ventricular_outflow_tract_obstruction" >}}) (RVOTO) (due to pulmonary infundibular stenosis)
    2.  [Right ventricular hypertrophy]({{< relref "right_ventricular_hypertrophy" >}}) (RVH)
    3.  [Ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) (VSD)
    4.  [Overriding aorta]({{< relref "overriding_aorta" >}}) (above the VSD)


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}}) is associated with [DiGeorge syndrome]({{< relref "digeorge_syndrome" >}})

    ---


#### [DiGeorge syndrome]({{< relref "digeorge_syndrome" >}}) {#digeorge-syndrome--digeorge-syndrome-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Immunology > DiGeorge syndrome > Clinical features: > Cardiac anomalies**

    [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}})

    ---

<!--list-separator-->

-  **🔖 Cardiology**

    <!--list-separator-->

    -  [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}})

        <!--list-separator-->

        -  Overview

            -   The simultaneous occurrence of four defects:

            <!--list-separator-->

            -  [Right ventricular outflow tract obstruction]({{< relref "right_ventricular_outflow_tract_obstruction" >}}) (RVOTO) (due to pulmonary infundibular stenosis)

                -   Inability of blood to pass out of the right ventricle through the pulmonic valve.
                -   Etiologies include pulmonic valve stenosis, defects in the infundibulum, and defects in the pulmonary artery.

            <!--list-separator-->

            -  [Right ventricular hypertrophy]({{< relref "right_ventricular_hypertrophy" >}}) (RVH)

                -   A thickening of the muscular wall of the right ventricle of the heart
                    -   Usually due to chronic pumping against increased resistance
                        -   E.g. pulmonary stenosis, pulmonary hypertension, chronic lung disease

            <!--list-separator-->

            -  [Ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) (VSD)

                -   An abnormal communication between the left and right ventricle -> [left to right shunting]({{< relref "left_to_right_shunt" >}}) of blood flow
                -   The most common congenital heart defect
                -   Manifests as a loud, harsh holosystolic murmur best appreciated at the left sternal border.

            <!--list-separator-->

            -  [Overriding aorta]({{< relref "overriding_aorta" >}}) (above the VSD)

                -   Aorta is displaced over the ventricular septal defect (instead of the left ventricle).
                -   Causes blood from both ventricles to flow into the aorta.


### Unlinked references {#unlinked-references}

[Show unlinked references]
