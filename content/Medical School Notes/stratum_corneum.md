+++
title = "Stratum corneum"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Keratin layer of the [epidermis]({{< relref "epidermis" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}}) {#name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot--name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Epidermis - strata**

    <!--list-separator-->

    -  [Stratum corneum]({{< relref "stratum_corneum" >}}) - keratin layer

        -   Skeletal remains of dead cells that are still held together by desmosomes
        -   [Keratin]({{< relref "keratin" >}}) and thick plasma membranes form a waxy barrier


### Unlinked references {#unlinked-references}

[Show unlinked references]
