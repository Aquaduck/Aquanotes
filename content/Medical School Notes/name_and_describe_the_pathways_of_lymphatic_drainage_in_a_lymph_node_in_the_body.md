+++
title = "Name and describe the pathways of lymphatic drainage (in a lymph node, in the body)."
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}}) {#from-lymphatic-and-immune-systems-ppt--lymphatic-and-immune-systems-ppt-dot-md}


### [Lymph]({{< relref "lymph" >}}) {#lymph--lymph-dot-md}

-   Lymph gradually moves **centrally** in [lymphatic channels]({{< relref "lymphatic_channel" >}}) where it flows into the [vascular system]({{< relref "vascular_system" >}}) at the **base of the neck**
    -   Forward movement occurs with pressures created by macroscopic movements of the extremities aided by small luminal valves in lymphatic channels
-   Lymph is filtered through groups of [lymph nodes]({{< relref "lymph_node" >}}) as it moves centrally
-   The absence of lymph flow can be detected by swelling of the feet if a person stands motionless for a long period of time
-   Fluid accumulation, clinically called [edema]({{< relref "edema" >}}), is a frequent syndrome in patients with **cardiovascular, renal, and protein abnormalities**
-   Daily lymph flow is about 4-5 liters


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon] </span></span> > Lymphatic and Immune Systems > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Name and describe the pathways of lymphatic drainage (in a lymph node, in the body).]({{< relref "name_and_describe_the_pathways_of_lymphatic_drainage_in_a_lymph_node_in_the_body" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
