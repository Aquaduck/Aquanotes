+++
title = "Lipid"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Fatty acid]({{< relref "fatty_acid" >}}) {#fatty-acid--fatty-acid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The main structural component of [lipids]({{< relref "lipid" >}})

    ---


#### [Dietary lipid]({{< relref "dietary_lipid" >}}) {#dietary-lipid--dietary-lipid-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Lipids]({{< relref "lipid" >}}) used as energy storage

    ---


#### [Beta globulin]({{< relref "beta_globulin" >}}) {#beta-globulin--beta-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Transport [iron]({{< relref "iron" >}}), [lipids]({{< relref "lipid" >}}), and the fat-soluble vitamins [A]({{< relref "vitamin_a" >}}), [D]({{< relref "vitamin_d" >}}), [E]({{< relref "vitamin_e" >}}), and [K]({{< relref "vitamin_k" >}}) to cells

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Atherosclerosis**

    Formation of [lipid]({{< relref "lipid" >}}), [cholesterol]({{< relref "cholesterol" >}}), and/or [calcium]({{< relref "calcium" >}})-laden plaques within [tunica intima]({{< relref "tunica_intima" >}}) of arterial wall -> restricts [blood]({{< relref "blood" >}}) flow

    ---


#### [Alpha globulin]({{< relref "alpha_globulin" >}}) {#alpha-globulin--alpha-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Transport [iron]({{< relref "iron" >}}), [lipids]({{< relref "lipid" >}}), and the fat-soluble vitamins [A]({{< relref "vitamin_a" >}}), [D]({{< relref "vitamin_d" >}}), [E]({{< relref "vitamin_e" >}}), and [K]({{< relref "vitamin_k" >}}) to cells

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
