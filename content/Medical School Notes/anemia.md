+++
title = "Anemia"
author = ["Arif Ahsan"]
date = 2021-08-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Sickle cell disease]({{< relref "sickle_cell_disease" >}}) {#sickle-cell-disease--sickle-cell-disease-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Sickling of [RBCs]({{< relref "red_blood_cell" >}}) causes RBCs to breakdown prematurely -> leads to [anemia]({{< relref "anemia" >}})

    ---


#### [Pyruvate kinase]({{< relref "pyruvate_kinase" >}}) {#pyruvate-kinase--pyruvate-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Deficiencies in the RBC isozyme cause hemolytic [anemia]({{< relref "anemia" >}})

    ---


#### [FoCS (Sickle Cell Case)]({{< relref "focs_sickle_cell_case" >}}) {#focs--sickle-cell-case----focs-sickle-cell-case-dot-md}

<!--list-separator-->

-  **🔖 Introduction**

    Causes RBCs to breakdown prematurely -> leads to [anemia]({{< relref "anemia" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
