+++
title = "Alpha-1 receptor"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Catecholamine]({{< relref "catecholamine" >}}) ([norepinephrine]({{< relref "norepinephrine" >}})) binding to an [alpha-1 receptor]({{< relref "alpha_1_receptor" >}}) -> vasoconstriction -> decreased blood flow

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
