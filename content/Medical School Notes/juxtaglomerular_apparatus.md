+++
title = "Juxtaglomerular apparatus"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}}) {#explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result--explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology**

    <!--list-separator-->

    -  [Juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}}) (p. 21)

        -   Composed of three cell types

        <!--list-separator-->

        -  [Granular cells]({{< relref "granular_cell" >}})

            -   Differentiated smooth muscle cells in the walls of the [afferent arterioles]({{< relref "afferent_arteriole_of_the_kidney" >}})
                -   Can sense blood pressure
            -   Contain secretory granules -> contain [Renin]({{< relref "renin" >}})

        <!--list-separator-->

        -  Extraglomerular [mesangial cells]({{< relref "mesangial_cell" >}})

            -   Morphologically similar to + continuous with the glomerular mesangial cells, **but lie outside the Bowman's capsule**

        <!--list-separator-->

        -  [Macula densa]({{< relref "macula_densa" >}}) cells

            -   Detectors of flow rate and composition of fluid in nephron at th every end of the [thick ascending limb]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}})
            -   Contribute to the control of [glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}) and to the control of [renin]({{< relref "renin" >}}) secretion


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Renin + Angiotensin**

    Specialized cells in the [kidneys]({{< relref "kidney" >}}) found in the [juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}}) respond to decreased blood flow by secreting _renin_ into the blood

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Macula densa**

    Part of the [Juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
