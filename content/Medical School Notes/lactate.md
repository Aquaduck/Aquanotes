+++
title = "Lactate"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Basic form of [lactic acid]({{< relref "lactic_acid" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Lactic acid]({{< relref "lactic_acid" >}}) {#lactic-acid--lactic-acid-dot-md}

<!--list-separator-->

-  **🔖 Concept link**

    Acid form of [Lactate]({{< relref "lactate" >}})

    ---


#### [Lactate dehydrogenase]({{< relref "lactate_dehydrogenase" >}}) {#lactate-dehydrogenase--lactate-dehydrogenase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The only way in which [lactate]({{< relref "lactate" >}}) is utilized physiologically

    ---


#### [Hypoxia-inducible factor 1]({{< relref "hypoxia_inducible_factor_1" >}}) {#hypoxia-inducible-factor-1--hypoxia-inducible-factor-1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Increased [Lactate]({{< relref "lactate" >}}) concentrations upregulate HIF-1

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
