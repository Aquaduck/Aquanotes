+++
title = "B-cell lymphoma 2"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A member of the [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026)**

    <!--list-separator-->

    -  [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) and [BclX<sub>L</sub>]({{< relref "bclx_l" >}})

        -   **Anti-apoptotic**
        -   Bind to pro-apoptotic Bcl2 family proteins -> inhibit apoptosis


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 HIF-1 target genes involved in glucose and energy metabolism**

    HIF-1 activates transcription of the gene encoding the [BH3-only protein]({{< relref "bh3_only_protein" >}}) BNIP3 -> induces selective mitochondrial autophagy by competing with [Beclin 1]({{< relref "beclin_1" >}}) for binding to [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) -> allows Beclin 1 to trigger autophagy

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
