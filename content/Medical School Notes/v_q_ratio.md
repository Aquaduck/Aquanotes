+++
title = "V/Q ratio"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [Ventilation]({{< relref "ventilation" >}})-[perfusion]({{< relref "perfusion" >}}) ratio


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [V/Q mismatch]({{< relref "v_q_mismatch" >}}) {#v-q-mismatch--v-q-mismatch-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An imbalance in the [V/Q ratio]({{< relref "v_q_ratio" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology**

    <!--list-separator-->

    -  [V/Q ratio]({{< relref "v_q_ratio" >}})

        -   The volumetric ratio of air that reaches the alveoli ([ventilation]({{< relref "ventilation" >}})) to alveolar blood supply ([perfusion]({{< relref "perfusion" >}})) per minute
        -   The ideal is **1**
        -   Average V/Q ratio is **0.8**
        -   In the upright position, the lung bases are better ventilated and perfused than the apices


### Unlinked references {#unlinked-references}

[Show unlinked references]
