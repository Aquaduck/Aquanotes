+++
title = "Inhibitor of apoptosis"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Bind to and inhibit activated [caspases]({{< relref "caspase" >}})
-   Polyubiquitylate caspases -> mark for destruction by [proteasomes]({{< relref "proteasome" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Smac, a Mitochondrial Protein that Promotes Cytochrome c–Dependent Caspase Activation by Eliminating IAP Inhibition]({{< relref "smac_a_mitochondrial_protein_that_promotes_cytochrome_c_dependent_caspase_activation_by_eliminating_iap_inhibition" >}}) {#smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition--smac-a-mitochondrial-protein-that-promotes-cytochrome-c-dependent-caspase-activation-by-eliminating-iap-inhibition-dot-md}

<!--list-separator-->

-  **🔖 Discussion > SMAC promotes procaspase-9 activation by countering IAPs**

    SMAC is released from mitochondria during apoptosis -> neutralizes the inhibitory activity of [IAPs]({{< relref "inhibitor_of_apoptosis" >}})

    ---


#### [Second mitochondria-derived activator of caspases]({{< relref "second_mitochondria_derived_activator_of_caspases" >}}) {#second-mitochondria-derived-activator-of-caspases--second-mitochondria-derived-activator-of-caspases-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Binds [IAPs]({{< relref "inhibitor_of_apoptosis" >}}) to free [caspases]({{< relref "caspase" >}}) and activate apoptosis

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  [IAPs]({{< relref "inhibitor_of_apoptosis" >}})

        -   All IAPs have one or more [baculovirus IAP repeat]({{< relref "baculovirus_iap_repeat" >}}) (BIR) domains
            -   Enable them to bind to and inhibit activated caspases
        -   Some IAPs also polyubiquitylate [caspases]({{< relref "caspase" >}}) -> marking them for destruction by proteasomes


#### [Baculovirus IAP repeat]({{< relref "baculovirus_iap_repeat" >}}) {#baculovirus-iap-repeat--baculovirus-iap-repeat-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Allows [IAPs]({{< relref "inhibitor_of_apoptosis" >}}) to bind + inhibit activated caspases

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
