+++
title = "Glycogenesis"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [List the glucose-utilizing pathways that predominate in:]({{< relref "list_the_glucose_utilizing_pathways_that_predominate_in" >}}) {#list-the-glucose-utilizing-pathways-that-predominate-in--list-the-glucose-utilizing-pathways-that-predominate-in-dot-md}

<!--list-separator-->

-  **🔖 Liver**

    [Glycogenesis]({{< relref "glycogenesis" >}})

    ---

<!--list-separator-->

-  **🔖 Muscle**

    [Glycogenesis]({{< relref "glycogenesis" >}})

    ---

<!--list-separator-->

-  **🔖 Brain**

    [Glycogenesis]({{< relref "glycogenesis" >}})

    ---


#### [Glucose 6-phosphate]({{< relref "glucose_6_phosphate" >}}) {#glucose-6-phosphate--glucose-6-phosphate-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Signals [muscle]({{< relref "muscle" >}}) cells to start [synthesizing]({{< relref "glycogenesis" >}}) [glycogen]({{< relref "glycogen" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
