+++
title = "Ligamentum arteriosum"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the relevant anatomy and disease etiology and where applicable, the clinical presentation of disorders associated with the superior mediastinum.]({{< relref "describe_the_relevant_anatomy_and_disease_etiology_and_where_applicable_the_clinical_presentation_of_disorders_associated_with_the_superior_mediastinum" >}}) {#describe-the-relevant-anatomy-and-disease-etiology-and-where-applicable-the-clinical-presentation-of-disorders-associated-with-the-superior-mediastinum-dot--describe-the-relevant-anatomy-and-disease-etiology-and-where-applicable-the-clinical-presentation-of-disorders-associated-with-the-superior-mediastinum-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Coarctation of the Aorta**

    Most common site is near [ligamentum arteriosum]({{< relref "ligamentum_arteriosum" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
