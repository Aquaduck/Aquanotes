+++
title = "Alveoli"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Part of the [lung]({{< relref "lung" >}}) where gas exchange occurs


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Interstitial lung disease]({{< relref "interstitial_lung_disease" >}}) {#interstitial-lung-disease--interstitial-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Often marked by inflammatory changes in the [alveoli]({{< relref "alveoli" >}})

    ---


#### [Alveolar dead space]({{< relref "alveolar_dead_space" >}}) {#alveolar-dead-space--alveolar-dead-space-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Sum of the volumes of [alveoli]({{< relref "alveoli" >}}) that do not participat ein gas exchange

    ---


#### [A-a gradient]({{< relref "a_a_gradient" >}}) {#a-a-gradient--a-a-gradient-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Difference in partial pressure of [oxygen]({{< relref "oxygen" >}}) in the [alveoli]({{< relref "alveoli" >}}) (A) and the partial pressure of oxygen in the arteries (a)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
