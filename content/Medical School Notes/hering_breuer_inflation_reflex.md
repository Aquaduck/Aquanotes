+++
title = "Hering-Breuer inflation reflex"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Inhibits [Inspiration]({{< relref "inspiration" >}}) to prevent overinflation of the [lungs]({{< relref "lung" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration**

    <!--list-separator-->

    -  [Hering-Breuer inflation reflex]({{< relref "hering_breuer_inflation_reflex" >}})

        -   Inhibits inspiration to prevent overinflation of the lungs and alveolar damage
        -   Mediated by pulmonary stretch receptors and vagal afferents


### Unlinked references {#unlinked-references}

[Show unlinked references]
