+++
title = "Lipoic acid"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links > Cofactors/Coenzymes**

    [Lipoic acid]({{< relref "lipoic_acid" >}})

    ---


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Pyruvate DH complex**

    [Lipoic acid]({{< relref "lipoic_acid" >}})

    ---


#### [Kaplan Biochemistry Chapter 10]({{< relref "kaplan_biochemistry_chapter_10" >}}) {#kaplan-biochemistry-chapter-10--kaplan-biochemistry-chapter-10-dot-md}

<!--list-separator-->

-  **🔖 10.1 Acetyl-CoA > Methods of Forming Acetyl-CoA > Pyruvate dehydrogenase complex enzymes:**

    FAD used as coenzyme to **reoxidize [lipoic acid]({{< relref "lipoic_acid" >}})** -> allows lipoic acid to be reused

    ---

<!--list-separator-->

-  **🔖 10.1 Acetyl-CoA > Methods of Forming Acetyl-CoA > Pyruvate dehydrogenase complex enzymes:**

    2-carbon molecule bonded to TPP oxidized and transferred to _[lipoic acid]({{< relref "lipoic_acid" >}})_, a coenzyme

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
