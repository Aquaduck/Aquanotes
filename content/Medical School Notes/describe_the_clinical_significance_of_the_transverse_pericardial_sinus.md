+++
title = "Describe the clinical significance of the transverse pericardial sinus."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### Surgical significance of the [transverse pericardial sinus]({{< relref "transverse_pericardial_sinus" >}}) {#surgical-significance-of-the-transverse-pericardial-sinus--transverse-pericardial-sinus-dot-md}

-   After pericardial sac is opened anteriorly, a finger can be passed through the transverse pericardial sinus posterior to ascending aorta and pulmonary trunk
-   By passing a surgical clamp or a ligature around these large vessels, inserting tubes of a coronary bypass machine, and then tightening ligature, surgeons can **stop or divert circulation of blood in these arteries while performing cardiac surgery**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the clinical significance of the transverse pericardial sinus.]({{< relref "describe_the_clinical_significance_of_the_transverse_pericardial_sinus" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
