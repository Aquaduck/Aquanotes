+++
title = "Death receptor"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   "[Death domains]({{< relref "death_domain" >}})" (characteristic feature of DRs) activate [caspases]({{< relref "caspase" >}}) as part of the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}})
-   Subgroup of the [TNFR]({{< relref "tumor_necrosis_factor_receptor" >}}) superfamily
-   Includes:
    -   [TNFR1]({{< relref "tnfr1" >}})
    -   [Fas]({{< relref "fas" >}})
    -   DR3
    -   DR4
    -   DR5
    -   DR6
    -   NGFR
    -   EDAR


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Tumor necrosis factor]({{< relref "tumor_necrosis_factor" >}}) {#tumor-necrosis-factor--tumor-necrosis-factor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Binds to a [Death receptor]({{< relref "death_receptor" >}}) on the cell surface

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Bcl2 family > Bcl2 proteins regulate the intrinsic pathway of apoptosis (p. 1026) > BH3-only proteins > Bid**

    When [death receptors]({{< relref "death_receptor" >}}) activate the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}), [Caspase-8]({{< relref "caspase_8" >}}) cleaves Bid -> activates Bid -> translocates to outer mitochondrial membrane -> inhibits anti-apoptotic Bcl2 family proteins -> **amplification of death signal**

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition**

    <!--list-separator-->

    -  [Death receptors]({{< relref "death_receptor" >}})

        <!--list-separator-->

        -  Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024)

            -   Death receptors are transmembrane proteins containing:
                -   An extracellular ligand-binding domain
                -   A single transmembrane domain
                -   An intracellular [death domain]({{< relref "death_domain" >}}) -> required for receptor to activate [apoptosis]({{< relref "apoptosis" >}})
            -   Death receptors are homotrimers and belong to the [tumor necrosis factor]({{< relref "tumor_necrosis_factor_receptor" >}}) (TNF) **receptor** family
                -   The ligands that activate death receptors are also homotrimers and are part of the [TNF]({{< relref "tumor_necrosis_factor" >}}) family of **signal proteins**


#### [Fas]({{< relref "fas" >}}) {#fas--fas-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [death receptor]({{< relref "death_receptor" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
