+++
title = "Connective tissue"
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Perichondrium]({{< relref "perichondrium" >}}) {#perichondrium--perichondrium-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    Dense irregular [CT]({{< relref "connective_tissue" >}})

    ---


#### [Name the types of cellular and acellular components of connective tissue]({{< relref "name_the_types_of_cellular_and_acellular_components_of_connective_tissue" >}}) {#name-the-types-of-cellular-and-acellular-components-of-connective-tissue--name-the-types-of-cellular-and-acellular-components-of-connective-tissue-dot-md}

<!--list-separator-->

-  **🔖 From Introduction to Connective Tissue (CT) Pre-workshop Reading**

    <!--list-separator-->

    -  Four major components that make up [connective tissue]({{< relref "connective_tissue" >}})

        <!--list-separator-->

        -  Fibrous structural proteins

        <!--list-separator-->

        -  [Ground substance]({{< relref "ground_substance" >}})

        <!--list-separator-->

        -  Adhesion proteins

        <!--list-separator-->

        -  Cells


#### [Name and describe the basic types of connective tissues]({{< relref "name_and_describe_the_basic_types_of_connective_tissues" >}}) {#name-and-describe-the-basic-types-of-connective-tissues--name-and-describe-the-basic-types-of-connective-tissues-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology**

    <!--list-separator-->

    -  Classification of [connective tissue]({{< relref "connective_tissue" >}}) (p. 86)

        <!--list-separator-->

        -  Connective tissue proper

            <!--list-separator-->

            -  Loose CT

                -   Fewer fibers but more cells than dense CT
                -   Well vascularized, flexible, and not resistance to stress
                -   More abundant than dense CT
                -   Fills in spaces just deep to the skin

            <!--list-separator-->

            -  Dense CT

                -   More fibers but fewer cells than loose CT

                <!--list-separator-->

                -  Dense irregular CT

                    -   Most common
                    -   Contains fiber bundles that have **no definite orientation**
                    -   Characteristic of the dermis and capsules of many organs

                <!--list-separator-->

                -  Dense regular CT

                    -   Contains fiber bundles and attenuated fibroblasts arranged in a **uniform parallel fashion**
                    -   **Present only in tendons and ligaments**
                    -   May be collagenous or elastic


### Unlinked references {#unlinked-references}

[Show unlinked references]
