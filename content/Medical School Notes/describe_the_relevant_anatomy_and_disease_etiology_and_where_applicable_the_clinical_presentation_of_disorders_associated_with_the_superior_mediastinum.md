+++
title = "Describe the relevant anatomy and disease etiology and where applicable, the clinical presentation of disorders associated with the superior mediastinum."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### Aneurysm of the Ascending Aorta {#aneurysm-of-the-ascending-aorta}

-   Distal part of ascending aorta receives a strong thrust of blood when the left ventricle contracts
    -   Wall is not yet reinforced by fibrous pericardium -> aneurysm may develop
-   Evident on chest film or MR angiogram as **an enlarged area of ascending aorta silhouette**
-   Chest pain that radiates to back
-   May exert pressure on trachea, esophagus, and recurrent laryngeal nerve -> difficulty in breathing and swallowing


### Coarctation of the Aorta {#coarctation-of-the-aorta}

-   Arch of aorta or thoracic aorta has stenosis -> diminishes caliber of aortic lumen -> obstructed blood flow to inferior body
-   Most common site is near [ligamentum arteriosum]({{< relref "ligamentum_arteriosum" >}})
-   When coarctation is inferior to this site (_postductal coarctation_) -> good collateral circulation develops between proximal and distal parts of aorta through intercostal and internal thoracic arteries
    -   Compatible with many years of life because of collateral circulation


### Injury to [recurrent laryngeal nerve]({{< relref "recurrent_laryngeal_nerve" >}}) {#injury-to-recurrent-laryngeal-nerve--recurrent-laryngeal-nerve-dot-md}

-   Recurrent laryngeal nerve supplies all intrinsic muscle of larynx except one
    -   Any diagnostic procedure or disease in superior mediastinum may injure these nerves -> affect voice
-   Left recurrent laryngeal nerve winds around arch of aorta and ascends between trachea and esophagus -> may be involved in:
    1.  Bronchogenic or esophageal carcinoma
    2.  Enlargement of mediastinal lymph nodes
    3.  Aneurysm of arch of aorta
        -   Nerve may be stretched by the dilated arch


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the relevant anatomy and disease etiology and where applicable, the clinical presentation of disorders associated with the superior mediastinum.]({{< relref "describe_the_relevant_anatomy_and_disease_etiology_and_where_applicable_the_clinical_presentation_of_disorders_associated_with_the_superior_mediastinum" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
