+++
title = "Histology: Bone Lab Pre-work"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  From [Histology: Bone Lab Pre-work]({{< relref "histology_bone_lab_pre_work" >}})

    <!--list-separator-->

    -  Organization of [bones]({{< relref "bone" >}})

        -   Bone is mostly hollow
            -   The dense outer part is called _cortical_ or _compact_ bone
        -   Porous bone is called _[spongy]({{< relref "spongy_bone" >}})_ or _cancellous_ bone
            -   Composed of thin _[trabeculae]({{< relref "trabeculae" >}})_ of bone
        -   Remaining non-bony inside part is called [bone marrow]({{< relref "bone_marrow" >}}) or _medullary cavity_
            -   [Red marrow]({{< relref "red_marrow" >}}) contains hematopoietic cells
            -   [Yellow marrow]({{< relref "yellow_marrow" >}}) contains only adipocytes
        -   Outer bone covered by a thin layer of dense irregular CT called the [periosteum]({{< relref "periosteum" >}})
            -   Encases **all bone except at joints** where there is a thin layer of cartilage
        -   Cortical bone and trabeculae are lined by an extremely thin layer of CT called [endosteum]({{< relref "endosteum" >}})
            -   Contains progenitor cells for bone
            -   Endosteum is typically not clearly visible on H&E sections

    <!--list-separator-->

    -  Parts of a [tubular bone]({{< relref "tubular_bone" >}})

        -   3 named regions:
            1.  [Diaphysis]({{< relref "diaphysis" >}}):  mid-shaft of bone
            2.  [Metaphysis]({{< relref "metaphysis" >}}): expanding part of bone next to the growth plate
            3.  [Epiphysis]({{< relref "epiphysis" >}}): end of the bone adjacent to the articular surface and including the growth plate
        -   A [Metaphysis]({{< relref "metaphysis" >}}) and [Epiphysis]({{< relref "epiphysis" >}}) are also present at the top of the bone
        -   Sometimes the [epiphyseal plate]({{< relref "epiphyseal_plate" >}}) is called the physis
            -   Area where a child's bones grow in length
            -   Once this growth plate calcifies (in teenage years), lengthening of bone is no longer possible
        -   [Nutrient canal]({{< relref "nutrient_canal" >}}) goes through the cortex of the [Diaphysis]({{< relref "diaphysis" >}}) -> blood vessel travels to nourish the marrow cavity

    <!--list-separator-->

    -  Histologic components of mature [bone]({{< relref "bone" >}})

        -   Mature bone is highly organized dense regular CT
            -   90% of the protein is [Type 1 Collagen]({{< relref "type_1_collage" >}})
        -   Complete protein matrix of bone is called the [osteoid]({{< relref "osteoid" >}})
        -   [Calcium]({{< relref "calcium" >}}) is bound to the bone in a crystlaline form called [calcium hydroxyapatite]({{< relref "calcium_hydroxyapatite" >}})
        -   Mature bone is laid down in parallel layers called [lamellae]({{< relref "lamellae" >}})
            -   External circumferential lamella just underneath the [Periosteum]({{< relref "periosteum" >}})
            -   Internal circumferential lamella just inside the cortex
            -   Lamellae of bone are also laid down on the [Trabeculae]({{< relref "trabeculae" >}}) of [Cancellous bone]({{< relref "spongy_bone" >}})
            -   Dense cortical bone between internal and external lamellae consists of [osteons]({{< relref "osteon" >}})
                -   Small tubular structure comprised of multiple concentric lamellae

    <!--list-separator-->

    -  Cytologic components of mature [bone]({{< relref "bone" >}})

        -   Three types of cells in addition to progenitor cells in endosteum:
            1.  [Osteoclasts]({{< relref "osteoclast" >}}) - resorb bone and digest the osteoid
                -   Multinucleated cells that reside in [Howship's lacunae]({{< relref "howship_s_lacunae" >}}) (irregular pits) in the bone formed by digestion of the osteoid
            2.  [Osteoblasts]({{< relref "osteoblast" >}}) - lay down new osteoid and calcify it to form new bone
                -   Sometimes line up in rows (almost look like a columnar epithelium) at the edge, adding more osteoid
                -   **Always immediately adjacent to the bone**
            3.  [Osteocytes]({{< relref "osteocyte" >}}) - emtombed in bone but highly metabolically active in [calcium]({{< relref "calcium" >}}) homeostasis
                -   Small cells encased deep into the osteoid
                -   Osteocytes communicate with one another through cell processes extending through tiny channels called [canaliculi]({{< relref "canaliculi" >}})
                -   Involved in day-to-day control of calcium levels
                -   Respond to the hormones [parathyroid hormone]({{< relref "parathyroid_hormone" >}}) and [calcitonin]({{< relref "calcitonin" >}})

    <!--list-separator-->

    -  Bone microscopic morphology

        -   Two ways to examine bone:
            1.  _Decalcification_ allows bone to be processed like other tissues
                -   [Osteoid]({{< relref "osteoid" >}}) appears dense and eosinophilic
            2.  **Grinding thin slices of bone** destroys the cells but reveals structural organization of the bone matrix, especially in the cortex

    <!--list-separator-->

    -  Bone formation

        -   Two pathways to produce bone
            -   Both initially weak
                -   Initial weak bone is called [woven bone]({{< relref "woven_bone" >}})
                    -   Poorly calcified with a haphazardly arranged collagen matrix
            -   Second remodeling step is needed to produce strong mature bone

        <!--list-separator-->

        -  [Intramembranous bone formation]({{< relref "intramembranous_bone_formation" >}})

            -   Occurs in flat bones of the skull, jaw, and the clavicles during embryologic development

        <!--list-separator-->

        -  [Endochondral bone formation]({{< relref "endochondral_bone_formation" >}})

            -   Most bones develop this way (e.g. long bones)
            -   There are **five recognizable zones** that occur in sequence from the [Epiphysis]({{< relref "epiphysis" >}}) into the [Metaphysis]({{< relref "metaphysis" >}})
                1.  Resting zone
                2.  Proliferating zone
                3.  Hypertrophic zone
                4.  Cartilage calcification zone
                5.  Ossification zone

            {{< figure src="/ox-hugo/_20210930_150657screenshot.png" caption="Figure 1: Five zones of bone development in sequence (left to right)" width="700" >}}

            <!--list-separator-->

            -  Steps

                1.  [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) first forms a model of the bone that is then converted to bone
                2.  A bone collar appears around the shaft as the center (destined to be the [Diaphysis]({{< relref "diaphysis" >}})) of the "cartilage model" and is ossified
                    -   Called the _primary center of ossification_
                    -   Ossification center expands, converting most of the cartilage (except the epiphyses) to bone with an expanding marrow cavity
                3.  Secondary centers of ossification are initiated in the middle of each [Epiphysis]({{< relref "epiphysis" >}})
                4.  A region of cartilage is preserved between the [Epiphysis]({{< relref "epiphysis" >}}) and the [Diaphysis]({{< relref "diaphysis" >}}) -> becomes the [Growth plate]({{< relref "epiphyseal_plate" >}})
                5.  A region of cartilage at the ends is preserved to form the articular surfaces
                6.  Cartilage of [Growth plate]({{< relref "epiphyseal_plate" >}}) proliferates to lengthen the bone
                7.  As proliferating cartilage of the [Growth plate]({{< relref "epiphyseal_plate" >}}) grows away from the [Diaphysis]({{< relref "diaphysis" >}}) (extending the length of the bone) -> cartilage closest to the diaphysis is **converted to ossified bone**

    <!--list-separator-->

    -  Bone remodeling

        -   Remodeling of compact cortical bone is done by a structure called a [bone-remodeling unit]({{< relref "bone_remodeling_unit" >}})
            -   A group of [osteoclasts]({{< relref "osteoclast" >}}) at the "cutting" cone resorb a round channel of bone about 200 microns in diameter while traveling parallel to the direction of the major stress on the bone
            -   A wave of [osteoblasts]({{< relref "osteoblast" >}}) follow the [osteoclasts]({{< relref "osteoclast" >}}) -> lay down concentric rings of [Osteoid]({{< relref "osteoid" >}}) -> osteoblasts differentiate into [osteocytes]({{< relref "osteocyte" >}}) -> encased in bone by the next wave of osteoblasts
            -   Blood vessels remain in the middle of the concentric rings of [Osteoid]({{< relref "osteoid" >}})
        -   The result of the [Bone-remodeling unit]({{< relref "bone_remodeling_unit" >}}) is known as an [Osteon]({{< relref "osteon" >}}) (haversian system) with a central [Haversian canal]({{< relref "haversian_canal" >}})

        {{< figure src="/ox-hugo/_20210930_151542screenshot.png" caption="Figure 2: A bone-remodeling unit schematic. Note that while it appears the BRU is drilling down the middle of an old osteon, the site of cutting is driven by stress in the bone and is unrelated with the sites of previous osteons" width="700" >}}

    <!--list-separator-->

    -  [Appositional growth]({{< relref "appositional_growth" >}}) and remodeling

        -   Compact bone may form by appositional growth of lamellae
            -   _Appositional_ - on the surface
            -   This can occur on the external surface (external circumferential lamella) or the surface of internal trabeculae
        -   Remodeling of bone occurs **during growth** and **with changing stresses on bone**
            -   In an adult, intact [osteons]({{< relref "osteon" >}}) have a half-life of many years
                -   Remodeling in a growing child or fractured adult bone is much faster
            -   A broken bone is initially fused together with a fibrous (collagenous) scar that calcifies ([Woven bone]({{< relref "woven_bone" >}})) and then undergoes remodeling by [Haversian canal]({{< relref "haversian_canal" >}})
                -   This remodeling can produce a bone with the shape and strength of the original bone

    <!--list-separator-->

    -  Joints, ligaments, and tendons

        <!--list-separator-->

        -  [Joints]({{< relref "joint" >}})

            -   Most joints are lined by [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) on the surface of each articulating bone
                -   Contacting cartilage surfaces lack a [Perichondrium]({{< relref "perichondrium" >}}), but are lubricated by synovial cells at the side of the join
            -   Joints with limited movement consist of fibrocartilage (e.g. intervertebral disks and [symphysis pubis]({{< relref "symphysis_pubis" >}}))
            -   Some joints have fibrocartilage supplementing the [Hyaline cartilage]({{< relref "hyaline_cartilage" >}}) (e.g. menisci of knee, glenoid labra, ligamentum teres and acetabular labra of hip)

        <!--list-separator-->

        -  [Ligaments]({{< relref "ligament" >}})

            -   Stablize [joints]({{< relref "joint" >}})

        <!--list-separator-->

        -  [Tendons]({{< relref "tendon" >}})

            -   Connect muscles to bones
            -   Made of dense regular CT
            -   Connections at myotendinous junction is an intertwining of dense CT with finger-like projections of sarcolemmal (skeletal muscle) membrane
            -   Dense irregular connective tissue of [Periosteum]({{< relref "periosteum" >}}) mostly is oriented parallel to the surface of the bone
                -   At the site of tendinous and [ligamentous]({{< relref "ligament" >}}) insertions there are collagen fibers extending perpendicularly into the bone - [Sharpey's fibers]({{< relref "sharpey_s_fibers" >}})

    <!--list-separator-->

    -  Basic articular [joint]({{< relref "joint" >}})

        {{< figure src="/ox-hugo/_20210930_154007screenshot.png" width="700" >}}

        -   In image:
            -   Sinovial cavity
            -   [Bursa]({{< relref "bursa" >}})
            -   Articular cartilage
            -   Epiphyseal bone
            -   Enthesis
            -   Ligament
            -   Joint capsule with synovial lining
            -   Tendon


### Unlinked references {#unlinked-references}

[Show unlinked references]
