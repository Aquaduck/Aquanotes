+++
title = "Hypoxanthine-guanine phosphoribosyltransferase"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Salvage pathway for purines (p. 294) > Conversion of purine bases to nucleotides**

    [Hypoxanthine-guanine phosphoribosyltransferase]({{< relref "hypoxanthine_guanine_phosphoribosyltransferase" >}}) (HGPRT)

    ---


#### [Allopurinol]({{< relref "allopurinol" >}}) {#allopurinol--allopurinol-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [HGPRT]({{< relref "hypoxanthine_guanine_phosphoribosyltransferase" >}}) dependent

    ---


#### [6-mercaptopurine]({{< relref "6_mercaptopurine" >}}) {#6-mercaptopurine--6-mercaptopurine-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [HGPRT]({{< relref "hypoxanthine_guanine_phosphoribosyltransferase" >}}) dependent

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
