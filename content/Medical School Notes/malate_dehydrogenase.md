+++
title = "Malate dehydrogenase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Requires [NAD<sup>+</sup>]({{< relref "nadh" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [List the coenzymes, and where appropriate their vitamin precursors, essential to:]({{< relref "list_the_coenzymes_and_where_appropriate_their_vitamin_precursors_essential_to" >}}) {#list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to--list-the-coenzymes-and-where-appropriate-their-vitamin-precursors-essential-to-dot-md}

<!--list-separator-->

-  **🔖 Gluconeogenesis**

    [Malate DH]({{< relref "malate_dehydrogenase" >}}) requires [NAD<sup>+</sup>]({{< relref "nadh" >}}) from [Niacin]({{< relref "niacin" >}}) (B3)

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle**

    <!--list-separator-->

    -  NAD<sup>+</sup>-linked [malate dehydrogenase]({{< relref "malate_dehydrogenase" >}})

        -   Required for the **reversible oxidation of malate to oxaloacetate**
            -   Completes the TCA cycle
            -   **Critical reaction in gluconeogenesis**
                -   Malate - α-ketoglutarate transporter moves oxaloacetate from the mitosol to the cytosol as a **gluconeogenic precursor**


#### [Identify those enzymes whose activity will be compromised by a deficiency of the vitamin(s) listed above by explaining the role of the vitamin in enzyme function]({{< relref "identify_those_enzymes_whose_activity_will_be_compromised_by_a_deficiency_of_the_vitamin_s_listed_above_by_explaining_the_role_of_the_vitamin_in_enzyme_function" >}}) {#identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin--s--listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function--identify-those-enzymes-whose-activity-will-be-compromised-by-a-deficiency-of-the-vitamin-s-listed-above-by-explaining-the-role-of-the-vitamin-in-enzyme-function-dot-md}

<!--list-separator-->

-  **🔖 By enzyme**

    <!--list-separator-->

    -  [Malate DH]({{< relref "malate_dehydrogenase" >}})

        -   Niacin (B3) in the form of NAD<sup>+</sup>


### Unlinked references {#unlinked-references}

[Show unlinked references]
