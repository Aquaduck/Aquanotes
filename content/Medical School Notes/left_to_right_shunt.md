+++
title = "Left-to-right shunt"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of cardiac [Shunt]({{< relref "shunt" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) {#ventricular-septal-defect--ventricular-septal-defect-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An abnormal communication between the [left]({{< relref "left_ventricle" >}}) and [right ventricle]({{< relref "right_ventricle" >}}) -> [left to right shunting]({{< relref "left_to_right_shunt" >}}) of blood flow

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Tetralogy of Fallot > Overview > Ventricular septal defect (VSD)**

    An abnormal communication between the left and right ventricle -> [left to right shunting]({{< relref "left_to_right_shunt" >}}) of blood flow

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
