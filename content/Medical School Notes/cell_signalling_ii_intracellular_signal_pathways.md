+++
title = "Cell Signalling II - Intracellular Signal Pathways"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 16 linked references {#16-linked-references}


#### [Discuss the role of titin as a component of mechano-chemical signal transduction in skeletal muscle.]({{< relref "discuss_the_role_of_titin_as_a_component_of_mechano_chemical_signal_transduction_in_skeletal_muscle" >}}) {#discuss-the-role-of-titin-as-a-component-of-mechano-chemical-signal-transduction-in-skeletal-muscle-dot--discuss-the-role-of-titin-as-a-component-of-mechano-chemical-signal-transduction-in-skeletal-muscle-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  [Titin]({{< relref "titin" >}})

        -   An enormous (ca 3.5MDa) protein located in the **contractile unit of skeletal and cardiac muscle** - the _sarcomere_
        -   Spring-like molecule
        -   Ideally positioned to sense mechanical load
        -   **Has ser/thr kinase within its structure** - [Titin kinase]({{< relref "titin_kinase" >}})
        -   **Main takeaway**: titin and its protein kinase is critical to mechano-chemical signaling in striated muscle

        <!--list-separator-->

        -  Mechanism

            -   Mechanical load triggers Titin Kinase -> signal cascade activation
                -   Protein degradation
                -   Ubiquitin pathway
                -   Cell signaling in the nucleus through [Serum Response Factor]({{< relref "serum_response_factor" >}}) (SRF)


#### [Discuss the monomeric GTPases and how they are regulated, with specific reference to Ras.]({{< relref "discuss_the_monomeric_gtpases_and_how_they_are_regulated_with_specific_reference_to_ras" >}}) {#discuss-the-monomeric-gtpases-and-how-they-are-regulated-with-specific-reference-to-ras-dot--discuss-the-monomeric-gtpases-and-how-they-are-regulated-with-specific-reference-to-ras-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  The Monomeric [GTPases]({{< relref "g_protein_gtpase" >}})

        -   Small molecular weight GTPases analogous to the α-subunit of the trimeric G-protein family
        -   Located in cytoplasm of cell
        -   Has a wide variety of functions

    <!--list-separator-->

    -  [Ras]({{< relref "ras" >}})

        <!--list-separator-->

        -  Activation

            -   Requires a Guanine Nucleotide Exchange Factor ([GEF]({{< relref "guanine_nucleotide_exchange_factor" >}}))
            -   Replaces GDP on an inactive Ras with GTP
            -   Analogous to the hormone-receptor complex of the plasma membrane trimeric G-proteins

        <!--list-separator-->

        -  Inactivation

            -   Requires GTPase-Activating Protein ([GAP]({{< relref "gtpase_activating_protein" >}}))
                -   Significantly boost intrinsic GTPase activity


#### [Discuss the concept of mechanochemical signal transduction in biology.]({{< relref "discuss_the_concept_of_mechanochemical_signal_transduction_in_biology" >}}) {#discuss-the-concept-of-mechanochemical-signal-transduction-in-biology-dot--discuss-the-concept-of-mechanochemical-signal-transduction-in-biology-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Mechano-chemical signal transduction

        -   **Mechanical forces** serve as trigger
        -   A physiological example of this is **hearing**
        -   Another example is found in **endothelial cells**
            -   Increased shear stress or wall tension -> NO production -> Smooth muscle relaxation as discussed [here]({{< relref "describe_the_role_of_gases_in_cell_signaling_with_special_emphasis_on_nitric_oxide_and_its_basic_mechanism_of_action" >}})

        <!--list-separator-->

        -  Tension affects cell shape

            -   Rigidity and tension is critical to cell growth and differentiation
                -   The [extracellular matrix]({{< relref "extracellular_matrix" >}}) is a key determinant of cell function - "_dynamic reciprocity_"
                    -   Mechanically distorting any one of a cell's elements -> changes in shape/mechanical forces throughout entire cell because of [cytoskeleton]({{< relref "cytoskeleton" >}})
                -   Genes specify extracellular matrix proteins -> regulate genes involved in these functions

        <!--list-separator-->

        -  Role of ion channels

            -   Ion channels are tethered to cell membrane
                -   **Cell membrane distortion = ion channel distortion**

        <!--list-separator-->

        -  Exercise

            -   An example of mechano-chemical signaling pathways that do not involve the plasma membrane
            -   Exercise can cause muscle to grow substantially or change the muscle fiber type
            -   Passively stretching a muscle -> **significant increase in glucose transport**
                -   Why exercise is a critical part of therapy for obesity and diabetes
            -   Believed that [titin]({{< relref "titin" >}}) is an important component of this pathway


#### [Discuss receptor tyrosine kinases and tyrosine-associated receptor signal pathways]({{< relref "discuss_receptor_tyrosine_kinases_and_tyrosine_associated_receptor_signal_pathways" >}}) {#discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways--discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  [Receptor protein tyrosine kinases]({{< relref "receptor_protein_tyrosine_kinase" >}}) (RPTKs)

        -   **Receptors that are also tyrosine kinases**
        -   Involved in **growth** -> think cancer
        -   Tyrosine kinase receptor **phosphorylates itself** -> creates a specific kind of binding site (_SH<sub>2</sub>_)
            -   Multiple tyrosines phosphorylated -> **multiple binding sites for additional signal proteins**

        <!--list-separator-->

        -  Examples of growth factor receptors that are tyrosine kinases

            1.  Epidermal Growth Factor (EGF)
            2.  Insulin-like Growth Factor-1 (IGF-1)
            3.  Nerve Growth Factor (NGF)
            4.  Platelet Derived Growth Factor (PDGF)
            5.  Insulin

        <!--list-separator-->

        -  Two key pathways

            <!--list-separator-->

            -  [Phosphatidylinositol kinase]({{< relref "phosphatidylinositol_kinase" >}}) (PI-3 Kinase)

                -   Involves [PIP<sub>2</sub> signalling]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}})
                -   Controls (e.g.):
                    -   Growth/survival of cells
                    -   Apoptosis
                    -   Many more

            <!--list-separator-->

            -  [Ras-GTP]({{< relref "ras" >}})

                -   Controls (e.g.):
                    -   Endocytosis
                    -   Cell cycle
                    -   Nuclear transport
                    -   Membrane trafficking
                    -   Many more

                <!--list-separator-->

                -  Pathway

                    1.  RPTK receptors phosphorylated on multiple locations -> activated
                    2.  Allows binding of [SH<sub>2</sub>]({{< relref "src_homology_2_domain" >}}) binding proteins -> Ras activation
                    3.  Ras activates [MAP-kinase-kinase-kinase]({{< relref "map_kinase_kinase_kinase" >}}) (Raf) -> activates [MAP-kinase-kinase]({{< relref "map_kinase_kinase" >}}) (Mek) -> activates [MAP-kinase]({{< relref "map_kinase" >}}) (Erk)
                        -   Abnormal expression due to gene mutation and/or dysregulation -> cancer -> **[oncogenes]({{< relref "oncogene" >}})**


#### [Describe the Yin-Yang regulation of adenylate cyclase.]({{< relref "describe_the_yin_yang_regulation_of_adenylate_cyclase" >}}) {#describe-the-yin-yang-regulation-of-adenylate-cyclase-dot--describe-the-yin-yang-regulation-of-adenylate-cyclase-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  The Yin and Yang of cAMP Regulation

        -   More than one [GPCR]({{< relref "g_protein_coupled_receptors" >}}) can be coupled to the same intracellular signal pathway
        -   Individual signal pathways often are subject to both stimulatory and inhibitory arms - each of which can be regulated indpeendently

        <!--list-separator-->

        -  Opposing G proteins (G<sub>s</sub> vs G<sub>i</sub>) regulating cAMP

            -   cAMP can be regulated by changing activity of **either G<sub>s</sub> or G<sub>i</sub>** -> **multiple pathways to regulate cAMP levels**


#### [Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important.]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}}) {#describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot--describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Off Signals

        -   Good control systems must be able to be **turned off rapidly**
        -   The time course of approach to a new steady state is **solely a function of the rate constant for degradation**

        <!--list-separator-->

        -  Several pathways in the cell to turn off a signal:

            <!--list-separator-->

            -  Initiating signal disappears

                -   At the largest scale **levels of signal molecule itself falls** due to engagement of negative feedback mechanisms

            <!--list-separator-->

            -  Inactivating transducer - G-protein [GTPase]({{< relref "g_protein_gtpase" >}})

                -   Gα-subunit is a "GTPase time bomb"
                -   **GTPase activity increases shortly after G-protein activation**
                    -   Essentially, shortly after activation steps have already been set in motion to inactivate the [G-protein]({{< relref "g_protein" >}})

            <!--list-separator-->

            -  Get rid of second messenger - [phosphodiesterases]({{< relref "phosphodiesterase" >}}) (PDE)

                -   You can **degrade the intracellular signal molecule** e.g. cAMP degradation via [cAMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})

            <!--list-separator-->

            -  Dephosphorylation

                -   Phosphatases dephosphorylate PKA targets -> no activity

            <!--list-separator-->

            -  Inactivate the receptor

                -   [PKA]({{< relref "protein_kinase_a" >}}) has an additional action: **phosphorylation of the ser/thr residue of the receptor itself** -> turns receptor off
                    -   Creates "docking sites" -> permits binding of [arrestin]({{< relref "arrestin" >}}) -> inhibits receptor

        <!--list-separator-->

        -  Why is the off signal important?

            -   Goal: regulate amount of substance or process in the body **as tightly as possible**
            -   Being able to turn a signal on and off rapidly **reduces the error signal**


#### [Describe the roles of tyrosine vs serine / threonine phosphorylation in the regulation of receptor function]({{< relref "describe_the_roles_of_tyrosine_vs_serine_threonine_phosphorylation_in_the_regulation_of_receptor_function" >}}) {#describe-the-roles-of-tyrosine-vs-serine-threonine-phosphorylation-in-the-regulation-of-receptor-function--describe-the-roles-of-tyrosine-vs-serine-threonine-phosphorylation-in-the-regulation-of-receptor-function-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Flavor counts

        -   Tyrosine phosphorylation **activates**
        -   Serine/threonine phosphorylase **inactivates**
            -   Often causes loss of receptor function


#### [Describe the role of gases in cell signaling with special emphasis on nitric oxide and its basic mechanism of action.]({{< relref "describe_the_role_of_gases_in_cell_signaling_with_special_emphasis_on_nitric_oxide_and_its_basic_mechanism_of_action" >}}) {#describe-the-role-of-gases-in-cell-signaling-with-special-emphasis-on-nitric-oxide-and-its-basic-mechanism-of-action-dot--describe-the-role-of-gases-in-cell-signaling-with-special-emphasis-on-nitric-oxide-and-its-basic-mechanism-of-action-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Gases as second messengers - [NO]({{< relref "nitric_oxide" >}})

        -   NO is synthesized by certain cells (e.g. epithelial) from arginine
        -   Three (of many) physiological responses to elevated levels of NO:
            1.  Smooth muscle relaxation
            2.  Neurotransmission in CNS
            3.  Cell-mediated immune response

        <!--list-separator-->

        -  Mechanism of action

            1.  Signal binds to receptor
            2.  Receptor activates [NO synthase]({{< relref "nitric_oxide_synthase" >}}) -> synthesizes NO
            3.  NO diffuses to neighboring cells across membranes
            4.  NO binds to _guanylyl cyclase_ -> activation of cGMP pathway
            5.  Triggers smooth muscle relaxation

    <!--list-separator-->

    -  [CO]({{< relref "carbon_monoxide" >}})

        -   CO is a physiologically important signal molecule and is thought to work in the same way as nitric oxide


#### [Describe the process of G-protein activation and inactivation.]({{< relref "describe_the_process_of_g_protein_activation_and_inactivation" >}}) {#describe-the-process-of-g-protein-activation-and-inactivation-dot--describe-the-process-of-g-protein-activation-and-inactivation-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  The Trimeric G-Protein Cycle

        -   All G-proteins work in this way
        -   **Inactive when GDP is bound** to α subunit
            -   Involves **dephosphorylation** of already bound GTP
        -   **Active when GTP is bound**
            -   **completely replaces** GDP with GTP


#### [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}) {#describe-the-inositol-phosphatide-second-messenger-system--describe-the-inositol-phosphatide-second-messenger-system-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Important members

        <!--list-separator-->

        -  Membrane phosphatidylinositol

            <!--list-separator-->

            -  [PIP<sub>2</sub>]({{< relref "phosphatidylinositol" >}})

                -   Naturally occuring membrane phospholipid
                -   Composed of a glycerol backbone attached to two fatty acids and the sugar _inositol_ via a covalently bound phosphate
                    -   The inositol is bound to 2 phosphate groups

        <!--list-separator-->

        -  [G-proteins]({{< relref "g_protein" >}})

            -   G<sub>q</sub> activated when ligand binds to GPCR
            -   Behaves like G<sub>s</sub> or G<sub>i</sub>
            -   Activates phospholipase C-β

        <!--list-separator-->

        -  Phospholipase

            <!--list-separator-->

            -  [Phospholipase C-β]({{< relref "phospholipase_c_β" >}})

                -   Cleaves the glycerol-phosphate bond in PIP<sub>2</sub> -> produces DAG (diacylglycerol) and IP<sub>3</sub>

        <!--list-separator-->

        -  [Inositol-tris-phosphate]({{< relref "inositol_tris_phosphate" >}}) (IP<sub>3</sub>)

            -   Created through Phospholipase C-β cleavage of PIP<sub>2</sub>

        <!--list-separator-->

        -  [Diacylglycerol]({{< relref "diacylglycerol" >}}) (DAG)

            -   Created through Phospholipase C-β cleavage of PIP<sub>2</sub>

        <!--list-separator-->

        -  [Protein kinase C]({{< relref "protein_kinase_c" >}})

            -   Activated by DAG
                -   Reinforced by increased intracellular levels of calcium
            -   Phosphorylates ser/thr residues on target proteins
                -   Results in gene regulation of genes involved with hypertrophy and proliferation (growth and differentiation)

        <!--list-separator-->

        -  Calcium

            -   Calcium is released from the ER in response to IP<sub>3</sub>
            -   Binds to calmodulin

        <!--list-separator-->

        -  [Calmodulin]({{< relref "calmodulin" >}})

            -   Activated by calcium release from ER
            -   Functions to activate a series of calmodulin kinases -> **phosphorylation of target proteins\*** on ser/thr residues

    <!--list-separator-->

    -  Sequence of events

        1.  Ligand binds to receptor
        2.  G<sub>q</sub> complex activated (behaves just like G<sub>s</sub> or G<sub>i</sub>)
        3.  G<sub>q</sub> activates phospholipase C-β
        4.  Formation of DAG and IP<sub>3</sub>
            1.  IP<sub>3</sub> travels to intracellular membranes of the ER -> calcium release -> calcium binds to [calmodulin]({{< relref "calmodulin" >}})
            2.  DAG recruits [protein kinase-C]({{< relref "protein_kinase_c" >}}) to the plasma membrane -> partial PKC activation
                -   PKC activation reinforced by increased intracellular levels of calcium
                    -   Note: **calcium does not replace the need for prior DAG activation**
        5.  PKC phosphorylates its target on the ser/thr residue


#### [Describe the cyclic-AMP (cAMP) second messenger signal pathway from hormone binding to activation of protein kinase-A.]({{< relref "describe_the_cyclic_amp_camp_second_messenger_signal_pathway_from_hormone_binding_to_activation_of_protein_kinase_a" >}}) {#describe-the-cyclic-amp--camp--second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot--describe-the-cyclic-amp-camp-second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  G-protein signal cascades: cAMP

        -   Located in the plasma membrane are three key elements of the cAMP signal cascade:
            1.  **Receptor**
                -   **Must traverse the plasma membrane 7 times** <- required of **all** [GPCRs]({{< relref "g_protein_coupled_receptors" >}}) (so far described)
            2.  [G-protein]({{< relref "g_protein" >}})
                -   A trimeric protein situated in the plasma membrane
                -   Requires GTP and GDP to function
            3.  [Adenylyl cyclase]({{< relref "adenylate_cyclase" >}})

    <!--list-separator-->

    -  [cAMP]({{< relref "camp" >}}) Activation

        1.  Signal activates receptor -> **conformational change in receptor**
        2.  G<sub>s</sub> complex activated -> activates [adenylyl cyclase]({{< relref "adenylate_cyclase" >}}) -> formation of cAMP

    <!--list-separator-->

    -  Activation of [Protein kinase A]({{< relref "protein_kinase_a" >}}) (PKA)

        <ol class="org-ol">
        <li value="20">cAMP activates PKA</li>
        </ol>


#### [Describe the basic mechanisms that cause the symptoms of cholera and whooping cough.]({{< relref "describe_the_basic_mechanisms_that_cause_the_symptoms_of_cholera_and_whooping_cough" >}}) {#describe-the-basic-mechanisms-that-cause-the-symptoms-of-cholera-and-whooping-cough-dot--describe-the-basic-mechanisms-that-cause-the-symptoms-of-cholera-and-whooping-cough-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Cholera Activates G<sub>s</sub>

        -   [Cholera]({{< relref "cholera" >}}) causes inappropriate, chronic activation of G<sub>s</sub>
            -   Modifies the Gα-subunit by ribosylation -> inhibition
                -   (don't need to know specifics, just that the subunit is chemically modified)
            -   Occurs in the epithelium of the gut

    <!--list-separator-->

    -  Pertussis Inhibits G<sub>i</sub>

        -   [Whooping cough]({{< relref "whooping_cough" >}}) is caused by Bordetella Pertussis
        -   Modifies the Gα-subunit by ribosylation -> inhibition
        -   Occurs in the lung


#### [Describe the basic mechanism of action of steroid hormones both in terms of intracellular and surface receptors.]({{< relref "describe_the_basic_mechanism_of_action_of_steroid_hormones_both_in_terms_of_intracellular_and_surface_receptors" >}}) {#describe-the-basic-mechanism-of-action-of-steroid-hormones-both-in-terms-of-intracellular-and-surface-receptors-dot--describe-the-basic-mechanism-of-action-of-steroid-hormones-both-in-terms-of-intracellular-and-surface-receptors-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Hydrophobic signal molecules - [steroids]({{< relref "steroid" >}}) + thyroid hormones

        -   Examples:
            -   Testosterone
            -   Estradiol
            -   Cortisol
            -   Aldosterone
            -   Vitamin D<sub>3</sub>

        <!--list-separator-->

        -  Intracellular mechanism fundamentals

            1.  Hormone diffuses through plasma membrane -> binds to high-affinity intracellular receptor
                -   This traps the steroid hormone
            2.  Binding causes a change in conformation in recepter that results in **dimerization**
            3.  Occupied, dimerized receptor migrates to the nucleus (if not already there) -> binds to specific transcriptional regulatory elements to either turn on or off specific genes

        <!--list-separator-->

        -  Steroid hormones also act on plasma membrane receptors

            -   Second messenger mechanism similar to that for hydrophilic hormones

    <!--list-separator-->

    -  Important differences between intracellular receptors and surface receptors:

        -   For intracellular receptors, **maximal effect requires maximal binding**
            -   Intracellular receptors are limiting for the action of the hormone
        -   Why the difference?
            -   Surface receptor signaling works fast, intracellular receptor signalling takes hours (gene transcription)


#### [Describe and give an example of receptors as ion channels.]({{< relref "describe_and_give_an_example_of_receptors_as_ion_channels" >}}) {#describe-and-give-an-example-of-receptors-as-ion-channels-dot--describe-and-give-an-example-of-receptors-as-ion-channels-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Receptors as Ion Channels

        -   Simplest and most primitive signal cascade involving plasma membrane receptors
        -   Especially prevalent in excitable membranes
            -   E.g. nerve signal transmission at synapses between nerves, between nerves and muscle -> contraction
        -   The prototypic example is the **[acetylcholine]({{< relref "acetylcholine" >}}) (ACh) receptor**

    <!--list-separator-->

    -  Synapses between neurons - [acetylcholine]({{< relref "acetylcholine" >}})

        -   Major neurotransmitter in the CNS and ANS
        -   Major role is to **open sodium channels in the membrane -> depolarization**
        -   Motor neurons cause muscle contraction via th eopening of acetylcholine receptors at the neuromuscular junction


#### [Define the enzymes involved in synthesis and degradation of cyclic-AMP.]({{< relref "define_the_enzymes_involved_in_synthesis_and_degradation_of_cyclic_amp" >}}) {#define-the-enzymes-involved-in-synthesis-and-degradation-of-cyclic-amp-dot--define-the-enzymes-involved-in-synthesis-and-degradation-of-cyclic-amp-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Adenylyl cyclase/[Adenylate cyclase]({{< relref "adenylate_cyclase" >}})

        -   converts ATP -> cAMP (synthesis)

    <!--list-separator-->

    -  [Cyclic AMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}})

        -   converts cAMP -> 5'AMP (degradation)


#### [Compare the cAMP and Inositol Phosphatide signal pathways.]({{< relref "compare_the_camp_and_inositol_phosphatide_signal_pathways" >}}) {#compare-the-camp-and-inositol-phosphatide-signal-pathways-dot--compare-the-camp-and-inositol-phosphatide-signal-pathways-dot-md}

<!--list-separator-->

-  From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}})

    <!--list-separator-->

    -  Common features of cAMP and IP3

        1.  Both use cell surface receptors
        2.  Both involve a G-protein
            -   cAMP: G<sub>s</sub> or G<sub>i</sub>
            -   IP3: G<sub>q</sub>
        3.  Both involve enzyme activity
            -   cAMP: Adenylate Cyclase
            -   IP3: Phospholipase C-β
        4.  Both involve kinase activity -> phosphorylation of targets (on ser/thr residues)
            -   cAMP: PKA
            -   IP3: PKC
        5.  Both affect gene transcription


### Unlinked references {#unlinked-references}

[Show unlinked references]
