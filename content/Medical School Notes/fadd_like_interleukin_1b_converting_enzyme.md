+++
title = "FADD-like interleukin-1β converting enzyme"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Procaspase-8]({{< relref "procaspase_8" >}}) {#procaspase-8--procaspase-8-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Composed of [FLICE]({{< relref "fadd_like_interleukin_1b_converting_enzyme" >}}), MACH, and Mch5

    ---


#### [FLICE-inhibitor protein]({{< relref "flice_inhibitor_protein" >}}) {#flice-inhibitor-protein--flice-inhibitor-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Restrains [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}) by inhibiting [FLICE]({{< relref "fadd_like_interleukin_1b_converting_enzyme" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
