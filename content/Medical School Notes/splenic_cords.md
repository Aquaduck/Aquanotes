+++
title = "Splenic cords"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [Sheathed capillaries]({{< relref "sheathed_capillary" >}}) in the [spleen]({{< relref "spleen" >}}) dump blood into here


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen > Spleen filtration**

    Senescent cells and material are removed as they attempt to navigate the [macrophages]({{< relref "macrophage" >}}) of the [sheath]({{< relref "sheathed_capillary" >}}) and [cords]({{< relref "splenic_cords" >}}) -> squeeze between endothelial cells to return to the vascular system in the sinuses of the [red pulp]({{< relref "red_pulp" >}})

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen > Spleen filtration**

    Sheathed capillaries dump blood into the interstitial tissues of the spleen - called [splenic cords]({{< relref "splenic_cords" >}}) (aka Cords of Billroth)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
