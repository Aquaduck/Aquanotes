+++
title = "Amino Acid"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Definition {#definition}

-   Molecules that contain two functional groups: an amino group (-NH<sub>2</sub>) and a carboxyl group (-COOH)

{{< figure src="/ox-hugo/_20210712_213003screenshot.png" >}}


## Backlinks {#backlinks}


### 19 linked references {#19-linked-references}


#### [Osmosis - Transcription, Translation, and Replication]({{< relref "osmosis_transcription_translation_and_replication" >}}) {#osmosis-transcription-translation-and-replication--osmosis-transcription-translation-and-replication-dot-md}

<!--list-separator-->

-  **🔖 Translation > Transfer RNA (tRNA)**

    Finds and carries [amino acids]({{< relref "amino_acid" >}}) to ribosome

    ---

<!--list-separator-->

-  **🔖 Translation > Process**

    _Elongation_: [ribosome]({{< relref "ribosome" >}}) moves along mRNA producing specific [AA]({{< relref "amino_acid" >}})s for each codon

    ---


#### [Osmosis - Gluconeogenesis]({{< relref "osmosis_gluconeogenesis" >}}) {#osmosis-gluconeogenesis--osmosis-gluconeogenesis-dot-md}

<!--list-separator-->

-  **🔖 Notes > Sources of pyruvate**

    18/20 [amino acids]({{< relref "amino_acid" >}}) are _glucogenic_ - can be used to make glucose

    ---


#### [Kaplan Biochemistry Chapter 1]({{< relref "kaplan_biochemistry_chapter_1" >}}) {#kaplan-biochemistry-chapter-1--kaplan-biochemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 Chapter 1 > 1.3 Peptide Bond Formation and Hydrolysis > Overview**

    _Peptide bonds_: amide bond forms between the carboxylic acid of one [AA]({{< relref "amino_acid" >}}) and the amino group of another AA

    ---

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins > Structures of the Amino Acids > Positively Charged (Basic) Side Chains**

    3 [amino acids]({{< relref "amino_acid" >}}) have side chains that have positively charged nitrogen atoms

    ---

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins > Structures of the Amino Acids > Negatively Charged (Acidic) Side Chains**

    Only **2** of the 20 [amino acids]({{< relref "amino_acid" >}}) have negative charges on their side chains

    ---

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins > Structures of the Amino Acids > Polar Side Chains**

    Unlike the amino group in all [AAs]({{< relref "amino_acid" >}}), the **amide nitrogens do not gain or lose proteins with changes to PH -> do not become charged**

    ---

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins > Structures of the Amino Acids > Polar Side Chains**

    Five [amino acids]({{< relref "amino_acid" >}}) have side chains that are polar _but not aromatic_

    ---

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins > Structures of the Amino Acids > Aromatic Side Chains**

    Three [amino acids]({{< relref "amino_acid" >}}) have uncharged aromatic side chains

    ---

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins > Stereochemistry of AAs**

    [L-amino acids]({{< relref "amino_acid" >}}) are the only ones found in eukaryotic proteins

    ---

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins > Stereochemistry of AAs**

    All [AAs]({{< relref "amino_acid" >}}) **except for cysteine** have an (S) absolute configuration

    ---

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins > Stereochemistry of AAs**

    For most [AAs]({{< relref "amino_acid" >}}), the ɑ-carbon is a [chiral]({{< relref "chirality" >}}) (or stereogenic) center

    ---

<!--list-separator-->

-  **🔖 Chapter 1 > 1.1 Amino Acids Found in Proteins**

    The **side chains** (R groups) of [AAs]({{< relref "amino_acid" >}}) determine their chemical properties

    ---


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 1. Recognize the 20 amino acids and classify them based on the characteristics of their side chains**

    [Amino Acid]({{< relref "amino_acid" >}})

    ---


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21) > Glomerular filtration (p. 23)**

    E.g. [Sodium]({{< relref "sodium" >}}), [Potassium]({{< relref "potassium" >}}), [Chloride]({{< relref "chloride" >}}), [Bicarbonate]({{< relref "bicarbonate" >}}), [Glucose]({{< relref "glucose" >}}), [Urea]({{< relref "urea" >}}), [amino acids]({{< relref "amino_acid" >}}), peptides such as [Insulin]({{< relref "insulin" >}}) and [ADH]({{< relref "antidiuretic_hormone" >}})

    ---


#### [Enzyme]({{< relref "enzyme" >}}) {#enzyme--enzyme-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    _Isoenzyme_: Enzyme that has a different [AA]({{< relref "amino_acid" >}}) sequence but catalyzes the same chemical reaction

    ---


#### [CrashCourse - Biological Molecules]({{< relref "crashcourse_biological_molecules" >}}) {#crashcourse-biological-molecules--crashcourse-biological-molecules-dot-md}

<!--list-separator-->

-  **🔖 Biological Molecules > Proteins**

    <!--list-separator-->

    -  9 [Amino Acids]({{< relref "amino_acid" >}}) we can't make ourselves:

        1.  Lysine
        2.  Methionine
        3.  Histidine
        4.  Isoleucine
        5.  Leucine
        6.  Phenylalanine
        7.  Threonine
        8.  Tryptophan
        9.  Valine

<!--list-separator-->

-  **🔖 Biological Molecules > Proteins**

    Made up of 20 [Amino Acids]({{< relref "amino_acid" >}})

    ---


#### [Amoeba Sisters - Protein Structure and Folding]({{< relref "amoeba_sisters_protein_structure_and_folding" >}}) {#amoeba-sisters-protein-structure-and-folding--amoeba-sisters-protein-structure-and-folding-dot-md}

<!--list-separator-->

-  **🔖 Primary Structure**

    Sequence of [AAs]({{< relref "amino_acid" >}}) that make up a protein held together by peptide bonds

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
