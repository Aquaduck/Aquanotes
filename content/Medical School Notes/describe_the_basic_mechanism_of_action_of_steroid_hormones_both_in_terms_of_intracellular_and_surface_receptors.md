+++
title = "Describe the basic mechanism of action of steroid hormones both in terms of intracellular and surface receptors."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Hydrophobic signal molecules - [steroids]({{< relref "steroid" >}}) + thyroid hormones {#hydrophobic-signal-molecules-steroids--steroid-dot-md--plus-thyroid-hormones}

-   Examples:
    -   Testosterone
    -   Estradiol
    -   Cortisol
    -   Aldosterone
    -   Vitamin D<sub>3</sub>


#### Intracellular mechanism fundamentals {#intracellular-mechanism-fundamentals}

1.  Hormone diffuses through plasma membrane -> binds to high-affinity intracellular receptor
    -   This traps the steroid hormone
2.  Binding causes a change in conformation in recepter that results in **dimerization**
3.  Occupied, dimerized receptor migrates to the nucleus (if not already there) -> binds to specific transcriptional regulatory elements to either turn on or off specific genes


#### Steroid hormones also act on plasma membrane receptors {#steroid-hormones-also-act-on-plasma-membrane-receptors}

-   Second messenger mechanism similar to that for hydrophilic hormones


### Important differences between intracellular receptors and surface receptors: {#important-differences-between-intracellular-receptors-and-surface-receptors}

-   For intracellular receptors, **maximal effect requires maximal binding**
    -   Intracellular receptors are limiting for the action of the hormone
-   Why the difference?
    -   Surface receptor signaling works fast, intracellular receptor signalling takes hours (gene transcription)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Describe the basic mechanism of action of steroid hormones both in terms of intracellular and surface receptors.]({{< relref "describe_the_basic_mechanism_of_action_of_steroid_hormones_both_in_terms_of_intracellular_and_surface_receptors" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
