+++
title = "Gastroschisis"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Embryology**

    <!--list-separator-->

    -  [Gastroschisis]({{< relref "gastroschisis" >}})

        -   A ventral wall defect -> paraumbilical herniation of the intestine through the abdominal wall **without formation of a hernia sac**


### Unlinked references {#unlinked-references}

[Show unlinked references]
