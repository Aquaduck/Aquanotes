+++
title = "Type II error"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Statistical power]({{< relref "statistical_power" >}}) {#statistical-power--statistical-power-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Complementary to the [type 2 error]({{< relref "type_ii_error" >}}) rate

    ---


#### [Define statistical power and explain how it affects the interpretation of negative results]({{< relref "define_statistical_power_and_explain_how_it_affects_the_interpretation_of_negative_results" >}}) {#define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results--define-statistical-power-and-explain-how-it-affects-the-interpretation-of-negative-results-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Statistical power**

    Complementary to the [type 2 error]({{< relref "type_ii_error" >}}) rate

    ---


#### [Compare and contrast type I error and type II error]({{< relref "compare_and_contrast_type_i_error_and_type_ii_error" >}}) {#compare-and-contrast-type-i-error-and-type-ii-error--compare-and-contrast-type-i-error-and-type-ii-error-dot-md}

<!--list-separator-->

-  **🔖 From Amboss**

    <!--list-separator-->

    -  [Type II error]({{< relref "type_ii_error" >}})

        -   **False negative** -> observed effect did not occur due to chance
        -   Null hypothesis is accpeted when it is actually false


### Unlinked references {#unlinked-references}

[Show unlinked references]
