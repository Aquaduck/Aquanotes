+++
title = "Citrate synthase"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Citric Acid Cycle**

    <!--list-separator-->

    -  [Citrate synthase]({{< relref "citrate_synthase" >}})

        -   **Not allosterically regulated**
            -   [Citrate]({{< relref "citrate" >}}) itself inhibits the enzyme by competing with [oxaloacetate]({{< relref "oxaloacetate" >}})
        -   [Succinyl-CoA]({{< relref "succinyl_coa" >}}) is a strong inhibitor of citrate production
            -   Strictly competitive with [acetyl-CoA]({{< relref "acetyl_coa" >}})
            -   Noncompetitive with [oxaloacetate]({{< relref "oxaloacetate" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
