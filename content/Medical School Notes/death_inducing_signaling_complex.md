+++
title = "Death-inducing signaling complex"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 9 linked references {#9-linked-references}


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  **🔖 TRADD in TRAIL/TRAILR Signaling**

    Both TRADD and [RIP]({{< relref "receptor_interacting_protein" >}}) were demonstrated to be recruited to the TRAILR complex in the [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---


#### [Procaspase-8]({{< relref "procaspase_8" >}}) {#procaspase-8--procaspase-8-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Recruited to the [DISC]({{< relref "death_inducing_signaling_complex" >}}) and proteolysed into mature Caspase-8

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > FLIP > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    FLIP dimerizes with [caspase-8]({{< relref "caspase_8" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Fas > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    Once dimerized + activated in [DISC]({{< relref "death_inducing_signaling_complex" >}}), [initiator caspases]({{< relref "initiator_caspase" >}}) cleave their partners -> activate downstreain [executioner caspases]({{< relref "executioner_caspase" >}}) to induce [apoptosis]({{< relref "apoptosis" >}})

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Fas > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    Fas activated by [Fas ligand]({{< relref "fas_ligand" >}}) -> death domains on Fas death receptors bind intracellular adaptor proteins -> bind [initiator caspases]({{< relref "initiator_caspase" >}}) (primarily [caspase-8]({{< relref "caspase_8" >}})) -> formation of a [death-inducing signaling complex]({{< relref "death_inducing_signaling_complex" >}}) (DISC)

    ---


#### [FLICE-inhibitor protein]({{< relref "flice_inhibitor_protein" >}}) {#flice-inhibitor-protein--flice-inhibitor-protein-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Dimerizes with [Caspase-8]({{< relref "caspase_8" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---


#### [Fas-associated death domain]({{< relref "fas_associated_death_domain" >}}) {#fas-associated-death-domain--fas-associated-death-domain-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Recruited as part of formation of the [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---


#### [Caspase-8]({{< relref "caspase_8" >}}) {#caspase-8--caspase-8-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Dimerizes with [FLIP]({{< relref "flice_inhibitor_protein" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---


#### [Apo2L/TRAIL: apoptosis signaling, biology, and potential for cancer therapy]({{< relref "apo2l_trail_apoptosis_signaling_biology_and_potential_for_cancer_therapy" >}}) {#apo2l-trail-apoptosis-signaling-biology-and-potential-for-cancer-therapy--apo2l-trail-apoptosis-signaling-biology-and-potential-for-cancer-therapy-dot-md}

<!--list-separator-->

-  **🔖 Apoptosis signaling by Apo2L/TRAIL**

    Similar to [FasL]({{< relref "fas_ligand" >}}), TRAIL initiates apoptosis upon binding to its cognate death receptors by inducing the recruitment of specific cytoplasmic proteins to the intracellular death domain of the receptor -> formation of [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
