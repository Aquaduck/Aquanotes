+++
title = "Diacylglycerol"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}) {#describe-the-inositol-phosphatide-second-messenger-system--describe-the-inositol-phosphatide-second-messenger-system-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Important members**

    <!--list-separator-->

    -  [Diacylglycerol]({{< relref "diacylglycerol" >}}) (DAG)

        -   Created through Phospholipase C-β cleavage of PIP<sub>2</sub>


#### [2,3-Diacylglycerol]({{< relref "2_3_diacylglycerol" >}}) {#2-3-diacylglycerol--2-3-diacylglycerol-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A form of [Diacylglycerol]({{< relref "diacylglycerol" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
