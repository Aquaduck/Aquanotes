+++
title = "Khan - Enzyme Regulation"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "khan", "source"]
draft = false
+++

Link: <https://www.khanacademy.org/science/biology/energy-and-enzymes/enzyme-regulation/a/enzyme-regulation>


## Regulatory molecules {#regulatory-molecules}


### Inhibitors {#inhibitors}


#### Reversible inhibitors {#reversible-inhibitors}

-   _Reversible inhibitor_: Molecule is not permanently attached to enzyme
-   Reversible inhibitors are divided into groups based on their binding behavior

{{< figure src="/ox-hugo/_20210715_153855screenshot.png" caption="Figure 1: Mechanisms of reversible inhibition" >}}

{{< figure src="/ox-hugo/_20210715_153706screenshot.png" caption="Figure 2: Effect of inhibitors on the rate of enzyme-catalyzed reactions" >}}


#### Competitive inhibition {#competitive-inhibition}

-   Inhibitor binds to the active site and **blocks the substrate from binding**
-   _Decreases_ reaction rate when little substrate, but can be "outcompeted" by a lot of substrate -> **enzyme still able to reach maximum rate**

<!--list-separator-->

-  Noncompetitive inhibition

    -   Inhibitor binds to a different site, not blocking the substrate but **deactivating the enzyme**
    -   Enzyme-catalyzed reaction will **never** reach its normal maximum rate


### Alloesteric regulation {#alloesteric-regulation}

-   _Allosteric regulation_: any form of regulation where the regulatory molecule (activator/inhibitor) binds to enzyme **somewhere other than active site**
    -   Binding site is called _allosteric site_

{{< figure src="/ox-hugo/_20210715_154120screenshot.png" caption="Figure 3: Allosteric mechanisms" >}}

-   **Virtually all cases of noncompetitive inhibition are forms of allosteric regulation**
-   [Allosteric enzymes]({{< relref "khan_basics_of_enzyme_kinematics" >}}): allosterically regulated enzymes with unique properties


### Cofactors and coenzymes {#cofactors-and-coenzymes}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 14. Explain how competitive, noncompetitive (mixed) and uncompetitive inhibitors react with enzymes and how that affects their kinetic parameters**

    [Regulatory molecules]({{< relref "khan_enzyme_regulation" >}})

    ---

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 13. Distinguish between reversible and irreversible inhibitors**

    [Regulatory molecules]({{< relref "khan_enzyme_regulation" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
