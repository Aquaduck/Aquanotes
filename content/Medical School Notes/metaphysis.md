+++
title = "Metaphysis"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Expanding part of [Long bone]({{< relref "tubular_bone" >}}) next to growth plate


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone formation > Endochondral bone formation**

    There are **five recognizable zones** that occur in sequence from the [Epiphysis]({{< relref "epiphysis" >}}) into the [Metaphysis]({{< relref "metaphysis" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Parts of a tubular bone**

    A [Metaphysis]({{< relref "metaphysis" >}}) and [Epiphysis]({{< relref "epiphysis" >}}) are also present at the top of the bone

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Parts of a tubular bone**

    [Metaphysis]({{< relref "metaphysis" >}}): expanding part of bone next to the growth plate

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
