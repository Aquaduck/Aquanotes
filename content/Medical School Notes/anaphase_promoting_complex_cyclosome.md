+++
title = "Anaphase promoting complex/cyclosome"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Inactivates [CDKs]({{< relref "cyclin_dependent_kinase" >}}) in [M-phase]({{< relref "m_phase" >}}) by ubiquitinating [cyclin B]({{< relref "cyclin_b" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop > Activation/inactivation mechanisms for CDKs > Inactivation of CDKs**

    The [Anaphase promoting complex/cyclosome]({{< relref "anaphase_promoting_complex_cyclosome" >}}) (APC/C) **inactivates** CDKs in [M-phase]({{< relref "m_phase" >}}) by ubiquitinating [cyclin B]({{< relref "cyclin_b" >}})

    ---


#### [APC disambiguation]({{< relref "apc_disambiguation" >}}) {#apc-disambiguation--apc-disambiguation-dot-md}

<!--list-separator-->

-  **🔖 APC can refer to:**

    [Anaphase promoting complex/cyclosome]({{< relref "anaphase_promoting_complex_cyclosome" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
