+++
title = "Session 5"
author = ["Arif Ahsan"]
date = 2021-07-16T00:00:00-04:00
tags = ["medschool", "jumpstart"]
draft = false
+++

## Integration Packet 1 {#integration-packet-1}

1.  B
2.  A
3.  B
4.  C
5.  B
6.  D
7.  B


## Integration Packet 2 {#integration-packet-2}

1.  A
2.  D
    -   Suicide inhibitors
3.  A
4.  C
    -   Remember Q vs K
    -   free energy related to K<sub>eq</sub>
5.  C
6.  B
7.  C


## Integration Packet 3 {#integration-packet-3}

1.  D
    -   Hydrophobic interactions influence tertiary structure
2.  D
3.  E
4.  E
    -   Remember michaelis-menten equation
5.  B
6.  D
7.  B


## Integration Packet 4 {#integration-packet-4}

1.  B
2.  A
3.  C
    -   Confidence interval: true value of proportion is within that range
    -   Intervals don't overlap
4.  A
5.  C
    -   Brush up on suicide inhibitors
6.  B
7.  C


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
