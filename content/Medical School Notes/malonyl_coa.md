+++
title = "Malonyl-CoA"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The **rate-limiting step** of [fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})
-   Inhibits [CPT1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) -> prevents synthesized fatty acids (>C12) from entering the [Mitochondria]({{< relref "mitochondrion" >}}) and being oxidized


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    This is required to prevent formation of [malonyl-CoA]({{< relref "malonyl_coa" >}}) (inhibits entry of long-chain fatty acids into mitochondria by blocking [CPT1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}), the first enzyme in the carnitine shuttle)

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Acetyl-CoA carboxylase**

    [Malonyl-CoA]({{< relref "malonyl_coa" >}}) binds to and inhibits [carnitine palmitoyl acyltransferase 1]({{< relref "carnitine_palmitoyl_acyltransferase_1" >}}) (CPT1) -> prevents synthesized fatty acids from entering mitochondria and being oxidized

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Metabolism**

    [Malonyl-CoA]({{< relref "malonyl_coa" >}}) is the rate-limiting step of fatty acid synthesis

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
