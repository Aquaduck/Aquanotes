+++
title = "Periarteriole lymphoid sheath"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Spleen**

    Surrounding central arterioles in the spleen are an aggregate of [T-lymphocytes]({{< relref "t_lymphocyte" >}}) called a [periarteriole lymphoid sheath]({{< relref "periarteriole_lymphoid_sheath" >}}) (PALS)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
