+++
title = "Buffer"
author = ["Arif Ahsan"]
date = 2021-07-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   Solutions that resist changes in pH
-   Salts of weak acids and bases form _buffer systems_
    -   Consist of an equilibrium between an acidic species and a basic species
    -   Acidic species will **donate protons to resist pH increase**, while the basic species will **accept protons to resist decrease in pH**
-   Buffer systems formed by weak acids have maximum buffering capacity at the pH = pKa of the acid
    -   When [acid] = [confjugate base], the system is buffered at pH = pKa of the acid
-   Buffer systems formed by weak bases have maximum buffering capacity at the pH = 14 - pKa of the base
    -   When [base] = [conjugate acid], the system is bufferred at pH = 14 - pKb of the base


## Influence on Titration Curves {#influence-on-titration-curves}

-   Buffers make the titration curve "flat" **at the region where buffering occurs**
    -   This is called the _point of inflection_
        -   pH = pKa (or 14 - pKb) of the buffer
    -   The area around the point of inflection = region where solution has buffering capacity
        -   pH of this region is typically pKa +/- 1 (or 14 - pKb +/- 1)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 General Chemistry > 3. Describe how the structure, or composition, of a buffer functions to resist changes in pH**

    [Buffer]({{< relref "buffer" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
