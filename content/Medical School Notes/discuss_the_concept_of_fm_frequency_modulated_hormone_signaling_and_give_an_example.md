+++
title = "Discuss the concept of FM (Frequency Modulated) hormone signaling and give an example."
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

from [AM vs FM signaling]({{< relref "cell_signalling_iii_underlying_design_features" >}})


## AM vs FM signaling {#am-vs-fm-signaling}


### The frequency of increases in [ligand] is a critical variable {#the-frequency-of-increases-in-ligand-is-a-critical-variable}

-   Initially believed that it was only absolute concentration
-   **If there are spikes in concentration more frequently, the activity of the affected enzyme will increase**

[Screenshot 2021-08-30 202632.png](<~/Pictures/surface-screenshots/Screenshot 2021-08-30 202632.png>)

-   **Key feature**
    -   Signal transduction depends on multi-protein complexes
    -   In the case of G-proteins, these involve large assemblies of receptors, G-protein complexes and G-protein targets (e.g. cAMP)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-31 Tue] </span></span> > Cell Signaling III > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Discuss the concept of FM (Frequency Modulated) hormone signaling and give an example.]({{< relref "discuss_the_concept_of_fm_frequency_modulated_hormone_signaling_and_give_an_example" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
