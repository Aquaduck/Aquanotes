+++
title = "Compare and contrast type I error and type II error"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}

| True state of the world |                     |                    |
|-------------------------|---------------------|--------------------|
| Decision                | H<sub>0</sub> false | H<sub>0</sub> true |
| Reject H<sub>0</sub>    | Correct decision    | Type I error       |
| Retain H<sub>0</sub>    | Type II error       | Correct decision   |


### [Type I error]({{< relref "type_i_error" >}}) {#type-i-error--type-i-error-dot-md}

-   **False positive** -> observed effect is actually due to chance
-   Null hypothesis is rejected when it is actually true


### [Type II error]({{< relref "type_ii_error" >}}) {#type-ii-error--type-ii-error-dot-md}

-   **False negative** -> observed effect did not occur due to chance
-   Null hypothesis is accpeted when it is actually false


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > The Practice of Evidence-Based Medicine: Inferential Statistics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Compare and contrast type I error and type II error]({{< relref "compare_and_contrast_type_i_error_and_type_ii_error" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
