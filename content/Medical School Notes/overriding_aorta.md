+++
title = "Overriding aorta"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   [Aorta]({{< relref "aorta" >}}) is displaced over the [ventricular septal defect]({{< relref "ventricular_septal_defect" >}}) (instead of the left ventricle).


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}}) {#tetralogy-of-fallot--tetralogy-of-fallot-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Overriding aorta]({{< relref "overriding_aorta" >}}) (above the VSD)

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Tetralogy of Fallot > Overview**

    <!--list-separator-->

    -  [Overriding aorta]({{< relref "overriding_aorta" >}}) (above the VSD)

        -   Aorta is displaced over the ventricular septal defect (instead of the left ventricle).
        -   Causes blood from both ventricles to flow into the aorta.


### Unlinked references {#unlinked-references}

[Show unlinked references]
