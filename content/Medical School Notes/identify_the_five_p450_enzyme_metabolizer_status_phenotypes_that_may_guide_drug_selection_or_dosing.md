+++
title = "Identify the five P450 enzyme metabolizer status phenotypes that may guide drug selection or dosing"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Pharmacogenetics Pre-learning Material]({{< relref "pharmacogenetics_pre_learning_material" >}}) {#from-pharmacogenetics-pre-learning-material--pharmacogenetics-pre-learning-material-dot-md}


### Five types of [P450]({{< relref "cytochrome_p450" >}}) enzyme metabolizers {#five-types-of-p450--cytochrome-p450-dot-md--enzyme-metabolizers}

1.  _Poor metabolizer_: markedly reduced or absent enzymatic activity -> **cannot be relied on to metabolize drugs or pro-drugs**
2.  _Intermediate metabolizer_: **reduced but still adequate metabolic capacity** depending on the context
3.  _Normal (extensive) metabolizer_: baseline enzymatic activity -> **do not require any adjustment in dose or drug choice**
4.  _Rapid metabolizer_: **mild increase in enzymatic activity**
5.  _Ultrarapid metabolizer_: **greatly increased enzymatic activity**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-24 Fri] </span></span> > Pharmacogenetics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Identify the five P450 enzyme metabolizer status phenotypes that may guide drug selection or dosing]({{< relref "identify_the_five_p450_enzyme_metabolizer_status_phenotypes_that_may_guide_drug_selection_or_dosing" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
