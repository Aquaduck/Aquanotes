+++
title = "Explain the t-test and the factors that influence t-values"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}


### [T-test]({{< relref "t_test" >}}) {#t-test--t-test-dot-md}

-   Calculates the difference between the [means]({{< relref "sample_mean" >}}) of two samples _or_ between a sample and a [population]({{< relref "population" >}})/value subject to change
    -   Especially when samples are small and/or population or _value subject to change distribution_ is not known


#### One sample t-test {#one-sample-t-test}

-   T-value can be classified according to a table that lists t-values and corresponding quantities **based on number of [degrees of freedom]({{< relref "degrees_of_freedom" >}})** and **[significance level]({{< relref "significance_level" >}})**
-   Alternatively, calculate [confidence intervals]({{< relref "confidence_interval" >}}) of the sample observations -> check if [population mean]({{< relref "population_mean" >}}) falls within the range given by confidence intervals
-   _Formula:_ \\(t = \frac{\bar{x} - \mu}{\frac{s}{\sqrt{n}}}\\) where:
    -   \\(t\\) = T-value
    -   \\(\bar{x}\\) = sample mean
    -   \\(\mu\\) = population mean
    -   \\(s\\) = sample standard deviation
    -   \\(n\\) = number of observations


#### Two sample t-test {#two-sample-t-test}

-   Calculates whether the means of two groups differ from one another
-   **Prerequisites:**
    1.  Both sample groups are drawn from the same population and have the same (but unknown) [variance]({{< relref "variance" >}})
    2.  The difference between the observations in the two groups approximately follows a [normal distribution]({{< relref "normal_distribution" >}})
-   _Formula:_ \\(t = \frac{\bar{x\_{1}} - \bar{x\_{2}}}{\sqrt{s^2(\frac{1}{n\_{1}} + \frac{1}{n\_{2}})}}\\) where:
    -   \\(\bar{x\_{1}} - \bar{x\_{2}\\) = mean difference between two samples
    -   \\(s\\) = sample standard deviation
    -   \\(n\\) = number of observations

<!--list-separator-->

-  [Unpaired t-test]({{< relref "unpaired_t_test" >}})

    -   Two **different groups** are sampled at the **same time**
    -   The difference between the means of a continuous outcome variable of these 2 groups is compared
    -   H<sub>0</sub> = \\(\bar{x\_{1}} = \bar{x\_{2}}\\)
    -   Statistically significant difference **rejects** the null hypothesis

<!--list-separator-->

-  [Paired t-test]({{< relref "paired_t_test" >}})

    -   The **same group** is sampled at **two different times**
    -   The difference between the means of a continuous outcome variable of these 2 groups is compared
    -   H<sub>0</sub> = \\(\bar{x\_{t\_{1}}} = \bar{x\_{t\_{2}}}\\) -> means at both times are **equal**
    -   Statistically significant difference **rejects** the null hypothesis


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > The Practice of Evidence-Based Medicine: Inferential Statistics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
