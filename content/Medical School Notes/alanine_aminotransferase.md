+++
title = "Alanine aminotransferase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [List the four enzymes, and their intracellular location, for which pyruvate serves as a substrate, and the product that is made]({{< relref "list_the_four_enzymes_and_their_intracellular_location_for_which_pyruvate_serves_as_a_substrate_and_the_product_that_is_made" >}}) {#list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made--list-the-four-enzymes-and-their-intracellular-location-for-which-pyruvate-serves-as-a-substrate-and-the-product-that-is-made-dot-md}

<!--list-separator-->

-  **🔖 Enzymes > Cytosol**

    [Alanine aminotransferase]({{< relref "alanine_aminotransferase" >}})

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Pyruvate metabolism**

    <!--list-separator-->

    -  [Alanine aminotransferase]({{< relref "alanine_aminotransferase" >}})

        -   A cytosolic enzyme
        -   Catalyzes the readily reversible transfer of an amino group from alanine to α-ketoglutarate -> form pyruvate and glutamate
            -   Allows amino acids to be used as gluconeogenic precursors


### Unlinked references {#unlinked-references}

[Show unlinked references]
