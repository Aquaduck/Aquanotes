+++
title = "Describe the general mechanisms of packaging cargo into vesicles."
author = ["Arif Ahsan"]
date = 2021-09-12T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Organelles and Trafficking I-II]({{< relref "organelles_and_trafficking_i_ii" >}}) {#from-organelles-and-trafficking-i-ii--organelles-and-trafficking-i-ii-dot-md}


### Role of coat proteins in physically driving vesicle formation {#role-of-coat-proteins-in-physically-driving-vesicle-formation}


### Role of adaptor proteins in cargo selection {#role-of-adaptor-proteins-in-cargo-selection}


### Role of GTPases in coating/uncoating cycle {#role-of-gtpases-in-coating-uncoating-cycle}

-   [GTPases]({{< relref "gtpase" >}}) **regulate** cycles of vesicle coating/uncoating
    -   Ubiquitious cellular on/off switches
    -   GTP hydrolysis induces a large conformational change in the GTPase protein
        -   Serves as a switch to alter the function of proteins the GTPase binds to
    -   Regulated by [GEF]({{< relref "guanine_nucleotide_exchange_factor" >}}) and [GAP]({{< relref "gtpase_activating_protein" >}})
-   Steps in vesicle coating/uncoating using [ARF1]({{< relref "adp_ribosylation_factor_1" >}}):
    1.  ARF1-GDP abundant in cytosol
    2.  [GEF]({{< relref "guanine_nucleotide_exchange_factor" >}}) **in donor compartment** phosphorylates ARF1 -> formation of ARF1-GTP
    3.  Conformation change -> **exposes lipid anchor**
    4.  ARF1 inserts into membrane -> recruits coat proteins
    5.  Coat assembles -> vesicle pinches off
    6.  [GAP]({{< relref "gtpase_activating_protein" >}}) dephosphorylates ARF1 -> formation of ARF1-GDP
    7.  Conformation change -> lipid anchor out of membrane -> **coat disassembles**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-01 Wed] </span></span> > Organelles & Trafficking I (+ II) > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Describe the general mechanisms of packaging cargo into vesicles.]({{< relref "describe_the_general_mechanisms_of_packaging_cargo_into_vesicles" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
