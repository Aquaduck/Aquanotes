+++
title = "Cardiovascular system"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Renin-angiotensin-aldosterone mechanism]({{< relref "renin_angiotensin_aldosterone_mechanism" >}}) {#renin-angiotensin-aldosterone-mechanism--renin-angiotensin-aldosterone-mechanism-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Major effector on the [Cardiovascular system]({{< relref "cardiovascular_system" >}})

    ---


#### [Langman's Medical Embryology]({{< relref "langman_s_medical_embryology" >}}) {#langman-s-medical-embryology--langman-s-medical-embryology-dot-md}

<!--list-separator-->

-  13: [Cardiovascular System]({{< relref "cardiovascular_system" >}})

    <!--list-separator-->

    -  Establishment and Patterning of the [Primary Heart Field]({{< relref "primary_heart_field" >}})

        -   The [vascular system]({{< relref "vascular_system" >}}) appears in the middle of the **third week** when the embryo is no longer able to satisfy its nutritional requirements by diffusion alone
        -   _Progenitor heart cells_ lie in the [epiblast]({{< relref "epiblast" >}}), immediately adjacent to the **cranial end** of the [primitive streak]({{< relref "primitive_streak" >}})
            -   From there, they migrate through the streak and into the visceral layer of lateral plate mesoderm -> some form a horseshoe-shaped cluster of cells called the [primary heart field]({{< relref "primary_heart_field" >}}) -> eventually form **portions of the atria** and **the entire [left ventricle]({{< relref "left_ventricle" >}})**


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927)**

    Major effect on the [cardiovascular system]({{< relref "cardiovascular_system" >}})

    ---


#### [Cardiac cycle]({{< relref "cardiac_cycle" >}}) {#cardiac-cycle--cardiac-cycle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The cycle of the [Heart]({{< relref "heart" >}}) to pump [blood]({{< relref "blood" >}}) through the [Cardiovascular system]({{< relref "cardiovascular_system" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
