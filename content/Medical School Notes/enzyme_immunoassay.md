+++
title = "Enzyme immunoassay"
author = ["Arif Ahsan"]
date = 2021-08-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [HIV Infection and AIDS]({{< relref "hiv_infection_and_aids" >}}) {#hiv-infection-and-aids--hiv-infection-and-aids-dot-md}

<!--list-separator-->

-  **🔖 Practice essentials > HIV screening recommendations**

    WHO recommends HIV testing whereby a combination of rapid diagnostic tests (RDTs) and/or [EIA]({{< relref "enzyme_immunoassay" >}})s are used to achieve at least a **99% positive predictive value**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
