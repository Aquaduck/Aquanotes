+++
title = "Apolipoprotein CII"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   ApoCII anchors the [chylomicron]({{< relref "chylomicron" >}}) to the [LPL]({{< relref "lipoprotein_lipase" >}}) molecules expressed by capillary endothelial cells


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Lipoprotein lipase > From Fat Metabolism in Muscle & Adipose Tissue**

    Tethering the LPL to the cell via a long polysaccharide chain consisting of _heparan sulfates_ -> **significantly improves the likelihood of an [apoCII]({{< relref "apocii" >}}):LPL interaction within the capillary**

    ---

<!--list-separator-->

-  **🔖 apoB48 & apoCII**

    <!--list-separator-->

    -  [ApoCII]({{< relref "apocii" >}})

        <!--list-separator-->

        -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

            -   ApoCII plays a **crucial role in delivering dietary fat to extrahepatic tissues**
            -   ApoCII anchors the chylomicron to the LPL molecules expressed by capillary endothelial cells

        <!--list-separator-->

        -  From [Amboss]({{< relref "amboss" >}})

            -   Cofactor for [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})
            -   A component of [chylomicrons]({{< relref "chylomicron" >}}), [VLDL]({{< relref "very_low_density_lipoprotein" >}}), and [HDL]({{< relref "high_density_lipoprotein" >}})


#### [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}}) {#lipoprotein-lipase--lipoprotein-lipase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Carries [apoCII]({{< relref "apocii" >}}) on its surface

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism > Lipoprotein lipase**

    Both aforementioned lipoproteins carry [apoCII]({{< relref "apocii" >}}) on their surface -> **facilitates interaction of lipoprotein lipase with lipoprotein**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
