+++
title = "Osmosis - Cellular Structure & Processes"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "osmosis", "source"]
draft = false
+++

## [Cellular]({{< relref "cell" >}}) Structure & Function {#cellular--cell-dot-md--structure-and-function}

{{< figure src="/ox-hugo/_20210715_191656screenshot.png" >}}


### Cell Structure Basics {#cell-structure-basics}

-   Cells are the basic structural, biological, functional unit that comprise organisms
-   Smallest self-replicating life-form
-   Cells -> tissue -> organ -> organ systems -> organism


### Cytosol {#cytosol}

-   Intracellular fluid
    -   Composition:
        -   Water
        -   Dissolved/suspended organic and inorganic chemicals
        -   Macromolecules
        -   Pigments
        -   Organelles
-   **Site of most cellular activity**


### Organelles {#organelles}

-   Specialized cellular subunits carry out essential function


#### Ribosomes {#ribosomes}

-   Composition: rRNA, ribosomal proteins
-   Can exist freely in cytoplasm/bound to endoplasmic reticulum (forms rough endoplasmic reticuluml)
-   **Turns mRNA into proteins via translation**
-   Organized into two subunits (40s, 60s)
    -   _Small subunit_: binding sites for mRNA, tRNA
    -   _Large subunit_: contains ribozyme to catalyze peptide bond formation


## [Cell]({{< relref "cell" >}}) Membrane {#cell--cell-dot-md--membrane}

-   Semipermeable membrane made from phospholipid bilayer; surrounds cell cytoplasm


### Phospholipid bilayer {#phospholipid-bilayer}

-   Two-layered polar phospholipid molecules comprising two parts:
    1.  **Negatively charged phosphate head**
        -   Hydrophilic and oriented outwards
    2.  **Fatty acid tail**
        -   Hydrophobic and oriented inwards
-   **Semipermeable**
    -   Allows passage of certain molecules (e.g. small molecules) and denies passage for others (e.g. large molecules)

{{< figure src="/ox-hugo/_20210715_191419screenshot.png" caption="Figure 1: Phospholipid parts and their arrangement in a cell membrane" >}}


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
