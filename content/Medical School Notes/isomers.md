+++
title = "Isomers"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   <span class="underline">Isomers</span> are groups of molecules that have the same molecular formula but different structure


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Kaplan Organic Chemistry Chapter 2]({{< relref "kaplan_organic_chemistry_chapter_2" >}}) {#kaplan-organic-chemistry-chapter-2--kaplan-organic-chemistry-chapter-2-dot-md}

<!--list-separator-->

-  **🔖 2.1 Structural Isomers**

    Structural isomers are the **least similar of all the [isomers]({{< relref "isomers" >}})**

    ---


#### [Kaplan Biochemistry Chapter 2]({{< relref "kaplan_biochemistry_chapter_2" >}}) {#kaplan-biochemistry-chapter-2--kaplan-biochemistry-chapter-2-dot-md}

<!--list-separator-->

-  **🔖 2.1 Enzymes as Biological Catalysts > Enzyme classifications > 6 categories of enzymes mechanisms > Isomerases**

    Work on both stereoisomers and constitutional [isomers]({{< relref "isomers" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
