+++
title = "Openstax Anatomy & Physiology"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

Where I keep notes that haven't been connecting to a learning objective yet


## [Antidiuretic Hormone]({{< relref "antidiuretic_hormone" >}}) (ADH) (p.746) {#antidiuretic-hormone--antidiuretic-hormone-dot-md----adh----p-dot-746}

-   Blood osmolarity is constantly monitored by [osmoreceptors]({{< relref "osmoreceptor" >}}) - specialized cells within the hypothalamus that are particularly sensitive to the concentration of sodium ions and other solutes
-   In response to **high blood osmolarity** -> [osmoreceptors]({{< relref "osmoreceptor" >}}) signal the [posterior pituitary]({{< relref "posterior_pituitary" >}}) to release ADH
-   The target cells of ADH are located in the [tubular cells]({{< relref "tubular_cell" >}}) of the [kidney]({{< relref "kidney" >}})
-   The **effect of ADH** is to **increase epithelial permeability to water** -> allows **increased water absorption**
-   In very high concentrations -> constricts blood vessels -> increase in blood pressure via increase of peripheral resistance
-   The release of ADH is controlled by a negative feedback loop
    -   As blood osmolarity decreases -> hypothalamic [osmoreceptors]({{< relref "osmoreceptor" >}}) sense change -> decrease secretion of ADH -> less water reabsorbed from urine


## Plasma Proteins (p. 796) {#plasma-proteins--p-dot-796}


### [Albumin]({{< relref "albumin" >}}) {#albumin--albumin-dot-md}

-   **The most abundant plasma protein**
-   Manufactured by the [Liver]({{< relref "liver" >}})
-   Albumin functions as a binding protein -> **transports fatty acids and steroid hormones**
-   **The most significant contributor to osmotic pressure of blood**
    -   Albumin's presence holds water inside blood vessels and draws water from the tissues across blood vessel walls and into the bloodstream
        -   Low serum albumin leads to [edema]({{< relref "edema" >}})


### [Globulin]({{< relref "globulin" >}}) {#globulin--globulin-dot-md}

-   Second most common plasma protein
-   Three main subgroups: alpha, beta, gamma globulins
    -   [Alpha]({{< relref "alpha_globulin" >}}) and [beta]({{< relref "beta_globulin" >}}) globulins transport iron, lipids, and fat-soluble vitamins A, D, E, and K to the cells
        -   They also contribute to osmotic pressure
    -   [Gamma globulins]({{< relref "gamma_globulin" >}}) are proteins involved in immunity, better known as antibodies or [immunoglobulins]({{< relref "immunoglobulin" >}})
        -   Gamma globulins are produced by plasma cells


### [Fibrinogen]({{< relref "fibrinogen" >}}) {#fibrinogen--fibrinogen-dot-md}

-   Produced by the liver
-   Essential for [blood clotting]({{< relref "blood_clotting" >}})


## Disorders of [Platelets]({{< relref "thrombocyte" >}}) (p. 813) {#disorders-of-platelets--thrombocyte-dot-md----p-dot-813}

-   [Thrombocytosis]({{< relref "thrombocytosis" >}}): too many platelets
    -   Thrombocytosis may lead to the formation of unwanted [blood clots]({{< relref "blood_clotting" >}}) ([thrombosis]({{< relref "thrombosis" >}}))
-   [Thrombocytopenia]({{< relref "thrombocytopenia" >}}): too few platelets
    -   May lead to blood not clotting properly -> excessive bleeding


## [Fibrinolysis]({{< relref "fibrinolysis" >}}) (p. 818) {#fibrinolysis--fibrinolysis-dot-md----p-dot-818}

-   Stablized clot is acted upon by contractile proteins within platelets
-   To restore normal blood flow, the clot must be removed -> _Fibrinolysis_
-   Inactive protein plasminogen converted to [plasmin]({{< relref "plasmin" >}}) -> gradually breaks down [fibrin]({{< relref "fibrin" >}}) of the clot
-   [Bradykinin]({{< relref "bradykinin" >}}) is released -> vasodilation -> reverses the effects of serotonin and prostaglandins from platelets
    -   Allows smooth muscle in the walls of the vessels to relax + restore circulation


## Input to the Cardiovascular Center (p.879) {#input-to-the-cardiovascular-center--p-dot-879}


### [Baroreceptor]({{< relref "baroreceptor" >}}) {#baroreceptor--baroreceptor-dot-md}

-   Stretch receptors located in the aortic sinus, carotid bodies, venae cavae, and other locations such as pulmonary vessels and the right side of the heart itself
-   Rates of firing from baroreceptors represent blood pressure, level of physical activity, and relative distribution of blood
-   [Baroreceptor reflex]({{< relref "baroreceptor_reflex" >}}): Cardiac center monitoring of baroreceptor firing to maintain cardiac homeostasis
    -   In response to **increased pressure and stretch** -> rate of baroreceptor firing increases -> cardiac centers decrease sympathetic stimulation + increase parasympathetic stimulation
    -   In response to **decreased pressure and stretch** -> rate of baroreceptor firing decreases -> cardiac centers increase sympathetic stimulation + decrease parasympathetic stimulation


## Structure and Function of Blood Vessels (p. 898) {#structure-and-function-of-blood-vessels--p-dot-898}


### Shared Structures (p. 899) {#shared-structures--p-dot-899}


#### [Vasa vasorum]({{< relref "vasa_vasorum" >}}) (p. 900) {#vasa-vasorum--vasa-vasorum-dot-md----p-dot-900}

-   Literally "vessels of the vessel"
-   Small blood vessels in larger arteries and veins to supply themselves with blood
-   Since pressure within arteries is relatively high, the vasa vasorum must function in the **outer layers of this vessel**
    -   Otherwise, the pressure exerted by passing blood would collapse the vessels
    -   Restriction of vasa vasorum to outer layers of arteries is thought to be one reason that arterial diseases are more common than venous diseases
-   Lower pressure in veins -> vasa vasorum is located closer to lumin


## Metarterioles and Capillary Beds (p. 904) {#metarterioles-and-capillary-beds--p-dot-904}

-   _Metarteriole_: Has structural characteristics of **both an arteriole and a capillary**
    -   The smooth muscle of its tunica media is **not continuous** - forms rings of smooth muscle (_sphincters_) prior to the entrance to the capillaries


## [Atrial Natriuretic Hormone]({{< relref "atrial_natriuretic_hormone" >}}) (p. 928) {#atrial-natriuretic-hormone--atrial-natriuretic-hormone-dot-md----p-dot-928}

-   Secreted by cells in the atria of the heart when high blood volume -> extreme stretching of cardiac cells
-   ANH is antagonistic to angiotensin II
    -   Promote **loss of sodium and water from the kidneys**
    -   Suppress renin, aldosterone, and ADH production/release
    -   **Results in drop of blood volume and blood pressure**


## [Lymphoid Nodules]({{< relref "lymphoid_nodule" >}}) (p. 997) {#lymphoid-nodules--lymphoid-nodule-dot-md----p-dot-997}

-   Consist of a dense cluster of lymphocytes without a surrounding fibrous capsules
-   Located in the respiratory and digestive tracts - areas routinely exposed to environmental pathogens


### [Tonsils]({{< relref "tonsil" >}}) {#tonsils--tonsil-dot-md}

-   Lymphoid nodules located along the inner surface of the pharynx
-   Important in developing immunity to oral pathogens
-   The tonsil located at the back of the throat (_pharyngeal tonsil_) is sometimes referred to as the _adenoid_ when swollen
-   Epithelial layer invaginates deeply into the interior of the tonsil to form _tonsillar crypts_
    -   Accumulate materials taken into the body through eating and breathing
    -   A "honey pot" for pathogens
-   **Main function**: help children's bodies recognize, destroy, and develop immunity to common environmental pathogens


### [MALT]({{< relref "mucosa_associated_lymphoid_tissue" >}}) {#malt--mucosa-associated-lymphoid-tissue-dot-md}

-   An aggregate of lymphoid follicles directly assoicated with the mucous membrane epithelia
-   Dome-shaped structures found underlying mucosa of:
    -   GI tract
    -   Breast tissue
    -   Lungs
    -   Eyes
-   _Peyer's patches_: contain specialized endothelial cells (called _M (microfold) cells_) -> sample material from intestinal lumen -> transport to nearby follicles -> allows adaptive immune response to potential pathogens

{{< figure src="/ox-hugo/_20211018_141838screenshot.png" >}}


## [Pleura]({{< relref "pleura" >}}) of the lung (p. 1057) {#pleura--pleura-dot-md--of-the-lung--p-dot-1057}

-   Each lung enclosed within a cavity surrounded by the pleura
-   _Pleura_: a serous membrane that surrounds the lung
    -   [Visceral pleura]({{< relref "visceral_pleura" >}}): the layer superficial to the lungs, extending into and lining the lung fissures
    -   [Parietal pleura]({{< relref "parietal_pleura" >}}): Outer layer that connects the thoracic wall, the mediastinum, and the diaphragm
    -   Visceral and parietal pleura connect to each other at the [hilum of the lung]({{< relref "hilum_of_the_lung" >}})
    -   [Pleural cavity]({{< relref "pleural_cavity" >}}): space between the visceral and parietal layers

{{< figure src="/ox-hugo/_20211016_201442screenshot.png" caption="Figure 1: Parietal and visceral pleurae of the lungs" >}}

-   **Two major functions of the pleura:**
    1.  Produce pleural fluid
    2.  Create cavities to separate the major organs
-   [Pleural fluid]({{< relref "pleural_fluid" >}}): lubricates the surfaces of the two pleural layers
    -   Secreted by mesothelial cells
    -   Both reduces physical trauma from breathing and helps maintain the position of the lungs against the thoracic wall
    -   Also helps prevent the spread of infection


## [Oxygen]({{< relref "oxygen" >}}) Dissociation from [Hemoglobin]({{< relref "hemoglobin" >}}) (p. 1074) {#oxygen--oxygen-dot-md--dissociation-from-hemoglobin--hemoglobin-dot-md----p-dot-1074}

-   _Oxygen-hemoglobin dissociation curve_: graph that describes the relationship of partial pressure to the binding of oxygen to [heme]({{< relref "heme" >}}) vs. its subsequent dissociation from heme
    -   Two important rules to this relationship:
        1.  **Gases travel from an area of high partial pressure to an area of lower partial pressure**
        2.  **Oxygen affinity to [heme]({{< relref "heme" >}}) increases as more oxygen molecules are bound**
    -   Thus -> **as partial pressure of oxygen increases, more oxygen is bound by heme**

{{< figure src="/ox-hugo/_20211017_162608screenshot.png" caption="Figure 2: Partial pressure of oxygen and hemoglobin saturation" >}}

-   Certain hormones can affect the oxygen-hemoglobin saturation/dissociation curve by **stimulating the production of [2,3-BPG]({{< relref "2_3_biphosphoglycerate" >}})** e.g.:
    -   Androgens
    -   Epinephrine
    -   Thyroid hormones
    -   Growth hormone
-   BPG is a byproduct of glycolysis


## Absorption (p. 1144) {#absorption--p-dot-1144}


### [Mineral]({{< relref "mineral" >}}) absorption {#mineral--mineral-dot-md--absorption}


#### [Calcium]({{< relref "calcium" >}}) {#calcium--calcium-dot-md}

-   Blood levels of ionic calcium determine the absorption of dietary calcium
-   When blood levels of ionic calcium drop -> [PTH]({{< relref "parathyroid_hormone" >}}) secreted -> stimulates **release of calcium ions from bone matrices** + **increases reabsorption of calcium by [kidneys]({{< relref "kidney" >}})**
-   [PTH]({{< relref "parathyroid_hormone" >}}) upregulates the activation of [vitamin D]({{< relref "vitamin_d" >}}) in the [kidney]({{< relref "kidney" >}}) -> facilitates intestinal calcium ion absorption


## [Endothelin]({{< relref "endothelin" >}}) (p. 1242) {#endothelin--endothelin-dot-md----p-dot-1242}

-   _Endothelins_ are extremely powerful vasoconstrictors
-   Produced in the endothelial cells of the renal blood vessels, mesangial cells, and cells of the DCT
-   Stimulated by:
    -   Angiotensin II
    -   Bradykinin
    -   Epinephrine
-   **Do not typically influence blood pressure in healthy people**
-   Diminish GFR by damaging podocytes and potently vasoconstricting afferent and efferent arterioles


## [Metabolic Alkalosis]({{< relref "metabolic_alkalosis" >}}): Primary Bicarbonate Excess (p. 1281) {#metabolic-alkalosis--metabolic-alkalosis-dot-md--primary-bicarbonate-excess--p-dot-1281}

-   [Blood]({{< relref "blood" >}}) is too alkaline (pH above 7.45) due to **too much [bicarbonate]({{< relref "bicarbonate" >}})** (called _primary bicarbonate excess_)
-   Causes of metabolic alkalosis:
    -   Can be transiently caused by an ingestion of excessive [bicarbonate]({{< relref "bicarbonate" >}}), [citrate]({{< relref "citrate" >}}), or [antacids]({{< relref "antacid" >}})
    -   [Cushing's disease]({{< relref "cushing_s_disease" >}}): oversecretion of [ACTH]({{< relref "adrenocorticotrophic_hormone" >}}) -> elevated [aldosterone]({{< relref "aldosterone" >}}) + more [potassium]({{< relref "potassium" >}}) excreted in [urine]({{< relref "urine" >}})


## [Respiratory Acidosis]({{< relref "respiratory_acidosis" >}}): Primary Carbonic Acid/Carbon Dioxide Excess {#respiratory-acidosis--respiratory-acidosis-dot-md--primary-carbonic-acid-carbon-dioxide-excess}

-   Occurs when the [blood]({{< relref "blood" >}}) is overly acidic due to an excess of [carbonic acid]({{< relref "carbonic_acid" >}}) <- excess [carbon dioxide]({{< relref "carbon_dioxide" >}}) in the blood
-   Can result from anything that interferes with respiration, e.g. [pneumonia]({{< relref "pneumonia" >}}), [emphysema]({{< relref "emphysema" >}}), or [congestive heart failure]({{< relref "congestive_heart_failure" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  From [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}})

    <!--list-separator-->

    -  [Renin-Angiotensin-Aldosterone Mechanism]({{< relref "renin_angiotensin_aldosterone_mechanism" >}}) (p. 927)

        -   Major effect on the [cardiovascular system]({{< relref "cardiovascular_system" >}})

        <!--list-separator-->

        -  [Renin]({{< relref "renin" >}}) + Angiotensin

            -   Specialized cells in the [kidneys]({{< relref "kidney" >}}) found in the [juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}}) respond to decreased blood flow by secreting _renin_ into the blood
                -   Converts the plasma protein [angiotensinogen]({{< relref "angiotensinogen" >}}) (produced by the liver) into its active form - [angiotensin I]({{< relref "angiotensin_i" >}})

        <!--list-separator-->

        -  Angiotensin ([I]({{< relref "angiotensin_i" >}}) + [II]({{< relref "angiotensin_ii" >}}))

            -   Angiotensin I circulates in the blood -> reaches lungs -> converted into Angiotensin II
            -   Angiotensin II is a powerful **vasoconstrictor** -> greatly increases blood pressure
            -   Angiotensin II stimulates the release of [ADH]({{< relref "antidiuretic_hormone" >}}) from the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) and [aldosterone]({{< relref "aldosterone" >}}) from the [adrenal cortex]({{< relref "adrenal_cortex" >}})
            -   Angiotensin II stimulates the thirst center in the hypothalamus -> increase of fluid consumption -> increase blood volume and blood pressure

        <!--list-separator-->

        -  [Aldosterone]({{< relref "aldosterone" >}})

            -   Increases the reabsorption of [sodium]({{< relref "sodium" >}}) into the [blood]({{< relref "blood" >}}) by the [kidneys]({{< relref "kidney" >}}) -> **increases reabsorption of water** -> increases blood volume -> raises blood pressure


#### [Explain how the defect leads to dysfunction in the lungs.]({{< relref "explain_how_the_defect_leads_to_dysfunction_in_the_lungs" >}}) {#explain-how-the-defect-leads-to-dysfunction-in-the-lungs-dot--explain-how-the-defect-leads-to-dysfunction-in-the-lungs-dot-md}

<!--list-separator-->

-  From [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
