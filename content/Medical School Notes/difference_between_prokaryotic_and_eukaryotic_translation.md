+++
title = "Difference Between Prokaryotic and Eukaryotic Translation"
author = ["Arif Ahsan"]
date = 2021-07-28T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

[Link](https://www.researchgate.net/publication/314214643%5FDifference%5FBetween%5FProkaryotic%5Fand%5FEukaryotic%5FTranslation)


## Prokaryotic Translation vs. Eukaryotic Translation {#prokaryotic-translation-vs-dot-eukaryotic-translation}

| Prokaryotic Translation                            | Eukaryotic Translation                                    |
|----------------------------------------------------|-----------------------------------------------------------|
| Transcription and translation are **simultaneous** | Transcription **precedes** translation                    |
| 30S + 50S = 70S ribosomes                          | 40S + 60S = 80S ribosomes                                 |
| mRNA is in **cytoplasm**                           | mRNA is in **nucleus**                                    |
| mRNA unstable; lives for few seconds to 2 minutes  | mRNA stable; lives for few hours to days                  |
| Performed by **70S ribosomes in cytoplasm**        | Performed by **80S ribosomes attached with the ER**       |
| No definite cell cycle phase                       | Occurs in **G1** and **G2** phases                        |
| Cap-**independent** initiation                     | **Both** cap-_independent_ and cap-_dependent_ initiation |
| Three initiation factors: _IF1, IF2, IF3_          | Nine initiation factors: _1, 2, 3, 4A, 4B, 4C, 4D, 5, 6_  |
| Fast                                               | Slow                                                      |
| **Single** release factor: _cRF1_                  | **Two** release factors: _RF1, RF2_                       |


## Prokaryotic Translation {#prokaryotic-translation}

-   **Simultaneously** synthesizing proteins with transcription
-   Begins just after transcribing the 5' end of the gene into mRNA
-   **GTP** is used as the energy source for the peptide bond formation
-   Translation initiation factor is EF-P
-   When bacteria enter stationary phase -> translation downregulated by _dimerization of ribosomes_


### Elongation {#elongation}

-   [Ribosome]({{< relref "ribosome" >}}) binds to the _Shine-Dalgarno sequence_ -> form double-stranded RNA structure
    -   _Purine_ rich region located upstream of AUG start codon
    -   Complimentary to the _pyrimidine_-rich region on 16S rRNA
        -   16S component of 30S subunit
    -   This pairing brings initiation codon into P-site of ribosome
-   Translation elongates until the [ribosome]({{< relref "ribosome" >}}) reaches one of three stop codons (UAA, UGA, UAG)
    -   Release factors recognize stop codon -> hydrolyze ester bond -> release polypeptide chain
        -   IF3 releases mRNA by replacing deacylated tRNA


## Eukaryotic Translation {#eukaryotic-translation}

-   Second step of eukaryotic gene expression after transcription
-   Eukaryotic mRNAs are processed in the nucleus by adding a 5' cap, poly A tail, and splicing out introns before they are released into the cytoplasm
-   _Ribosomal pausing_ affects translation by co-translational folding of the newly synthesizing polypeptide chain on the ribosome


### Cap-dependent initiation {#cap-dependent-initiation}

-   Initiation factors bind to 5' end of mRNA
    -   hold mRNA in the small subunit of ribosome


### Cap-independent initiation {#cap-independent-initiation}

-   Internal ribosome entry sites allow ribosome to directly bind to start site


### Elongation {#elongation}

-   Similar to that of prokaryotes


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
