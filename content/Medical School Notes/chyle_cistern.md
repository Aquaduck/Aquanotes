+++
title = "Chyle cistern"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}}) {#list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot--list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymphatic anatomy**

    Lymph from **the GI tract** joins that from the lower extremities in the [Chyle cistern]({{< relref "chyle_cistern" >}}) -> enters thoracic duct

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
