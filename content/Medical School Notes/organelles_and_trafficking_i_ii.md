+++
title = "Organelles and Trafficking I-II"
author = ["Arif Ahsan"]
date = 2021-09-02T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Endoplasmic reticulum {#endoplasmic-reticulum}

-   A **single compartment**


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the general mechanisms of packaging cargo into vesicles.]({{< relref "describe_the_general_mechanisms_of_packaging_cargo_into_vesicles" >}}) {#describe-the-general-mechanisms-of-packaging-cargo-into-vesicles-dot--describe-the-general-mechanisms-of-packaging-cargo-into-vesicles-dot-md}

<!--list-separator-->

-  From [Organelles and Trafficking I-II]({{< relref "organelles_and_trafficking_i_ii" >}})

    <!--list-separator-->

    -  Role of coat proteins in physically driving vesicle formation

    <!--list-separator-->

    -  Role of adaptor proteins in cargo selection

    <!--list-separator-->

    -  Role of GTPases in coating/uncoating cycle

        -   [GTPases]({{< relref "gtpase" >}}) **regulate** cycles of vesicle coating/uncoating
            -   Ubiquitious cellular on/off switches
            -   GTP hydrolysis induces a large conformational change in the GTPase protein
                -   Serves as a switch to alter the function of proteins the GTPase binds to
            -   Regulated by [GEF]({{< relref "guanine_nucleotide_exchange_factor" >}}) and [GAP]({{< relref "gtpase_activating_protein" >}})
        -   Steps in vesicle coating/uncoating using [ARF1]({{< relref "adp_ribosylation_factor_1" >}}):
            1.  ARF1-GDP abundant in cytosol
            2.  [GEF]({{< relref "guanine_nucleotide_exchange_factor" >}}) **in donor compartment** phosphorylates ARF1 -> formation of ARF1-GTP
            3.  Conformation change -> **exposes lipid anchor**
            4.  ARF1 inserts into membrane -> recruits coat proteins
            5.  Coat assembles -> vesicle pinches off
            6.  [GAP]({{< relref "gtpase_activating_protein" >}}) dephosphorylates ARF1 -> formation of ARF1-GDP
            7.  Conformation change -> lipid anchor out of membrane -> **coat disassembles**


#### [Describe protein folding, identifying roles for chaperones, BiP, peptidyl prolyl isomerases and protein disulfide isomerases.]({{< relref "describe_protein_folding_identifying_roles_for_chaperones_bip_peptidyl_prolyl_isomerases_and_protein_disulfide_isomerases" >}}) {#describe-protein-folding-identifying-roles-for-chaperones-bip-peptidyl-prolyl-isomerases-and-protein-disulfide-isomerases-dot--describe-protein-folding-identifying-roles-for-chaperones-bip-peptidyl-prolyl-isomerases-and-protein-disulfide-isomerases-dot-md}

<!--list-separator-->

-  From [Organelles and Trafficking I-II]({{< relref "organelles_and_trafficking_i_ii" >}})

    <!--list-separator-->

    -  Folding

        -   [Chaperone proteins]({{< relref "chaperone_protein" >}}) bind to:
            -   Hydrophobic patches (e.g. BiP)
                -   [Binding immunoglobulin protein]({{< relref "binding_immunoglobulin_protein" >}}) - helps keep hydrophobic sections "buried" within protein
            -   Incompletely trimmed sugars
            -   Non disulfide-linked cystein residues
            -   On incompletely folded proteins -> **prevent aggregation & keep unfolded proteins in the ER until they achieve the right conformation**
        -   [Peptidyl-prolyl isomerase]({{< relref "peptidyl_prolyl_isomerase" >}})
            -   Catalyzes **rotation** around the peptide bond N-terminal to **proline**
                -   Due to proline's ring, it creates **rigidity**

    <!--list-separator-->

    -  Formation of disulfide bonds

        -   [Protein disulfide isomerase]({{< relref "protein_disulfide_isomerase" >}}) (PDI)
            -   In the ER
            -   **Oxidoreductase** and **isomerase** properties
            -   Formation of disulfide bonds and fixes incorrect disulfide bond


### Unlinked references {#unlinked-references}

[Show unlinked references]
