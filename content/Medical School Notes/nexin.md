+++
title = "Nexin"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Connects adjacent doublet [microtubules]({{< relref "microtubule" >}}) to help maintain the shape of [cilium]({{< relref "cilium" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
