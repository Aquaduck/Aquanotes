+++
title = "Khan - London Dispersion Forces"
author = ["Arif Ahsan"]
date = 2021-07-16T00:00:00-04:00
tags = ["medschool", "khan", "videos", "source"]
draft = false
+++

Link: <https://www.khanacademy.org/science/ap-chemistry-beta/x2eef969c74e0d802:intermolecular-forces-and-properties/x2eef969c74e0d802:intermolecular-forces/v/london-dispersion-forces>


## Overview {#overview}

-   _London dispersion forces_ result from the coulombic interactions between **instantaneous dipoles**
-   Present between all molecules, greater for heavier and more polarizable molecules


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
