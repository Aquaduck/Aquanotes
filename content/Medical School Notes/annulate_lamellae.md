+++
title = "Annulate lamellae"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   An [organelle]({{< relref "organelle" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles**

    <!--list-separator-->

    -  [Annulate lamellae]({{< relref "annulate_lamellae" >}})

        <!--list-separator-->

        -  Structure

            -   Parallel stacks of membranes (usually 6 to 10) that resemble the nuclear envelope (including its pore complexes)
            -   Often arraged with their _annuli_ (pores) in register and are **frequently continuous with the RER**

        <!--list-separator-->

        -  Function

            -   Found in rapidly growing cells
                -   Germ cells
                -   Embryonic cells
                -   Tumor cells
            -   Function and significance remain unknown


### Unlinked references {#unlinked-references}

[Show unlinked references]
