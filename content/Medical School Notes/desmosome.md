+++
title = "Desmosome"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}}) {#name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot--name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Epidermis - strata > Stratum spinosum - "spiny layer"**

    Correspond to [desmosomes]({{< relref "desmosome" >}})

    ---


#### [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}}) {#know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot--know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Overview of Basic Skin > 3 layers > Epidermis > Components**

    Held together by [desmosomes]({{< relref "desmosome" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
