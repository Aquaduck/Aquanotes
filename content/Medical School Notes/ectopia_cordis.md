+++
title = "Ectopia cordis"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Embryology**

    <!--list-separator-->

    -  [Ectopia cordis]({{< relref "ectopia_cordis" >}})

        -   Rare congenital condition where the [Heart]({{< relref "heart" >}}) is completely or partially located outside the thoracic cavity
        -   Caused by impaired migration of lateral mesoderm to the midline -> incomplete fusion of anterior chest wall


### Unlinked references {#unlinked-references}

[Show unlinked references]
