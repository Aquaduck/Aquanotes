+++
title = "Explain the basic concepts of the baroreceptors and chemoreceptors in the cardiovascular system."
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From <span class="timestamp-wrapper"><span class="timestamp">[2021-10-16 Sat] </span></span> Session {#from-session}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-18 Mon] </span></span> > Regional and Peripheral Circulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Explain the basic concepts of the baroreceptors and chemoreceptors in the cardiovascular system.]({{< relref "explain_the_basic_concepts_of_the_baroreceptors_and_chemoreceptors_in_the_cardiovascular_system" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
