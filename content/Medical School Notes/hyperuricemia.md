+++
title = "Hyperuricemia"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   High levels of [Uric acid]({{< relref "uric_acid" >}}) in the blood


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Diseases associated with purine degradation > Gout**

    [Hyperuricemia]({{< relref "hyperuricemia" >}}) -> deposition of monosodium urate crystlas in the joints -> triggers inflammatory response to the crystals -> _gouty arthritis_

    ---

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Diseases associated with purine degradation > Gout**

    Characterized by high levels of [Uric acid]({{< relref "uric_acid" >}}) in the blood ([hyperuricemia]({{< relref "hyperuricemia" >}})) as a result of either **overproduction** or **underexcretion** of **uric acid**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
