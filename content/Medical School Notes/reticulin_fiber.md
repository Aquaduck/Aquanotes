+++
title = "Reticulin fiber"
author = ["Arif Ahsan"]
date = 2021-10-10T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}}) {#list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot--list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Lymph nodes > Parts of the lymph node:**

    The overall structure of a lymph node is formed by [reticulin fibers]({{< relref "reticulin_fiber" >}}) - a thin/fine type of collagen that allows easy movement of immune cells

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
