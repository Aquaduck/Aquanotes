+++
title = "G protein"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Pharmacology: General Principles of Pharmacodynamics II]({{< relref "pharmacology_general_principles_of_pharmacodynamics_ii" >}}) {#pharmacology-general-principles-of-pharmacodynamics-ii--pharmacology-general-principles-of-pharmacodynamics-ii-dot-md}

<!--list-separator-->

-  **🔖 Receptor signaling: drug effects > G protein-coupled receptors**

    Several different [G proteins]({{< relref "g_protein" >}}) that couple to numerous receptors and can have opposing effects:

    ---


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase**

    Intracellular domain that interacts with trimeric [G proteins]({{< relref "g_protein" >}})

    ---


#### [Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important.]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}}) {#describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot--describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Off Signals > Several pathways in the cell to turn off a signal: > Inactivating transducer - G-protein GTPase**

    Essentially, shortly after activation steps have already been set in motion to inactivate the [G-protein]({{< relref "g_protein" >}})

    ---


#### [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}) {#describe-the-inositol-phosphatide-second-messenger-system--describe-the-inositol-phosphatide-second-messenger-system-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Important members**

    <!--list-separator-->

    -  [G-proteins]({{< relref "g_protein" >}})

        -   G<sub>q</sub> activated when ligand binds to GPCR
        -   Behaves like G<sub>s</sub> or G<sub>i</sub>
        -   Activates phospholipase C-β


#### [Describe the cyclic-AMP (cAMP) second messenger signal pathway from hormone binding to activation of protein kinase-A.]({{< relref "describe_the_cyclic_amp_camp_second_messenger_signal_pathway_from_hormone_binding_to_activation_of_protein_kinase_a" >}}) {#describe-the-cyclic-amp--camp--second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot--describe-the-cyclic-amp-camp-second-messenger-signal-pathway-from-hormone-binding-to-activation-of-protein-kinase-a-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > G-protein signal cascades: cAMP**

    [G-protein]({{< relref "g_protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
