+++
title = "Bioenergetics and Oxidative Phosphorylation"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "papers", "source"]
draft = false
+++

Link:  <https://vicportal.med.uvm.edu/bbcswebdav/pid-34291-dt-content-rid-134686%5F1/courses/CC%5F2021-1/July%2012%20Free%20energy%20objectives.pdf>


## [Free Energy]({{< relref "free_energy" >}}) {#free-energy--free-energy-dot-md}

-   The direction and extent a chemical reaction proceeds is determined by \\(\Delta H\\) (change in enthalpy) and \\(\Delta S\\) (change in entropy)
    -   _Enthalpy_: Change in heat content of reactants + products
    -   _Entropy_: Change in randomness/disorder of reactants + products


## [Free Energy]({{< relref "free_energy" >}}) Change {#free-energy--free-energy-dot-md--change}

-   Represented by either \\(\Delta G\\) or \\(\Delta G^{o}\\)
    -   \\(\Delta G\\) represents change in free energy = variable
    -   \\(\Delta G^{o}\\) represents **standard** change in free energy = when reactants and products have concentration of 1 mol/L


### Sign of \\(\Delta G\\) and direction of a reaction {#sign-of-delta-g-and-direction-of-a-reaction}

1.  **Negative \\(\Delta G\\)**: net loss of energy -> **spontaneous**
2.  **Positive \\(\Delta G\\)**: net gain of energy -> **not spontaneous**
3.  **\\(\Delta G = 0\\)**: no net change in energy -> **equilibrium**


### \\(\Delta G\\) of forward and back reactions {#delta-g-of-forward-and-back-reactions}

-   **Equal in magnitude** but **opposite in sign**


### \\(\Delta G\\) and concentration of reactants and products {#delta-g-and-concentration-of-reactants-and-products}

-   At **constant temperature and pressure**: \\(\Delta G = \Delta G^{o} + RTln\frac{[B]}{[A]}\\)
    -   R is gas constant (1.987 cal/mol K)
    -   T is absolute temperature (K)
    -   [A] and [B] are actual concentrations of reactant and product
    -   ln is natural log
-   A reaction with a positive \\(\Delta G^{o}\\) can proceed in the forward direction if the ratio of products to reactants is small enough


## Standard [free energy]({{< relref "free_energy" >}}) change {#standard-free-energy--free-energy-dot-md--change}

-   Equal to free energy change under standard conditions (i.e. concentrations are 1 mol/L)
    -   This is because \\(ln(1) = 0\\), and therefore, above equation becomes \\(\Delta G = \Delta G^{o} + 0\\)


### Relationship between \\(\Delta G^{o} and K\_{eq}\\) {#relationship-between-delta-g-o-and-k-eq}

-   \\(K\_{eq} = \frac{[B]\_{eq}}{[A]\_{eq}}\\)
    -   K<sub>eq</sub> = equilibrium constant
    -   [A]<sub>eq</sub> and [B]<sub>eq</sub> are concentrations at equilibrium
-   If reaction \\(A \rightleftharpoons B\\) is allowed to go to equilibrium at **constant temperature and pressure**:
    -   \\(\Delta G = 0 = \Delta G^{o} + RT ln\frac{[B]\_{eq}}{[A]\_{eq}}\\)
    -   Where [A] and [B] = [A]<sub>eq</sub> and [B]<sub>eq</sub>
    -   Therefore, \\(\Delta G^{o} = -RTlnK\_{eq}\\) which allows some simple predictions:
        -   **If \\(K\_{eq} = 1\\)**, then \\(\Delta G^{o} = 0\\)
        -   **If \\(K\_{eq} > 1\\)**, then \\(\Delta G^{o} < 0\\) -> reaction **proceeds forward**
        -   **If \\(K\_{eq} < 1\\)**, then \\(\Delta G^{o} > 0\\) -> reaction **favors reverse**


## [ATP]({{< relref "atp" >}}) as an Energy Carrier {#atp--atp-dot-md--as-an-energy-carrier}

-   Reactions that have a large positive \\(\Delta G\\) are made possible by coupling endergonic movement of ions with a second, spontaneous process with a **large negative \\(\Delta G\\)**
    -   The exergonic hydrolysis of ATP is an example of one such reaction
-   _Note: in the absence of enzymes, ATP is a stable molecule because its hydrolysis has a high activation energy_


### Common intermediates {#common-intermediates}

-   A _common intermediate_ is when a molecule serves as both the product of one reaction and the substrate of the next reaction
    -   It can carry chemical energy between the two reactions
-   Many coupled reactions use ATP to generate a common intermediate
    -   May involve transfer of phosphate group to another molecule, or reverse


### Energy caried by ATP {#energy-caried-by-atp}

-   Standard free energy of hydrolysis of ATP is approximately -7.3 kcal/mol for each of the two terminal phosphate groups - this is very high


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
