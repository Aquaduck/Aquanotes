+++
title = "Lipoprotein lipase"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Catalyzes lipolysis of [triglycerides]({{< relref "triacylglycerol" >}}) circulating in [chylomicrons]({{< relref "chylomicron" >}})
-   Targets [very low-density lipoprotein]({{< relref "very_low_density_lipoprotein" >}})
-   Carries [apoCII]({{< relref "apocii" >}}) on its surface
-   Induced by [insulin]({{< relref "insulin" >}})


## Backlinks {#backlinks}


### 8 linked references {#8-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Lipoprotein lipase > From Fat Metabolism in Muscle & Adipose Tissue**

    [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**

    ---

<!--list-separator-->

-  [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**
            -   Done through its effects on gene transcription
        -   LPL will cleave the triacylglycerol to **either** _three fatty acids and glycerol_ or _two fatty acids and a monoacylglycerol_
        -   The LPL isozyme of adipose tissue has the **largest K<sub>m</sub> -> [adipose tissue's]({{< relref "adipose_cell" >}}) uptake of [dietary fat]({{< relref "dietary_lipid" >}}) is a last resort**
            -   **Adipose tissue stores fat** -> least likely to utilize it
            -   **LPL is not expressed by the [liver]({{< relref "liver" >}}) or the [brain]({{< relref "brain" >}})** -> NO dietary fat reaches either of those tissues
        -   Tethering the LPL to the cell via a long polysaccharide chain consisting of _heparan sulfates_ -> **significantly improves the likelihood of an [apoCII]({{< relref "apocii" >}}):LPL interaction within the capillary**

<!--list-separator-->

-  **🔖 apoB48 & apoCII > ApoCII > From Amboss**

    Cofactor for [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Pathway of triacylglycerol breakdown**

    [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**

    ---


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Fatty acid metabolism**

    <!--list-separator-->

    -  [Lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})

        -   Expressed on _capillary endothelium_ primarily of muscle, adipose tissue and lactating mammary glands
        -   **Catalyzes lipolysis of triglycerides circulating in [chylomicrons]({{< relref "chylomicron" >}})**
            -   _Chylomicron_ lipoprotein originating in gut that carries dietary fat and cholesterol to their appropriate destinations
        -   Also targets [very low-density lipoprotein]({{< relref "very_low_density_lipoprotein" >}}) (VLDL)
            -   VLDL carries triglycerides and cholesterol from the liver to extrahepatic tissues
        -   Both aforementioned lipoproteins carry [apoCII]({{< relref "apocii" >}}) on their surface -> **facilitates interaction of lipoprotein lipase with lipoprotein**
        -   **Induced by insulin in response to macronutrient digestion and formation of chylomicrons**


#### [Glycerol]({{< relref "glycerol" >}}) {#glycerol--glycerol-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Taken up by the [liver]({{< relref "liver" >}}) following cleavage of [Triacylglycerol]({{< relref "triacylglycerol" >}}) via [LPL]({{< relref "lipoprotein_lipase" >}})

    ---


#### [Explain how chylomicrons deliver dietary fat to extrahepatic tissues.]({{< relref "explain_how_chylomicrons_deliver_dietary_fat_to_extrahepatic_tissues" >}}) {#explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot--explain-how-chylomicrons-deliver-dietary-fat-to-extrahepatic-tissues-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Delivering dietary fat to extrahepatic tissues via chylomicrons**

    [Insulin]({{< relref "insulin" >}}) **increases [[lipoprotein lipase]({{< relref "lipoprotein_lipase" >}})] -> activates fat uptake**

    ---


#### [Apolipoprotein CII]({{< relref "apocii" >}}) {#apolipoprotein-cii--apocii-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    ApoCII anchors the [chylomicron]({{< relref "chylomicron" >}}) to the [LPL]({{< relref "lipoprotein_lipase" >}}) molecules expressed by capillary endothelial cells

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
