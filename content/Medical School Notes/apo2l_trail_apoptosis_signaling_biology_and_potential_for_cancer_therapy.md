+++
title = "Apo2L/TRAIL: apoptosis signaling, biology, and potential for cancer therapy"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Abstract {#abstract}

-   Apo2 ligand or tumor necrosis factor (TNF)-related apoptosis-inducing ligand ([Apo2L/TRAIL]({{< relref "apo2_ligand" >}})) is one of several members of the TNF gene superfamily that induce apoptosis through engagement of death receptors


## Apoptosis signaling by [Apo2L]({{< relref "apo2_ligand" >}})/TRAIL {#apoptosis-signaling-by-apo2l--apo2-ligand-dot-md--trail}

-   Similar to [FasL]({{< relref "fas_ligand" >}}), TRAIL initiates apoptosis upon binding to its cognate death receptors by inducing the recruitment of specific cytoplasmic proteins to the intracellular death domain of the receptor -> formation of [DISC]({{< relref "death_inducing_signaling_complex" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
