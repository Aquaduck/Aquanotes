+++
title = "Myocardial infarction"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   [Infarction]({{< relref "infarction" >}}) of [Heart]({{< relref "heart" >}}) tissue due to prolonged [Ischemia]({{< relref "ischemia" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the causes of myocardial infarction and the vessels most commonly narrowed or occluded.]({{< relref "describe_the_causes_of_myocardial_infarction_and_the_vessels_most_commonly_narrowed_or_occluded" >}}) {#describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot--describe-the-causes-of-myocardial-infarction-and-the-vessels-most-commonly-narrowed-or-occluded-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Atherosclerosis]({{< relref "atherosclerosis" >}}) and [MI]({{< relref "myocardial_infarction" >}})

        -   Atherosclerotic process: lipid deposits in tunica intima (innermost vascular lining) of the [coronary arteries]({{< relref "coronary_artery" >}})
        -   Begins in early adulthood -> slowly results in stenosis of lumina of arteries
        -   Collateral channels connecting one coronary artery with the other expand -> initially permits adequate perfusion during relative inactivity
            -   Not enough when heart needs to perform increased amount of work (e.g. strenous exercise)

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    Severe enough ischemia + lasts long enough -> [myocardial infarction]({{< relref "myocardial_infarction" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
