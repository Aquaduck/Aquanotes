+++
title = "Executioner caspase"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A class of [caspases]({{< relref "caspase" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Initiator caspase]({{< relref "initiator_caspase" >}}) {#initiator-caspase--initiator-caspase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    main function is to activate [executioner caspases]({{< relref "executioner_caspase" >}}) via cleavage of inactive executioner caspase dimers

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Fas > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    Once dimerized + activated in [DISC]({{< relref "death_inducing_signaling_complex" >}}), [initiator caspases]({{< relref "initiator_caspase" >}}) cleave their partners -> activate downstreain [executioner caspases]({{< relref "executioner_caspase" >}}) to induce [apoptosis]({{< relref "apoptosis" >}})

    ---

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Caspase**

    <!--list-separator-->

    -  [Executioner caspases]({{< relref "executioner_caspase" >}})

        -   Normally exist as inactive dimers
            -   Cleaved by initiator caspase -> active site rearranged -> activation


#### [Cytochrome c]({{< relref "cytochrome_c" >}}) {#cytochrome-c--cytochrome-c-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    When released into the cytosol, cytochrome c binds to [Apaf1]({{< relref "apoptotic_protease_activating_factor_1" >}}) -> formation of [Apoptosome]({{< relref "apoptosome" >}}) -> recruits [Caspase-9]({{< relref "caspase_9" >}}) -> activation of downstream [Executioner caspase]({{< relref "executioner_caspase" >}}) -> induction of apoptosis

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
