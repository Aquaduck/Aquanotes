+++
title = "Provide examples of how actin-binding proteins regulate actin filament function."
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cytoskeleton Powerpoint]({{< relref "cytoskeleton_powerpoint" >}}) {#from-cytoskeleton-powerpoint--cytoskeleton-powerpoint-dot-md}


### [ABP]({{< relref "actin_binding_protein" >}}) functions {#abp--actin-binding-protein-dot-md--functions}

1.  ABPs can **regulate actin polymerization**
    -   E.g. Arp 2/3 complex
2.  ABPs can cross-link actin
3.  ABPs can link actin to the membrane


## From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}}) {#from-molecular-biology-of-the-cell-sixth-edition--molecular-biology-of-the-cell-sixth-edition-dot-md}


### Overview of actin accessory proteins (p. 905) {#overview-of-actin-accessory-proteins--p-dot-905}

![](/ox-hugo/_20210914_190416screenshot.png)
Listed above:

-   Formin
-   Arp2/3 complex
-   Thymosin
-   Profilin
-   Tropomodulin
-   Cofilin
-   Gelsolin
-   Capping protein
-   Tropomyosin
-   Fimbrin
-   alpha-actinin
-   filamin
-   spectrin
-   ERM


### Actin-nucleating factors accelerate polymerization and generate branched or straight filaments (p. 906) {#actin-nucleating-factors-accelerate-polymerization-and-generate-branched-or-straight-filaments--p-dot-906}

-   [Arp 2/3 complex]({{< relref "arp_2_3_complex" >}}) nucleates actin filament growth from the **minus end**, allowing rapid elongation at the plus end
    -   It can attach to one side of another actin filament while still remaining bound to the minus end of its nucleated filament -> allows the construction of a treelike web of individual filaments
        ![](/ox-hugo/_20210914_190110screenshot.png)


### Higher-order actin filament arrays influence cellular mechanical properties and signaling {#higher-order-actin-filament-arrays-influence-cellular-mechanical-properties-and-signaling}

-   Two classes of proteins:
    -   _Bundling proteins_: cross-link actin filaments into a parallel array
    -   _Gel-forming proteins_: hold two actin filaments together at a large angle to each other -> creates a looser network


#### Bundling proteins {#bundling-proteins}

{{< figure src="/ox-hugo/_20210914_191413screenshot.png" >}}

<!--list-separator-->

-  [Fimbrin]({{< relref "fimbrin" >}})

    {{< figure src="/ox-hugo/_20210914_191436screenshot.png" >}}

    -   Parallel bundle
        -   Tight packing prevents myosin II from entering bundle

<!--list-separator-->

-  [Alpha-actinin]({{< relref "alpha_actinin" >}})

    {{< figure src="/ox-hugo/_20210914_191459screenshot.png" >}}

    -   Forms a contractile bundle
        -   Loose packing allows myosin II to enter bundle


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-08 Wed] </span></span> > Cytoskeleton > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Provide examples of how actin-binding proteins regulate actin filament function.]({{< relref "provide_examples_of_how_actin_binding_proteins_regulate_actin_filament_function" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
