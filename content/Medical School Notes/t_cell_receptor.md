+++
title = "T-cell receptor"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}}) {#describe-the-structure-of-the-thymus--include-changes-with-age-dot----describe-the-structure-of-the-thymus-include-changes-with-age-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Cortex of the thymus**

    [T-lymphocytes]({{< relref "t_lymphocyte" >}}) are rearranging their [TCR]({{< relref "t_cell_receptor" >}})

    ---

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT > Thymus > Thymus epithelium**

    Epithelial cells in the [Cortex of the thymus]({{< relref "cortex_of_the_thymus" >}}) assist precursor T-cells to rearrange their [TCR]({{< relref "t_cell_receptor" >}}) genes and become naive T-cells

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
