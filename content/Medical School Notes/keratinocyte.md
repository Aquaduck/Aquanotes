+++
title = "Keratinocyte"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Makes large amounts of [keratin]({{< relref "keratin" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Understand how the epidermis grows/regenerates.]({{< relref "understand_how_the_epidermis_grows_regenerates" >}}) {#understand-how-the-epidermis-grows-regenerates-dot--understand-how-the-epidermis-grows-regenerates-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Growth/regeneration**

    As cells of the basal layer divide, some cells stay in place to maintain the basal layer while others are pushed up off the basement membrane by the growth of new [keratinocytes]({{< relref "keratinocyte" >}})

    ---


#### [Name and recognize the 4 layers of the epidermis and be able to relate their histology to their function.]({{< relref "name_and_recognize_the_4_layers_of_the_epidermis_and_be_able_to_relate_their_histology_to_their_function" >}}) {#name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot--name-and-recognize-the-4-layers-of-the-epidermis-and-be-able-to-relate-their-histology-to-their-function-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Epidermis - strata > Stratum granulosum - granular layer**

    [Keratinocytes]({{< relref "keratinocyte" >}}) start to degenerate and make keratin precursors

    ---


#### [Know the three layers of skin and the major components of each layer.]({{< relref "know_the_three_layers_of_skin_and_the_major_components_of_each_layer" >}}) {#know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot--know-the-three-layers-of-skin-and-the-major-components-of-each-layer-dot-md}

<!--list-separator-->

-  **🔖 From Histology - Skin Pre-session Powerpoint > Overview of Basic Skin > 3 layers > Epidermis > Components**

    Contain [keratinocytes]({{< relref "keratinocyte" >}}) - cells that make large amounts of [keratin]({{< relref "keratin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
