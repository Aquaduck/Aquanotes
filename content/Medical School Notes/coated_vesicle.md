+++
title = "Coated vesicle"
author = ["Arif Ahsan"]
date = 2021-08-01T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   An [organelle]({{< relref "organelle" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles**

    <!--list-separator-->

    -  [Coated vesicles]({{< relref "coated_vesicle" >}})

        <!--list-separator-->

        -  Clathrin-coated vesicles

            <!--list-separator-->

            -  Structure

                -   _[Clathrin]({{< relref "clathrin" >}})_: consists of three large and three small polypeptine chains that form a three-legged structure (_triskelion_)
                    -   36 of these associate to form a polyhedral cage-like lattice around the vesicle
                -   _Adaptins_: proteins part of the structure that:
                    -   Recognize and recruit clathrin coat
                    -   Capture cargo receptors containing specific molecules
                    -   Help establish the vesicle curvature
                -   _Dynamin_: forms a ring around the neck of a budding vesicle/pit and aids in pinching it off the parent membrane
                    -   GTP-binding protein

            <!--list-separator-->

            -  Function

                -   Mediate the continuous _constitutive protein transport_ within the cell
                    -   Specific GTP-binding proteins present at each step of vesicle budding and fusion
                    -   _[SNARE]({{< relref "snare" >}})_: proteins that ensure vesicles dock and fuse only with its correct target membrane
                -   Transport proteins from RER -> VTC -> cis Golgi -> across cisternae -> TGN (anterograde transport)
                    -   [COP-II]({{< relref "cop_ii" >}}) transports molecules forward in path mentioned above
                    -   [COP-I]({{< relref "cop_i" >}}) facilitates **retrograde transport** - path above but **in reverse**

        <!--list-separator-->

        -  Caveolin-coated vesicles

            <!--list-separator-->

            -  Structure

                -   Invaginations of the plasma membrane **in endothelial and smooth muscle cells**
                -   Possess a distinct coat formed by the protein _caveolin_

            <!--list-separator-->

            -  Function

                -   Caveolae have been asociated w/ cell signaling and a variety of transport processes (e.g. transcytosis and endocytosis)


### Unlinked references {#unlinked-references}

[Show unlinked references]
