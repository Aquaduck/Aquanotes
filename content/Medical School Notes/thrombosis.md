+++
title = "Thrombosis"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   When unwanted [blood clots]({{< relref "blood_clotting" >}}) block blood vessels


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Disorders of Platelets (p. 813)**

    Thrombocytosis may lead to the formation of unwanted [blood clots]({{< relref "blood_clotting" >}}) ([thrombosis]({{< relref "thrombosis" >}}))

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
