+++
title = "Langman's Medical Embryology"
author = ["T. W. Sadler"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## 13: [Cardiovascular System]({{< relref "cardiovascular_system" >}}) {#13-cardiovascular-system--cardiovascular-system-dot-md}


### Establishment and Patterning of the [Primary Heart Field]({{< relref "primary_heart_field" >}}) {#establishment-and-patterning-of-the-primary-heart-field--primary-heart-field-dot-md}

-   The [vascular system]({{< relref "vascular_system" >}}) appears in the middle of the **third week** when the embryo is no longer able to satisfy its nutritional requirements by diffusion alone
-   _Progenitor heart cells_ lie in the [epiblast]({{< relref "epiblast" >}}), immediately adjacent to the **cranial end** of the [primitive streak]({{< relref "primitive_streak" >}})
    -   From there, they migrate through the streak and into the visceral layer of lateral plate mesoderm -> some form a horseshoe-shaped cluster of cells called the [primary heart field]({{< relref "primary_heart_field" >}}) -> eventually form **portions of the atria** and **the entire [left ventricle]({{< relref "left_ventricle" >}})**


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
