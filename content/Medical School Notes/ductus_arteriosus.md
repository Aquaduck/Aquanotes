+++
title = "Ductus arteriosus"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Connects [pulmonary artery]({{< relref "pulmonary_artery" >}}) and [aorta]({{< relref "aorta" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Ductus arteriosus]({{< relref "ductus_arteriosus" >}}): connects [pulmonary artery]({{< relref "pulmonary_artery" >}}) and [aorta]({{< relref "aorta" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
