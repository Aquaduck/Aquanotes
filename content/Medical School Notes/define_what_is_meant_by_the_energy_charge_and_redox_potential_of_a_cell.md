+++
title = "Define what is meant by the energy charge and redox potential of a cell"
author = ["Arif Ahsan"]
date = 2021-08-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## Energy charge {#energy-charge}

-   Ratio of ATP to AMP/ADP
    -   More ATP -> higher energy charge


## Redox potential {#redox-potential}

-   Ratio of NADH to NAD<sup>+</sup> or FADH<sub>2</sub> to FAD<sup>​+</sup>


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Metabolism: Organ Specialization and Regulation > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Define what is meant by the energy charge and redox potential of a cell]({{< relref "define_what_is_meant_by_the_energy_charge_and_redox_potential_of_a_cell" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
