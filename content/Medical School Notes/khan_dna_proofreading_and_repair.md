+++
title = "Khan - DNA Proofreading and Repair"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "khan", "source"]
draft = false
+++

## [Proofreading]({{< relref "dna_repair" >}}) {#proofreading--dna-repair-dot-md}

-   Done by _[DNA polymerases]({{< relref "dna_polymerase" >}})_ during replication

{{< figure src="/ox-hugo/_20210721_170438screenshot.png" >}}


## [Mismatch repair]({{< relref "dna_repair" >}}) {#mismatch-repair--dna-repair-dot-md}

-   Happens **right after new DNA has been made**
-   Fixes **mis-paired bases**


### Process {#process}

1.  Mismatch detected in newly synthesized DNA
2.  New DNA strand is cut -> mispaired nucleotide + neighbors are removed
3.  Missing patch replaced with correct nucleotides by a _[DNA polymerase]({{< relref "dna_polymerase" >}})_
4.  _DNA ligase_ seals the gap

{{< figure src="/ox-hugo/_20210721_170734screenshot.png" >}}


### How does DNA repair tell which is the correct base in a base pair? {#how-does-dna-repair-tell-which-is-the-correct-base-in-a-base-pair}

-   In bacteria:
    -   _Methylation state_: an old DNA strand will have methyl groups attached to some of its bases, while newly made DNA does not
-   In eukaryotes:
    -   Recognizes nicks (single-stranded breaks) found only in newly synthesized DNA


## [DNA damage repair mechanisms]({{< relref "dna_repair" >}}) {#dna-damage-repair-mechanisms--dna-repair-dot-md}


### Direct reversal {#direct-reversal}

-   Some DNA-damaging chemical reactions can be directly undone by enzymes in a cell


### Excision repair {#excision-repair}


#### Base excision repair {#base-excision-repair}

-   Detects and **removes certain types of damaged bases**
    -   A _Glycosylase_ detects and removes a specific kind of damaged base

{{< figure src="/ox-hugo/_20210721_171334screenshot.png" >}}


#### Nucleotide excision repair {#nucleotide-excision-repair}

-   Detects and corrects damage that **distorts the DNA double helix**
-   The damaged nucleotide(s) are removed along with a surrounding patch of DNA
    -   A _helicase_ cranks open the DNA to form a bubble
    -   DNA-cutting enzymes chop out damaged part of the bubble
    -   A _[DNA polymerase]({{< relref "dna_polymerase" >}})_ replaces the missing DNA
    -   _DNA ligase_ seals the gap in the backbone of the strand

{{< figure src="/ox-hugo/_20210721_171622screenshot.png" >}}


### Double-stranded break repair {#double-stranded-break-repair}

-   Splitting of a chromosome into two
    -   Large segments chromosomes (and contained genes) can be lost if not repaired
-   Two pathways exist to repair double-stranded DNA breaks


#### Non-homologous end joining {#non-homologous-end-joining}

-   Two ends of chromosome are glued back together
-   "Messy"
    -   Involves loss or addition of a few nucleotides at cut site
    -   Better than losing an entire chromosome arm

{{< figure src="/ox-hugo/_20210721_171807screenshot.png" width="400" >}}


#### Homologous recombination {#homologous-recombination}

-   Information from homologous chromosome that matches damaged one is used to repair the break
    -   Two homologous chromosomes come together -> undamaged region of homologue is used as template to replace damaged region
-   "Clean"
    -   Does not usually cause mutations

{{< figure src="/ox-hugo/_20210721_171916screenshot.png" width="400" >}}


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Kaplan Biochemistry Chapter 6]({{< relref "kaplan_biochemistry_chapter_6" >}}) {#kaplan-biochemistry-chapter-6--kaplan-biochemistry-chapter-6-dot-md}

<!--list-separator-->

-  **🔖 6.4 DNA Repair > Thymine Dimers**

    Eliminated by [nucleotide excision repair]({{< relref "khan_dna_proofreading_and_repair" >}})

    ---


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 7. Compare and contrast polymerase proofreading, base excision repair, nucleotide excision repair, mismatch repair, and recombination**

    [Khan - DNA Proofreading and Repair]({{< relref "khan_dna_proofreading_and_repair" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
