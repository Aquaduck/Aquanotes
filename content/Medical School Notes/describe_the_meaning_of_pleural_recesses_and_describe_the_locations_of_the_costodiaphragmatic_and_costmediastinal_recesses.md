+++
title = "Describe the meaning of pleural recesses and describe the locations of the costodiaphragmatic and costmediastinal recesses."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### [Pleural recesses]({{< relref "pleural_recess" >}}) {#pleural-recesses--pleural-recess-dot-md}

-   Several parts of thorax where lung (and consequently the visceral pleura) is **not in contact with the parietal pleura** -> _Pleural reflections_


#### [Costodiaphragmatic recesses]({{< relref "costodiaphragmatic_recess" >}}) {#costodiaphragmatic-recesses--costodiaphragmatic-recess-dot-md}

-   There are two - right and left
-   Spaces between lower border of lung and pleural reflections from chest wall onto diaphgram
-   Potential spaces where flui dmay accumulate
    -   Excess fluid may be drained (pleural tap or thoracentesis) from pleural cavity **without penetrating lungs**
-   **Only in forced inspiration do the lungs expand into these recesses**


#### [Costomediastinal recess]({{< relref "costomediastinal_recess" >}}) {#costomediastinal-recess--costomediastinal-recess-dot-md}

-   On left side between anterior chest wall and pericardial sac
-   heart lies close to anterior thoracic wall and costal pleura is close to the mediastinal pleura


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the meaning of pleural recesses and describe the locations of the costodiaphragmatic and costmediastinal recesses.]({{< relref "describe_the_meaning_of_pleural_recesses_and_describe_the_locations_of_the_costodiaphragmatic_and_costmediastinal_recesses" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
