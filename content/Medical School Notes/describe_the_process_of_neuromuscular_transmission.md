+++
title = "Describe the process of neuromuscular transmission."
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}}) {#from-brs-cell-biology-and-histology--brs-cell-biology-histology-dot-md}


### Conduction of a nerve impulse across a myoneural junction (p. 116) {#conduction-of-a-nerve-impulse-across-a-myoneural-junction--p-dot-116}

1.  The presynaptic membrane is **depolarized**
2.  **Voltage-gated [calcium]({{< relref "calcium" >}}) channels** open -> calcium enters axon terminal
3.  Rise of [cytosolic calcium] -> **release of [acetylcholine]({{< relref "acetylcholine" >}}) into synaptic cleft**
4.  Released ACh binds to receptors of the postsynaptic membrane -> **depolarization of sarcolemma** -> **generation of action potential**
5.  [Acetylcholinesterase]({{< relref "acetylcholinesterase" >}}) degrades acetylcholine -> depolarization signal ends
6.  Choline returns to the axon terminal to be recombined with acetyl-CoA (from mitochondria) via [choline acetyl transferase]({{< relref "choline_acetyl_transferase" >}}) to regenerate acetylcholine -> stored in synaptic vesicles at synaptic bouton
    -   Membranes of the emptied synaptic vesicles are recycled via [clathrin]({{< relref "clathrin" >}})-coated endocytic vesicles

After this, continues at step 6 below (at muscle cell)


## From [Neuromuscular Junction Session Powerpoint]({{< relref "neuromuscular_junction_session_powerpoint" >}}) {#from-neuromuscular-junction-session-powerpoint--neuromuscular-junction-session-powerpoint-dot-md}


### Order of neuromuscular signaling: {#order-of-neuromuscular-signaling}

1.  Action potential invades the nerve terminal
2.  Influx of calcium through voltage-gated calcium channels
3.  ACh released into the synaptic cleft
4.  Diffusion of ACh across cleft
5.  Reaction of ACh with specific receptor sites
6.  Activation of a nonselective cation channel that initiates an outward current (end-place current, EPC)
7.  EPC produces depolarization of the end-plate region (end-plate potential, EPP)
8.  Suprathreshold EPP initiates a muscle action potential
9.  Muscle action potential propagates along muscle fiber surface and down the T-tubules
10. Initiate process of muscle contraction in the muscle fibers


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-13 Mon] </span></span> > Neuromuscular Junction, Motor Units and Recruitment > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the process of neuromuscular transmission.]({{< relref "describe_the_process_of_neuromuscular_transmission" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
