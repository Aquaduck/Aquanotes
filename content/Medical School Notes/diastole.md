+++
title = "Diastole"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Regional and Peripheral Circulation Pre-reading]({{< relref "regional_and_peripheral_circulation_pre_reading" >}}) {#regional-and-peripheral-circulation-pre-reading--regional-and-peripheral-circulation-pre-reading-dot-md}

<!--list-separator-->

-  **🔖 Coronary Blood Flow and the Cardiac Cycle**

    <!--list-separator-->

    -  [Diastole]({{< relref "diastole" >}})

        -   **Most coronary artery inflow occurs during diastole** when the myocardium is relaxed
            -   Emphasized in left ventircle because of greater variation in pressure levels + wall tension than right ventricle
        -   When heart rate increases in exercise -> **duration of diastole decreases much more than duration of systole**
            -   This is normally not an issue because the coronary arteriolar vasodilation results in adequate coronary blood flow


### Unlinked references {#unlinked-references}

[Show unlinked references]
