+++
title = "Cardiac cycle"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The cycle of the [Heart]({{< relref "heart" >}}) to pump [blood]({{< relref "blood" >}}) through the [Cardiovascular system]({{< relref "cardiovascular_system" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [West's zones of the lung]({{< relref "west_s_zones_of_the_lung" >}}) {#west-s-zones-of-the-lung--west-s-zones-of-the-lung-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Relative distributions of [blood]({{< relref "blood" >}}) flow in the [lung]({{< relref "lung" >}}) during the [cardiac cycle]({{< relref "cardiac_cycle" >}})

    ---


#### [Pacemaker cell]({{< relref "pacemaker_cell" >}}) {#pacemaker-cell--pacemaker-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Cells responsible for the automation of the [Cardiac cycle]({{< relref "cardiac_cycle" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
