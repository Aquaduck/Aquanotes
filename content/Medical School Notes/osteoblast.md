+++
title = "Osteoblast"
author = ["Arif Ahsan"]
date = 2021-09-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Lay down new [Osteoid]({{< relref "osteoid" >}}) and calcify it to form new [Bone]({{< relref "bone" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Bone remodeling**

    A wave of [osteoblasts]({{< relref "osteoblast" >}}) follow the [osteoclasts]({{< relref "osteoclast" >}}) -> lay down concentric rings of [Osteoid]({{< relref "osteoid" >}}) -> osteoblasts differentiate into [osteocytes]({{< relref "osteocyte" >}}) -> encased in bone by the next wave of osteoblasts

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Cytologic components of mature bone**

    [Osteoblasts]({{< relref "osteoblast" >}}) - lay down new osteoid and calcify it to form new bone

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
