+++
title = "Subcostal nerve"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the neurovasculature of the intercostal spaces.]({{< relref "describe_the_neurovasculature_of_the_intercostal_spaces" >}}) {#describe-the-neurovasculature-of-the-intercostal-spaces-dot--describe-the-neurovasculature-of-the-intercostal-spaces-dot-md}

<!--list-separator-->

-  **🔖 From Accessing an Intercostal Space > Intercostal Nerves**

    T12 nerve (aka [subcostal nerve]({{< relref "subcostal_nerve" >}})) is not confined between ribs

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
