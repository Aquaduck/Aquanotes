+++
title = "Calmodulin"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Explain the role of calcium in striated muscle contraction and describe its regulation.]({{< relref "explain_the_role_of_calcium_in_striated_muscle_contraction_and_describe_its_regulation" >}}) {#explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot--explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > A sudden rise in cytosolic Ca<sup>2+</sup> concentration initiates muscle contraction (p. 920)**

    Closely related to [calmodulin]({{< relref "calmodulin" >}})

    ---


#### [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}) {#describe-the-inositol-phosphatide-second-messenger-system--describe-the-inositol-phosphatide-second-messenger-system-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Sequence of events**

    IP<sub>3</sub> travels to intracellular membranes of the ER -> calcium release -> calcium binds to [calmodulin]({{< relref "calmodulin" >}})

    ---

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Important members**

    <!--list-separator-->

    -  [Calmodulin]({{< relref "calmodulin" >}})

        -   Activated by calcium release from ER
        -   Functions to activate a series of calmodulin kinases -> **phosphorylation of target proteins\*** on ser/thr residues


### Unlinked references {#unlinked-references}

[Show unlinked references]
