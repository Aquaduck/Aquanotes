+++
title = "Lymph"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Name and describe the pathways of lymphatic drainage (in a lymph node, in the body).]({{< relref "name_and_describe_the_pathways_of_lymphatic_drainage_in_a_lymph_node_in_the_body" >}}) {#name-and-describe-the-pathways-of-lymphatic-drainage--in-a-lymph-node-in-the-body--dot--name-and-describe-the-pathways-of-lymphatic-drainage-in-a-lymph-node-in-the-body-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT**

    <!--list-separator-->

    -  [Lymph]({{< relref "lymph" >}})

        -   Lymph gradually moves **centrally** in [lymphatic channels]({{< relref "lymphatic_channel" >}}) where it flows into the [vascular system]({{< relref "vascular_system" >}}) at the **base of the neck**
            -   Forward movement occurs with pressures created by macroscopic movements of the extremities aided by small luminal valves in lymphatic channels
        -   Lymph is filtered through groups of [lymph nodes]({{< relref "lymph_node" >}}) as it moves centrally
        -   The absence of lymph flow can be detected by swelling of the feet if a person stands motionless for a long period of time
        -   Fluid accumulation, clinically called [edema]({{< relref "edema" >}}), is a frequent syndrome in patients with **cardiovascular, renal, and protein abnormalities**
        -   Daily lymph flow is about 4-5 liters


#### [List the functions of lymphatic drainage.]({{< relref "list_the_functions_of_lymphatic_drainage" >}}) {#list-the-functions-of-lymphatic-drainage-dot--list-the-functions-of-lymphatic-drainage-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT**

    <!--list-separator-->

    -  Functions of [lymphatic]({{< relref "lymph" >}}) flow

        -   Slow movement of lymph -> **filter out any foreign or undesirable material that may have inadvertently reached the interstitial fluid**
            -   This filtering action is performed by [lymph nodes]({{< relref "lymph_node" >}}) - the **major sites of activation of the adaptive immune system**
        -   The arrangement of lymph and [lymph nodes]({{< relref "lymph_node" >}}) is designed to **activate the immune system before antigens and invading organisms reach the blood**
        -   An additional function unrelated to immunity is the **transport of [lipoproteins]({{< relref "lipoprotein" >}}) absored in the G.I. tract**, which **exclusively enter the blood via lymphatics**


#### [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}}) {#list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot--list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot-md}

<!--list-separator-->

-  **🔖 From Lymphatic and Immune Systems PPT**

    <!--list-separator-->

    -  [Lymphatic]({{< relref "lymph" >}}) anatomy

        -   Only a few named lymphatic vessels
        -   All lymph from **below the diaphragm** and **on the left side** passes through the _thoracic duct_ to enter the vascular system at the **bifurcation of the left internal juglar and subclavian veins**
        -   Lymph from **upper right side** enters through the **right lymphatic duct** at the same junction on the right
        -   Lymph from **the GI tract** joins that from the lower extremities in the [Chyle cistern]({{< relref "chyle_cistern" >}}) -> enters thoracic duct

        {{< figure src="/ox-hugo/_20211009_180548screenshot.png" width="600" >}}


### Unlinked references {#unlinked-references}

[Show unlinked references]
