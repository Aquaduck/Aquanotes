+++
title = "Molecular Biology of the Cell, Sixth Edition"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}})

    <!--list-separator-->

    -  Mitosis (p. 978)

        -   An abrupt increase in [M-Cdk]({{< relref "m_cdk" >}}) activity at the [G2/M transition checkpoint]({{< relref "g2_m_transition_checkpoint" >}}) triggers the events of early [mitosis]({{< relref "mitosis" >}})

    <!--list-separator-->

    -  Dephosphorylation Activates [M-Cdk]({{< relref "m_cdk" >}}) at the Onset of [Mitosis]({{< relref "mitosis" >}})

        1.  M-Cdk activation begins with the accumulation of [M-cyclin]({{< relref "cyclin_b" >}})
            -   In **embryonic cell cycles**, the synthesis of M-cyclin is constant throughout, and M-cyclin accumulation results from the high stability of the protein in interphase
            -   In **most other cell types**, M-cyclin synthesis increases during G2 and M, owing primarily to an increase in M-cyclin gene transcription
        2.  [Cdk1]({{< relref "cdk1" >}}) forms a complex with [M-cyclin]({{< relref "cyclin_b" >}}) -> formation of M-Cdk complex as the cell approaches mitosis
        3.  [CAK]({{< relref "cdk_activating_kinase" >}}) phosphorylates M-Cdk at its activating site
            -   [Wee1]({{< relref "wee1" >}}) **also** phosphorylates M-Cdk at two neighboring inhibitory sites
            -   Ultimately, this **holds M-Cdk in the inactive state** -> by the end of G2, the cell contains an abundant stockpile of M-Cdk ready to act
        4.  [Cdc25]({{< relref "cdc25" >}}) is phosphorylated -> **activated** -> **removes the inhibitory phosphates that restrain M-Cdk**
            -   At the same time, [Wee1]({{< relref "wee1" >}}) activity is suppressed -> **ensures that M-Cdk activity increases**
        5.  M-Cdk can interact with its own effectors:
            -   [Cdc25]({{< relref "cdc25" >}}) can be activated (in part) by M-Cdk
            -   [Wee1]({{< relref "wee1" >}}) can be inhibited by M-Cdk
            -   This suggests that **M-Cdk activation in mitosis involves positive feedback loops**

        {{< figure src="/ox-hugo/_20210927_172513screenshot.png" width="700" >}}

    <!--list-separator-->

    -  [Mitogens]({{< relref "mitogen" >}}) Stimulate G<sub>1</sub>-Cdk and G1/S-Cdk activities (p. 1012)

        -   For the vast majority of animal cells, mitogens control the rate of cell division by **acting in the G<sub>1</sub> phase of the cell cycle**
            -   Mitogens allow Cdk activity to occur in G<sub>1</sub> -> entry into a new cell cycle

        <!--list-separator-->

        -  Mitogens interact with cell-surface receptors to trigger multiple intracellular signaling pathways

            <!--list-separator-->

            -  [Ras]({{< relref "ras" >}})

                -   GTPase activity activates MAP kinase cascade -> increased production of transcription regulatory proteins such as [Myc]({{< relref "myc" >}})

            <!--list-separator-->

            -  [Myc]({{< relref "myc" >}})

                -   Myc is thought to promote cell-cycle entry by several mechanisms
                    -   Increase expression of genes encoding G<sub>1</sub> cyclins ([D cyclins]({{< relref "d_cyclin" >}})) -> increase G<sub>1</sub>-Cdk (cyclin D-Cdk4) activity
                    -   Myc also stimulates the transcription of genes that increase cell growth

            <!--list-separator-->

            -  [E2F]({{< relref "e2f" >}}) proteins

                -   Activation of E2F is a key function of G<sub>1</sub>-Cdk complexes
                -   E2F proteins bind to specific DNA sequences in promoters of genes that encode proteins required for S-phase entry
                    -   G<sub>1</sub>/S-cyclins
                    -   S-cyclins
                    -   Proteins involved in DNA synthesis + chromosome duplication
                -   In the absence of mitogenic stimulation, E2F-dependent gene expression is inhibited by members of the [Retinoblastoma protein family]({{< relref "retinoblastoma_protein_family" >}})

            <!--list-separator-->

            -  [Rb family]({{< relref "retinoblastoma_protein_family" >}})

                -   When cells are stimulated to divide by mitogens -> active G<sub>1</sub>/Cdk accumulates -> **phosphorylates** Rb -> reduces Rb binding to E2F
                    -   Allows E2F to activate expression of target genes
                -   Loss of both copies of the Rb gene -> excessive proliferation of some cells in the developing retina
                    -   Suggests that **Rb is particularly important for restraining cell division in this tissue**

            <!--list-separator-->

            -  Overview

                {{< figure src="/ox-hugo/_20210928_161037screenshot.png" caption="Figure 1: Mitogen stimulated cell cycle entry" >}}


#### [Provide examples of how actin-binding proteins regulate actin filament function.]({{< relref "provide_examples_of_how_actin_binding_proteins_regulate_actin_filament_function" >}}) {#provide-examples-of-how-actin-binding-proteins-regulate-actin-filament-function-dot--provide-examples-of-how-actin-binding-proteins-regulate-actin-filament-function-dot-md}

<!--list-separator-->

-  From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}})

    <!--list-separator-->

    -  Overview of actin accessory proteins (p. 905)

        ![](/ox-hugo/_20210914_190416screenshot.png)
        Listed above:

        -   Formin
        -   Arp2/3 complex
        -   Thymosin
        -   Profilin
        -   Tropomodulin
        -   Cofilin
        -   Gelsolin
        -   Capping protein
        -   Tropomyosin
        -   Fimbrin
        -   alpha-actinin
        -   filamin
        -   spectrin
        -   ERM

    <!--list-separator-->

    -  Actin-nucleating factors accelerate polymerization and generate branched or straight filaments (p. 906)

        -   [Arp 2/3 complex]({{< relref "arp_2_3_complex" >}}) nucleates actin filament growth from the **minus end**, allowing rapid elongation at the plus end
            -   It can attach to one side of another actin filament while still remaining bound to the minus end of its nucleated filament -> allows the construction of a treelike web of individual filaments
                ![](/ox-hugo/_20210914_190110screenshot.png)

    <!--list-separator-->

    -  Higher-order actin filament arrays influence cellular mechanical properties and signaling

        -   Two classes of proteins:
            -   _Bundling proteins_: cross-link actin filaments into a parallel array
            -   _Gel-forming proteins_: hold two actin filaments together at a large angle to each other -> creates a looser network

        <!--list-separator-->

        -  Bundling proteins

            {{< figure src="/ox-hugo/_20210914_191413screenshot.png" >}}

            <!--list-separator-->

            -  [Fimbrin]({{< relref "fimbrin" >}})

                {{< figure src="/ox-hugo/_20210914_191436screenshot.png" >}}

                -   Parallel bundle
                    -   Tight packing prevents myosin II from entering bundle

            <!--list-separator-->

            -  [Alpha-actinin]({{< relref "alpha_actinin" >}})

                {{< figure src="/ox-hugo/_20210914_191459screenshot.png" >}}

                -   Forms a contractile bundle
                    -   Loose packing allows myosin II to enter bundle


#### [Microtubule associated protein]({{< relref "microtubule_associated_protein" >}}) {#microtubule-associated-protein--microtubule-associated-protein-dot-md}

<!--list-separator-->

-  From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}})

    <!--list-separator-->

    -  Glossary (p. G:20)

        -   Any protein that binds to [microtubules]({{< relref "microtubule" >}}) and modifies their properties


#### [MAP2]({{< relref "map2" >}}) {#map2--map2-dot-md}

<!--list-separator-->

-  From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}})

    <!--list-separator-->

    -  Microtubule-binding proteins modulate filament dynamics and organization

        -   Due to its long projecting domain, MAP2 **forms bundles of stable microtubules** that are kept widely spaced and is thus confined to neuron cell bodies and its [dendrites]({{< relref "dendrite" >}})


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}})

    <!--list-separator-->

    -  [Caspase]({{< relref "caspase" >}})

        <!--list-separator-->

        -  [Apoptosis]({{< relref "apoptosis" >}}) depends on an intracellular proteolytic cascade that is mediated by caspases (p.1022)

            -   Irreversible apoptosis is triggered by _Caspase_ proteases
                -   A family of specialized intracellular proteases which cleave specific sequences in numerous proteins inside the cell leading to cell death
                -   Have a **cysteine** at their active site and cleave target proteins at specific **aspartic acids**
                -   Synthesized in the cell as inactive precursors -> **activated only during apoptosis**
                -   Two major classes: _initiator_ caspases and _executioner_ caspases

        <!--list-separator-->

        -  [Initiator caspases]({{< relref "initiator_caspase" >}})

            -   Begin apoptosis, main function being to **activate executioner caspases**
            -   Normally exist as inactive, soluble monomers in the cytosol
            -   An apoptotic signal triggers the assembly of large protein platofrms -> formation of large complexes of initiator caspases
                -   Pairs of caspases associate to form dimers -> **protease activation**
                    -   Each caspase in the dimer cleaves its partner at a specific site in the protease domain -> stabilizes active complex and required for proper function of the enzyme in the cell

        <!--list-separator-->

        -  [Executioner caspases]({{< relref "executioner_caspase" >}})

            -   Normally exist as inactive dimers
                -   Cleaved by initiator caspase -> active site rearranged -> activation

        <!--list-separator-->

        -  Targets of caspase

            -   Nuclear [lamins]({{< relref "lamin" >}}) -> irreversible breakdown of nuclear lamina
            -   Cleavage of protein that frees [endonuclease]({{< relref "endonuclease" >}}) -> cuts up DNA in cell nucleus
            -   Components of the cytoskeleton and cell-cell adhesion proteins -> allows cell to round up and detach from neighbors -> easier for neighboring cells to engulf it

    <!--list-separator-->

    -  [Death receptors]({{< relref "death_receptor" >}})

        <!--list-separator-->

        -  Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024)

            -   Death receptors are transmembrane proteins containing:
                -   An extracellular ligand-binding domain
                -   A single transmembrane domain
                -   An intracellular [death domain]({{< relref "death_domain" >}}) -> required for receptor to activate [apoptosis]({{< relref "apoptosis" >}})
            -   Death receptors are homotrimers and belong to the [tumor necrosis factor]({{< relref "tumor_necrosis_factor_receptor" >}}) (TNF) **receptor** family
                -   The ligands that activate death receptors are also homotrimers and are part of the [TNF]({{< relref "tumor_necrosis_factor" >}}) family of **signal proteins**

    <!--list-separator-->

    -  [Fas]({{< relref "fas" >}})

        <!--list-separator-->

        -  Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024)

            -   A well-understood example of how death receptors trigger the extrinsic pathway of apoptosis is the activation of Fas on the surface of a target cell by Fas ligand on the surface of a killer (cytotoxic) lymphocyte
                -   Fas activated by [Fas ligand]({{< relref "fas_ligand" >}}) -> death domains on Fas death receptors bind intracellular adaptor proteins -> bind [initiator caspases]({{< relref "initiator_caspase" >}}) (primarily [caspase-8]({{< relref "caspase_8" >}})) -> formation of a [death-inducing signaling complex]({{< relref "death_inducing_signaling_complex" >}}) (DISC)
                -   Once dimerized + activated in [DISC]({{< relref "death_inducing_signaling_complex" >}}), [initiator caspases]({{< relref "initiator_caspase" >}}) cleave their partners -> activate downstreain [executioner caspases]({{< relref "executioner_caspase" >}}) to induce [apoptosis]({{< relref "apoptosis" >}})

            {{< figure src="/ox-hugo/_20210926_143723screenshot.png" >}}

    <!--list-separator-->

    -  [FLIP]({{< relref "flice_inhibitor_protein" >}})

        <!--list-separator-->

        -  Cell-surface death receptors activate the [extrinsic pathway]({{< relref "extrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1024)

            -   FLIP is an inhibitory protein that **restrains the extrinsic pathway** -> inhibits inappropriate activation of apoptosis
            -   FLIP resembles an initiator caspase **but has no protease activity** because it lacks a key cysteine in its active site
            -   FLIP dimerizes with [caspase-8]({{< relref "caspase_8" >}}) in the [DISC]({{< relref "death_inducing_signaling_complex" >}})
                -   While caspase-8 appears to be active, it is not cleaved at the site required for its stable activation -> blockage of apoptotic signal

    <!--list-separator-->

    -  [Cytochrome c]({{< relref "cytochrome_c" >}}) and Apaf1

        <!--list-separator-->

        -  The [intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}}) depends on mitochondria (p. 1025)

            -   Cytochrome c is a water-soluble component of the mitochondrial electron transport chain
            -   When released into the cytosol, cytochrome c binds to [Apaf1]({{< relref "apoptotic_protease_activating_factor_1" >}}) -> Apaf1 oligomerizes into a wheel-like heptamer called an [apoptosome]({{< relref "apoptosome" >}}) -> recruits [caspase-9]({{< relref "caspase_9" >}}) -> activation of downstream executioner caspases -> induction of apoptosis

    <!--list-separator-->

    -  [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}})

        <!--list-separator-->

        -  Bcl2 proteins regulate the [intrinsic pathway]({{< relref "intrinsic_pathway_of_apoptosis" >}}) of apoptosis (p. 1026)

            -   Bcl2 proteins are a major class of intracellular regulators of the intrinsic pathway of apoptosis
                -   Controls the release of [cytochrome c]({{< relref "cytochrome_c" >}}) and other intermembrane mitochondrial proteins into the cytosol
                -   Some are _pro-apoptotic_ -> promote apoptosis by enhancing release
                -   Some are _anti-apoptotic_ -> inhibit apoptosis by blocking release
            -   Pro- and anti-apoptotic Bcl2 proteins can bind to each other in various combinations to form heterodimers -> can **inhibit each other's function**
                -   Largely determines whether a mammalian cell lives or dies by the intrinsic pathway of apoptosis

            <!--list-separator-->

            -  [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}})

                -   **Pro-apoptotic** effector Bcl2 family proteins
                -   **At least one of these is required** for the intrinsic pathway of apoptosis to operate

            <!--list-separator-->

            -  [Bcl2]({{< relref "b_cell_lymphoma_2" >}}) and [BclX<sub>L</sub>]({{< relref "bclx_l" >}})

                -   **Anti-apoptotic**
                -   Bind to pro-apoptotic Bcl2 family proteins -> inhibit apoptosis

            <!--list-separator-->

            -  [BH3-only proteins]({{< relref "bh3_only_protein" >}})

                -   Cell either produces or activates BH3-only proteins in response to an apoptotic stimulus
                -   Provide the crucial link between apoptotic stimuli and the intrinsic pathway of apoptosis
                    -   Different stimuli activates different BH3-only proteins
                -   Thought to promote apoptotis mainly by inhibiting anti-apoptotic Bcl2 family proteins
                -   BH3 domain binds to a long hydrophobic groove on anti-apoptotic Bcl2 family proteins -> neutralizes their activity
                    -   Enables the aggregation of [Bax]({{< relref "bax" >}}) and [Bak]({{< relref "bak" >}}) on the surface of mitochondria -> triggers release of intermembrane mitochondrial proteins -> induction of apoptosis
                -   Some bind to Bax and Bak directly to stimulate their aggregation

                <!--list-separator-->

                -  [Bid]({{< relref "bid" >}})

                    -   Normally inactive
                    -   When [death receptors]({{< relref "death_receptor" >}}) activate the [Extrinsic pathway of apoptosis]({{< relref "extrinsic_pathway_of_apoptosis" >}}), [Caspase-8]({{< relref "caspase_8" >}}) cleaves Bid -> activates Bid -> translocates to outer mitochondrial membrane -> inhibits anti-apoptotic Bcl2 family proteins -> **amplification of death signal**

    <!--list-separator-->

    -  [IAPs]({{< relref "inhibitor_of_apoptosis" >}})

        -   All IAPs have one or more [baculovirus IAP repeat]({{< relref "baculovirus_iap_repeat" >}}) (BIR) domains
            -   Enable them to bind to and inhibit activated caspases
        -   Some IAPs also polyubiquitylate [caspases]({{< relref "caspase" >}}) -> marking them for destruction by proteasomes

    <!--list-separator-->

    -  [SMAC]({{< relref "second_mitochondria_derived_activator_of_caspases" >}})


#### [Explain the role of calcium in striated muscle contraction and describe its regulation.]({{< relref "explain_the_role_of_calcium_in_striated_muscle_contraction_and_describe_its_regulation" >}}) {#explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot--explain-the-role-of-calcium-in-striated-muscle-contraction-and-describe-its-regulation-dot-md}

<!--list-separator-->

-  From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}})

    <!--list-separator-->

    -  A sudden rise in cytosolic Ca<sup>2+</sup> concentration initiates muscle contraction (p. 920)

        -   Incoming AP activates a calcium channel in the T-tubule membrane -> calcium influx -> opening of calcium release channels in [sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}})
            -   Calcium is then rapidly pumped back into SR by [calcium atpase]({{< relref "calcium_atpase" >}}), an abundant ATP-dependent calcium pump -> [myofibrils]({{< relref "myofibril" >}}) relax
        -   Calcium dependence of vertebrate skeletal muscle contraction due entirely to a set of specialized accessory proteins closely associated with [actin]({{< relref "actin" >}}) thin filaments:
            -   Muscle form of [tropomyosin]({{< relref "tropomyosin" >}})
            -   [Troponin]({{< relref "troponin" >}})
                -   A complex of three polypeptides:
                    -   Troponin T (Tropomyosin-binding)
                    -   Troponin I (Inhibitory)
                    -   Troponin C (Calcium binding)
                        -   Closely related to [calmodulin]({{< relref "calmodulin" >}})
                -   Troponin I binds to actin as well as to troponin T
                    -   In resting muscle, this complex pulls ropomyosin out of its normal binding groove into a position along the [actin]({{< relref "actin" >}}) filament that **interferes with the binding of myosin heads** -> **no force generation**
                -   When calcium levels rise, troponin C binds to calcium -> causes troponin I to release actin
                    -   Allows tropomyosin to slip back into normal position -> allows myosin head binding to actin


#### [Define apoptosis and necrosis]({{< relref "define_apoptosis_and_necrosis" >}}) {#define-apoptosis-and-necrosis--define-apoptosis-and-necrosis-dot-md}

<!--list-separator-->

-  From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}})

    <!--list-separator-->

    -  Intro (p. 1021)

        -   [Apoptosis]({{< relref "apoptosis" >}}) is programmed cell death, whereas [necrosis]({{< relref "necrosis" >}}) occurs in response to suboptimal conditions such as truama or inadequate blood supply


### Unlinked references {#unlinked-references}

[Show unlinked references]
