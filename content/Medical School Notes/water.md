+++
title = "Water"
author = ["Arif Ahsan"]
date = 2021-07-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

Water is made up of two hydrogen atoms and one oxygen atom. It is an essential component of all life on earth.

{{< figure src="/ox-hugo/_20210712_213202screenshot.png" width="200" >}}


## Backlinks {#backlinks}


### 11 linked references {#11-linked-references}


#### [Loop of Henle]({{< relref "loop_of_henle" >}}) {#loop-of-henle--loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Component of the [Nephron]({{< relref "nephron" >}}) responsible for reabsorption of [water]({{< relref "water" >}}) and [sodium]({{< relref "sodium" >}})

    ---


#### [Khan - Water and life]({{< relref "khan_water_and_life" >}}) {#khan-water-and-life--khan-water-and-life-dot-md}

<!--list-separator-->

-  **🔖 Unique properties of water**

    [Water]({{< relref "water" >}}) is less dense as a solid than as a liquid

    ---

<!--list-separator-->

-  **🔖 Unique properties of water**

    [Water]({{< relref "water" >}}) has cohesive and adhesive properties

    ---

<!--list-separator-->

-  **🔖 Unique properties of water**

    [Water]({{< relref "water" >}}) has high heat of vaporization

    ---

<!--list-separator-->

-  **🔖 Unique properties of water**

    [Water]({{< relref "water" >}}) has a high heat capacity

    ---

<!--list-separator-->

-  **🔖 Unique properties of water**

    [Water]({{< relref "water" >}}) is an excellent solvent

    ---

<!--list-separator-->

-  **🔖 Unique properties of water**

    [Water]({{< relref "water" >}}) is polar

    ---


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 General Chemistry > 1. Relate water's chemical structure to its unique properties, and explain how these properties are vital to living systems**

    [Water]({{< relref "water" >}})

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Renal functions (p. 10)**

    <!--list-separator-->

    -  Regulation of [water]({{< relref "water" >}}) and electrolyte balance


#### [Explain how the glomerular filtration barrier works and affects filtrate]({{< relref "explain_how_the_glomerular_filtration_barrier_works_and_affects_filtrate" >}}) {#explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate--explain-how-the-glomerular-filtration-barrier-works-and-affects-filtrate-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Basic renal excretory processes (p. 21)**

    _[Filtration]({{< relref "filtration" >}})_: the process by which [Water]({{< relref "water" >}}) and solutes in the [Blood]({{< relref "blood" >}}) leave the vascular system through the [glomerular filtration barrier]({{< relref "glomerular_filtration_barrier" >}}) and enter [Bowman's space]({{< relref "bowman_s_space" >}})

    ---


#### [Descending thin limb of the loop of Henle]({{< relref "descending_thin_limb_of_the_loop_of_henle" >}}) {#descending-thin-limb-of-the-loop-of-henle--descending-thin-limb-of-the-loop-of-henle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Responsible for [water]({{< relref "water" >}}) reabsorption

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
