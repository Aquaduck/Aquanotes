+++
title = "Pulmonary valve"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Right ventricular outflow tract obstruction]({{< relref "right_ventricular_outflow_tract_obstruction" >}}) {#right-ventricular-outflow-tract-obstruction--right-ventricular-outflow-tract-obstruction-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inability of blood to pass out of the [right ventricle]({{< relref "right_ventricle" >}}) through the [pulmonic valve]({{< relref "pulmonary_valve" >}}).

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Heart sound is split during [inspiration]({{< relref "inspiration" >}}) because the [pulmonic valve]({{< relref "pulmonary_valve" >}}) closing is slightly later than [aortic valve]({{< relref "aortic_valve" >}}) closing

    ---


#### [Describe the anatomical locations for auscultation of the heart valves.]({{< relref "describe_the_anatomical_locations_for_auscultation_of_the_heart_valves" >}}) {#describe-the-anatomical-locations-for-auscultation-of-the-heart-valves-dot--describe-the-anatomical-locations-for-auscultation-of-the-heart-valves-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    [Pulmonary valve]({{< relref "pulmonary_valve" >}}): Second intercostal space just to the left of the sternum

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
