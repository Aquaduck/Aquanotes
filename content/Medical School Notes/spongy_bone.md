+++
title = "Spongy bone"
author = ["Arif Ahsan"]
date = 2021-09-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Porous [bone]({{< relref "bone" >}}) composed of thin [trabeculae]({{< relref "trabeculae" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Name the gross, microscopic, cellular, and major molecular components of bone.]({{< relref "name_the_gross_microscopic_cellular_and_major_molecular_components_of_bone" >}}) {#name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot--name-the-gross-microscopic-cellular-and-major-molecular-components-of-bone-dot-md}

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Histologic components of mature bone**

    Lamellae of bone are also laid down on the [Trabeculae]({{< relref "trabeculae" >}}) of [Cancellous bone]({{< relref "spongy_bone" >}})

    ---

<!--list-separator-->

-  **🔖 From Histology: Bone Lab Pre-work > Organization of bones**

    Porous bone is called _[spongy]({{< relref "spongy_bone" >}})_ or _cancellous_ bone

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
