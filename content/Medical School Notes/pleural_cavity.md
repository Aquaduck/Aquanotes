+++
title = "Pleural cavity"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Pleura of the lung (p. 1057)**

    [Pleural cavity]({{< relref "pleural_cavity" >}}): space between the visceral and parietal layers

    ---


#### [Apply principles of lung mechanics to understand the cause and consequences of obstructive lung disease.]({{< relref "apply_principles_of_lung_mechanics_to_understand_the_cause_and_consequences_of_obstructive_lung_disease" >}}) {#apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot--apply-principles-of-lung-mechanics-to-understand-the-cause-and-consequences-of-obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Restrictive lung disease > Etiology**

    Diseases of [pleura]({{< relref "pleura" >}}) and [pleural cavity]({{< relref "pleural_cavity" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
