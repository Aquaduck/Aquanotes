+++
title = "Define the enzymes involved in synthesis and degradation of cyclic-AMP."
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Signalling II - Intracellular Signal Pathways]({{< relref "cell_signalling_ii_intracellular_signal_pathways" >}}) {#from-cell-signalling-ii-intracellular-signal-pathways--cell-signalling-ii-intracellular-signal-pathways-dot-md}


### Adenylyl cyclase/[Adenylate cyclase]({{< relref "adenylate_cyclase" >}}) {#adenylyl-cyclase-adenylate-cyclase--adenylate-cyclase-dot-md}

-   converts ATP -> cAMP (synthesis)


### [Cyclic AMP phosphodiesterase]({{< relref "camp_phosphodiesterase" >}}) {#cyclic-amp-phosphodiesterase--camp-phosphodiesterase-dot-md}

-   converts cAMP -> 5'AMP (degradation)


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-08-30 Mon] </span></span> > Cell Signalling II > Session**

    <!--list-separator-->

    - <span class="org-todo todo ___">[-]</span>  [Define the enzymes involved in synthesis and degradation of cyclic-AMP.]({{< relref "define_the_enzymes_involved_in_synthesis_and_degradation_of_cyclic_amp" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
