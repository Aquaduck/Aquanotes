+++
title = "Describe lymphatic flow in the breast."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### [Breast]({{< relref "breast" >}}) lymphatics {#breast--breast-dot-md--lymphatics}

-   The vast majority of lymph (75%) from the breast **is directed towards** _axillary nodes_
    -   Remaining lymph (especially from medial quadrants) directed to _parasternal nodes_
-   Some drainage from the inferior region flows along the anterior abdominal wall to the inguinal nodes in the groin


### [Breast]({{< relref "breast" >}}) quadrants {#breast--breast-dot-md--quadrants}

{{< figure src="/ox-hugo/_20211028_153603screenshot.png" caption="Figure 1: Four quadrants of the breast" >}}


### Metastasis of [Breast Cancer]({{< relref "breast_cancer" >}}) {#metastasis-of-breast-cancer--breast-cancer-dot-md}

-   Carcinomas of the brast are malignant tumors
    -   Usually adenocarcinomas (glandular cancer) arising from epithelial cells of lactiferous ducts in mamary gland lobules
-   ~60% of malignant breast tumors arise from _superior lateral quadrant_ -> tumor cells that break away picked up by lymph and travel to lymph nodes in _axillary region_
-   Breast cancer typically spreads form breast by lymphatic vessels
    -   Most common site is _axillary node_
-   Surgical removal of axillary lymph nodes can disrupt lymph flow from upper limb -> lymphedema in affected limb


### Skin changes in [breast cancer]({{< relref "breast_cancer" >}}) {#skin-changes-in-breast-cancer--breast-cancer-dot-md}

-   Lymphedema
-   "Pitting" or "dimpling" along skin of breast
-   Presence of lump under skin
-   Rash-like color changes
-   Mammography is primarily used for screening
    -   In conventional mammography, denser structures appear light


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe common patterns of cancer metastasis in the venous and lymphatic systems and the modality used to diagnose the presence of breast carcinoma.]({{< relref "describe_lymphatic_flow_in_the_breast" >}})

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe lymphatic flow in the breast.]({{< relref "describe_lymphatic_flow_in_the_breast" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
