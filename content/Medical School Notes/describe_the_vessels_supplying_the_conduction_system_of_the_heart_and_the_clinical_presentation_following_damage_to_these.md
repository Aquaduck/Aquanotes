+++
title = "Describe the vessels supplying the conduction system of the heart and the clinical presentation following damage to these."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}


### Coronary occlusion and conducting system of [Heart]({{< relref "heart" >}}) {#coronary-occlusion-and-conducting-system-of-heart--heart-dot-md}

-   Damage to conducting system of heart -> disturbances of [cardiac muscle]({{< relref "cardiac_muscle" >}}) contraction
-   LAD gives rise to septal branches supplying AV bundle in most people
-   Branches of RCA supply both SA and AV nodes (60% of the time, 40% of people have supply from LCA instead) -> conducting system affected by occlusion -> heart block
    -   Ventricles will begin to contract independently at their own rate of 25-30 times per minute (much slower than normal)
    -   Atria continue to contract at normal rate if SA node is spared, but impulse from SA does not reach ventricles
-   Damage to one bundle branch -> bundle-branch block
    -   Excitation passes along unaffected branch and causes a normally timed systole **of that ventricle only**
    -   Impulse then spreads to other ventricle via myogenic (muscle propagated) conduction -> **late asynchronous contraction**
    -   In this case, a cardiac pacemaker may be implanted to increase the ventricular rate of contraction to 70-80 per minute


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the vessels supplying the conduction system of the heart and the clinical presentation following damage to these.]({{< relref "describe_the_vessels_supplying_the_conduction_system_of_the_heart_and_the_clinical_presentation_following_damage_to_these" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
