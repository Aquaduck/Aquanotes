+++
title = "Cyanide"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Carotid & Aortic bodies (p. 671) > Carotid bodies**

    [Cyanide]({{< relref "cyanide" >}}) is a powerful stimulus <- prevents oxygen utilization at the tissue level

    ---

<!--list-separator-->

-  **🔖 Carotid & Aortic bodies (p. 671)**

    Have dense-core granules containing catecholamines that are released upon excitation via exposure to hypoxia and [cyanide]({{< relref "cyanide" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
