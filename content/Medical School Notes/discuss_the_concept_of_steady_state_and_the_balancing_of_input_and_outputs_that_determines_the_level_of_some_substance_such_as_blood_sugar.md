+++
title = "Discuss the concept of steady state and the balancing of input and outputs that determines the level of some substance such as blood sugar."
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Homeostasis PPT]({{< relref "homeostasis_ppt" >}}) {#from-homeostasis-ppt--homeostasis-ppt-dot-md}


### Steady state {#steady-state}

-   _Steady state_ = **Input equals output**
-   [Homeostasis]({{< relref "homeostasis" >}}) means the maintenance of **the right steady state** in the face of changing inputs and outputs


### Balance is everything {#balance-is-everything}

-   Nothing is ever completely on or off
-   Glucose is an example of the steady state issue
    -   Glucose is constantly being added and removed from the blood
    -   Poorly controlled [Diabetes mellitus]({{< relref "diabetes_mellitus" >}}) illustrates **the wrong Steady State**

{{< figure src="/ox-hugo/_20211009_143608screenshot.png" width="700" >}}

-   Examples of **too much input**
    -   Diabetes mellitus
    -   High sodium diet
    -   Overeating
-   Examples of **"Hair in the drain"**
    -   Smoking
    -   Kidney disease
    -   Type II Diabetes Mellitus


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon] </span></span> > Homeostasis > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Discuss the concept of steady state and the balancing of input and outputs that determines the level of some substance such as blood sugar.]({{< relref "discuss_the_concept_of_steady_state_and_the_balancing_of_input_and_outputs_that_determines_the_level_of_some_substance_such_as_blood_sugar" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
