+++
title = "Aldehyde"
author = ["Arif Ahsan"]
date = 2021-07-14T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## [Overview]({{< relref "functional_group" >}}) {#overview--functional-group-dot-md}


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Lumen - Functional Group Names, Properties, and Reactions]({{< relref "lumen_functional_group_names_properties_and_reactions" >}}) {#lumen-functional-group-names-properties-and-reactions--lumen-functional-group-names-properties-and-reactions-dot-md}

<!--list-separator-->

-  **🔖 Functional Groups > Aldehydes and Ketones**

    <!--list-separator-->

    -  Similarities between [aldehydes]({{< relref "aldehyde" >}}) and [ketones]({{< relref "ketone" >}})

        -   Both exist in equilibrium with their enol forms:

        <_20210714_215454screenshot.png>

        -   Keto form predominates at equilibrium
        -   Deprotonated enolate is a **strong nucleophile**

<!--list-separator-->

-  **🔖 Functional Groups > Aldehydes and Ketones**

    <!--list-separator-->

    -  [Aldehydes]({{< relref "aldehyde" >}})

        -   sp2 hybridized -> **trigonal planar geometry**


#### [Kaplan Organic Chemistry Chapter 1]({{< relref "kaplan_organic_chemistry_chapter_1" >}}) {#kaplan-organic-chemistry-chapter-1--kaplan-organic-chemistry-chapter-1-dot-md}

<!--list-separator-->

-  **🔖 1.3 Aldehydes and Ketones**

    <!--list-separator-->

    -  [Aldehydes]({{< relref "aldehyde" >}})

        -   Carbonyl group **at the end of a carbon chain**
        -   Usually attached to carbon 1
        -   Aldehydes are named by replacing the \\(-e\\) of the parent alkane with the suffix \\(-al\\)

        <_20210714_193911screenshot.png>


### Unlinked references {#unlinked-references}

[Show unlinked references]
