+++
title = "Embryology - Thorax Part 1: Thoracic Cavity & Lung Development PPT"
author = ["Amelia Sybenga", "DO"]
date = 2021-10-23T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Name the stages of lung development, their approximate times of development, and explain the major changes in each stage.]({{< relref "name_the_stages_of_lung_development_their_approximate_times_of_development_and_explain_the_major_changes_in_each_stage" >}}) {#name-the-stages-of-lung-development-their-approximate-times-of-development-and-explain-the-major-changes-in-each-stage-dot--name-the-stages-of-lung-development-their-approximate-times-of-development-and-explain-the-major-changes-in-each-stage-dot-md}

<!--list-separator-->

-  From [Embryology - Thorax Part 1: Thoracic Cavity & Lung Development PPT]({{< relref "embryology_thorax_part_1_thoracic_cavity_lung_development_ppt" >}})

    <!--list-separator-->

    -  Phases of [lung]({{< relref "lung" >}}) growth:

        {{< figure src="/ox-hugo/_20211023_135716screenshot.png" width="700" >}}

        <!--list-separator-->

        -  Embryonic period (26 days - 6 weeks):

            -   Lung bud -> 2 diverticula, bronchial buds -> segmental (right + left mainstem) bronchi
            -   _Potential airways_: epithelium surrounded by mesenchyme w/ widely separated capillaries
            -   Pulmonary arteries from 6th aortic arch ~ end of 6 weeks

        <!--list-separator-->

        -  Pseudoglandular period (6 - 16 weeks):

            -   Development of **conducting airway** to **terminal bronchioles & divisions of lobules & acini**
            -   Lymphatics into lungs
            -   Cartilaginous rings and smooth muscle
            -   Pseudostratified columnar epithelium, cilia & goblet cells
            -   Tubular submucosal glands

        <!--list-separator-->

        -  Canalicular period (17 - 28 weeks):

            -   Subdivision of respiratory bronchioles & multiple irregular alveolar ducts
            -   **Appearance of [type II alveolar cells]({{< relref "type_2_alveolar_cell" >}})** -> **[surfactant]({{< relref "surfactant" >}}) synthesis**
            -   [Type I alveolar cells]({{< relref "type_1_alveolar_cell" >}}) differentiate from Type II
                -   Capillaries proliferate under Type I cells
            -   Cartilage extension to distal bronchi
            -   Submucosal tubules -> mucin containing acini
            -   Bronchioles straighten

        <!--list-separator-->

        -  Saccular ("Terminal sac") period (28 - 32 weeks):

            -   Distal air-spaces divide into smaller units
            -   Interstitial tissue shrinks
            -   Fibroblasts differentiate -> production of **collagen, ECM & elastin**
            -   Capillary network proliferates (double capillary network)
            -   **Alveoli & early gas exchange begins @ 32 weeks**

        <!--list-separator-->

        -  Alveolar period (32-36 weeks to ~3 years post-natal):

            -   Alveoli -> flask shape
            -   Lymphatics extend through interlobular septae to pleura
            -   **Double -> single capillary bed** -> **increased oxygen exchange**
            -   **Surge of maternal glucocorticoids** ->
                -   Increased fetal plasma cortisol -> increased secretion of [surfactant]({{< relref "surfactant" >}}) (progressive increase from 32 weeks)
            -   [Type II pneumocytes]({{< relref "type_2_alveolar_cell" >}}) increase -> increased [surfactant]({{< relref "surfactant" >}}) production
            -   **Lungs become more inflated + plastic**


### Unlinked references {#unlinked-references}

[Show unlinked references]
