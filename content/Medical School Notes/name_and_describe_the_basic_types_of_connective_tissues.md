+++
title = "Name and describe the basic types of connective tissues"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}}) {#from-brs-cell-biology-and-histology--brs-cell-biology-histology-dot-md}


### Classification of [connective tissue]({{< relref "connective_tissue" >}}) (p. 86) {#classification-of-connective-tissue--connective-tissue-dot-md----p-dot-86}


#### Connective tissue proper {#connective-tissue-proper}

<!--list-separator-->

-  Loose CT

    -   Fewer fibers but more cells than dense CT
    -   Well vascularized, flexible, and not resistance to stress
    -   More abundant than dense CT
    -   Fills in spaces just deep to the skin

<!--list-separator-->

-  Dense CT

    -   More fibers but fewer cells than loose CT

    <!--list-separator-->

    -  Dense irregular CT

        -   Most common
        -   Contains fiber bundles that have **no definite orientation**
        -   Characteristic of the dermis and capsules of many organs

    <!--list-separator-->

    -  Dense regular CT

        -   Contains fiber bundles and attenuated fibroblasts arranged in a **uniform parallel fashion**
        -   **Present only in tendons and ligaments**
        -   May be collagenous or elastic


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-07 Tue] </span></span> > Histology of Extracellular Matrix and Connective Tissue/Cartilage > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Name and describe the basic types of connective tissues]({{< relref "name_and_describe_the_basic_types_of_connective_tissues" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
