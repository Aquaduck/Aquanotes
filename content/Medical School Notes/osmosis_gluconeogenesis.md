+++
title = "Osmosis - Gluconeogenesis"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "osmosis", "videos", "source"]
draft = false
+++

## [Summary]({{< relref "gluconeogenesis" >}}) {#summary--gluconeogenesis-dot-md}

-   Essentially the reverse of [glycolysis]({{< relref "glycolysis" >}}) with _3 irreversible reactions_ bypassed with unique enzymes
    -   Pyruvate kinase bypassed by _pyruvate carboxylase_, _malate dehydrogenase_, and _PEPCK_
    -   PFK1 bypassed by _fructose-1,6-bisphosphatase_
    -   Hexokinase bypassed by _glucose-6-phosphate_
-   Production of glucose from pyruvate to help maintain steady blood glucose levels during prolonged fasts


## [Notes]({{< relref "gluconeogenesis" >}}) {#notes--gluconeogenesis-dot-md}

-   Body maintains glucose levels during fasting using **gluconeogenesis**
-   Primarily takes place in **liver**, but can also happen in **epithelial cells of kidney and intestines**
    -   Specifically in **cytoplasm, mitochondria, and ER** of cells found in these tissues
-   Extra glucose stored in liver cells as _glycogen_ - glucose molecules strung together
-   **RBCs and brain can only metabolize glucose**
-   Liver gets energy **exclusively from burning fat**
    -   As soon as its **ATP levels are high enough -> release glucose into blood**
-   By 12 hours of fasting, gluconeogensis is the main provider of glucose in the bloodstream

<div class="table-caption">
  <span class="table-number">Table 1</span>:
  Glycogenolysis vs. Gluconeogenesis
</div>

| Glycogenolysis                      | Gluconeogenesis                                     |
|-------------------------------------|-----------------------------------------------------|
| Breaks down **glycogen** -> glucose | **glucose from scratch**                            |
| **12-24 hours** of fasting          | Main provider of glucose **by 12 hours of fasting** |

-   7 reactions in glycolysis are reversible

    -   3 are reversible -> **needs bypass**

    {{< figure src="/ox-hugo/_20210729_175427screenshot.png" caption="Figure 1: Gluconeogenesis vs. Glycolysis" width="500" >}}


### Sources of pyruvate {#sources-of-pyruvate}

-   Two main **sources of pyruvate** are **lactate** and **amino acids like alanine**
    -   _Lactate_ produced as a byproduct of anaerobic respiration in RBCs and exercising skeletal muscle cells
        -   _Lactate DH_ removes a hydrogen from lactate -> pyruvate

            {{< figure src="/ox-hugo/_20210729_175851screenshot.png" width="500" >}}
    -   18/20 [amino acids]({{< relref "amino_acid" >}}) are _glucogenic_ - can be used to make glucose
        -   Leucine and Lysine cannot - "The lazy Ls"
        -   When fasting for a long time -> breakdown of protein in skeletal muscle into AAs
        -   AA attaches to \\(\alpha\\)-ketoglutarate -> glutamate
        -   Transaminase [enzymes]({{< relref "enzyme" >}}) require _pyridoxine ([vitamin B<sub>6</sub>]({{< relref "pyridoxine" >}}))_ as a cofactor

            {{< figure src="/ox-hugo/_20210729_180304screenshot.png" caption="Figure 2: Alanine metabolism in skeletal muscle" width="500" >}}


### Fat metabolism {#fat-metabolism}

-   After fasting for a long time -> body breaks down fats (triacylglycerides)
    -   _Pancreatic \\(\alpha\\) cells_ sense blood glucose levels decreasing -> release [glucagon]({{< relref "glucagon" >}})
    -   Low levels of insulin, presence of epinephrine and ACTH -> adipocytes stimulate [hormone-sensitive lipase]({{< relref "enzyme" >}}) (HSL) -> break down triacylgyceride into 3 fatty acids + glycerol
    -   Fatty acids enter bloodstream -> enter hepatocyte mitochondria -> broken down into Acetyl-CoA + ATP via beta oxidation


### Gluconeogenesis steps {#gluconeogenesis-steps}

1.  Convert pyruvate to PEP **alternate route**

    -   Involves 3 enzymes: _pyruvate carboxylase_, _malate dehydrogenase_, and _PEP carboxykinase (PEPCK)_
        -   _Pyruvate carboxylase_ has 3 cofactors: (ABCs)
            1.  \*A\*TP
            2.  \*B\*iotin
            3.  \*C\*arbon dioxide
    -   First converted to oxaloacetate -> **can't go through mitochondrial membrane** -> **Malate shuttle**

    {{< figure src="/ox-hugo/_20210729_181104screenshot.png" width="500" >}}

    {{< figure src="/ox-hugo/_20210729_181334screenshot.png" caption="Figure 3: Malate shuttle" width="500" >}}

    -   Once oxaloacetate is in the cytoplasm -> _PEPCK_ removes a carbon group + adds phosphate group -> formation of PEP
        -   Uses **GTP** as energy source
        -   _"stress" hormones_ enhance activity of _PEPCK_ via induction
            -   Glucagon
            -   Epinephrine
            -   Cortisol
2.  Reversible reactions **until its converted to DHAP**
    -   Alternatively, _glycerol_ from HSL activity used to make DHAP

        {{< figure src="/ox-hugo/_20210729_183625screenshot.png" caption="Figure 4: Pathway from glycerol to DHAP" width="500" >}}
3.  DHAP converted to fructose-1,6-bisphosphate by a reversible reaction via _[aldolase]({{< relref "enzyme" >}})_
4.  Fructose-1,6-bisphosphate -> fructose-6-phosphate via _[fructose-1,6-bisphosphatase]({{< relref "enzyme" >}})_
    -   **RATE-LIMITING STEP**
    -   Highly regulated

        {{< figure src="/ox-hugo/_20210729_184345screenshot.png" caption="Figure 5: Conversion between Fructose-1,6-bisphosphate and fructose-6-phosphate in glycolysis and gluconeogenesis" width="500" >}}
5.  Isomerize F6P -> G6P

    {{< figure src="/ox-hugo/_20210729_184517screenshot.png" caption="Figure 6: Fructose-6-phosphate isomerized to glucose-6-phosphate via _isomerase_" width="500" >}}
6.  Remove phosphate off G6P -> allow glucose to leave cell
    -   **phosphate group is what keeps glucose inside cell**
    -   Performed by _[glucose-6-phosphatase]({{< relref "enzyme" >}})_ in the endoplasmic reticulum


## Alcoholism affect on [gluconeogenesis]({{< relref "gluconeogenesis" >}}) {#alcoholism-affect-on-gluconeogenesis--gluconeogenesis-dot-md}

-   Ethanol processed in liver cells -> over time, results in accumulation of NADH and ATP in cytoplasm
    -   Increased NADH -> signal the lactic, malate, and glycerol-3-phosphate DH [enzymes]({{< relref "enzyme" >}}) in gluconeogenesis to go in reverse
    -   High ATP -> slows down fatty acid beta oxidation -> abnormal accumulation of fat in liver
-   Both impair gluconeogenesis -> chronic low blood sugar when fasting


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
