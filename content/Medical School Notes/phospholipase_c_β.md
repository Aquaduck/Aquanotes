+++
title = "Phospholipase C-β"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the inositol phosphatide second messenger system]({{< relref "describe_the_inositol_phosphatide_second_messenger_system" >}}) {#describe-the-inositol-phosphatide-second-messenger-system--describe-the-inositol-phosphatide-second-messenger-system-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Important members > Phospholipase**

    <!--list-separator-->

    -  [Phospholipase C-β]({{< relref "phospholipase_c_β" >}})

        -   Cleaves the glycerol-phosphate bond in PIP<sub>2</sub> -> produces DAG (diacylglycerol) and IP<sub>3</sub>


### Unlinked references {#unlinked-references}

[Show unlinked references]
