+++
title = "Describe the anatomical locations for auscultation of the heart valves."
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Clinically Oriented Anatomy]({{< relref "clinically_oriented_anatomy" >}}) {#from-clinically-oriented-anatomy--clinically-oriented-anatomy-dot-md}

-   [Pulmonary valve]({{< relref "pulmonary_valve" >}}): Second intercostal space just to the left of the sternum
-   [Aortic valve]({{< relref "aortic_valve" >}}): Second right intercostal space at the edge of the sternum
-   [Tricuspid valve]({{< relref "tricuspid_valve" >}}): Over left half of inferior end of body of sternum
-   [Bicuspid valve]({{< relref "bicuspid_valve" >}}): Fifth left intercostal space, approximately in the mid-clavicular line


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 3 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-26 Tue] </span></span> > Anatomy clinical correlations > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the anatomical locations for auscultation of the heart valves.]({{< relref "describe_the_anatomical_locations_for_auscultation_of_the_heart_valves" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
