+++
title = "Fatty acid synthesis"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Malonyl-CoA]({{< relref "malonyl_coa" >}}) {#malonyl-coa--malonyl-coa-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The **rate-limiting step** of [fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})

    ---


#### [List the glucose-utilizing pathways that predominate in:]({{< relref "list_the_glucose_utilizing_pathways_that_predominate_in" >}}) {#list-the-glucose-utilizing-pathways-that-predominate-in--list-the-glucose-utilizing-pathways-that-predominate-in-dot-md}

<!--list-separator-->

-  **🔖 Liver**

    Steroid and minimal [fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})

    ---

<!--list-separator-->

-  **🔖 Adipose**

    [Fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})

    ---

<!--list-separator-->

-  **🔖 Muscle**

    [Fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})

    ---

<!--list-separator-->

-  **🔖 Brain**

    [Fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})

    ---


#### [A Discussion of the Significance of Two Key Metabolic Branchpoints]({{< relref "a_discussion_of_the_significance_of_two_key_metabolic_branchpoints" >}}) {#a-discussion-of-the-significance-of-two-key-metabolic-branchpoints--a-discussion-of-the-significance-of-two-key-metabolic-branchpoints-dot-md}

<!--list-separator-->

-  **🔖 Glucose metabolism varies in different cells and tissues > Brain > Fed state**

    Also use glucose for glycogen synthesis and [fatty acid synthesis]({{< relref "fatty_acid_synthesis" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
