+++
title = "Kaplan Biochemistry Chapter 6"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["medschool", "kaplan", "source"]
draft = false
+++

## 6.2 Eukaryotic Chromosome Organization {#6-dot-2-eukaryotic-chromosome-organization}


### Histones {#histones}

-   Group of small basic proteins
-   A kind of _nucleoprotein_: proteins that associate with DNA
-   DNA wrapped around histones -> _chromatin_
-   **Five** histone proteins found in eukaryotic cells
    -   Two copies of 4 of the histone proteins form a core -> 200bps of DNA wrap around the core -> formation of _nucleosome_
    -   Last histone seals off DNA as it enters and leaves nucleosome -> stability


### Heterochromatin and Euchromatin {#heterochromatin-and-euchromatin}

-   _Heterochromatin_: Compacted chromatin
-   _Euchromatin_: Dispersed chromatine


### Telomeres and Centromeres {#telomeres-and-centromeres}

-   _Telomere_: simple, repeating unit at the end of DNA
    -   Some of the telomere sequence is lost each round of DNA replication -> replaced by the enzyme _telomerase_
        -   More highly expressed in rapidly dividing cells
    -   Progressive shortening of telomeres contributes to aging
    -   High GC-content creates exceptionally strong strand attractions at the end of chromosomes -> prevent unraveling
-   _Centromeres_: region of heterochromatin found in the center of chromosomes
    -   Allows two sister chromatids to remain connected during cell division until microtubules separate them during anaphase


## 6.4 [DNA Repair]({{< relref "dna_repair" >}}) {#6-dot-4-dna-repair--dna-repair-dot-md}


### Thymine Dimers {#thymine-dimers}

-   **Ultraviolet light** induces the formation of **dimers between adjacent thymine residues in DNA**
    -   Called _thymine dimers_
    -   Interferes with DNA replication and normal gene expression
    -   Distorts the shape of the double helix
    -   Eliminated by [nucleotide excision repair]({{< relref "khan_dna_proofreading_and_repair" >}})

{{< figure src="/ox-hugo/_20210721_175634screenshot.png" caption="Figure 1: Thymine dimer formation and nucleotide excision repair" >}}


### Thermal energy {#thermal-energy}

-   Thermal energy absorbed by DNA -> _Cytosine deamination_
    -   Loss of an amino group from cytosine -> converted into **uracil**


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 6. Summarize the major types of DNA damage caused by replication errors, ionizing radiation, and reactive oxygen species**

    [6.4 DNA Repair]({{< relref "kaplan_biochemistry_chapter_6" >}})

    ---

<!--list-separator-->

-  **🔖 DNA Replication, Transcription, and Translation > 3. Summarize the structure and function of eukaryotic chromosomes and chromatin, including chromatin modifications, telomeres, and centromeres**

    [6.2 Eukaryotic Chromosome Organization]({{< relref "kaplan_biochemistry_chapter_6" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
