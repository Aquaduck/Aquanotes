+++
title = "Smac, a Mitochondrial Protein that Promotes Cytochrome c–Dependent Caspase Activation by Eliminating IAP Inhibition"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Discussion {#discussion}


### [SMAC]({{< relref "second_mitochondria_derived_activator_of_caspases" >}}) promotes [procaspase-9]({{< relref "procaspase_9" >}}) activation by countering IAPs {#smac--second-mitochondria-derived-activator-of-caspases-dot-md--promotes-procaspase-9--procaspase-9-dot-md--activation-by-countering-iaps}

-   SMAC is released from mitochondria during apoptosis -> neutralizes the inhibitory activity of [IAPs]({{< relref "inhibitor_of_apoptosis" >}})


### Regulation of [SMAC]({{< relref "second_mitochondria_derived_activator_of_caspases" >}}) {#regulation-of-smac--second-mitochondria-derived-activator-of-caspases-dot-md}

-   SMAC is a mitochondrial protein with a typical amphipathic mitochondrial targeting sequence at its N terminus -> cleaved after mitochondrial entry -> activation
-   Only mature SMAC has caspase activation promoting activity
    -   Thus, requires a maturation process inside mitochondria before gaining its apoptotic activity
-   Since SMAC is only released during apoptosis, the major regulatory step for SMAC should be its release from the mitochondria
    -   This process is likely to be controlled by the [Bcl2 family]({{< relref "b_cell_lymphoma_2_family" >}}) of proteins
    -   This is analogous to the requirement for mitochondrial processing of [cytochrome c]({{< relref "cytochrome_c" >}}), which promotes caspase activation after release from the mitochondrial intermembrnae space


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
