+++
title = "SAR1B gene"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Codes for the SAR1B protein


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Chylomicron retention disease]({{< relref "chylomicron_retention_disease" >}}) {#chylomicron-retention-disease--chylomicron-retention-disease-dot-md}

<!--list-separator-->

-  **🔖 From Molecular analysis and intestinal expression of SAR1 genes and proteins in Anderson's disease (Chylomicron retention disease)**

    Results from a mutation in the [SAR1B gene]({{< relref "sar1b_gene" >}}) which encodes the [SAR1B protein]({{< relref "sar1b_protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
