+++
title = "Anesthesia"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the procedures associated with intercostal nerve anesthesia and thoracotomy and their indications.]({{< relref "describe_the_procedures_associated_with_intercostal_nerve_anesthesia_and_thoracotomy_and_their_indications" >}}) {#describe-the-procedures-associated-with-intercostal-nerve-anesthesia-and-thoracotomy-and-their-indications-dot--describe-the-procedures-associated-with-intercostal-nerve-anesthesia-and-thoracotomy-and-their-indications-dot-md}

<!--list-separator-->

-  **🔖 From Accessing an Intercostal Space**

    <!--list-separator-->

    -  [Anesthesia]({{< relref "anesthesia" >}})

        -   Anesthesia can be placed in the intercostal space to block the [intercostal nerve]({{< relref "intercostal_nerve" >}})
        -   **Ideally, it should be given between internal and innermost intercostal layers** where the nerve resides
            -   Can use ultrasound to guide procedure
        -   Procedure is commonly used in **patients with rib fractures or for thoracic surgery**
        -   When inserting a chest tube, the tube is inserted in the **inferior portion of the intercostal space**, just above the lower rib
            -   **Minimizes risk to vessels in the superior portion of the intercostal space**


### Unlinked references {#unlinked-references}

[Show unlinked references]
