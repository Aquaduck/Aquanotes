+++
title = "Osmolality"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Kidney]({{< relref "kidney" >}}) {#kidney--kidney-dot-md}

<!--list-separator-->

-  **🔖 Concept links > Major functions**

    Regulation of [plasma]({{< relref "plasma" >}}) [osmolality]({{< relref "osmolality" >}})

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Major physiological functions of the kidney > Production of Urine**

    **Regulation** of extracellular fluid volume and [osmolality]({{< relref "osmolality" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
