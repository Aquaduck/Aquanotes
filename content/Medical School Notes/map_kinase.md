+++
title = "MAP kinase"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Discuss receptor tyrosine kinases and tyrosine-associated receptor signal pathways]({{< relref "discuss_receptor_tyrosine_kinases_and_tyrosine_associated_receptor_signal_pathways" >}}) {#discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways--discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Receptor protein tyrosine kinases (RPTKs) > Two key pathways > Ras-GTP > Pathway**

    Ras activates [MAP-kinase-kinase-kinase]({{< relref "map_kinase_kinase_kinase" >}}) (Raf) -> activates [MAP-kinase-kinase]({{< relref "map_kinase_kinase" >}}) (Mek) -> activates [MAP-kinase]({{< relref "map_kinase" >}}) (Erk)

    ---


#### [Cell Signalling III - Underlying Design Features]({{< relref "cell_signalling_iii_underlying_design_features" >}}) {#cell-signalling-iii-underlying-design-features--cell-signalling-iii-underlying-design-features-dot-md}

<!--list-separator-->

-  **🔖 AM vs FM signaling > Macromolecule complexes**

    <!--list-separator-->

    -  [MAP Kinase]({{< relref "map_kinase" >}}) cascade complex

        -   Another example, will be elaborated on later


### Unlinked references {#unlinked-references}

[Show unlinked references]
