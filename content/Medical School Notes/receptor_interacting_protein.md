+++
title = "Receptor interacting protein"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  **🔖 TRADD in TRAIL/TRAILR Signaling**

    Both TRADD and [RIP]({{< relref "receptor_interacting_protein" >}}) were demonstrated to be recruited to the TRAILR complex in the [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
