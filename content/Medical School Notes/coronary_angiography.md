+++
title = "Coronary angiography"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.]({{< relref "describe_the_common_clinical_procedures_involving_the_heart_include_a_description_of_the_procedure_s_and_the_structure_s_that_are_accessed_or_repaired" >}}) {#describe-the-common-clinical-procedures-involving-the-heart-dot-include-a-description-of-the-procedure--s--and-the-structure--s--that-are-accessed-or-repaired-dot--describe-the-common-clinical-procedures-involving-the-heart-include-a-description-of-the-procedure-s-and-the-structure-s-that-are-accessed-or-repaired-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Clinical procedures related to heart structures:**

    <!--list-separator-->

    -  [Coronary angiography]({{< relref "coronary_angiography" >}})

        -   Coronary arteries visualized with coronary arteriograms
        -   Long narrow catheter passed into ascending aorta via femoral artery
        -   Under fluoroscopic control, tip of catheter placed just inside opening of a coronary artery
        -   Small injection of radiopaque contrast material made -> cineradiographs taken to show lumen of artery and its branches, as well as any stenotic areas present
        -   Noninvasive CT or MR angiography is replacing invasive conventional methods


### Unlinked references {#unlinked-references}

[Show unlinked references]
