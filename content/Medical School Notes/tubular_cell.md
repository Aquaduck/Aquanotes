+++
title = "Tubular cell"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Cells in the [kidney]({{< relref "kidney" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Antidiuretic Hormone (ADH) (p.746)**

    The target cells of ADH are located in the [tubular cells]({{< relref "tubular_cell" >}}) of the [kidney]({{< relref "kidney" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
