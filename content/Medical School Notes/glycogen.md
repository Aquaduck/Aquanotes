+++
title = "Glycogen"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Kinetic and Allosteric Regulation of Glucose Catabolism]({{< relref "kinetic_and_allosteric_regulation_of_glucose_catabolism" >}}) {#kinetic-and-allosteric-regulation-of-glucose-catabolism--kinetic-and-allosteric-regulation-of-glucose-catabolism-dot-md}

<!--list-separator-->

-  **🔖 Glycolysis > 6-Phosphofructo-1-kinase (PFK-1)**

    Slows [glycolysis]({{< relref "glycolysis" >}}) and allows for the synthesis of either [glycogen]({{< relref "glycogen" >}}) or fatty acid synthesis

    ---


#### [Glucose 6-phosphate]({{< relref "glucose_6_phosphate" >}}) {#glucose-6-phosphate--glucose-6-phosphate-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Signals [muscle]({{< relref "muscle" >}}) cells to start [synthesizing]({{< relref "glycogenesis" >}}) [glycogen]({{< relref "glycogen" >}})

    ---


#### [6-Phosphofructo-1-kinase]({{< relref "6_phosphofructo_1_kinase" >}}) {#6-phosphofructo-1-kinase--6-phosphofructo-1-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Slows glycolysis and allows for the synthesis of either [glycogen]({{< relref "glycogen" >}}) or fatty acid synthesis

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
