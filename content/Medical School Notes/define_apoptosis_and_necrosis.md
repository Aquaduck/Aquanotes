+++
title = "Define apoptosis and necrosis"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}}) {#from-molecular-biology-of-the-cell-sixth-edition--molecular-biology-of-the-cell-sixth-edition-dot-md}


### Intro (p. 1021) {#intro--p-dot-1021}

-   [Apoptosis]({{< relref "apoptosis" >}}) is programmed cell death, whereas [necrosis]({{< relref "necrosis" >}}) occurs in response to suboptimal conditions such as truama or inadequate blood supply


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Intrinsic pathway of apoptosis]({{< relref "intrinsic_pathway_of_apoptosis" >}}) {#intrinsic-pathway-of-apoptosis--intrinsic-pathway-of-apoptosis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    One pathway of [apoptosis]({{< relref "define_apoptosis_and_necrosis" >}})

    ---


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-27 Mon] </span></span> > Cell Death > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Define apoptosis and necrosis]({{< relref "define_apoptosis_and_necrosis" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
