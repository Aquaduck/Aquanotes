+++
title = "Ascending aorta"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The portion of the [aorta]({{< relref "aorta" >}}) that rises immediately after the heart


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Aortic sinus]({{< relref "aortic_sinus" >}}) {#aortic-sinus--aortic-sinus-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A space immediately above the [aortic valve]({{< relref "aortic_valve" >}}), between an aortic wall leaflet and the wall of the [ascending aorta]({{< relref "ascending_aorta" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
