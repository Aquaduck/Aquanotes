+++
title = "Oxygen"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  [Oxygen]({{< relref "oxygen" >}}) Dissociation from [Hemoglobin]({{< relref "hemoglobin" >}}) (p. 1074)

    -   _Oxygen-hemoglobin dissociation curve_: graph that describes the relationship of partial pressure to the binding of oxygen to [heme]({{< relref "heme" >}}) vs. its subsequent dissociation from heme
        -   Two important rules to this relationship:
            1.  **Gases travel from an area of high partial pressure to an area of lower partial pressure**
            2.  **Oxygen affinity to [heme]({{< relref "heme" >}}) increases as more oxygen molecules are bound**
        -   Thus -> **as partial pressure of oxygen increases, more oxygen is bound by heme**

    {{< figure src="/ox-hugo/_20211017_162608screenshot.png" caption="Figure 1: Partial pressure of oxygen and hemoglobin saturation" >}}

    -   Certain hormones can affect the oxygen-hemoglobin saturation/dissociation curve by **stimulating the production of [2,3-BPG]({{< relref "2_3_biphosphoglycerate" >}})** e.g.:
        -   Androgens
        -   Epinephrine
        -   Thyroid hormones
        -   Growth hormone
    -   BPG is a byproduct of glycolysis


#### [Ischemia]({{< relref "ischemia" >}}) {#ischemia--ischemia-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Decreased [Blood]({{< relref "blood" >}}) supply that cannot meet [Oxygen]({{< relref "oxygen" >}}) demands of an organ or tissue

    ---


#### [Hypoxia]({{< relref "hypoxia" >}}) {#hypoxia--hypoxia-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inadequate supply of [oxygen]({{< relref "oxygen" >}}) to tissues

    ---


#### [Hypoxemia]({{< relref "hypoxemia" >}}) {#hypoxemia--hypoxemia-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Decreased [oxygen]({{< relref "oxygen" >}}) content in arterial [blood]({{< relref "blood" >}}) (↓ PaO<sub>2</sub>)

    ---


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 HIF-1 target genes involved in glucose and energy metabolism**

    HIF-1 appears to play a crucial homeostatic role in **managing [oxygen]({{< relref "oxygen" >}}) consumption** to balance [ATP]({{< relref "atp" >}}) and [ROS]({{< relref "reactive_oxygen_species" >}}) production

    ---


#### [Haldane effect]({{< relref "haldane_effect" >}}) {#haldane-effect--haldane-effect-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The [carbon dioxide]({{< relref "carbon_dioxide" >}}) affinity of [Hemoglobin]({{< relref "hemoglobin" >}}) is **inversely proportional** to the [oxygenation]({{< relref "oxygen" >}}) of hemoglobin

    ---


#### [Fraction of inspired oxygen]({{< relref "fraction_of_inspired_oxygen" >}}) {#fraction-of-inspired-oxygen--fraction-of-inspired-oxygen-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Fraction of [Oxygen]({{< relref "oxygen" >}}) (by volume) in the [inspired]({{< relref "inspiration" >}}) air

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology > Infarction**

    Tissue necrosis that results from insufficient [Blood]({{< relref "blood" >}}) nad [Oxygen]({{< relref "oxygen" >}}) supply to affected region

    ---

<!--list-separator-->

-  **🔖 Respiration > Fraction of inspired oxygen**

    Fraction of [Oxygen]({{< relref "oxygen" >}}) (by volume) in the [inspired]({{< relref "inspiration" >}}) air

    ---


#### [A-a gradient]({{< relref "a_a_gradient" >}}) {#a-a-gradient--a-a-gradient-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Difference in partial pressure of [oxygen]({{< relref "oxygen" >}}) in the [alveoli]({{< relref "alveoli" >}}) (A) and the partial pressure of oxygen in the arteries (a)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
