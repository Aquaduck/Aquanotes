+++
title = "Mediastinum"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Parietal pleura]({{< relref "parietal_pleura" >}}) {#parietal-pleura--parietal-pleura-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The outer layer that connects to the thoracic wall, the [mediastinum]({{< relref "mediastinum" >}}), and the [diaphragm]({{< relref "diaphragm" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
