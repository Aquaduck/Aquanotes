+++
title = "Primary heart field"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   An embryonic structure of the [Heart]({{< relref "heart" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Langman's Medical Embryology]({{< relref "langman_s_medical_embryology" >}}) {#langman-s-medical-embryology--langman-s-medical-embryology-dot-md}

<!--list-separator-->

-  **🔖 13: Cardiovascular System > Establishment and Patterning of the Primary Heart Field**

    From there, they migrate through the streak and into the visceral layer of lateral plate mesoderm -> some form a horseshoe-shaped cluster of cells called the [primary heart field]({{< relref "primary_heart_field" >}}) -> eventually form **portions of the atria** and **the entire [left ventricle]({{< relref "left_ventricle" >}})**

    ---

<!--list-separator-->

-  **🔖 13: Cardiovascular System**

    <!--list-separator-->

    -  Establishment and Patterning of the [Primary Heart Field]({{< relref "primary_heart_field" >}})

        -   The [vascular system]({{< relref "vascular_system" >}}) appears in the middle of the **third week** when the embryo is no longer able to satisfy its nutritional requirements by diffusion alone
        -   _Progenitor heart cells_ lie in the [epiblast]({{< relref "epiblast" >}}), immediately adjacent to the **cranial end** of the [primitive streak]({{< relref "primitive_streak" >}})
            -   From there, they migrate through the streak and into the visceral layer of lateral plate mesoderm -> some form a horseshoe-shaped cluster of cells called the [primary heart field]({{< relref "primary_heart_field" >}}) -> eventually form **portions of the atria** and **the entire [left ventricle]({{< relref "left_ventricle" >}})**


### Unlinked references {#unlinked-references}

[Show unlinked references]
