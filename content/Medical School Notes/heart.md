+++
title = "Heart"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 15 linked references {#15-linked-references}


#### [Tetralogy of Fallot]({{< relref "tetralogy_of_fallot" >}}) {#tetralogy-of-fallot--tetralogy-of-fallot-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The simultaneous occurrence of four [cardiac]({{< relref "heart" >}}) defects:

    ---


#### [Shunt]({{< relref "shunt" >}}) {#shunt--shunt-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Pathological connections between the right and left [Heart]({{< relref "heart" >}}) chambers

    ---


#### [Right ventricle]({{< relref "right_ventricle" >}}) {#right-ventricle--right-ventricle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The right ventricle of the [heart]({{< relref "heart" >}})

    ---


#### [Right atrium]({{< relref "right_atrium" >}}) {#right-atrium--right-atrium-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Chamber of the [heart]({{< relref "heart" >}}) fed by the [IVC]({{< relref "inferior_vena_cava" >}}) and [SVC]({{< relref "superior_vena_cava" >}})

    ---


#### [Purkinje fiber]({{< relref "purkinje_fiber" >}}) {#purkinje-fiber--purkinje-fiber-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    In the [Heart]({{< relref "heart" >}})

    ---


#### [Pulmonary artery]({{< relref "pulmonary_artery" >}}) {#pulmonary-artery--pulmonary-artery-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Brings deoxygenated blood from the [Heart]({{< relref "heart" >}}) to the [lungs]({{< relref "lung" >}})

    ---


#### [Primary heart field]({{< relref "primary_heart_field" >}}) {#primary-heart-field--primary-heart-field-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An embryonic structure of the [Heart]({{< relref "heart" >}})

    ---


#### [Myocardial infarction]({{< relref "myocardial_infarction" >}}) {#myocardial-infarction--myocardial-infarction-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Infarction]({{< relref "infarction" >}}) of [Heart]({{< relref "heart" >}}) tissue due to prolonged [Ischemia]({{< relref "ischemia" >}})

    ---


#### [Left ventricle]({{< relref "left_ventricle" >}}) {#left-ventricle--left-ventricle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A chamber of the [heart]({{< relref "heart" >}})

    ---


#### [Describe the vessels supplying the conduction system of the heart and the clinical presentation following damage to these.]({{< relref "describe_the_vessels_supplying_the_conduction_system_of_the_heart_and_the_clinical_presentation_following_damage_to_these" >}}) {#describe-the-vessels-supplying-the-conduction-system-of-the-heart-and-the-clinical-presentation-following-damage-to-these-dot--describe-the-vessels-supplying-the-conduction-system-of-the-heart-and-the-clinical-presentation-following-damage-to-these-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  Coronary occlusion and conducting system of [Heart]({{< relref "heart" >}})

        -   Damage to conducting system of heart -> disturbances of [cardiac muscle]({{< relref "cardiac_muscle" >}}) contraction
        -   LAD gives rise to septal branches supplying AV bundle in most people
        -   Branches of RCA supply both SA and AV nodes (60% of the time, 40% of people have supply from LCA instead) -> conducting system affected by occlusion -> heart block
            -   Ventricles will begin to contract independently at their own rate of 25-30 times per minute (much slower than normal)
            -   Atria continue to contract at normal rate if SA node is spared, but impulse from SA does not reach ventricles
        -   Damage to one bundle branch -> bundle-branch block
            -   Excitation passes along unaffected branch and causes a normally timed systole **of that ventricle only**
            -   Impulse then spreads to other ventricle via myogenic (muscle propagated) conduction -> **late asynchronous contraction**
            -   In this case, a cardiac pacemaker may be implanted to increase the ventricular rate of contraction to 70-80 per minute


#### [Describe the common clinical procedures involving the heart. Include a description of the procedure(s) and the structure(s) that are accessed or repaired.]({{< relref "describe_the_common_clinical_procedures_involving_the_heart_include_a_description_of_the_procedure_s_and_the_structure_s_that_are_accessed_or_repaired" >}}) {#describe-the-common-clinical-procedures-involving-the-heart-dot-include-a-description-of-the-procedure--s--and-the-structure--s--that-are-accessed-or-repaired-dot--describe-the-common-clinical-procedures-involving-the-heart-include-a-description-of-the-procedure-s-and-the-structure-s-that-are-accessed-or-repaired-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  Clinical procedures related to [heart]({{< relref "heart" >}}) structures:

        <!--list-separator-->

        -  [Cardiac catheterization]({{< relref "cardiac_catheterization" >}}):

            -   Radiopaque catheter inserted into peripheral vein (e.g. femoral vein) and passed under fluoroscopic control into the [right atrium]({{< relref "right_atrium" >}}), [right ventricle]({{< relref "right_ventricle" >}}), [pulmonary trunk]({{< relref "pulmonary_trunk" >}}) and [pulmonary arteries]({{< relref "pulmonary_artery" >}}) respectively
            -   Intracardiac pressures can be recorded and blood samples may be removed
            -   If radopaque contrast medium is injected -> can be followed through heart and great vessels using serially exposed X-ray films

        <!--list-separator-->

        -  [Coronary angiography]({{< relref "coronary_angiography" >}})

            -   Coronary arteries visualized with coronary arteriograms
            -   Long narrow catheter passed into ascending aorta via femoral artery
            -   Under fluoroscopic control, tip of catheter placed just inside opening of a coronary artery
            -   Small injection of radiopaque contrast material made -> cineradiographs taken to show lumen of artery and its branches, as well as any stenotic areas present
            -   Noninvasive CT or MR angiography is replacing invasive conventional methods

        <!--list-separator-->

        -  [Coronary artery bypass]({{< relref "coronary_artery_bypass" >}})

            -   Segment of artery or vein is connected to ascending aorta or to proximal part of a coronary artery -> connected to coronary artery distal to stenosis
            -   Great saphenous vein is commonly harvested for coronary bypass because:
                1.  Has a diameter equal to or greater than that of oronary arteries
                2.  Can be easily dissected from lower limb
                3.  Offers relatively lengthy portion with minimum occurrence of valves or branching
            -   Use of radial artery in bypass surgery has become more common
            -   Coronary bypass graft shunts blood from aorta to a steonic coronary artery -> increase flow distal to obstrution
                -   i.e. detour around stenotic area
            -   Revascularization of myocardium may also be achieved by surgically anastomosing an internal thoracic artery with a coronary artery


#### [Coronary circulation]({{< relref "coronary_circulation" >}}) {#coronary-circulation--coronary-circulation-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Circulation of blood that perfuses the [Heart]({{< relref "heart" >}})

    ---


#### [Cardiac muscle]({{< relref "cardiac_muscle" >}}) {#cardiac-muscle--cardiac-muscle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A subtype of [striated muscle]({{< relref "striated_muscle" >}}) found in the [heart]({{< relref "heart" >}})

    ---


#### [Cardiac cycle]({{< relref "cardiac_cycle" >}}) {#cardiac-cycle--cardiac-cycle-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The cycle of the [Heart]({{< relref "heart" >}}) to pump [blood]({{< relref "blood" >}}) through the [Cardiovascular system]({{< relref "cardiovascular_system" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Embryology > Ectopia cordis**

    Rare congenital condition where the [Heart]({{< relref "heart" >}}) is completely or partially located outside the thoracic cavity

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
