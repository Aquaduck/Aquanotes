+++
title = "Frank-Starling law"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Describe the relationship between ventricular filling and stroke volume on a Frank-Starling curve.]({{< relref "describe_the_relationship_between_ventricular_filling_and_stroke_volume_on_a_frank_starling_curve" >}}) {#describe-the-relationship-between-ventricular-filling-and-stroke-volume-on-a-frank-starling-curve-dot--describe-the-relationship-between-ventricular-filling-and-stroke-volume-on-a-frank-starling-curve-dot-md}

<!--list-separator-->

-  **🔖 From Physiology, Frank Starling Law**

    <!--list-separator-->

    -  Clinical Significance of the [Frank-Starling law]({{< relref "frank_starling_law" >}})

        -   Plays a role in the **compensation of systolic heart failure** by buffering the fall in cardiac output to help preserve sufficient blood pressure to perfuse vital organs
            -   In severe heart failure with greater cardiac contractility malfunction, the ventricular performance curve may be nearly flat at higher diastolic volumes
                -   This reduces the increased cardiac output with increases in chamber filling
                -   May result in pulmonary congestion
        -   Also plays a compensatory role in patients with dilated cardiomyopathy

<!--list-separator-->

-  **🔖 From Physiology, Frank Starling Law**

    <!--list-separator-->

    -  Mechanism of the [Frank-Starling law]({{< relref "frank_starling_law" >}})

        -   On the curve of a normally functioning heart, cardiac performance increases continuously as preload increases
            -   During states of **increased left ventricular contractility**, there is a **greater cardiac performance** for a given preload
                -   Represented graphically as an **upward shift of the normal curve**
            -   During states of **decreased left ventricular contractility** associated with **systolic heart failure**, there is **decreased cardiac performance** for a given preload as compared to the normal curve
                -   Represented graphically as a **downward shift of the normal curve**
        -   Changes in [afterload]({{< relref "afterload" >}}) (the force of resistance the ventricle must overcome to empty contents at the beginning of systole) will also shift the Frank-Starling curve
            -   **Decreased afterload -> upward shift** of ventricular performance curve
            -   **Increased afterload -> downward shift** of ventricular performance curve
        -   An increase in [catecholamines]({{< relref "catecholamine" >}}) (e.g. norepinephrine during exercise) -> upward shift of Frank-Starling curve
            -   Caused by catecholamines binding to a myocyte beta1-adrenergic receptor (a G-protein coupled receptor) -> increased calcium channel release from the [Sarcoplasmic reticulum]({{< relref "sarcoplasmic_reticulum" >}}) -> enables force of contraction

<!--list-separator-->

-  **🔖 From Physiology, Frank Starling Law > Introduction**

    The [Frank-Starling relationship]({{< relref "frank_starling_law" >}}) is the observation that **ventricular output increases as preload (end-diastolic pressure) increases**

    ---

<!--list-separator-->

-  **🔖 From Physiology, Frank Starling Law > Introduction**

    The [Frank-Starling relationship]({{< relref "frank_starling_law" >}}) is based on the **link between initial length of myocardial fibers and the force generated by contraction**

    ---


#### [Afterload]({{< relref "afterload" >}}) {#afterload--afterload-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Changes in afterload will shift the [Frank-Starling]({{< relref "frank_starling_law" >}}) curve

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
