+++
title = "Describe the history and meaning of the term Homeostasis."
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Homeostasis PPT]({{< relref "homeostasis_ppt" >}}) {#from-homeostasis-ppt--homeostasis-ppt-dot-md}


### Claude Bernard {#claude-bernard}

-   _Claude Bernard_ (the father of Physiology) explained that **life depended on maintaining the correct internal environment**
    -   He called this _The Goldilocks condition_
-   _Milieu interieur_: "internal environment" - the compartment between cells and tissues that arose with the development of multicellular life forms


### Walter Cannon {#walter-cannon}

-   Actually invented the term [Homeostasis]({{< relref "homeostasis" >}})
    -   Referred to **the problems of maintaining the Goldilocks condition in the face of external challenges**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon] </span></span> > Homeostasis > Session**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the history and meaning of the term Homeostasis.]({{< relref "describe_the_history_and_meaning_of_the_term_homeostasis" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
