+++
title = "Breast cancer"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   [Cancer]({{< relref "cancer" >}}) of the [Breast]({{< relref "breast" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe lymphatic flow in the breast.]({{< relref "describe_lymphatic_flow_in_the_breast" >}}) {#describe-lymphatic-flow-in-the-breast-dot--describe-lymphatic-flow-in-the-breast-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  Skin changes in [breast cancer]({{< relref "breast_cancer" >}})

        -   Lymphedema
        -   "Pitting" or "dimpling" along skin of breast
        -   Presence of lump under skin
        -   Rash-like color changes
        -   Mammography is primarily used for screening
            -   In conventional mammography, denser structures appear light

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  Metastasis of [Breast Cancer]({{< relref "breast_cancer" >}})

        -   Carcinomas of the brast are malignant tumors
            -   Usually adenocarcinomas (glandular cancer) arising from epithelial cells of lactiferous ducts in mamary gland lobules
        -   ~60% of malignant breast tumors arise from _superior lateral quadrant_ -> tumor cells that break away picked up by lymph and travel to lymph nodes in _axillary region_
        -   Breast cancer typically spreads form breast by lymphatic vessels
            -   Most common site is _axillary node_
        -   Surgical removal of axillary lymph nodes can disrupt lymph flow from upper limb -> lymphedema in affected limb


### Unlinked references {#unlinked-references}

[Show unlinked references]
