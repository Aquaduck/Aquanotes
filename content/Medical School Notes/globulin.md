+++
title = "Globulin"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Plasma Proteins (p. 796)**

    <!--list-separator-->

    -  [Globulin]({{< relref "globulin" >}})

        -   Second most common plasma protein
        -   Three main subgroups: alpha, beta, gamma globulins
            -   [Alpha]({{< relref "alpha_globulin" >}}) and [beta]({{< relref "beta_globulin" >}}) globulins transport iron, lipids, and fat-soluble vitamins A, D, E, and K to the cells
                -   They also contribute to osmotic pressure
            -   [Gamma globulins]({{< relref "gamma_globulin" >}}) are proteins involved in immunity, better known as antibodies or [immunoglobulins]({{< relref "immunoglobulin" >}})
                -   Gamma globulins are produced by plasma cells


#### [Beta globulin]({{< relref "beta_globulin" >}}) {#beta-globulin--beta-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    One of the three subgroups of [globulins]({{< relref "globulin" >}})

    ---


#### [Alpha globulin]({{< relref "alpha_globulin" >}}) {#alpha-globulin--alpha-globulin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    One of the three subgroups of [globulins]({{< relref "globulin" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
