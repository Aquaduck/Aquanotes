+++
title = "Club cell"
author = ["Arif Ahsan"]
date = 2021-10-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

Formerly called "Clara cells" but Max Clara is a nazi so we don't do that anymore


## Concept links {#concept-links}

-   Nonciliated, secretory, cuboidal cells located in the terminal and respiratory bronchioles of the [lungs]({{< relref "lung" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration**

    <!--list-separator-->

    -  [Club cell]({{< relref "club_cell" >}})

        -   Nonciliated, secretory, cuboidal cells located in the terminal and respiratory bronchioles of the [lungs]({{< relref "lung" >}})
        -   **Function:** maintain the integrity of respiratory epithelium by **secreting specialized immunomodulary proteins, glycoproteins, and lipids**
        -   [Cytochrome P450]({{< relref "cytochrome_p450" >}}) dependent degradation of toxins
        -   Act as a reserve for ciliated cells to restore bronchiolar epithelium


### Unlinked references {#unlinked-references}

[Show unlinked references]
