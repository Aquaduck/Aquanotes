+++
title = "MSH2"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   One of a set of genes known as the [mismatch repair]({{< relref "dna_mismatch_repair" >}}) (MMR) genes


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Lynch syndrome (hereditary nonpolyposis colorectal cancer): Clinical manifestations and diagnosis]({{< relref "lynch_syndrome_hereditary_nonpolyposis_colorectal_cancer_clinical_manifestations_and_diagnosis" >}}) {#lynch-syndrome--hereditary-nonpolyposis-colorectal-cancer--clinical-manifestations-and-diagnosis--lynch-syndrome-hereditary-nonpolyposis-colorectal-cancer-clinical-manifestations-and-diagnosis-dot-md}

<!--list-separator-->

-  **🔖 Lynch syndrome > Genetics**

    Lynch syndrome is an autosomal dominant disorder that is caused by a **germline mutation** in one of several [DNA mismatch repair]({{< relref "dna_mismatch_repair" >}}) (MMR) genes or loss of expression of [MSH2]({{< relref "msh2" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
