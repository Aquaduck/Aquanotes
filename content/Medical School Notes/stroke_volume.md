+++
title = "Stroke volume"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Stroke volume]({{< relref "stroke_volume" >}}) = volume of [blood]({{< relref "blood" >}}) ejected from [left ventricle]({{< relref "left_ventricle" >}}) per heartbeat

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
