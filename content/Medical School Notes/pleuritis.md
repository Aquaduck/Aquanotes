+++
title = "Pleuritis"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the etiology of pleuritis.]({{< relref "describe_the_etiology_of_pleuritis" >}}) {#describe-the-etiology-of-pleuritis-dot--describe-the-etiology-of-pleuritis-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  [Pleuritis]({{< relref "pleuritis" >}})

        -   During inspiration and expiration, sliding of normally smooth, moist pleurae makes no detectable sound during ascultation of lungs
        -   Inflammatoin of pleura (pleuritic) makes lung surfaces rough
            -   Sounds like clump of hairs being rolled between fingers
        -   Inflamed surfaces of pleura may also cause parietal and visceral layers of pleura to adhere (pleural adhesion)
        -   Acute pleuritis marked by **sharp, stabbing pain especially on exertion**
        -   Typically a result of infection, [cancer]({{< relref "cancer" >}}), and [congestive heart failure]({{< relref "congestive_heart_failure" >}})
        -   Typical complaints: chest pain, SOB, and pain in shoulder


### Unlinked references {#unlinked-references}

[Show unlinked references]
