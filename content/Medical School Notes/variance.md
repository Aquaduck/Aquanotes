+++
title = "Variance"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}}) {#explain-the-t-test-and-the-factors-that-influence-t-values--explain-the-t-test-and-the-factors-that-influence-t-values-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > T-test > Two sample t-test**

    Both sample groups are drawn from the same population and have the same (but unknown) [variance]({{< relref "variance" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
