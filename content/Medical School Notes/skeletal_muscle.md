+++
title = "Skeletal muscle"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A subtype of [striated muscle]({{< relref "striated_muscle" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Skeletal muscle cell]({{< relref "skeletal_muscle_cell" >}}) {#skeletal-muscle-cell--skeletal-muscle-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept link**

    Cellular unit of [skeletal muscle]({{< relref "skeletal_muscle" >}})

    ---


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Chylomicrons > From Fat Metabolism in Muscle & Adipose Tissue**

    [Skeletal muscle]({{< relref "skeletal_muscle" >}}) obtains dietary fatty acids from chylomicrons that can be "re-synthesized" into triacylglycerol due to the muscle's regular uptake of glucose

    ---


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Overview - Muscle**

    Striated is further subdivided into _[skeletal]({{< relref "skeletal_muscle" >}})_ and _[cardiac]({{< relref "cardiac_muscle" >}})_ muscles

    ---


#### [Describe the hormonal and intracellular conditions which regulate the mobilization of intramuscular fat.]({{< relref "describe_the_hormonal_and_intracellular_conditions_which_regulate_the_mobilization_of_intramuscular_fat" >}}) {#describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot--describe-the-hormonal-and-intracellular-conditions-which-regulate-the-mobilization-of-intramuscular-fat-dot-md}

<!--list-separator-->

-  **🔖 From Fat Metabolism in Muscle & Adipose Tissue > Regulation of the mobilization of intramuscular fat**

    <!--list-separator-->

    -  Role of [skeletal muscle]({{< relref "skeletal_muscle" >}}) contraction

        1.  Nerve impulse via [acetylcholine]({{< relref "acetylcholine" >}}) to acetylcholine receptor
        2.  Depolarization
        3.  Stimulation of ER to release [Calcium]({{< relref "calcium" >}})
        4.  Activation of [PKC]({{< relref "protein_kinase_c" >}})
        5.  Activation of [HSL]({{< relref "hormone_sensitive_lipase" >}}) -> Breakdown of lipid droplet into fatty acids + glycerol


#### [Chylomicron]({{< relref "chylomicron" >}}) {#chylomicron--chylomicron-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Skeletal muscle]({{< relref "skeletal_muscle" >}}) obtains [dietary fatty acids]({{< relref "dietary_lipid" >}}) from chylomicrons that can be "re-synthesized" into [triacylglycerol]({{< relref "triacylglycerol" >}}) due to the muscle's regular uptake of glucose

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
