+++
title = "Significance level"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The probability of a [Type 1 error]({{< relref "type_i_error" >}})


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Explain the t-test and the factors that influence t-values]({{< relref "explain_the_t_test_and_the_factors_that_influence_t_values" >}}) {#explain-the-t-test-and-the-factors-that-influence-t-values--explain-the-t-test-and-the-factors-that-influence-t-values-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > T-test > One sample t-test**

    T-value can be classified according to a table that lists t-values and corresponding quantities **based on number of [degrees of freedom]({{< relref "degrees_of_freedom" >}})** and **[significance level]({{< relref "significance_level" >}})**

    ---


#### [Explain the meaning of a p-value less than 0.05]({{< relref "explain_the_meaning_of_a_p_value_less_than_0_05" >}}) {#explain-the-meaning-of-a-p-value-less-than-0-dot-05--explain-the-meaning-of-a-p-value-less-than-0-05-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > P-value**

    **It is not possible to prove [H<sub>1</sub>]({{< relref "alternative_hypothesis" >}}) true**, but having a p-value lower than [significance level]({{< relref "significance_level" >}}) -> very unlikely that [H<sub>0</sub>]({{< relref "null_hypothesis" >}}) is correct

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
