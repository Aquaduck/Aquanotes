+++
title = "G-protein GTPase"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Discuss the monomeric GTPases and how they are regulated, with specific reference to Ras.]({{< relref "discuss_the_monomeric_gtpases_and_how_they_are_regulated_with_specific_reference_to_ras" >}}) {#discuss-the-monomeric-gtpases-and-how-they-are-regulated-with-specific-reference-to-ras-dot--discuss-the-monomeric-gtpases-and-how-they-are-regulated-with-specific-reference-to-ras-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  The Monomeric [GTPases]({{< relref "g_protein_gtpase" >}})

        -   Small molecular weight GTPases analogous to the α-subunit of the trimeric G-protein family
        -   Located in cytoplasm of cell
        -   Has a wide variety of functions


#### [Describe the several mechanisms involved in turning off the cAMP second messenger system and why the off signal is important.]({{< relref "describe_the_several_mechanisms_involved_in_turning_off_the_camp_second_messenger_system_and_why_the_off_signal_is_important" >}}) {#describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot--describe-the-several-mechanisms-involved-in-turning-off-the-camp-second-messenger-system-and-why-the-off-signal-is-important-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Off Signals > Several pathways in the cell to turn off a signal:**

    <!--list-separator-->

    -  Inactivating transducer - G-protein [GTPase]({{< relref "g_protein_gtpase" >}})

        -   Gα-subunit is a "GTPase time bomb"
        -   **GTPase activity increases shortly after G-protein activation**
            -   Essentially, shortly after activation steps have already been set in motion to inactivate the [G-protein]({{< relref "g_protein" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
