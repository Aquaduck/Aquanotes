+++
title = "Cell Signalling III - Underlying Design Features"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Death of Monotheism {#death-of-monotheism}


### Complications: {#complications}


#### A specific hormone can exert its effects through more than one receptor type {#a-specific-hormone-can-exert-its-effects-through-more-than-one-receptor-type}

-   Catecholamine hormones of the ANS: _epinephrine_ and _norepinephrine_


#### A single receptor type can activate multiple pathways {#a-single-receptor-type-can-activate-multiple-pathways}

-   Corollary: **different hormones can act on the same receptor**


### Biased ligands {#biased-ligands}

-   _Biased ligands_ engage subsets of a receptor's normal repertoire
-   Have **selective** activation or inactivation of subsets of signaling pathways
-   _Ligand bias_ is an intrinsic property of the ligand-receptor complex


## AM vs FM signaling {#am-vs-fm-signaling}


### The frequency of increases in [ligand] is a critical variable {#the-frequency-of-increases-in-ligand-is-a-critical-variable}

-   Initially believed that it was only absolute concentration
-   **If there are spikes in concentration more frequently, the activity of the affected enzyme will increase**

{{< figure src="/ox-hugo/_20210830_222526Screenshot 2021-08-30 202632.png" width="500" >}}


### Macromolecule complexes {#macromolecule-complexes}

-   Signal transduction depends on _multi-protein complexes_
-   In the case of G-proteins, these involve large assemblies of receptors, G-protein complexes and G-protein targets (e.g. cAMP)
    -   **Remain assembled throughout the activation/deactivation cycle**
-   cAMP is an example of this!


#### cAMP multi-protein complex {#camp-multi-protein-complex}

-   It is known that the enzymes involved in the cAMP signal pathway are **often co-localized even before the signal pathway is activated**
-   One way this is engineered is through the actions of [anchor proteins]({{< relref "anchor_protein" >}}) - in this case, [A-Kinase Anchor Protein]({{< relref "a_kinase_anchor_protein" >}}) (AKAP)


#### [MAP Kinase]({{< relref "map_kinase" >}}) cascade complex {#map-kinase--map-kinase-dot-md--cascade-complex}

-   Another example, will be elaborated on later


#### [Pyruvate DH]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dh--pyruvate-dehydrogenase-complex-dot-md}

-   _E1_: 8 trimers of lipoamide reductase-transacetylase
-   _E2_: 6 dimers of dihydrolipoyl DH
-   _E3_: 12 dimers of pyruvate decarboxylase

{{< figure src="/ox-hugo/_20210830_222433Screenshot 2021-08-30 221729.png" width="700" >}}


#### Trimeric G-Proteins {#trimeric-g-proteins}

-   Macromolecular complexes of signal molecules **do not need to be preformed** - they can also be **created on demand**
-   Again evident in the cAMP signaling pathway in the receptor -> G-protein -> cyclase step
    -   Re-assembly/reconfiguration process dependent on a signal molecule binding and setting in motion the activation process

        {{< figure src="/ox-hugo/_20210830_222926Screenshot 2021-08-30 222848.png" width="900" >}}


#### Lipid rafts {#lipid-rafts}

-   Lipid rafts allow the grouping of key components of intracellular signal cascades
-   An example of this is [insulin]({{< relref "insulin" >}}) signaling

    {{< figure src="/ox-hugo/_20210830_223135Screenshot 2021-08-30 223104.png" width="700" >}}


## Location {#location}


### Organellar {#organellar}

-   Specific pathways usually occur within a particular subcellular compartment
-   The location of a pathway is defined by the location of the enzymes in that pathway
    -   e.g. glycolysis in cytosol and gluconeogenesis in mitochondria


### Cell signaling {#cell-signaling}


#### [Signalosomes]({{< relref "signalosome" >}}) {#signalosomes--signalosome-dot-md}

-   Assure that signaling molecules/enzymes encounter the **correct substrates in the right place at the right time**
-   Increase the local concentration of signal molecules, enzymes, substrates -> **↑ efficiency**
-   Signal molecule/enzyme segregation in a manner that prevents indiscriminate crosstalk/promiscuity/ectopic effects -> **specificity**


### Functional intracellular compartments {#functional-intracellular-compartments}

-   How?
    -   The site of manufacturing
        -   e.g. mRNA in nucleus
    -   Homing
        -   Molecules can be directed to specific subcellular locations
        -   e.g. chaperone proteins, polarized active transport
    -   Anchorage/entrapment
    -   Removal by uptake or degradation
    -   Macromolecular complexes


### cAMP signaling microdomains - lessons from heart failure {#camp-signaling-microdomains-lessons-from-heart-failure}


#### Dilemma {#dilemma}

-   Human heart has both β<sub>1</sub>AR and β<sub>2</sub>AR
    -   AR = _Adrenergic receptor_
    -   Both respond to the same Adrenergic hormones: epinephrine and norepinephrine
    -   **Both** utilize the **same cAMP pathway**
    -   Lead to **different responses**


#### Solution {#solution}

-   β<sub>1</sub>AR and β<sub>2</sub>AR are **differentially distributed** on the cardiomyocyte surface
    -   In **healthy** cell, β<sub>1</sub>AR is uniformly distributed across the cell surface
        -   β<sub>2</sub>AR are located **only in the T-tubules that invaginate into the cardiac muscle cell**
            -   Part of mechanism by which calcium enters the cardiac muscle to cause contraction


#### Heart failure {#heart-failure}

-   This restricted distribution is disrupted and **both** receptors are now found more generally across the cell surface
    -   Activation -> formation of cAMP in regions where it does not belong -> apoptosis


### Membrane fluidity and cell signaling {#membrane-fluidity-and-cell-signaling}

-   Various elements of a cell-surface signaling mechanism must migrate in the fluid mosaic plane of the plasma membrane -> allows second messenger system to work


## Specificity {#specificity}


### Receptor {#receptor}

-   Presence of receptor


### Response elements {#response-elements}

-   Whether or not the required second messenger system is present
-   More generally, **which signaling pathways are present**
    -   Some cells simply do not contain all possible signal pathways


### Location {#location}

-   Location and existence of signal micro-domains ([signalosomes]({{< relref "signalosome" >}}))
-   All activated receptors do not necessarily have spatial access to signal pathways that they otherwise would activate
    -   e.g. Heart example mentioned above


### Biased ligands {#biased-ligands}

-   Mentioned before


### Cross-talk/context/convergence {#cross-talk-context-convergence}

{{< figure src="/ox-hugo/_20210830_225859Screenshot 2021-08-30 225742.png" caption="Figure 1: The three pathways EACH are necessary but NOT sufficient - ALL inputs are required for activation" width="600" >}}


## Surface Receptor Regulation {#surface-receptor-regulation}

-   _Sensitivity_ - the amount of hormone required for half-maximal effect
    -   Moving graph left or right
    -   Moving graph up or down affects _maximal cell response_
-   Why would extra/spare receptors be useful?
    -   Remember: **only a small amount of receptors need to be occupied to get maximal response**
    -   If receptor number increases, equation on graph is shifted to the right -> **it takes less hormone to form hormone-receptor complexes** -> the cell is **more sensitive**

{{< figure src="/ox-hugo/_20210830_231629Screenshot 2021-08-30 231554.png" width="500" >}}

-   In summary, the existence of extra surface receptor capacity **provides a mechanism for adjusting sensitivity while retaining the capacity for maximal response**
    -   In order for this to be effective in physiologic regulation, one must be normally operating in the middle of a dose-response curve
        -   Allows adjustment of increased and decreased sensitivity


### What causes receptor number to change physiologically? {#what-causes-receptor-number-to-change-physiologically}


#### Homologous receptor regulation {#homologous-receptor-regulation}

-   A hormone (e.g. epinephrine or insulin) is **regulating its own receptors in a negative fashion**
    -   High levels of hormone -> loss of receptor function
    -   Low levels of hormone -> gain in receptor function
    -   **Reciprocal relationship between hormone concentration and receptor function**
    -   Called _down-regulation_, _densensitization_, or _uncoupling_
-   Why does this occur?
    -   **Good signal mechanisms must be turned on and off rapidly** -> this reduces the error signal and makes signal more level
-   Takeaway: **never make huge jumps in dosages of hormones and drugs**

<!--list-separator-->

-  How is this accomplished?

    -   As a physiologic response is being generated, there is an **internal feedback loop**
        -   e.g. PKA is activated and immediately turns signal off by phosphorylating receptor
        -   Happens in milliseconds
    -   You can **move receptor to a different location on the plasma membrane** -> separated from second messenger system
        -   Takes longer than direct receptor modification
    -   You can **internalize receptors into the cell**
        -   e.g. LDL receptor
        -   Takes hours


#### Heterologous receptor regulation {#heterologous-receptor-regulation}

-   **One hormone acts on another**
-   An excellent way to develop **timed hormone signal cascades**


## Backlinks {#backlinks}


### 9 linked references {#9-linked-references}


#### [Explain the concept of biased ligands.]({{< relref "explain_the_concept_of_biased_ligands" >}}) {#explain-the-concept-of-biased-ligands-dot--explain-the-concept-of-biased-ligands-dot-md}

from [Biased ligands]({{< relref "cell_signalling_iii_underlying_design_features" >}})

---


#### [Discuss the concepts that individual surface-acting hormones can act on multiple receptors; and can act through multiple signal pathways.]({{< relref "discuss_the_concepts_that_individual_surface_acting_hormones_can_act_on_multiple_receptors_and_can_act_through_multiple_signal_pathways" >}}) {#discuss-the-concepts-that-individual-surface-acting-hormones-can-act-on-multiple-receptors-and-can-act-through-multiple-signal-pathways-dot--discuss-the-concepts-that-individual-surface-acting-hormones-can-act-on-multiple-receptors-and-can-act-through-multiple-signal-pathways-dot-md}

[Death of Monotheism]({{< relref "cell_signalling_iii_underlying_design_features" >}})

---


#### [Discuss the concept of FM (Frequency Modulated) hormone signaling and give an example.]({{< relref "discuss_the_concept_of_fm_frequency_modulated_hormone_signaling_and_give_an_example" >}}) {#discuss-the-concept-of-fm--frequency-modulated--hormone-signaling-and-give-an-example-dot--discuss-the-concept-of-fm-frequency-modulated-hormone-signaling-and-give-an-example-dot-md}

from [AM vs FM signaling]({{< relref "cell_signalling_iii_underlying_design_features" >}})

---


#### [Describe the toolbox of mechanisms proposed to contribute to hormone / signal molecule specificity.]({{< relref "describe_the_toolbox_of_mechanisms_proposed_to_contribute_to_hormone_signal_molecule_specificity" >}}) {#describe-the-toolbox-of-mechanisms-proposed-to-contribute-to-hormone-signal-molecule-specificity-dot--describe-the-toolbox-of-mechanisms-proposed-to-contribute-to-hormone-signal-molecule-specificity-dot-md}

from [Specificity]({{< relref "cell_signalling_iii_underlying_design_features" >}})

---


#### [Describe the role of macromolecular complexes in cell signaling with examples.]({{< relref "describe_the_role_of_macromolecular_complexes_in_cell_signaling_with_examples" >}}) {#describe-the-role-of-macromolecular-complexes-in-cell-signaling-with-examples-dot--describe-the-role-of-macromolecular-complexes-in-cell-signaling-with-examples-dot-md}

from [Location]({{< relref "cell_signalling_iii_underlying_design_features" >}})

---


#### [Describe the importance of location in cell signaling and mechanisms involved in creating signaling domains / “signalosomes”.]({{< relref "describe_the_importance_of_location_in_cell_signaling_and_mechanisms_involved_in_creating_signaling_domains_signalosomes" >}}) {#describe-the-importance-of-location-in-cell-signaling-and-mechanisms-involved-in-creating-signaling-domains-signalosomes-dot--describe-the-importance-of-location-in-cell-signaling-and-mechanisms-involved-in-creating-signaling-domains-signalosomes-dot-md}

from [Cell signaling]({{< relref "cell_signalling_iii_underlying_design_features" >}})

---


#### [Define the role of hormone surface receptor regulation in determining hormone sensitivity and responsiveness.]({{< relref "define_the_role_of_hormone_surface_receptor_regulation_in_determining_hormone_sensitivity_and_responsiveness" >}}) {#define-the-role-of-hormone-surface-receptor-regulation-in-determining-hormone-sensitivity-and-responsiveness-dot--define-the-role-of-hormone-surface-receptor-regulation-in-determining-hormone-sensitivity-and-responsiveness-dot-md}

from [Surface Receptor Regulation]({{< relref "cell_signalling_iii_underlying_design_features" >}})

---


#### [Define homologous receptor regulation, its physiologic significance and the mechanisms proposed to explain the process.]({{< relref "define_homologous_receptor_regulation_its_physiologic_significance_and_the_mechanisms_proposed_to_explain_the_process" >}}) {#define-homologous-receptor-regulation-its-physiologic-significance-and-the-mechanisms-proposed-to-explain-the-process-dot--define-homologous-receptor-regulation-its-physiologic-significance-and-the-mechanisms-proposed-to-explain-the-process-dot-md}

from [Homologous receptor regulation]({{< relref "cell_signalling_iii_underlying_design_features" >}})

---


#### [Define heterologous receptor regulation.]({{< relref "define_heterologous_receptor_regulation" >}}) {#define-heterologous-receptor-regulation-dot--define-heterologous-receptor-regulation-dot-md}

from [Heterologous receptor regulation]({{< relref "cell_signalling_iii_underlying_design_features" >}})

---


### Unlinked references {#unlinked-references}

[Show unlinked references]
