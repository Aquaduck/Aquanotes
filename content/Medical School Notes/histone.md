+++
title = "Histone"
author = ["Arif Ahsan"]
date = 2021-08-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Information Transfer 1: Telomere Replication & Maintenance]({{< relref "information_transfer_1_telomere_replication_maintenance" >}}) {#information-transfer-1-telomere-replication-and-maintenance--information-transfer-1-telomere-replication-maintenance-dot-md}

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Steps in replication**

    [Histones]({{< relref "histone" >}}) must be removed and then replaced during the replication process -> accounts for lower replication rate in [eukaryotes]({{< relref "eukaryote" >}})

    ---

<!--list-separator-->

-  **🔖 DNA Replication in Eukaryotes > Steps in replication**

    [DNA]({{< relref "dna" >}}) is bound to basic proteins known as [histones]({{< relref "histone" >}}) to form stuctures called [nucleosomes]({{< relref "nucleosome" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
