+++
title = "Mechanistic target of rapamycin"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [HIF-1: upstream and downstream of cancer metabolism]({{< relref "hif_1_upstream_and_downstream_of_cancer_metabolism" >}}) {#hif-1-upstream-and-downstream-of-cancer-metabolism--hif-1-upstream-and-downstream-of-cancer-metabolism-dot-md}

<!--list-separator-->

-  **🔖 Genetic and metabolic activators of HIF-1**

    <!--list-separator-->

    -  [mTOR]({{< relref "mechanistic_target_of_rapamycin" >}})

        -   mTOR is a serine-threonine protein kinase that **increases the rate of translation of HIF-1α**
            -   Phosphorylates ribosomal protein S6 kinase and eIF-4E binding protein 1


### Unlinked references {#unlinked-references}

[Show unlinked references]
