+++
title = "Identify nephron anatomy (different parts of nephron)"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

The [Nephron]({{< relref "nephron" >}}) is the functional unit of the kidney composed of the [Renal corpuscle]({{< relref "renal_corpuscle" >}}) and a [Renal tubule]({{< relref "renal_tubule" >}})


## From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}}) {#from-vander-s-renal-physiology--vander-s-renal-physiology-dot-md}


### The [Renal tubule]({{< relref "renal_tubule" >}}) (p. 18) {#the-renal-tubule--renal-tubule-dot-md----p-dot-18}

{{< figure src="/ox-hugo/_20211031_163629screenshot.png" caption="Figure 1: Components of the nephron." >}}

-   The _Renal tubule_ begins at and leads out of the [Bowman capsule]({{< relref "bowman_capsule" >}}) on the side **opposite the vascular pole**
-   Contains segments further divided into subdivisions


#### [Proximal tubule]({{< relref "proximal_tubule" >}}) {#proximal-tubule--proximal-tubule-dot-md}

([Proximal convoluted tubule]({{< relref "proximal_convoluted_tubule" >}}) + [Proximal straight tubule]({{< relref "proximal_straight_tubule" >}}))

-   The first segment of the Renal tubule
-   Drains [Bowman capsule]({{< relref "bowman_capsule" >}})


#### [Descending thin limb of the loop of Henle]({{< relref "descending_thin_limb_of_the_loop_of_henle" >}}) {#descending-thin-limb-of-the-loop-of-henle--descending-thin-limb-of-the-loop-of-henle-dot-md}

-   The descending thin limbs **begin at the same level in all nephrons** - the point where they connect to proximal straight tubule in the [outer medulla]({{< relref "outer_medulla_of_the_kidney" >}})
    -   Marks the border between the oute rand inner stripes of the outer medulla
    -   On the other hand, the **penetrating depth** of each nephron's descending limb **varies**


#### [Ascending thin limb of the loop of Henle]({{< relref "ascending_thin_limb_of_the_loop_of_henle" >}}) {#ascending-thin-limb-of-the-loop-of-henle--ascending-thin-limb-of-the-loop-of-henle-dot-md}

-   Begins at the abrupt hairpin turn from the descending thin limb
-   In long loops (deeply penetrated), epithelium of the first portion of the ascending limb remains thin **but has a different function than the descending limb**


#### [Ascending thick limb of the loop of Henle]({{< relref "ascending_thick_limb_of_the_loop_of_henle" >}}) {#ascending-thick-limb-of-the-loop-of-henle--ascending-thick-limb-of-the-loop-of-henle-dot-md}

-   Epithelium thickens
-   In short loops, the abrupt hairpin turn from the descending thin limb leads **directly** to the ascending thick limb
-   **All thick ascending limbs begin at the same level**
    -   Marks the border between the [inner]({{< relref "inner_medulla_of_the_kidney" >}}) and [outer medulla]({{< relref "outer_medulla_of_the_kidney" >}})
-   Each thick ascending limb rises back into the cortex to the [Bowman's capsule]({{< relref "bowman_capsule" >}})
    -   Passes directly between the [afferent]({{< relref "afferent_arteriole_of_the_kidney" >}}) and [efferent]({{< relref "efferent_arteriole_of_the_kidney" >}}) arterioles at the vascular pole
        -   [Macula densa]({{< relref "macula_densa" >}}) cells found at this point between the two arterioles and marks the end of the ascending thick limb of the loop of Henle


#### [Distal convoluted tubule]({{< relref "distal_convoluted_tubule" >}}) {#distal-convoluted-tubule--distal-convoluted-tubule-dot-md}

-   Marked by the [Macula densa]({{< relref "macula_densa" >}}) cells at the vascular pole of the Bowman's capsule
-   Followed by the [connecting tubule]({{< relref "connecting_tubule" >}}) -> initial collecting tubule of the cortical collecting duct


#### [Cortical collecting duct]({{< relref "cortical_collecting_duct" >}}) {#cortical-collecting-duct--cortical-collecting-duct-dot-md}

-   First part is the _initial collecting tubule_
-   Connecting tubules from **several nephrons merge** to form a given cortical collecting duct
-   All cortical collecting ducks then run downward -> enter the medulla -> become [Outer medullary collecting duct]({{< relref "outer_medullary_collecting_duct" >}}) -> become [Inner medullary collecting duct]({{< relref "inner_medullary_collecting_duct" >}}) -> _papillary collecting ducts_ empties into a [calyx]({{< relref "renal_calyx" >}}) of the [Renal pelvis]({{< relref "renal_pelvis" >}})
    -   Each [renal calyx]({{< relref "renal_calyx" >}}) is continuous with the [ureter]({{< relref "ureter" >}})
        -   **Urine is not altered after it enters a [calyx]({{< relref "renal_calyx" >}})**


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > Renal Physiology: Glomerular Filtration > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
