+++
title = "GLUT4 Exocytosis"
author = ["Arif Ahsan"]
date = 2021-08-30T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Notes {#notes}


### [GLUT-4]({{< relref "glut_4" >}}) resides in specialised vesicles called [GSVs]({{< relref "glut4_storage_vesicles" >}}) that undergo insulin-dependent translocation to the plasma membrane {#glut-4--glut-4-dot-md--resides-in-specialised-vesicles-called-gsvs--glut4-storage-vesicles-dot-md--that-undergo-insulin-dependent-translocation-to-the-plasma-membrane}

-   This happens in the absense of [insulin]({{< relref "insulin" >}})


### There is evidence that [GSVs]({{< relref "glut4_storage_vesicles" >}}) directly fuse with the plasma membrane, but this occurs as a transient burst -> further effort needed to confirm whether GLUT-4 continues to recycle back to plasma membrane in GSVs or in endosomes when [insulin]({{< relref "insulin" >}}) is present {#there-is-evidence-that-gsvs--glut4-storage-vesicles-dot-md--directly-fuse-with-the-plasma-membrane-but-this-occurs-as-a-transient-burst-further-effort-needed-to-confirm-whether-glut-4-continues-to-recycle-back-to-plasma-membrane-in-gsvs-or-in-endosomes-when-insulin--insulin-dot-md--is-present}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the intercellular response to insulin-induced cellular signaling]({{< relref "describe_the_intercellular_response_to_insulin_induced_cellular_signaling" >}}) {#describe-the-intercellular-response-to-insulin-induced-cellular-signaling--describe-the-intercellular-response-to-insulin-induced-cellular-signaling-dot-md}

<!--list-separator-->

-  From [GLUT4 Exocytosis]({{< relref "glut4_exocytosis" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
