+++
title = "Nitric oxide"
author = ["Arif Ahsan"]
date = 2021-09-04T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the role of gases in cell signaling with special emphasis on nitric oxide and its basic mechanism of action.]({{< relref "describe_the_role_of_gases_in_cell_signaling_with_special_emphasis_on_nitric_oxide_and_its_basic_mechanism_of_action" >}}) {#describe-the-role-of-gases-in-cell-signaling-with-special-emphasis-on-nitric-oxide-and-its-basic-mechanism-of-action-dot--describe-the-role-of-gases-in-cell-signaling-with-special-emphasis-on-nitric-oxide-and-its-basic-mechanism-of-action-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways**

    <!--list-separator-->

    -  Gases as second messengers - [NO]({{< relref "nitric_oxide" >}})

        -   NO is synthesized by certain cells (e.g. epithelial) from arginine
        -   Three (of many) physiological responses to elevated levels of NO:
            1.  Smooth muscle relaxation
            2.  Neurotransmission in CNS
            3.  Cell-mediated immune response

        <!--list-separator-->

        -  Mechanism of action

            1.  Signal binds to receptor
            2.  Receptor activates [NO synthase]({{< relref "nitric_oxide_synthase" >}}) -> synthesizes NO
            3.  NO diffuses to neighboring cells across membranes
            4.  NO binds to _guanylyl cyclase_ -> activation of cGMP pathway
            5.  Triggers smooth muscle relaxation


### Unlinked references {#unlinked-references}

[Show unlinked references]
