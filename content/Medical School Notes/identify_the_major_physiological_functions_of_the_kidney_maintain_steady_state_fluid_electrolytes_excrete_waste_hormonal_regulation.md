+++
title = "Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}


### Major physiological functions of the [kidney]({{< relref "kidney" >}}) {#major-physiological-functions-of-the-kidney--kidney-dot-md}


#### Production of [Urine]({{< relref "urine" >}}) {#production-of-urine--urine-dot-md}

-   **Excretion** of metabolic waste and end-products of [metabolism]({{< relref "metabolism" >}})
-   **Regulation** of extracellular fluid volume and [osmolality]({{< relref "osmolality" >}})
-   **Maintenance** of acid-base balance
-   **Maintenance** of [electrolyte]({{< relref "electrolyte" >}}) concentrations
-   **Regulation** of blood pressure and blood volume
-   Participation in **[gluconeogenesis]({{< relref "gluconeogenesis" >}})** ([glutamine]({{< relref "glutamine" >}}) and [glutamate]({{< relref "glutamate" >}})) and **[ketogenesis]({{< relref "ketogenesis" >}})**


#### [Hormone]({{< relref "hormone" >}}) synthesis {#hormone--hormone-dot-md--synthesis}

-   [Erythropoietin]({{< relref "erythropoietin" >}})
-   [Calciferol]({{< relref "vitamin_d" >}})
-   [Prostaglandins]({{< relref "prostaglandin" >}})
-   [Dopamine]({{< relref "dopamine" >}})
-   [Renin]({{< relref "renin" >}}) **<- focus mostly on this one in class**


## From [Vander's Renal Physiology]({{< relref "vander_s_renal_physiology" >}}) {#from-vander-s-renal-physiology--vander-s-renal-physiology-dot-md}


### [Renal]({{< relref "kidney" >}}) functions (p. 10) {#renal--kidney-dot-md--functions--p-dot-10}


#### Excretion of metabolic waste and foreign substances {#excretion-of-metabolic-waste-and-foreign-substances}


#### Regulation of [water]({{< relref "water" >}}) and electrolyte balance {#regulation-of-water--water-dot-md--and-electrolyte-balance}


#### Regulation of extracellular fluid volume {#regulation-of-extracellular-fluid-volume}


#### Regulation of [plasma]({{< relref "plasma" >}}) osmolality {#regulation-of-plasma--plasma-dot-md--osmolality}


#### Regulation of [RBC]({{< relref "red_blood_cell" >}}) production {#regulation-of-rbc--red-blood-cell-dot-md--production}


#### Regulation of vascular resistance {#regulation-of-vascular-resistance}


#### Regulation of acid-base balance {#regulation-of-acid-base-balance}


#### Regulation of vitamin D production {#regulation-of-vitamin-d-production}


#### Gluconeogenesis {#gluconeogenesis}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 5 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-11-01 Mon] </span></span> > Renal Physiology: Glomerular Filtration > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
