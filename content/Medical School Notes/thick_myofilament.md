+++
title = "Thick myofilament"
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Made of [myosin]({{< relref "myosin" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the structural components of skeletal muscle from the level of the whole muscle to the level of a single sarcomere.]({{< relref "describe_the_structural_components_of_skeletal_muscle_from_the_level_of_the_whole_muscle_to_the_level_of_a_single_sarcomere" >}}) {#describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot--describe-the-structural-components-of-skeletal-muscle-from-the-level-of-the-whole-muscle-to-the-level-of-a-single-sarcomere-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology > Structure of Skeletal Muscle (p. 109) > Skeletal muscle cells > Myofibril**

    Composed of longitudinally arranged, cylindrical bundles of [thick]({{< relref "thick_myofilament" >}}) and [thin myofilaments]({{< relref "thin_myofilament" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
