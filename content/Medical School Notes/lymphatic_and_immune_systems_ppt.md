+++
title = "Lymphatic and Immune Systems PPT"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 7 linked references {#7-linked-references}


#### [Name and describe the pathways of lymphatic drainage (in a lymph node, in the body).]({{< relref "name_and_describe_the_pathways_of_lymphatic_drainage_in_a_lymph_node_in_the_body" >}}) {#name-and-describe-the-pathways-of-lymphatic-drainage--in-a-lymph-node-in-the-body--dot--name-and-describe-the-pathways-of-lymphatic-drainage-in-a-lymph-node-in-the-body-dot-md}

<!--list-separator-->

-  From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}})

    <!--list-separator-->

    -  [Lymph]({{< relref "lymph" >}})

        -   Lymph gradually moves **centrally** in [lymphatic channels]({{< relref "lymphatic_channel" >}}) where it flows into the [vascular system]({{< relref "vascular_system" >}}) at the **base of the neck**
            -   Forward movement occurs with pressures created by macroscopic movements of the extremities aided by small luminal valves in lymphatic channels
        -   Lymph is filtered through groups of [lymph nodes]({{< relref "lymph_node" >}}) as it moves centrally
        -   The absence of lymph flow can be detected by swelling of the feet if a person stands motionless for a long period of time
        -   Fluid accumulation, clinically called [edema]({{< relref "edema" >}}), is a frequent syndrome in patients with **cardiovascular, renal, and protein abnormalities**
        -   Daily lymph flow is about 4-5 liters


#### [List the functions of lymphatic drainage.]({{< relref "list_the_functions_of_lymphatic_drainage" >}}) {#list-the-functions-of-lymphatic-drainage-dot--list-the-functions-of-lymphatic-drainage-dot-md}

<!--list-separator-->

-  From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}})

    <!--list-separator-->

    -  Functions of [lymphatic]({{< relref "lymph" >}}) flow

        -   Slow movement of lymph -> **filter out any foreign or undesirable material that may have inadvertently reached the interstitial fluid**
            -   This filtering action is performed by [lymph nodes]({{< relref "lymph_node" >}}) - the **major sites of activation of the adaptive immune system**
        -   The arrangement of lymph and [lymph nodes]({{< relref "lymph_node" >}}) is designed to **activate the immune system before antigens and invading organisms reach the blood**
        -   An additional function unrelated to immunity is the **transport of [lipoproteins]({{< relref "lipoprotein" >}}) absored in the G.I. tract**, which **exclusively enter the blood via lymphatics**


#### [List and describe the major morphologic regions of a lymph node.]({{< relref "list_and_describe_the_major_morphologic_regions_of_a_lymph_node" >}}) {#list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot--list-and-describe-the-major-morphologic-regions-of-a-lymph-node-dot-md}

<!--list-separator-->

-  From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}})

    <!--list-separator-->

    -  [Lymphatic]({{< relref "lymph" >}}) anatomy

        -   Only a few named lymphatic vessels
        -   All lymph from **below the diaphragm** and **on the left side** passes through the _thoracic duct_ to enter the vascular system at the **bifurcation of the left internal juglar and subclavian veins**
        -   Lymph from **upper right side** enters through the **right lymphatic duct** at the same junction on the right
        -   Lymph from **the GI tract** joins that from the lower extremities in the [Chyle cistern]({{< relref "chyle_cistern" >}}) -> enters thoracic duct

        {{< figure src="/ox-hugo/_20211009_180548screenshot.png" width="600" >}}

    <!--list-separator-->

    -  [Lymph nodes]({{< relref "lymph_node" >}})

        -   Small kidney-bean shaped structures normally a few mm in size, but capable of rapidly growing to centimeters during an infection
        -   Occur in groups along the lymphatics
            -   Lymph flows through more than one lymph node on its journey back to the blood
        -   **Lymph nodes are the major site of the Basic Stages 2 and 3 of the [lymphocyte's]({{< relref "lymphocyte" >}}) life cycle** (search for its cognate antigen and activation)

        <!--list-separator-->

        -  Parts of the lymph node:

            {{< figure src="/ox-hugo/_20211009_220911screenshot.png" caption="Figure 1: Anatomy of a lymph node" width="700" >}}

            -   There is a distinctive _hilum_ (circled) where a small artery enters and a vein exits (blood vessels not illustrated)
                -   The _efferent lymphatic_ (10) also exits here
                -   This hilum occupies only a very small percentage of the lymph node surface
            -   The remainder of the surface has a thin _capsule_ (7) of connective tissue with _trabeculae_ (6) that penetrate inward
            -   Lymph enters through _afferent lymphatics_ (1) that perforate the capsule
                -   There is a circumferential _marginal sinus_ (aka subcapsular sinus - 8) and _trabecular sinuses_ that parlalel the trabeculae and lead inwards to a labyrinth of _medullary sinuses_ (4)
            -   The medullary sinuses come together to form the _efferent lymphatic_ (10)
            -   The lymph node is divided into an **outer cortex of follicles and paracortex** and an **inner medulla comprised of medullary sinuses** and the _medullary cords_ (5)
            -   Nodular aggregates of lymphocytes in the cortex are called lymphoid follicles (9)
                -   Follicles consist mostly of [B-lymphocytes]({{< relref "b_lymphocyte" >}})
                -   When activated -> follicle has a **pale germinal center with a dark rim of small resting B-lymphocytes** = _mantle zone_
            -   The paracortex (3) is the part of the cortex without follicles
                -   Comprised of [T-lymphocytes]({{< relref "t_lymphocyte" >}}) with scattered [APCs]({{< relref "antigen_presenting_cell" >}})

            -   _Interdigitating dendritic cells_ are a type of APC that are sprinkled among the T-lymphocytes of the paracortex
            -   [Histiocytes]({{< relref "histiocyte" >}}) (macrophages) mostly reside in the sinuses
                -   Histiocytes filter antigen and debris from the sinuses and may migrate to the paracortex -> differentiate into APCs
            -   Medullary cords contain numerous plasma cells and lymphocytes preparing to leave the lymph node
            -   [High endothelial venule]({{< relref "high_endothelial_venule" >}}) (HEV), a small post-capillary vein (i.e. venule) in the paracortex, is where [lymphocytes]({{< relref "lymphocyte" >}}) move from blood into the lymph node
            -   The overall structure of a lymph node is formed by [reticulin fibers]({{< relref "reticulin_fiber" >}}) - a thin/fine type of collagen that allows easy movement of immune cells


#### [Describe the structure of the thymus (include changes with age.)]({{< relref "describe_the_structure_of_the_thymus_include_changes_with_age" >}}) {#describe-the-structure-of-the-thymus--include-changes-with-age-dot----describe-the-structure-of-the-thymus-include-changes-with-age-dot-md}

<!--list-separator-->

-  From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}})

    <!--list-separator-->

    -  [Thymus]({{< relref "thymus" >}})

        -   The function of the thymus is to support [T-lymphocyte]({{< relref "t_lymphocyte" >}}) development (Stage 1), but remove autoreactive T-cells
        -   The thymus has a central, pale, [multilobate medulla]({{< relref "medulla_of_the_thymus" >}}) covered by a dark [cortex]({{< relref "cortex_of_the_thymus" >}})
            -   There is a dense CT capsule, but **no lymphatics or sinuses**
        -   Small but easily identifiable in the **central, anterior chest** during the first 10-12 years of life
            -   The thymus involutes in adults and is mostly replaced with [adipose cells]({{< relref "adipose_cell" >}}) -> makes it difficult to locate despite remaining active
        -   The thymus **does not contain B-cell follicles**

        <!--list-separator-->

        -  [Thymus epithelium]({{< relref "epithelium_of_the_thymus" >}})

            -   Unlike most epithelium, thymic epithelium **loses its connection with the surface endoderm**
            -   Thymus epithelium forms a blood/thymic barrier
                -   A basement membrane separates the thymus from its capsule and from the blood vessels within the thymus
            -   Internal part of the thymus is a sponge-like network of epithelial cells filled with [lymphocytes]({{< relref "lymphocyte" >}})
            -   Epithelial cells in the [Cortex of the thymus]({{< relref "cortex_of_the_thymus" >}}) assist precursor T-cells to rearrange their [TCR]({{< relref "t_cell_receptor" >}}) genes and become naive T-cells
                -   In the [Medulla of the thymus]({{< relref "medulla_of_the_thymus" >}}) epithelial cells direct elimination of autoreactive lymphocytes and help [T-cell]({{< relref "t_lymphocyte" >}}) differentiation into helper or suppressor cells
            -   Thymus epithelial cells form **broad loose sheets of cells** (with desmosomes) that are **heavily infiltrated by [lymphocytes]({{< relref "lymphocyte" >}}) in between the epithelial cells**

        <!--list-separator-->

        -  [Cortex of the thymus]({{< relref "cortex_of_the_thymus" >}})

            {{< figure src="/ox-hugo/_20211009_165706screenshot.png" caption="Figure 2: Cortex of the thymus" width="500" >}}

            -   Many more [lymphocytes]({{< relref "lymphocyte" >}}) than epithelial cells
            -   [T-lymphocytes]({{< relref "t_lymphocyte" >}}) are rearranging their [TCR]({{< relref "t_cell_receptor" >}})
            -   Many of the lymphocytes are unsuccessful -> die and are engulfed by [macrophages]({{< relref "macrophage" >}})
                -   Seen as **white holes in the dark cortex**

        <!--list-separator-->

        -  [Medulla of the thymus]({{< relref "medulla_of_the_thymus" >}})

            {{< figure src="/ox-hugo/_20211009_165743screenshot.png" caption="Figure 3: Medulla of the thymus (and a bit of cortex and capsule in the upper right)" width="700" >}}

            -   Epithelial cells are more prominant than in the cortex
                -   Cause the pallor
            -   Function of the [epithelial cells]({{< relref "epithelium_of_the_thymus" >}}):
                -   Test the new [T-cells]({{< relref "t_lymphocyte" >}}) for self-reactivity and HLA molecule recognition
                    -   Eliminate those with innapropriate reactivity
                -   Help [T-lymphocytes]({{< relref "t_lymphocyte" >}}) develop into helper or suppressor cells
            -   Some medullary epithelial cells form [Hassall's corpuscles]({{< relref "hassall_s_corpuscle" >}})


#### [Describe the structure (especially blood flow) and function of the spleen.]({{< relref "describe_the_structure_especially_blood_flow_and_function_of_the_spleen" >}}) {#describe-the-structure--especially-blood-flow--and-function-of-the-spleen-dot--describe-the-structure-especially-blood-flow-and-function-of-the-spleen-dot-md}

<!--list-separator-->

-  From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}})

    <!--list-separator-->

    -  [Spleen]({{< relref "spleen" >}})

        -   The spleen is a filter of blood
        -   The spleen has a kidney bean shape and a well-defined CT capsule with CT trabeculae projecting outwards from a central hilum
            -   There are efferent, but **no afferent lymphatics**
        -   The splenic parenchyma contains two interspersed components: [red pulp]({{< relref "red_pulp" >}}) and [white pulp]({{< relref "white_pulp" >}}) (named for their fresh unstained color)
            -   Red pulp is rich in blood
            -   White pulp is mostly lymphocytes
        -   Surrounding central arterioles in the spleen are an aggregate of [T-lymphocytes]({{< relref "t_lymphocyte" >}}) called a [periarteriole lymphoid sheath]({{< relref "periarteriole_lymphoid_sheath" >}}) (PALS)

        <!--list-separator-->

        -  Splenic function

            -   [White pulp]({{< relref "white_pulp" >}}) responds to bloodborne antigens similarly to how the lymph nodes respond to antigen in the lymph
            -   The [red pulp]({{< relref "red_pulp" >}}) filters and digests "particulate" material in the blood
                -   Ranges from unwanted or damaged proteins to senescent cells (especially aged [RBCs]({{< relref "red_blood_cell" >}}))
                -   There is a steady state need to remove damaged cells and protein complexes from the blood
                -   The filtration is accomplished by a unique splenic circulation called an _open circulation_

        <!--list-separator-->

        -  Spleen filtration

            -   _Closed vascular system_: blood flows from the arterial side through the capillaries to the venous side, all while enclosed by an endothelium
            -   _Open vascular system_: smallest vessels are blind-ended capillaries ([sheathed capillaries]({{< relref "sheathed_capillary" >}})) without an endothelium -> the ends are instead surrounded by aggregates of [histiocytes]({{< relref "histiocyte" >}})
                -   Sheathed capillaries dump blood into the interstitial tissues of the spleen - called [splenic cords]({{< relref "splenic_cords" >}}) (aka Cords of Billroth)
            -   Senescent cells and material are removed as they attempt to navigate the [macrophages]({{< relref "macrophage" >}}) of the [sheath]({{< relref "sheathed_capillary" >}}) and [cords]({{< relref "splenic_cords" >}}) -> squeeze between endothelial cells to return to the vascular system in the sinuses of the [red pulp]({{< relref "red_pulp" >}})

        <!--list-separator-->

        -  Splenic flow

            {{< figure src="/ox-hugo/_20211010_173004screenshot.png" caption="Figure 4: Blood flow through the spleen" width="700" >}}


#### [Describe the major actions occurring in these regions (with respect to lymph flow and lymphocyte lifecycle – especially paracotex and cortex.)]({{< relref "describe_the_major_actions_occurring_in_these_regions_with_respect_to_lymph_flow_and_lymphocyte_lifecycle_especially_paracotex_and_cortex" >}}) {#describe-the-major-actions-occurring-in-these-regions--with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot----describe-the-major-actions-occurring-in-these-regions-with-respect-to-lymph-flow-and-lymphocyte-lifecycle-especially-paracotex-and-cortex-dot-md}

<!--list-separator-->

-  From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}})

    <!--list-separator-->

    -  [Paracortex]({{< relref "paracortex_of_the_lymph_node" >}})

        -   [Dendritic cells]({{< relref "dendritic_cell" >}}) and antigen enter the node throug the afferent lymphatics
        -   [Lymphocytes]({{< relref "lymphocyte" >}}) enter through the [HEV]({{< relref "high_endothelial_venule" >}})
        -   These two cell types interact -> testing to see if any [T-cells]({{< relref "t_lymphocyte" >}}) recognize the peptide displayed by the [dendritic cells]({{< relref "dendritic_cell" >}})
            -   If the T-cell **fails to recognize** any antigen peptide -> enters a sinus and exits through the efferent lymphatic -> returns to blood
                -   A lymphocyte may complete this cycle as often as 10 times per day in nodes throughout the body
            -   If a T-cell **recognizes** a peptide while in the paracortex -> enters activation stage -> divides about 10 times -> becomes an effector cell
                -   Specific function depends on the type of T-cell and signals from the innate system

    <!--list-separator-->

    -  [Follicles]({{< relref "follicle_of_the_lymph_node" >}})

        -   [B-cells]({{< relref "b_lymphocyte" >}}) waiting to recognize antigen flowing in the lymph are located in the cortex in [primary follicles]({{< relref "primary_follicle" >}})
            -   B-cells engulf antigen that binds their receptor -> display peptide form the antigen on their surface
                -   If a previously activated [T-cell]({{< relref "t_lymphocyte" >}}) from the paracortex recognizes the peptide -> T-cell helps B-cell form a [germinal center]({{< relref "germinal_center" >}}) (i.e. [secondary follicle]({{< relref "secondary_follicle" >}})) and try to **make a more specific antibody through hypermutation of the Ig gene**
                -   Unsuccessful B-cells undergo apoptosis
                -   B-cells producing a highly specific [Ig]({{< relref "immunoglobulin" >}}) become plasma cells -> leave lymph node or enter medullary cords -> produce abundant Ig


#### [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}}) {#describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte--e-dot-g-dot-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot----describe-the-basic-stages-and-events-in-the-life-of-a-t-and-b-lymphocyte-e-g-the-summary-given-at-the-end-of-the-introduction-of-the-immune-system-dot-md}

<!--list-separator-->

-  From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}})

    <!--list-separator-->

    -  Two arms of the immune system

        <!--list-separator-->

        -  [Innate system]({{< relref "innate_immunity" >}})

            -   Comprised of cells and proteins
            -   The cells (neutrophils and monocytes/macrophages) can engulf and kill bacteria with the help of a large number of plasma protein and receptor molecules encoded in DNA
                -   These are synthesized by the [liver]({{< relref "liver" >}})

        <!--list-separator-->

        -  [Adaptive system]({{< relref "adaptive_immunity" >}})

            -   Generates two families of receptors with unique antigen specifities through **DNA mutation** (adaption\*)
                -   **Greater specificity and efficacy** than the recepters of the [innate system]({{< relref "innate_immunity" >}})
            -   The cells (B and T lymphocytes) have **memory** that allows a more rapid and effective response to **re-infection**

    <!--list-separator-->

    -  Activation and actions - T and B lymphocytes

        -   All resting [lymphocytes]({{< relref "lymphocyte" >}}) are indistinguishable morphologically
            -   Sometimes identifiable based on context in tissue
            -   Upon activation, they may dramatically change their morphology

        <!--list-separator-->

        -  [T lymphocytes]({{< relref "t_lymphocyte" >}})

            -   Activated by **recognition of a partially-digested foreign peptide displayed by [antigen presenting cells]({{< relref "antigen_presenting_cell" >}})**
                -   APCs are special phagocytic cells (similar to [macrophages]({{< relref "macrophage" >}})) that sample interstitial fluid and present partially digested peptides to T lymphocytes
            -   T cells have several effector actions
                1.  Assist activated [B-cells]({{< relref "b_lymphocyte" >}}) to improve binding of their receptor and differentiation into plasma cells
                2.  Enhance the phagocytic actions of [macrophages]({{< relref "macrophage" >}})
                3.  Regulation of the immune reaction
                4.  Directly destroy infected host cells (cytotoxicity)
            -   The first three are performed by [CD4-positive (helper/suppressor) T-cells]({{< relref "cd4_positive_t_lymphocyte" >}}), and the last is done by [CD8-positive (cytotoxic) T-cells]({{< relref "cd8_positive_t_lymphocyte" >}})
                -   CD4 and CD8 are surface molecules that define these T-cell subsets

        <!--list-separator-->

        -  [B lymphocyte]({{< relref "b_lymphocyte" >}})

            -   Activated by **recognition of intact foreign molecules ([antigens]({{< relref "antigen" >}}))**

    <!--list-separator-->

    -  Five basic stages in the life of a [lymphocyte]({{< relref "lymphocyte" >}})

        _Note: not "official" stages_

        -   These are not simply sequential stages
        -   Most lymphocytes do not progress beyond stage 2 because **they are never exposed to the antigen that their receptor recognizes**
        -   Memory cells cycle back to stage 2 and may reenter the activation or effector stages upon re-exposure
        -   Many cells die in stages 1 and 3 and at the conclusion of stage 4
            -   Stages 1 and 3 involved controlled mutation of part of the genome
                -   These mutations are often ineffective or detrimental -> cell death
                -   The cells with beneficial mutations often survive
            -   Effectors in stage 4 are produced in large numbers to ensure an effective response to foreign exposure
                -   These cells are not needed post-exposure, so a minority become memory cells and the remainder is eliminated

        <!--list-separator-->

        -  Basic stages

            1.  Birth and maturation
            2.  Searching for a cognate antigen
            3.  Activation
            4.  Effector (e.g. fighting the threat)
            5.  Memory

    <!--list-separator-->

    -  Summary of actions of B and T lymphocytes

        -   Both [T-cells]({{< relref "t_lymphocyte" >}}) and [B-cells]({{< relref "b_lymphocyte" >}}) derived from [hematopoietic stem cells]({{< relref "hematopoietic_stem_cell" >}}) of the bone marrow
        -   Stage 3 typically occurs in lymph nodes or other permanent lymphoid tissue, but may occur in other tissue (particularly for T cells)
        -   Rearrangement is risky -> many cells die in the process
        -   Most lymphocytes are eliminated when the threat abates, but some memory cells remain

        <!--list-separator-->

        -  [B lymphocytes]({{< relref "b_lymphocyte" >}})

            -   Usually dependent on an activated [T lymphocyte]({{< relref "t_lymphocyte" >}}) to help them become activated

            <!--list-separator-->

            -  Life cycle

                1.  Rearrangement (VDJ recombination) of the BCR (Ig) in bone marrow
                2.  Naive and memory cells circulate and migrate in tissues (especially lymphoid) looking for cognate antigen
                3.  Activate and proliferate in germinal centers where immunoglobulin mutation (somatic hypermutation) improves specificity of the immunoglobulin with T-cell help
                4.  Effector action through plasma cells -> produce highly specific immunoglobulin that enhances actions of innate system
                5.  Many effector cells die after threat abates, some survive as memory cells and return to step 2

        <!--list-separator-->

        -  [T lymphocytes]({{< relref "t_lymphocyte" >}})

            -   T-cell selection in the [thymus]({{< relref "thymus" >}}) reduces the likelihood of turning the immune system on yourself
            -   T-cells are activated **first**

            <!--list-separator-->

            -  Life cycle

                1.  Rearrangement (VDJ recombination) of the TCR in [thymus]({{< relref "thymus" >}}) with extensive selection
                2.  Naive and memory cells circulate and migrate in tissues (especially lymphoid tissue) looking for cognate antigen
                3.  Recognition of peptide antigen presented by APC -> activate and proliferate in paracortex of lymph nodes
                    -   No further mutation of TCR
                4.  Effector actions
                5.  Many effector cells die after threat abates, some survive as memory cells and return to step 2

        <!--list-separator-->

        -  Major actions of T and B lymphocytes table

            |               | Stage 1                                                   | Stage 2                                                       | Stage 3                                                                         | Stage 4                                                                            | Stage 5                                                                                         |
            |---------------|-----------------------------------------------------------|---------------------------------------------------------------|---------------------------------------------------------------------------------|------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------|
            | T-lymphocytes | Birth in bone marrow, recombination of receptor in Thymus | Circulates via blood between lymph nodes actively looking     | Mostly in paracortex of lymph nodes                                             | Depends on specific subtype; may act in lymph nodes or at site of infection        | Many cells of stage 4 die, but some memory cells survive and rejoin Stage 2                     |
            | B-lymphocytes | Birth and VDJ (Ig mutation) recombination in bone marrow  | Mostly in lymphoid tissue (nodes) waiting for cognate antigen | Mostly in lymphoid follicles where there is further mutation of the Ig receptor | Hypermutated B-cells develop into plasma cell in lymph node -> secrete abundant Ig | Memory plasma cells reside for many years in bone marrow; some memory B-cells return to Stage 2 |


### Unlinked references {#unlinked-references}

[Show unlinked references]
