+++
title = "Define the difference between V/Q for a shunt vs. V/Q for a dead space."
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Amboss]({{< relref "amboss" >}}) {#from-amboss--amboss-dot-md}


### [V/Q mismatch]({{< relref "v_q_mismatch" >}}) {#v-q-mismatch--v-q-mismatch-dot-md}

-   An imbalance between total lung ventilation (airflow; _V_) and total lung perfusion (blood flow; _Q_)
-   Characterized by an increased A-a gradient
-   **The most common cause of [hypoxemia]({{< relref "hypoxemia" >}})**


#### Increased V/Q ratio {#increased-v-q-ratio}

-   **Indicative of [dead space]({{< relref "physiological_dead_space" >}})**: Volume of inspired air that does not participate in gas exchange during [ventilation]({{< relref "ventilation" >}})
    -   [Anatomic dead space]({{< relref "anatomic_dead_space" >}}): the volume of air in the [conducting zone]({{< relref "conducting_zone" >}}) (e.g. mouth, trachea)
    -   [Alveolar dead space]({{< relref "alveolar_dead_space" >}}): the sum of hte volumes of alveoli that do not participate in gas exchange (mainly apex of lungs)
        -   These alveoli are ventilated **but not perfused**
-   Causes:
    -   Blood flow obstruction (e.g. pulmonary embolism)
    -   Exercise
        -   Cardiac output increases -> vasodilation of apical capillaries


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-22 Fri] </span></span> > Respiration: Ventilation - Perfusion Matching > Pre-work**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Define the difference between V/Q for a shunt vs. V/Q for a dead space.]({{< relref "define_the_difference_between_v_q_for_a_shunt_vs_v_q_for_a_dead_space" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
