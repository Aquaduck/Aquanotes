+++
title = "Protein farnesylation and disease"
author = ["Arif Ahsan"]
date = 2021-09-14T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Rationalize the use of farnesyltranferase inhibitors to slow the progression of Hutchinson-Guilford Progeria.]({{< relref "rationalize_the_use_of_farnesyltranferase_inhibitors_to_slow_the_progression_of_hutchinson_guilford_progeria" >}}) {#rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot--rationalize-the-use-of-farnesyltranferase-inhibitors-to-slow-the-progression-of-hutchinson-guilford-progeria-dot-md}

<!--list-separator-->

-  From [Protein farnesylation and disease]({{< relref "protein_farnesylation_and_disease" >}})

    <!--list-separator-->

    -  Nuclear [lamins]({{< relref "lamin" >}})

        -   Humans have **three** distinct lamin genes that encode seven different proteins

        <!--list-separator-->

        -  [A-type lamins]({{< relref "lamin_a" >}})

            -   Products of a single gene termed [LMNA]({{< relref "lmna" >}})
            -   [Prelamin A]({{< relref "prelamin_a" >}}) is the precursor protein of mature lamin A and possesses a COOH terminal CAAX motif -> the site of post-translational modifications
            -   Plays a major role in **nuclear envelope architecture** and a functional role in **heterochromatin organization, cell cycle, differentiation dynamics, and transcriptional regulation**
            -   Undergoes post-translational modification via [farnesylation]({{< relref "farnesylation" >}})

    <!--list-separator-->

    -  Isoprenylation and postprenylation of nuclear lamins

        -   [Farnesylation]({{< relref "farnesylation" >}}): the post-translational addition of the 15 carbon [farnesyl pyrophosphate]({{< relref "farnesyl_pyrophosphate" >}}) (FPP) via thioether bond to the sulphur atom of the cysteine of a carboxyterminal CAAX motif
        -   In [HGPS]({{< relref "hutchinson_guildford_progeria" >}}), a 50 AA deletion in prelamin A removes the site for a second endoproteolytic cleavage
            -   Consequently, no mature [lamin A]({{< relref "lamin_a" >}}) is formed -> accumulation of a **farnesylated mutant prelamin A** ([progerin]({{< relref "progerin" >}}))


### Unlinked references {#unlinked-references}

[Show unlinked references]
