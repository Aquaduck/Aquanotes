+++
title = "Posterior pituitary"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Osmoreceptor]({{< relref "osmoreceptor" >}}) {#osmoreceptor--osmoreceptor-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Signal the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) to release [ADH]({{< relref "antidiuretic_hormone" >}}) in response to high blood osmolarity

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Antidiuretic Hormone (ADH) (p.746)**

    In response to **high blood osmolarity** -> [osmoreceptors]({{< relref "osmoreceptor" >}}) signal the [posterior pituitary]({{< relref "posterior_pituitary" >}}) to release ADH

    ---


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927) > Angiotensin (I + II)**

    Angiotensin II stimulates the release of [ADH]({{< relref "antidiuretic_hormone" >}}) from the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) and [aldosterone]({{< relref "aldosterone" >}}) from the [adrenal cortex]({{< relref "adrenal_cortex" >}})

    ---


#### [Antidiuretic hormone]({{< relref "antidiuretic_hormone" >}}) {#antidiuretic-hormone--antidiuretic-hormone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Secreted by the [Posterior pituitary]({{< relref "posterior_pituitary" >}}) in response to [osmoreceptors]({{< relref "osmoreceptor" >}}) in the [Hypothalamus]({{< relref "hypothalamus" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
