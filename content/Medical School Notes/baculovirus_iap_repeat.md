+++
title = "Baculovirus IAP repeat"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Allows [IAPs]({{< relref "inhibitor_of_apoptosis" >}}) to bind + inhibit activated caspases


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > IAPs**

    All IAPs have one or more [baculovirus IAP repeat]({{< relref "baculovirus_iap_repeat" >}}) (BIR) domains

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
