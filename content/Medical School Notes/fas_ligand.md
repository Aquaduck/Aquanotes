+++
title = "Fas ligand"
author = ["Arif Ahsan"]
date = 2021-09-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  **🔖 TRADD and Other Death Receptors**

    [Fas]({{< relref "fas" >}})'s main function is to induce apoptosis via formation of a DISC following binding of [FasL]({{< relref "fas_ligand" >}})

    ---


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Fas > Cell-surface death receptors activate the extrinsic pathway of apoptosis (p. 1024)**

    Fas activated by [Fas ligand]({{< relref "fas_ligand" >}}) -> death domains on Fas death receptors bind intracellular adaptor proteins -> bind [initiator caspases]({{< relref "initiator_caspase" >}}) (primarily [caspase-8]({{< relref "caspase_8" >}})) -> formation of a [death-inducing signaling complex]({{< relref "death_inducing_signaling_complex" >}}) (DISC)

    ---


#### [Apo2L/TRAIL: apoptosis signaling, biology, and potential for cancer therapy]({{< relref "apo2l_trail_apoptosis_signaling_biology_and_potential_for_cancer_therapy" >}}) {#apo2l-trail-apoptosis-signaling-biology-and-potential-for-cancer-therapy--apo2l-trail-apoptosis-signaling-biology-and-potential-for-cancer-therapy-dot-md}

<!--list-separator-->

-  **🔖 Apoptosis signaling by Apo2L/TRAIL**

    Similar to [FasL]({{< relref "fas_ligand" >}}), TRAIL initiates apoptosis upon binding to its cognate death receptors by inducing the recruitment of specific cytoplasmic proteins to the intracellular death domain of the receptor -> formation of [DISC]({{< relref "death_inducing_signaling_complex" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
