+++
title = "Understand the mechanisms that control major cell cycle transitions and events"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Cell Cycle and Regulation Workshop]({{< relref "cell_cycle_and_regulation_workshop" >}}) {#from-cell-cycle-and-regulation-workshop--cell-cycle-and-regulation-workshop-dot-md}


### CDKs and Cyclins {#cdks-and-cyclins}

{{< figure src="/ox-hugo/_20210921_171730screenshot.png" caption="Figure 1: Activation and inactivation of Cyclin-CDK complexes drive cell progression" width="800" >}}


#### [Cyclin-dependent kinases]({{< relref "cyclin_dependent_kinase" >}}) (CDKs) {#cyclin-dependent-kinases--cyclin-dependent-kinase-dot-md----cdks}

-   Regulate cell cycle progression by modulating activity of other proteins via phosphorylation


#### [Cyclins]({{< relref "cyclin" >}}) {#cyclins--cyclin-dot-md}

-   Family of unstable regulatory subunits of CDKs
-   Synthesized and degraded at specific times during the cell cycle
-   Function as **"cell cycle clock"**
-   Dictate level of activity of [CDKs]({{< relref "cyclin_dependent_kinase" >}}) at specific points in the cell cycle


#### Different CDKs function at different phases of the cell cycle {#different-cdks-function-at-different-phases-of-the-cell-cycle}

| Phase   | CDK/cyclin    | Function                               |
|---------|---------------|----------------------------------------|
| G1      | CDK4/cyclin D | pRB phosphorylation, activation of E2F |
| G1/S    | CDK2/cyclin E | Histone gene expression                |
|         |               | Centrosome duplication                 |
|         |               | Origin activation                      |
| S phase | CDK2/cyclin A | Initation of DNA synthesis             |
| G2/M    | CDK1/cyclin B | Mitosis                                |


### Activation/inactivation mechanisms for [CDKs]({{< relref "cyclin_dependent_kinase" >}}) {#activation-inactivation-mechanisms-for-cdks--cyclin-dependent-kinase-dot-md}


#### Activation of CDKs {#activation-of-cdks}

-   CDKs are **activated** by [cyclin]({{< relref "cyclin" >}}) binding and phosphorylation of the T-loop
    -   _Inactive state_
        -   Active site blocked by T-loop (shown in red)
    -   _Partially active_
        -   Binding of cyclin -> T-loop moves out of active site -> partial activation
    -   _Fully active_
        -   Phosphorylation of CDK2 by [CAK]({{< relref "cdk_activating_kinase" >}}) at a threonine residue in T-loop further activates CDK by **changing shape of T-loop** -> improves ability of CDK to bind substrates

{{< figure src="/ox-hugo/_20210921_173559screenshot.png" caption="Figure 2: Structural basis of CDK activation based on 3D structures of human CDK2 and cyclin A" width="500" >}}

-   Protein degradation also provides both positive and negative regulation to CDKs


#### Inactivation of CDKs {#inactivation-of-cdks}

-   CDKs are also **inactivated** by phosphorylation
    -   Active cyclin-CDK complex is turned off when [Wee1]({{< relref "wee1" >}}) phosphorylates two closely spaced sites above the active site
        -   Removal of these phosphates by [Cdc25]({{< relref "cdc25" >}}) activates the cyclin-CDK complex

{{< figure src="/ox-hugo/_20210921_174512screenshot.png" width="400" >}}

-   The [Anaphase promoting complex/cyclosome]({{< relref "anaphase_promoting_complex_cyclosome" >}}) (APC/C) **inactivates** CDKs in [M-phase]({{< relref "m_phase" >}}) by ubiquitinating [cyclin B]({{< relref "cyclin_b" >}})


### Loading of cohesin to link replicated sister chromosomes {#loading-of-cohesin-to-link-replicated-sister-chromosomes}


## From [Molecular Biology of the Cell, Sixth Edition]({{< relref "molecular_biology_of_the_cell_sixth_edition" >}}) {#from-molecular-biology-of-the-cell-sixth-edition--molecular-biology-of-the-cell-sixth-edition-dot-md}


### Mitosis (p. 978) {#mitosis--p-dot-978}

-   An abrupt increase in [M-Cdk]({{< relref "m_cdk" >}}) activity at the [G2/M transition checkpoint]({{< relref "g2_m_transition_checkpoint" >}}) triggers the events of early [mitosis]({{< relref "mitosis" >}})


### Dephosphorylation Activates [M-Cdk]({{< relref "m_cdk" >}}) at the Onset of [Mitosis]({{< relref "mitosis" >}}) {#dephosphorylation-activates-m-cdk--m-cdk-dot-md--at-the-onset-of-mitosis--mitosis-dot-md}

1.  M-Cdk activation begins with the accumulation of [M-cyclin]({{< relref "cyclin_b" >}})
    -   In **embryonic cell cycles**, the synthesis of M-cyclin is constant throughout, and M-cyclin accumulation results from the high stability of the protein in interphase
    -   In **most other cell types**, M-cyclin synthesis increases during G2 and M, owing primarily to an increase in M-cyclin gene transcription
2.  [Cdk1]({{< relref "cdk1" >}}) forms a complex with [M-cyclin]({{< relref "cyclin_b" >}}) -> formation of M-Cdk complex as the cell approaches mitosis
3.  [CAK]({{< relref "cdk_activating_kinase" >}}) phosphorylates M-Cdk at its activating site
    -   [Wee1]({{< relref "wee1" >}}) **also** phosphorylates M-Cdk at two neighboring inhibitory sites
    -   Ultimately, this **holds M-Cdk in the inactive state** -> by the end of G2, the cell contains an abundant stockpile of M-Cdk ready to act
4.  [Cdc25]({{< relref "cdc25" >}}) is phosphorylated -> **activated** -> **removes the inhibitory phosphates that restrain M-Cdk**
    -   At the same time, [Wee1]({{< relref "wee1" >}}) activity is suppressed -> **ensures that M-Cdk activity increases**
5.  M-Cdk can interact with its own effectors:
    -   [Cdc25]({{< relref "cdc25" >}}) can be activated (in part) by M-Cdk
    -   [Wee1]({{< relref "wee1" >}}) can be inhibited by M-Cdk
    -   This suggests that **M-Cdk activation in mitosis involves positive feedback loops**

{{< figure src="/ox-hugo/_20210927_172513screenshot.png" width="700" >}}


### [Mitogens]({{< relref "mitogen" >}}) Stimulate G<sub>1</sub>-Cdk and G1/S-Cdk activities (p. 1012) {#mitogens--mitogen-dot-md--stimulate-g-cdk-and-g1-s-cdk-activities--p-dot-1012}

-   For the vast majority of animal cells, mitogens control the rate of cell division by **acting in the G<sub>1</sub> phase of the cell cycle**
    -   Mitogens allow Cdk activity to occur in G<sub>1</sub> -> entry into a new cell cycle


#### Mitogens interact with cell-surface receptors to trigger multiple intracellular signaling pathways {#mitogens-interact-with-cell-surface-receptors-to-trigger-multiple-intracellular-signaling-pathways}

<!--list-separator-->

-  [Ras]({{< relref "ras" >}})

    -   GTPase activity activates MAP kinase cascade -> increased production of transcription regulatory proteins such as [Myc]({{< relref "myc" >}})

<!--list-separator-->

-  [Myc]({{< relref "myc" >}})

    -   Myc is thought to promote cell-cycle entry by several mechanisms
        -   Increase expression of genes encoding G<sub>1</sub> cyclins ([D cyclins]({{< relref "d_cyclin" >}})) -> increase G<sub>1</sub>-Cdk (cyclin D-Cdk4) activity
        -   Myc also stimulates the transcription of genes that increase cell growth

<!--list-separator-->

-  [E2F]({{< relref "e2f" >}}) proteins

    -   Activation of E2F is a key function of G<sub>1</sub>-Cdk complexes
    -   E2F proteins bind to specific DNA sequences in promoters of genes that encode proteins required for S-phase entry
        -   G<sub>1</sub>/S-cyclins
        -   S-cyclins
        -   Proteins involved in DNA synthesis + chromosome duplication
    -   In the absence of mitogenic stimulation, E2F-dependent gene expression is inhibited by members of the [Retinoblastoma protein family]({{< relref "retinoblastoma_protein_family" >}})

<!--list-separator-->

-  [Rb family]({{< relref "retinoblastoma_protein_family" >}})

    -   When cells are stimulated to divide by mitogens -> active G<sub>1</sub>/Cdk accumulates -> **phosphorylates** Rb -> reduces Rb binding to E2F
        -   Allows E2F to activate expression of target genes
    -   Loss of both copies of the Rb gene -> excessive proliferation of some cells in the developing retina
        -   Suggests that **Rb is particularly important for restraining cell division in this tissue**

<!--list-separator-->

-  Overview

    {{< figure src="/ox-hugo/_20210928_161037screenshot.png" caption="Figure 3: Mitogen stimulated cell cycle entry" >}}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-23 Thu] </span></span> > Cell Cycle & Regulation I > Pre-work**

    <!--list-separator-->

    - <span class="org-todo todo ___">[ ]</span>  [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}})

        <!--list-separator-->

        -  Activation/ inactivation mechanisms for Cdks

        <!--list-separator-->

        -  Loading of cohesin to link replicated sister chromosomes


### Unlinked references {#unlinked-references}

[Show unlinked references]
