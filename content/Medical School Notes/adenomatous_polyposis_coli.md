+++
title = "Adenomatous polyposis coli"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [APC disambiguation]({{< relref "apc_disambiguation" >}}) {#apc-disambiguation--apc-disambiguation-dot-md}

<!--list-separator-->

-  **🔖 APC can refer to:**

    [Adenomatous polyposis coli]({{< relref "adenomatous_polyposis_coli" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
