+++
title = "Protein"
author = ["Arif Ahsan"]
date = 2021-07-15T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}


## Backlinks {#backlinks}


### 8 linked references {#8-linked-references}


#### [Ribosome]({{< relref "ribosome" >}}) {#ribosome--ribosome-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Primarily responsible for [protein]({{< relref "protein" >}}) synthesis

    ---


#### [Malvern - Binding Affinity]({{< relref "malvern_binding_affinity" >}}) {#malvern-binding-affinity--malvern-binding-affinity-dot-md}

<!--list-separator-->

-  **🔖 What is Binding Affinity?**

    e.g. [proteins]({{< relref "protein" >}})

    ---


#### [Kaplan Biochemistry Chapter 7]({{< relref "kaplan_biochemistry_chapter_7" >}}) {#kaplan-biochemistry-chapter-7--kaplan-biochemistry-chapter-7-dot-md}

<!--list-separator-->

-  **🔖 7.1 The Genetic Code**

    _Central dogma of molecular biology_: the transfer of genetic information into [proteins]({{< relref "protein" >}})

    ---


#### [Introduction to Metabolism and Glycolysis]({{< relref "introduction_to_metabolism_and_glycolysis" >}}) {#introduction-to-metabolism-and-glycolysis--introduction-to-metabolism-and-glycolysis-dot-md}

<!--list-separator-->

-  **🔖 Anabolic pathways**

    Combine small molecules to form complex molecules such as [proteins]({{< relref "protein" >}})

    ---


#### [Differentiate between rough and smooth endoplasmic reticulum (ER) both in structure and function.]({{< relref "differentiate_between_rough_and_smooth_endoplasmic_reticulum_er_both_in_structure_and_function" >}}) {#differentiate-between-rough-and-smooth-endoplasmic-reticulum--er--both-in-structure-and-function-dot--differentiate-between-rough-and-smooth-endoplasmic-reticulum-er-both-in-structure-and-function-dot-md}

<!--list-separator-->

-  **🔖 Rough endoplasmic reticulum (RER) > Function**

    Location where **membrane-packaged [proteins]({{< relref "protein" >}})** are synthesized

    ---


#### [Cytoplasm and Organelles]({{< relref "cytoplasm_and_organelles" >}}) {#cytoplasm-and-organelles--cytoplasm-and-organelles-dot-md}

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Rough endoplasmic reticulum (RER) > Function**

    Location where **membrane-packaged [proteins]({{< relref "protein" >}})** are synthesized

    ---

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Ribosomes > Structure**

    _Polyribosome (polysome)_: a cluster of ribosomes along a single strand of [mRNA]({{< relref "messenger_rna" >}}) engaged w/ the synthesis of [protein]({{< relref "protein" >}})

    ---

<!--list-separator-->

-  **🔖 From BRS Cell Biology & Histology 6th edition > Structural components > Organelles > Ribosomes > Structure**

    Composed of several types of [rRNA]({{< relref "ribosomal_rna" >}}) and [proteins]({{< relref "protein" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
