+++
title = "Allosteric regulation"
author = ["Arif Ahsan"]
date = 2021-08-19T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of regulation found in [enzymes]({{< relref "enzyme" >}})


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Pyruvate kinase]({{< relref "pyruvate_kinase" >}}) {#pyruvate-kinase--pyruvate-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Allosterically]({{< relref "allosteric_regulation" >}}) inhibited by: [ATP]({{< relref "atp" >}})

    ---


#### [Pyruvate DH kinase]({{< relref "pyruvate_dh_kinase" >}}) {#pyruvate-dh-kinase--pyruvate-dh-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Allosterically]({{< relref "allosteric_regulation" >}}) stimulated by the products of the reaction

    ---


#### [Pyruvate dehydrogenase complex]({{< relref "pyruvate_dehydrogenase_complex" >}}) {#pyruvate-dehydrogenase-complex--pyruvate-dehydrogenase-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Allosterically]({{< relref "allosteric_regulation" >}}) regulated by product inhibition by [acetyl-CoA]({{< relref "acetyl_coa" >}}) & [NADH]({{< relref "nadh" >}}) against their respective enzymes

    ---


#### [Hexokinase]({{< relref "hexokinase" >}}) {#hexokinase--hexokinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Allosterically]({{< relref "allosteric_regulation" >}}) regulated

    ---


#### [6-Phosphofructo-1-kinase]({{< relref "6_phosphofructo_1_kinase" >}}) {#6-phosphofructo-1-kinase--6-phosphofructo-1-kinase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Allosterically]({{< relref "allosteric_regulation" >}}) regulated

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
