+++
title = "Endonuclease"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Identify key molecules involved in apoptotic cell death]({{< relref "identify_key_molecules_involved_in_apoptotic_cell_death" >}}) {#identify-key-molecules-involved-in-apoptotic-cell-death--identify-key-molecules-involved-in-apoptotic-cell-death-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Caspase > Targets of caspase**

    Cleavage of protein that frees [endonuclease]({{< relref "endonuclease" >}}) -> cuts up DNA in cell nucleus

    ---


#### [Caspase]({{< relref "caspase" >}}) {#caspase--caspase-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Cleavage of proteins that free [endonuclease]({{< relref "endonuclease" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
