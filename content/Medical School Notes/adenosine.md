+++
title = "Adenosine"
author = ["Arif Ahsan"]
date = 2021-10-06T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Use the metabolic pathways of purine catabolism and salvage to explain the molecular basis for gout.]({{< relref "use_the_metabolic_pathways_of_purine_catabolism_and_salvage_to_explain_the_molecular_basis_for_gout" >}}) {#use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot--use-the-metabolic-pathways-of-purine-catabolism-and-salvage-to-explain-the-molecular-basis-for-gout-dot-md}

<!--list-separator-->

-  **🔖 From Lippincott's Illustrated Reviews: Biochemistry, 5e > Degradation of Purine Nucleotides (p. 296) > Formation of uric acid**

    Alternatively, an amino group is removed from [adenosine]({{< relref "adenosine" >}}) -> produces [inosine]({{< relref "inosine" >}}) (hypoxanthine-ribose) by [adenosine deaminase]({{< relref "adenosine_deaminase" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Adenosine]({{< relref "adenosine" >}}) is the main metabolic vasodilator

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Macula densa > Feedback mechanisms**

    Triggers release of [Adenosine]({{< relref "adenosine" >}}) -> vasoconstriction of the [afferent arteriole]({{< relref "afferent_arteriole_of_the_kidney" >}}) -> **decrease in glomerular filtration rate**

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
