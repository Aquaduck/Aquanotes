+++
title = "Costodiaphragmatic recess"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A [Pleural recess]({{< relref "pleural_recess" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Describe the procedure of thoracocentesis, highlighting the rationale behind the use of the technique.]({{< relref "describe_the_procedure_of_thoracocentesis_highlighting_the_rationale_behind_the_use_of_the_technique" >}}) {#describe-the-procedure-of-thoracocentesis-highlighting-the-rationale-behind-the-use-of-the-technique-dot--describe-the-procedure-of-thoracocentesis-highlighting-the-rationale-behind-the-use-of-the-technique-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Insertion of a Chest Tube**

    Tube may be directed **inferiorly** towards [Costodiaphragmatic recess]({{< relref "costodiaphragmatic_recess" >}}) for **fluid drainage**

    ---

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Thoracocentesis**

    When patient in upright position -> intrapleural fluid accumulates in [costodiaphragmatic recess]({{< relref "costodiaphragmatic_recess" >}})

    ---


#### [Describe the meaning of pleural recesses and describe the locations of the costodiaphragmatic and costmediastinal recesses.]({{< relref "describe_the_meaning_of_pleural_recesses_and_describe_the_locations_of_the_costodiaphragmatic_and_costmediastinal_recesses" >}}) {#describe-the-meaning-of-pleural-recesses-and-describe-the-locations-of-the-costodiaphragmatic-and-costmediastinal-recesses-dot--describe-the-meaning-of-pleural-recesses-and-describe-the-locations-of-the-costodiaphragmatic-and-costmediastinal-recesses-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Pleural recesses**

    <!--list-separator-->

    -  [Costodiaphragmatic recesses]({{< relref "costodiaphragmatic_recess" >}})

        -   There are two - right and left
        -   Spaces between lower border of lung and pleural reflections from chest wall onto diaphgram
        -   Potential spaces where flui dmay accumulate
            -   Excess fluid may be drained (pleural tap or thoracentesis) from pleural cavity **without penetrating lungs**
        -   **Only in forced inspiration do the lungs expand into these recesses**


### Unlinked references {#unlinked-references}

[Show unlinked references]
