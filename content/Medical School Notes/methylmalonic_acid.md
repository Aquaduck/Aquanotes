+++
title = "Methylmalonic acid"
author = ["Arif Ahsan"]
date = 2021-10-24T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Increased [methylmalonic acid] is indicative of a [vitamin B12]({{< relref "cobalamin" >}}) deficiency


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  [Methylmalonic acid]({{< relref "methylmalonic_acid" >}})

    <!--list-separator-->

    -  From [Fat Metabolism in Muscle & Adipose Tissue]({{< relref "fat_metabolism_in_muscle_adipose_tissue" >}})

        -   Increased [methylmalonic acid] is indicative of a [vitamin B12]({{< relref "cobalamin" >}}) deficiency


### Unlinked references {#unlinked-references}

[Show unlinked references]
