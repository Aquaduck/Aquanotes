+++
title = "Mucin"
author = ["Arif Ahsan"]
date = 2021-10-17T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the molecular defect that leads to CF.]({{< relref "describe_the_molecular_defect_that_leads_to_cf" >}}) {#describe-the-molecular-defect-that-leads-to-cf-dot--describe-the-molecular-defect-that-leads-to-cf-dot-md}

<!--list-separator-->

-  **🔖 From Cystic fibrosis in the year 2020: A disease with a new face > Cystic fibrosis**

    [Bicarbonate]({{< relref "bicarbonate" >}}) release in the airway is important for the proper unfolding of [mucins]({{< relref "mucin" >}}) and defending against bacteria

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
