+++
title = "Antacid"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Metabolic Alkalosis: Primary Bicarbonate Excess (p. 1281)**

    Can be transiently caused by an ingestion of excessive [bicarbonate]({{< relref "bicarbonate" >}}), [citrate]({{< relref "citrate" >}}), or [antacids]({{< relref "antacid" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
