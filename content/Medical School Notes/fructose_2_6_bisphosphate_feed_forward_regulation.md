+++
title = "Fructose-2,6-bisphosphate (feed-forward regulation)"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "source", "amboss"]
draft = false
+++

## Function {#function}

-   Stimulates glycolysis by increasing PFK-1 activity in states of high [blood glucose]


## Synthesis {#synthesis}


### [PFK-2]({{< relref "pfk_2" >}}) + [FBPase-2]({{< relref "fructose_bisposphatase_2" >}}) {#pfk-2--pfk-2-dot-md--plus-fbpase-2--fructose-bisposphatase-2-dot-md}

-   Two domains of the same bifunctional enzyme


### [PKA]({{< relref "protein_kinase_a" >}}) {#pka--protein-kinase-a-dot-md}

-   Phosphorylates aforementioned bifunctional enzyme -> determines which of two domains is active


#### Dephosphorylated state {#dephosphorylated-state}

-   ↑ PFK-2 activity -> ↑ F-2,6-bP


#### Phosphorylated state {#phosphorylated-state}

-   ↑ FBPase-2 activity -> ↓ F-2,6-bP


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
