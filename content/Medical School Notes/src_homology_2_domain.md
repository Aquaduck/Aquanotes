+++
title = "Src Homology 2 domain"
author = ["Arif Ahsan"]
date = 2021-09-05T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Discuss receptor tyrosine kinases and tyrosine-associated receptor signal pathways]({{< relref "discuss_receptor_tyrosine_kinases_and_tyrosine_associated_receptor_signal_pathways" >}}) {#discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways--discuss-receptor-tyrosine-kinases-and-tyrosine-associated-receptor-signal-pathways-dot-md}

<!--list-separator-->

-  **🔖 From Cell Signalling II - Intracellular Signal Pathways > Receptor protein tyrosine kinases (RPTKs) > Two key pathways > Ras-GTP > Pathway**

    Allows binding of [SH<sub>2</sub>]({{< relref "src_homology_2_domain" >}}) binding proteins -> Ras activation

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
