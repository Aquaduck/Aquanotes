+++
title = "List the general treatment objectives underlying pharmacogenomic testing"
author = ["Arif Ahsan"]
date = 2021-09-25T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Pharmacogenetics Pre-learning Material]({{< relref "pharmacogenetics_pre_learning_material" >}}) {#from-pharmacogenetics-pre-learning-material--pharmacogenetics-pre-learning-material-dot-md}


### Precision (targeted) therapies {#precision--targeted--therapies}

-   Medicines **designed to exploit specific pathogenetic genetic variations** in order to have a therapeutic effect


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 3 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-24 Fri] </span></span> > Pharmacogenetics > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [List the general treatment objectives underlying pharmacogenomic testing]({{< relref "list_the_general_treatment_objectives_underlying_pharmacogenomic_testing" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
