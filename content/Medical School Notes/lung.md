+++
title = "Lung"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 16 linked references {#16-linked-references}


#### [West's zones of the lung]({{< relref "west_s_zones_of_the_lung" >}}) {#west-s-zones-of-the-lung--west-s-zones-of-the-lung-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Relative distributions of [blood]({{< relref "blood" >}}) flow in the [lung]({{< relref "lung" >}}) during the [cardiac cycle]({{< relref "cardiac_cycle" >}})

    ---


#### [Restrictive lung disease]({{< relref "restrictive_lung_disease" >}}) {#restrictive-lung-disease--restrictive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [lung]({{< relref "lung" >}}) disease

    ---


#### [Residual volume]({{< relref "residual_volume" >}}) {#residual-volume--residual-volume-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Volume of air that remains in the [lungs]({{< relref "lung" >}}) after a maximal [exhalation]({{< relref "exhalation" >}})

    ---


#### [Pulmonary artery]({{< relref "pulmonary_artery" >}}) {#pulmonary-artery--pulmonary-artery-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Brings deoxygenated blood from the [Heart]({{< relref "heart" >}}) to the [lungs]({{< relref "lung" >}})

    ---


#### [Pneumothorax]({{< relref "pneumothorax" >}}) {#pneumothorax--pneumothorax-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Air in chest cavity causes loss of negative pressure -> collapsed [lung]({{< relref "lung" >}})

    ---


#### [Pleura]({{< relref "pleura" >}}) {#pleura--pleura-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A serous membrane that surrounds the [lung]({{< relref "lung" >}})

    ---


#### [Obstructive lung disease]({{< relref "obstructive_lung_disease" >}}) {#obstructive-lung-disease--obstructive-lung-disease-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [lung]({{< relref "lung" >}}) disease

    ---


#### [Name the stages of lung development, their approximate times of development, and explain the major changes in each stage.]({{< relref "name_the_stages_of_lung_development_their_approximate_times_of_development_and_explain_the_major_changes_in_each_stage" >}}) {#name-the-stages-of-lung-development-their-approximate-times-of-development-and-explain-the-major-changes-in-each-stage-dot--name-the-stages-of-lung-development-their-approximate-times-of-development-and-explain-the-major-changes-in-each-stage-dot-md}

<!--list-separator-->

-  **🔖 From Embryology - Thorax Part 1: Thoracic Cavity & Lung Development PPT**

    <!--list-separator-->

    -  Phases of [lung]({{< relref "lung" >}}) growth:

        {{< figure src="/ox-hugo/_20211023_135716screenshot.png" width="700" >}}

        <!--list-separator-->

        -  Embryonic period (26 days - 6 weeks):

            -   Lung bud -> 2 diverticula, bronchial buds -> segmental (right + left mainstem) bronchi
            -   _Potential airways_: epithelium surrounded by mesenchyme w/ widely separated capillaries
            -   Pulmonary arteries from 6th aortic arch ~ end of 6 weeks

        <!--list-separator-->

        -  Pseudoglandular period (6 - 16 weeks):

            -   Development of **conducting airway** to **terminal bronchioles & divisions of lobules & acini**
            -   Lymphatics into lungs
            -   Cartilaginous rings and smooth muscle
            -   Pseudostratified columnar epithelium, cilia & goblet cells
            -   Tubular submucosal glands

        <!--list-separator-->

        -  Canalicular period (17 - 28 weeks):

            -   Subdivision of respiratory bronchioles & multiple irregular alveolar ducts
            -   **Appearance of [type II alveolar cells]({{< relref "type_2_alveolar_cell" >}})** -> **[surfactant]({{< relref "surfactant" >}}) synthesis**
            -   [Type I alveolar cells]({{< relref "type_1_alveolar_cell" >}}) differentiate from Type II
                -   Capillaries proliferate under Type I cells
            -   Cartilage extension to distal bronchi
            -   Submucosal tubules -> mucin containing acini
            -   Bronchioles straighten

        <!--list-separator-->

        -  Saccular ("Terminal sac") period (28 - 32 weeks):

            -   Distal air-spaces divide into smaller units
            -   Interstitial tissue shrinks
            -   Fibroblasts differentiate -> production of **collagen, ECM & elastin**
            -   Capillary network proliferates (double capillary network)
            -   **Alveoli & early gas exchange begins @ 32 weeks**

        <!--list-separator-->

        -  Alveolar period (32-36 weeks to ~3 years post-natal):

            -   Alveoli -> flask shape
            -   Lymphatics extend through interlobular septae to pleura
            -   **Double -> single capillary bed** -> **increased oxygen exchange**
            -   **Surge of maternal glucocorticoids** ->
                -   Increased fetal plasma cortisol -> increased secretion of [surfactant]({{< relref "surfactant" >}}) (progressive increase from 32 weeks)
            -   [Type II pneumocytes]({{< relref "type_2_alveolar_cell" >}}) increase -> increased [surfactant]({{< relref "surfactant" >}}) production
            -   **Lungs become more inflated + plastic**


#### [Inspiration]({{< relref "inspiration" >}}) {#inspiration--inspiration-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The part of [ventilation]({{< relref "ventilation" >}}) whewre air is taken into the [lungs]({{< relref "lung" >}})

    ---


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Embryology > Respiratory**

    Stages of [lung]({{< relref "lung" >}}) maturation:

    ---


#### [Hering-Breuer inflation reflex]({{< relref "hering_breuer_inflation_reflex" >}}) {#hering-breuer-inflation-reflex--hering-breuer-inflation-reflex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inhibits [Inspiration]({{< relref "inspiration" >}}) to prevent overinflation of the [lungs]({{< relref "lung" >}})

    ---


#### [Conducting zone]({{< relref "conducting_zone" >}}) {#conducting-zone--conducting-zone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Area of the [Lung]({{< relref "lung" >}}) that extends from the trachea to the terminal bronchioles

    ---


#### [Club cell]({{< relref "club_cell" >}}) {#club-cell--club-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Nonciliated, secretory, cuboidal cells located in the terminal and respiratory bronchioles of the [lungs]({{< relref "lung" >}})

    ---


#### [Angiotensin II]({{< relref "angiotensin_ii" >}}) {#angiotensin-ii--angiotensin-ii-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Converted from angiotensin I in the [lungs]({{< relref "lung" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Club cell**

    Nonciliated, secretory, cuboidal cells located in the terminal and respiratory bronchioles of the [lungs]({{< relref "lung" >}})

    ---


#### [Alveoli]({{< relref "alveoli" >}}) {#alveoli--alveoli-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Part of the [lung]({{< relref "lung" >}}) where gas exchange occurs

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
