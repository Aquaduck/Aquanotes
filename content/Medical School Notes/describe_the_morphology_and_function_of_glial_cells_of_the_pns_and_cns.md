+++
title = "Describe the morphology and function of glial cells of the PNS and CNS."
author = ["Arif Ahsan"]
date = 2021-09-13T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [BRS Cell Biology & Histology]({{< relref "brs_cell_biology_histology" >}}) {#from-brs-cell-biology-and-histology--brs-cell-biology-histology-dot-md}


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 2 > Week 2 > <span class="timestamp-wrapper"><span class="timestamp">[2021-09-09 Thu] </span></span> > Cells of the Nervous System > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the morphology and function of glial cells of the PNS and CNS.]({{< relref "describe_the_morphology_and_function_of_glial_cells_of_the_pns_and_cns" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
