+++
title = "Accessing an Intercostal Space"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "source"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Describe the procedures associated with intercostal nerve anesthesia and thoracotomy and their indications.]({{< relref "describe_the_procedures_associated_with_intercostal_nerve_anesthesia_and_thoracotomy_and_their_indications" >}}) {#describe-the-procedures-associated-with-intercostal-nerve-anesthesia-and-thoracotomy-and-their-indications-dot--describe-the-procedures-associated-with-intercostal-nerve-anesthesia-and-thoracotomy-and-their-indications-dot-md}

<!--list-separator-->

-  From [Accessing an Intercostal Space]({{< relref "accessing_an_intercostal_space" >}})

    <!--list-separator-->

    -  [Anesthesia]({{< relref "anesthesia" >}})

        -   Anesthesia can be placed in the intercostal space to block the [intercostal nerve]({{< relref "intercostal_nerve" >}})
        -   **Ideally, it should be given between internal and innermost intercostal layers** where the nerve resides
            -   Can use ultrasound to guide procedure
        -   Procedure is commonly used in **patients with rib fractures or for thoracic surgery**
        -   When inserting a chest tube, the tube is inserted in the **inferior portion of the intercostal space**, just above the lower rib
            -   **Minimizes risk to vessels in the superior portion of the intercostal space**

    <!--list-separator-->

    -  [Thoracotomy]({{< relref "thoracotomy" >}})

        -   _Thoracotomy_: the surgical creation of an opening throuhg the thoracic wall to enter a pleural cavity
        -   **Posterolateral aspects of 5th and 7th intercostal spaces are important sites for posterior thoracotomy incisions**
        -   **Often used to treat or diagnose a problem with the lungs or heart**
            -   Most commonly done for lung cancer

        <!--list-separator-->

        -  Procedure

            -   With pt lying on contralateral side, fully abduct upper limb, placing forearm beside pt's head
                -   This elevates and laterally rotates inferior angle of scapula -> allows access as high as 4th intercostal space
                -   Most commonly, rib retraction allows procedures to be performed through a single intercostal space, with care to avoid the superior neurovascular bundle
            -   If **wider exposure is required**, surgeons use an **H-shaped incision** -> incise superficial aspect of periosteum that ensheaths rib, strip periosteum from rib, and excise a wide segment of rib to gain better access


#### [Describe the neurovasculature of the intercostal spaces.]({{< relref "describe_the_neurovasculature_of_the_intercostal_spaces" >}}) {#describe-the-neurovasculature-of-the-intercostal-spaces-dot--describe-the-neurovasculature-of-the-intercostal-spaces-dot-md}

<!--list-separator-->

-  From [Accessing an Intercostal Space]({{< relref "accessing_an_intercostal_space" >}})

    <!--list-separator-->

    -  [Intercostal muscles]({{< relref "intercostal_muscle" >}})

        -   The intercostal muscles are 3 thin planes of muscular and tendinous fibers
            1.  _External_
            2.  _Internal_
            3.  _Innermost_
        -   They are **all innervated by intercostal nerves (ventral rami of thoracic spinal nerves)**

    <!--list-separator-->

    -  [Intercostal Vessels]({{< relref "intercostal_vessel" >}})

        -   The _intercostal arteries_ course through **the thoracic wall between the ribs**
        -   **With the exception of 10th and 11th intercostal spaces**, each is supplied by 3 arteries:
            1.  A larger posterior intercostal artery
            2.  **2** small anterior intercostal arteries
        -   _Posterior intercostal arteries_ of **all but the first two interspaces** are from the _descending thoracic aorta_
        -   _Anterior intercostal arteries_ are branches of the _internal thoracic artery_
        -   Veins have similar names and generally follow the distribution of the intercostal arteries
            -   **Exception**: _Posterior intercostal veins_ drain into the _azygos system_

    <!--list-separator-->

    -  [Intercostal Nerves]({{< relref "intercostal_nerve" >}})

        -   _Ventral rami_ of T1-T11
            -   T12 nerve (aka [subcostal nerve]({{< relref "subcostal_nerve" >}})) is not confined between ribs
        -   Near angle of ribs, intercostal nerves pass between the _internal and innermost intercostal muscles_
        -   Intercostal nerves **supply general sensory innervation to skin on the thoracic wall and to the pleura**
            -   They also **supply motor innervation to intercostal, tranversus thoracis, and serratus posterior muscles**
            -   Carry /postganglionic sympathetic nerve fibers?
        -   Intercostal nerves 2-6 are confined to the thorax
        -   _1st intercostal nerve_ contributes to the [brachial plexus]({{< relref "brachial_plexus" >}}) as well as innervating the region of the first intercostal space
        -   _Intercostal nerves 7-11_ leave the intercostal spaces after innervating them -> **continue on to innervate muscles, skin, and lining of arterial abdominal wall**
        -   Intercostal vessels and nerves travel parallel to one another through the intercostal space
            -   Order of structures **superior -> inferior**:
                1.  Vein
                2.  Artery
                3.  Nerve
            -   Mnemonic: _VAN_


### Unlinked references {#unlinked-references}

[Show unlinked references]
