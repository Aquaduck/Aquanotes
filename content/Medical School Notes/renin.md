+++
title = "Renin"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 10 linked references {#10-linked-references}


#### [Renin-angiotensin-aldosterone mechanism]({{< relref "renin_angiotensin_aldosterone_mechanism" >}}) {#renin-angiotensin-aldosterone-mechanism--renin-angiotensin-aldosterone-mechanism-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    [Renin]({{< relref "renin" >}})

    ---


#### [Macula densa]({{< relref "macula_densa" >}}) {#macula-densa--macula-densa-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Contribute to the control of [glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}) and to the control of [renin]({{< relref "renin" >}}) secretion

    ---


#### [Identify the major physiological functions of the kidney: maintain steady state (fluid, electrolytes), excrete waste, hormonal regulation]({{< relref "identify_the_major_physiological_functions_of_the_kidney_maintain_steady_state_fluid_electrolytes_excrete_waste_hormonal_regulation" >}}) {#identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state--fluid-electrolytes--excrete-waste-hormonal-regulation--identify-the-major-physiological-functions-of-the-kidney-maintain-steady-state-fluid-electrolytes-excrete-waste-hormonal-regulation-dot-md}

<!--list-separator-->

-  **🔖 From Amboss > Major physiological functions of the kidney > Hormone synthesis**

    [Renin]({{< relref "renin" >}}) **<- focus mostly on this one in class**

    ---


#### [Granular cell]({{< relref "granular_cell" >}}) {#granular-cell--granular-cell-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Contain secretory vesules (the "granules") that contain [Renin]({{< relref "renin" >}})

    ---


#### [Explain what the juxtaglomerular apparatus is sensing and what happens as a result]({{< relref "explain_what_the_juxtaglomerular_apparatus_is_sensing_and_what_happens_as_a_result" >}}) {#explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result--explain-what-the-juxtaglomerular-apparatus-is-sensing-and-what-happens-as-a-result-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Juxtaglomerular apparatus (p. 21) > Macula densa cells**

    Contribute to the control of [glomerular filtration rate]({{< relref "glomerular_filtration_rate" >}}) and to the control of [renin]({{< relref "renin" >}}) secretion

    ---

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > Juxtaglomerular apparatus (p. 21) > Granular cells**

    Contain secretory granules -> contain [Renin]({{< relref "renin" >}})

    ---


#### [Explain the basic concepts of the renin-angiotensin-aldosterone system (RAAS).]({{< relref "explain_the_basic_concepts_of_the_renin_angiotensin_aldosterone_system_raas" >}}) {#explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system--raas--dot--explain-the-basic-concepts-of-the-renin-angiotensin-aldosterone-system-raas-dot-md}

<!--list-separator-->

-  **🔖 From Openstax Anatomy & Physiology > Renin-Angiotensin-Aldosterone Mechanism (p. 927)**

    <!--list-separator-->

    -  [Renin]({{< relref "renin" >}}) + Angiotensin

        -   Specialized cells in the [kidneys]({{< relref "kidney" >}}) found in the [juxtaglomerular apparatus]({{< relref "juxtaglomerular_apparatus" >}}) respond to decreased blood flow by secreting _renin_ into the blood
            -   Converts the plasma protein [angiotensinogen]({{< relref "angiotensinogen" >}}) (produced by the liver) into its active form - [angiotensin I]({{< relref "angiotensin_i" >}})


#### [Atrial natriuretic hormone]({{< relref "atrial_natriuretic_hormone" >}}) {#atrial-natriuretic-hormone--atrial-natriuretic-hormone-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Suppresses [Renin]({{< relref "renin" >}}), [Aldosterone]({{< relref "aldosterone" >}}), and [ADH]({{< relref "antidiuretic_hormone" >}}) production/release

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Respiration > Macula densa > Feedback mechanisms**

    Triggers release of [Renin]({{< relref "renin" >}}) -> vasoconstriction of the [efferent arteriole]({{< relref "efferent_arteriole_of_the_kidney" >}}) -> **increase in glomerular filtration rate**

    ---

<!--list-separator-->

-  **🔖 Respiration > Macula densa**

    Senses [Sodium]({{< relref "sodium" >}}) and Chloride levels and controls [Renin]({{< relref "renin" >}}) release, glomerular filtration, and renal blood flow via signaling pathways

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
