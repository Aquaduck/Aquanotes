+++
title = "Cyclin-dependent kinase"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Regulate [cell cycle]({{< relref "cell_cycle" >}}) progression by modulating activity of other proteins via phosphorylation


## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop**

    <!--list-separator-->

    -  Activation/inactivation mechanisms for [CDKs]({{< relref "cyclin_dependent_kinase" >}})

        <!--list-separator-->

        -  Activation of CDKs

            -   CDKs are **activated** by [cyclin]({{< relref "cyclin" >}}) binding and phosphorylation of the T-loop
                -   _Inactive state_
                    -   Active site blocked by T-loop (shown in red)
                -   _Partially active_
                    -   Binding of cyclin -> T-loop moves out of active site -> partial activation
                -   _Fully active_
                    -   Phosphorylation of CDK2 by [CAK]({{< relref "cdk_activating_kinase" >}}) at a threonine residue in T-loop further activates CDK by **changing shape of T-loop** -> improves ability of CDK to bind substrates

            {{< figure src="/ox-hugo/_20210921_173559screenshot.png" caption="Figure 1: Structural basis of CDK activation based on 3D structures of human CDK2 and cyclin A" width="500" >}}

            -   Protein degradation also provides both positive and negative regulation to CDKs

        <!--list-separator-->

        -  Inactivation of CDKs

            -   CDKs are also **inactivated** by phosphorylation
                -   Active cyclin-CDK complex is turned off when [Wee1]({{< relref "wee1" >}}) phosphorylates two closely spaced sites above the active site
                    -   Removal of these phosphates by [Cdc25]({{< relref "cdc25" >}}) activates the cyclin-CDK complex

            {{< figure src="/ox-hugo/_20210921_174512screenshot.png" width="400" >}}

            -   The [Anaphase promoting complex/cyclosome]({{< relref "anaphase_promoting_complex_cyclosome" >}}) (APC/C) **inactivates** CDKs in [M-phase]({{< relref "m_phase" >}}) by ubiquitinating [cyclin B]({{< relref "cyclin_b" >}})

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop > CDKs and Cyclins > Cyclins**

    Dictate level of activity of [CDKs]({{< relref "cyclin_dependent_kinase" >}}) at specific points in the cell cycle

    ---

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop > CDKs and Cyclins**

    <!--list-separator-->

    -  [Cyclin-dependent kinases]({{< relref "cyclin_dependent_kinase" >}}) (CDKs)

        -   Regulate cell cycle progression by modulating activity of other proteins via phosphorylation


#### [Cyclin]({{< relref "cyclin" >}}) {#cyclin--cyclin-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Unstable regulatory subunits of [CDKs]({{< relref "cyclin_dependent_kinase" >}})

    ---


#### [Cdk1]({{< relref "cdk1" >}}) {#cdk1--cdk1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A [CDK]({{< relref "cyclin_dependent_kinase" >}})

    ---


#### [Anaphase promoting complex/cyclosome]({{< relref "anaphase_promoting_complex_cyclosome" >}}) {#anaphase-promoting-complex-cyclosome--anaphase-promoting-complex-cyclosome-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Inactivates [CDKs]({{< relref "cyclin_dependent_kinase" >}}) in [M-phase]({{< relref "m_phase" >}}) by ubiquitinating [cyclin B]({{< relref "cyclin_b" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
