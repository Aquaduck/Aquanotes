+++
title = "Intercostal muscle"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the neurovasculature of the intercostal spaces.]({{< relref "describe_the_neurovasculature_of_the_intercostal_spaces" >}}) {#describe-the-neurovasculature-of-the-intercostal-spaces-dot--describe-the-neurovasculature-of-the-intercostal-spaces-dot-md}

<!--list-separator-->

-  **🔖 From Accessing an Intercostal Space**

    <!--list-separator-->

    -  [Intercostal muscles]({{< relref "intercostal_muscle" >}})

        -   The intercostal muscles are 3 thin planes of muscular and tendinous fibers
            1.  _External_
            2.  _Internal_
            3.  _Innermost_
        -   They are **all innervated by intercostal nerves (ventral rami of thoracic spinal nerves)**


### Unlinked references {#unlinked-references}

[Show unlinked references]
