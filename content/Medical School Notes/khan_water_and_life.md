+++
title = "Khan - Water and life"
author = ["Arif Ahsan"]
date = 2021-07-12T00:00:00-04:00
tags = ["medschool", "khan", "source"]
draft = false
+++

Link: <https://www.khanacademy.org/science/high-school-biology/hs-biology-foundations/hs-water-and-life/a/hs-water-and-life-review>


## Key terms: {#key-terms}

-   _Polar molecule_: A neutral, or uncharged molecule that has an asymmetric internal distribution of charge, leading to partially positive and partially negative regions
-   _Cohesion_: The attraction of molecules for other molecules of the same kind
-   _Adhesion_: The attraction of molecules for other molecules of a different kind
-   _Density_: The mass per unit volume of a substance
-   _Specific heat capacity_: The amount of heat needed to raise the temperature of one gram of a substance by one degree Celsius
-   _Heat of vaporization_: The amount of energy needed to change one gram of a liquid substance to a gas at constant temperature


## Unique properties of water {#unique-properties-of-water}

1.  [Water]({{< relref "water" >}}) is polar
    -   Partial positive charges on H and partial negative charges on O
    -   Bent overall structure
    -   Oxygen is more electronegative
2.  [Water]({{< relref "water" >}}) is an excellent solvent
    -   Can dissolve many polar and ionic substances
    -   Important because as water travels through water cycle, it takes many valuable nutrients with it
3.  [Water]({{< relref "water" >}}) has a high heat capacity
    -   Water temperature is very stable -> good for regulating temperature
4.  [Water]({{< relref "water" >}}) has high heat of vaporization
    -   Humans use water's high heat of vaporization to cool off -> sweat
    -   Since sweat is made mostly of water, evaporating water absorbs excess body heat and releases it into atmosphere
        -   Called _evaporative cooling_
5.  [Water]({{< relref "water" >}}) has cohesive and adhesive properties
    -   Forms hydrogen bonds w/ each other -> strong _cohesive_ force
    -   _Surface tension_: tendency of a liquid's surface to resist rupture when placed under tension or stress
    -   These properties are essential for fluid transport
6.  [Water]({{< relref "water" >}}) is less dense as a solid than as a liquid
    -   Water freezes -> molecules form crystalline structure that spaces molecules further apart than in liquid water -> less dense
    -   Keeps bodies of water from freezing solid


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
