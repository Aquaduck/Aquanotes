+++
title = "CDK-activating kinase"
author = ["Arif Ahsan"]
date = 2021-09-21T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Understand the mechanisms that control major cell cycle transitions and events]({{< relref "understand_the_mechanisms_that_control_major_cell_cycle_transitions_and_events" >}}) {#understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events--understand-the-mechanisms-that-control-major-cell-cycle-transitions-and-events-dot-md}

<!--list-separator-->

-  **🔖 From Molecular Biology of the Cell, Sixth Edition > Dephosphorylation Activates M-Cdk at the Onset of Mitosis**

    [CAK]({{< relref "cdk_activating_kinase" >}}) phosphorylates M-Cdk at its activating site

    ---

<!--list-separator-->

-  **🔖 From Cell Cycle and Regulation Workshop > Activation/inactivation mechanisms for CDKs > Activation of CDKs**

    Phosphorylation of CDK2 by [CAK]({{< relref "cdk_activating_kinase" >}}) at a threonine residue in T-loop further activates CDK by **changing shape of T-loop** -> improves ability of CDK to bind substrates

    ---


#### [Cyclin-CDK complex]({{< relref "cyclin_cdk_complex" >}}) {#cyclin-cdk-complex--cyclin-cdk-complex-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A complex formed between [cyclin]({{< relref "cyclin" >}}) and [CDK]({{< relref "cdk_activating_kinase" >}}), activating enzymatic function

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
