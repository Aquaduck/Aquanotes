+++
title = "Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)"
author = ["Arif Ahsan"]
date = 2021-10-09T00:00:00-04:00
tags = ["medschool", "learning_objective"]
draft = false
+++

## From [Lymphatic and Immune Systems PPT]({{< relref "lymphatic_and_immune_systems_ppt" >}}) {#from-lymphatic-and-immune-systems-ppt--lymphatic-and-immune-systems-ppt-dot-md}


### Two arms of the immune system {#two-arms-of-the-immune-system}


#### [Innate system]({{< relref "innate_immunity" >}}) {#innate-system--innate-immunity-dot-md}

-   Comprised of cells and proteins
-   The cells (neutrophils and monocytes/macrophages) can engulf and kill bacteria with the help of a large number of plasma protein and receptor molecules encoded in DNA
    -   These are synthesized by the [liver]({{< relref "liver" >}})


#### [Adaptive system]({{< relref "adaptive_immunity" >}}) {#adaptive-system--adaptive-immunity-dot-md}

-   Generates two families of receptors with unique antigen specifities through **DNA mutation** (adaption\*)
    -   **Greater specificity and efficacy** than the recepters of the [innate system]({{< relref "innate_immunity" >}})
-   The cells (B and T lymphocytes) have **memory** that allows a more rapid and effective response to **re-infection**


### Activation and actions - T and B lymphocytes {#activation-and-actions-t-and-b-lymphocytes}

-   All resting [lymphocytes]({{< relref "lymphocyte" >}}) are indistinguishable morphologically
    -   Sometimes identifiable based on context in tissue
    -   Upon activation, they may dramatically change their morphology


#### [T lymphocytes]({{< relref "t_lymphocyte" >}}) {#t-lymphocytes--t-lymphocyte-dot-md}

-   Activated by **recognition of a partially-digested foreign peptide displayed by [antigen presenting cells]({{< relref "antigen_presenting_cell" >}})**
    -   APCs are special phagocytic cells (similar to [macrophages]({{< relref "macrophage" >}})) that sample interstitial fluid and present partially digested peptides to T lymphocytes
-   T cells have several effector actions
    1.  Assist activated [B-cells]({{< relref "b_lymphocyte" >}}) to improve binding of their receptor and differentiation into plasma cells
    2.  Enhance the phagocytic actions of [macrophages]({{< relref "macrophage" >}})
    3.  Regulation of the immune reaction
    4.  Directly destroy infected host cells (cytotoxicity)
-   The first three are performed by [CD4-positive (helper/suppressor) T-cells]({{< relref "cd4_positive_t_lymphocyte" >}}), and the last is done by [CD8-positive (cytotoxic) T-cells]({{< relref "cd8_positive_t_lymphocyte" >}})
    -   CD4 and CD8 are surface molecules that define these T-cell subsets


#### [B lymphocyte]({{< relref "b_lymphocyte" >}}) {#b-lymphocyte--b-lymphocyte-dot-md}

-   Activated by **recognition of intact foreign molecules ([antigens]({{< relref "antigen" >}}))**


### Five basic stages in the life of a [lymphocyte]({{< relref "lymphocyte" >}}) {#five-basic-stages-in-the-life-of-a-lymphocyte--lymphocyte-dot-md}

_Note: not "official" stages_

-   These are not simply sequential stages
-   Most lymphocytes do not progress beyond stage 2 because **they are never exposed to the antigen that their receptor recognizes**
-   Memory cells cycle back to stage 2 and may reenter the activation or effector stages upon re-exposure
-   Many cells die in stages 1 and 3 and at the conclusion of stage 4
    -   Stages 1 and 3 involved controlled mutation of part of the genome
        -   These mutations are often ineffective or detrimental -> cell death
        -   The cells with beneficial mutations often survive
    -   Effectors in stage 4 are produced in large numbers to ensure an effective response to foreign exposure
        -   These cells are not needed post-exposure, so a minority become memory cells and the remainder is eliminated


#### Basic stages {#basic-stages}

1.  Birth and maturation
2.  Searching for a cognate antigen
3.  Activation
4.  Effector (e.g. fighting the threat)
5.  Memory


### Summary of actions of B and T lymphocytes {#summary-of-actions-of-b-and-t-lymphocytes}

-   Both [T-cells]({{< relref "t_lymphocyte" >}}) and [B-cells]({{< relref "b_lymphocyte" >}}) derived from [hematopoietic stem cells]({{< relref "hematopoietic_stem_cell" >}}) of the bone marrow
-   Stage 3 typically occurs in lymph nodes or other permanent lymphoid tissue, but may occur in other tissue (particularly for T cells)
-   Rearrangement is risky -> many cells die in the process
-   Most lymphocytes are eliminated when the threat abates, but some memory cells remain


#### [B lymphocytes]({{< relref "b_lymphocyte" >}}) {#b-lymphocytes--b-lymphocyte-dot-md}

-   Usually dependent on an activated [T lymphocyte]({{< relref "t_lymphocyte" >}}) to help them become activated

<!--list-separator-->

-  Life cycle

    1.  Rearrangement (VDJ recombination) of the BCR (Ig) in bone marrow
    2.  Naive and memory cells circulate and migrate in tissues (especially lymphoid) looking for cognate antigen
    3.  Activate and proliferate in germinal centers where immunoglobulin mutation (somatic hypermutation) improves specificity of the immunoglobulin with T-cell help
    4.  Effector action through plasma cells -> produce highly specific immunoglobulin that enhances actions of innate system
    5.  Many effector cells die after threat abates, some survive as memory cells and return to step 2


#### [T lymphocytes]({{< relref "t_lymphocyte" >}}) {#t-lymphocytes--t-lymphocyte-dot-md}

-   T-cell selection in the [thymus]({{< relref "thymus" >}}) reduces the likelihood of turning the immune system on yourself
-   T-cells are activated **first**

<!--list-separator-->

-  Life cycle

    1.  Rearrangement (VDJ recombination) of the TCR in [thymus]({{< relref "thymus" >}}) with extensive selection
    2.  Naive and memory cells circulate and migrate in tissues (especially lymphoid tissue) looking for cognate antigen
    3.  Recognition of peptide antigen presented by APC -> activate and proliferate in paracortex of lymph nodes
        -   No further mutation of TCR
    4.  Effector actions
    5.  Many effector cells die after threat abates, some survive as memory cells and return to step 2


#### Major actions of T and B lymphocytes table {#major-actions-of-t-and-b-lymphocytes-table}

|               | Stage 1                                                   | Stage 2                                                       | Stage 3                                                                         | Stage 4                                                                            | Stage 5                                                                                         |
|---------------|-----------------------------------------------------------|---------------------------------------------------------------|---------------------------------------------------------------------------------|------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------|
| T-lymphocytes | Birth in bone marrow, recombination of receptor in Thymus | Circulates via blood between lymph nodes actively looking     | Mostly in paracortex of lymph nodes                                             | Depends on specific subtype; may act in lymph nodes or at site of infection        | Many cells of stage 4 die, but some memory cells survive and rejoin Stage 2                     |
| B-lymphocytes | Birth and VDJ (Ig mutation) recombination in bone marrow  | Mostly in lymphoid tissue (nodes) waiting for cognate antigen | Mostly in lymphoid follicles where there is further mutation of the Ig receptor | Hypermutated B-cells develop into plasma cell in lymph node -> secrete abundant Ig | Memory plasma cells reside for many years in bone marrow; some memory B-cells return to Stage 2 |


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [FoCS Learning Objectives]({{< relref "focs_learning_objectives" >}}) {#focs-learning-objectives--focs-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Learning Objectives > Block 4 > Week 1 > <span class="timestamp-wrapper"><span class="timestamp">[2021-10-11 Mon] </span></span> > Lymphatic and Immune Systems > Pre-work**

    <!--list-separator-->

    - <span class="org-todo done _X_">[X]</span>  [Describe the basic stages and events in the life of a T and B lymphocyte (e.g. the summary given at the end of the introduction of the immune system.)]({{< relref "describe_the_basic_stages_and_events_in_the_life_of_a_t_and_b_lymphocyte_e_g_the_summary_given_at_the_end_of_the_introduction_of_the_immune_system" >}})


### Unlinked references {#unlinked-references}

[Show unlinked references]
