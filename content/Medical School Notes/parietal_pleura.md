+++
title = "Parietal pleura"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   A type of [Pleura]({{< relref "pleura" >}})
-   The outer layer that connects to the thoracic wall, the [mediastinum]({{< relref "mediastinum" >}}), and the [diaphragm]({{< relref "diaphragm" >}})


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Pleura of the lung (p. 1057)**

    [Parietal pleura]({{< relref "parietal_pleura" >}}): Outer layer that connects the thoracic wall, the mediastinum, and the diaphragm

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
