+++
title = "Tunica adventitia"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Inbox]({{< relref "inbox" >}}) {#inbox--inbox-dot-md}

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    [Tunica externa]({{< relref "tunica_adventitia" >}}) made up of:

    ---

<!--list-separator-->

-  **🔖 From Anki > Cardiology**

    Thinner [tunica adventitia]({{< relref "tunica_adventitia" >}}) that also contains collagen, elastic fibers, and [vasa vasorum]({{< relref "vasa_vasorum" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
