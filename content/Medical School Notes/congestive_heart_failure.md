+++
title = "Congestive heart failure"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Respiratory Acidosis: Primary Carbonic Acid/Carbon Dioxide Excess**

    Can result from anything that interferes with respiration, e.g. [pneumonia]({{< relref "pneumonia" >}}), [emphysema]({{< relref "emphysema" >}}), or [congestive heart failure]({{< relref "congestive_heart_failure" >}})

    ---


#### [Describe the etiology of pleuritis.]({{< relref "describe_the_etiology_of_pleuritis" >}}) {#describe-the-etiology-of-pleuritis-dot--describe-the-etiology-of-pleuritis-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy > Pleuritis**

    Typically a result of infection, [cancer]({{< relref "cancer" >}}), and [congestive heart failure]({{< relref "congestive_heart_failure" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
