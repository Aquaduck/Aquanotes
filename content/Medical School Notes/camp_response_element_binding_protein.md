+++
title = "cAMP response element binding protein"
author = ["Arif Ahsan"]
date = 2021-08-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Metabolic Effects of Insulin and Glucagon]({{< relref "metabolic_effects_of_insulin_and_glucagon" >}}) {#metabolic-effects-of-insulin-and-glucagon--metabolic-effects-of-insulin-and-glucagon-dot-md}

<!--list-separator-->

-  **🔖 Second messenger systems > Glucagon's metabolic effects**

    [PKA]({{< relref "protein_kinase_a" >}})-mediated phosphorylation of [CREB]({{< relref "camp_response_element_binding_protein" >}}) -> increased transcription of [gluconeogenic]({{< relref "gluconeogenesis" >}}) enzymes if liver glycogen stores become depleted

    ---

<!--list-separator-->

-  **🔖 Second messenger systems > Adenylate cyclase > Takeaways**

    [PKA]({{< relref "protein_kinase_a" >}}) enters nucleus and phosphorylates [cAMP response element binding protein]({{< relref "camp_response_element_binding_protein" >}}) (CREB)

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
