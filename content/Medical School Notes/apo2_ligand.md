+++
title = "Apo2 ligand"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) in [TRAIL]({{< relref "apo2_ligand" >}})/TRAILR Signaling

    -   Both TRADD and [RIP]({{< relref "receptor_interacting_protein" >}}) were demonstrated to be recruited to the TRAILR complex in the [DISC]({{< relref "death_inducing_signaling_complex" >}})
        -   The recruitment of [FADD]({{< relref "fas_associated_death_domain" >}}) was increased in the absence of TRADD, indicating that:
            1.  Recruitment of RIP to the TRAILR complex is TRADD-dependent
            2.  **The binding of TRADD and FADD to TRAILR is most likely independent and competitive**


#### [Apo2L/TRAIL: apoptosis signaling, biology, and potential for cancer therapy]({{< relref "apo2l_trail_apoptosis_signaling_biology_and_potential_for_cancer_therapy" >}}) {#apo2l-trail-apoptosis-signaling-biology-and-potential-for-cancer-therapy--apo2l-trail-apoptosis-signaling-biology-and-potential-for-cancer-therapy-dot-md}

<!--list-separator-->

-  Apoptosis signaling by [Apo2L]({{< relref "apo2_ligand" >}})/TRAIL

    -   Similar to [FasL]({{< relref "fas_ligand" >}}), TRAIL initiates apoptosis upon binding to its cognate death receptors by inducing the recruitment of specific cytoplasmic proteins to the intracellular death domain of the receptor -> formation of [DISC]({{< relref "death_inducing_signaling_complex" >}})

<!--list-separator-->

-  **🔖 Abstract**

    Apo2 ligand or tumor necrosis factor (TNF)-related apoptosis-inducing ligand ([Apo2L/TRAIL]({{< relref "apo2_ligand" >}})) is one of several members of the TNF gene superfamily that induce apoptosis through engagement of death receptors

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
