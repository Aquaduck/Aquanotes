+++
title = "Recurrent laryngeal nerve"
author = ["Arif Ahsan"]
date = 2021-10-28T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Describe the relevant anatomy and disease etiology and where applicable, the clinical presentation of disorders associated with the superior mediastinum.]({{< relref "describe_the_relevant_anatomy_and_disease_etiology_and_where_applicable_the_clinical_presentation_of_disorders_associated_with_the_superior_mediastinum" >}}) {#describe-the-relevant-anatomy-and-disease-etiology-and-where-applicable-the-clinical-presentation-of-disorders-associated-with-the-superior-mediastinum-dot--describe-the-relevant-anatomy-and-disease-etiology-and-where-applicable-the-clinical-presentation-of-disorders-associated-with-the-superior-mediastinum-dot-md}

<!--list-separator-->

-  **🔖 From Clinically Oriented Anatomy**

    <!--list-separator-->

    -  Injury to [recurrent laryngeal nerve]({{< relref "recurrent_laryngeal_nerve" >}})

        -   Recurrent laryngeal nerve supplies all intrinsic muscle of larynx except one
            -   Any diagnostic procedure or disease in superior mediastinum may injure these nerves -> affect voice
        -   Left recurrent laryngeal nerve winds around arch of aorta and ascends between trachea and esophagus -> may be involved in:
            1.  Bronchogenic or esophageal carcinoma
            2.  Enlargement of mediastinal lymph nodes
            3.  Aneurysm of arch of aorta
                -   Nerve may be stretched by the dilated arch


### Unlinked references {#unlinked-references}

[Show unlinked references]
