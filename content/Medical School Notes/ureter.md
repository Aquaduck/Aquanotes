+++
title = "Ureter"
author = ["Arif Ahsan"]
date = 2021-10-31T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Identify nephron anatomy (different parts of nephron)]({{< relref "identify_nephron_anatomy_different_parts_of_nephron" >}}) {#identify-nephron-anatomy--different-parts-of-nephron----identify-nephron-anatomy-different-parts-of-nephron-dot-md}

<!--list-separator-->

-  **🔖 From Vander's Renal Physiology > The Renal tubule (p. 18) > Cortical collecting duct**

    Each [renal calyx]({{< relref "renal_calyx" >}}) is continuous with the [ureter]({{< relref "ureter" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
