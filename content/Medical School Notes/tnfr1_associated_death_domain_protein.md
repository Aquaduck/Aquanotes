+++
title = "TNFR1-associated death domain protein"
author = ["Arif Ahsan"]
date = 2021-09-27T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   Contains a [Death domain]({{< relref "death_domain" >}})


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [TNFR1]({{< relref "tnfr1" >}}) {#tnfr1--tnfr1-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Signalling to [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) is cell-type specific

    ---


#### [The role of TRADD in death receptor signaling]({{< relref "the_role_of_tradd_in_death_receptor_signaling" >}}) {#the-role-of-tradd-in-death-receptor-signaling--the-role-of-tradd-in-death-receptor-signaling-dot-md}

<!--list-separator-->

-  [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) and Other Death Receptors

    -   [Fas]({{< relref "fas" >}})'s main function is to induce apoptosis via formation of a DISC following binding of [FasL]({{< relref "fas_ligand" >}})
        -   Includes FADD and caspase-8
    -   TRADD was never reported as a component of the FAS DISC
        -   In line with this, apoptosis induced by anti-FAS antibody was unaffected in TRADD-deficient mouse embryonic fibroblasts and thymocytes

<!--list-separator-->

-  [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) in [TRAIL]({{< relref "apo2_ligand" >}})/TRAILR Signaling

    -   Both TRADD and [RIP]({{< relref "receptor_interacting_protein" >}}) were demonstrated to be recruited to the TRAILR complex in the [DISC]({{< relref "death_inducing_signaling_complex" >}})
        -   The recruitment of [FADD]({{< relref "fas_associated_death_domain" >}}) was increased in the absence of TRADD, indicating that:
            1.  Recruitment of RIP to the TRAILR complex is TRADD-dependent
            2.  **The binding of TRADD and FADD to TRAILR is most likely independent and competitive**

<!--list-separator-->

-  [TRADD]({{< relref "tnfr1_associated_death_domain_protein" >}}) in [TNF]({{< relref "tumor_necrosis_factor" >}})/[TNFR1]({{< relref "tnfr1" >}}) signalling

    -   The default signaling pathway activating by the binding of TNF alpha is the induction of [NFκB]({{< relref "nfκb" >}}) -> responsible for **cell survival and growth**
    -   TNFR1 induces cell death (apoptosis AND necrosis) **only when [NFκB]({{< relref "nfκb" >}}) activation is impaired**
    -   Dependence of TNFR1 signaling on TRADD appears to be **cell-type specific**


### Unlinked references {#unlinked-references}

[Show unlinked references]
