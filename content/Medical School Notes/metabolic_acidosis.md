+++
title = "Metabolic acidosis"
author = ["Arif Ahsan"]
date = 2021-10-16T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [List the features and functions of the following: (Muscle Intermediary Metabolism II Pre-work 1)]({{< relref "list_the_features_and_functions_of_the_following_muscle_intermediary_metabolism_ii_pre_work_1" >}}) {#list-the-features-and-functions-of-the-following--muscle-intermediary-metabolism-ii-pre-work-1----list-the-features-and-functions-of-the-following-muscle-intermediary-metabolism-ii-pre-work-1-dot-md}

<!--list-separator-->

-  **🔖 Beta-oxidation > From Fat Metabolism in Muscle & Adipose Tissue**

    **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**

    ---

<!--list-separator-->

-  **🔖 Triacylglycerol > From Fat Metabolism in Muscle & Adipose Tissue > Fatty acid catabolism > Oxidation phase**

    **A problem with either the [biotin]({{< relref "biotin" >}})-dependent enzyme ([propionyl-CoA carboxylase]({{< relref "propionyl_coa_carboxylase" >}})) or the [B12]({{< relref "cobalamin" >}})-dependent enzyme ([methylmalonyl-CoA mutase]({{< relref "methylmalonyl_coa_mutase" >}})) in beta-oxidation -> profound [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) caused by:**

    ---


#### [Ganong's Review of Medical Physiology]({{< relref "ganong_s_review_of_medical_physiology" >}}) {#ganong-s-review-of-medical-physiology--ganong-s-review-of-medical-physiology-dot-md}

<!--list-separator-->

-  **🔖 Changes in Ventilation (p. 664)**

    This provides **respiratory compensation** for [metabolic acidosis]({{< relref "metabolic_acidosis" >}}) produced by increased lactic acid

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
