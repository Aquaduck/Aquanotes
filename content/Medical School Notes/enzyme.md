+++
title = "Enzyme"
author = ["Arif Ahsan"]
date = 2021-07-13T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Overview {#overview}

-   Enzymes act as catalysts -> speed up reactions
-   _Isoenzyme_: Enzyme that has a different [AA]({{< relref "amino_acid" >}}) sequence but catalyzes the same chemical reaction


## Backlinks {#backlinks}


### 29 linked references {#29-linked-references}


#### [Zymogen]({{< relref "zymogen" >}}) {#zymogen--zymogen-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    An inactive precursor to an [enzyme]({{< relref "enzyme" >}})

    ---


#### [Osmosis - Gluconeogenesis]({{< relref "osmosis_gluconeogenesis" >}}) {#osmosis-gluconeogenesis--osmosis-gluconeogenesis-dot-md}

<!--list-separator-->

-  **🔖 Alcoholism affect on gluconeogenesis**

    Increased NADH -> signal the lactic, malate, and glycerol-3-phosphate DH [enzymes]({{< relref "enzyme" >}}) in gluconeogenesis to go in reverse

    ---

<!--list-separator-->

-  **🔖 Notes > Gluconeogenesis steps**

    Performed by _[glucose-6-phosphatase]({{< relref "enzyme" >}})_ in the endoplasmic reticulum

    ---

<!--list-separator-->

-  **🔖 Notes > Gluconeogenesis steps**

    Fructose-1,6-bisphosphate -> fructose-6-phosphate via _[fructose-1,6-bisphosphatase]({{< relref "enzyme" >}})_

    ---

<!--list-separator-->

-  **🔖 Notes > Gluconeogenesis steps**

    DHAP converted to fructose-1,6-bisphosphate by a reversible reaction via _[aldolase]({{< relref "enzyme" >}})_

    ---

<!--list-separator-->

-  **🔖 Notes > Fat metabolism**

    Low levels of insulin, presence of epinephrine and ACTH -> adipocytes stimulate [hormone-sensitive lipase]({{< relref "enzyme" >}}) (HSL) -> break down triacylgyceride into 3 fatty acids + glycerol

    ---

<!--list-separator-->

-  **🔖 Notes > Sources of pyruvate**

    Transaminase [enzymes]({{< relref "enzyme" >}}) require _pyridoxine ([vitamin B<sub>6</sub>]({{< relref "pyridoxine" >}}))_ as a cofactor

    ---


#### [Michaelis-Menten Kinetics]({{< relref "michaelis_menten_kinetics" >}}) {#michaelis-menten-kinetics--michaelis-menten-kinetics-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    **Michaelis-Menten kinetics** models [enzyme]({{< relref "enzyme" >}}) kinetics

    ---


#### [Khan - Steady states and the Michaelis Menten equation]({{< relref "khan_steady_states_and_the_michaelis_menten_equation" >}}) {#khan-steady-states-and-the-michaelis-menten-equation--khan-steady-states-and-the-michaelis-menten-equation-dot-md}

<!--list-separator-->

-  **🔖 Notes > Steady-state assumption**

    [Enzyme]({{< relref "enzyme" >}}) kinematics equation:
      \\(E + S \overset{1}{\rightleftharpoons} ES \overset{2}{\rightleftharpoons} E + P\\)

    ---


#### [Khan - Basics of enzyme kinematics]({{< relref "khan_basics_of_enzyme_kinematics" >}}) {#khan-basics-of-enzyme-kinematics--khan-basics-of-enzyme-kinematics-dot-md}

<!--list-separator-->

-  [Enzyme]({{< relref "enzyme" >}}) kinetics inhibitors

    -   _Competitive inhibitors_: impair reaction by **binding to an enzyme and preventing the real substrate from binding**
        -   Often binds at the active site
    -   _Noncompetitive inhibitors_: **does not impact substrate binding**, but binds elsewhere and prevents enzyme function

        {{< figure src="/ox-hugo/_20210713_155056screenshot.png" caption="Figure 1: Difference in rate of reaction between normal enzymes, competitive inhibition, and noncompetitive inhibition" width="400" >}}

    <!--list-separator-->

    -  With a competitive inhibitor

        -   Reaction is able to reach its normal V<sub>max</sub>, but takes a higher concentration of substrate to get there
            -   V<sub>max</sub> is unchanged, but apparenty K<sub>m</sub> is higher.
        -   Why must more substrate be added in order to reach V<sub>max</sub>?
            -   Extra substrate makes substrate molecules abundant enough to consistently "beat" inhibitor molecules to the enzyme

    <!--list-separator-->

    -  With a noncompetitive inhibitor

        -   Reaction never reaches its normal V<sub>max</sub>, regardless of how much substrate
            -   Due to inhibition, effective concentration of enzyme is reduced
        -   Reaction reaches half if its new V<sub>max</sub> at the same substrate concentration -> **K<sub>m</sub> is unchanged**
            -   This is because the inhibitor doesn't affect binding of enzyme to substrate, just lowers concentration of usable enzyme

<!--list-separator-->

-  Basic [enzyme]({{< relref "enzyme" >}}) kinetics graphs

    {{< figure src="/ox-hugo/_20210713_154751screenshot.png" caption="Figure 2: Graph of hypothetical enzyme activity" width="400" >}}


#### [Kaplan Biochemistry Chapter 2]({{< relref "kaplan_biochemistry_chapter_2" >}}) {#kaplan-biochemistry-chapter-2--kaplan-biochemistry-chapter-2-dot-md}

<!--list-separator-->

-  **🔖 2.2 Mechanisms of Enzyme Activity > Cofactors and coenzymes**

    _Prosthetic group_: Tightly bound cofactors/coenzymes necessary for [enzyme]({{< relref "enzyme" >}}) function

    ---

<!--list-separator-->

-  **🔖 2.2 Mechanisms of Enzyme Activity > Cofactors and coenzymes**

    _Holoenzyme_: [enzyme]({{< relref "enzyme" >}}) with cofactor

    ---

<!--list-separator-->

-  **🔖 2.2 Mechanisms of Enzyme Activity > Cofactors and coenzymes**

    _Apoenzyme_: [enzyme]({{< relref "enzyme" >}}) without its cofactor

    ---

<!--list-separator-->

-  **🔖 2.2 Mechanisms of Enzyme Activity > Cofactors and coenzymes**

    Nonprotein molecules that increase effectiveness of [enzymes]({{< relref "enzyme" >}})

    ---

<!--list-separator-->

-  **🔖 2.1 Enzymes as Biological Catalysts > Enzyme classifications**

    <!--list-separator-->

    -  6 categories of [enzymes]({{< relref "enzyme" >}}) mechanisms

        <!--list-separator-->

        -  Oxidoreductases

            -   Catalyze _oxidation-reduction_ reactions - the transfer of electrions between biological molecules
            -   Often have a cofactor that acts as an electron carrier
                -   e.g NAD+ or NADP+
            -   _Reductant_: electron donor
            -   _Oxidant_: electron acceptor
            -   Enzymes with _dehydrogenase_ or _reductase_ in their names are usually oxidoreductases

        <!--list-separator-->

        -  Transferases

            -   Catalyze movement of a functional group from one molecule to another
            -   _Kinase_: transferase that moves phosphate

        <!--list-separator-->

        -  Hydrolases

            -   Catalyze the breaking of a compound into two molecules using the addition of **water**
            -   Most are named only for their substrate
                -   e.g. phosphatase

        <!--list-separator-->

        -  Lyases

            -   Catalyze cleavage of a single molecule into two products
            -   **Do not require water** and **do not act as oxidoreductases**
            -   Synthesis of two molecules may also be catalyzed by a lyase
                -   If so, referred to as _synthases_

        <!--list-separator-->

        -  Isomerases

            -   Catalyze the rearrangement of bonds within a molecule
            -   Some can also be classified as _oxidoreductases_, _transferases_, or _lyases_
            -   Work on both stereoisomers and constitutional [isomers]({{< relref "isomers" >}})

        <!--list-separator-->

        -  Ligases

            -   Catalyze **addition or synthesis reactions**
                -   Generally between large similar molecules
                -   Often require ATP
            -   Synthesis reactions of smaller molecules are generally accomplished by _lyases_

<!--list-separator-->

-  **🔖 2.1 Enzymes as Biological Catalysts**

    [Enzymes]({{< relref "enzyme" >}}) are important as _catalysts_

    ---


#### [Kaplan Biochemistry Chapter 10]({{< relref "kaplan_biochemistry_chapter_10" >}}) {#kaplan-biochemistry-chapter-10--kaplan-biochemistry-chapter-10-dot-md}

<!--list-separator-->

-  **🔖 10.2 Reactions of the Citric Acid Cycle > Key Reactions > 8) Oxaloacetate Formed Anew**

    _[malate dehydrogenase]({{< relref "enzyme" >}})_ catalyzes the oxidation of malate to _oxaloacetate_

    ---

<!--list-separator-->

-  **🔖 10.2 Reactions of the Citric Acid Cycle > Key Reactions > 7) Malate Formation**

    _[fumarase]({{< relref "enzyme" >}})_ catalyzes hydrolysis of **alkene bond in fumarate** -> _malate_

    ---

<!--list-separator-->

-  **🔖 10.2 Reactions of the Citric Acid Cycle > Key Reactions > 6) Fumarate Formation**

    Catalyzed by _[succinate dehydrogenase]({{< relref "enzyme" >}})_

    ---

<!--list-separator-->

-  **🔖 10.2 Reactions of the Citric Acid Cycle > Key Reactions > 5) Succinate Formation**

    _[nucleosidediphosphate kinase]({{< relref "enzyme" >}})_ catalyzes phosphate transfer from GTP to ADP -> producing ATP

    ---

<!--list-separator-->

-  **🔖 10.2 Reactions of the Citric Acid Cycle > Key Reactions > 5) Succinate Formation**

    Succinate formation catalyzed by _[succinyl-CoA synthetase]({{< relref "enzyme" >}})_

    ---

<!--list-separator-->

-  **🔖 10.2 Reactions of the Citric Acid Cycle > Key Reactions > 3) \\(\alpha\\)-Ketoglutarate and CO<sub>2</sub> Formation**

    Isocitrate oxidized to _oxalosuccinate_ by _[isocitrate dehydrogenase]({{< relref "enzyme" >}})_

    ---

<!--list-separator-->

-  **🔖 10.2 Reactions of the Citric Acid Cycle > Key Reactions > 2) Citrate Isomerized to Isocitrate**

    Citrate binds at three points to _[aconitase]({{< relref "enzyme" >}})_

    ---

<!--list-separator-->

-  **🔖 10.1 Acetyl-CoA > Methods of Forming Acetyl-CoA**

    <!--list-separator-->

    -  Pyruvate dehydrogenase complex [enzymes]({{< relref "enzyme" >}}):

        1.  _Pyruvate dehydrogenase (PDH)_:
            -   Pyruvate is oxidized -> CO<sub>2</sub>
            -   Remaining 2-carbon molecule binds covalently to _thiamine pyrophosphate_ (vitamin B<sub>1</sub>, TPP)
            -   TPP is a coenzyme held by noncovalent interactions to PDH
            -   Mg<sup>2+</sup> is also required
        2.  _Dihydrolipoyl transacetylase_:
            -   2-carbon molecule bonded to TPP oxidized and transferred to _[lipoic acid]({{< relref "lipoic_acid" >}})_, a coenzyme
                -   Lipoic acid's disulfide group oxidizes -> creates acetyl group bonded to lipoic acid via thioester linkage
                -   Then, catalyzes CoA-SH interaction w/ newly formed thioester link
                    -   Transfer of acetyl group to form acetyl-CoA, leaving lipoic acid reduced
        3.  _Dihydrolipoyl dehydrogenase_:
            -   FAD used as coenzyme to **reoxidize [lipoic acid]({{< relref "lipoic_acid" >}})** -> allows lipoic acid to be reused
                -   FAD reduced to FADH<sub>2</sub>
                -   In subsequent reactions, FADH<sub>2</sub> reoxidized to FAD while NAD<sup>+</sup> reduced to NADH

        {{< figure src="/home/kita/Documents/logseq-org-roam-wiki/hugo/content-org/medschool/_20210727_120744screenshot.png" caption="Figure 3: Mechanism of pyruvate dehydrogenase" >}}


#### [Jumpstart VIC Hybrid Learning Objectives]({{< relref "jumpstart_vic_hybrid_learning_objectives" >}}) {#jumpstart-vic-hybrid-learning-objectives--jumpstart-vic-hybrid-learning-objectives-dot-md}

<!--list-separator-->

-  **🔖 Proteins and Enzymes > 6. Define and describe the roles of the following enzyme-related terms: > Isozymes**

    [Enzyme]({{< relref "enzyme" >}})

    ---


#### [How Enzymes Work]({{< relref "how_enzymes_work" >}}) {#how-enzymes-work--how-enzymes-work-dot-md}

<!--list-separator-->

-  **🔖 Objective:**

    Understand the effect of [enzymes]({{< relref "enzyme" >}}) on the activation energy of reactions, and why this makes these chemical reactions proceed more quickly

    ---


#### [Biotin]({{< relref "biotin" >}}) {#biotin--biotin-dot-md}

<!--list-separator-->

-  **🔖 From First Aid Section II Biochemistry Nutrition > Function**

    Cofactor for carboxylation [enzymes]({{< relref "enzyme" >}})/carboxylases (which add a 1-carbon group):

    ---


#### [Allosteric regulation]({{< relref "allosteric_regulation" >}}) {#allosteric-regulation--allosteric-regulation-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of regulation found in [enzymes]({{< relref "enzyme" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
