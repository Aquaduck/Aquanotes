+++
title = "Cell"
author = ["Arif Ahsan"]
date = 2021-07-29T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 6 linked references {#6-linked-references}


#### [Osmosis - Cellular Structure & Processes]({{< relref "osmosis_cellular_structure_processes" >}}) {#osmosis-cellular-structure-and-processes--osmosis-cellular-structure-processes-dot-md}

<!--list-separator-->

-  [Cell]({{< relref "cell" >}}) Membrane

    -   Semipermeable membrane made from phospholipid bilayer; surrounds cell cytoplasm

    <!--list-separator-->

    -  Phospholipid bilayer

        -   Two-layered polar phospholipid molecules comprising two parts:
            1.  **Negatively charged phosphate head**
                -   Hydrophilic and oriented outwards
            2.  **Fatty acid tail**
                -   Hydrophobic and oriented inwards
        -   **Semipermeable**
            -   Allows passage of certain molecules (e.g. small molecules) and denies passage for others (e.g. large molecules)

        {{< figure src="/ox-hugo/_20210715_191419screenshot.png" caption="Figure 1: Phospholipid parts and their arrangement in a cell membrane" >}}

<!--list-separator-->

-  [Cellular]({{< relref "cell" >}}) Structure & Function

    {{< figure src="/ox-hugo/_20210715_191656screenshot.png" >}}

    <!--list-separator-->

    -  Cell Structure Basics

        -   Cells are the basic structural, biological, functional unit that comprise organisms
        -   Smallest self-replicating life-form
        -   Cells -> tissue -> organ -> organ systems -> organism

    <!--list-separator-->

    -  Cytosol

        -   Intracellular fluid
            -   Composition:
                -   Water
                -   Dissolved/suspended organic and inorganic chemicals
                -   Macromolecules
                -   Pigments
                -   Organelles
        -   **Site of most cellular activity**

    <!--list-separator-->

    -  Organelles

        -   Specialized cellular subunits carry out essential function

        <!--list-separator-->

        -  Ribosomes

            -   Composition: rRNA, ribosomal proteins
            -   Can exist freely in cytoplasm/bound to endoplasmic reticulum (forms rough endoplasmic reticuluml)
            -   **Turns mRNA into proteins via translation**
            -   Organized into two subunits (40s, 60s)
                -   _Small subunit_: binding sites for mRNA, tRNA
                -   _Large subunit_: contains ribozyme to catalyze peptide bond formation


#### [Organelle]({{< relref "organelle" >}}) {#organelle--organelle-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    Components of the [cell]({{< relref "cell" >}})

    ---


#### [Factors Affecting Hemoglobin Dissociation Curve]({{< relref "factors_affecting_hemoglobin_dissociation_curve" >}}) {#factors-affecting-hemoglobin-dissociation-curve--factors-affecting-hemoglobin-dissociation-curve-dot-md}

<!--list-separator-->

-  **🔖 Notes > Factors affecting O<sub>2</sub>-Hb Dissociation Curve: > Temperature**

    [Cells]({{< relref "cell" >}}) that have a high rate of metabolism produce more thermal energy as a byproduct

    ---


#### [Cytoplasm]({{< relref "cytoplasm" >}}) {#cytoplasm--cytoplasm-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A component of the [cell]({{< relref "cell" >}})

    ---


#### [Apoptosis]({{< relref "apoptosis" >}}) {#apoptosis--apoptosis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    The programmed death of a [cell]({{< relref "cell" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
