+++
title = "Blood clotting"
author = ["Arif Ahsan"]
date = 2021-10-12T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Concept links {#concept-links}

-   The process of clotting [blood]({{< relref "blood" >}}) to close wounds and prevent excessive bleeding


## Backlinks {#backlinks}


### 5 linked references {#5-linked-references}


#### [Thrombosis]({{< relref "thrombosis" >}}) {#thrombosis--thrombosis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    When unwanted [blood clots]({{< relref "blood_clotting" >}}) block blood vessels

    ---


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Disorders of Platelets (p. 813)**

    Thrombocytosis may lead to the formation of unwanted [blood clots]({{< relref "blood_clotting" >}}) ([thrombosis]({{< relref "thrombosis" >}}))

    ---

<!--list-separator-->

-  **🔖 Plasma Proteins (p. 796) > Fibrinogen**

    Essential for [blood clotting]({{< relref "blood_clotting" >}})

    ---


#### [Fibrinolysis]({{< relref "fibrinolysis" >}}) {#fibrinolysis--fibrinolysis-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Final step in [clotting]({{< relref "blood_clotting" >}}) to restore normal circulation by breaking down the clot

    ---


#### [Fibrinogen]({{< relref "fibrinogen" >}}) {#fibrinogen--fibrinogen-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    Essential for [blood clotting]({{< relref "blood_clotting" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
