+++
title = "Shunt"
author = ["Arif Ahsan"]
date = 2021-10-26T00:00:00-04:00
tags = ["medschool", "concept", "patho"]
draft = false
+++

## Concept links {#concept-links}

-   Pathological connections between the right and left [Heart]({{< relref "heart" >}}) chambers
-   Results in [cyanosis]({{< relref "cyanosis" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Right-to-left shunt]({{< relref "right_to_left_shunt" >}}) {#right-to-left-shunt--right-to-left-shunt-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A class of [shunts]({{< relref "shunt" >}}) where [blood]({{< relref "blood" >}}) fails to be oxygenated -> deoxygenated blood enters the [systemic circuit]({{< relref "systemic_circuit" >}})

    ---


#### [Left-to-right shunt]({{< relref "left_to_right_shunt" >}}) {#left-to-right-shunt--left-to-right-shunt-dot-md}

<!--list-separator-->

-  **🔖 Concept links**

    A type of cardiac [Shunt]({{< relref "shunt" >}})

    ---


#### [Amboss]({{< relref "amboss" >}}) {#amboss--amboss-dot-md}

<!--list-separator-->

-  **🔖 Cardiology**

    <!--list-separator-->

    -  [Shunts]({{< relref "shunt" >}})

        -   Pathological connections between the right and left heart chambers
        -   Results in blood bypassing part of circulation leading to oxygenated or deoxygenated blood in the wrong part of the cardiac cycle
        -   V/Q ratio = 0

        <!--list-separator-->

        -  [Right-to-left shunt]({{< relref "right_to_left_shunt" >}})

            -   Blood **fails to be oxygenated** -> **deoxygenated blood enters the systemic circuit**


### Unlinked references {#unlinked-references}

[Show unlinked references]
