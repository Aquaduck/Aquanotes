+++
title = "Heme"
author = ["Arif Ahsan"]
date = 2021-10-17T00:00:00-04:00
tags = ["medschool", "concept"]
draft = false
+++

## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Openstax Anatomy & Physiology]({{< relref "openstax_anatomy_physiology" >}}) {#openstax-anatomy-and-physiology--openstax-anatomy-physiology-dot-md}

<!--list-separator-->

-  **🔖 Oxygen Dissociation from Hemoglobin (p. 1074)**

    **Oxygen affinity to [heme]({{< relref "heme" >}}) increases as more oxygen molecules are bound**

    ---

<!--list-separator-->

-  **🔖 Oxygen Dissociation from Hemoglobin (p. 1074)**

    _Oxygen-hemoglobin dissociation curve_: graph that describes the relationship of partial pressure to the binding of oxygen to [heme]({{< relref "heme" >}}) vs. its subsequent dissociation from heme

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
