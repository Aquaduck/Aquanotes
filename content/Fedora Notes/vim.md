+++
title = "Vim"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## **Overview** {#overview}

-   **Vim** is a terminal-based text editor that uses [Modal Editing]({{< relref "modal_editing" >}})


## Comparison to Kakoune {#comparison-to-kakoune}

-   I currently use Vim over [Kakoune]({{< relref "Kakoune" >}}) due to familiarity


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
