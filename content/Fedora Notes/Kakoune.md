+++
title = "Kakoune"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Kakoune {#kakoune}

**Kakoune** is a [modal text editor]({{< relref "modal_editing" >}}) similar to vi or vim, but addressing the faults in the modal language of vim


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [Vim]({{< relref "vim" >}}) {#vim--vim-dot-md}

<!--list-separator-->

-  **🔖 Comparison to Kakoune**

    I currently use Vim over [Kakoune]({{< relref "Kakoune" >}}) due to familiarity

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
