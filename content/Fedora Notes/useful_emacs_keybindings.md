+++
title = "Useful Emacs Notes"
author = ["Arif Ahsan"]
date = 2021-07-21T00:00:00-04:00
tags = ["fedora"]
draft = false
+++

## Bindings {#bindings}

| Binding   | Function               | Description                                          |
|-----------|------------------------|------------------------------------------------------|
| SPC-m-l-l | org-insert-link        | Prompts for link, then for description in minibuffer |
| C-M-j     | ivy-immediate-done     | Disables autocompletion for a minibuffer             |
| SPC-s-d   | counsel-grep-or-swiper | Searches current directory for provided text         |


## Notes {#notes}

-   If you get "wrong hash-table-p nil" (e.g. when trying to attach images) run "org-id-update-id-locations"


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
