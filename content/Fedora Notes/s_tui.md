+++
title = "s-tui"
author = ["Arif Ahsan"]
date = 2021-07-30T00:00:00-04:00
tags = ["fedora"]
draft = false
+++

[Github](https://github.com/amanusk/s-tui)


## Overview {#overview}

-   Stress-Terminal UI, s-tui, monitors CPU temperature, frequency, power and utilization in a graphical way from the terminal.


## Personal notes {#personal-notes}

-   I use it to monitor CPU throttling and effectiveness of [throttled]({{< relref "throttled" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
