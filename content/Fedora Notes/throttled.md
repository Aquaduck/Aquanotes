+++
title = "throttled"
author = ["Arif Ahsan"]
date = 2021-07-30T00:00:00-04:00
tags = ["fedora"]
draft = false
+++

[Github](https://github.com/erpalma/throttled)


## Overview {#overview}

-   Throttled is a fix for certain thinkpad laptops running under linux whose CPUs are in a permanently throttled state


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [s-tui]({{< relref "s_tui" >}}) {#s-tui--s-tui-dot-md}

<!--list-separator-->

-  **🔖 Personal notes**

    I use it to monitor CPU throttling and effectiveness of [throttled]({{< relref "throttled" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
