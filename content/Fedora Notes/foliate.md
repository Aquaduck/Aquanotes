+++
title = "Foliate"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Overview {#overview}

-   Foliate is an ePub reader for linux desktops
-   I am currently using it to read the Kaplan MCAT book to test it out


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
