+++
title = "Modal Editing"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Overview {#overview}

-   **Modal Editing** is a type of functionality that involves multiple modes in order to better facilitate editing and writing text files


## Backlinks {#backlinks}


### 2 linked references {#2-linked-references}


#### [Vim]({{< relref "vim" >}}) {#vim--vim-dot-md}

<!--list-separator-->

-  **🔖 **Overview****

    **Vim** is a terminal-based text editor that uses [Modal Editing]({{< relref "modal_editing" >}})

    ---


#### [Kakoune]({{< relref "Kakoune" >}}) {#kakoune--kakoune-dot-md}

<!--list-separator-->

-  **🔖 Kakoune**

    **Kakoune** is a [modal text editor]({{< relref "modal_editing" >}}) similar to vi or vim, but addressing the faults in the modal language of vim

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
