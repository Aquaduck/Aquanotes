+++
title = "Starfleet"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Overview {#overview}

-   **Starfleet** was the deep space exploratory and defense service maintained by the [United Federations of Planets]({{< relref "United Federations of Planets" >}})


## Backlinks {#backlinks}


### 3 linked references {#3-linked-references}


#### [Galaxy-class]({{< relref "galaxy_class" >}}) {#galaxy-class--galaxy-class-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    The **Galaxy**-class was a [Starfleet]({{< relref "starfleet" >}}) vessel first introduced in the mid 2360s

    ---


#### [United Federations of Planets]({{< relref "United Federations of Planets" >}}) {#united-federations-of-planets--united-federations-of-planets-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    [Starfleet]({{< relref "starfleet" >}}) was incorporated to maintain exploratory, scientific, diplomatic, and defense functions

    ---


#### [USS Enterprise (NCC-1701-D)]({{< relref "USS Enterprise (NCC-1701-D)" >}}) {#uss-enterprise--ncc-1701-d----uss-enterprise-ncc-1701-d--dot-md}

<!--list-separator-->

-  **🔖 Overview**

    The \*USS Enterprise (NCC-1701-D) was a 24th century [Federation]({{< relref "United Federations of Planets" >}}) [Galaxy-class]({{< relref "galaxy_class" >}}) starship operated by [Starfleet]({{< relref "starfleet" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
