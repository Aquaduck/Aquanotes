+++
title = "Galaxy-class"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Overview {#overview}

-   The **Galaxy**-class was a [Starfleet]({{< relref "starfleet" >}}) vessel first introduced in the mid 2360s
-   It was one of the largest and most powerful [Federation]({{< relref "United Federations of Planets" >}}) starship classes of its time


## Backlinks {#backlinks}


### 1 linked reference {#1-linked-reference}


#### [USS Enterprise (NCC-1701-D)]({{< relref "USS Enterprise (NCC-1701-D)" >}}) {#uss-enterprise--ncc-1701-d----uss-enterprise-ncc-1701-d--dot-md}

<!--list-separator-->

-  **🔖 Overview**

    The \*USS Enterprise (NCC-1701-D) was a 24th century [Federation]({{< relref "United Federations of Planets" >}}) [Galaxy-class]({{< relref "galaxy_class" >}}) starship operated by [Starfleet]({{< relref "starfleet" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
