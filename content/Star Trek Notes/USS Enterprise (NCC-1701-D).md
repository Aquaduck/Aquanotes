+++
title = "USS Enterprise (NCC-1701-D)"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Overview {#overview}

-   The \*USS Enterprise (NCC-1701-D) was a 24th century [Federation]({{< relref "United Federations of Planets" >}}) [Galaxy-class]({{< relref "galaxy_class" >}}) starship operated by [Starfleet]({{< relref "starfleet" >}})
-   It is the fifth Federation starship to bear the name _Enterprise_


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
