+++
title = "United Federations of Planets"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Overview {#overview}

-   The **United Federations of Planets** (abbreviated as UFP and commonly referred to as the **Federation**) was a supranational interstellar union of multiple planetary nation-states that operated semi-autonomously under a single central government
    -   This central government was founded on the principles of:
        -   liberty
        -   equality
        -   peace
        -   justice
        -   progress
-   The goal of the Federation was to further the universal rights of all sentient life.
-   [Starfleet]({{< relref "starfleet" >}}) was incorporated to maintain exploratory, scientific, diplomatic, and defense functions


## Backlinks {#backlinks}


### 4 linked references {#4-linked-references}


#### [Vulcans]({{< relref "vulcans" >}}) {#vulcans--vulcans-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    In 2161, their homeworld became a founding member of the [United Federations of Planets]({{< relref "United Federations of Planets" >}})

    ---


#### [Starfleet]({{< relref "starfleet" >}}) {#starfleet--starfleet-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    **Starfleet** was the deep space exploratory and defense service maintained by the [United Federations of Planets]({{< relref "United Federations of Planets" >}})

    ---


#### [Galaxy-class]({{< relref "galaxy_class" >}}) {#galaxy-class--galaxy-class-dot-md}

<!--list-separator-->

-  **🔖 Overview**

    It was one of the largest and most powerful [Federation]({{< relref "United Federations of Planets" >}}) starship classes of its time

    ---


#### [USS Enterprise (NCC-1701-D)]({{< relref "USS Enterprise (NCC-1701-D)" >}}) {#uss-enterprise--ncc-1701-d----uss-enterprise-ncc-1701-d--dot-md}

<!--list-separator-->

-  **🔖 Overview**

    The \*USS Enterprise (NCC-1701-D) was a 24th century [Federation]({{< relref "United Federations of Planets" >}}) [Galaxy-class]({{< relref "galaxy_class" >}}) starship operated by [Starfleet]({{< relref "starfleet" >}})

    ---


### Unlinked references {#unlinked-references}

[Show unlinked references]
