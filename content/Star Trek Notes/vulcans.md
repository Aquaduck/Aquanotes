+++
title = "Vulcans"
author = ["Arif Ahsan"]
date = 2021-07-07T00:00:00-04:00
draft = false
+++

## Overview {#overview}

-   The **Vulcans**, or **Vulcanians**, were a warp-capable humanoid species from the planet Vulcan. They were widely renowned for their strict adherence to logic and reason as well as their remarkable stoicism
-   In 2161, their homeworld became a founding member of the [United Federations of Planets]({{< relref "United Federations of Planets" >}})


## Backlinks {#backlinks}


### No linked reference {#no-linked-reference}


### Unlinked references {#unlinked-references}

[Show unlinked references]
