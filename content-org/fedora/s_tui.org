#+title: s-tui
#+date: [2021-07-30 Fri]
#+hugo_tags: fedora
[[https://github.com/amanusk/s-tui][Github]]
* Overview
- Stress-Terminal UI, s-tui, monitors CPU temperature, frequency, power and utilization in a graphical way from the terminal.
* Personal notes
- I use it to monitor CPU throttling and effectiveness of [[file:throttled.org][throttled]]
