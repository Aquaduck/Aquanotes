#+title: USS Enterprise (NCC-1701-D)
#+date: [2021-07-07 Wed]
* Overview
- The *USS Enterprise (NCC-1701-D) was a 24th century [[file:United Federations of Planets.org][Federation]] [[file:galaxy_class.org][Galaxy-class]] starship operated by [[file:starfleet.org][Starfleet]]
- It is the fifth Federation starship to bear the name /Enterprise/
