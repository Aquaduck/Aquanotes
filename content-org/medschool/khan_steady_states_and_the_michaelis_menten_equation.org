#+title: Khan - Steady states and the Michaelis Menten equation
#+date: [2021-07-13 Tue]
#+hugo_tags: medschool khan source
#+LATEX_HEADER: \usepackage{amsmath}
Link:  https://www.khanacademy.org/test-prep/mcat/biomolecules/enzyme-kinetics/v/steady-states-and-the-michaelis-menten-equation
* Video
#+BEGIN_EXPORT html
<iframe width="420" height="315"
src="https://youtu.be/7u2MkbsE_dw">
</iframe>
#+END_EXPORT

* Notes
** Steady-state assumption
- [[file:enzyme.org][Enzyme]] kinematics equation:
  \(E + S \overset{1}{\rightleftharpoons} ES \overset{2}{\rightleftharpoons} E + P\)
  + E = Enzyme
  + S = Substrate
  + P = Product
- /Steady-state assumption/: [ES] is constant
  + Formation of ES = Loss of ES
- Therefore, \(Rate_{1} + Rate_{-2} = Rate_{-1} + Rate_{2}\)
- Products rarely go back to reactants because these reactions are usually thermodynamically stable
  + Rate_{-2} is so insignificant compared to Rate_{1} that we can ignore it
  + The kinematics equation then looks like this:
    - \(E + S \overset{1}{\rightleftharpoons} ES \overset{2}{\rightarrow} E + P\)
** Deriving [[file:michaelis_menten_kinetics.org][Michaelis-Menten]] equation
Begin with these equations:
1) \(E + S \overset{1}{\rightleftharpoons} ES \overset{2}{\rightarrow} E + P\)
2) \(Rate_{1} = Rate_{-1} + Rate_{2}\)
Swap out Rate values for Rate constant (K) x reactants
- \(K_{1}[E][S] = K_{-1}[ES] + K_{2}[ES]\)
The total amount of enzyme in a system includes both bound and unbound enzymes
- \([E]_{T(otal)} = [E] + [ES]\)
Rearrange and substitute in for [E] and factor out common term [ES] on right side of equation
- \(K_{1}([E]_{T} - [ES])[S] = [ES](K_{-1} + K_{2})\)
Expand left side of equation
- \(K_{1}[E]_{T}[S] - K_{1}[ES][S] - [ES](K_{-1} + K_{2})\)
Divide both sides of equation by K_{1}
- \([E]_{T}[S] - [ES][S] = [ES](\frac{K_{-1} + K_{2}}{K_{1}})\)
Rate constants are constant values, so lets define this as one term:
- \(\frac{K_{-1} + K_{2}}{K_{1}} = K_{M}\)
Substitute in K_{m} and multiply both sides by [ES][S]
- \([E]_{t}[S] = [ES]K_{M} + [ES][S]\)
Flip equation around for legibility and factor out common terms
- \([ES](K_{m} + [S]) = [E]_{T}[S]\)
Divide both sides of equation by \(K_{M} + S\) to move that term to right side
- \([ES] = \frac{[E]_{T}[S]}{K_{M} + [S]}\)
Remember V_{o} is equal to the rate of formation of our product, which is equal to \(K_{2}[ES]\)
- \(V_{o} = \frac{\Delta P}{\Delta t} = K_{2}[ES]\)
Multiply both sides by \(K_{2}\)
- \(K_{2}[ES] = \frac{K_{2}[E]_{T}[S]}{K_{M} + [S]}\)
If \(V_{o} = V_{max}\), then \([E]_{T} = [ES]\), then \(K_{2}[E]_{T} = V_{max}\)
Now we substitute in V_{o} and V_{max}
- \(V_{o} = \frac{V_{max}[S]}{K_{M} + [S]}\)

** [[file:michaelis_menten_kinetics.org::*Michaelis constant][Michaelis constant]] (K_{M}) :ATTACH:
:PROPERTIES:
:ID:       1ef7df18-ee3f-412d-9c6c-0a81a50f7609
:END:
- What does K_{M} mean?
  + Let's assume \(K_{M} = [S]\)
  + If we substitute that into the Michaelis-Mentin equation, we get: \\
    \(V_{o} = \frac{V_{max}[S]}{2[S]}\) \\
    Which we can simplify further into \\
    \(V_{o} = \frac{V_{max}}{2}\)
  + Thus, K_{M} is the [S] where \(V_{o} = \frac{1}{2}V_{max}\)
#+ATTR_HTML: :width 400
#+CAPTION: The relationship between K_{M} and V_{max}
[[attachment:_20210713_212631screenshot.png]]

