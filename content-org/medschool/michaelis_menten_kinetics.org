#+title: Michaelis-Menten Kinetics
#+date: [2021-07-13 Tue]
#+hugo_tags: medschool concept
* Overview
- *Michaelis-Menten kinetics* models [[file:enzyme.org][enzyme]] kinetics
- It takes the form of an equation describing the rate of enzymatic reactions by relating reaction rate (\(\nu\)) to the concentration of a substrate \([S]\):
  + \(\nu = V_{max}\frac{[S]}{K_{M} + [S]}\)
* Michaelis constant
- K_{M}: the concentration of substrate where \(V_{o} = \frac{1}{2}V_{max}\)
- \(V_{o}\) is the rate of catalysis = number of moles of product formed per second

* Resources
- [[https://www.ncbi.nlm.nih.gov/books/NBK22430/][The Michaelis-Menten Model Accounts for the Kinetic Properties of Many Enzymes]]

