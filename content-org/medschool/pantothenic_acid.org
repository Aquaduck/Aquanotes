#+title: Pantothenic acid
#+date: [2021-07-29 Thu]
#+hugo_tags: medschool concept
#+roam_alias: "Vitamin B5" "Vitamin B_{5}"
* Overview
- AKA /Vitamin B_{5}/
- [[file:b_vitamin.org][B vitamin]]
* From First Aid Section II Biochemistry Nutrition
** Function
- Essential component of [[file:coenzyme_a.org][coenzyme A]] (used in synthesis) and [[file:fatty_acid_synthase.org][fatty acid synthase]]
