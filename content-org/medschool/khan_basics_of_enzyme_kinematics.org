#+title: Khan - Basics of enzyme kinematics
#+date: [2021-07-13 Tue]
#+hugo_tags: medschool khan source
* Basic [[file:enzyme.org][enzyme]] kinetics graphs
:PROPERTIES:
:ID:       ee29dacf-bb38-4c2a-8b5d-035910a95c57
:END:
#+ATTR_HTML: :width 400
#+CAPTION: Graph of hypothetical enzyme activity
[[attachment:_20210713_154751screenshot.png]]
* [[file:enzyme.org][Enzyme]] kinetics inhibitors
:PROPERTIES:
:ID:       58a4c21b-853d-4f30-9e02-ed796336bb13
:END:
- /Competitive inhibitors/: impair reaction by *binding to an enzyme and preventing the real substrate from binding*
  + Often binds at the active site
- /Noncompetitive inhibitors/: *does not impact substrate binding*, but binds elsewhere and prevents enzyme function
  #+ATTR_HTML: :width 400
  #+CAPTION: Difference in rate of reaction between normal enzymes, competitive inhibition, and noncompetitive inhibition
 [[attachment:_20210713_155056screenshot.png]]
** With a competitive inhibitor
- Reaction is able to reach its normal V_{max}, but takes a higher concentration of substrate to get there
  + V_{max} is unchanged, but apparenty K_{m} is higher.
- Why must more substrate be added in order to reach V_{max}?
  + Extra substrate makes substrate molecules abundant enough to consistently "beat" inhibitor molecules to the enzyme
** With a noncompetitive inhibitor
- Reaction never reaches its normal V_{max}, regardless of how much substrate
  + Due to inhibition, effective concentration of enzyme is reduced
- Reaction reaches half if its new V_{max} at the same substrate concentration -> *K_{m} is unchanged*
  + This is because the inhibitor doesn't affect binding of enzyme to substrate, just lowers concentration of usable enzyme
* Michaelis-Menten and allosteric enzymes
** [[file:michaelis_menten_kinetics.org][Michaelis-Menten Kinetics]]
- /Michaelis-Menten enzymes/: enzymes that behave according to Michaelis-Menten kinetics
** Allosteric enzymes
:PROPERTIES:
:ID:       584e8c34-ea86-46d3-b71a-e062a803af42
:END:
- Have multiple activation sites and display /cooperativity/
  + Binding of a substrate at one active site increases ability of other active sites to bind
- Diplay a "switch-like" transition from low to high reaction rate as substrate concentration increases:
#+ATTR_HTML: :width 400
#+CAPTION: "Switch-like" transition in allosteric enzymes
 [[attachment:_20210713_184431screenshot.png]]

