:PROPERTIES:
:ID:       6adde685-ed87-4b38-8b06-11817fcb6c6d
:END:
#+title: Openstax - Polyclonal and Monoclonal Antibody Production
#+date: [2021-07-15 Thu]
#+hugo_tags: medschool papers openstax source
Link: https://openstax.org/books/microbiology/pages/20-1-polyclonal-and-monoclonal-antibody-production#27566
#+CAPTION: Characteristics of Monoclonal and Polyclonal Antibodies
| Monoclonal Antibodies                                           | Polyclonal Antibodies                        |
|-----------------------------------------------------------------+----------------------------------------------|
| Expensive production                                            | Inexpensive production                       |
| Long production time                                            | Rapid production                             |
| Large quantities of /specific/ antibodies                       | Large quantities of /nonspecific/ antibodies |
| Recognizes /a single epitope/ on an antigen                     | Recognizes /multiple epitopes/ on an antigen |
| Production is continuous and uniform once the hybridoma is made | Different batches vary in composition        |

#+ATTR_HTML: :width 400
#+CAPTION: Interaction between antibodies and an antigen
[[attachment:_20210715_200156screenshot.png]]

