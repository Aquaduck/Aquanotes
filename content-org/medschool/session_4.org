#+title: Session 4
#+date: [2021-07-15 Thu]
#+hugo_tags: medschool jumpstart
* Application Packet 1
1) C
   - Because \(\Delta G^{o}\) is negative, \(K_{eq}\) must be greater than 1, and therefore at equilibrium *products must be greater than reactants*
     + Therefore it cannot be at equilibrium
2) B
3) B
4) A + D
   - Transition state = free energy of activation, so both are right
5) D
   - \(Products - Reactants\)
* Application Packet 2
1) B
2) C
3) A
4) A
5) C
6) E
7) A
