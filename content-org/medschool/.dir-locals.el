;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((org-hugo-section . "Medical School Notes")
         (org-hugo-base-dir . "~/Documents/logseq-org-roam-wiki/hugo/")))
 (org-mode . ((eval . (org-hugo-auto-export-mode)))))
